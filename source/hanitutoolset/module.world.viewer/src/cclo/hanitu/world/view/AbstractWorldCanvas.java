/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.MoleculeType;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.data.WormData;
import cclo.hanitu.gui.ColorProperty;
import cclo.hanitu.gui.FontProperty;
import cclo.hanitu.gui.PaintArea;

/**
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public abstract class AbstractWorldCanvas extends PaintArea{

    protected WorldViewer viewer;

    public final FontProperty textFont = new FontProperty();
    public final ColorProperty backgroundColor = new ColorProperty(Color.WHITE);
    public final SimpleBooleanProperty drawBoundary = new SimpleBooleanProperty(false);
    public final SimpleBooleanProperty drawGridLine = new SimpleBooleanProperty(false);
    public final SimpleIntegerProperty foodSize = new SimpleIntegerProperty(8);
    private int gridLineSpacing = 10;

    public AbstractWorldCanvas(){
    }

    public void setViewer(WorldViewer viewer){
        this.viewer = viewer;
        updateWorld();
    }

    public abstract Color getUserRepresentColor(String userID);

    public abstract Color getMoleculeRepresentColor(MoleculeType type);

    public void updateWorld(){
    }

    public int getGridLineSpacing(){
        return gridLineSpacing;
    }

    @SuppressWarnings("unused")
    public void setGridLineSpacing(int gridLineSpacing){
        this.gridLineSpacing = gridLineSpacing;
    }

    public abstract void paintArea(GraphicsContext g, double x, double y, double w, double h);

    public double getWorldBoundary(){
        if (viewer == null || viewer.world == null) return 100.0;
        return viewer.world.boundary.get();
    }

    /**
     * @param v x location in virtual world in unit 0.1mm
     * @return x location on screen
     */
    protected double getXFromVirtual(double v){
        return (v / viewer.world.boundary.get() + 1) * width.get() / 2;
    }

    /**
     * @param v y location in virtual world in unit 0.1mm
     * @return y location on screen
     */
    protected double getYFromVirtual(double v){
        return (1 - v / viewer.world.boundary.get()) * height.get() / 2;
    }

    /**
     * @param v x location on the screen
     * @return x location in the virtual in unit 0.1mm
     */
    protected double getXFromPane(double v){
        return (v * 2.0 / width.get() - 1) * viewer.world.boundary.get();
    }

    /**
     * @param v y location on the screen
     * @return y location in the virtual in unit 0.1mm
     */
    protected double getYFromPane(double v){
        return -((v * 2.0 / height.get() - 1) * viewer.world.boundary.get());
    }

    public PositionData findTarget(double x, double y){
        if (viewer != null){
            for (WormData w : viewer.worms){
                if (w.contact(x, y, w.size)) return w;
            }
            for (MoleculeData m : viewer.molecule){
                if (m.contact(x, y, foodSize.get())) return m;
            }
        }
        return new PositionData(x, y);
    }
}
