/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import cclo.hanitu.data.*;
import cclo.hanitu.gui.AbstractModule;
import cclo.hanitu.world.WorldConfig;

/**
 * @author antonio
 */
public abstract class AbstractWorldPlotModule extends AbstractModule<WorldViewerModulePlot> implements WorldPlotModule{

    @Override
    public void processSetWorldConfig(WorldConfig config){
    }

    @Override
    public void processSetWorldCanvas(AbstractWorldCanvas canvas){
    }

    @Override
    public void processSetLocation(){
    }

    @Override
    public void processLocationClose(){
    }

    @Override
    public void processLocationUpdate(LocationData data){
    }

    @Override
    public void processWormEat(WormData worm, MoleculeData molecule){
    }

    @Override
    public void processWormDie(WormData worm){
    }

    @Override
    public void processWormTouchWorm(WormData worm, WormData other){
    }

    @Override
    public void processWormTouchWall(WormData worm, PositionData wall){
    }

    @Override
    public boolean processMenuEvent(ActionEvent e, String action){
        return false;
    }

    @Override
    public boolean processKeyboardEvent(KeyEvent e){
        return false;
    }


    @Override
    public void processMousePressed(MouseEvent e, WormData worm){
    }

    @Override
    public void processMousePressed(MouseEvent e, MoleculeData molecule){
    }

    @Override
    public void processMousePressed(MouseEvent e, PositionData position){
    }

    @Override
    public void processMouseClicked(MouseEvent e, WormData worm){
    }

    @Override
    public void processMouseClicked(MouseEvent e, MoleculeData molecule){
    }

    @Override
    public void processMouseClicked(MouseEvent e, PositionData position){
    }

    @Override
    public void processMouseRightClicked(MouseEvent e, WormData worm){
    }

    @Override
    public void processMouseRightClicked(MouseEvent e, MoleculeData molecule){
    }

    @Override
    public void processMouseRightClicked(MouseEvent e, PositionData position){
    }

    @Override
    public void processMouseDoubleClicked(MouseEvent e, WormData worm){
    }

    @Override
    public void processMouseDoubleClicked(MouseEvent e, MoleculeData molecule){
    }

    @Override
    public void processMouseDoubleClicked(MouseEvent e, PositionData position){
    }

    @Override
    public void processMouseDragging(MouseEvent e, WormData worm, PositionData position){
    }

    @Override
    public void processMouseDragging(MouseEvent e, MoleculeData molecule, PositionData position){
    }

    @Override
    public void processMouseDragReleased(MouseEvent e, WormData worm, PositionData position){
    }

    @Override
    public void processMouseDragReleased(MouseEvent e, MoleculeData molecule, PositionData position){
    }
}
