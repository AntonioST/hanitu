/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import cclo.hanitu.data.LocationData;
import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.data.WormData;
import cclo.hanitu.gui.Module;

/**
 * @author antonio
 */
@SuppressWarnings({"UnusedParameters", "unused", "EmptyMethod"})
public interface WorldPlotModule extends Module<WorldViewerModulePlot>, WorldViewEvent{

    void processSetLocation();

    void processLocationClose();

    void processLocationUpdate(LocationData data);

    void processWormEat(WormData worm, MoleculeData molecule);

    void processWormDie(WormData worm);

    void processWormTouchWorm(WormData worm, WormData other);

    void processWormTouchWall(WormData worm, PositionData wall);
}
