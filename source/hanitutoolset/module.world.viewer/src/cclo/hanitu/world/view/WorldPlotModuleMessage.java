/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.util.*;

import javafx.event.ActionEvent;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import org.slf4j.helpers.MessageFormatter;

import cclo.hanitu.Base;
import cclo.hanitu.data.FormatClass;
import cclo.hanitu.data.Formatter;
import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.WormData;
import cclo.hanitu.gui.FontProperty;
import cclo.hanitu.gui.PaintArea;

/**
 * @author antonio
 */
public class WorldPlotModuleMessage extends AbstractWorldPlotModule{

    private final FontProperty messageFont = new FontProperty();

    private MessageArea messageArea;
    private AbstractWorldCanvas canvas;

    private final Deque<Message> messageQueue = new LinkedList<>();
    private String messageEat;
    private String messageDie;
    private String messageTouchWorm;
    private String messageTouchMolecule;

    private int messageDisplayCount;
    private int messageFadeDelay;
    private int messageFadeDecay;
    private transient long showAllMessageTime = -1;

    @Override
    public String getName(){
        return "message";
    }

    @Override
    public void setupModule(WorldViewerModulePlot host){
        WorldViewer world = host.getWorld();
        world.menu.addMenu("menu.view");
        canvas = world.canvas;
        host.addPaintArea(6, messageArea = new MessageArea(world));

        Properties p = Base.loadProperties();

        if (messageFont.get() == null){
            messageFont.set(Font.font(world.textFont.get().getFamily(),
                                      Double.parseDouble(p.getProperty("message.fontsize", "14"))));
        }

        messageEat = p.getProperty("worm.eat");
        messageDie = p.getProperty("worm.die");
        messageTouchWorm = p.getProperty("worm.touch");
        messageTouchMolecule = p.getProperty("molecule.touch");

        messageDisplayCount = Integer.parseInt(p.getProperty("message.count", "5"));
        messageFadeDelay = Integer.parseInt(p.getProperty("message.delay", "5000"));
        messageFadeDecay = Integer.parseInt(p.getProperty("message.decay", "1000"));
    }

    @Override
    public void destroy(){
        host.getWorld().menu.removeMenu("menu.view");
        host.removePaintArea(messageArea);
        super.destroy();
    }

    @Override
    public boolean processMenuEvent(ActionEvent e, String action){
        switch (action){
        case "menu.view.showmsg":
            showMessage();
            break;
        case "menu.view.showhide":
            showHideMessage(!messageArea.isVisible());
            break;
        case "menu.view.cleanmsg":
            cleanSysMessage();
            break;
        default:
            return false;
        }
        return true;
    }

    @Override
    public boolean processKeyboardEvent(KeyEvent e){
        switch (e.getCode()){
        case C:
            if (e.isControlDown()){
                cleanAllMessage();
            } else {
                cleanSysMessage();
            }
            break;
        case D:
            if (e.isShiftDown()){
                showHideMessage(!messageArea.isVisible());
            } else {
                showMessage();
            }
            break;
        default:
            return false;
        }
        return true;
    }

    @Override
    public void processSetWorldCanvas(AbstractWorldCanvas canvas){
        this.canvas = canvas;
    }

    @Override
    public void processMouseClicked(MouseEvent e, WormData worm){
        if (messageTouchWorm != null){
            try {
                messageQueue.addFirst(new Message(canvas.getUserRepresentColor(worm.user),
                                                  formatMessage(messageTouchWorm, worm),
                                                  false));
            } catch (IllegalFormatException ex){
                messageTouchWorm = null;
            }
        }
    }

    @Override
    public void processMouseClicked(MouseEvent e, MoleculeData molecule){
        if (messageTouchMolecule != null){
            try {
                messageQueue.addFirst(new Message(canvas.getMoleculeRepresentColor(molecule.type),
                                                  formatMessage(messageTouchMolecule, molecule),
                                                  false));
            } catch (IllegalFormatException ex){
                messageTouchMolecule = null;
            }
        }
    }

    @Override
    public void processWormEat(WormData worm, MoleculeData molecule){
        if (messageEat != null){
            try {
                messageQueue.addFirst(new Message(canvas.getUserRepresentColor(worm.user),
                                                  formatMessage(messageEat, worm, molecule),
                                                  true));
            } catch (IllegalFormatException e){
                messageEat = null;
            }
        }
    }

    @Override
    public void processWormDie(WormData worm){
        if (messageDie != null){
            try {
                messageQueue.addFirst(new Message(canvas.getUserRepresentColor(worm.user),
                                                  formatMessage(messageDie, worm),
                                                  true));
            } catch (IllegalFormatException e){
                messageDie = null;
            }
        }
    }

    @SuppressWarnings("unused")
    public void submitMessage(String message, Object... args){
        messageQueue.addFirst(new Message(Color.BLACK,
                                          MessageFormatter.arrayFormat(message, args).getMessage(),
                                          false));
    }

    public void submitMessageForWorm(String userID, String message, Object... args){
        messageQueue.addFirst(new Message(canvas.getUserRepresentColor(userID),
                                          MessageFormatter.arrayFormat(message, args).getMessage(),
                                          false));
    }

    private String formatMessage(String format, FormatClass target){
        Formatter expr = new Formatter();
        expr.setSingleReplace(op -> {
            if (op == 't'){
                return String.format("%5.2f", host.getCurrentTime() / 1000.0);
            } else {
                return target.formatSingleFlag(op);
            }
        });
        return expr.format(format);
    }

    private String formatMessage(String format, FormatClass t1, FormatClass t2){
        Formatter expr = new Formatter();
        expr.setSingleReplace(op -> {
            if (op == 't'){
                return String.format("%5.2f", host.getCurrentTime() / 1000.0);
            } else {
                String s = t1.formatSingleFlag(op);
                if (s == null) s = t2 != null? t2.formatSingleFlag(op): "something";
                return s;
            }
        });
        return expr.format(format);
    }

    @SuppressWarnings("WeakerAccess")
    public void showMessage(){
        long time = System.currentTimeMillis();
        messageQueue.forEach(m -> m.createTime = time);
        showAllMessageTime = time;
    }

    @SuppressWarnings("WeakerAccess")
    public void showHideMessage(boolean show){
        messageArea.setVisible(show);
    }

    @SuppressWarnings("WeakerAccess")
    public void cleanSysMessage(){
        messageQueue.removeIf(msg -> !msg.isNormalMessage);
    }

    @SuppressWarnings("WeakerAccess")
    public void cleanAllMessage(){
        messageQueue.clear();
    }

    private class Message{

        long createTime;
        final Color color;
        boolean isNormalMessage = true;
        final String message;

        public Message(Color color, String message, boolean normal){
            createTime = System.currentTimeMillis();
            this.color = color;
            this.message = message;
            isNormalMessage = normal;
        }

        Color getFadeColor(){
            int time = (int)(System.currentTimeMillis() - createTime);
            if (time < messageFadeDelay) return color;
            time -= messageFadeDelay;
            if (time > messageFadeDecay) return null;
            return new Color(color.getRed(), color.getGreen(), color.getBlue(), 1 - (double)time / messageFadeDecay);
        }

        @Override
        public String toString(){
            return message;
        }
    }

    private class MessageArea extends PaintArea{

        public MessageArea(WorldViewer world){
            x.bind(world.viewLeft);
            y.bind(world.viewTop);
            width.bind(world.viewLength);
            height.bind(world.viewLength);
        }

        @Override
        public void paint(GraphicsContext g){
            renderMessageText(g);
            if (canvas.drawGridLine.get()){
                renderMouseLocationText(g);
            }
        }

        private void renderMessageText(GraphicsContext g){
            Font font = messageFont.get();
            g.setFont(font);
            Iterator<Message> it = messageQueue.iterator();
            double button = height.get() - 5;
            int count = 0;
            double dec = font.getSize();
            while (it.hasNext()){
                Message msg = it.next();
                Color c = msg.getFadeColor();
                if (c == null){
                    showAllMessageTime = -1;
                    break;
                } else {
                    g.setFill(c);
                    g.fillText(msg.message, 5, button);
                    button -= dec;
                    count++;
                }
                if (button - dec < y.get()) break;
                if (showAllMessageTime < 0 && count >= messageDisplayCount) break;
            }
        }

        private void renderMouseLocationText(GraphicsContext g){
            int x = this.x.get();
            int y = this.y.get();
            WorldViewer world = host.getWorld();
            double mx = world.mouseX;
            double my = world.mouseY;
            if (x < mx && mx < width.get() + x && y < my && my < height.get() + y){
                g.setFont(messageFont.get());
                g.setFill(Color.DARKGRAY);
                g.fillText(String.format("(%.2f,%.2f)", canvas.getXFromPane(mx), canvas.getYFromPane(my)), mx, my);
            }
        }
    }
}
