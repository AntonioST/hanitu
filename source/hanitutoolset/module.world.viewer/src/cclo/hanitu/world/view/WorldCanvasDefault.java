/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;

import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.MoleculeType;
import cclo.hanitu.data.WormData;

/**
 * @author antonio
 */
public class WorldCanvasDefault extends AbstractWorldCanvas{

    //color
    private final Color[] userColors = {Color.RED,
                                        Color.GREEN,
                                        Color.BLUE,
                                        Color.CYAN,
                                        Color.ORANGE,
                                        Color.PINK,
                                        Color.GRAY,
                                        Color.YELLOW};
    private Color foodColor = Color.BLACK;

    private Color toxicantColor = Color.MAGENTA;

    private Color boundaryColor = Color.BLACK;

    private Color gridLineColor = Color.GRAY;

    private final List<String> nameList = new ArrayList<>();

    @Override
    public Color getUserRepresentColor(String userID){
        int i = nameList.indexOf(userID);
        return userColors[i >= 0? i: userColors.length - 1];
    }

    @Override
    public Color getMoleculeRepresentColor(MoleculeType type){
        return type == MoleculeType.FOOD? foodColor: toxicantColor;
    }

    @Override
    public void updateWorld(){
        nameList.clear();
        Set<String> set = new HashSet<>();
        viewer.forEachWorm(w -> set.add(w.user));
        nameList.addAll(set);
    }

    @Override
    public void paint(GraphicsContext g){
        if (drawBoundary.get()){
            renderBoundary(g);
        }
        if (drawGridLine.get()){
            renderGridLine(g);
        }
        viewer.forEachMolecule(m -> renderMolecule(g, m));
        viewer.forEachWorm(w -> renderWorm(g, w));
    }

    @Override
    public void paintArea(GraphicsContext g, double x, double y, double w, double h){
        g.setStroke(boundaryColor);
        g.setLineWidth(3);
        g.setLineCap(StrokeLineCap.ROUND);
        g.strokeRect(x, y, w, h);
    }

    @SuppressWarnings("WeakerAccess")
    protected void renderBoundary(GraphicsContext g){
        paintArea(g, 0, 0, width.get(), height.get());
    }

    @SuppressWarnings("WeakerAccess")
    protected void renderGridLine(GraphicsContext g){
        g.setStroke(gridLineColor);
        g.setLineDashes(10, 10);
        g.setLineWidth(1);
        double w = width.get();
        double h = height.get();
        double o = getXFromVirtual(0);
        int d = (int)Math.abs(getXFromVirtual(getGridLineSpacing()) - o);
        for (int i = (int)o; i < w; i += d){
            g.strokeLine(i, 0, i, h);
        }
        for (int i = (int)o - d; i >= 0; i -= d){
            g.strokeLine(i, 0, i, h);
        }
        for (int i = (int)o; i < h; i += d){
            g.strokeLine(0, i, w, i);
        }
        for (int i = (int)o - d; i >= 0; i -= d){
            g.strokeLine(0, i, w, i);
        }
        g.setLineDashes();
    }

    @SuppressWarnings("WeakerAccess")
    protected void renderMolecule(GraphicsContext g, MoleculeData data){
        if (data.type == MoleculeType.FOOD){
            g.setFill(foodColor);
        } else {
            g.setFill(toxicantColor);
        }
        int d = foodSize.get() >> 1;
        double x = getXFromVirtual(data.x) - d;
        double y = getYFromVirtual(data.y) - d;
        d <<= 1;
        g.fillRect(x, y, d, d);
    }

    @SuppressWarnings("WeakerAccess")
    protected void renderWorm(GraphicsContext g, WormData data){
        double sz = data.size * width.get() / getWorldBoundary();
        double sz2 = sz > 10? sz - 10: sz / 2;
        double x = getXFromVirtual(data.x);
        double y = getYFromVirtual(data.y);
        g.setFill(getUserRepresentColor(data.user));
        double d = sz / 2;
        g.fillOval(x - d, y - d, sz, sz);
        d = sz2 / 2;
        g.setFill(Color.gray(data.hp / 100));
        g.fillOval(x - d, y - d, sz2 - 1, sz2 - 1);
    }
}
