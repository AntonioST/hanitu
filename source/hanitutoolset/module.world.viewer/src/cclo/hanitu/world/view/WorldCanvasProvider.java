/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.util.Map;
import java.util.ServiceLoader;

import cclo.hanitu.Base;

/**
 * @author antonio
 */
public class WorldCanvasProvider{

    private WorldCanvasProvider(){
        throw new RuntimeException();
    }

    public static AbstractWorldCanvas getCanvas(String style){
        if(style == null) style = Base.getProperty("cclo.hanitu.plot.style", "default");
        ServiceLoader<WorldCanvasManager> loader = ServiceLoader.load(WorldCanvasManager.class);
        for (WorldCanvasManager manager : loader){
            Map<String, Class<AbstractWorldCanvas>> map = manager.getCanvas();
            if (map.containsKey(style)) try {
                return map.get(style).newInstance();
            } catch (InstantiationException | IllegalAccessException e){
                throw new RuntimeException(e);
            }
        }
        try {
            return (AbstractWorldCanvas)Class.forName(style).newInstance();
        } catch (InstantiationException | IllegalAccessException e){
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e){
            throw new RuntimeException("unknown style : " + style, e);
        }
    }
}
