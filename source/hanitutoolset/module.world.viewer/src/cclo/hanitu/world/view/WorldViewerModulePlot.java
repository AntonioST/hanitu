/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.data.*;
import cclo.hanitu.gui.FPS;
import cclo.hanitu.gui.KeyboardBufferInt;
import cclo.hanitu.gui.ModuleManager;
import cclo.hanitu.gui.PaintArea;
import cclo.hanitu.io.TailHistoryStream;
import cclo.hanitu.io.TailStream;
import cclo.hanitu.world.WorldConfig;

/**
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class WorldViewerModulePlot extends AbstractWorldViewerModule implements TimeDataHandler,
                                                                                LocationDataHandler,
                                                                                EventDataHandler{

    private final Logger LOG = LoggerFactory.getLogger(WorldViewerModulePlot.class);

    public final ModuleManager<WorldViewerModulePlot, WorldPlotModule> module
      = new ModuleManager<>(this, WorldPlotModule.class);

    private Timeline timeline;
    private volatile boolean pause = false;

    private final SimpleIntegerProperty currentTime = new SimpleIntegerProperty(0);
    private Integer jumpTime;
    private int jumpTimeStep = 100;
    private final KeyboardBufferInt keyboardBuffer = new KeyboardBufferInt();

    private final FPS fpsRecorder = new FPS();
    private double fps = 50;

    private TailHistoryStream<LocationData> locationStream;
    private TailHistoryStream<EventData> eventStream;
    private boolean lastFetchLocationResult;

    @Override
    public String getName(){
        return "plot";
    }

    @Override
    public void setupModule(WorldViewer world){
        LOG.debug("setup");
        world.menu.addMenu("menu.play");
        //
        world.viewLeft.set(10);
        world.viewTop.set(10);
        world.viewExtend.set(130);
        //
        LOG.trace("time line init");
        timeline = new Timeline(new KeyFrame(new Duration(1000 / fps), this::timeLineKeyFrame));
        timeline.setCycleCount(Timeline.INDEFINITE);
        LOG.trace("time line play");
        timeline.play();
    }

    public WorldViewer getWorld(){
        return host;
    }

    @Override
    public void destroy(){
        LOG.debug("destroy");
        module.removeAll();
        host.menu.removeMenu("menu.play");
        //
        LOG.trace("time line stop");
        timeline.stop();
        timeline = null;
        //
        super.destroy();
    }

    protected void addPaintArea(int layer, PaintArea area){
        Objects.requireNonNull(area);
        host.content.addPanel(layer, area);
    }

    protected void removePaintArea(PaintArea area){
        host.content.removePanel(area);
    }

    @Override
    public boolean processMenuEvent(ActionEvent e, String action){
        return !processMenuEventPrivate(e, action) && module.root.processMenuEvent(e, action);
    }

    @Override
    public boolean processKeyboardEvent(KeyEvent e){
        return !processKeyboardEventPrivate(e) && module.root.processKeyboardEvent(e);
    }

    @Override
    public void processSetWorldConfig(WorldConfig data){
        LOG.debug("set world data : {}", data);
        module.root.processSetWorldConfig(data);
    }

    @Override
    public void processSetWorldCanvas(AbstractWorldCanvas canvas){
        LOG.debug("set world canvas : {}", canvas);
        canvas.drawBoundary.set(true);
        module.root.processSetWorldCanvas(canvas);
        host.repaintCanvas();
    }

    @Override
    public void processMousePressed(MouseEvent e, WormData worm){
        module.root.processMousePressed(e, worm);
    }

    @Override
    public void processMousePressed(MouseEvent e, MoleculeData molecule){
        module.root.processMousePressed(e, molecule);
    }

    @Override
    public void processMousePressed(MouseEvent e, PositionData position){
        module.root.processMousePressed(e, position);
    }

    @Override
    public void processMouseClicked(MouseEvent e, WormData worm){
        module.root.processMouseClicked(e, worm);
    }

    @Override
    public void processMouseClicked(MouseEvent e, MoleculeData molecule){
        module.root.processMouseClicked(e, molecule);
    }

    @Override
    public void processMouseClicked(MouseEvent e, PositionData position){
        module.root.processMouseClicked(e, position);
    }

    @Override
    public void processMouseRightClicked(MouseEvent e, WormData worm){
        module.root.processMouseRightClicked(e, worm);
    }

    @Override
    public void processMouseRightClicked(MouseEvent e, MoleculeData molecule){
        module.root.processMouseRightClicked(e, molecule);
    }

    @Override
    public void processMouseRightClicked(MouseEvent e, PositionData position){
        module.root.processMouseRightClicked(e, position);
    }

    @Override
    public void processMouseDoubleClicked(MouseEvent e, WormData worm){
        module.root.processMouseDoubleClicked(e, worm);
    }

    @Override
    public void processMouseDoubleClicked(MouseEvent e, MoleculeData molecule){
        module.root.processMouseDoubleClicked(e, molecule);
    }

    @Override
    public void processMouseDoubleClicked(MouseEvent e, PositionData position){
        module.root.processMouseDoubleClicked(e, position);
    }

    @Override
    public void processMouseDragging(MouseEvent e, WormData worm, PositionData position){
        module.root.processMouseDragging(e, worm, position);
    }

    @Override
    public void processMouseDragging(MouseEvent e, MoleculeData molecule, PositionData position){
        module.root.processMouseDragging(e, molecule, position);
    }

    @Override
    public void processMouseDragReleased(MouseEvent e, WormData worm, PositionData position){
        module.root.processMouseDragReleased(e, worm, position);
    }

    @Override
    public void processMouseDragReleased(MouseEvent e, MoleculeData molecule, PositionData position){
        module.root.processMouseDragReleased(e, molecule, position);
    }

    public void forwardTime(boolean increaseShiftMount, boolean toEnd){
        int time;
        if (!toEnd){
            if (jumpTimeStep < 0){
                jumpTimeStep = 100;
            } else if (increaseShiftMount){
                jumpTimeStep += 100;
            }
            time = currentTime.get() + jumpTimeStep;
        } else {
            time = -1;
        }
        jumpTime = time;
    }

    public void backwardTime(boolean increaseShiftMount, boolean toEnd){
        int time;
        if (!toEnd){
            if (jumpTimeStep > 0){
                jumpTimeStep = -100;
            } else if (increaseShiftMount){
                jumpTimeStep -= 100;
            }
            time = currentTime.get() + jumpTimeStep;
        } else {
            time = 0;
        }
        if (time <= 0){
            time = 0;
        }
        jumpTime = time;
    }

    public boolean isGotoTimeEnable(){
        return keyboardBuffer.isValid();
    }

    public int getGotoTime(){
        return keyboardBuffer.get().getAsInt();
    }

    private void gotoTime(){
        if (keyboardBuffer.isValid()){
            gotoTime(keyboardBuffer.clear().getAsInt());
        }
    }

    public void gotoTime(int time){
        jumpTime = time;
    }

    @SuppressWarnings("unused")
    public double getFPSExpect(){
        return fps;
    }

    public int getFPSActual(){
        return fpsRecorder.getAvgFPS();
    }

    public void setFPS(double fps){
        if (fps < 10) fps = 10;
        else if (fps > 100) fps = 100;
        LOG.debug("set FPS {}", fps);
        timeline.stop();
        timeline.getKeyFrames().clear();
        timeline.getKeyFrames().add(new KeyFrame(new Duration(1000 / fps), this::timeLineKeyFrame));
        timeline.play();
        this.fps = fps;
    }

    @SuppressWarnings("UnusedParameters")
    private void timeLineKeyFrame(ActionEvent e){
        fpsRecorder.update();
        updateLocation();
        repaint();
    }


    private void increaseFPS(boolean inc){
        int v = (int)(fps + (inc? 10: -10));
        if (v > 100) return;
        if (v < 10){
            v = 10;
            setFPS(v);
        }
        setFPS(v);
    }

    public void play(){
        pause = false;
    }

    public void pause(){
        pause = true;
    }

    @SuppressWarnings("unused")
    public boolean isPause(){
        return pause;
    }

    @Override
    public ReadOnlyIntegerProperty getTimeProperty(){
        return currentTime;
    }

    @Override
    public boolean updateUntilTime(int time){
        if (pause || locationStream == null) return false;
        TimeSerialData.untilTime(locationStream, time, this::updateLocation);
        return true;
    }

    public void updateLocation(){
        lastFetchLocationResult = false;
        if (pause || locationStream == null) return;
        LocationData location = null;
        if (jumpTime != null && jumpTime >= 0){
            LOG.debug("jump to time {}", jumpTime);
            locationStream.head();
            if (jumpTime > 0){
                location = TimeSerialData.getAfterTime(locationStream, jumpTime);
            }
            jumpTime = null;
        } else {
            if (jumpTime != null){
                assert jumpTime < 0;
                LOG.debug("jump to time end");
                jumpTime = null;
                locationStream.end();
            }
            location = locationStream.next();
        }
        if (location != null){
            lastFetchLocationResult = true;
            updateLocation(location);
        }
    }

    public void updateLocation(LocationData data){
        currentTime.set(data.time);
        data.forEach(this::updateWormLocation);
        Platform.runLater(() -> module.root.processLocationUpdate(data));
        updateEvent(data.time);
    }

    public void updateWormLocation(WormData data){
        WormData w = host.getWorm(data.user, data.worm);
        if (w != null){
            w.x = data.x;
            w.y = data.y;
            w.hp = data.hp;
        }
    }

    public void updateEvent(int time){
        if (eventStream == null) return;
        if (eventStream.get() != null && eventStream.get().time > time){
            eventStream.head();
            TimeSerialData.getAfterTime(eventStream, time);
        } else {
            TimeSerialData.untilTime(eventStream, time, this::updateEvent);
        }
    }

    public void updateEvent(EventData event){
        switch (event.type){
        case TOUCH_FOOD:
        case TOUCH_TOXICANT:{
            EventData.MoleculeEvent eat = (EventData.MoleculeEvent)event;
            module.root.processWormEat(host.getWorm(eat.user, eat.worm),
                                       host.getMolecule(eat.getMoleculeType(), eat.getMoleculeID()));
            break;
        }
        case WORM_DIE:
            module.root.processWormDie(host.getWorm(event.user, event.worm));
            break;
        case MOVE_UP:
        case MOVE_DOWN:
        case MOVE_LEFT:
        case MOVE_RIGHT:
            if (LOG.isTraceEnabled()){
                EventData.MoveEvent move = (EventData.MoveEvent)event;
                WorldPlotModuleMessage message = (WorldPlotModuleMessage)module.get("message");
                if (message != null){
                    message.submitMessageForWorm(move.user, "[{}] {}[{}] move {}",
                                                 move.time,
                                                 move.user,
                                                 move.worm,
                                                 move.getDirection().name());
                } else {
                    LOG.trace("[{}] {}[{}] move {}",
                              move.time,
                              move.user,
                              move.worm,
                              move.getDirection().name());
                }
            }
            break;
        }
    }

    @Override
    public void setLocationFile(String path){
        LOG.debug("set location file {}", path);
        setLocationStream(TailStream.follow(path).map(LocationData::parseLine,
                                                      e -> LOG.warn("location data parsing", e)));
    }

    @Override
    public void setLocationFile(Path path){
        LOG.debug("set location file {}", path);
        setLocationStream(TailStream.follow(path).map(LocationData::parseLine,
                                                      e -> LOG.warn("location data parsing", e)));
    }

    @Override
    public void setLocationStream(TailStream<LocationData> iterator){
        timeline.stop();
        //
        closeLocationStream();
        LOG.debug("set location stream");
        locationStream = new TailHistoryStream<>(iterator);
        currentTime.set(0);
        module.root.processSetLocation();
        //
        timeline.play();
    }

    @Override
    public void closeLocationStream(){
        closeEventStream();
        //
        timeline.stop();
        jumpTime = null;
        if (locationStream != null){
            LOG.debug("close location stream");
            //noinspection EmptyCatchBlock
            try {
                locationStream.close();
            } catch (IOException e){
            }
            locationStream = null;
            module.root.processLocationClose();
        }
    }

    @Override
    public void setEventFile(String path){
        LOG.debug("set event file {}", path);
        setEventStream(TailStream.follow(path).map(EventData::parseLine, e -> LOG.warn("event data parsing", e)));
    }

    @Override
    public void setEventFile(Path path){
        LOG.debug("set event file {}", path);
        setEventStream(TailStream.follow(path).map(EventData::parseLine, e -> LOG.warn("event data parsing", e)));
    }

    @Override
    public void setEventStream(TailStream<EventData> iterator){
        closeEventStream();
        LOG.debug("set event stream");
        eventStream = new TailHistoryStream<>(iterator);
    }

    @Override
    public void closeEventStream(){
        if (eventStream != null){
            LOG.debug("close event stream");
            //noinspection EmptyCatchBlock
            try {
                eventStream.close();
            } catch (IOException e){
            }
            eventStream = null;
        }
    }

    public void repaint(){
        host.content.repaint();
    }

    public String getStatusText(){
        return pause? "pause": lastFetchLocationResult? "": "end";
    }

    private boolean processMenuEventPrivate(ActionEvent e, String action){
        switch (action){
        case "menu.play.pause":
            if (pause){
                play();
            } else {
                pause();
            }
            break;
        case "menu.play.forward":
            forwardTime(false, false);
            break;
        case "menu.play.backward":
            backwardTime(false, false);
            break;
        case "menu.play.gotohead":
            backwardTime(false, true);
            break;
        case "menu.play.gotoend":
            forwardTime(false, true);
            break;
        case "menu.play.goto":
            if (!keyboardBuffer.isValid()){
                keyboardBuffer.add(0);
            } else {
                gotoTime();
            }
            break;
        case "menu.play.fpsinc":
            increaseFPS(true);
            break;
        case "menu.play.fpsdec":
            increaseFPS(false);
            break;
        default:
            return false;
        }
        return true;
    }

    private boolean processKeyboardEventPrivate(KeyEvent e){
        if (keyboardBuffer.processKeyboardEvent(e.getCode())) return true;
        switch (e.getCode()){
        case SPACE:
            if (pause){
                play();
            } else {
                pause();
            }
            break;
        case LEFT:
            backwardTime(e.isShiftDown(), e.isControlDown());
            break;
        case RIGHT:
            forwardTime(e.isShiftDown(), e.isControlDown());
            break;
        case ENTER:
            gotoTime();
            break;
        case PAGE_UP:
            increaseFPS(true);
            break;
        case PAGE_DOWN:
            increaseFPS(false);
            break;
        default:
            return false;
        }
        return true;
    }
}
