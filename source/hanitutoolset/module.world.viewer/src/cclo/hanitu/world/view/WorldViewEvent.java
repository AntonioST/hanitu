/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.data.WormData;
import cclo.hanitu.world.WorldConfig;

/**
 * @author antonio
 */
public interface WorldViewEvent{

    boolean processMenuEvent(ActionEvent e, String action);

    boolean processKeyboardEvent(KeyEvent e);

    void processSetWorldConfig(WorldConfig config);

    void processSetWorldCanvas(AbstractWorldCanvas canvas);

    void processMousePressed(MouseEvent e, WormData worm);

    void processMousePressed(MouseEvent e, MoleculeData molecule);

    void processMousePressed(MouseEvent e, PositionData position);

    void processMouseClicked(MouseEvent e, WormData worm);

    void processMouseClicked(MouseEvent e, MoleculeData molecule);

    void processMouseClicked(MouseEvent e, PositionData position);

    void processMouseRightClicked(MouseEvent e, WormData worm);

    void processMouseRightClicked(MouseEvent e, MoleculeData molecule);

    void processMouseRightClicked(MouseEvent e, PositionData position);

    void processMouseDoubleClicked(MouseEvent e, WormData worm);

    void processMouseDoubleClicked(MouseEvent e, MoleculeData molecule);

    void processMouseDoubleClicked(MouseEvent e, PositionData position);

    void processMouseDragging(MouseEvent e, WormData worm, PositionData position);

    void processMouseDragging(MouseEvent e, MoleculeData molecule, PositionData position);

    /**
     *
     * @param e mouse event
     * @param worm dragged worm. If editable, worm's position will be updated
     * @param position new position
     */
    void processMouseDragReleased(MouseEvent e, WormData worm, PositionData position);

    /**
     *
     * @param e mouse event
     * @param molecule dragged molecule. If editable, molecule's position will be updated
     * @param position new position
     */
    void processMouseDragReleased(MouseEvent e, MoleculeData molecule, PositionData position);
}
