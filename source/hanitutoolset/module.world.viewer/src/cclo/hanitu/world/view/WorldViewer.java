/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.util.*;
import java.util.function.Consumer;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.scene.control.MenuBar;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.text.Font;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.Base;
import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.MoleculeType;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.data.WormData;
import cclo.hanitu.exe.About;
import cclo.hanitu.gui.*;
import cclo.hanitu.world.MoleculeConfig;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.WormConfig;

/**
 * @author antonio
 */
public class WorldViewer extends FXApplication{

    private final Logger LOG = LoggerFactory.getLogger(WorldViewer.class);

    static{
        Base.setProperty("cclo.hanitu.world.style", "shadow");
    }

    final IntegerProperty viewLeft = new SimpleIntegerProperty(0);
    final IntegerProperty viewTop = new SimpleIntegerProperty(0);
    final IntegerProperty viewLength = new SimpleIntegerProperty(500);
    final IntegerProperty viewExtend = new SimpleIntegerProperty(0);
    final IntegerProperty viewWidth = new SimpleIntegerProperty();
    final IntegerProperty viewHeight = new SimpleIntegerProperty();
    final IntegerProperty viewRight = new SimpleIntegerProperty();
    final IntegerProperty viewBottom = new SimpleIntegerProperty();

    {
        viewRight.bind(viewLeft.add(viewLength));
        viewBottom.bind(viewTop.add(viewLength));
        viewWidth.bind(viewLeft.multiply(2).add(viewLength));
        viewHeight.bind(viewTop.add(viewLength).add(viewExtend));
    }

    final FontProperty textFont = new FontProperty();

    public final ModuleManager<WorldViewer, WorldViewerModule> module;
    final PaintLayer content;
    final MenuManager menu;

    AbstractWorldCanvas canvas;
    WorldConfig world;
    final List<WormData> worms = new ArrayList<>();
    final List<MoleculeData> molecule = new ArrayList<>();
    private boolean editable = false;

    double mouseX;
    double mouseY;
    private MouseButton mousePressButton;
    private PositionData mousePressedTarget;
    private PositionData mouseDruggingTarget;

    public WorldViewer(){
        primaryStage.setResizable(false);
        viewWidth.addListener((v, o, n) -> primaryStage.setWidth(n.doubleValue()));
        viewHeight.addListener((v, o, n) -> primaryStage.setHeight(n.doubleValue()));
        //
        Properties p = Base.loadProperties();
        setTitle(p.getProperty("title"));
        textFont.set(Font.font(p.getProperty("font"), Double.parseDouble(p.getProperty("font.size"))));
        //
        menu = initMenu();
        content = initCanvas();
        module = new ModuleManager<>(this, WorldViewerModule.class);
        root.getChildren().addAll(menu.getMenuBar(), content);
        //
        setCanvas(new WorldCanvasDefault());
        //
        primaryStage.sizeToScene();
        primaryStage.show();
        content.repaint();
    }

    private MenuManager initMenu(){
        MenuManager ret = new MenuManager(this::processMenuEvent);
        MenuBar menuBar = ret.getMenuBar();
        menuBar.managedProperty().bind(menuBar.visibleProperty());
        menuBar.setVisible(false);
        return ret;
    }

    @SuppressWarnings("unused")
    protected void resetMenu(String menu){
        LOG.debug("reset menu : {}", menu);
        this.menu.resetTopMenu(menu);
    }

    private PaintLayer initCanvas(){
        PaintLayer ret = new PaintLayer(viewWidth, viewHeight);
        ret.setOnMouseMoved(e -> {
            mouseX = e.getX();
            mouseY = e.getY();
        });
        ret.setOnMousePressed(e -> {
            mousePressButton = e.getButton();
            if (canvas != null){
                mousePressedTarget = canvas.findTarget(canvas.getXFromPane(e.getX() - canvas.x.get()),
                                                       canvas.getYFromPane(e.getY() - canvas.y.get()));
            }
            if (mousePressButton == MouseButton.PRIMARY){
                if (mousePressedTarget instanceof WormData){
                    module.root.processMousePressed(e, (WormData)mousePressedTarget);

                } else if (mousePressedTarget instanceof MoleculeData){
                    module.root.processMousePressed(e, (MoleculeData)mousePressedTarget);
                } else {
                    module.root.processMousePressed(e, mouseDruggingTarget);
                }
                ret.repaint();
            }
        });
        ret.setOnMouseClicked(e -> {
            if (canvas != null){
                MouseButton button = e.getButton();
                PositionData target = canvas.findTarget(canvas.getXFromPane(e.getX() - canvas.x.get()),
                                                        canvas.getYFromPane(e.getY() - canvas.y.get()));
                if (target instanceof WormData){
                    if (button == MouseButton.PRIMARY){
                        if (e.getClickCount() > 1){
                            module.root.processMouseDoubleClicked(e, (WormData)target);
                        } else {
                            module.root.processMouseClicked(e, (WormData)target);
                        }
                    } else if (button == MouseButton.SECONDARY){
                        module.root.processMouseRightClicked(e, (WormData)target);
                    }
                } else if (target instanceof MoleculeData){
                    if (button == MouseButton.PRIMARY){
                        if (e.getClickCount() > 1){
                            module.root.processMouseDoubleClicked(e, (MoleculeData)target);
                        } else {
                            module.root.processMouseClicked(e, (MoleculeData)target);
                        }
                    } else if (button == MouseButton.SECONDARY){
                        module.root.processMouseRightClicked(e, (MoleculeData)target);
                    }
                } else {
                    if (button == MouseButton.PRIMARY){
                        if (e.getClickCount() > 1){
                            module.root.processMouseDoubleClicked(e, target);
                        } else {
                            module.root.processMouseClicked(e, target);
                        }
                    } else if (button == MouseButton.SECONDARY){
                        module.root.processMouseRightClicked(e, target);
                    }
                }
                ret.repaint();
            }
        });
        ret.setOnMouseDragged(e -> {
            mouseX = e.getX();
            mouseY = e.getY();
            if (mouseDruggingTarget == null){
                mouseDruggingTarget = mousePressedTarget;
            }
            if (canvas != null && mouseDruggingTarget != null){
                PositionData pos = new PositionData(canvas.getXFromPane(mouseX - canvas.x.get()),
                                                    canvas.getYFromPane(mouseY - canvas.y.get()));
                if (editable){
                    mouseDruggingTarget.x = pos.x;
                    mouseDruggingTarget.y = pos.y;
                }
                if (mouseDruggingTarget instanceof WormData){
                    module.root.processMouseDragging(e, (WormData)mouseDruggingTarget, pos);
                } else if (mouseDruggingTarget instanceof MoleculeData){
                    module.root.processMouseDragging(e, (MoleculeData)mouseDruggingTarget, pos);
                }
                ret.repaint();
            }
        });
        ret.setOnMouseReleased(e -> {
            if (canvas != null && mouseDruggingTarget != null && mousePressButton == MouseButton.PRIMARY){
                PositionData pos = new PositionData(canvas.getXFromPane(mouseX - canvas.x.get()),
                                                    canvas.getYFromPane(mouseY - canvas.y.get()));
                if (editable){
                    mouseDruggingTarget.x = pos.x;
                    mouseDruggingTarget.y = pos.y;
                }
                if (mouseDruggingTarget instanceof WormData){
                    module.root.processMouseDragReleased(e, (WormData)mouseDruggingTarget, pos);
                } else if (mouseDruggingTarget instanceof MoleculeData){
                    module.root.processMouseDragReleased(e, (MoleculeData)mouseDruggingTarget, pos);
                }
                ret.repaint();
            }
            mouseDruggingTarget = null;
            mousePressedTarget = null;
        });
        return ret;
    }

    @Override
    public void close(){
        module.removeAll();
        super.close();
    }

    @Override
    protected void processMenuEvent(ActionEvent event, String action){
        if (action.startsWith("&")){
            try {
                MenuSession.processReflectionMenuEvent(this, event, action.substring(1));
            } catch (RuntimeException e){
                MessageSession.showErrorMessageDialog(e);
            }
        } else if (!module.root.processMenuEvent(event, action)){
            switch (action){
            case "menu.file.exit":
                close();
                break;
            case "menu.view.grid":
                canvas.drawGridLine.set(!canvas.drawGridLine.get());
                break;
            case "menu.help.about":
                FXApplication.launch(About.class).show();
                break;
            }
        }
    }

    @Override
    protected boolean processKeyboardEvent(KeyEvent e){
        switch (e.getCode()){
        case ALT:
            menu.getMenuBar().setVisible(!menu.getMenuBar().isVisible());
            break;
        case F5:
            content.repaint();
            break;
        default:
            return module.root.processKeyboardEvent(e);
        }
        return true;
    }

    public void setCanvas(AbstractWorldCanvas canvas){
        if (this.canvas != null){
            content.removePanel(this.canvas);
        }
        this.canvas = canvas;
        canvas.setViewer(this);
        content.addPanel(0, canvas);
        content.backgroundColor.bind(canvas.backgroundColor);
        canvas.x.bind(viewLeft);
        canvas.y.bind(viewTop);
        canvas.width.bind(viewLength);
        canvas.height.bind(viewLength);
        LOG.debug("set canvas : {}", canvas);
        module.root.processSetWorldCanvas(canvas);
        if (world != null) setWorld(world);
    }

    public void repaintCanvas(){
        content.repaint();
    }

    public void updateWorldConfig(){
        worms.clear();
        molecule.clear();
        world.worms().stream()
          .map(WormConfig::asWormBindData)
          .forEach(worms::add);
        world.molecule().stream()
          .map(MoleculeConfig::asMoleculeBindData)
          .forEach(molecule::add);
        canvas.updateWorld();
        content.repaint();
    }

    public void setWorld(WorldConfig config){
        world = config;
        worms.clear();
        molecule.clear();
        config.worms().stream()
          .map(WormConfig::asWormBindData)
          .forEach(worms::add);
        config.molecule().stream()
          .map(MoleculeConfig::asMoleculeBindData)
          .forEach(molecule::add);
        canvas.updateWorld();
        module.root.processSetWorldConfig(world);
    }

    @SuppressWarnings("unused")
    public WorldConfig getWorld(){
        return world;
    }

    public List<WormData> getWorms(){
        return Collections.unmodifiableList(worms);
    }

    public WormData getWorm(String user, String id){
        Objects.requireNonNull(user, "user ID");
        Objects.requireNonNull(id, "worm ID");
        for (WormData w : worms){
            if (user.equals(w.user) && id.equals(w.worm)) return w;
        }
        return null;
    }

    public List<MoleculeData> getMolecule(){
        return Collections.unmodifiableList(molecule);
    }

    public MoleculeData getMolecule(MoleculeType type, String id){
        for (MoleculeData m : molecule){
            if (m.type == type && m.ID.equals(id)) return m;
        }
        return null;
    }

    public void forEachWorm(Consumer<WormData> c){
        worms.forEach(c);
    }

    public void forEachMolecule(Consumer<MoleculeData> c){
        molecule.forEach(c);
    }

    @SuppressWarnings("unused")
    public boolean isEditable(){
        return editable;
    }

    public void setEditable(boolean editable){
        LOG.debug("set editable : {}", editable);
        this.editable = editable;
    }

// --Commented out by Inspection START (3/4/16 6:15 PM):
//    public double getXFromPane(double v){
//        return canvas == null? 0: canvas.getXFromPane(v);
//    }
// --Commented out by Inspection STOP (3/4/16 6:15 PM)

// --Commented out by Inspection START (3/4/16 6:16 PM):
//    public double getYFromPane(double v){
//        return canvas == null? 0: canvas.getYFromPane(v);
//    }
// --Commented out by Inspection STOP (3/4/16 6:16 PM)
}
