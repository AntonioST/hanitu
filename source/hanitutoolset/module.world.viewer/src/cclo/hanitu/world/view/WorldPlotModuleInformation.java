/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import cclo.hanitu.gui.GraphicTextUtils;
import cclo.hanitu.gui.PaintArea;
import cclo.hanitu.world.WorldConfig;

/**
 * @author antonio
 */
public class WorldPlotModuleInformation extends AbstractWorldPlotModule{

    private AbstractWorldCanvas canvas;
    private String[] nameList;
    private int maxNameLength;
    private double[] hpList;
    private int[] userCountList;

    private PaintArea area;

    private transient String currentTimeText;
    private transient String gotoTimeText;
    private transient String fpsText;

    @Override
    public String getName(){
        return "information";
    }

    @Override
    public void setupModule(WorldViewerModulePlot host){
        area = new InformationBoard(host.getWorld());
        host.addPaintArea(2, area);
    }

    @Override
    public void destroy(){
        host.removePaintArea(area);
        super.destroy();
    }

    @Override
    public void processSetWorldConfig(WorldConfig config){
        updateUser();
    }

    @Override
    public void processSetWorldCanvas(AbstractWorldCanvas canvas){
        this.canvas = canvas;
        updateUser();
    }

    private void updateUser(){
        Map<String, Integer> map = new HashMap<>();
        host.getWorld().forEachWorm(w -> map.compute(w.user, (k, v) -> v == null? 1: v + 1));
        int size = map.size();
        nameList = map.keySet().toArray(new String[size]);
        Arrays.sort(nameList);
        hpList = new double[size];
        Arrays.fill(hpList, 0);
        userCountList = new int[size];
        maxNameLength = 0;
        for (int i = 0; i < size; i++){
            userCountList[i] = map.get(nameList[i]);
            maxNameLength = Math.max(maxNameLength, nameList[i].length());
        }
    }

    private void updateText(){
        String state = host.getStatusText();
        double time = host.getCurrentTime() / 1000.0;
        if (state.isEmpty()){
            currentTimeText = String.format("Time: %.4f s", time);
        } else {
            currentTimeText = String.format("Time: %.4f s (%s)", time, state);
        }
        if (host.isGotoTimeEnable()){
            gotoTimeText = String.format("goto: %d ms", host.getGotoTime());
        } else {
            gotoTimeText = null;
        }
        fpsText = String.format("FPS: %d", host.getFPSActual());
    }

    private void updateAvgHp(){
        if (hpList == null) return;
        Arrays.fill(hpList, 0);
        host.getWorld().forEachWorm(worm -> {
            int i = Arrays.binarySearch(nameList, worm.user);
            if (i >= 0){
                hpList[i] += worm.hp;
            }
        });
        for (int i = 0, length = nameList.length; i < length; i++){
            hpList[i] /= userCountList[i];
        }
    }

    private class InformationBoard extends PaintArea{

        public InformationBoard(WorldViewer world){
            x.bind(world.viewLeft);
            y.bind(world.viewBottom.add(10));
            width.bind(world.viewLength);
            height.bind(world.viewHeight.subtract(world.viewBottom).subtract(10));
        }

        @Override
        public void paint(GraphicsContext g){
            if (canvas == null) return;
            updateText();
            updateAvgHp();
            //
            Font font = host.getWorld().textFont.get();
            g.setFont(font);
            //
            double size = font.getSize();
            double left = 10;
            double top = size;
            double height = GraphicTextUtils.getTextHeight(g, "test");
            double width = this.width.get();
            int length = nameList.length;
            canvas.paintArea(g, 0, 0, width, height * (1 + length) + size);
            //
            g.setFill(Color.BLACK);
            g.fillText(currentTimeText, left, top);
            if (gotoTimeText != null){
                g.fillText(gotoTimeText, width / 2 - size * 4, top);
            }
            g.fillText(fpsText, width - size * 7, top);
            //
            top += size;
            left = 10 + maxNameLength * size;
            for (int i = 0; i < length; i++){
                String user = nameList[i];
                double avg = hpList[i];
                g.setFill(canvas.getUserRepresentColor(user));
                g.fillText(user, 10, top);
                g.fillText(String.format("avg: %2.2f", avg), left, top);
                top += size;
            }
        }
    }
}
