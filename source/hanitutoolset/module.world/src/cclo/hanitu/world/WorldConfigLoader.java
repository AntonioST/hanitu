/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import cclo.hanitu.data.MoleculeType;
import cclo.hanitu.io.FileLoader;

/**
 * The loader of world_config file.This class is reusable but not thread-safe.
 */
public class WorldConfigLoader extends FileLoader<WorldConfig>{

    /**
     * identify file type for meta header
     */
    public static final String WORLD_CONFIG_TYPE = "world_config";

    static final String BLOCK_START_WORM = "setworminf";
    static final String BLOCK_END_WORM = "endsetworminf";
    static final String USER_ID = "userid";
    static final String WORM_ID = "wormid";
    static final String WORM_INIT_X = "initialx";
    static final String WORM_INIT_Y = "initialy";
    static final String WORM_SIZE = "wormsize";
    static final String WORM_TIME_DECAY = "timedecay";
    static final String WORM_STEP_DECAY = "stepdecay";
    static final String WORM_CIRCUIT_FILE = "filename";
    static final String BLOCK_START_WORLD = "setworld";
    static final String BLOCK_END_WORLD = "endsetworld";
    static final String DELTA_HEALTH_POINT = "dhp";
    static final String WORLD_BOUNDARY = "boundary";
    static final String WORLD_TYPE = "type";
    static final String WORLD_DEPTH = "depth";
    static final String MOLECULE_COUNT_MODE = "countmode";
    static final String FIXED_MODE = "fixed";
    static final String BLOCK_START_FOOD = "foodlocation";
    static final String BLOCK_END_FOOD = "endfoodlocation";
    static final String BLOCK_START_TOXICANT = "toxicantlocation";
    static final String BLOCK_END_TOXICANT = "endtoxicantlocation";
    static final String MOLECULE_FOOD_ID = "fid";
    static final String MOLECULE_TOXICANT_ID = "tid";
    static final String MOLECULE_X = "x";
    static final String MOLECULE_Y = "y";
    static final String MOLECULE_COUNT = "count";
    static final String MOLECULE_DIFFUSE = "diffusioncoef";
    static final String MOLECULE_CONCENTRATION = "concentration";
    static final String MOLECULE_DELAY = "delaytime";
    static final String GAIN_FF = "gainff";
    static final String GAIN_FT = "gainft";
    static final String GAIN_TF = "gaintf";
    static final String GAIN_TT = "gaintt";
    static final String GAIN_NPY = "gainnpy";
    static final String BASELINE_FF = "baselineff";
    static final String BASELINE_FT = "baselineft";
    static final String BASELINE_TF = "baselinetf";
    static final String BASELINE_TT = "baselinett";
    static final String BASELINE_NPY = "baselinenpy";

    private static final int STATE_WORM = 1;
    private static final int STATE_WORLD = 2;
    private static final int STATE_FOOD = 3;
    private static final int STATE_TOXICANT = 4;

    //
    private WorldConfig world;
    private WormConfig worm;
    private MoleculeConfig molecule;
    private String user;
    //
    private boolean ignoreCircuitFile;

    @Override
    public String getFileType(){
        return WORLD_CONFIG_TYPE;
    }

    @Override
    public String getFileVersionExpression(){
        return ">=1.4";
    }

    @SuppressWarnings("unused")
    public boolean isIgnoreCircuitFile(){
        return ignoreCircuitFile;
    }

    public void setIgnoreCircuitFile(boolean ignoreCircuitFile){
        this.ignoreCircuitFile = ignoreCircuitFile;
    }

    @Override
    public void prevProcess() throws IOException{
        super.prevProcess();
        world = new WorldConfig();
        worm = null;
        molecule = null;
    }

    @Override
    public WorldConfig postProcess() throws IOException{
        world.file = sourcePath;
        return world;
    }

    @Override
    public void processLine(String line) throws IOException{
        switch (currentState){
        case STATE_INIT:
            parseInit(line);
            break;
        case STATE_WORM:
            parseWorms(line);
            break;
        case STATE_WORLD:
            parseWorld(line);
            break;
        case STATE_FOOD:
        case STATE_TOXICANT:
            parseMolecule(line);
            break;
        }
    }

    private void parseInit(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        switch (line.toLowerCase()){
        case BLOCK_START_WORM:
            currentState = STATE_WORM;
            break;
        case BLOCK_START_WORLD:
            currentState = STATE_WORLD;
            break;
        default:
            addError("unknown keyword : '{}'", getKey(line));
        }
    }

    private void parseWorms(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        switch (getKey(line).toLowerCase()){
        case USER_ID:
            user = getStrValue(line);
            break;
        case WORM_ID:
            worm = new WormConfig(user, getStrValue(line), null);
            world.addWorm(worm);
            break;
        case WORM_INIT_X:
            worm.x.set(getFloatValue(line));
            break;
        case WORM_INIT_Y:
            worm.y.set(getFloatValue(line));
            break;
        case WORM_SIZE:
            worm.size.set(getFloatValue(line));
            break;
        case WORM_TIME_DECAY:
            worm.timeDecay.set(getFloatValue(line));
            break;
        case WORM_STEP_DECAY:
            worm.stepDecay.set(getFloatValue(line));
            break;
        case WORM_CIRCUIT_FILE:
            worm.circuitFileName.set(checkCircuitFile(getValue(line)));
            break;
        case BLOCK_END_WORM:
            currentState = STATE_INIT;
            break;
        default:
            addError("unknown keyword : '{}'", getKey(line));
        }
    }


    private void parseWorld(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        switch (getKey(line).toLowerCase()){
        case DELTA_HEALTH_POINT:
            world.deltaHealthPoint.set(getFloatValue(line));
            break;
        case GAIN_FF:
            world.gainFF.set(getFloatValue(line));
            break;
        case GAIN_FT:
            world.gainFT.set(getFloatValue(line));
            break;
        case GAIN_TF:
            world.gainTF.set(getFloatValue(line));
            break;
        case GAIN_TT:
            world.gainTT.set(getFloatValue(line));
            break;
        case GAIN_NPY:
            world.gainNPY.set(getFloatValue(line));
            break;
        case BASELINE_FF:
            world.baselineFF.set(getFloatValue(line));
            break;
        case BASELINE_FT:
            world.baselineFT.set(getFloatValue(line));
            break;
        case BASELINE_TF:
            world.baselineTF.set(getFloatValue(line));
            break;
        case BASELINE_TT:
            world.baselineTT.set(getFloatValue(line));
            break;
        case BASELINE_NPY:
            world.baselineNPY.set(getFloatValue(line));
            break;
        case WORLD_BOUNDARY:
            world.boundary.set(getIntValue(line));
            break;
        case WORLD_TYPE:
            world.type.set(getIntValue(line));
            break;
        case WORLD_DEPTH:
            world.depth.set(getFloatValue(line));
            break;
        case MOLECULE_COUNT_MODE:
            world.fixFoodCount.set(getIntValue(line) != 0);
            break;
        case FIXED_MODE:
            world.fixWormLocation.set(getIntValue(line) != 0);
            break;
        case BLOCK_START_FOOD:
            currentState = STATE_FOOD;
            break;
        case BLOCK_START_TOXICANT:
            currentState = STATE_TOXICANT;
            break;
        case BLOCK_END_WORLD:
            currentState = STATE_INIT;
            break;
        default:
            addError("unknown keyword : '{}'", getKey(line));
        }
    }

    private void parseMolecule(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        switch (getKey(line).toLowerCase()){
        case MOLECULE_FOOD_ID:
            if (currentState != STATE_FOOD){
                addError("unexpected keyword : '{}'", getKey(line));
            }
            world.addMolecule(molecule = new MoleculeConfig(MoleculeType.FOOD, getStrValue(line)));
            break;
        case MOLECULE_TOXICANT_ID:
            if (currentState != STATE_TOXICANT){
                addError("unexpected keyword : '{}'", getKey(line));
            }
            world.addMolecule(molecule = new MoleculeConfig(MoleculeType.TOXICANT, getStrValue(line)));
            break;
        case MOLECULE_X:
            molecule.x.set(getFloatValue(line));
            break;
        case MOLECULE_Y:
            molecule.y.set(getFloatValue(line));
            break;
        case MOLECULE_COUNT:
            molecule.count.set(getIntValue(line));
            break;
        case MOLECULE_DIFFUSE:
            molecule.diffuse.set(getFloatValue(line));
            break;
        case MOLECULE_CONCENTRATION:
            molecule.concentration.set(getFloatValue(line));
            break;
        case MOLECULE_DELAY:
            molecule.delay.set(getIntValue(line));
            break;
        case BLOCK_END_FOOD:
            if (currentState != STATE_FOOD){
                addError("unexpected keyword : '{}'", getKey(line));
            }
            currentState = STATE_WORLD;
            break;
        case BLOCK_END_TOXICANT:
            if (currentState != STATE_TOXICANT){
                addError("unexpected keyword : '{}'", getKey(line));
            }
            currentState = STATE_WORLD;
            break;
        default:
            addError("unknown keyword : '{}'", getKey(line));
        }
    }

    private String checkCircuitFile(String circuitFile) throws HanituLoadingException{
        if (ignoreCircuitFile){
            if (circuitFile != null && circuitFile.isEmpty()) return null;
        } else if (circuitFile == null){
            addError("lost circuit file");
        } else if (isFailOnLoad()){
            Path path;
            if (sourcePath.getParent() != null){
                path = sourcePath.getParent();
            } else {
                path = Paths.get(System.getProperty("user.dir"));
            }
            if (!Files.isRegularFile(path.resolve(circuitFile))){
                addError("file not found {}", circuitFile);
            }
        }
        return circuitFile;
    }

    /**
     * testing main
     * @param args world config files
     */
    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public static void main(String[] args) throws IOException{
        for (String arg : args){
            System.out.println(arg);
            WorldConfigLoader loader = new WorldConfigLoader();
            loader.setFailOnLoad(true);
            loader.load(arg);
        }
    }
}
