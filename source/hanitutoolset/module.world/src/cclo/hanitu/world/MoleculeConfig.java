/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.util.Objects;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import cclo.hanitu.data.DataClass;
import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.MoleculeType;

/**
 * molecule configuration, has sub-type food and toxicant.
 *
 * @author antonio
 */
public class MoleculeConfig implements DataClass{

    /**
     * molecule type
     */
    public final MoleculeType type;
    /**
     * identify number, different type can shared same id.
     */
    public final ReadOnlyStringProperty ID = new SimpleStringProperty(this, "id");

    public final SimpleDoubleProperty x = new SimpleDoubleProperty(this, "x");
    public final SimpleDoubleProperty y = new SimpleDoubleProperty(this, "y");
    public final SimpleIntegerProperty count = new SimpleIntegerProperty(this, "count", 10);
    public final SimpleDoubleProperty diffuse = new SimpleDoubleProperty(this, "diffusioncoef", 0.000015);
    public final SimpleDoubleProperty concentration = new SimpleDoubleProperty(this, "concentration", 100);
    public final SimpleIntegerProperty delay = new SimpleIntegerProperty(this, "delay", 1000);

    public MoleculeConfig(MoleculeType type, String ID){
        Objects.requireNonNull(ID, "molecule ID");
        this.type = Objects.requireNonNull(type, "module type");
        ((SimpleStringProperty)this.ID).set(ID);
    }

    public MoleculeConfig(MoleculeConfig molecule){
        this(molecule.ID.get(), molecule);
    }

    /**
     * create a molecule with copy the value from the reference
     * @param ID identify number
     * @param molecule reference molecule
     */
    public MoleculeConfig(String ID, MoleculeConfig molecule){
        Objects.requireNonNull(ID, "molecule ID");
        ((SimpleStringProperty)this.ID).set(ID);
        type = molecule.type;
        x.set(molecule.x.get());
        y.set(molecule.y.get());
        count.set(molecule.count.get());
        diffuse.set(molecule.diffuse.get());
        concentration.set(molecule.concentration.get());
        delay.set(molecule.delay.get());
    }

    public MoleculeData asMoleculeData(){
        MoleculeData ret = new MoleculeData(type, ID.get());
        ret.x = x.get();
        ret.y = y.get();
        return ret;
    }

    public MoleculeData asMoleculeBindData(){
        MoleculeData ret = asMoleculeData();
        x.addListener((v, o, n) -> ret.x = n.doubleValue());
        y.addListener((v, o, n) -> ret.y = n.doubleValue());
        return ret;
    }
}
