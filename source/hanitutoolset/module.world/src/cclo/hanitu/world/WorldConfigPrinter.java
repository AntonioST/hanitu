/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.util.Objects;

import cclo.hanitu.data.MoleculeType;
import cclo.hanitu.io.FilePrinter;

import static cclo.hanitu.world.WorldConfigLoader.*;

/**
 * output world configuration, accept {@link  WorldConfig}.
 *
 * @author antonio
 */
public class WorldConfigPrinter extends FilePrinter<WorldConfig>{

    private boolean humanReadable = true;

    @Override
    public String getFileType(){
        return WorldConfigLoader.WORLD_CONFIG_TYPE;
    }

    @Override
    public String getTargetVersionExpression(){
        return "1.4";
    }


    @SuppressWarnings("unused")
    public boolean isHumanReadable(){
        return humanReadable;
    }

    @SuppressWarnings("unused")
    public void setHumanReadable(boolean humanReadable){
        this.humanReadable = humanReadable;
    }

    private String get(String k){
        return humanReadable? getHumanReadable(k): k;
    }

    private static String getHumanReadable(String k){
        switch (k.toLowerCase()){
        case BLOCK_START_WORM:
            return "SetWormInf";
        case BLOCK_END_WORM:
            return "EndSetWormInf";
        case USER_ID:
            return "UserID";
        case WORM_ID:
            return "WormID";
        case WORM_INIT_X:
            return "InitialX";
        case WORM_INIT_Y:
            return "InitialY";
        case WORM_SIZE:
            return "WormSize";
        case WORM_TIME_DECAY:
            return "TimeDecay";
        case WORM_STEP_DECAY:
            return "StepDecay";
        case WORM_CIRCUIT_FILE:
            return "FileName";
        case BLOCK_START_WORLD:
            return "SetWorld";
        case BLOCK_END_WORLD:
            return "EndSetWorld";
        case DELTA_HEALTH_POINT:
            return "dHP";
        case WORLD_BOUNDARY:
            return "Boundary";
        case WORLD_TYPE:
            return "Type";
        case WORLD_DEPTH:
            return "Depth";
        case MOLECULE_COUNT_MODE:
            return "CountMode";
        case FIXED_MODE:
            return "Fixed";
        case BLOCK_START_FOOD:
            return "FoodLocation";
        case BLOCK_END_FOOD:
            return "EndFoodLocation";
        case BLOCK_START_TOXICANT:
            return "ToxicantLocation";
        case BLOCK_END_TOXICANT:
            return "EndToxicantLocation";
        case MOLECULE_FOOD_ID:
            return "FID";
        case MOLECULE_TOXICANT_ID:
            return "TID";
        case MOLECULE_X:
            return "X";
        case MOLECULE_Y:
            return "Y";
        case MOLECULE_COUNT:
            return "Count";
        case MOLECULE_DIFFUSE:
            return "DiffusionCoef";
        case MOLECULE_CONCENTRATION:
            return "Concentration";
        case MOLECULE_DELAY:
            return "DelayTime";
        case GAIN_FF:
            return "GainFF";
        case GAIN_FT:
            return "GainFT";
        case GAIN_TF:
            return "GainTF";
        case GAIN_TT:
            return "GainTT";
        case GAIN_NPY:
            return "GainNPY";
        case BASELINE_FF:
            return "BaselineFF";
        case BASELINE_FT:
            return "BaselineFT";
        case BASELINE_TF:
            return "BaselineTF";
        case BASELINE_TT:
            return "BaselineTT";
        case BASELINE_NPY:
            return "BaselineNPY";
        default:
            throw new IllegalArgumentException("unknown keyword : " + k);
        }
    }

    @Override
    protected void process(WorldConfig world){
        preWorm();
        for (WormConfig w : world.worms()){
            line();
            processWorm(w);
        }
        line();
        postWorm();

        preWorld();
        line();
        processWordParameter(world);
        line();

        preFood();
        world.molecule().stream()
          .filter(m -> m.type == MoleculeType.FOOD)
          .forEach(this::processMolecule);
        postFood();
        line();

        preToxicant();
        world.molecule().stream()
          .filter(m -> m.type == MoleculeType.TOXICANT)
          .forEach(this::processMolecule);
        postToxicant();
        line();

        postWorld();
    }


    private void preWorm(){
        line(get(BLOCK_START_WORM));
    }

    private void processWorm(WormConfig worm){
        pair(get(USER_ID), worm.user);
        pair(get(WORM_ID), worm.worm);
        pair(get(WORM_INIT_X), worm.x);
        pair(get(WORM_INIT_Y), worm.y);
        pair(get(WORM_SIZE), worm.size);
        pair(get(WORM_TIME_DECAY), worm.timeDecay);
        pair(get(WORM_STEP_DECAY), worm.stepDecay);
        pair(get(WORM_CIRCUIT_FILE), Objects.toString(worm.circuitFileName.get(), ""));
    }

    private void postWorm(){
        line(get(BLOCK_END_WORM));
    }

    private void preWorld(){
        line(get(BLOCK_START_WORLD));
    }

    private void processWordParameter(WorldConfig world){
        pair(get(DELTA_HEALTH_POINT), world.deltaHealthPoint);
        pair(get(GAIN_FF), world.gainFF);
        pair(get(GAIN_FT), world.gainFT);
        pair(get(GAIN_TF), world.gainTF);
        pair(get(GAIN_TT), world.gainTT);
        pair(get(GAIN_NPY), world.gainNPY);
        pair(get(BASELINE_FF), world.baselineFF);
        pair(get(BASELINE_FT), world.baselineFT);
        pair(get(BASELINE_TF), world.baselineTF);
        pair(get(BASELINE_TT), world.baselineTT);
        pair(get(BASELINE_NPY), world.baselineNPY);
        //
        pair(get(WORLD_BOUNDARY), world.boundary);
        pair(get(WORLD_TYPE), world.type);
        pair(get(WORLD_DEPTH), world.depth);
        pair(get(MOLECULE_COUNT_MODE), world.fixFoodCount.get()? 1: 0);
        pair(get(FIXED_MODE), world.fixWormLocation.get()? 1: 0);
    }

    private void postWorld(){
        line(get(BLOCK_END_WORLD));
    }

    private void preFood(){
        line(get(BLOCK_START_FOOD));
    }

    private void processMolecule(MoleculeConfig molecule){
        if (molecule.type == MoleculeType.FOOD){
            pair(get(MOLECULE_FOOD_ID), molecule.ID);
        } else if (molecule.type == MoleculeType.TOXICANT){
            pair(get(MOLECULE_TOXICANT_ID), molecule.ID);
        } else {
            throw new RuntimeException();
        }
        pair(get(MOLECULE_X), molecule.x);
        pair(get(MOLECULE_Y), molecule.y);
        pair(get(MOLECULE_COUNT), molecule.count);
        pair(get(MOLECULE_DIFFUSE), molecule.diffuse);
        pair(get(MOLECULE_CONCENTRATION), molecule.concentration);
        pair(get(MOLECULE_DELAY), molecule.delay);

    }

    private void postFood(){
        line(get(BLOCK_END_FOOD));
    }

    private void preToxicant(){
        line(get(BLOCK_START_TOXICANT));
    }

    private void postToxicant(){
        line(get(BLOCK_END_TOXICANT));
    }
}
