/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.util.Objects;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

import cclo.hanitu.data.DataClass;
import cclo.hanitu.data.WormData;

/**
 * @author antonio
 */
public class WormConfig implements DataClass{

    /**
     * identify , different user can shared same worm.
     */
    public final ReadOnlyStringProperty worm;
    /**
     * identify for user
     */
    public final ReadOnlyStringProperty user;

    public final SimpleDoubleProperty x = new SimpleDoubleProperty(this, "x");
    public final SimpleDoubleProperty y = new SimpleDoubleProperty(this, "y");
    public final SimpleDoubleProperty size = new SimpleDoubleProperty(this, "size", 3);
    public final SimpleDoubleProperty timeDecay = new SimpleDoubleProperty(this, "timedecay", 100);
    public final SimpleDoubleProperty stepDecay = new SimpleDoubleProperty(this, "stepdecay", 0.01);
    public final SimpleStringProperty circuitFileName = new SimpleStringProperty(this, "circuit");

    /**
     * create a worm with (program) default value
     */
    public WormConfig(){
        this(WormName.genName(), "", null);
    }

    public WormConfig(WormConfig wc){
        this(wc.user.get(), wc.worm.get(), wc);
    }

    /**
     * create a worm with copy value from reference
     *
     * @param user identify for user
     * @param worm identify
     * @param wc   reference worm
     */
    public WormConfig(String user, String worm, WormConfig wc){
        Objects.requireNonNull(user, "user ID");
        Objects.requireNonNull(worm, "worm ID");
        this.user = new SimpleStringProperty(this, "worm.user", user);
        this.worm = new SimpleStringProperty(this, "worm.worm", worm);
        if (wc != null){
            x.set(wc.x.get());
            y.set(wc.y.get());
            size.set(wc.size.get());
            timeDecay.set(wc.timeDecay.get());
            stepDecay.set(wc.stepDecay.get());
            circuitFileName.set(wc.circuitFileName.get());
        }
    }

    public WormData asWormData(){
        WormData ret = new WormData();
        ret.user = user.get();
        ret.worm = worm.get();
        ret.size = size.getValue().intValue();
        ret.x = x.get();
        ret.y = y.get();
        return ret;
    }

    public WormData asWormBindData(){
        WormData ret = asWormData();
        user.addListener((v, o, n) -> ret.user = n);
        worm.addListener((v, o, n) -> ret.worm = n);
        size.addListener((v, o, n) -> ret.size = n.intValue());
        x.addListener((v, o, n) -> ret.x = n.doubleValue());
        y.addListener((v, o, n) -> ret.y = n.doubleValue());
        return ret;
    }
}


