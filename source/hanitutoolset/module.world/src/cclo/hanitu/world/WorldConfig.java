/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;

import cclo.hanitu.data.DataClass;
import cclo.hanitu.data.EnumProperty;
import cclo.hanitu.data.MoleculeType;
import cclo.hanitu.data.SwitchProperty;

/**
 * world configuration.
 *
 * @author antonio
 */
public class WorldConfig implements DataClass{

    public static final WorldConfig EMPTY = new WorldConfig();
    public static final Comparator<WormConfig> WORM_CMP
      = Comparator.<WormConfig, String>comparing(w -> w.user.get(), String::compareToIgnoreCase)
      .thenComparing(w -> w.user.get(), String::compareToIgnoreCase);
    public static final Comparator<MoleculeConfig> MOLECULE_CMP
      = Comparator.<MoleculeConfig>comparingInt(w -> w.type.ordinal())
      .thenComparing(w -> w.ID.get(), String::compareToIgnoreCase);

    public Path file;

    public final SimpleDoubleProperty deltaHealthPoint = new SimpleDoubleProperty(this, "deltahealthpoint", 10);
    public final SimpleDoubleProperty gainFF = new SimpleDoubleProperty(this, "gainff", 10);
    public final SimpleDoubleProperty baselineFF = new SimpleDoubleProperty(this, "baselineff", 5);
    public final SimpleDoubleProperty gainFT = new SimpleDoubleProperty(this, "gainft", 1);
    public final SimpleDoubleProperty baselineFT = new SimpleDoubleProperty(this, "baselineft", 5);
    public final SimpleDoubleProperty gainTF = new SimpleDoubleProperty(this, "gaintf", 1);
    public final SimpleDoubleProperty baselineTF = new SimpleDoubleProperty(this, "baselinetf", 5);
    public final SimpleDoubleProperty gainTT = new SimpleDoubleProperty(this, "gaintt", 10);
    public final SimpleDoubleProperty baselineTT = new SimpleDoubleProperty(this, "baselinett", 5);
    public final SimpleDoubleProperty gainNPY = new SimpleDoubleProperty(this, "gainnpy", -20);
    public final SimpleDoubleProperty baselineNPY = new SimpleDoubleProperty(this, "baselinenpy", 2000);
    public final SimpleIntegerProperty boundary = new SimpleIntegerProperty(this, "boundary", 60);
    public final EnumProperty type = new EnumProperty(this, "type", 0);
    public final SimpleDoubleProperty depth = new SimpleDoubleProperty(this, "depth", 0.264);
    public final SwitchProperty fixFoodCount = new SwitchProperty(this, "fixfoodcount", true);
    public final SwitchProperty fixWormLocation = new SwitchProperty(this, "fixwormlocation", false);
    //
    /**
     * molecule in world
     */
    public final SimpleListProperty<MoleculeConfig> molecule
      = new SimpleListProperty<>(FXCollections.observableArrayList());
    /**
     * worms in world
     */
    public final SimpleListProperty<WormConfig> worms
      = new SimpleListProperty<>(FXCollections.observableArrayList());

    /**
     * create a world with (program) default value
     */
    public WorldConfig(){
    }

    /**
     * create a world which copy from reference
     *
     * @param wc reference world
     */
    public WorldConfig(WorldConfig wc){
        deltaHealthPoint.set(wc.deltaHealthPoint.get());
        gainFF.set(wc.gainFF.get());
        gainFT.set(wc.gainFT.get());
        gainTF.set(wc.gainTF.get());
        gainTT.set(wc.gainTT.get());
        gainNPY.set(wc.gainNPY.get());
        baselineFF.set(wc.baselineFF.get());
        baselineFT.set(wc.baselineFT.get());
        baselineTF.set(wc.baselineTF.get());
        baselineTT.set(wc.baselineTT.get());
        baselineNPY.set(wc.baselineNPY.get());
        boundary.set(wc.boundary.get());
        type.set(wc.type.get());
        depth.set(wc.depth.get());
        fixFoodCount.set(wc.fixFoodCount.get());
        fixWormLocation.set(wc.fixWormLocation.get());
        wc.molecule.stream().map(MoleculeConfig::new).forEach(molecule::add);
        wc.worms.stream().map(WormConfig::new).forEach(worms::add);
    }

    public List<WormConfig> worms(){
        return Collections.unmodifiableList(worms);
    }

    public List<MoleculeConfig> molecule(){
        return Collections.unmodifiableList(molecule);
    }

    @SuppressWarnings("unused")
    public int userCount(){
        return (int)worms.stream().map(w -> w.user).distinct().count();
    }

    public int wormCount(){
        return worms.size();
    }

    public int foodCount(){
        return (int)molecule().stream().filter(m -> m.type == MoleculeType.FOOD).count();
    }

    public int toxicantCount(){
        return (int)molecule().stream().filter(m -> m.type == MoleculeType.TOXICANT).count();
    }

    public Set<String> getUserNameSet(){
        return worms.stream().map(w -> w.user.get()).collect(Collectors.toSet());
    }

    public Set<String> getWormNameSet(String user){
        Objects.requireNonNull(user);
        return worms.stream()
          .filter(w -> user.equals(w.user.get()))
          .map(w -> w.worm.get())
          .collect(Collectors.toSet());
    }

    public boolean containUser(String user){
        Objects.requireNonNull(user, "user name");
        return worms.stream().anyMatch(w -> user.equals(w.user.get()));
    }

    @SuppressWarnings("unused")
    public void changeUserName(String oldName, String newName){
        Objects.requireNonNull(oldName);
        Objects.requireNonNull(newName);
        Set<String> set = getUserNameSet();
        if (set.contains(newName)) throw new IllegalArgumentException("user '" + newName + "' ha been used");
        if (!set.contains(oldName)) return;
        for (WormConfig worm : worms){
            if (oldName.equals(worm.user.get())){
                ((SimpleStringProperty)worm.user).set(newName);
            }
        }
    }

    @SuppressWarnings("unused")
    public void changeWormName(String user, String oldName, String newName){
        Objects.requireNonNull(user);
        Objects.requireNonNull(oldName);
        Objects.requireNonNull(newName);
        Set<String> set = getWormNameSet(user);
        if (set.contains(newName)) throw new IllegalArgumentException("worm name '" + newName + "' has been used");
        if (!set.contains(oldName)) return;
        for (WormConfig worm : worms){
            if (user.equals(worm.user.get()) && oldName.equals(worm.worm.get())){
                ((SimpleStringProperty)worm.worm).set(newName);
            }
        }
    }

    @SuppressWarnings("unused")
    public void changeWormName(String user, Function<String, String> rename){
        Objects.requireNonNull(user);
        Objects.requireNonNull(rename);
        Map<String, String> map = getWormNameSet(user).stream().collect(Collectors.toMap(Function.identity(), rename));
        if (map.size() != new HashSet<>(map.values()).size()) throw new IllegalArgumentException("name duplicate");
        for (WormConfig worm : worms){
            if (user.equals(worm.user.get())){
                ((SimpleStringProperty)worm.worm).set(map.get(worm.worm.get()));
            }
        }
    }

    /**
     * get worm in world
     *
     * @param user user identify number
     * @param ID   worm identify number
     * @return worm, null if not found
     */
    public WormConfig getWorm(String user, String ID){
        Objects.requireNonNull(user, "user ID");
        Objects.requireNonNull(ID, "worm ID");
        for (WormConfig w : worms){
            if (user.equals(w.user.get()) && ID.equals(w.worm.get())) return w;
        }
        return null;
    }

    /**
     * get molecule in the world
     *
     * @param type molecule type
     * @param ID   identify number
     * @return food or toxicant or null if not found
     */
    public MoleculeConfig getMolecule(MoleculeType type, String ID){
        Objects.requireNonNull(type, "molecule type");
        Objects.requireNonNull(ID, "molecule id");
        for (MoleculeConfig m : molecule){
            if (m.type == type && ID.equals(m.ID.get())) return m;
        }
        return null;
    }

    public void addWorm(WormConfig config){
        Objects.requireNonNull(config.user, "user ID");
        Objects.requireNonNull(config.worm, "worm ID");
        if (getWorm(config.user.get(), config.worm.get()) != null){
            throw new RuntimeException("Worm ID duplicate : user=" + config.user.get() + ", ID=" + config.worm.get());
        }
        worms.add(config);
        worms.sort(WORM_CMP);
    }

    public void addMolecule(MoleculeConfig config){
        Objects.requireNonNull(config.type, "molecule type");
        if (getMolecule(config.type, config.ID.get()) != null){
            throw new RuntimeException("Molecule ID duplicate : type" + config.type + ", ID=" + config.ID);
        }
        molecule.add(config);
        molecule.sort(MOLECULE_CMP);
    }

    public WormConfig removeWorm(WormConfig config){
        return removeWorm(config.user.get(), config.worm.get());
    }

    public WormConfig removeWorm(String user, String id){
        Objects.requireNonNull(user, "user ID");
        Objects.requireNonNull(id, "worm ID");
        Iterator<WormConfig> it = worms.iterator();
        while (it.hasNext()){
            WormConfig next = it.next();
            if (user.equals(next.user.get()) && id.equals(next.worm.get())){
                it.remove();
                return next;
            }
        }
        return null;
    }

    public MoleculeConfig removeMolecule(MoleculeConfig config){
        return removeMolecule(config.type, config.ID.get());
    }

    public MoleculeConfig removeMolecule(MoleculeType type, String id){
        Objects.requireNonNull(id);
        Iterator<MoleculeConfig> it = molecule.iterator();
        while (it.hasNext()){
            MoleculeConfig next = it.next();
            if (next.type == type && id.equals(next.ID.get())){
                it.remove();
                return next;
            }
        }
        return null;
    }
}
