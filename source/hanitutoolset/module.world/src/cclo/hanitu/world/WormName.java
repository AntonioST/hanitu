/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author antonio
 */
public class WormName{

    private static final Random RANDOM = new Random();
    private static final List<String> NAME_LIST = new ArrayList<>();

    static{
        Logger LOG = LoggerFactory.getLogger(WormName.class);
        String path = "META-INF/cclo.hanitu.world.WormName.list";
        InputStream is = ClassLoader.getSystemResourceAsStream(path);
        if (is != null){
            try (BufferedReader r = new BufferedReader(new InputStreamReader(is))) {
                String line;
                while ((line = r.readLine()) != null){
                    if (line.isEmpty() || line.startsWith("#")) continue;
                    NAME_LIST.add(line);
                }
            } catch (IOException e){
                LOG.warn("load name list fail", e);
            }
        } else {
            LOG.warn("not found : {}", path);
        }
    }

    private WormName(){
        throw new RuntimeException();
    }

    public static String genName(){
        return NAME_LIST.get(RANDOM.nextInt(NAME_LIST.size()));
    }

    @SuppressWarnings("unused")
    public static List<String> getAllDefaultName(){
        return Collections.unmodifiableList(NAME_LIST);
    }

    public static String genName(Collection<String> avoid){
        List<String> left = new ArrayList<>(NAME_LIST);
        left.removeAll(avoid);
        if (!left.isEmpty()){
            return left.get(RANDOM.nextInt(left.size()));
        }
        String ret;
        do {
            ret = Integer.toHexString(RANDOM.nextInt());
        } while (avoid.contains(ret));
        return ret;
    }

    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public static void main(String[] args){
        System.out.println(genName(Arrays.asList(args)));
    }
}
