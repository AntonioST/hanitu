/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author antonio
 */
public class ModuleManager<T, M extends Module<T>> implements InvocationHandler{

    private final Logger LOG = LoggerFactory.getLogger(ModuleManager.class);

    private final T owner;
    private final Map<String, M> module = new HashMap<>();
    public final M root;
    private final String name;

    public ModuleManager(T owner, Class<M> cls){
        this.owner = owner;
        name = cls.getSimpleName();
        root = (M)Proxy.newProxyInstance(ModuleManager.class.getClassLoader(), new Class[]{cls}, this);
        LOG.debug("{} manager created for {}", name, cls.getName());
    }

    @SuppressWarnings("unused")
    public Set<String> getLoaded(){
        return Collections.unmodifiableSet(module.keySet());
    }

    public M get(String name){
        return module.get(name);
    }

    public <R extends M> R add(R m){
        String name = m.getName();
        if (module.containsKey(name))
            throw new RuntimeException("module " + name + " has been loaded : " + module.get(name).getClass());
        LOG.debug("{} add {} : {}", this.name, name, m.getClass().getName());
        module.put(name, m);
        m.setup(owner);
        LOG.debug("{} setup {} done", this.name, name);
        return m;
    }

    @SuppressWarnings("unused")
    public void remove(String name){
        M m = module.remove(name);
        if (m != null){
            LOG.debug("{} remove {}", this.name, name);
            m.destroy();
        }
    }

    public void removeAll(){
        LOG.debug("{} remove all module", name);
        module.values().forEach(M::destroy);
        module.clear();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable{
        String methodName = method.getName();
        if (methodName.equals("toString")) return null;
        LOG.trace("{} call {}", name, methodName);
        Class<?> returnType = method.getReturnType();
        if (returnType == boolean.class){
            for (M m : module.values()){
                boolean ret = (Boolean)method.invoke(m, args);
                if (ret) return true;
            }
            return false;
        } else {
            for (M m : module.values()){
                CompletableFuture.runAsync(() -> {
                    //noinspection EmptyCatchBlock
                    try {
                        method.invoke(m, args);
                    } catch (IllegalAccessException e){
                    } catch (InvocationTargetException e){
                        LOG.warn("invoke : " + methodName, e.getCause());
                    }
                });
            }
            return null;
        }
    }
}
