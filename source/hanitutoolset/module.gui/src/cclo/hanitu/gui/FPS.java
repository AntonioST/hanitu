/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

/**
 * @author antonio
 */
public class FPS{

    //    private double FPS = 20;
    private int avgFPS;
    private int fpsCount = 0;
    private long fpsFrame = System.currentTimeMillis();

    public void update(){
        fpsCount++;
        long time = System.currentTimeMillis();
        if (time - fpsFrame > 1000 * 60) fpsFrame = time;
        if (time - fpsFrame > 1000){
            avgFPS = fpsCount;
            fpsFrame += 1000;
            fpsCount = 0;
        }
    }

//    public void sync(){
//        long current = System.currentTimeMillis();
//        long step = (long)(1000 / FPS);
//        do {
//            lastFrame += step;
//        } while (current > lastFrame);
//        current = lastFrame - current;
//        if (current > 0){
//            synchronized (this){
//                try {
//                    wait(current);
//                } catch (InterruptedException ex){
//                }
//            }
//        }
//    }

//    /**
//     * get current frame per second
//     *
//     * @return frame per second
//     */
//    public double getFPS(){
//        return FPS;
//    }

//    /**
//     * setting frame per second
//     *
//     * @param FPS frame per second
//     */
//    public void setFPS(double FPS){
//        if (FPS <= 0) throw new IllegalArgumentException("negative or zero value : " + FPS);
//        this.FPS = FPS;
//    }

    public int getAvgFPS(){
        return avgFPS;
    }
}
