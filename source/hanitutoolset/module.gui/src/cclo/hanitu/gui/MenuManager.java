/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;

import cclo.hanitu.Base;

import static cclo.hanitu.gui.MenuSession.createCheckBoxMenuItem;
import static cclo.hanitu.gui.MenuSession.createMenuItem;

/**
 * @author antonio
 */
public class MenuManager{

    private static final String MENU = ".menu";
    private static final String ACTION = ".action";
    private static final String SHORTCUT = ".shortcut";
    private static final String TYPE = ".type";
    private static final String INIT = ".init";

    private final MenuSession session;
    private final BiConsumer<ActionEvent, String> handle;
    private final Map<String, Properties> additionMenuID = new HashMap<>();
    private String suffix = ".menu";

    public MenuManager(BiConsumer<ActionEvent, String> handle){
        this(new MenuSession(), handle);
    }

    @SuppressWarnings("WeakerAccess")
    public MenuManager(MenuSession session, BiConsumer<ActionEvent, String> handle){
        this.session = Objects.requireNonNull(session);
        this.handle = Objects.requireNonNull(handle);
    }

    public MenuBar getMenuBar(){
        return session.getMenuBar();
    }

    public MenuSession getMenuSession(){
        return session;
    }

    @SuppressWarnings("unused")
    public String getSuffix(){
        return suffix;
    }

    @SuppressWarnings("unused")
    public void setSuffix(String suffix){
        this.suffix = suffix;
    }

    private String getSourceClassName(){
        // 0 currentThread()
        // 1 getSourceClassName()
        // 2 MenuManager.()
        // 3 ?.()
        StackTraceElement st = Thread.currentThread().getStackTrace()[3];
        return st.getClassName();
    }

    public void resetTopMenu(){
        String file = getSourceClassName() + suffix;
        Properties p = Base.loadProperties(file);
        if (p == null) throw new NullPointerException("menu property : " + file + " not found");
        resetTopMenu(p);
    }


    public void resetTopMenu(String suffix){
        String file = getSourceClassName() + "." + suffix + this.suffix;
        Properties p = Base.loadProperties(file);
        if (p == null) throw new NullPointerException("menu property : " + file + " not found");
        resetTopMenu(p);
    }

    private void resetTopMenu(Properties p){
        session.clear();
        String menuList = p.getProperty("menu");
        if (menuList == null) throw new IllegalArgumentException("lost property menu");
        for (String menuTitle : menuList.split(" +")){
            String prefix = "menu." + menuTitle;
            String name = p.getProperty(prefix);
            if (name == null) throw new IllegalArgumentException("lost menu name : " + prefix);
            session.addTopMenu(prefix, name);
            session.addMenuItems(prefix, -1, loadMenu(p, prefix, handle));
        }
    }

    @SuppressWarnings("WeakerAccess")
    public void addIntoTopMenu(Properties p, String menuID, BiConsumer<ActionEvent, String> action){
        session.addTopMenu(menuID, p.getProperty(menuID), -1);
        session.addMenuItems(menuID, -1, loadMenu(p, menuID, action));
    }

    @SuppressWarnings("WeakerAccess")
    public void removeFromTopMenu(Properties p, String menuID){
        Menu menu = session.getTopMenu(menuID);
        if (menu == null) return;
        List<String> list = getMenuNameList(p, menuID).stream()
          .filter(t -> !t.equals("|") && !t.matches("-+"))
          .map(t -> menuID + "." + t)
          .collect(Collectors.toList());
        session.removeMenuItems(menuID, list);
        if (menu.getItems().isEmpty()){
            session.removeMenu(menuID);
        }
    }

    public void addMenu(String menuID){
        addMenu(getSourceClassName(), menuID);
    }

    @SuppressWarnings("unused")
    public void addMenu(Class cls, String menuID){
        addMenu(cls.getName(), menuID);
    }

    @SuppressWarnings("WeakerAccess")
    public void addMenu(String properties, String menuID){
        Objects.requireNonNull(menuID);
        Properties p = Base.loadProperties(properties + suffix);
        additionMenuID.put(menuID, p);
        addIntoTopMenu(p, menuID, handle);
    }

    public void removeMenu(String menuID){
        Objects.requireNonNull(menuID);
        if (!additionMenuID.containsKey(menuID)) return;
        removeFromTopMenu(additionMenuID.get(menuID), menuID);
        additionMenuID.remove(menuID);
    }

    @SuppressWarnings("unused")
    public void removeAllMenu(){
        if (additionMenuID.isEmpty()) return;
        Properties p = Base.loadProperties(getClass().getName() + suffix);
        for (String menuID : additionMenuID.keySet()){
            removeFromTopMenu(p, menuID);
        }
        additionMenuID.clear();
    }

    public MenuItem getById(String id){
        return getMenuSession().getById(id);
    }

    private static List<String> getMenuNameList(Properties p, String prefix){
        String menu = p.getProperty(prefix + MENU);
        if (menu == null) throw new IllegalArgumentException("lost menu : " + prefix);
        if (menu.isEmpty()) return Collections.emptyList();
        return Arrays.asList(menu.split(" +"));
    }

    public static ContextMenu loadContextMenu(Properties p,
                                              String prefix,
                                              BiConsumer<ActionEvent, String> action){
        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(loadMenu(p, prefix, action));
        return menu;
    }

    public static List<MenuItem> loadMenu(Properties p,
                                          String prefix,
                                          BiConsumer<ActionEvent, String> action){
        List<MenuItem> list = new ArrayList<>();
        boolean separator = false;
        for (String title : getMenuNameList(p, prefix)){
            if (title.equals("|") || title.matches("-+")){
                if (separator){
                    list.add(new SeparatorMenuItem());
                    separator = false;
                }
            } else {
                String cache = prefix + "." + title;
                if (p.containsKey(cache + MENU)){
                    list.add(resolveMenu(p, cache, action));
                    separator = true;
                } else {
                    list.add(resolveMenuItem(p, cache, action));
                    separator = true;
                }
            }
        }
        return list;
    }


    private static Menu resolveMenu(Properties p,
                                    String prefix,
                                    BiConsumer<ActionEvent, String> action){
        String name = p.getProperty(prefix);
        if (name == null) throw new IllegalArgumentException("lost menu name : " + prefix);
        //
        Menu menu = new Menu(name);
        menu.setId(prefix);
        //
        ObservableList<MenuItem> items = menu.getItems();
        for (String menuTitle : getMenuNameList(p, prefix)){
            if (menuTitle.equals("|") || menuTitle.matches("-+")){
                items.add(new SeparatorMenuItem());
            } else if (p.containsKey(prefix + "." + menuTitle + MENU)){
                items.add(resolveMenu(p, prefix + "." + menuTitle, action));
            } else {
                items.add(resolveMenuItem(p, prefix + "." + menuTitle, action));
            }
        }
        return menu;
    }

    private static MenuItem resolveMenuItem(Properties p,
                                            String prefix,
                                            BiConsumer<ActionEvent, String> action){
        String name = p.getProperty(prefix);
        if (name == null) throw new IllegalArgumentException("lost menu name : " + prefix);
        String act;
        if (p.containsKey(prefix + ACTION)){
            act = p.getProperty(prefix + ACTION);
        } else {
            act = prefix;
        }
        MenuItem item;
        switch (p.getProperty(prefix + TYPE, "default")){
        case "check":
            item = createCheckBoxMenuItem(name,
                                          p.getProperty(prefix + SHORTCUT),
                                          Boolean.valueOf(p.getProperty(prefix + INIT, "false")),
                                          (event) -> action.accept(event, act));
            break;
        case "default":
        default:
            item = createMenuItem(name, p.getProperty(prefix + SHORTCUT), event -> action.accept(event, act));
        }
        item.setId(prefix);
        return item;
    }
}
