/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author antonio
 */
public class PaintLayer extends Canvas{

    private final Logger LOG = LoggerFactory.getLogger(PaintLayer.class);

    public final ColorProperty backgroundColor = new ColorProperty(this, "background", Color.WHITE);
    private final List<PaintArea> panels = new ArrayList<>();

    @SuppressWarnings("WeakerAccess")
    public PaintLayer(double w, double h){
        super(w, h);
    }

    public PaintLayer(IntegerProperty w, IntegerProperty h){
        super();
        widthProperty().bind(w);
        heightProperty().bind(h);
    }

    @SuppressWarnings("WeakerAccess")
    public void paint(){
        GraphicsContext g = getGraphicsContext2D();
        if (backgroundColor != null){
            g.setFill(backgroundColor.get());
            g.fillRect(0, 0, getWidth(), getHeight());
        }
        Affine origin = g.getTransform();
        for (PaintArea area : panels){
            if (area.visible){
                g.translate(area.x.get(), area.y.get());
                try {
                    area.paint(g);
                } catch (Exception e){
                    LOG.error(area.getClass().getName(), e);
                }
                g.setTransform(origin);
            }
        }
    }

    public void repaint(){
        Platform.runLater(this::paint);
    }

    public void addPanel(int layer, PaintArea viewSubPanel){
        panels.add(viewSubPanel);
        viewSubPanel.layer = layer;
        panels.sort(Comparator.comparingInt(p -> p.layer));
        viewSubPanel.setPanel(this);
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean removePanel(PaintArea o){
        if (panels.remove(o)){
//            Env.debug(() -> this + " ~*y{remove~} [" + o.layer + "] " + o);
            o.setPanel(null);
            return true;
        }
        return false;
    }

    @SuppressWarnings("unused")
    public void removeLayer(int layer){
        panels.removeIf(p -> p.layer == layer);
    }
}
