/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;


import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.WritableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import cclo.hanitu.data.*;
import cclo.hanitu.util.ErrorFunction;
import cclo.hanitu.util.Events.RunnableEvent;

/**
 * manage property setting panel.
 *
 * @author antonio
 */
public class PropertySession{

    private final GridPane layout = new GridPane();

    //    private final List<String> propertyNameList = new ArrayList<>();
    private final List<Control> components = new ArrayList<>();


    private static final Font LABEL_FONT = Font.font("FreeMono", FontWeight.BOLD, 16);
    private static final Font TEXT_FONT = Font.font("FreeMono", 16);

    private final SimpleObjectProperty<Font> labelFont = new SimpleObjectProperty<>(this, "font.label", LABEL_FONT);
    private final SimpleObjectProperty<Font> textFont = new SimpleObjectProperty<>(this, "font.text", TEXT_FONT);

    public final RunnableEvent modifyEvent = new RunnableEvent();
    private boolean editable = true;
    private boolean showReadOnlyProperties = true;

    private DataClass target;
    private DataClassMeta meta;
    private Properties property;

    public PropertySession(){
        layout.setHgap(5);
        layout.setVgap(5);
        ColumnConstraints c0 = new ColumnConstraints();
        ColumnConstraints c1 = new ColumnConstraints();
        ColumnConstraints c2 = new ColumnConstraints();
//        c0.setMinWidth(100);
//        c0.setPrefWidth(200);
        c1.setMinWidth(200);
        c1.setHgrow(Priority.SOMETIMES);
        c2.setMinWidth(50);
        layout.getColumnConstraints().addAll(c0, c1, c2);
    }

    public GridPane getLayout(){
        return layout;
    }

    public Control getControl(int index){
        return components.get(index);
    }

    public void setEditable(boolean edit){
        editable = edit;
        components.forEach(c -> {
            if (c instanceof TextInputControl){
                TextInputControl text = (TextInputControl)c;
                if (!text.isDisable()){
                    text.setEditable(edit);
                }
            } else if (c instanceof ComboBox){
                c.setDisable(!edit);
            } else {
                c.setDisable(!edit);
            }
        });
    }

    @SuppressWarnings("unused")
    public boolean isShowReadOnlyProperties(){
        return showReadOnlyProperties;
    }

    public void setShowReadOnlyProperties(boolean showReadOnlyProperties){
        this.showReadOnlyProperties = showReadOnlyProperties;
    }

    @SuppressWarnings("WeakerAccess")
    protected void clear(){
        target = null;
        meta = null;
        property = null;
        layout.getChildren().clear();
//        propertyNameList.clear();
        components.clear();
    }

    public void reset(DataClass data){
        clear();
        target = data;
        meta = DataClassMeta.getMeta(target);
        property = data.getProperty();
        if (meta.getPropertyNameList().isEmpty()) return;
        for (int group = 0, groupSize = meta.getGroupSize(); group < groupSize; group++){
            String groupName = meta.getGroupName(group);
            String tipProp = groupName + ".tip";
            String unitProp = groupName + ".unit";
            //
            Label nameLabel = new Label(property.getProperty(groupName));
            nameLabel.fontProperty().bind(labelFont);
            if (property.containsKey(tipProp)){
                nameLabel.setTooltip(new Tooltip(property.getProperty(tipProp)));
            }
            //
            Label unitLabel = null;
            if (property.containsKey(unitProp)){
                unitLabel = new Label(property.getProperty(unitProp));
                unitLabel.fontProperty().bind(labelFont);
            }
            //
            HBox hbox = new HBox(5);
            hbox.setAlignment(Pos.BASELINE_LEFT);
            //
            boolean readOnlyProperties;
            List<String> propList = meta.getGroupList(group);
            if (propList.size() == 1){
                Control control = addControl(propList.get(0));
                hbox.getChildren().add(control);
                HBox.setHgrow(control, Priority.ALWAYS);
                //
                readOnlyProperties = meta.isPropertyReadOnly(propList.get(0));
            } else {
                ObservableList<Node> children = hbox.getChildren();
                readOnlyProperties = true;
                for (String prop : propList){
                    String simpleName = groupName + "." + prop;
                    if (property.containsKey(simpleName)){
                        Label simpleLabel = new Label(property.getProperty(simpleName));
                        children.add(simpleLabel);
                        //
                        String tip = prop + ".tip";
                        if (property.containsKey(tip)){
                            simpleLabel.setTooltip(new Tooltip(property.getProperty(tip)));
                        }
                    }
                    //
                    Control control = addControl(prop);
                    control.setPrefWidth(50);
                    children.add(control);
                    HBox.setHgrow(control, Priority.SOMETIMES);
                    //
                    readOnlyProperties = readOnlyProperties && meta.isPropertyReadOnly(prop);
                }
            }
            //
            if (showReadOnlyProperties || !readOnlyProperties){
                layout.add(nameLabel, 0, group);
                layout.add(hbox, 1, group);
                if (unitLabel != null){
                    layout.add(unitLabel, 2, group);
                    GridPane.setHalignment(unitLabel, HPos.LEFT);
                }
            }
            GridPane.setFillWidth(hbox, true);
        }
        layout.requestLayout();
    }

    private void modifyEvent(){
        modifyEvent.run();
    }

    private Control addControl(String name){
        ReadOnlyProperty property = meta.getProperty(target, name);
        Control control;
        if (property instanceof StringProperty){
            control = createTextField((StringProperty)property);
        } else if (property instanceof ColorProperty){
            control = createColorPicker((ColorProperty)property);
        } else if (property instanceof FontProperty){
            control = createFontChooser((FontProperty)property);
        } else if (property instanceof SwitchProperty){
            control = createSwitchButton((SwitchProperty)property);
        } else if (property instanceof EnumProperty){
            control = createComboBox((EnumProperty)property);
        } else if (property instanceof EventProperty){
            control = createActionButton((EventProperty)property);
        } else if (property instanceof IntegerProperty){
            control = createIntTextField((IntegerProperty)property);
        } else if (property instanceof DoubleProperty){
            control = createFloatTextField((DoubleProperty)property);
        } else if (property instanceof BooleanProperty){
            control = createButton((BooleanProperty)property);
        } else {
            throw new IllegalArgumentException("unknown property type : " + property.getClass());
        }
//        propertyNameList.add(name);
        components.add(control);
        return control;
    }

    private Button createSwitchButton(SwitchProperty property){
        Button button = new Button(property.getSwitchName());
        button.setMinWidth(100);
        button.fontProperty().bind(textFont);
        property.addListener((s, i, j) -> button.setText(property.getSwitchName()));
        button.setOnAction(e -> {
            if (editable){
                property.set(!property.get());
                modifyEvent();
            }
        });
        return button;
    }

    private Button createButton(BooleanProperty property){
        Button button = new Button(Boolean.toString(property.get()));
        button.setMinWidth(100);
        button.fontProperty().bind(textFont);
        property.addListener((s, i, j) -> button.setText(j.toString()));
        button.setOnAction(e -> {
            if (editable){
                property.set(!property.get());
                modifyEvent();
            }
        });
        return button;
    }

    private Button createActionButton(EventProperty property){
        String textProp = property.getName() + ".text";
        Button button = new Button();
        if (this.property.containsKey(textProp)){
            button.setText(this.property.getProperty(textProp));
        }
        button.setMinWidth(100);
        button.fontProperty().bind(textFont);
        button.setOnAction(e -> {
            if (editable){
                property.fire();
                modifyEvent();
            }
        });
        return button;
    }

    private ComboBox<String> createComboBox(EnumProperty property){
        ComboBox<String> box = new ComboBox<>();
        box.getItems().addAll(property.getEnum());
        box.getSelectionModel().select(property.get());
        box.setEditable(false);
        box.setMinWidth(100);
        textFont.addListener((s, i, j) -> box.setStyle("-fx-font: " + j.getSize() + "px \"" + j.getName() + "\""));
        property.addListener((s, i, j) -> box.getSelectionModel().select(property.get()));
        box.setOnAction(e -> {
            if (editable){
                property.set(box.getSelectionModel().getSelectedIndex());
                modifyEvent();
            }
        });
        return box;
    }

    private TextField createTextField(StringProperty property){
        TextField text = new TextField(property.get());
        if (meta.isPropertyReadOnly(property.getName())){
            text.setDisable(true);
        } else {
            property.addListener((s, i, j) -> text.setText(j));
            setupFocusEvent(text);
            text.fontProperty().bind(textFont);
            text.setOnAction(e -> {
                if (editable){
                    property.set(text.getText());
                    modifyEvent();
                }
            });
        }
        return text;
    }

    private TextField createIntTextField(IntegerProperty property){
        TextField text = new TextField(Integer.toString(property.get()));
        if (meta.isPropertyReadOnly(property.getName())){
            text.setDisable(true);
        } else {
            property.addListener((s, i, j) -> text.setText(j.toString()));
            setupFocusEvent(text);
            text.fontProperty().bind(textFont);
            setTextFieldGuider(text, Integer::parseInt, "wrong number format");
            text.setOnAction(e -> {
                try {
                    if (editable){
                        property.set(Integer.parseInt(text.getText()));
                        modifyEvent();
                    }
                } catch (NumberFormatException ex){
                    MessageSession.showErrorMessageDialog("wrong number format", ex);
                }
            });
        }
        return text;
    }

    private TextField createFloatTextField(DoubleProperty property){
        TextField text = new TextField(Double.toString(property.get()));
        if (meta.isPropertyReadOnly(property.getName())){
            text.setDisable(true);
        } else {
            property.addListener((s, i, j) -> text.setText(j.toString()));
            setupFocusEvent(text);
            text.fontProperty().bind(textFont);
            setTextFieldGuider(text, Double::parseDouble, "wrong number format");
            text.setOnAction(e -> {
                try {
                    if (editable){
                        property.set(Double.parseDouble(text.getText()));
                        modifyEvent();
                    }
                } catch (NumberFormatException ex){
                    MessageSession.showErrorMessageDialog("wrong number format", ex);
                }
            });
        }
        return text;
    }

    private ColorPicker createColorPicker(ColorProperty property){
        ColorPicker button = new ColorPicker(property.get());
        property.addListener((s, i, j) -> button.setValue(j));
        button.setOnAction(e -> {
            if (editable){
                property.set(button.getValue());
                modifyEvent();
            }
        });
        return button;
    }

    private Button createFontChooser(FontProperty property){
        Font font = property.get();
        Button button = new Button(font.getName());
        button.setFont(font);
        property.addListener((s, i, j) -> {
            button.setText(j.getName());
            button.setFont(j);
        });
        button.setOnAction(e -> {
            if (editable){
                FontChooser chooser = new FontChooser();
                chooser.setSelectedFont(button.getFont());
                int ret = chooser.showDialog();
                if (ret == MessageSession.YES_OPTION){
                    property.set(chooser.getSelectedFont());
                    modifyEvent();
                }
            }
        });
        return button;
    }

    public static void setBackground(Control target, Color color){
        target.setStyle("-fx-control-inner-background: #" + color.toString().substring(2));
    }

    @SuppressWarnings("unused")
    public static void setBackground(Node target, Color color){
        target.setStyle("-fx-background: #" + color.toString().substring(2));
    }

    private void setupFocusEvent(final TextField text){
        text.focusedProperty().addListener(new ChangeListener<Boolean>(){
            String contentCache;

            @Override
            public void changed(ObservableValue<? extends Boolean> s, Boolean i, Boolean j){
                if (j){ //focusGained
                    contentCache = text.getText();
                } else {
                    String newContent = text.getText();
                    if (contentCache == null || !contentCache.equals(newContent)){
                        Event.fireEvent(text, new ActionEvent(text, text));
                    }
                }

            }
        });
    }

    public static void setTextFieldGuider(TextField text,
                                          ErrorFunction.EConsumer<String, Exception> act,
                                          String warning){
        text.textProperty().addListener((s, i, j) -> {
            try {
                act.accept(j);
                setBackground(text, Color.WHITE);
                text.setTooltip(null);
            } catch (Throwable e){
                setBackground(text, Color.PINK);
                text.setTooltip(new Tooltip(warning));
            }
        });
    }

    public static <T> void setTextFieldGuideSetter(TextField text,
                                                   ErrorFunction.EFunction<String, T, Exception> act,
                                                   String warning,
                                                   WritableValue<T> properties){
        text.setOnAction(e -> {
            try {
                T value = act.accept(text.getText());
                properties.setValue(value);
                setBackground(text, Color.WHITE);
                text.setTooltip(null);
            } catch (Throwable ex){
                setBackground(text, Color.PINK);
                text.setTooltip(new Tooltip(warning));
                MessageSession.showErrorMessageDialog(warning, ex);
            }
        });
    }
}
