/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.util.ErrorFunction;

/**
 * @author antonio
 */
public class FXApplication{

    private static final Logger LOG = LoggerFactory.getLogger(FXApplication.class);

    private static PrimaryFxApplication PRIMARY_APPLICATION;

    public final Stage primaryStage;
    protected final Scene scene;
    protected final VBox root;
    private boolean close;

    protected FXApplication(){
        this(new Stage());
    }

    protected FXApplication(StageStyle style){
        this(new Stage(style));
    }

    protected FXApplication(Stage stage){
        primaryStage = Objects.requireNonNull(stage);
        scene = new Scene(root = new VBox()/*, primaryStage.getWidth(), primaryStage.getHeight()*/);
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(e -> close());
        scene.setOnKeyPressed(e -> {
            if (processKeyboardEvent(e)) e.consume();
        });
        root.setFillWidth(true);
    }

    @SuppressWarnings("WeakerAccess")
    public void close(){
        LOG.debug("quit : {}", primaryStage.getTitle());
        if (inFxThread()){
            primaryStage.close();
            close = true;
        } else {
            Platform.runLater(() -> {
                primaryStage.close();
                close = true;
            });
        }
    }

    public boolean isClose(){
        return close;
    }

    protected static boolean inFxThread(){
        return Thread.currentThread().getName().equals("JavaFX Application Thread");
    }

    @SuppressWarnings("WeakerAccess")
    public void setTitle(String title){
        primaryStage.setTitle(title);
    }

    @SuppressWarnings("unused")
    protected void processMenuEvent(ActionEvent e, String action){
    }

    protected boolean processKeyboardEvent(KeyEvent e){
        return false;
    }

    // cannot be private
    public static class PrimaryFxApplication extends Application{

        @Override
        public void start(Stage primaryStage) throws Exception{
            PRIMARY_APPLICATION = this;
            LOG.debug("FX init done");
            synchronized (FXApplication.class){
                FXApplication.class.notifyAll();
            }
        }
    }

    private static void checkFxInit(){
        if (PRIMARY_APPLICATION == null){
            synchronized (FXApplication.class){
                if (PRIMARY_APPLICATION == null){
                    LOG.debug("Fx init ");
                    Thread th = new Thread(() -> Application.launch(PrimaryFxApplication.class), "primary FX");
                    th.start();
                    while (PRIMARY_APPLICATION == null){
                        //noinspection EmptyCatchBlock
                        try {
                            FXApplication.class.wait(10);
                        } catch (InterruptedException e){
                        }
                    }
                }
            }
        }
    }

    public static <T extends FXApplication> T launch(Class<T> application){
        checkFxInit();
        if (inFxThread()){
            LOG.debug("FX launch class : {}", application);
            try {
                T instance = application.newInstance();
                LOG.debug("FX launch : {}", instance);
                return instance;
            } catch (InstantiationException | IllegalAccessException e){
                LOG.warn("FX launch fail", e);
                throw new RuntimeException(e);
            }
        } else {
            CompletableFuture<T> promise = new CompletableFuture<>();
            Platform.runLater(() -> {
                LOG.debug("FX launch class : {}", application);
                try {
                    T instance = application.newInstance();
                    LOG.debug("FX launch : {}", instance);
                    promise.complete(instance);
                } catch (InstantiationException | IllegalAccessException e){
                    LOG.warn("FX launch fail", e);
                    promise.completeExceptionally(e);
                }
            });
            return promise.join();
        }
    }

    @SuppressWarnings("unused")
    public static <T extends FXApplication, E extends Exception>
    void launchAndWait(Class<T> application, ErrorFunction.EConsumer<T, E> init) throws E{
        checkFxInit();
        if (inFxThread()){
            LOG.debug("FX launch class : {}", application);
            try {
                T instance = application.newInstance();
                LOG.debug("FX launch : {}", instance);
                Stage stage = instance.primaryStage;
                stage.showAndWait();
            } catch (InstantiationException | IllegalAccessException e){
                LOG.warn("FX launch fail", e);
                throw new RuntimeException(e);
            }
        } else {
            CompletableFuture<T> promise = new CompletableFuture<>();
            CompletableFuture<T> escape = new CompletableFuture<>();
            Platform.runLater(() -> {
                LOG.debug("FX launch class : {}", application);
                try {
                    T instance = application.newInstance();
                    LOG.debug("FX launch : {}", instance);
                    Stage stage = instance.primaryStage;
                    EventHandler<WindowEvent> close = stage.getOnCloseRequest();
                    stage.setOnCloseRequest(event -> {
                        close.handle(event);
                        escape.complete(null);
                    });
                    promise.complete(instance);
                } catch (InstantiationException | IllegalAccessException e){
                    LOG.warn("FX launch fail", e);
                    promise.completeExceptionally(e);
                    escape.completeExceptionally(e);
                }
            });
            if (init != null) init.accept(promise.join());
            escape.join();
        }
    }

    public static void doInFxThreadAndWait(Runnable doIt){
        if (inFxThread()){
            doIt.run();
        } else {
            CompletableFuture promise = new CompletableFuture();
            Platform.runLater(() -> {
                try {
                    doIt.run();
                } finally {
                    promise.complete(null);
                }
            });
            promise.join();
        }
    }
}
