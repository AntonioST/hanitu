/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.util.OptionalInt;

import javafx.scene.input.KeyCode;

/**
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class KeyboardBufferInt{

    private int buffer = -1;
    private long waitingTime = -1;
    private long lastInputTime;

    public KeyboardBufferInt(){
    }

    @SuppressWarnings("unused")
    public long getWaitingTime(){
        return waitingTime;
    }

    public void setWaitingTime(long waitingTime){
        this.waitingTime = waitingTime;
    }

    @SuppressWarnings("UnusedReturnValue")
    public int add(int v){
        if (v > 10){
            v = v % 10;
        }
        if (v >= 0){
            if (buffer < 0){
                buffer = v;
            } else {
                buffer = buffer * 10 + v;
            }
        }
        return buffer;
    }

    public boolean isValid(){
        return buffer >= 0;
    }

    public OptionalInt get(){
        if (buffer < 0) return OptionalInt.empty();
        return OptionalInt.of(buffer);
    }

    @SuppressWarnings("UnusedReturnValue")
    public OptionalInt back(){
        if (buffer == 0){
            buffer = -1;
        } else {
            buffer /= 10;
        }
        return get();
    }

    public OptionalInt clear(){
        OptionalInt ret = get();
        buffer = -1;
        return ret;
    }

    public boolean processKeyboardEvent(KeyCode code){
        long time = System.currentTimeMillis();
        if (waitingTime > 0 && (time - lastInputTime) > waitingTime){
            lastInputTime = time;
            clear();
        }
        switch (code){
        case DIGIT0:
        case NUMPAD0:
            add(0);
            break;
        case DIGIT1:
        case NUMPAD1:
            add(1);
            break;
        case DIGIT2:
        case NUMPAD2:
            add(2);
            break;
        case DIGIT3:
        case NUMPAD3:
            add(3);
            break;
        case DIGIT4:
        case NUMPAD4:
            add(4);
            break;
        case DIGIT5:
        case NUMPAD5:
            add(5);
            break;
        case DIGIT6:
        case NUMPAD6:
            add(6);
            break;
        case DIGIT7:
        case NUMPAD7:
            add(7);
            break;
        case DIGIT8:
        case NUMPAD8:
            add(8);
            break;
        case DIGIT9:
        case NUMPAD9:
            add(9);
            break;
        case BACK_SPACE:
            back();
            break;
        default:
            return false;
        }
        return true;
    }
}
