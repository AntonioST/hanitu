/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.input.KeyCombination;

/**
 * handle JavaFX Menu, configuration with properties file.
 *
 * Top Level Properties
 * --------------------
 *
 *     menu=[ID]...
 *
 * Menu Properties
 * ---------------
 *
 * ### Menu name
 *
 *     menu.[ID]=[Name]
 *
 * for deep level menu, (use level 2 as example):
 *
 *     menu.[IDv1].[IDv2]=[Name]
 *
 * ### Sub menu
 *
 *     menu.[IDv1].menu=[IDv2]...
 *
 * can use '|' as a menu separator, like
 *
 *     menu.[IDv1].menu=[IDv2.1] | [IDv2.2]
 *
 * ### Menu type (optional)
 *
 *     menu.[ID].type=default|check
 *
 * * default
 *
 *     MenuItem
 *
 * * check
 *
 *     CheckMenuItem
 *
 * ### Menu shortcut (optional)
 *
 *     menu.[ID].shortcut=[Key]
 *
 * use the JavaFX rule, see
 * [KeyCombination](https://docs.oracle.com/javase/8/javafx/api/javafx/scene/input/KeyCombination.html)
 * for more detail
 *
 * ### Menu action identify (optional)
 *
 *     menu.[ID].action=[Identify]
 *
 * default use the property name as action identify.
 *
 * @author antonio
 */
public class MenuSession{

    private final MenuBar menuBar;

    public MenuSession(){
        menuBar = new MenuBar();
        menuBar.setUseSystemMenuBar(true);
    }

    public MenuSession(MenuBar menuBar){
        this.menuBar = menuBar;
    }

    public MenuBar getMenuBar(){
        return menuBar;
    }

    @SuppressWarnings("unused")
    public int getTopMenuCount(){
        return menuBar.getMenus().size();
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean addTopMenu(String menuID, String menuName){
        if (getTopMenu(menuID) != null) return false;
        Menu menu = createMenu(menuName);
        menu.setId(menuID);
        menuBar.getMenus().add(menu);
        return true;
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean addTopMenu(String menuID, String menuName, int index){
        if (getTopMenu(menuID) != null) return false;
        Menu menu = createMenu(menuName);
        menu.setId(menuID);
        ObservableList<Menu> menus = menuBar.getMenus();
        if (menus.isEmpty()){
            menus.add(menu);
        } else if (index < 0){
            // last always Help
            menus.add(menus.size() + index, menu);
        } else {
            menus.add(index, menu);
        }
        return true;
    }

    @SuppressWarnings("unused")
    public void addMenuItems(String menuID, int insertGroupIndex, MenuItem... items){
        addMenuItems(menuID, insertGroupIndex, Arrays.asList(items));
    }

    /**
     * @param menuID        menu title
     * @param insertGroupIndex the index of the menu items block which spread by separator, can negative.
     * @param list             the list of the menu items.
     */
    public void addMenuItems(String menuID, int insertGroupIndex, List<MenuItem> list){
        Menu menu = getTopMenu(menuID);
        if (menu == null) throw new RuntimeException("top menu not found : " + menuID);
        int index = getRealMenuInsertIndex(menu, insertGroupIndex);
        ObservableList<MenuItem> items = menu.getItems();
        if (!items.isEmpty()){
            items.add(index, new SeparatorMenuItem());
        }
        if (index != 0){
            index++;
        }
        for (int i = 0, size = list.size(); i < size; i++){
            MenuItem item = list.get(i);
            if (item == null){
                items.add(index + i, new SeparatorMenuItem());
            } else {
                items.add(index + i, item);
            }
        }
    }

    public Menu getTopMenu(String menuID){
        Objects.requireNonNull(menuID, "menu ID");
        for (Menu m : menuBar.getMenus()){
            if (menuID.equals(m.getId())){
                return m;
            }
        }
        return null;
    }

    private int getRealMenuInsertIndex(Menu menu, int insertGroupIndex){
        if (insertGroupIndex == 0) return 0;
        ObservableList<MenuItem> items = menu.getItems();
        int size = items.size();
        if (insertGroupIndex > 0){
            int ret = 0;
            for (int i = 0; i < size; i++){
                if ((items.get(i) instanceof SeparatorMenuItem) && ++ret == insertGroupIndex) return i - 1;
            }
        } else if (insertGroupIndex < -1){
            int ret = -1;
            for (int i = size - 1; i >= 0; i--){
                if ((items.get(i) instanceof SeparatorMenuItem) && --ret == insertGroupIndex) return i - 1;
            }
            return 0;
        }
        // insertGroupIndex == -1
        return size;
    }

    @SuppressWarnings("unused")
    public void removeMenuItems(String menuID, String... removeMenuID){
        removeMenuItems(menuID, Arrays.asList(removeMenuID));
    }

    public void removeMenuItems(String menuID, Collection<String> removeMenuID){
        Menu menu = getTopMenu(menuID);
        if (menu == null){
            return;
        }
        ObservableList<MenuItem> items = menu.getItems();
        items.removeIf(t -> !(t instanceof SeparatorMenuItem) && removeMenuID.contains(t.getId()));
        for (int i = items.size() - 1; i > 0; i--){
            if (items.get(i) instanceof SeparatorMenuItem){
                if (items.get(i - 1) instanceof SeparatorMenuItem){
                    items.remove(i);
                }
            }
        }
        while (!items.isEmpty() && (items.get(0) instanceof SeparatorMenuItem)){
            items.remove(0);
        }
    }

    public void removeMenu(String menuID){
        Objects.requireNonNull(menuID, "menu ID");
        menuBar.getMenus().removeIf(menu -> menuID.equals(menu.getId()));
    }

    public void clear(){
        menuBar.getMenus().clear();
    }

    public MenuItem getById(String id){
        Objects.requireNonNull(id, "menu ID");
        for (Menu menu : menuBar.getMenus()){
            if (id.equals(menu.getId())) return menu;
        }
        for (Menu menu : menuBar.getMenus()){
            MenuItem item = getMenuItemById(menu, id);
            if (item != null) return item;
        }
        return null;
    }

    public static MenuItem createMenuItem(String text, EventHandler<ActionEvent> action){
        return createMenuItem(text, (KeyCombination)null, action);
    }

    public static MenuItem createMenuItem(String text, String shortcut, EventHandler<ActionEvent> action){
        KeyCombination combination = shortcut == null || shortcut.isEmpty()?
                                     null:
                                     KeyCombination.keyCombination(shortcut);
        return createMenuItem(text, combination, action);
    }

    @SuppressWarnings("WeakerAccess")
    public static MenuItem createMenuItem(String text, KeyCombination shortcut, EventHandler<ActionEvent> action){
        MenuItem ret = new MenuItem(text);
        if (shortcut != null){
            ret.setAccelerator(shortcut);
        }
        if (action != null){
            ret.setOnAction(action);
        }
        return ret;
    }

    @SuppressWarnings("unused")
    public static CheckMenuItem createCheckBoxMenuItem(String text,
                                                       boolean initValue,
                                                       Consumer<ActionEvent> action){
        return createCheckBoxMenuItem(text, (KeyCombination)null, initValue, action);
    }

    public static CheckMenuItem createCheckBoxMenuItem(String text,
                                                       String shortcut,
                                                       boolean initValue,
                                                       Consumer<ActionEvent> action){
        KeyCombination combination = shortcut == null || shortcut.isEmpty()?
                                     null:
                                     KeyCombination.keyCombination(shortcut);
        return createCheckBoxMenuItem(text,
                                      combination,
                                      initValue,
                                      action);
    }

    @SuppressWarnings("WeakerAccess")
    public static CheckMenuItem createCheckBoxMenuItem(String text,
                                                       KeyCombination shortcut,
                                                       boolean initValue,
                                                       Consumer<ActionEvent> action){
        Objects.requireNonNull(action, "event listener");
        CheckMenuItem item = new CheckMenuItem(text);
        if (shortcut != null){
            item.setAccelerator(shortcut);
        }
        item.setOnAction(action::accept);
        item.setSelected(initValue);
        return item;
    }

    private static Menu createMenu(String text){
        Menu ret = new Menu(Objects.requireNonNull(text, "menu text"));
        ret.setMnemonicParsing(true);
        return ret;
    }

    public static void processReflectionMenuEvent(Object host, ActionEvent event, String action){
        String clazz;
        String args;
        if (action.contains("=")){
            int idx = action.indexOf("=");
            clazz = action.substring(idx);
            args = action.substring(idx + 1);
        } else {
            clazz = action;
            args = null;
        }
        try {
            Class<?> cls = Class.forName(clazz);
            BiConsumer<ActionEvent, String> caller;
            try {
                Constructor<?> constructor = cls.getConstructor(host.getClass());
                caller = (BiConsumer<ActionEvent, String>)constructor.newInstance(host);
            } catch (NoSuchMethodException e){
                caller = (BiConsumer<ActionEvent, String>)cls.newInstance();
            }
            caller.accept(event, args);
        } catch (InstantiationException
          | IllegalAccessException
          | ClassNotFoundException
          | ClassCastException
          | InvocationTargetException ex){
            throw new RuntimeException(ex);
        }
    }

    public static MenuItem getMenuItemById(ObservableList<MenuItem> items, String id){
        Objects.requireNonNull(id, "menu item ID");
        for (MenuItem item : items){
            if (id.equals(item.getId())) return item;
            if (item instanceof Menu){
                MenuItem ret = getMenuItemById((Menu)item, id);
                if (ret != null) return ret;
            }
        }
        return null;
    }

    @SuppressWarnings("WeakerAccess")
    public static MenuItem getMenuItemById(Menu menu, String id){
        Objects.requireNonNull(id, "menu ID");
        for (MenuItem item : menu.getItems()){
            if (id.equals(item.getId())) return item;
        }
        for (MenuItem item : menu.getItems()){
            if (item instanceof Menu){
                MenuItem ret = getMenuItemById(((Menu)item), id);
                if (ret != null) return ret;
            }
        }
        return null;
    }


    public static boolean removeMenuItemById(ObservableList<MenuItem> items, String id){
        Objects.requireNonNull(id, "menu item ID");
        if (items.removeIf(item -> id.equals(item.getId()))) return true;
        for (MenuItem item : items){
            if (item instanceof Menu){
                if (removeMenuItemById(((Menu)item).getItems(), id)) return true;
            }
        }
        return false;
    }
}
