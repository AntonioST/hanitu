/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javafx.event.ActionEvent;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

import cclo.hanitu.Base;
import cclo.hanitu.util.ErrorFunction.EConsumer;


/**
 * @author antonio
 */
@SuppressWarnings("unused")
public abstract class FileSession{

    private final Path defaultWorkDirectory = Paths.get(System.getProperty("user.dir")).toAbsolutePath();
    private Path currentWorkDirectory = defaultWorkDirectory;

    private final Stage stage;
    private String defaultSaveFileName;
    private String defaultFileExtName;
    private final List<ExtensionFilter> fileFilters = new ArrayList<>();
    private boolean editable = true;
    private boolean currentFileModified;
    private ExtensionFilter selectedExt;
    /**
     * current file saving path.
     *
     * **Notice** : make sure assign it with absolute path
     */
    private Path currentFilePath;

    public FileSession(Stage stage){
        this.stage = stage;
        stage.setOnCloseRequest(e -> {
            if (!quit()){
                e.consume();
            }
        });
    }

    protected abstract void onNewFile();

    protected abstract void onOpenFile(Path openFilePath) throws IOException;

    protected abstract void onSaveFile(Path saveFilePath) throws IOException;

    protected abstract void onUpdate();

    protected abstract void onQuit();

    public boolean isCurrentFileModified(){
        return currentFileModified;
    }

    public void setCurrentFileModified(boolean isCurrentFileModified){
        if (editable){
            currentFileModified = isCurrentFileModified;
            onUpdate();
        }
    }

    public boolean isEditable(){
        return editable;
    }

    public void setEditable(boolean editable){
        this.editable = editable;
    }

    /**
     * current working directory
     *
     * @return directory path, null if current file not associate with any file.
     */
    public Path getCurrentWorkDirectory(){
        return currentWorkDirectory != null? currentWorkDirectory: defaultWorkDirectory;
    }

    /**
     * get the file saving path relative to current working directory.
     *
     * @return file saving path, may null
     */
    public Path getCurrentFilePath(){
        return currentFilePath;
    }

    /**
     * set the file saving path.
     *
     * @param currentFilePath path, null for dis-associating file and reset to default.
     */
    public void setCurrentFilePath(Path currentFilePath){
        if (currentFilePath != null){
            this.currentFilePath = currentFilePath.toAbsolutePath().normalize();
            currentWorkDirectory = currentFilePath.getParent();
        } else {
            this.currentFilePath = null;
            currentWorkDirectory = defaultWorkDirectory;
        }
        onUpdate();
    }

    public String getDefaultSaveFileName(){
        return defaultSaveFileName;
    }

    public void setDefaultSaveFileName(String defaultSaveFileName){
        this.defaultSaveFileName = defaultSaveFileName;
        if (defaultFileExtName == null && defaultSaveFileName.contains(".")){
            defaultFileExtName = defaultSaveFileName.substring(defaultSaveFileName.lastIndexOf(".") + 1);
        }
    }

    public String getDefaultFileExtName(){
        return defaultFileExtName;
    }

    public void setDefaultFileExtName(String defaultFileExtName){
        this.defaultFileExtName = defaultFileExtName;
    }

    public void setFileFilter(ExtensionFilter... ff){
        fileFilters.clear();
        if (ff.length > 0){
            fileFilters.addAll(Arrays.asList(ff));
        }
    }

    public ExtensionFilter getSelectedExtension(){
        return selectedExt;
    }

    /**
     * new a empty, clear session.
     *
     * If current file is modify, will ask user save it or not?
     */
    @SuppressWarnings("WeakerAccess")
    public void newEmptyFile(){
        if (!editable) return;
        if (!askSaveCurrentFile()) return;
        onNewFile();
        currentFilePath = null;
        currentWorkDirectory = defaultWorkDirectory;
        currentFileModified = false;
        onUpdate();
    }

    /**
     * load file and start a new session
     *
     * If current file is modify, will ask user save it or not?
     */
    @SuppressWarnings("WeakerAccess")
    public void openFile(){
        if (!editable) return;
        if (!askSaveCurrentFile()) return;
        Path path = showOpenFileDialog();
        if (path == null) return;
        currentFilePath = path;
        currentWorkDirectory = currentFilePath.getParent();
        try {
            onOpenFile(currentFilePath);
            currentFileModified = false;
        } catch (IOException | RuntimeException ex){
            Properties p = Base.loadProperties("gui");
            String title = String.format(p.getProperty("dialog.loaderror.title"), path.toString());
            MessageSession.showErrorMessageDialog(title, ex);
        }
        onUpdate();
    }

    public Path showOpenFileDialog(){
        FileChooser jfc = new FileChooser();
        jfc.setInitialDirectory(getCurrentWorkDirectory().toFile());
        if (!fileFilters.isEmpty()){
            jfc.getExtensionFilters().addAll(fileFilters);
        }
        File file = jfc.showOpenDialog(stage);
        if (file == null) return null;
        return file.getAbsoluteFile().toPath();
    }

    @SuppressWarnings("WeakerAccess")
    public boolean saveFile(){
        if (!editable) return false;
        if (currentFilePath == null){
            setCurrentFileModified(true);
            if (!askWhereToSaveFile()) return false;
        }
        boolean result = savingFile();
        onUpdate();
        return result;
    }

    @SuppressWarnings({"WeakerAccess", "UnusedReturnValue"})
    public boolean saveFileAs(){
        setCurrentFileModified(true);
        if (askWhereToSaveFile()){
            boolean result = savingFile();
            onUpdate();
            return result;
        } else {
            return false;
        }
    }

    /**
     * ask user save modify to file?
     *
     * The panel has four choose, there are yes, no, cancel and close (the dialog). Save file only when user choose yes.
     * Do Nothing when choosing others.
     *
     * @return true if this action doesn't be interrupted.
     */
    @SuppressWarnings("WeakerAccess")
    public boolean askSaveCurrentFile(){
        if (!currentFileModified) return true;
        Properties p = Base.loadProperties("gui");
        int choose = MessageSession.showConfirmDialog(p.getProperty("dialog.savewhenexit.title"),
                                                      p.getProperty("dialog.savewhenexit.message"),
                                                      MessageSession.YES_NO_CANCEL_OPTIONS);
        if (choose == MessageSession.YES_OPTION){
            return saveFile();
        }
        return choose == MessageSession.NO_OPTION;
    }

    /**
     * ask user where to save the file. and set the result to the currentFilePath
     *
     * @return Is User cancel this action?
     */
    private boolean askWhereToSaveFile(){
        FileChooser jfc = new FileChooser();
        if (currentWorkDirectory == null){
            jfc.setInitialDirectory(defaultWorkDirectory.toFile());
        } else {
            jfc.setInitialDirectory(currentWorkDirectory.toFile());
            if (currentFilePath != null){
                jfc.setInitialFileName(currentFilePath.getFileName().toString());
            } else if (defaultSaveFileName != null){
                jfc.setInitialFileName(defaultSaveFileName);
            }
        }
        if (!fileFilters.isEmpty()){
            jfc.getExtensionFilters().addAll(fileFilters);
        }
        File file = jfc.showSaveDialog(stage);
        if (file == null) return false;
        Path newFilePath = file.getAbsoluteFile().toPath();
        selectedExt = jfc.getSelectedExtensionFilter();
        if (selectedExt != HanituFile.ALL_FILTER){
            String filename = newFilePath.getFileName().toString();
            if (selectedExt == null || !filename.endsWith(defaultFileExtName)){
                newFilePath = newFilePath.getParent().resolve(filename + "." + defaultFileExtName);
            } else if (selectedExt.getExtensions().stream().map(e -> e.substring(1)).noneMatch(filename::endsWith)){
                newFilePath = newFilePath.getParent()
                  .resolve(filename + selectedExt.getExtensions().get(0).substring(1));
            }
        }
        currentFilePath = newFilePath;
        return true;
    }

    /**
     * show saving file dialog.
     *
     * @return true if this action doesn't be interrupted.
     */
    private boolean savingFile(){
        if (currentFilePath == null) return false;
        try {
            onSaveFile(currentFilePath);
            currentFileModified = false;
            currentWorkDirectory = currentFilePath.getParent();
        } catch (IOException | RuntimeException e){
            Properties p = Base.loadProperties("gui");
            MessageSession.showErrorMessageDialog(
              String.format(p.getProperty("dialog.saveerror.message"), currentFilePath), e);
            return false;
        }
        return true;
    }

    /**
     * saving file to path
     *
     * ### saving file step
     * 1. open file stream to `savePath.tmp`
     * 2. write content to `savePath.tmp`
     * 3. delete `savePath`
     * 4. rename `savePath.tmp` to `savePath`
     *
     * @param saveFilePath original save file path
     * @param action actual saving action
     */
    public static void saveFileByTempReplace(Path saveFilePath, EConsumer<OutputStream, IOException> action)
      throws IOException{
        Path tmp = saveFilePath.getParent().resolve(saveFilePath.getFileName().toString() + ".tmp");
        try (OutputStream os = Files.newOutputStream(tmp)) {
            action.accept(os);
            Files.deleteIfExists(saveFilePath);
            Files.move(tmp, saveFilePath);
        } finally {
            Files.deleteIfExists(tmp);
        }
    }

    /**
     * quit
     *
     * If current file is modify, will ask user save it or not?
     */
    @SuppressWarnings("WeakerAccess")
    public boolean quit(){
        if (askSaveCurrentFile()){
            onQuit();
            return true;
        }
        return false;
    }

    @SuppressWarnings({"UnusedParameters"})
    public boolean processMenuEvent(ActionEvent e, String action){
        switch (action){
        case "menu.file.new":
            newEmptyFile();
            break;
        case "menu.file.open":
            openFile();
            break;
        case "menu.file.save":
            saveFile();
            break;
        case "menu.file.saveas":
            saveFileAs();
            break;
        case "menu.file.exit":
            quit();
            break;
        default:
            return false;
        }
        return true;
    }
}
