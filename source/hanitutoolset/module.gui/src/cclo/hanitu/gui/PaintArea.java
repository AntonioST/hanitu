/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.canvas.GraphicsContext;

/**
 * @author antonio
 */
public abstract class PaintArea{

    private PaintLayer pane;
    public final IntegerProperty x = new SimpleIntegerProperty();
    public final IntegerProperty y = new SimpleIntegerProperty();
    public final IntegerProperty width = new SimpleIntegerProperty();
    public final IntegerProperty height = new SimpleIntegerProperty();
    boolean visible = true;
    int layer;

    void setPanel(PaintLayer pane){
        this.pane = pane;
    }

    public abstract void paint(GraphicsContext g);

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isVisible(){
        return visible;
    }

    public void setVisible(boolean visible){
        this.visible = visible;
    }

    public double getXInArea(double x){
        return x - this.x.get();
    }

    public double getYInArea(double y){
        return y - this.y.get();
    }

    @SuppressWarnings("WeakerAccess")
    public double getPaneWidth(){
        return pane.getWidth();
    }

    @SuppressWarnings("unused")
    public double getPaneHeight(){
        return pane.getHeight();
    }
}
