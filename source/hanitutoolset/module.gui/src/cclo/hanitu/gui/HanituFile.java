/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.util.Properties;

import cclo.hanitu.Base;

import static javafx.stage.FileChooser.ExtensionFilter;

/**
 * @author antonio
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class HanituFile{

    public static final String WORLD_CONFIG_EXTEND_FILENAME;
    public static final String CIRCUIT_EXTEND_FILENAME;
    public static final String LOCATION_EXTEND_FILENAME;
    public static final String SPIKE_EXTEND_FILENAME;
    public static final String WORLD_CONFIG_DEFAULT_FILENAME;
    public static final String LOCATION_DEFAULT_FILENAME;
    public static final String SPIKE_DEFAULT_FILENAME;
    public static final String EVENT_DEFAULT_FILENAME;
    public static final String STAT_DEFAULT_FILENAME;

    static{
        Properties p = Base.loadProperties("file");
        WORLD_CONFIG_EXTEND_FILENAME = p.getProperty("world.extend");
        CIRCUIT_EXTEND_FILENAME = p.getProperty("circuit.extend");
        LOCATION_EXTEND_FILENAME = p.getProperty("location.extend");
        SPIKE_EXTEND_FILENAME = p.getProperty("spike.extend");
        WORLD_CONFIG_DEFAULT_FILENAME = p.getProperty("world.default");
        LOCATION_DEFAULT_FILENAME = p.getProperty("location.default");
        SPIKE_DEFAULT_FILENAME = p.getProperty("spike.default");
        EVENT_DEFAULT_FILENAME = p.getProperty("event.default");
        STAT_DEFAULT_FILENAME = p.getProperty("statistic.default");
    }

    public static final ExtensionFilter WORLD_CONFIG_FILTER;

    public static final ExtensionFilter CIRCUIT_CONFIG_FILTER;

    public static final ExtensionFilter NORMAL_TEXT_FILTER;

    public static final ExtensionFilter ALL_FILTER;

    static{
        Properties p = Base.loadProperties("file");
        WORLD_CONFIG_FILTER = new ExtensionFilter(p.getProperty("world.desp", "Hanitu World Configuration")
                                                  + "(" + WORLD_CONFIG_EXTEND_FILENAME + ")",
                                                  "*." + WORLD_CONFIG_EXTEND_FILENAME);
        CIRCUIT_CONFIG_FILTER = new ExtensionFilter(p.getProperty("circuit.desp", "Hanitu Circuit")
                                                    + "(" + CIRCUIT_EXTEND_FILENAME + ")",
                                                    "*." + CIRCUIT_EXTEND_FILENAME);
        NORMAL_TEXT_FILTER = new ExtensionFilter(p.getProperty("text.desp", "Text File") + " (txt)", "*.txt");
        ALL_FILTER = new ExtensionFilter(p.getProperty("all.desp", "All Files"), "*.*");
    }

    public static final ExtensionFilter[] WORLD_CONFIG_FILTERS
      = {WORLD_CONFIG_FILTER, NORMAL_TEXT_FILTER, ALL_FILTER};
    public static final ExtensionFilter[] CIRCUIT_CONFIG_FILTERS
      = {CIRCUIT_CONFIG_FILTER, NORMAL_TEXT_FILTER, ALL_FILTER};
    public static final ExtensionFilter[] LOCATION_FILTERS
      = {NORMAL_TEXT_FILTER, ALL_FILTER};
}
