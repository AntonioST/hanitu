/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.StringJoiner;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.Base;
import cclo.hanitu.util.ErrorFunction;

/**
 * @author antonio
 */
public class MessageSession{

    private static final String ALL_OPTIONS = "YNCKX";
    @SuppressWarnings("unused")
    public static final String DEFAULT_OPTIONS = "NY";
    public static final String NONE_OPTIONS = "";
    public static final String YES_NO_OPTIONS = "NY";
    public static final String YES_NO_CANCEL_OPTIONS = "CNY";
    @SuppressWarnings("unused")
    public static final String OK_CANCEL_OPTIONS = "CK";
    public static final String CLOSE_OPTIONS = "X";

    public static final int YES_OPTION = 1;
    public static final int NO_OPTION = 2;
    public static final int CANCEL_OPTION = 4;
    public static final int CLOSE_OPTION = 8;

    private static final String YES_TEXT;
    private static final String NO_TEXT;
    private static final String CANCEL_TEXT;
    private static final String CLOSE_TEXT;
    private static final String OK_TEXT;
    private static final String ERROR_TEXT;
    private static final String MESSAGE_TEXT;

    static{
        Properties p = Base.loadProperties("gui");
        YES_TEXT = p.getProperty("message.control.yes");
        NO_TEXT = p.getProperty("message.control.no");
        CANCEL_TEXT = p.getProperty("message.control.cancel");
        CLOSE_TEXT = p.getProperty("message.control.close");
        OK_TEXT = p.getProperty("message.control.ok");
        ERROR_TEXT = p.getProperty("message.error.title");
        MESSAGE_TEXT = p.getProperty("message.message.title");
    }

    private MessageSession(){
        throw new RuntimeException();
    }

    private static class CallBack<T>{

        T returnAction;
    }

    public static void showMessageDialog(String title, String message){
        Stage stage = new Stage(StageStyle.UTILITY);
        stage.setIconified(false);
        stage.setMaximized(false);
        stage.setTitle(title);
        stage.setMinWidth(200);
        stage.setMinHeight(100);
        //
        VBox root = new VBox();
        addText(root, message);
//        root.setOnKeyPressed(e -> {
//            switch (e.getCode()){
//            case ENTER:
//            case ESCAPE:
//                stage.close();
//                break;
//            }
//        });
        Insets value = new Insets(15);
        root.getChildren().forEach(c -> VBox.setMargin(c, value));
        //
        Scene scene = new Scene(root);
        scene.setOnMouseClicked(e -> stage.close());
        stage.setScene(scene);
        stage.sizeToScene();
        //
        stage.show();
        root.requestFocus();
    }

    public static int showConfirmDialog(String title, String message, String option){
        CallBack<Integer> session = new CallBack<>();
        session.returnAction = CLOSE_OPTION;
        //
        Stage stage = new Stage(StageStyle.UTILITY);
        stage.setTitle(title);
        stage.setMinWidth(400);
        stage.setMinHeight(100);
        //
        VBox textRegion = addText(new VBox(), message);
        //
        BiConsumer<ActionEvent, Integer> action = (e, code) -> {
            session.returnAction = code;
            stage.close();
        };
        HBox controlRegion = addControl(new HBox(), option, action);
        //
        textRegion.getChildren().addAll(controlRegion);
        Insets value = new Insets(15);
        textRegion.getChildren().forEach(c -> VBox.setMargin(c, value));
        //
        Scene scene = new Scene(textRegion);
        scene.setOnKeyPressed(e -> {
            switch (e.getCode()){
            case ENTER:
                controlRegion.getChildren().stream()
                  .filter(Node::isFocused)
                  .findFirst()
                  .ifPresent(child -> Event.fireEvent(child, new ActionEvent(child, child)));
                break;
            case ESCAPE:
                if (option.contains("C")){
                    action.accept(null, CANCEL_OPTION);
                } else {
                    action.accept(null, CLOSE_OPTION);
                }
                break;
            }
        });
        //
        stage.setScene(scene);
        //
        stage.requestFocus();
        stage.showAndWait();
        return session.returnAction;
    }

    public static String showInputDialog(String title, String message){
        return showInputDialog(title, message, null, null);
    }

    @SuppressWarnings("unused")
    public static String showInputDialog(String title, String message, Pattern pattern){
        Matcher m = pattern.matcher("");
        return showInputDialog(title, message, null, text -> {
            if (m.reset(text).matches()) return null;
            throw new IllegalArgumentException("illegal format");
        });
    }

    public static String showInputDialog(String title,
                                         String message,
                                         String initText,
                                         ErrorFunction.EFunction<String, String, RuntimeException> suggest){
        CallBack<String> session = new CallBack<>();
        Stage stage = new Stage(StageStyle.UTILITY);
        stage.setTitle(title);
        stage.setMinWidth(200);
        stage.setMinHeight(100);
        //
        TextField field = new TextField();
        if (initText != null){
            field.setText(initText);
        }
        Tooltip tooltip = new Tooltip();
        if (suggest != null){
            field.setOnKeyTyped(event -> {
                String c = event.getCharacter();
                int position = field.getSelection().getStart();
                String originText = field.getText();
                String tailText = originText.substring(field.getSelection().getEnd());
                String headText = originText.substring(0, position) + c;
                try {
                    String suggestText = suggest.accept(headText);
                    if (suggestText != null && !suggestText.isEmpty()){
                        field.setText(headText + suggestText + tailText);
                        field.selectRange(headText.length() + suggestText.length(), headText.length());
                        event.consume();
                    }
                    field.setTooltip(null);
                    PropertySession.setBackground(field, Color.WHITE);
                } catch (RuntimeException e){
                    tooltip.setText(e.getMessage());
                    field.setTooltip(tooltip);
                    PropertySession.setBackground(field, Color.PINK);
                }
            });
        }
        //
        BiConsumer<ActionEvent, Integer> action = (e, code) -> {
            if (code == YES_OPTION){
                String text = field.getText();
                if (suggest != null){
                    try {
                        suggest.accept(text);
                    } catch (RuntimeException ex){
                        showErrorMessageDialog(ex.getMessage());
                        return;
                    }
                }
                session.returnAction = text;
            }
            stage.close();
        };
        field.setOnAction(e -> action.accept(e, YES_OPTION));
        field.setOnKeyPressed(e -> {
            switch (e.getCode()){
            case ESCAPE:
                action.accept(null, CLOSE_OPTION);
                break;
            }
        });
        HBox controlRegion = addControl(new HBox(), YES_NO_OPTIONS, action);
        //
        VBox textRegion = new VBox();
        addText(textRegion, message);
        textRegion.getChildren().addAll(field, controlRegion);
        Insets value = new Insets(15);
        textRegion.getChildren().forEach(c -> VBox.setMargin(c, value));
        //
        Scene scene = new Scene(textRegion);
        stage.setScene(scene);
        stage.requestFocus();
        stage.showAndWait();
        return session.returnAction;
    }

    public static String showListSelectDialog(String title, String message, List<String> list, int initIndex){
        StringConverter<String> converter = new StringConverter<String>(){
            @Override
            public String toString(String object){
                return object;
            }

            @Override
            public String fromString(String string){
                return string;
            }
        };
        return showListSelectDialog(title, message, list, initIndex, converter);
    }

    public static <T> T showListSelectDialog(String title,
                                             String message,
                                             List<T> list,
                                             int initIndex,
                                             Function<T, String> f){
        List<String> texts = list.stream().map(f).collect(Collectors.toList());
        StringConverter<T> converter = new StringConverter<T>(){
            @Override
            public String toString(T object){
                return f.apply(object);
            }

            @Override
            public T fromString(String string){
                return list.get(texts.indexOf(string));
            }
        };
        return showListSelectDialog(title, message, list, initIndex, converter);
    }

    public static <T> T showListSelectDialog(String title,
                                             String message,
                                             List<T> list,
                                             int initIndex,
                                             StringConverter<T> toString){
        CallBack<T> session = new CallBack<>();
        Stage stage = new Stage(StageStyle.UTILITY);
        stage.setIconified(false);
        stage.setMaximized(false);
        stage.setTitle(title);
        //
        VBox root = new VBox();
        addText(root, message);
        ChoiceBox<T> choice = new ChoiceBox<>();
        choice.getItems().addAll(list);
        choice.getSelectionModel().select(initIndex);
        choice.setConverter(toString);
        //
        HBox control = addControl(new HBox(), YES_NO_OPTIONS, (e, code) -> {
            if (code == YES_OPTION){
                session.returnAction = choice.getValue();
            }
            stage.close();
        });
        root.getChildren().addAll(choice, control);
        Insets value = new Insets(15);
        root.getChildren().forEach(c -> VBox.setMargin(c, value));
        //
        Scene scene = new Scene(root);
        scene.setOnMouseClicked(e -> stage.close());
        stage.setScene(scene);
        stage.sizeToScene();
        root.requestFocus();
        stage.showAndWait();
        //
        return session.returnAction;
    }

    public static void showTextDialog(String title, String message, String content){
        Stage stage = new Stage(StageStyle.UTILITY);
        stage.setIconified(false);
        stage.setMaximized(false);
        stage.setTitle(title);
        stage.setMinWidth(200);
        stage.setMinHeight(200);
        //
        VBox root = new VBox();
        addText(root, message);
        TextArea text = new TextArea(content);
        text.setEditable(false);
        HBox control = addControl(new HBox(), CLOSE_OPTIONS, (actionEvent, integer) -> stage.close());
        root.getChildren().addAll(text, control);
        root.setOnKeyPressed(e -> {
            switch (e.getCode()){
            case ESCAPE:
                stage.close();
                break;
            }
        });
        Insets value = new Insets(15);
        root.getChildren().forEach(c -> VBox.setMargin(c, value));
        //
        Scene scene = new Scene(root);
        scene.setOnMouseClicked(e -> stage.close());
        stage.setScene(scene);
        stage.sizeToScene();
        //
        stage.show();
        root.requestFocus();
    }

    public static HBox addControl(HBox control, String option, BiConsumer<ActionEvent, Integer> action){
        control.setAlignment(Pos.BOTTOM_RIGHT);
        control.setSpacing(5);
        String upOption = option.toUpperCase();
        int[] count = new int[ALL_OPTIONS.length()];
        Arrays.fill(count, 0);
        for (int i = 0, lim = upOption.length(); i < lim; i++){
            Button button;
            char optionChar = upOption.charAt(i);
            int optionCount = count[ALL_OPTIONS.indexOf(optionChar)]++;
            if (optionCount > 1){
                throw new IllegalArgumentException("duplicate option : " + option.charAt(i));
            }
            switch (optionChar){
            case 'Y':
                button = new Button(YES_TEXT);
                button.setOnAction(e -> action.accept(e, YES_OPTION));
                break;
            case 'N':
                button = new Button(NO_TEXT);
                button.setOnAction(e -> action.accept(e, NO_OPTION));
                break;
            case 'C':
                button = new Button(CANCEL_TEXT);
                button.setOnAction(e -> action.accept(e, CANCEL_OPTION));
                break;
            case 'K':
                button = new Button(OK_TEXT);
                button.setOnAction(e -> action.accept(e, YES_OPTION));
                break;
            case 'X':
                button = new Button(CLOSE_TEXT);
                button.setOnAction(e -> action.accept(e, CLOSE_OPTION));
                break;
            default:
                throw new IllegalArgumentException("unknown option : " + option.charAt(i));
            }
            button.setMinWidth(70);
            control.getChildren().add(button);
        }
        return control;
    }

    public static VBox addText(VBox textRegion, String message){
        for (String line : message.split("\n")){
            TextFlow text = GraphicRichMessageTranslator.mapTextExpression(line);
            text.setTextAlignment(TextAlignment.JUSTIFY);
            textRegion.getChildren().add(text);
        }
        textRegion.setAlignment(Pos.TOP_CENTER);
        textRegion.setFillWidth(true);
        return textRegion;
    }

    public static void addTextWithUrlEvent(VBox textRegion, String message){
        for (String line : message.split("\n")){
            TextFlow text = GraphicRichMessageTranslator.mapTextExpression(line);
            text.getChildren().stream()
              .filter(node -> node instanceof Text)
              .map(node -> (Text)node)
              .filter(t -> t.isUnderline() || t.getText().contains("@"))
              .forEach(t -> setTextWithUrlEvent(textRegion, t));
            text.setTextAlignment(TextAlignment.JUSTIFY);
            textRegion.getChildren().add(text);
        }
        textRegion.setAlignment(Pos.TOP_CENTER);
        textRegion.setFillWidth(true);
    }

    private static void setTextWithUrlEvent(VBox textRegion, Text t){
        Scene scene = textRegion.getScene();
        Paint defaultColor = t.getFill();
        t.setOnMouseEntered(e -> {
            t.setFill(Color.CYAN);
            scene.setCursor(Cursor.HAND);
        });
        t.setOnMouseExited(e -> {
            t.setFill(defaultColor);
            scene.setCursor(Cursor.DEFAULT);
        });
        t.setOnMouseClicked(e -> {
            Clipboard clipboard = Clipboard.getSystemClipboard();
            ClipboardContent content = new ClipboardContent();
            content.putString(t.getText());
            content.putHtml(t.getText());
            clipboard.setContent(content);
        });
    }

    public static void showErrorMessageDialog(String... message){
        Logger LOG = LoggerFactory.getLogger(MessageSession.class);
        if (LOG.isDebugEnabled()){
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            for (String line : message){
                LOG.warn("[message] {}", line);
            }
            for (int i = 2, length = st.length; i < length; i++){
                LOG.warn("[trace] {}", st[i]);
            }
        }
        StringJoiner sj = new StringJoiner("\n");
        for (String s : message){
            sj.add(s);
        }
        showMessageDialog(ERROR_TEXT, sj.toString());
    }

    public static void showErrorMessageDialog(Throwable ex){
        Logger LOG = LoggerFactory.getLogger(MessageSession.class);
        LOG.warn("(empty error message) {}", ex);
        StringBuilder sb = new StringBuilder();
        for (Throwable e = ex; e != null; e = e.getCause()){
            sb.append(e.getClass()).append("\n");
            sb.append(e.getMessage()).append("\n");
        }
        showTextDialog(ERROR_TEXT, ex.getClass().getName(), sb.toString());
    }

    public static void showErrorMessageDialog(String message, Throwable ex){
        Logger LOG = LoggerFactory.getLogger(MessageSession.class);
        LOG.warn(message, ex);
        StringBuilder sb = new StringBuilder();
        sb.append(message).append("\n");
        for (Throwable e = ex; e != null; e = e.getCause()){
            sb.append(e.getClass()).append("\n");
            sb.append(e.getMessage()).append("\n");
        }
        showTextDialog(ERROR_TEXT, ex.getClass().getName(), sb.toString());
    }

    @SuppressWarnings("unused")
    public static void showDebugMessageDialog(String... message){
        Logger LOG = LoggerFactory.getLogger(MessageSession.class);
        if (LOG.isDebugEnabled()){
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            for (String line : message){
                LOG.debug("[message] {}", line);
            }
            for (int i = 2, length = st.length; i < length; i++){
                LOG.debug("[trace] {}", st[i]);
            }
        }
        StringJoiner sj = new StringJoiner("\n");
        for (String s : message){
            sj.add(s);
        }
        showMessageDialog(MESSAGE_TEXT, sj.toString());
    }


}
