/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.*;

import cclo.hanitu.Base;
import cclo.hanitu.util.RichMessageTranslator;

/**
 * @author antonio
 */
public class GraphicRichMessageTranslator{

    private static final String DEFAULT_FONT_FAMILY = Base.getProperty("cclo.hanitu.rich.font", "FreeMono");
    private static final int DEFAULT_FONT_SIZE = Base.getIntegerProperty("cclo.hanitu.rich.fontsize", 16);

    private static final FontWeight[] WEIGHTS
      = {FontWeight.LIGHT, FontWeight.THIN, FontWeight.NORMAL, FontWeight.BOLD, FontWeight.BLACK};
    private static final FontPosture[] STYLES
      = {FontPosture.REGULAR, FontPosture.ITALIC};

    private static Paint mapColor(String m){
        switch (m){
        case "r":
            return Color.RED;
        case "g":
            return Color.GREEN;
        case "y":
            return Color.YELLOW;
        case "b":
            return Color.BLUE;
        case "m":
            return Color.MAGENTA;
        case "c":
            return Color.CYAN;
        case "":
        case "k":
            return Color.BLACK;
        case "w":
            return Color.WHITE;
        }
        return null;
    }

    private static Text map(RichMessageTranslator.Unit unit){
        Text text = new Text(unit.text);
        String font = unit.font.isEmpty()? DEFAULT_FONT_FAMILY: unit.font;
        FontWeight weight = WEIGHTS[unit.weight];
        FontPosture style = STYLES[unit.style];
        int fontsize = unit.size < 0? DEFAULT_FONT_SIZE: unit.size;
        text.setFont(Font.font(font, weight, style, fontsize));
        Paint color = mapColor(unit.foregroundColor);
        if (color != null){
            text.setFill(color);
        }
        text.setUnderline(unit.underLine);
        text.setStrikethrough(unit.strikeThrough);
        return text;
    }

    public static TextFlow mapTextExpression(String line){
        TextFlow ret = new TextFlow();
        RichMessageTranslator.parse(line).map(GraphicRichMessageTranslator::map).forEach(ret.getChildren()::add);
        return ret;
    }
}
