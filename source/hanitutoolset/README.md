Hanitu Package
==============

The software package of the Hanitu system

Hanitu is a simulator environment for education of computational neuroscience.
A manuscript is submitted for publication in a scientific journal.

The package contains source code and pre-compiled executable for Linux.
See User's guide in the documentations directory for detail.

This application is developed by CCLo lab, National Tsing Hua University, Taiwan
([website](http://life.nthu.edu.tw/~lablcc/index.html))

Basic Information
-----------------

* __authors__

    Ta-Shun Su ([email](mailto:tashunsu@lolab-nthu.org))

* __version__

    stable : v2.5

    match Hanitu version : v1.4-r2

What is Hanitu?
---------------

__Hanitu__ is a simulation environment that simulates behavior and neural activity of
user-designed virtual worms in a two-dimensional virtual world. In __Hanitu__, students need to
think deeply about how different components of a nervous system work together to achieve
the ultimate goal of an animal – to survive in a changing environment

__Hanitu__ is designed for graduate and undergraduate students who already acquired basic
knowledge about computational neuroscience and would like to apply what they learned to
addressing some of the very interesting system-level questions.

-------------------------------------------------------------------------------

Required
--------

* Java Development Kit 8
* Apache Ant (build)
* Apache Ivy (build)
* g++ (for native program)

Compile
-------

    ant
   
You can get find the jar file in directory `build/package`.
    
Install
-------

1. Decompress hanitu-linux-x *arch* - *version* .tar.gz file to any directory.
 
        tar xvf hanitu-linux-x64-v1.3.tar.gz
        mv hanitu-linux-x64-v1.3 path-to-install
        
2. Edit `.bashrc` add install path to environment variable `PATH`

        PATH="path-to-install/bin:$PATH"
        
3. run command `hanitu_tool` to check installing successfully

        hanitu_tool

-------------------------------------------------------------------------------

Web Site
---------

* [wiki] (https://bitbucket.org/AntonioST/hanitutoolset/wiki/Home)
* [hanitu] (https://bitbucket.org/AntonioST/hanitu)

-------------------------------------------------------------------------------

TODO RoadMap
------------

* **v1.5** 

    Somatic sensory neurons, light sensory neurons, turn movement, continuous space (non-grid) 
     
    _GUI_: Electrophysiology lab  -> _for lego bots_
    
    **Expect** HanituToolSet version _v2.4_
    
* **v1.6** 

    Short-term and long-term plasticity + reward system, environmental events (variable foods and toxicants, disasters) 
     
    _GUI_: server infrastructure, account system  -> _for online games_
    
* **v1.7**

    implement reproduction and evolution

-------------------------------------------------------------------------------

Bug Report
----------

Please go to this [site](https://bitbucket.org/AntonioST/hanitutoolset/issues?status=new&status=open) to report a issue
