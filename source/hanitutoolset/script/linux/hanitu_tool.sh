#! /bin/bash

declare -r JAVA_HOME
declare HANITU_HOME

if [ -z "$HANITU_HOME" ]; then
    PRG="$0"
    while [ -h "$PRG" ]; do
        ls=`ls -ld "$PRG"`
        link=`expr "$ls" : '.*-> \(.*\)$'`
        if expr "$link" : '/.*' > /dev/null; then
            PRG=$link
        else
            PRG=`dirname "$PRG"`/$link
        fi
    done
    OLD_PWD=`pwd`
    HANITU_HOME=`cd $(dirname $PRG)/..; pwd -P`
    cd $OLD_PWD >&-
fi

# jar file
HANITU_JAR=@HANITU_JAR@
HANITU=$HANITU_HOME/lib/@HANITU@
FLYSIM=$HANITU_HOME/lib/@FLYSIM@
MAIN=@MAIN@

# java path setting and version check
check_java_version(){
    if [ -n "`$1 -version 2>&1 | grep version | grep 1.8`" ]; then
        return 0
    else
        return 1
    fi
}

if check_java_version java; then
    JAVA=java
elif [ -d $HANITU_HOME/jre ]; check_java_version $HANITU_HOME/jre/bin/java; then
    JAVA=$HANITU_HOME/jre/bin/java
elif [ -d $HANITU_HOME/jre1.8 ]; check_java_version $HANITU_HOME/jre1.8/bin/java; then
    JAVA=$HANITU_HOME/jre1.8/bin/java
elif [ -n "$JAVA_HOME" ]; check_java_version $JAVA_HOME/bin/java; then
    JAVA=$JAVA_HOME/bin/java
elif [ -d $HANITU_HOME/jre ]; check_java_version $HANITU_HOME/jre/bin/java; then
    JAVA=$HANITU_HOME/jre/bin/java
else
    >&2 echo "java version require 1.8"
    exit 1
fi

# global program options
if [ -f "$HANITU_HOME/jvmoption" ]; then
    JVM_OPTION="$JVM_OPTION $(cat $HANITU_HOME/jvmoption | tr '\n' ' ' )"
fi

exec $JAVA $JVM_OPTION  \
    -Dcclo.hanitu.0="$0"                            \
    -Dcclo.hanitu.home="$HANITU_HOME"               \
    -Dcclo.hanitu.flysim="$FLYSIM"                  \
    -Dcclo.hanitu.flysim.out=null                   \
    -Dcclo.hanitu.flysim.err=null                   \
    -Dcclo.hanitu.hanitu="$HANITU"                  \
    -Dcclo.hanitu.hanitu.out=null                   \
    -Dcclo.hanitu.hanitu.err=-                      \
    -jar "$HANITU_HOME/lib/$HANITU_JAR" $MAIN $*
