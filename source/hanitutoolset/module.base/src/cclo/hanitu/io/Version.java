/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.io;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author antonio
 */
public class Version{

    @SuppressWarnings("unused")
    public static int toNumeric(String version){
        return toNumeric(version, 2);
    }

    @SuppressWarnings("WeakerAccess")
    public static int toNumeric(String version, int level){
        if (!version.matches("\\d+(\\.\\d+)*(-.+)?")){
            throw new IllegalArgumentException("unknown version expression : " + version);
        }
        Iterator<String> it;
        if (version.contains("-")){
            int index = version.indexOf("-");
            it = Arrays.asList(version.substring(0, index).split("\\.")).iterator();
        } else {
            it = Arrays.asList(version.split("\\.")).iterator();
        }
        int ret = 0;
        for (int i = 0; i < level; i++){
            if (it.hasNext()){
                ret = ret * 100 + Integer.parseInt(it.next());
            } else {
                ret = ret * 100;
            }
        }
        return ret;
    }

    /**
     * test target version is compatibility to the required version
     *
     * Pattern
     * -------
     *
     * *Target Version*
     *
     * number[.number]*[-text]
     *
     * text after '-' will be ignored.
     *
     * *Required Version*
     *
     * [op]number[.number]*[.*]?
     *
     * op could be:
     *
     * + `=`, `==`
     * + `>=`
     * + `<=`
     * + `>`, `>>`
     * + `<`, `<<`
     * + '!`, `!=`
     *
     * @param version target version expression
     * @param require required version expression, cau use ',' to split each require condition.
     * @return version is compatibility to require
     */
    public static boolean isCompatibility(String version, String require){
        Objects.requireNonNull(version, "version expression");
        if (require.contains(",")){
            return Stream.of(require.split(",")).allMatch(r -> isCompatibility(version, r));
        }
        String requiredVersion;
        int op;
        if (require.startsWith("==")){
            requiredVersion = require.substring(2).trim();
            op = 0;
        } else if (require.startsWith("=")){
            requiredVersion = require.substring(1).trim();
            op = 0;
        } else if (require.startsWith(">=")){
            requiredVersion = require.substring(2).trim();
            op = 3;
        } else if (require.startsWith(">>")){
            requiredVersion = require.substring(2).trim();
            op = 2;
        } else if (require.startsWith(">")){
            requiredVersion = require.substring(1).trim();
            op = 2;
        } else if (require.startsWith("<=")){
            requiredVersion = require.substring(2).trim();
            op = 5;
        } else if (require.startsWith("<<")){
            requiredVersion = require.substring(2).trim();
            op = 4;
        } else if (require.startsWith("<")){
            requiredVersion = require.substring(1).trim();
            op = 4;
        } else if (require.startsWith("!=")){
            requiredVersion = require.substring(2).trim();
            op = 1;
        } else if (require.startsWith("!")){
            requiredVersion = require.substring(1).trim();
            op = 1;
        } else {
            throw new IllegalArgumentException("unknown version require expression : " + require);
        }
        if (!version.matches("\\d+(\\.\\d+)*(-.+)?")){
            throw new IllegalArgumentException("unknown version expression : " + version);
        } else if (!requiredVersion.matches("\\d+(\\.\\d+)*(\\.\\*)?")){
            throw new IllegalArgumentException("unknown version expression : " + requiredVersion);
        } else if((op == 2 || op == 3 || op == 4 || op == 5) && requiredVersion.contains("*")) {
            throw new IllegalArgumentException("cannot contain '*' for non-determine version");
        }
        Iterator<String> t1;
        if (version.contains("-")){
            int index = version.indexOf("-");
            t1 = Arrays.asList(version.substring(0, index).split("\\.")).iterator();
        } else {
            t1 = Arrays.asList(version.split("\\.")).iterator();
        }
        Iterator<String> t2 = Arrays.asList(requiredVersion.split("\\.")).iterator();
        String padding = "0";
        while (t1.hasNext() || t2.hasNext()){
            String s1 = t1.hasNext()? t1.next(): "0";
            String s2 = t2.hasNext()? t2.next(): padding;
            if ("*".equals(s2)){
                s2 = s1;
                padding = "*";
            }
            int v1 = Integer.parseInt(s1);
            int v2 = Integer.parseInt(s2);
            // 0 ==
            // 1 !=
            // 2 >>
            // 3 >=
            // 4 <<
            // 5 <=
            if (op == 0 && v1 != v2){
                return false;
            } else if (op == 1 && v1 != v2){
                return true;
            } else if (op == 2 && v1 > v2){
                return true;
            } else if (op == 3 && v1 < v2){
                return false;
            } else if (op == 4 && v1 < v2){
                return true;
            } else if (op == 5 && v1 > v2){
                return false;
            }
        }
        return (op == 0 || op == 3 || op == 5);
    }
}
