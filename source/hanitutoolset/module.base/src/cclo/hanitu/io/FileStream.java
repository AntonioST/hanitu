/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * @author antonio
 */
public class FileStream{

    public static Path fixPath(String filePath){
        if (filePath.startsWith("~/")){
            return Paths.get(System.getProperty("user.home")).resolve(filePath.substring(2));
        } else {
            return Paths.get(filePath);
        }
    }

    public static Stream<String> load(String path) throws IOException{
        return load(fixPath(path));
    }

    public static Stream<String> load(Path path) throws IOException{
        BufferedReader reader = Files.newBufferedReader(path);
        return reader.lines().onClose(() -> {
            try {
                reader.close();
            } catch (IOException e){
                e.printStackTrace();
            }
        });
    }

    public static Stream<String> load(InputStream is){
        return load(new BufferedReader(new InputStreamReader(is)));
    }

    public static Stream<String> load(BufferedReader reader){
        return reader.lines();
    }

    public static Stream<String> loadCommandLineFile(String commandLineFile) throws IOException{
        if (commandLineFile == null || commandLineFile.equals("-")){
            return load(System.in);
        } else {
            return load(commandLineFile);
        }
    }

}
