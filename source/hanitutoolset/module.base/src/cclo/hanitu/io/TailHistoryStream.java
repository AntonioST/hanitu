/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.io;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * @author antonio
 */
public class TailHistoryStream<T> extends TailStream<T>{

    private final TailStream<T> source;
    private final LinkedList<T> history = new LinkedList<>();
    private Iterator<T> iterator;
    private T current;

    @SuppressWarnings("unused")
    public TailHistoryStream(List<T> source){
        this.source = TailStream.<T>nullStream();
        history.addAll(source);
        iterator = history.iterator();
    }

    public TailHistoryStream(TailStream<T> source){
        this.source = Objects.requireNonNull(source);
    }

    @Override
    public void close() throws IOException{
        source.close();
    }

    @Override
    public boolean hasNext(){
        if (iterator != null && iterator.hasNext()) return true;
        iterator = null;
        return source.hasNext();
    }

    @Override
    public T next(){
        T ret;
        if (iterator != null && iterator.hasNext()){
            ret = iterator.next();
        } else {
            iterator = null;
            ret = source.next();
            if (ret != null) history.add(ret);
        }
        if (ret != null) current = ret;
        return ret;
    }

    public T get(){
        return current;
    }

    public void head(){
        current = null;
        if (!history.isEmpty()) iterator = history.iterator();
    }

    public void end(){
        iterator = null;
    }
}
