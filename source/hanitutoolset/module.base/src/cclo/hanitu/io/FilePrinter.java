/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.value.ObservableValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static cclo.hanitu.io.FileLoader.*;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;

/**
 * Hanitu file Printer for printing text content to output stream.
 *
 * @author antonio
 */
public abstract class FilePrinter<T> implements AutoCloseable{

    protected final Logger LOG = LoggerFactory.getLogger(FilePrinter.class);

    private PrintStream out;
    private boolean enableIndent = false;
    private int indentLevel = 0;
    private int indentSpace = 4;
    private String indentCache = "";

    public int getIndentLevel(){
        return indentLevel;
    }

    public void setIndentLevel(int indentLevel){
        this.indentLevel = indentLevel;
    }

    public int getIndentSpace(){
        return indentSpace;
    }

    public void setIndentSpace(int indentSpace){
        this.indentSpace = indentSpace;
    }

    public boolean isEnableIndent(){
        return enableIndent;
    }

    public void setEnableIndent(boolean enableIndent){
        this.enableIndent = enableIndent;
    }

    @SuppressWarnings("WeakerAccess")
    public abstract String getFileType();

    @SuppressWarnings({"WeakerAccess", "SameReturnValue"})
    public abstract String getTargetVersionExpression();

    public void write(String filePath, T target) throws IOException{
        if (filePath.startsWith("~/")){
            write(Paths.get(System.getProperty("user.home")).resolve(filePath.substring(2)), target);
        } else {
            write(Paths.get(filePath), target);
        }
    }

    public void write(Path file, T target) throws IOException{
        LOG.debug("write target : {}", target);
        LOG.debug("write to : {}", file.toAbsolutePath());
        try (OutputStream os = Files.newOutputStream(file, CREATE, TRUNCATE_EXISTING)) {
            writePrivate(new PrintStream(os), target);
        }
    }

    public void write(OutputStream os, T target){
        write(new PrintStream(os), target);
    }

    @SuppressWarnings("WeakerAccess")
    public void write(PrintStream ps, T target){
        LOG.debug("write target : {}", target);
        LOG.debug("write to steam");
        writePrivate(ps, target);
    }

    private void writePrivate(PrintStream ps, T target){
        out = ps;
        prevProcess(/*target*/);
        process(target);
    }

    protected void prevProcess(/*T target*/){
        meta(META_HEADER, getFileType());
        meta(META_VERSION, getTargetVersionExpression());
        meta(META_CREATE_TIME, Long.toString(System.currentTimeMillis()));
        line();
    }

    protected abstract void process(T target);

    @Override
    public void close(){
        out.close();
    }

    /**
     * add comment.
     *
     * @param content comment content
     */
    protected void comment(String content){
        indent();
        out.printf("%% %s%n", content);
    }

    protected void meta(String key){
        indent();
        out.printf("%s%s%n", FileLoader.META_HEAD, key);
    }

    protected void meta(String key, String value){
        indent();
        out.printf("%s%s=%s%n", FileLoader.META_HEAD, key, value);
    }

    protected void metaf(String key, String format, Object... args){
        indent();
        Object[] clone = args.clone();
        for (int i = 0, length = clone.length; i < length; i++){
            if (args[i] instanceof ObservableValue){
                clone[i] = ((ObservableValue)args[i]).getValue();
            }
        }
        out.printf("%s%s=%s%n", FileLoader.META_HEAD, key, String.format(format, clone));
    }

    protected void pair(String key, int value){
        indent();
        out.printf("%s=%d%n", key, value);
    }

    protected void pair(String key, double value){
        indent();
        out.printf("%s=%f%n", key, value);
    }

    protected void pair(String key, String value){
        indent();
        out.printf("%s=%s%n", key, value);
    }

    protected void pair(String key, ReadOnlyIntegerProperty value){
        indent();
        out.printf("%s=%d%n", key, value.get());
    }

    protected void pair(String key, ReadOnlyDoubleProperty value){
        indent();
        out.printf("%s=%f%n", key, value.get());
    }

    protected void pair(String key, ReadOnlyStringProperty value){
        indent();
        out.printf("%s=%s%n", key, value.get());
    }

    protected void line(String key){
        indent();
        out.println(key);
    }

    protected void line(){
        out.println();
    }

    private void indent(){
        if (enableIndent){
            out.print(indentCache);
        }
    }

    protected void incIndentLevel(){
        indentLevel++;
        StringBuilder tmp = new StringBuilder();
        for (int i = 0, c = indentLevel * indentSpace; i < c; i++) tmp.append(' ');
        indentCache = tmp.toString();
    }

    protected void decIndentLevel(){
        indentLevel--;
        if (indentLevel > 0){
            StringBuilder tmp = new StringBuilder();
            for (int i = 0, c = indentLevel * indentSpace; i < c; i++) tmp.append(' ');
            indentCache = tmp.toString();
        } else {
            indentLevel = 0;
            indentCache = "";
        }
    }
}
