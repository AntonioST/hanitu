/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import cclo.hanitu.util.ErrorFunction;

/**
 * @author antonio
 */
@SuppressWarnings("unused")
public abstract class TailStream<T> implements Iterator<T>, AutoCloseable{

    @Override
    public void close() throws IOException{
    }

    public static <T> TailStream<T> nullStream(){
        return new TailStream<T>(){

            @Override
            public boolean hasNext(){
                return false;
            }

            @Override
            public T next(){
                return null;
            }
        };
    }

    public <R> TailStream<R> map(Function<T, R> map){
        Objects.requireNonNull(map);
        TailStream<T> self = this;
        return new TailStream<R>(){
            @Override
            public boolean hasNext(){
                return self.hasNext();
            }

            @Override
            public R next(){
                T next = self.next();
                return next == null? null: map.apply(next);
            }

            @Override
            public void close() throws IOException{
                self.close();
            }
        };
    }

    public <R> TailStream<R> map(Function<T, R> map, Consumer<Throwable> handle){
        Objects.requireNonNull(map);
        Objects.requireNonNull(handle);
        TailStream<T> self = this;
        return new TailStream<R>(){
            @Override
            public boolean hasNext(){
                return self.hasNext();
            }

            @Override
            public R next(){
                T next = self.next();
                if (next != null){
                    try {
                        return map.apply(next);
                    } catch (Exception e){
                        handle.accept(e);
                    }
                }
                return null;
            }

            @Override
            public void close() throws IOException{
                self.close();
            }
        };
    }

    public <R> TailStream<R> mapIgnoreError(Function<T, R> map){
        Objects.requireNonNull(map);
        TailStream<T> self = this;
        return new TailStream<R>(){
            @Override
            public boolean hasNext(){
                return self.hasNext();
            }

            @Override
            public R next(){
                T next = self.next();
                if (next != null){
                    //noinspection EmptyCatchBlock
                    try {
                        return map.apply(next);
                    } catch (Exception e){
                    }
                }
                return null;
            }

            @Override
            public void close() throws IOException{
                self.close();
            }
        };
    }

    public <R> TailStream<R> flatMap(Function<T, Optional<R>> map){
        Objects.requireNonNull(map);
        TailStream<T> self = this;
        return new TailStream<R>(){
            R next = null;

            @Override
            public boolean hasNext(){
                while (next == null){
                    if (!self.hasNext()) break;
                    map.apply(self.next()).ifPresent(r -> next = r);
                }
                return next != null;
            }

            @Override
            public R next(){
                R ret = next;
                next = null;
                return ret;
            }

            @Override
            public void close() throws IOException{
                self.close();
            }
        };
    }

    public TailStream<T> peek(Consumer<T> consumer){
        Objects.requireNonNull(consumer);
        TailStream<T> self = this;
        return new TailStream<T>(){
            @Override
            public boolean hasNext(){
                return self.hasNext();
            }

            @Override
            public T next(){
                T next = self.next();
                if (next == null) return null;
                consumer.accept(next);
                return next;
            }

            @Override
            public void close() throws IOException{
                self.close();
            }
        };
    }

    public TailStream<T> filter(Predicate<T> test){
        Objects.requireNonNull(test);
        TailStream<T> self = this;
        return new TailStream<T>(){
            T next = null;

            @Override
            public boolean hasNext(){
                while (next == null){
                    if (!self.hasNext()) break;
                    T t = self.next();
                    if (test.test(t)) next = t;
                }
                return next != null;
            }

            @Override
            public T next(){
                T ret = next;
                next = null;
                return ret;
            }

            @Override
            public void close() throws IOException{
                self.close();
            }
        };
    }

    public TailStream<T> autoClose(){
        TailStream<T> self = this;
        return new TailStream<T>(){
            boolean close = false;

            @Override
            public boolean hasNext(){
                if (close) return false;
                if (!self.hasNext()){
                    //noinspection EmptyCatchBlock
                    try {
                        close();
                    } catch (Exception e){
                    }
                    return false;
                }
                return true;
            }

            @Override
            public T next(){
                if (!close){
                    T next = self.next();
                    if (next != null) return next;
                }
                return null;
            }

            @Override
            public void close() throws IOException{
                close = true;
                self.close();
            }
        };
    }

    public TailStream<T> onClose(ErrorFunction.Action<IOException> action){
        Objects.requireNonNull(action);
        TailStream<T> self = this;
        return new TailStream<T>(){
            @Override
            public boolean hasNext(){
                return self.hasNext();
            }

            @Override
            public T next(){
                return self.next();
            }

            @Override
            public void close() throws IOException{
                self.close();
                action.run();
            }
        };
    }

    public static TailStream<String> follow(String path){
        return new TailFileStream(FileStream.fixPath(path));
    }

    public static TailStream<String> follow(Path path){
        return new TailFileStream(path);
    }

    @SuppressWarnings("WeakerAccess")
    public static TailStream<String> follow(InputStream is){
        return new TailFileStream(new BufferedReader(new InputStreamReader(is)));
    }

    public static TailStream<String> follow(BufferedReader source){
        return new TailFileStream(source);
    }

    public static TailStream<String> followCommandLineFile(String commandLineFile){
        if (commandLineFile == null || commandLineFile.equals("-")){
            return follow(System.in);
        } else {
            return follow(commandLineFile);
        }
    }

    @SuppressWarnings("WeakerAccess")
    public static <T> TailStream<T> of(Iterator<T> source){
        return new TailStream<T>(){
            @Override
            public boolean hasNext(){
                return source.hasNext();
            }

            @Override
            public T next(){
                try {
                    return source.next();
                } catch (NoSuchElementException e){
                    return null;
                }
            }
        };
    }

    public static <T> TailStream<T> of(Stream<T> source){
        return of(source.iterator()).onClose(source::close);
    }

    private static class TailFileStream extends TailStream<String>{

        private Path sourceFile;
        private BufferedReader source;
        private String nextLine;

        public TailFileStream(Path path){
            sourceFile = path;
        }

        public TailFileStream(BufferedReader source){
            this.source = source;
        }

        private BufferedReader reader() throws IOException{
            if (source != null) return source;
            if (sourceFile == null) return null;
            if (Files.exists(sourceFile) && Files.isRegularFile(sourceFile)){
                source = Files.newBufferedReader(sourceFile);
            }
            return source;
        }

        @Override
        public boolean hasNext(){
            if (nextLine == null){
                //noinspection EmptyCatchBlock
                try {
                    BufferedReader reader = reader();
                    if (reader != null){
                        nextLine = reader.readLine();
                    }
                } catch (IOException e){
                }
            }
            return nextLine != null;
        }

        @Override
        public String next(){
            if (nextLine == null) hasNext();
            String ret = nextLine;
            nextLine = null;
            return ret;
        }

        @Override
        public void close() throws IOException{
            if (source != null){
                source.close();
                source = null;
                sourceFile = null;
            }
        }
    }

    /**
     * testing main
     * @param args listen file
     */
    @SuppressWarnings({"UseOfSystemOutOrSystemErr", "InfiniteLoopStatement"})
    public static void main(String[] args) throws IOException{
        if (args.length == 0){
            System.out.println("java ... " + TailStream.class + " FILE");
            System.exit(0);
        } else if (args.length != 1){
            throw new IllegalArgumentException("unknown argument : " + args[1]);
        } else {
            TailStream<String> tail = follow(args[0]);
            while (true){
                // until user send term or kill signal
                if (tail.hasNext()) System.out.println(tail.next());
                //noinspection EmptyCatchBlock
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e){
                }
            }
        }
    }
}
