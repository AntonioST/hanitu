/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;


/**
 * The Abstract Class of Hanitu Parameter File Parser.
 *
 * It is a line-base, state parser. Each parameter setting following the `KEY=VALUE` pattern, and use Keyword to label
 * the begin of data structure and the end.
 * @author antonio
 */
public abstract class FileLoader<T>{

    protected static final int STATE_INIT = 0;
    protected static final int STATE_TERMINATE = -1;
    protected static final int STATE_ERROR = -2;

    public static final String META_HEAD = "%meta.";
    public static final String META_HEADER = "header";
    public static final String META_AUTHOR = "author";
    public static final String META_VERSION = "version";
    public static final String META_CREATE_TIME = "createtime";

    protected final Logger LOG = LoggerFactory.getLogger(FileLoader.class);

    protected Path sourcePath;
    private BufferedReader reader;
    private int lineNumber;
    private String originLine;
    private String currentLine;
    protected int currentState;
    //
    private String metaHeader;
    private String metaAuthor;
    private String metaVersion;
    private String metaCreateTime;
    //
    private boolean failOnLoad = false;
    private boolean caseSensitive = false;

    @SuppressWarnings("unused")
    public String getMetaCreateTime(){
        return metaCreateTime;
    }

    @SuppressWarnings("unused")
    public String getMetaHeader(){
        return metaHeader;
    }

    @SuppressWarnings("unused")
    public String getMetaAuthor(){
        return metaAuthor;
    }

    @SuppressWarnings("unused")
    public String getMetaVersion(){
        return metaVersion;
    }

    @SuppressWarnings("WeakerAccess")
    public boolean isFailOnLoad(){
        return failOnLoad;
    }

    public void setFailOnLoad(boolean failOnLoad){
        this.failOnLoad = failOnLoad;
    }

    @SuppressWarnings("unused")
    public boolean isCaseSensitive(){
        return caseSensitive;
    }

    @SuppressWarnings("unused")
    public void setCaseSensitive(boolean caseSensitive){
        this.caseSensitive = caseSensitive;
    }

    @SuppressWarnings("WeakerAccess")
    public abstract String getFileType();

    @SuppressWarnings("WeakerAccess")
    public boolean isFileTypeSupport(String value){
        return getFileType().equals(value);
    }

    @SuppressWarnings({"WeakerAccess", "SameReturnValue"})
    public abstract String getFileVersionExpression();

    @SuppressWarnings("WeakerAccess")
    public boolean isFileVersionSupport(String version){
        return Version.isCompatibility(version, getFileVersionExpression());
    }

    @SuppressWarnings("WeakerAccess")
    public T load(String filePath) throws IOException{
        return load(FileStream.fixPath(filePath));
    }

    public T load(Path filePath) throws IOException{
        LOG.debug("load from : {}", filePath.toAbsolutePath());
        sourcePath = filePath;
        try (BufferedReader r = Files.newBufferedReader(filePath)) {
            return loadPrivate(r);
        }
    }

    @SuppressWarnings("unused")
    public T load(InputStream is) throws IOException{
        return load(new BufferedReader(new InputStreamReader(is)));
    }

    @SuppressWarnings("WeakerAccess")
    public T load(BufferedReader r) throws IOException{
        LOG.debug("load from stream");
        return loadPrivate(r);
    }

    private T loadPrivate(BufferedReader r) throws IOException{
        reader = r;
        lineNumber = 0;
        currentState = STATE_INIT;
        LOG.trace("pre-process");
        prevProcess();
        while (currentState >= 0){
            //noinspection EmptyCatchBlock
            try {
                nextLine();
                if (LOG.isTraceEnabled()){
                    LOG.trace("[{}][{}] {}", sourcePath, lineNumber, originLine);
                    LOG.trace("state : {}", getStateDescription(getClass(), currentState));
                }
                if (currentState < 0) break;
                if (!currentLine.isEmpty()){
                    processLine(currentLine);
                }
                currentLine = null;
            } catch (ReParseInterrupt interrupt){
            } catch (RuntimeException e){
                addWarning(e);
                currentLine = null;
            }
        }
        if (currentState <= STATE_ERROR){
            addError("error {}", currentState);
        }
        T ret = postProcess();
        if (ret != null){
            LOG.debug("loaded {}@{}", ret.getClass().getName(), Objects.hashCode(ret));
        } else {
            LOG.debug("loaded null");
        }
        return ret;
    }

    private void nextLine() throws IOException{
        if (currentLine == null){
            originLine = reader.readLine();
            if (originLine == null){
                currentState = STATE_TERMINATE;
                return;
            }
            lineNumber++;
            currentLine = prevProcessLine(originLine);
        }
    }

    protected void prevProcess() throws IOException{
        processFileMeta();
        if (metaHeader != null && !isFileTypeSupport(metaHeader)){
            addError("unsupported file type {}", metaHeader);
        }
        if (metaVersion != null && !isFileVersionSupport(metaVersion)){
            addError("unsupported file version {}", metaVersion);
        }
    }

    protected abstract T postProcess() throws IOException;

    private String prevProcessLine(String line){
        line = line.trim();
        // remove comment
        if (line.contains("%")){
            int index;
            if (isMetaLine(META_HEAD)){
                index = line.indexOf("%", 1);
            } else {
                index = line.indexOf("%");
            }
            if (index != -1){
                return line.substring(0, index).trim();
            } else {
                return line;
            }
        }
        return line;
    }

    private void processFileMeta() throws IOException{
        while (true){
            nextLine();
            if (!currentLine.isEmpty()){
                if (!isMetaLine(currentLine)){
                    break;
                }
                LOG.trace("[{}] {}", lineNumber, originLine);
                processFileMeta(getMetaKey(currentLine), getMetaValue(currentLine));
            }
            currentLine = null;
        }
    }

    private void processFileMeta(String key, String value){
        LOG.debug("{} = {}", key, value);
        switch (key){
        case "header":
            metaHeader = value;
            break;
        case "version":
            metaVersion = value;
            break;
        case "author":
            metaAuthor = value;
            break;
        case "createtime":
            metaCreateTime = value;
            break;
        }
    }

    protected abstract void processLine(String line) throws IOException;

    protected void reParsing(){
        throw new ReParseInterrupt();
    }

    protected static boolean isMetaLine(String line){
        return line.startsWith("%meta.");
    }

    protected static String getMetaKey(String line){
        if (!isMetaLine(line)) throw new IllegalArgumentException("not a meta line : " + line);
        int index = line.indexOf("=");
//        if (index == -1) index = line.indexOf(":");
//        if (index == -1) index = line.indexOf(" ");
        if (index == -1) return line.substring(6).toLowerCase();
        return line.substring(6, index).toLowerCase();
    }

    protected static String getMetaValue(String line){
        if (!isMetaLine(line)) throw new IllegalArgumentException("not a meta line : " + line);
        int index = line.indexOf("=");
//        if (index == -1) index = line.indexOf(":");
//        if (index == -1) index = line.indexOf(" ");
        if (index == -1) return null;
        return line.substring(index + 1);
    }

    protected static String getKey(String line){
        int index = line.indexOf("=");
//        if (index == -1) index = line.indexOf(":");
//        if (index == -1) index = line.indexOf(" ");
        if (index == -1) return line;
        return line.substring(0, index);
    }

    protected static String getValue(String line){
        int index = line.indexOf("=");
//        if (index == -1) index = line.indexOf(":");
//        if (index == -1) index = line.indexOf(" ");
        if (index == -1) return null;
        return line.substring(index + 1);
    }

    protected String getStrValue(String line) throws HanituLoadingException{
        String ret = getValue(line);
        if (ret == null){
            addWarning("lost value");
        }
        return ret;
    }

    protected int getIntValue(String line) throws HanituLoadingException{
        //noinspection EmptyCatchBlock
        try {
            return Integer.parseInt(getStrValue(line));
        } catch (NumberFormatException e){
            addWarning("not a integer number");
        } catch (NullPointerException e){

        }
        return 0;
    }

    protected int getNonNegIntValue(String line) throws HanituLoadingException{
        //noinspection EmptyCatchBlock
        try {
            int ret = Integer.parseInt(getStrValue(line));
            if (ret >= 0){
                return ret;
            }
            addWarning("negative number");
        } catch (NumberFormatException e){
            addWarning("not a integer number");
        } catch (NullPointerException e){
        }
        return 0;
    }

    protected double getFloatValue(String line) throws HanituLoadingException{
        //noinspection EmptyCatchBlock
        try {
            return Double.parseDouble(getStrValue(line));
        } catch (NumberFormatException e){
            addWarning("not a float number");
        } catch (NullPointerException e){
        }
        return 0.0;
    }

    protected boolean getBoolValue(String line) throws HanituLoadingException{
        //noinspection EmptyCatchBlock
        try {
            return Integer.parseInt(getStrValue(line)) != 0;
        } catch (NumberFormatException e){
            addWarning("not a integer number");
        } catch (NullPointerException e){
        }
        return false;
    }

    @SuppressWarnings("WeakerAccess")
    protected void addWarning(Throwable th) throws HanituLoadingException{
        LOG.warn("{} : {}", th.getClass().getSimpleName(), th.getMessage());
        if (LOG.isTraceEnabled()){
            traceMethodStack("exception stack", th.getStackTrace(), 0);
        }
        LOG.warn("source {}, line {}", sourcePath, lineNumber);
        LOG.warn("  {}", originLine);
        if (failOnLoad){
            throw new HanituLoadingException(this, th, "{} : {}", th.getClass().getSimpleName(), th.getMessage());
        }
    }

    @SuppressWarnings("WeakerAccess")
    protected void addWarning(String message, Object... objects) throws HanituLoadingException{
        LOG.warn(message, objects);
        if (LOG.isTraceEnabled()){
            traceMethodStack("method stack", Thread.currentThread().getStackTrace(), 2);
        }
        LOG.warn("source {}, line {}", sourcePath, lineNumber);
        LOG.warn("  {}", originLine);
        if (failOnLoad){
            throw new HanituLoadingException(this, message, objects);
        }
    }

    @SuppressWarnings("unused")
    protected void addError(Throwable th) throws HanituLoadingException{
        LOG.error("{} : {}", th.getClass().getSimpleName(), th.getMessage());
        if (LOG.isTraceEnabled()){
            traceMethodStack("exception stack", th.getStackTrace(), 0);
        }
        LOG.error("source {}, line {}", sourcePath, lineNumber);
        LOG.error("  {}", originLine);
        throw new HanituLoadingException(this, th, "{} : {}", th.getClass().getSimpleName(), th.getMessage());
    }

    protected void addError(String message, Object... objects) throws HanituLoadingException{
        LOG.error(message, objects);
        if (LOG.isTraceEnabled()){
            traceMethodStack("method stack", Thread.currentThread().getStackTrace(), 2);
        }
        LOG.error("source {}, line {}", sourcePath, lineNumber);
        LOG.error("  {}", originLine);
        throw new HanituLoadingException(this, message, objects);
    }

    private void traceMethodStack(String message, StackTraceElement[] trace, int start){
        LOG.trace("{} : ", message);
        for (int i = start, limit = Math.min(trace.length, 10); i < limit; i++){
            StackTraceElement st = trace[i];
            LOG.trace("  {}.{} ({}:{})",
                      st.getClassName(),
                      st.getMethodName(),
                      st.getFileName(),
                      st.getLineNumber());
        }
    }

    private static String getStateDescription(Class cls, int state){
        for (Class c = cls; c != null; c = c.getSuperclass()){
            for (Field f : c.getDeclaredFields()){
                //static final int STATE*
                if (f.getType() != int.class) continue;
                int m = f.getModifiers();
                if (!Modifier.isFinal(m) || !Modifier.isStatic(m)) continue;
                String name = f.getName();
                if (!name.startsWith("STATE")) continue;
                if (!Modifier.isPublic(m)){
                    f.setAccessible(true);
                }
                //noinspection EmptyCatchBlock
                try {
                    if (f.getInt(null) == state) return name;
                } catch (IllegalAccessException e){
                }
            }
        }
        return null;
    }

    private static class ReParseInterrupt extends RuntimeException{}

    public static class HanituLoadingException extends IOException{

        private final FileLoader loader;
        public final Path sourcePath;
        public final int lineNumber;
        public final String originLine;
        public final int currentState;
        public final List<Object> objects;

        private HanituLoadingException(FileLoader loader, Throwable cause, String message, Object... objects){
            super(MessageFormatter.arrayFormat(message, objects).getMessage(), cause);
            this.loader = loader;
            sourcePath = loader.sourcePath;
            lineNumber = loader.lineNumber;
            originLine = loader.originLine;
            currentState = loader.currentState;
            this.objects = Arrays.asList(objects);
        }

        private HanituLoadingException(FileLoader loader, String message, Object... objects){
            super(MessageFormatter.arrayFormat(message, objects).getMessage());
            this.loader = loader;
            sourcePath = loader.sourcePath;
            lineNumber = loader.lineNumber;
            originLine = loader.originLine;
            currentState = loader.currentState;
            this.objects = Arrays.asList(objects);
        }

        @Override
        public String getMessage(){
            return String.format("%s (%s:%d)", super.getMessage(), sourcePath.getFileName(), lineNumber);
        }
    }
}
