/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.util;

import java.util.Objects;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author antonio
 */
@SuppressWarnings("unused")
public class Strings{

    /**
     * use {@code regex} to search {@code input} and replace matched string with {@code map} which accept the content
     * of the match group 1. If {@code regex} doesn't have any match group, use matched string instead.
     *
     * @param regex regular expression.
     * @param input replaced string
     * @param map replace mapping function
     * @return replaced result
     * @throws PatternSyntaxException
     */
    public static String replace(String regex, String input, Function<String, String> map){
        Objects.requireNonNull(regex, "regular expression");
        if (regex.isEmpty()) return input;
        Matcher m = Pattern.compile(regex).matcher(input);
        if (!m.find()) return input;
        StringBuilder buffer = new StringBuilder();
        int start = 0;
        do {
            buffer.append(input.substring(start, m.start()));
            String rep;
            if (m.groupCount() == 1){
                rep = map.apply(m.group(1));
            } else {
                rep = map.apply(m.group());
            }
            buffer.append(rep);
            start = m.end();
        } while (m.find(start));
        buffer.append(input.substring(start));
        return buffer.toString();
    }

    /**
     * use {@code regex} to match {@code input} and return the match group 1.
     * @param regex regular expression.
     * @param input matched string
     * @return match group 1, null if match fail.
     */
    public static String match(String regex, String input){
        Matcher match = Pattern.compile(regex).matcher(input);
        if (match.matches()){
            if (match.groupCount() >= 1){
                return match.group(1);
            } else {
                return match.group();
            }
        }
        return null;
    }

    public static String match(String regex, String input, int group){
        Matcher match = Pattern.compile(regex).matcher(input);
        if (match.matches()){
            return match.group(group);
        }
        return null;
    }

    public static String match(String regex, String input, String group){
        Matcher match = Pattern.compile(regex).matcher(input);
        if (match.matches()){
            return match.group(group);
        }
        return null;
    }

    /**
     * delete matched string from buffer {@code input} with pattern {@code regex}.
     *
     * @param regex  regular expression.
     * @param input matched string
     * @return match group 1, null if match fail.
     */
    public static String delete(String regex, StringBuilder input){
        Matcher match = Pattern.compile(regex).matcher(input);
        if (match.find()){
            String ret;
            if (match.groupCount() > 0){
                ret = match.group(1);
            } else {
                ret = match.group();
            }
            input.delete(match.start(), match.end());
            return ret;
        }
        return null;
    }
}
