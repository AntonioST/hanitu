/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.util;

import java.util.List;
import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author antonio
 */
public class ErrorFunction{

    @FunctionalInterface
    public interface Action<E extends Throwable>{

        void run() throws E;
    }

// --Commented out by Inspection START (3/4/16 5:50 PM):
//    @FunctionalInterface
//    public interface ESupplier<T, E extends Throwable>{
//
//        T get() throws E;
//    }
// --Commented out by Inspection STOP (3/4/16 5:50 PM)

    @FunctionalInterface
    public interface EConsumer<T, E extends Throwable>{

        void accept(T t) throws E;
    }

// --Commented out by Inspection START (3/4/16 5:50 PM):
//    @FunctionalInterface
//    public interface EBiConsumer<T, U, E extends Throwable>{
//
//        void accept(T t, U u) throws E;
//    }
// --Commented out by Inspection STOP (3/4/16 5:50 PM)

    @FunctionalInterface
    public interface EFunction<T, R, E extends Throwable>{

        R accept(T t) throws E;
    }

// --Commented out by Inspection START (3/4/16 5:50 PM):
//    @FunctionalInterface
//    public interface EBiFunction<T, U, R, E extends Throwable>{
//
//        R accept(T t, U u) throws E;
//    }
// --Commented out by Inspection STOP (3/4/16 5:50 PM)

// --Commented out by Inspection START (3/4/16 5:50 PM):
//    @FunctionalInterface
//    public interface EPredicate<T, E extends Throwable>{
//
//        boolean accept(T t) throws E;
//    }
// --Commented out by Inspection STOP (3/4/16 5:50 PM)

    @FunctionalInterface
    public interface EBiPredicate<T, U, E extends Throwable>{

        boolean accept(T t, U u) throws E;
    }

    public static <T, E extends Throwable> void forEach(Stream<T> src, EConsumer<T, E> action) throws E{
        Objects.requireNonNull(action);
        List<E> errors = src.map(t -> {
            try {
                action.accept(t);
                return null;
            } catch (Throwable e){
                return (E)e;
            }
        }).filter(e -> e != null)
          .collect(Collectors.toList());
        if (!errors.isEmpty()){
            E e = errors.get(0);
            for (int i = 1, size = errors.size(); i < size; i++){
                e.addSuppressed(errors.get(i));
            }
            throw e;
        }
    }

// --Commented out by Inspection START (3/4/16 5:50 PM):
//    public static <T, R, E extends Throwable> Function<T, R> warpError(EFunction<T, R, E> function, R nil){
//        return t -> {
//            try {
//                return function.accept(t);
//            } catch (Throwable e){
//                return nil;
//            }
//        };
//    }
// --Commented out by Inspection STOP (3/4/16 5:50 PM)

// --Commented out by Inspection START (3/4/16 5:51 PM):
//    public static <T, U, R, E extends Throwable> BiFunction<T, U, R> warpError(EBiFunction<T, U, R, E> function, R nil){
//        return (t, u) -> {
//            try {
//                return function.accept(t, u);
//            } catch (Throwable e){
//                return nil;
//            }
//        };
//    }
// --Commented out by Inspection STOP (3/4/16 5:51 PM)

// --Commented out by Inspection START (3/4/16 5:51 PM):
//    public static <T, E extends Throwable> Predicate<T> warpError(EPredicate<T, E> function){
//        return t -> {
//            try {
//                return function.accept(t);
//            } catch (Throwable e){
//                return false;
//            }
//        };
//    }
// --Commented out by Inspection STOP (3/4/16 5:51 PM)

    public static <T, U, E extends Throwable> BiPredicate<T, U> warpError(EBiPredicate<T, U, E> function){
        return (t, u) -> {
            try {
                return function.accept(t, u);
            } catch (Throwable e){
                return false;
            }
        };
    }
}
