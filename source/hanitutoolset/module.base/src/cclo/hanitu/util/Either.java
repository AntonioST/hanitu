/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.util;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * @author antonio
 */
public abstract class Either<T, U>{

    private Either(){
    }

    public static <L, R> Either<L, R> of(L success, R failure){
        return success != null? new Left<>(success): new Right<>(failure);
    }

    public static <L, R> Either<L, R> of(boolean test, L success, R failure){
        return test? new Left<>(success): new Right<>(failure);
    }

    public static <L, R> Either<L, R> success(L value){
        return new Left<>(value);
    }

    public static <L, R> Either<L, R> failure(R value){
        return new Right<>(value);
    }

    public boolean isSuccess(){
        return false;
    }

    public boolean isFailure(){
        return false;
    }

    public void ifSuccess(Consumer<T> consumer){
    }

    public void ifFailure(Consumer<U> consumer){
    }

    public abstract <T> T get();

    public static class Left<L, R> extends Either<L, R>{

        final L value;

        private Left(L value){
            this.value = Objects.requireNonNull(value);
        }

        @Override
        public boolean isSuccess(){
            return true;
        }

        @Override
        public void ifSuccess(Consumer<L> consumer){
            consumer.accept(value);
        }

        @Override
        public L get(){
            return value;
        }

        @Override
        public boolean equals(Object o){
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Left<?, ?> left = (Left<?, ?>)o;

            return value.equals(left.value);

        }

        @Override
        public int hashCode(){
            return value.hashCode();
        }
    }

    public static class Right<L, R> extends Either<L, R>{

        final R value;

        private Right(R value){
            this.value = Objects.requireNonNull(value);
        }

        @Override
        public boolean isFailure(){
            return true;
        }

        @Override
        public void ifFailure(Consumer<R> consumer){
            consumer.accept(value);
        }

        @Override
        public R get(){
            return value;
        }

        @Override
        public boolean equals(Object o){
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Right<?, ?> right = (Right<?, ?>)o;

            return value.equals(right.value);

        }

        @Override
        public int hashCode(){
            return value.hashCode();
        }
    }
}
