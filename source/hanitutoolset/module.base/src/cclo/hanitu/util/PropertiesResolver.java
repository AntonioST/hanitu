/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * ### searching path
 *
 * 1. META-INF/ *path* .property (in classpath)
 * 2. ./ *path* .property (in classpath)
 * 3. *path* (in classpath)
 * 4. *path*
 *
 * ### property **inherit**
 *
 * If properties file contain property **inherit**, this function also loading *inherited* properties file and
 * overwrite its content with current properties.
 *
 * ### property reference
 *
 * Use the pattern `${property}` to reference a property in current properties file or inherited. It will eval
 * and be replaced with the value of the referenced property when loaded.
 *
 * Use the pattern `${namespase::property}` to reference a property in another properties file. Load the properties
 * file with path *namespase*.
 * @author antonio
 */
public final class PropertiesResolver{

    private static final Logger LOG = LoggerFactory.getLogger(PropertiesResolver.class);
    // ${(DOMAIN::)?PROPERTY}
    public static final Pattern REF = Pattern.compile("(\\$+)\\{(([a-zA-Z0-9_.]+::)?[a-zA-Z0-9_.]+?)}");
    /**
     * the property used for loading other property which its properties are resolved.
     *
     * only one property allowed.
     */
    private static final String INHERIT = "inherit";
    /**
     * the property used for loading other property
     *
     * can allow many property, use space char to spread each other.
     */
    private static final String INCLUDE = "include";

    private static final Map<String, Properties> resolvedPropertiesCache = new HashMap<>();

    private PropertiesResolver(){
        throw new RuntimeException();
    }

    public static Properties loadProperties(String path){
        if (path == null){
            return new Properties();
        }
        synchronized (resolvedPropertiesCache){
            // resolvedPropertiesCache
            // key not found            -> properties not load
            // key found but null value -> properties is loading
            // key found                -> properties loaded
            if (resolvedPropertiesCache.containsKey(path)){
                Properties ret = resolvedPropertiesCache.get(path);
                if (ret == null){
                    throw new RuntimeException("recursive properties inherit");
                }
                return ret;
            }
            LOG.trace("loading {}", path);
            resolvedPropertiesCache.put(path, null);
            Properties properties = loadPropertiesDirectly(path);
            try {
                if (properties.containsKey(INHERIT)){
                    LOG.trace("{} : {}", INHERIT, properties.getProperty(INHERIT));
                }
                Properties inherit = loadProperties((String)properties.remove(INHERIT));
                resolveInclude(properties);
                //avoid parent properties be overwrite
                Properties ret = new Properties();
                ret.putAll(inherit);
                resolveProperties(ret, properties);
                resolvedPropertiesCache.put(path, ret);
                LOG.trace("loaded {}", path);
                return ret;
            } catch (RuntimeException e){
                throw new RuntimeException("loading : " + path, e);
            }
        }
    }

// --Commented out by Inspection START (3/4/16 6:06 PM):
//    public static Properties extendProperties(Properties parent, String path){
//        Properties properties = loadPropertiesDirectly(path);
//        properties.remove(INHERIT);
//        resolveInclude(properties);
//        Properties ret = new Properties();
//        ret.putAll(parent);
//        return resolveProperties(ret, properties);
//    }
// --Commented out by Inspection STOP (3/4/16 6:06 PM)

    private static Properties loadPropertiesDirectly(String path){
        Properties properties = new Properties();
        try {
            properties.load(ClassLoader.getSystemResourceAsStream("META-INF/" + path + ".properties"));
            LOG.debug("load META-INF/{}.properties", path);
        } catch (IOException | NullPointerException e){
            try {
                properties.load(ClassLoader.getSystemResourceAsStream(path + ".properties"));
                LOG.debug("load {}.properties", path);
            } catch (IOException | NullPointerException e1){
                try {
                    properties.load(ClassLoader.getSystemResourceAsStream(path));
                    LOG.debug("load (in classpath) : {}", path);
                } catch (IOException | NullPointerException e2){
                    try (InputStream is = Files.newInputStream(Paths.get(path))) {
                        properties.load(is);
                        LOG.debug("load {}", path);
                    } catch (IOException e3){
                        e.addSuppressed(e1);
                        e.addSuppressed(e2);
                        e.addSuppressed(e3);
                        throw new RuntimeException("load properties fail : " + path, e);
                    }
                }
            }
        }
        return properties;
    }

    private static void resolveInclude(Properties properties){
        String includes = (String)properties.remove(INCLUDE);
        if (includes != null){
            for (String include : includes.split(" +")){
                LOG.trace("{} : {}", INCLUDE, include);
                Properties p = loadPropertiesDirectly(include);
                p.forEach(properties::putIfAbsent);
            }
        }
    }

    private static void resolveProperties(Properties override, Properties current){
        LOG.trace("resolve properties");
        List<String> it = current.stringPropertyNames().stream()
          .filter(key -> {
              String value = current.getProperty(key);
              if (!value.contains("$")){
                  LOG.trace("+ {} = {}", key, value);
                  override.put(key, value);
                  return false;
              }
              return true;
          }).collect(Collectors.toList());
        int size = it.size();
        while (!it.isEmpty()){
            it.removeIf(k -> resolveProperty(override, k, current.getProperty(k)));
            if (it.size() == size) throw new RuntimeException("un-resolve reference : " + it);
            size = it.size();
        }
    }

    private static boolean resolveProperty(Properties override, String key, String value){
        LOG.trace("! {} = {}", key, value);
        Matcher m = REF.matcher(value);
        if (m.find()){
            StringBuilder buffer = new StringBuilder();
            int index = 0;
            do {
                buffer.append(value.substring(index, m.start()));
                String refOrigin = m.group();
                String refSymbol = m.group(1);
                if (refSymbol.length() != 1){
                    // $$$ -> $$
                    buffer.append(refOrigin.substring(1));
                } else {
                    String ref = m.group(2);
                    if (ref.contains("::")){
                        int i = ref.indexOf("::");
                        buffer.append(loadProperties(ref.substring(0, i).replaceAll("\\.", "/")) //domain
                                        .getProperty(ref.substring(i + 2), //property
                                                     refOrigin)); // on failure
                    } else if (override.containsKey(ref)){
                        buffer.append(override.getProperty(ref));
                    } else {
                        // resolve fail, drop all
                        return false;
                    }
                }
                index = m.end();
            } while (m.find(index));
            buffer.append(value.substring(index));
            value = buffer.toString();
        }
        LOG.trace("+ {} = {}", key, value);
        override.put(key, value);
        return true;
    }
}
