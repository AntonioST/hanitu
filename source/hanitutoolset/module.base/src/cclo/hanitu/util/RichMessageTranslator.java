/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.util;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cclo.hanitu.Base;

/**
 *
 * @author antonio
 */
public class RichMessageTranslator{

    private static final boolean COLORLESS = Base.getBooleanProperty("cclo.hanitu.colorless", false);

    public static String echo(String line){
        return COLORLESS? removeExpression(line): bashStyleExpression(line);
    }

    // ~~
    // ~}
    // ~...{
    private static final String EXPRESSION_REGEX = "~?~(}|(?<S>.*?)\\{)";
    private static final Pattern EXPRESSION_PATTERN = Pattern.compile(EXPRESSION_REGEX);

    private static final int WEIGHT_LIGHT = 0;
    private static final int WEIGHT_THIN = 1;
    private static final int WEIGHT_NORMAL = 2;
    private static final int WEIGHT_BOLD = 3;
    private static final int WEIGHT_BLACK = 4;

    private static final int STYLE_NORMAL = 0;
    private static final int STYLE_ITALIC = 1;

    private static final Builder EMPTY = new Builder();

    public static Builder parse(String line){
        if (line == null) return EMPTY;
        if (line.isEmpty()) return new Builder(new Unit("", null));
        Builder ret = new Builder();
        Matcher match = EXPRESSION_PATTERN.matcher(line);
        if (match.find()){
            int offset = 0;
            Unit unit = new Unit("", null);
            do {
                int start = match.start();
                if (start != offset){
                    ret.append(line.substring(offset, start), unit);
                }
                String group = match.group();
                if (group.startsWith("~~")){
                    ret.append(group.substring(1), unit);
                } else if (group.equals("~}")){
                    unit = new Unit("", null);
                } else {
                    unit.setFormat(match.group("S"));
                }
            } while (match.find(offset = match.end()));
            if (offset != line.length()){
                ret.append(line.substring(offset), unit);
            }
        } else {
            ret.append(line, null);
        }
        return ret;
    }


    @SuppressWarnings("unused")
    public static String mapForegroundColor(String color){
        switch (color){
        case "":
        case "w":
        case "k":
            return "\033[1m";
        case "r":
            return "\033[1;31m";
        case "g":
            return "\033[1;32m";
        case "y":
            return "\033[1;33m";
        case "b":
            return "\033[1;34m";
        case "m":
            return "\033[1;35m";
        case "c":
            return "\033[1;36m";
        }
        return null;
    }

    @SuppressWarnings("unused")
    public static String mapBackgroundColor(String color){
        switch (color){
        case "":
        case "w":
        case "k":
            return "\033[1m";
        case "r":
            return "\033[1;41m";
        case "g":
            return "\033[1;42m";
        case "y":
            return "\033[1;43m";
        case "b":
            return "\033[1;44m";
        case "m":
            return "\033[1;45m";
        case "c":
            return "\033[1;46m";
        }
        return null;
    }

    @SuppressWarnings("WeakerAccess")
    public static String mapBachStyle(Unit unit){
        if (unit.normal) return unit.text;
        List<String> ls = new LinkedList<>();
        if (unit.weight > WEIGHT_NORMAL){
            ls.add("1");
        }
        if (unit.strikeThrough){
            ls.add("2");
        }
        if (unit.underLine){
            ls.add("4");
        }
        switch (unit.foregroundColor){
        case "":
            ls.add("39");
            break;
        case "w":
            ls.add("97");
            break;
        case "k":
            ls.add("30");
            break;
        case "r":
            ls.add("31");
            break;
        case "g":
            ls.add("32");
            break;
        case "y":
            ls.add("33");
            break;
        case "b":
            ls.add("34");
            break;
        case "m":
            ls.add("35");
            break;
        case "c":
            ls.add("36");
            break;
        }
        switch (unit.backgroundColor){
        case "":
            ls.add("49");
            break;
        case "w":
            ls.add("107");
            break;
        case "k":
            ls.add("40");
            break;
        case "r":
            ls.add("41");
            break;
        case "g":
            ls.add("42");
            break;
        case "y":
            ls.add("43");
            break;
        case "b":
            ls.add("44");
            break;
        case "m":
            ls.add("45");
            break;
        case "c":
            ls.add("46");
            break;
        }
        return "\033[" + String.join(";", ls) + "m" + unit.text + "\033[m";
    }

    @SuppressWarnings("WeakerAccess")
    public static String bashStyleExpression(String line){
        return parse(line).toString();
    }

    @SuppressWarnings("WeakerAccess")
    public static String removeExpression(String line){
        return EXPRESSION_PATTERN.matcher(line).replaceAll("");
    }

    @SuppressWarnings("unused")
    public static String unescape(String line){
        return line.replaceAll("\\033\\[.*?m", "");
    }

    public static boolean containExpression(String line){
        return EXPRESSION_PATTERN.matcher(line).find();
    }

    public static class Unit{

        public final String text;
        public boolean normal = true;
        public String foregroundColor = "";
        public String backgroundColor = "";
        public String font = "";
        public int weight = WEIGHT_NORMAL;
        public int style = STYLE_NORMAL;
        public int size = -1;
        public boolean underLine;

        public boolean strikeThrough;

        private Unit(String text, Unit copy){
            this.text = text;
            if (copy != null){
                normal = copy.normal;
                foregroundColor = Objects.requireNonNull(copy.foregroundColor);
                backgroundColor = Objects.requireNonNull(copy.backgroundColor);
                font = Objects.requireNonNull(copy.font);
                weight = copy.weight;
                style = copy.style;
                size = copy.size;
                underLine = copy.underLine;
                strikeThrough = copy.strikeThrough;
            }
        }

        private void setFormat(String format){
            if (format.isEmpty()) return;
            normal = false;
            StringBuilder builder = new StringBuilder(format);
            String m;
            if ((m = Strings.delete("<(.*)>", builder)) != null){
                font = m;
            }
            if ((m = Strings.delete("\\d+", builder)) != null){
                size = Integer.parseInt(m);
            }
            if ((m = Strings.delete("[*]+", builder)) != null){
                weight = m.length() == 1? WEIGHT_BOLD: WEIGHT_BLACK;
            } else if ((m = Strings.delete("[!]+", builder)) != null){
                weight = m.length() == 1? WEIGHT_THIN: WEIGHT_LIGHT;
            }
            if ((m = Strings.delete("[rgybmckw]", builder)) != null){
                foregroundColor = m;
            }
            if ((m = Strings.delete("[RGYBMCKW]", builder)) != null){
                backgroundColor = m.toLowerCase();
            }
            if (Strings.delete("/", builder) != null){
                style = STYLE_ITALIC;
            }
            if (Strings.delete("-", builder) != null){
                strikeThrough = true;
            }
            if (Strings.delete("_", builder) != null){
                underLine = true;
            }
            if (builder.length() != 0){
                throw new IllegalArgumentException("unknown rich text format : " + builder);
            }
        }

        @Override
        public String toString(){
            if (normal){
                return text;
            } else {
                StringBuilder sb = new StringBuilder("~");
                if (!font.isEmpty()){
                    sb.append("<").append(font).append(">");
                }
                if (size >= 0){
                    sb.append(size);
                }
                if (weight != WEIGHT_NORMAL){
                    if (weight == WEIGHT_LIGHT){
                        sb.append("!**");
                    } else if (weight == WEIGHT_THIN){
                        sb.append("!*");
                    } else if (weight == WEIGHT_BOLD){
                        sb.append("*");
                    } else if (weight == WEIGHT_BLACK){
                        sb.append("**");
                    }
                }
                if (style != STYLE_NORMAL){
                    if (style == STYLE_ITALIC){
                        sb.append("/");
                    }
                }
                if (!foregroundColor.isEmpty()){
                    sb.append(foregroundColor.toLowerCase());
                }
                if (!backgroundColor.isEmpty()){
                    sb.append(backgroundColor.toUpperCase());
                }
                if (underLine){
                    sb.append("_");
                }
                if (strikeThrough){
                    sb.append("-");
                }
                sb.append("{").append(text).append("~}");
                return sb.toString();
            }
        }

    }

    public static class Builder{

        private final List<Unit> list;

        Builder(){
            list = new ArrayList<>();
        }

        Builder(Unit... units){
            list = new ArrayList<>(Arrays.asList(units));
        }

        @SuppressWarnings("unused")
        private void append(Unit unit){
            list.add(unit);
        }

        private void append(String text, Unit ref){
            list.add(new Unit(text, ref));
        }

        @SuppressWarnings("unused")
        public int size(){
            return list.size();
        }

        @SuppressWarnings("unused")
        public Unit getUnits(int index){
            return list.get(index);
        }

        public <T> Stream<T> map(Function<Unit, T> f){
            return list.stream().map(f);
        }

        @Override
        public String toString(){
            return list.stream().map(RichMessageTranslator::mapBachStyle).collect(Collectors.joining(""));
        }
    }
}
