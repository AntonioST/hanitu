/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * @author antonio
 */
public abstract class Events<T>{

    private final List<T> event = new LinkedList<>();

    public void add(T t){
        event.add(Objects.requireNonNull(t));
    }

// --Commented out by Inspection START (3/4/16 5:52 PM):
//    public boolean isEmpty(){
//        return event.isEmpty();
//    }
// --Commented out by Inspection STOP (3/4/16 5:52 PM)

    @SuppressWarnings("unused")
    public void clear(){
        event.clear();
    }

    private void forEach(Consumer<T> c){
        event.forEach(c);
    }

    public static class RunnableEvent extends Events<Runnable> implements Runnable{

        @Override
        public void run(){
            super.forEach(Runnable::run);
        }
    }

//    public static class ConsumerEvent<P> extends Events<Consumer<P>> implements Consumer<P>{
//
//        @Override
//        public void accept(P p){
//            super.forEach(c -> c.accept(p));
//        }
//    }
//
//    public static class BiConsumerEvent<P, Q> extends Events<BiConsumer<P, Q>> implements BiConsumer<P, Q>{
//
//        @Override
//        public void accept(P p, Q q){
//            super.forEach(c -> c.accept(p, q));
//        }
//    }
}
