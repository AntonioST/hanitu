/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author antonio
 */
public class Maps{

    private Maps(){
        throw new RuntimeException();
    }

// --Commented out by Inspection START (3/4/16 6:02 PM):
//    public static <K, V> Map<K, V> mapOf(Class<K> keyClass, Class<V> valueClass, Object... content){
//        Objects.requireNonNull(keyClass);
//        Objects.requireNonNull(valueClass);
//        int length = content.length;
//        if (length % 2 != 0) throw new IllegalArgumentException("lost value");
//        Map<K, V> map = new HashMap<>();
//        for (int i = 0; i < length; i += 2){
//            map.put(keyClass.cast(content[i]), valueClass.cast(content[i + 1]));
//        }
//        return map;
//    }
// --Commented out by Inspection STOP (3/4/16 6:02 PM)

// --Commented out by Inspection START (3/4/16 6:02 PM):
//    public static <V> Map<String, V> mapOf(Class<V> valueClass, String firstKey, Object... content){
//        Objects.requireNonNull(firstKey);
//        Objects.requireNonNull(valueClass);
//        int length = content.length;
//        if (length % 2 != 1) throw new IllegalArgumentException("lost value");
//        Map<String, V> map = new HashMap<>();
//        map.put(firstKey, valueClass.cast(content[0]));
//        for (int i = 1; i < length; i += 2){
//            map.put((String)content[i], valueClass.cast(content[i + 1]));
//        }
//        return map;
//    }
// --Commented out by Inspection STOP (3/4/16 6:02 PM)

    public static <V> Map<String, Class<V>> mapOfClass(Class<V> valueClass, String firstKey, Object... content){
        Objects.requireNonNull(firstKey);
        Objects.requireNonNull(valueClass);
        int length = content.length;
        if (length % 2 != 1) throw new IllegalArgumentException("lost value");
        Map<String, Class<V>> map = new HashMap<>();
        if (!(content[0] instanceof Class)){
            throw new ClassCastException("not a class : " + content[0]);
        }
        Class vc = (Class)content[0];
        if (!valueClass.isAssignableFrom(vc)){
            throw new ClassCastException("not a sub class of " + valueClass + " : " + vc);
        }
        map.put(firstKey, vc);
        for (int i = 1; i < length; i += 2){
            if (!(content[i + 1] instanceof Class)){
                throw new ClassCastException("not a class : " + content[i + 1]);
            }
            vc = (Class)content[i + 1];
            if (!valueClass.isAssignableFrom(vc)){
                throw new ClassCastException("not a sub class of " + valueClass + " : " + vc);
            }
            map.put((String)content[i], vc);
        }
        return map;
    }
}
