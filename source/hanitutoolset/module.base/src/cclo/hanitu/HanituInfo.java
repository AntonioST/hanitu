/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * the static, global information of the Hanitu Tool Set. And defined information annotation.
 *
 * @author antonio
 */
public class HanituInfo{

    public static final String HANITU_TOOL_SET_NAME = Base.getProperty("cclo.hanitu.name.toolset", "Hanitu Tool Set");

    /**
     * actually program version. loading from jar resource.
     */
    public static final String VERSION = Base.getInternalProperty("cclo.hanitu.version");
    public static final String TOOLSET_VERSION = Base.getInternalProperty("cclo.hanitu.version.toolset");

    /**
     * version description.
     */
    public static final String VERSION_DESCRIPTION = HANITU_TOOL_SET_NAME + " " + VERSION;

    /**
     * web-site information
     */
    public static final String[][] WEB_SITE;

    static{
        String[][] tmp;
        try (InputStream is = ClassLoader.getSystemResourceAsStream("META-INF/cclo.hanitu.website")) {
            Properties properties = new Properties();
            properties.load(is);
            int count = Integer.parseInt(properties.getProperty("website.count", "0"));
            tmp = new String[count][2];
            for (int i = 0; i < count; i++){
                tmp[i][0] = properties.getProperty("website." + i + ".name");
                tmp[i][1] = properties.getProperty("website." + i + ".url");
            }
        } catch (IOException | NullPointerException e){
            tmp = new String[0][2];
        }
        WEB_SITE = tmp;
    }

    /**
     * authors information
     */
    public static final String[][] AUTHORS;

    static{
        String[][] tmp;
        try (InputStream is = ClassLoader.getSystemResourceAsStream("META-INF/cclo.hanitu.author")) {
            Properties properties = new Properties();
            properties.load(is);
            int count = Integer.parseInt(properties.getProperty("author.count", "0"));
            tmp = new String[count][2];
            for (int i = 0; i < count; i++){
                tmp[i][0] = properties.getProperty("author." + i + ".name");
                tmp[i][1] = properties.getProperty("author." + i + ".mail");
            }
        } catch (IOException | NullPointerException e){
            tmp = new String[0][2];
        }
        AUTHORS = tmp;
    }

    /**
     * license name
     */
    public static final String LICENSE = "GPLv3";
    /**
     * development group description
     */
    public static final String DEVELOPER_DESCRIPTION
      = "This application is developed by CCLo lab, National Tsing Hua University, Taiwan.";
}
