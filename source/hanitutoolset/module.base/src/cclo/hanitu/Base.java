/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.nio.file.Paths;
import java.util.Properties;
import java.util.regex.Matcher;

import cclo.hanitu.util.PropertiesResolver;

/**
 * @author antonio
 */
@SuppressWarnings("unused")
public class Base{

    // get the jar file name in runtime
    public static final String HANITU_JAR = Base.class.getProtectionDomain().getCodeSource().getLocation().getPath();

    public static final String HOME_PROPERTY = "cclo.hanitu.home";
    public static final String HOME_ENVIRONMENT = "HANITU_HOME";
    public static final String HOME;

    static{
        String home = System.getProperty(HOME_PROPERTY);
        if (home != null){
            home = Paths.get(home).toAbsolutePath().toString();
        }
        if (home == null && System.getenv(HOME_ENVIRONMENT) != null){
            home = Paths.get(System.getenv(HOME_ENVIRONMENT)).toAbsolutePath().toString();
        }
        if (home == null){
            home = Paths.get(HANITU_JAR).getParent().toString();
        }
        HOME = home;
    }

    public static final String PRG_PROPERTY = "cclo.hanitu.0";
    public static final String PRG; // hanitu_?

    static{
        String tmp = System.getProperty(PRG_PROPERTY);
        PRG = tmp == null? null: tmp.replaceAll(".*/", "").intern();
    }

    private static final String DEFAULT_PROPERTY_FILE = System.getProperty("cclo.hanitu.property", "default");
    private static final Properties defaultProperties;

    static{
        Properties p;
        try {
            p = loadProperties(DEFAULT_PROPERTY_FILE);
        } catch (RuntimeException e){
            e.printStackTrace();
            p = new Properties();
        }
        defaultProperties = p;
    }

    public static Properties loadProperties(){
        StackTraceElement stack = Thread.currentThread().getStackTrace()[2];
        return PropertiesResolver.loadProperties(stack.getClassName());
    }

    public static Properties loadProperties(Class cls){
        return PropertiesResolver.loadProperties(cls.getName());
    }

    public static Properties loadProperties(String pathProperty){
        return PropertiesResolver.loadProperties(pathProperty);
    }

    public static String getReference(String ref){
        Matcher m = PropertiesResolver.REF.matcher(ref);
        if (!m.matches()) throw new IllegalArgumentException();
        String g1 = m.group(1);
        if (g1.length() != 1) return ref.substring(1);
        String g2 = m.group(2);
        if (g2.contains("::")){
            int i = g2.indexOf("::");
            return loadProperties(g2.substring(0, i).replaceAll("\\.", "/"))
              .getProperty(g2.substring(i + 2));
        } else {
            return getProperty(g2);
        }
    }

    public static boolean hasProperties(String key){
        return defaultProperties.contains(key) || System.getProperties().contains(key);
    }

    public static String getProperty(String key){
        Properties properties = System.getProperties();
        if (properties.containsKey(key)){
            return properties.getProperty(key);
        } else {
            return defaultProperties.getProperty(key);
        }
    }

    public static String getProperty(String key, String def){
        Properties properties = System.getProperties();
        if (properties.containsKey(key)){
            return properties.getProperty(key);
        } else {
            return defaultProperties.getProperty(key, def);
        }
    }

    public static String getInternalProperty(String key){
        return defaultProperties.getProperty(key);
    }

    public static String getInternalProperty(String key, String def){
        return defaultProperties.getProperty(key, def);
    }

    public static String getAndSetProperty(String key, String def){
        Properties properties = System.getProperties();
        if (properties.containsKey(key)){
            return properties.getProperty(key);
        } else if (defaultProperties.containsKey(key)){
            return defaultProperties.getProperty(key);
        } else {
            defaultProperties.put(key, def);
            return def;
        }
    }

    public static int getIntegerProperty(String key, int def){
        try {
            return Integer.parseInt(getProperty(key));
        } catch (NumberFormatException | NullPointerException e){
            return def;
        }
    }

    public static boolean getBooleanProperty(String key, boolean def){
        try {
            return Boolean.parseBoolean(getProperty(key));
        } catch (NumberFormatException | NullPointerException e){
            return def;
        }
    }

    public static void setProperty(String key, String value){
        defaultProperties.put(key, value);
    }

    public static void setProperty(String key, int value){
        defaultProperties.put(key, Integer.toString(value));
    }

    public static void setProperty(String key, boolean value){
        defaultProperties.put(key, Boolean.toString(value));
    }

    public static void appendProperties(Properties p){
        defaultProperties.putAll(p);
    }

    public static boolean osWin(){
        return System.getProperty("os.name").toLowerCase().contains("win");
    }

    public static boolean osLinux(){
        String name = System.getProperty("os.name").toLowerCase();
        return name.contains("mux") || name.contains("nix");
    }

    public static boolean osMac(){
        return System.getProperty("os.name").toLowerCase().contains("mac");
    }
}
