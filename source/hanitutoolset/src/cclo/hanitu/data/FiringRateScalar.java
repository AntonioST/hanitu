/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * @author antonio
 */
@FunctionalInterface
public interface FiringRateScalar{

    /**
     * @param shift
     * @return the weight
     */
    double rate(double shift);

    default double rate(double timeDifference, double timeWindow){
        if (Math.abs(timeDifference) > timeWindow / 2) return 0;
        return rate(timeDifference / timeWindow);
    }

    default double rate( double currentTime, double dataTime, double timeWindow){
        if (Math.abs(dataTime - currentTime) > timeWindow / 2) return 0;
        return rate((dataTime - currentTime) / timeWindow);
    }

    /*
    1 |      __
      |     /  \
      |    /    \
      |   /      \
      | --        --
    t |      ^
     */
    static FiringRateScalar normal(double std){
        double std2 = std * std * 2;
        return shift -> Math.exp(-shift * shift / std2);
    }

    static FiringRateScalar normal(double std, double timeWindow){
        return normal(std / timeWindow);
    }

    /*
    1 | ----------------
    0 |
    t |        ^
     */
    static FiringRateScalar uniform(){
        return shift -> 1;
    }

    /*
    1 | -------
      |       |
    0 |        --------
    t |       ^
     */
    static FiringRateScalar half(){
        return shift -> shift <= 0? 1: 0;
    }

    /*
    1 |      /
      |     / |
      |    /  |
      |   /   |
      | --    ------
    t |      ^
     */
    static FiringRateScalar expDecay(double half){
        double rate = Math.log(2) / half;
        return shift -> {
            if (shift == 0) return 1;
            if (shift > 0) return 0;
            return Math.exp(rate * shift);
        };
    }

    static FiringRateScalar weight(double... weight){
        int length = weight.length;
        return shift -> {
            if (Math.abs(shift) >= 1) return 0;
            double p = (shift + 1) * length / 2;
            int i = (int)p;
            p -= i;
            return weight[i] * (1 - p) + weight[i + 1] * p;
        };
    }

    static FiringRateScalar weight(String filename) throws IOException{
        try (Stream<String> lines = Files.lines(Paths.get(filename))) {
            return weight(lines.mapToDouble(Double::parseDouble).toArray());
        }
    }

}
