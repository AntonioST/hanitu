/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import cclo.hanitu.world.WormConfig;

import java.io.Serializable;

/**
 * the data class for storing the worm information during plotting.
 *
 * @author antonio
 */
public class WormData extends PositionData implements Serializable{

    /**
     * user identify number
     */
    public int user;
    /**
     * worm identify number
     */
    public int worm;
    /**
     * the health point of the worm
     */
    public double hp = 100.0;
    /**
     * the size of the worm's body, in unit 0.1mm
     */
    public int size;

    /**
     * create a empty worm data
     */
    public WormData(){
    }

    /**
     * create  with copy data from reference
     *
     * @param w reference
     */
    public WormData(WormData w){
        super(w);
        user = w.user;
        worm = w.worm;
        hp = w.hp;
        size = w.size;
    }

    /**
     * create with setting value from configuration
     *
     * @param c configuration
     */
    public WormData(WormConfig c){
        user = c.user;
        worm = c.worm;
        size = c.size;
        x = c.x;
        y = c.y;
        size = c.size;
    }

    /**
     * Does this worm contact with  another worm?
     *
     * @param other another worm
     * @return
     */
    public boolean contact(WormData other){
        return contact(other.x, other.y, size + other.size);
    }

    /**
     * Does this worm contact with a point?
     *
     * @param data point data
     * @return
     */
    public boolean contact(PositionData data){
        return contact(data.x, data.y, size);
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WormData wormData = (WormData)o;

        if (worm != wormData.worm) return false;
        if (user != wormData.user) return false;

        return true;
    }

    @Override
    public int hashCode(){
        int result;
        result = user;
        result = 31 * result + worm;
        return result;
    }

    @Override
    public String toString(){
        return String.format("worm[u=%d,%d](x=%.2f, y=%.2f, hp=%.2f)", user, worm, x, y, hp);
    }
}
