/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import cclo.hanitu.HanituInfo;
import cclo.hanitu.Executable;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static cclo.hanitu.HelpBuilder.*;

/**
 * @author antonio
 */
@HanituInfo.Tip("Concentration Distribution")
@HanituInfo.Description("The tool to calculate the concentration distribution of the single place molecule.")
public class Concentration extends Executable{

    @HanituInfo.Option(shortName = 'd', value = "diffusion", arg = "VALUE")
    @HanituInfo.Description("Environment argument. default is 1.86e-5 (cm^2/sec)")
    public double diffusion = 1.86e-5;

    @HanituInfo.Option(shortName = 'p', value = "depth", arg = "VALUE")
    @HanituInfo.Description("Environment argument. default is 0.264 (cm)")
    public double depth = 0.264;

    @HanituInfo.Option(shortName = 't', value = "delaytime", arg = "TIME")
    @HanituInfo.Description("Environment argument. default is 0 (+1e-5) (sec)")
    public double delayTime = 0;

    @HanituInfo.Option(shortName = 'c', value = "concentration", arg = "VALUE")
    @HanituInfo.Description("Environment argument. default is 100 (mM)")
    public double concentration = 100;

    @HanituInfo.Option(shortName = 'C', value = "count", arg = "VALUE")
    @HanituInfo.Description("Environment argument. default is 1 (uL)")
    public int count = 1;

    @HanituInfo.Option(value = "start", arg = "VALUE", order = 0)
    @HanituInfo.Description("The start distance. default is 0 (mm)")
    public double start = 0.0;

    @HanituInfo.Option(value = "end", arg = "VALUE", order = 0)
    @HanituInfo.Description("The end distance. default is 200 (mm)")
    public double end = 200.0;

    @HanituInfo.Option(value = "inc", arg = "VALUE", order = 0)
    @HanituInfo.Description("The amount of increasing distance. default is 1 (mm)")
    public double inc = 1.0;

    @HanituInfo.Option(value = "no-header", order = 1)
    @HanituInfo.Description("Don't output header information.")
    public boolean headerInformation = true;

    @HanituInfo.Argument(index = 0, value = "FILE")
    @HanituInfo.Description("Parameter Setting file. It ~_{FILE~} is given, it will discard all setting in options.")
    public String inputFileName = null;
    //
    public Distribution distribution;

    @Override
    public String extendHelpDoc(){
        return
          DOC_TITLE + "FILE Format" +
          DOC_BLOCK_START +
          "If ~_{FILE~} is given, program will ignore all the setting in command line." +
          DOC_NEWLINE +
          "every setting should be match this pattern KEY=VALUE. you can add any command which " +
          "start with //. Otherwise, program will throw error when parsing file." +
          DOC_NEWLINE +
          "the list of KEY:" +
          DOC_LIST_0 + "Diffusion" +
          DOC_LIST_0 + "Depth" +
          DOC_LIST_0 + "Delaytime" +
          DOC_LIST_0 + "Concentration" +
          DOC_LIST_0 + "Count" +
          DOC_LIST_0 + "Time" +
          DOC_BLOCK_END;
    }

    public Distribution setupDistribution(){
        if (distribution == null){
            distribution = new Distribution();
            if (inputFileName != null){
                setByFile(distribution, inputFileName);
            } else {
                distribution.setDiffusion(diffusion);
                distribution.setDepth(depth);
                distribution.setDelayTime(delayTime);
                distribution.setConcentration(concentration);
                distribution.setCount(count);
            }
        }
        return distribution;
    }

    @Override
    public void start() throws IOException{
        if (start > end){
            throw new IllegalArgumentException(String.format("error distance range :[%.4f, %.4f]\n", start, end));
        } else if (inc <= 0){
            throw new IllegalArgumentException(String.format("error distance increasing :%.4f\n", inc));
        }
        Distribution d = setupDistribution();
        if (headerInformation){
            System.out.println("Distant (mm)\tConcentration (mM)");
        }
        for (double distance = start; distance <= end; distance += inc){
            System.out.print(distance);
            System.out.print("\t");
            System.out.printf("%.5g", d.getConcentration(distance));
            System.out.println();
        }
    }

    private static void setByFile(Distribution d, String fileName){
        int lineNum = 0;
        try (BufferedReader r = Files.newBufferedReader(Paths.get(fileName))) {
            String line;
            while ((line = r.readLine()) != null){
                lineNum++;
                line = line.replaceFirst("//.*$", "").trim();
                if (line.isEmpty()) continue;
                if (line.startsWith("Diffusion")){
                    d.setDiffusion(Double.parseDouble(line.substring(line.indexOf("=") + 1)));
                } else if (line.startsWith("Depth")){
                    d.setDepth(Double.parseDouble(line.substring(line.indexOf("=") + 1)));
                } else if (line.startsWith("Delaytime")){
                    d.setDelayTime(Double.parseDouble(line.substring(line.indexOf("=") + 1)));
                } else if (line.startsWith("Concentration")){
                    d.setConcentration(Double.parseDouble(line.substring(line.indexOf("=") + 1)));
                } else if (line.startsWith("Count")){
                    d.setCount(Integer.parseInt(line.substring(line.indexOf("=") + 1)));
                } else if (line.startsWith("Time")){
                    d.setDelayTime(d.getDelayTime() + Double.parseDouble(line.substring(line.indexOf("=") + 1)));
                } else {
                    throw new RuntimeException("unknown line " + "@" + fileName + ":" + lineNum);
                }
            }
        } catch (RuntimeException ex){
            throw ex;
        } catch (Throwable ex){
            throw new RuntimeException(ex.getMessage() + ", at " + fileName + ":" + lineNum, ex);
        }
    }
}
