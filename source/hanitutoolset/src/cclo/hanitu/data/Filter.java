/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import cclo.hanitu.HanituInfo;
import cclo.hanitu.Executable;

import java.io.IOException;
import java.util.ListIterator;

import static cclo.hanitu.HelpBuilder.*;

/**
 * @author antonio
 */
@HanituInfo.Tip("Spike Filter")
@HanituInfo.Description("The filter tool used to pick some information from hanitu spike data.")
public class Filter extends Executable{

    @HanituInfo.Option(value = "time", arg = "START[:END]", order = 1)
    @HanituInfo.Description("spike time filter")
    public String time;

    @HanituInfo.Argument(index = 0, value = "FILE")
    @HanituInfo.Description("Spike data file. default it will read data from the standard input.")
    public String inputFilePath = null;
    public SpikeLoader loader;
    private SpikeFilter filter;
    //
    private boolean inverse = false;
    private boolean inverseAll = false;

    @Override
    @HanituInfo.Option(shortName = 'u', value = "user", arg = "ID", description = "user filter. keep given id.")
    @HanituInfo.Option(shortName = 'w', value = "worm", arg = "ID", description = "worm filter. keep given id.")
    @HanituInfo.Option(shortName = 'n', value = "neuron", arg = "ID", description = "neuron filter. keep given id.")
    @HanituInfo.Option(shortName = 't', value = "type", arg = "ID", description = "neuron type. keep given type.")
    @HanituInfo.Option(shortName = 'i', description = "inverse the following one filter condition.", order = 0)
    @HanituInfo.Option(shortName = 'I', description = "inverse the following filter conditions", order = 0)
    public boolean extendOption(String key, String value, ListIterator<String> it){
        SpikeFilter sf;
        switch (key){
        case "u":
        case "user":
            sf = SpikeFilter.testUser(value, inverseAll || inverse);
            break;
        case "w":
        case "worm":
            sf = SpikeFilter.testWorm(value, inverseAll || inverse);
            break;
        case "n":
        case "neuron":
            sf = SpikeFilter.testNeuron(value, inverseAll || inverse);
            break;
        case "t":
        case "type":
            sf = SpikeFilter.testType(value, inverseAll || inverse);
            break;
        case "i":
            inverse = true;
            return true;
        case "I":
            inverseAll = true;
            return true;
        default:
            return false;
        }
        inverse = false;
        if (filter == null){
            filter = sf;
        } else {
            filter = filter.and(sf);
        }
        return true;
    }

    @Override
    public String extendHelpDoc(){
        return
          DOC_TITLE + "ID filter format" +
          DOC_BLOCK_START +
          "For single id filter, you can only specific use a integer value, such as '-w 0', and this " +
          "mean you want to get all of the spike data of the user 0. However, you can filter id with " +
          "a range with format 'START:END'. For example '-w 0:4', it mean you want to get all of the " +
          "spike data of user from 0 to 4 (include). Finally, you also can choose a list of id with " +
          "format 'ID,ID,...' (use ',' spread each other without and space). For example '-w 0,2,4', " +
          "it mean you want to get all of the spike data of user 0, 2 and 4." +
          DOC_BLOCK_END +
          DOC_NEWLINE +
          DOC_TITLE + "TYPE filter format" +
          DOC_BLOCK_START +
          "There three kinds of neurons and each one has one character to present. b for brain circuit " +
          "neuron. s for sensor neuron. m for motor neuron. You can use '-t sm' to get all of the sensor " +
          "and motor neurons' spike data." +
          DOC_BLOCK_END +
          DOC_NEWLINE +
          DOC_TITLE + "Spike File format" +
          DOC_BLOCK_START +
          "In program arguments, using '-' to reading data from standard input." +
          DOC_NEWLINE +
          "For common hanitu spike output file, it is Nx4 data." +
          DOC_NEWLINE +
          "First column is time in virtual time (simulation time). Second column is user id. Third column " +
          "is worm id. Forth column is neuron id. Fifth is optional column and mean neuron type." +
          DOC_NEWLINE +
          "so it can present as : time user worm neuron type." +
          DOC_NEWLINE +
          "for other spike format, program also detect other spike file including:" +
          DOC_LIST_0 + "time neuron" +
          DOC_LIST_0 + "time" +
          DOC_BLOCK_END;
    }

    public SpikeLoader setupLoader() throws IOException{
        if (loader == null){
            if (inputFilePath == null){
                loader = new SpikeLoader.Stream(System.in);
            } else {
                loader = new SpikeLoader.File(inputFilePath);
            }
        }
        return loader;
    }

    @Override
    public void start() throws IOException{
        DataLoader<SpikeData> ld = setupLoader();
        if (filter != null){
            ld = new DataFilter<>(filter, ld);
        }
        while (ld.hasNext()){
            System.out.println(ld.next());
        }
        ld.close();
    }
}
