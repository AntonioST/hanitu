/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Deque;
import java.util.LinkedList;

import static java.util.Objects.requireNonNull;

/**
 * @author antonio
 */
public abstract class FiringRateLoader extends DataLoader<FiringRateData>{

    /**
     * time window size in unit msec.
     */
    private int timeWindow = 10000;
    /**
     * unit msec
     */
    private int timeStep = 1000;
    private FiringRateScalar scalar = FiringRateScalar.uniform();

    public int getTimeWindow(){
        return timeWindow;
    }

    public void setTimeWindow(int timeWindow){
        if (timeWindow <= 0) throw new IllegalArgumentException("negative or zero time window size");
        this.timeWindow = timeWindow;
    }

    public int getTimeStep(){
        return timeStep;
    }

    public void setTimeStep(int timeStep){
        if (timeStep <= 0) throw new IllegalArgumentException("negative or zero time step mount");
        this.timeStep = timeStep;
    }

    public FiringRateScalar getScalar(){
        return scalar;
    }

    public void setScalar(FiringRateScalar scalar){
        this.scalar = requireNonNull(scalar);
    }

    public static class TimeSerial extends FiringRateLoader{

        final DataLoader<SpikeData> source;
        final Deque<SpikeData> queue = new LinkedList<>();
        SpikeData lastSpikeData;
        private double current;
        private boolean init = false;

        public TimeSerial(DataLoader<SpikeData> source){
            this.source = requireNonNull(source);
        }

        @Override
        protected FiringRateData tryGetNextData(){
            int w = super.timeWindow;
            SpikeData t = null;
            // initial current time when first data input
            if (!init){
                if ((t = source.tryGetNextData()) == null){
                    return null;
                } else {
                    current = t.time;
                    lastSpikeData = t;
                    init = true;
                }
            }
            double rb = current + w / 2;
            double lb = current - w / 2;
            // filter data in queue
            if (lastSpikeData == null || lastSpikeData.time < rb){
                if (lastSpikeData != null){
                    queue.add(lastSpikeData);
                }
                while (source.hasNext()){
                    t = source.next();
                    if (t.time > rb) break;
                    queue.add(t);
                }
                lastSpikeData = t;
            }
            while (!queue.isEmpty()){
                t = queue.removeFirst();
                if (t.time >= lb){
                    queue.addFirst(t);
                    break;
                }
            }
            // check terminate
            if (queue.isEmpty() && lastSpikeData == null){
                //no more data in queue and no more further spike data
                return null;
            }
            // calculate firing rate
            double rate = queue.stream()
              .mapToDouble(d -> super.scalar.rate(current, d.time, w))
              .sum();
            FiringRateData ret = new FiringRateData(current / 1000, 1000 * rate / w);
            // update current time
            current += super.timeStep;
            return ret;
        }

        @Override
        public void close(){
            source.close();
            super.close();
        }
    }

    public static class File extends TimeSerial{

        public File(Path path) throws IOException{
            super(new SpikeLoader.File(path));
            ((SpikeLoader)source).setDataParser(SpikeData::parseTimeSerial);
        }

        public File(String filename) throws IOException{
            super(new SpikeLoader.File(filename));
        }

        @Override
        protected FiringRateData tryGetNextData(){
            FiringRateData ret = super.tryGetNextData();
            if (ret == null){
                close();
            }
            return ret;
        }
    }

    public static class Stream extends TimeSerial{

        public Stream(BufferedReader reader){
            super(new SpikeLoader.Stream(reader));
            ((SpikeLoader)source).setDataParser(SpikeData::parseTimeSerial);
        }

        public Stream(InputStream is){
            super(new SpikeLoader.Stream(is));
        }

        public Stream(Path path) throws IOException{
            super(new SpikeLoader.Stream(path));
        }

        public Stream(String filename) throws IOException{
            super(new SpikeLoader.Stream(filename));
        }
    }

    public static class TestScalar extends FiringRateLoader{

        private int startTime;
        private int endTime;
        private int current;

        @Override
        public void setTimeWindow(int timeWindow){
            super.setTimeWindow(timeWindow);
            if (current == startTime){
                startTime = -timeWindow / 2;
                endTime = timeWindow / 2;
                current = startTime;
            }
        }

        @Override
        protected final FiringRateData tryGetNextData(){
            if (current > endTime){
                close();
                return null;
            } else {
                FiringRateData ret = new FiringRateData(current / 1000.0, super.scalar.rate(current, super.timeWindow));
                current += super.timeStep;
                return ret;
            }
        }
    }
}
