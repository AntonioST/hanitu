/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;

/**
 * @author antonio
 */
public class FRChartData{

    SingleSpikeFilter filter;
    private List<FiringRateData> recorder = new LinkedList<>();
    private FiringRateLoader.TimeSerial translator;
    private Series series;
    //    private double fromTime;
    private double toTime;

    public FRChartData(SingleSpikeFilter filter, DataLoader<SpikeData> source){
        this.filter = filter;
        translator = new FiringRateLoader.TimeSerial(source);
    }

    public SingleSpikeFilter getFilter(){
        return filter;
    }

    public int getTimeWindow(){
        return translator.getTimeWindow();
    }

    public void setTimeWindow(int timeWindow){
        translator.setTimeWindow(timeWindow);
    }

    public int getTimeStep(){
        return translator.getTimeStep();
    }

    public void setTimeStep(int timeStep){
        translator.setTimeStep(timeStep);
    }

    public FiringRateScalar getScalar(){
        return translator.getScalar();
    }

    public void setScalar(FiringRateScalar scalar){
        translator.setScalar(scalar);
    }

    public Stream<FiringRateData> stream(){
        return recorder.stream();
    }

    public void updateTimeRange(double fromTimeSec, double toTimeSec, Consumer<FiringRateData> updateData){
        toTime = toTimeSec;
        translator.until(fr -> fr.time > toTimeSec, data -> {
            recorder.add(data);
            series.accept(data);
            if (updateData != null){
                updateData.accept(data);
            }
        });
        recorder.removeIf(fr -> fr.time < fromTimeSec);
    }

    public XYChart.Series<Double, Double> getChartSeries(){
        if (series == null){
            newChartSeries();
        }
        return series.ref;
    }

    public XYChart.Series<Double, Double> newChartSeries(){
        series = new Series(new XYChart.Series<>());
        return series.ref;
    }

    private class Series implements Consumer<FiringRateData>{

        XYChart.Series<Double, Double> ref;

        public Series(XYChart.Series<Double, Double> ref){
            this.ref = ref;
            if (filter.type == 'b'){
                ref.setName("neuron : " + filter.neuron);
            } else if (filter.type == 's'){
                ref.setName("sensor : " + filter.neuron);
            } else if (filter.type == 'm'){
                ref.setName("motor : " + filter.neuron);
            }
            ObservableList<XYChart.Data<Double, Double>> data = ref.getData();
            recorder.forEach(fr -> data.add(new XYChart.Data<>(fr.time, fr.rate)));
        }

        @Override
        public void accept(FiringRateData fr){
            ref.getData().addAll(new XYChart.Data<>(fr.time, fr.rate));
        }
    }
}
