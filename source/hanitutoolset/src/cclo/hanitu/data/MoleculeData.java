/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import cclo.hanitu.world.MoleculeConfig;
import cclo.hanitu.world.MoleculeType;

/**
 * the data class for storing the molecule information during plotting
 *
 * @author antonio
 */
public class MoleculeData extends PositionData{

    /**
     * molecule identify number
     */
    public int id;
    /**
     * molecule type
     */
    public MoleculeType type;

    /**
     * create a empty molecule data
     */
    public MoleculeData(){
    }

    /**
     * create  with copy data from reference
     *
     * @param m reference
     */
    public MoleculeData(MoleculeData m){
        super(m);
        id = m.id;
        type = m.type;
    }

    /**
     * create with setting value from configuration
     *
     * @param c configuration
     */
    public MoleculeData(MoleculeConfig c){
        id = c.id;
        type = c.type;
        x = c.x;
        y = c.y;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        MoleculeData that = (MoleculeData)o;

        if (id != that.id) return false;
        if (type != that.type) return false;

        return true;
    }

    @Override
    public int hashCode(){
        int result = super.hashCode();
        result = 31 * result + id;
        result = 31 * result + (type != null? type.hashCode(): 0);
        return result;
    }

    @Override
    public String toString(){
        return String.format("%s[%d](x=%.2f, y=%.2f)", type.name(), id, x, y);
    }
}
