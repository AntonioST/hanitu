/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.function.Function;

/**
 * @author antonio
 */
public abstract class SpikeLoader extends DataLoader<SpikeData>{

    public static class SpikeDataFormatDetector implements Function<String, SpikeData>{

        Function<String, SpikeData> function = null;

        @Override
        public SpikeData apply(String line){
            if (function == null){
                int count = line.split("[ \t]+").length;
                if (count == 4 || count == 5){
                    function = SpikeData::parseWormsSpike;
                } else if (count == 2 || count == 3){
                    function = SpikeData::parseWormSpike;
                } else if (count == 1){
                    function = SpikeData::parseTimeSerial;
                } else {
                    throw new RuntimeException("unknown Spike data format");
                }
            }
            return function.apply(line);
        }
    }

    private Function<String, SpikeData> detector = new SpikeDataFormatDetector();

    public void setDataParser(Function<String, SpikeData> detector){
        this.detector = detector;
    }

    public static class File extends SpikeLoader{

        private BufferedReader reader;

        public File(Path file) throws IOException{
            reader = Files.newBufferedReader(file);
        }

        public File(String filename) throws IOException{
            reader = Files.newBufferedReader(Paths.get(filename));
        }

        @Override
        protected SpikeData tryGetNextData(){
            String line = null;
            try {
                line = reader.readLine();
            } catch (IOException e){
            }
            if (line == null){
                close();
                return null;
            }
            return super.detector.apply(line);
        }

        @Override
        public void close(){
            try {
                reader.close();
            } catch (IOException e){
            }
            super.close();
        }
    }

    public static class Stream extends SpikeLoader{

        private final BufferedReader reader;
        private final boolean autoClose;
        private int limitLength = 10;

        public Stream(BufferedReader reader){
            this.reader = Objects.requireNonNull(reader);
            autoClose = false;
        }

        public Stream(InputStream is){
            this.reader = new BufferedReader(new InputStreamReader(is));
            autoClose = false;
        }

        public Stream(Path file) throws IOException{
            this.reader = Files.newBufferedReader(file);
            autoClose = true;
        }

        public Stream(String filename) throws IOException{
            this.reader = Files.newBufferedReader(Paths.get(filename));
            autoClose = true;
        }

        @Override
        protected SpikeData tryGetNextData(){
            String line;
            try {
                reader.mark(limitLength);
                line = reader.readLine();
                SpikeData data = super.detector.apply(line);
                limitLength = Math.max(limitLength, line.length());
                return data;
            } catch (IOException e){
            } catch (RuntimeException e){
                try {
                    reader.reset();
                } catch (IOException e1){
                }
            }
            return null;
        }

        @Override
        public void close(){
            if (autoClose){
                try {
                    reader.close();
                } catch (IOException e){
                }
            }
            super.close();
        }
    }

}
