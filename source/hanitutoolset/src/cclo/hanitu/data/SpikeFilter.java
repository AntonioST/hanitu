/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Objects;
import java.util.Set;
import java.util.function.DoublePredicate;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author antonio
 */
public interface SpikeFilter extends Predicate<SpikeData>{

    static SpikeFilter testUser(String testUser, boolean inverse){
        IntPredicate t = parse(testUser);
        return data -> t.test(data.user) ^ inverse;
    }

    static SpikeFilter testWorm(String testWorm, boolean inverse){
        IntPredicate t = parse(testWorm);
        return data -> t.test(data.worm) ^ inverse;
    }


    static SpikeFilter testNeuron(String testNeuron, boolean inverse){
        IntPredicate t = parse(testNeuron);
        return data -> t.test(data.neuron) ^ inverse;
    }


    static SpikeFilter testType(String testType, boolean inverse){
        IntPredicate t = i -> testType.indexOf((char)i) >= 0;
        return data -> t.test(data.type) ^ inverse;
    }

    static SpikeFilter testTime(String time, boolean inverse){
        DoublePredicate t = parseTimeRange(time);
        return data -> t.test(data.time);
    }

    static IntPredicate parse(String expr){
        int index = expr.indexOf(":");
        if (index == 0){
            int value = Integer.parseInt(expr.substring(1));
            return i -> i >= 0 && i <= value;
        } else if (index > 0){
            int start = Integer.parseInt(expr.substring(0, index));
            int end = Integer.parseInt(expr.substring(index + 1));
            return i -> i >= start && i <= end;
        } else if (expr.contains(",")){
            Set<Integer> set = Stream.of(expr.split(",")).map(Integer::parseInt).collect(Collectors.toSet());
            return set::contains;
        } else {
            int value = Integer.parseInt(expr);
            return i -> i == value;
        }
    }

    static DoublePredicate parseTimeRange(String expr){
        int index = expr.indexOf(":");
        if (index < 0){
            double start = Double.parseDouble(expr);
            return t -> t >= start;
        } else if (index == 0){
            double end = Double.parseDouble(expr.substring(index + 1));
            return t -> t < end;
        } else {
            double start = Double.parseDouble(expr.substring(0, index));
            double end = Double.parseDouble(expr.substring(index + 1));
            return t -> start <= t && t < end;
        }
    }

    default SpikeFilter and(SpikeFilter other){
        Objects.requireNonNull(other);
        return t -> test(t) && other.test(t);
    }

    @Override
    default SpikeFilter negate(){
        return t -> !test(t);
    }

    default SpikeFilter or(SpikeFilter other){
        Objects.requireNonNull(other);
        return t -> test(t) || other.test(t);
    }
}
