/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.*;
import java.util.function.*;

/**
 * @author antonio
 */
public class DataRecorder<T extends Comparable<? super T>> extends DataLoader<T>{

    private final DataLoader<T> source;
    private final LinkedList<T> history = new LinkedList<>();
    private transient ListIterator<T> iterator;
    private transient T last;

    public DataRecorder(DataLoader<T> source){
        this.source = Objects.requireNonNull(source);
    }

    public int size(){
        return history == null? 0: history.size();
    }

    public void removeIf(Predicate<T> test){
        history.removeIf(test);
    }

    public boolean hasNext(T key){
        if (history == null) return false;
        if (close){
            if (key == null){
                nextData = history.getLast();
                iterator = null;
            } else if (last != null && key.compareTo(last) < 0){
                nextData = setIterator(key);
            } else {
                nextData = null;
            }
        } else {
//            System.out.println(key + " <?> " + last);
            if (key == null){
                nextData = tryGetNextDataFromSource();
            } else if (last == null || key.compareTo(last) > 0){
                nextData = tryGetNextUntil(key);
            } else {
                nextData = setIterator(key);
            }
        }
        return nextData != null;
    }

    @Override
    protected T tryGetNextData(){
        if (iterator != null && iterator.hasNext()){
            return iterator.next();
        } else if (source.hasNext()){
            return tryGetNextDataFromSource();
        } else {
            if (source.isClose()){
                close();
            }
            return null;
        }
    }

    private T tryGetNextDataFromSource(){
        iterator = null;
        T t = source.next();
        if (t == null) throw new IllegalStateException();
        history.add(t);
        last = t;
        return t;
    }

    private int getIndex(T key){
        Objects.requireNonNull(key);
        int i = 0;
        for (i = 0; i < history.size(); i++){
            if (key.compareTo(history.get(i)) < 0) break;
        }
        return i;
    }

    private T setIterator(T key){
        T data = null;
        ListIterator<T> it = iterator;
        if (it == null){
            it = iterator = history.listIterator(getIndex(key));
        }
        if (it.hasNext()){
            data = it.next();
        } else if (it.hasPrevious()){
            data = it.previous();
        }
        if (data != null && data.compareTo(key) > 0){
            for (T t; it.hasPrevious() && (t = it.previous()).compareTo(key) > 0; data = t) ;
        }
        for (T t; it.hasNext() && (t = it.next()).compareTo(key) < 0; data = t) ;
        return data;
    }

    private T tryGetNextUntil(T key){
        T data = null;
        while (!close){
            data = tryGetNextData();
            if (data == null) break;
            if (data.compareTo(key) >= 0) break;
        }
        return data;
    }

    public Iterable<T> forEach(){
        return Collections.unmodifiableList(history);
    }

    public void forEach(Consumer<T> c){
        Objects.requireNonNull(c);
        LinkedList<T> ls = history;
        if (ls != null){
            ls.forEach(c);
        }
    }

    public <R> Optional<R> forEach(Function<T, R> f, BinaryOperator<R> g){
        Objects.requireNonNull(f);
        Objects.requireNonNull(g);
        LinkedList<T> ls = history;
        if (ls == null) return Optional.empty();
        return ls.stream().map(f).reduce(g);
    }

//    public OptionalInt forEachInt(ToIntFunction<T> f, IntBinaryOperator g){
//        Objects.requireNonNull(f);
//        Objects.requireNonNull(g);
//        LinkedList<T> ls = history;
//        if (ls == null) return OptionalInt.empty();
//        return ls.stream().mapToInt(f).reduce(g);
//    }
//
//    public OptionalDouble forEachDouble(ToDoubleFunction<T> f, DoubleBinaryOperator g){
//        Objects.requireNonNull(f);
//        Objects.requireNonNull(g);
//        LinkedList<T> ls = history;
//        if (ls == null) return OptionalDouble.empty();
//        return ls.stream().mapToDouble(f).reduce(g);
//    }

    @Override
    public void close(){
        source.close();
        super.close();
    }
}
