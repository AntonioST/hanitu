/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Comparator;

/**
 * @author antonio
 */
public class SingleSpikeFilter implements SpikeFilter, Comparable<SingleSpikeFilter>{

    private static final Comparator<SingleSpikeFilter> CMP
      = Comparator.<SingleSpikeFilter>comparingInt(d -> d.user)
      .thenComparingInt(d -> d.worm)
      .thenComparingInt(d -> d.neuron)
      .thenComparingInt(d -> -"msb".indexOf(d.type));
    public final int user;
    public final int worm;
    public final int neuron;
    public final char type;

    public SingleSpikeFilter(int user, int worm, int neuron, char type){
        this.user = user;
        this.worm = worm;
        this.neuron = neuron;
        this.type = type;
    }

    @Override
    public boolean test(SpikeData spikeData){
        return spikeData.user == user
               && spikeData.worm == worm
               && spikeData.neuron == neuron
               && spikeData.type == type;
    }

    public SingleSpikeFilter toWormIdentify(){
        return new SingleSpikeFilter(user, worm, -1, '\0');
    }

    public SingleSpikeFilter toUserIdentify(){
        return new SingleSpikeFilter(user, -1, -1, '\0');
    }

    public boolean isSameWorm(SingleSpikeFilter that){
        return user == that.user
               && worm == that.worm;
    }

    public boolean isSensor(){
        return type == 's';
    }

    public boolean isMotor(){
        return type == 'm';
    }

    public boolean isNeuron(){
        return type == 'b';
    }

    @Override
    public int compareTo(SingleSpikeFilter o){
        return CMP.compare(this, o);
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SingleSpikeFilter that = (SingleSpikeFilter)o;

        return user == that.user
               && worm == that.worm
               && neuron == that.neuron
               && type == that.type;

    }

    @Override
    public int hashCode(){
        int result = user;
        result = 31 * result + worm;
        result = 31 * result + neuron;
        result = 31 * result + (int)type;
        return result;
    }

    @Override
    public String toString(){
        return String.format("%s:u=%d,w=%d,n=%d,t=%c", getClass().getSimpleName(), user, worm, neuron, type);
    }
}
