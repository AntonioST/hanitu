/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;

/**
 * @author antonio
 */
public class DataPrinter<T> extends DataLoader<T>{

    private final DataLoader<T> source;
    private final PrintStream output;
    private final boolean autoClose;

    public DataPrinter(PrintStream output, DataLoader<T> source){
        this.source = Objects.requireNonNull(source);
        this.output = Objects.requireNonNull(output);
        autoClose = false;
    }

    public DataPrinter(OutputStream os, DataLoader<T> source){
        this.source = Objects.requireNonNull(source);
        this.output = new PrintStream(os);
        autoClose = false;
    }

    public DataPrinter(Path path, DataLoader<T> source) throws IOException{
        this.source = Objects.requireNonNull(source);
        this.output = new PrintStream(Files.newOutputStream(path, CREATE, TRUNCATE_EXISTING));
        autoClose = true;
    }

    public DataPrinter(String filename, DataLoader<T> source) throws IOException{
        this.source = Objects.requireNonNull(source);
        this.output = new PrintStream(Files.newOutputStream(Paths.get(filename),
                                                            CREATE, TRUNCATE_EXISTING));
        autoClose = true;
    }


    @Override
    protected T tryGetNextData(){
        T t = source.tryGetNextData();
        if (t != null){
            output.println(t);
        }
        return t;
    }

    @Override
    public void close(){
        source.close();
        if (autoClose){
            output.close();
        }
        super.close();
    }
}
