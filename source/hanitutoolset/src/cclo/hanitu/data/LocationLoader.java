/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import cclo.hanitu.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.util.Objects.requireNonNull;

/**
 * The loader of the location. It is a abstract class because there are many way to load location in hanitu tool set,
 * and it need to implement different method in subclass to handle those difference.
 *
 *
 * @author antonio
 */
public abstract class LocationLoader extends DataLoader<LocationData>{

    public static class File extends LocationLoader{

        private final BufferedReader source;

        /**
         * @param file source file path
         * @throws IOException
         */
        public File(Path file) throws IOException{
            Log.debug(() -> "location from file : " + file);
            source = Files.newBufferedReader(file);
        }

        /**
         * @param filename source file name
         * @throws IOException
         */
        public File(String filename) throws IOException{
            this(Paths.get(filename));
        }

        @Override
        protected LocationData tryGetNextData(){
            String line = null;
            try {
                line = source.readLine();
            } catch (IOException e){
                close();
            }
            if (line == null){
                close();
                return null;
            }
            return LocationData.forString(line);
        }

        @Override
        public void close(){
            try {
                source.close();
            } catch (IOException e){
            }
            super.close();
        }
    }

    public static class Stream extends LocationLoader{

        private final BufferedReader source;
        private final boolean autoClose;
        public boolean close;
        private int limitLength = 10;

        /**
         * @param source source stream
         */
        public Stream(BufferedReader source){
            Log.debug("location from stream");
            this.source = requireNonNull(source);
            autoClose = false;
        }

        /**
         * @param is source stream
         */
        public Stream(InputStream is){
            Log.debug("location from stream");
            this.source = new BufferedReader(new InputStreamReader(is));
            autoClose = false;
        }

        /**
         * @param file source file path
         * @throws IOException
         */
        public Stream(Path file) throws IOException{
            Log.debug(() -> "location from stream : " + file);
            this.source = Files.newBufferedReader(file);
            autoClose = true;
        }

        /**
         * @param filename source file name
         * @throws IOException
         */
        public Stream(String filename) throws IOException{
            Log.debug(() -> "location from stream : " + filename);
            this.source = Files.newBufferedReader(Paths.get(filename));
            autoClose = true;
        }

        @Override
        protected LocationData tryGetNextData(){
            String line;
            try {
                source.mark(limitLength);
                line = source.readLine();
                LocationData data = LocationData.forString(line);
                limitLength = Math.max(limitLength, line.length());
                return data;
            } catch (IOException e){
            } catch (RuntimeException e){
                try {
                    source.reset();
                } catch (IOException e1){
                }
            }
            return null;
        }

        @Override
        public void close(){
            if (autoClose){
                try {
                    source.close();
                } catch (IOException e){
                }
            }
            super.close();
        }
    }
}
