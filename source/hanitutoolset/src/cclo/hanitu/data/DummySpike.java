/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import cclo.hanitu.Executable;

import java.util.Deque;
import java.util.LinkedList;

import static cclo.hanitu.HanituInfo.*;

/**
 * @author antonio
 */
@Tip("dummy spike generator")
@Description("~*y{EXPERIMENT~} : just use for testing")
public class DummySpike extends Executable{

    @Option(shortName = 'u', value = "user", arg = "NUMBER", order = 0)
    @Description("the number of user")
    public int userNumber = 1;

    @Option(shortName = 'w', value = "worm", arg = "NUMBER", order = 1)
    @Description("the number of total worm")
    public int wormNumber = 1;

    @Option(shortName = 'T', value = "total", arg = "NUMBER", order = 2)
    @Description("the total number of neuron per worm")
    public int totalNeuronNumber = 10;

    @Option(shortName = 'n', value = "neuron", arg = "RATE", order = 3)
    @Description("The firing rate of the body neuron")
    public double neuronFiringRate = 5;

    @Option(shortName = 's', value = "sensor", arg = "RATE", order = 3)
    @Description("The firing rate of the sensor")
    public double sensorFiringRate = 10;

    @Option(shortName = 'm', value = "motor", arg = "RATE", order = 3)
    @Description("The firing rate of the motor")
    public double motorFiringRate = 1;

    @Option(shortName = 't', value = "time", arg = "TIME", order = 4)
    @Description("the limit of the time in unit msec")
    public double limit = 10000;
    //
    private int currentTime;
    private Deque<SpikeData> queue = new LinkedList<>();

    @Override
    public void start(){
        while (next()){
            while (!queue.isEmpty()){
                System.out.println(queue.removeFirst());
            }
        }
    }

    private boolean next(){
        if (currentTime >= limit) return false;
        currentTime += 1;
        for (int u = 0; u < userNumber; u++){
            for (int w = 0; w < wormNumber; w++){
                for (int n = 0; n < totalNeuronNumber; n++){
                    if (Math.random() < neuronFiringRate / 1000){
                        queue.add(new SpikeData(currentTime, u, w, n, 'b'));
                    }
                }
                if (Math.random() < sensorFiringRate / 1000){
                    queue.add(new SpikeData(currentTime, u, w, -1, 's'));
                }
                if (Math.random() < motorFiringRate / 1000){
                    queue.add(new SpikeData(currentTime, u, w, -1, 'm'));
                }
            }
        }
        return true;
    }
}
