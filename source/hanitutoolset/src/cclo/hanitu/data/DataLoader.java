/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * @author antonio
 */
public abstract class DataLoader<T> implements Iterator<T>, AutoCloseable{

    T nextData;
    boolean close;

    @Override
    public boolean hasNext(){
        if (nextData == null){
            nextData = tryGetNextData();
        }
        return nextData != null;
    }

    @Override
    public T next(){
        if (nextData == null){
            while ((nextData = tryGetNextData()) == null){
                if (close){
                    throw new NoSuchElementException();
                }
            }
        }
        T ret = nextData;
        nextData = null;
        return ret;
    }

    public boolean tryNext(Consumer<T> action){
        if (nextData == null){
            if ((nextData = tryGetNextData()) == null){
                return false;
            }
        }
        action.accept(nextData);
        nextData = null;
        return true;
    }

    public void until(Predicate<T> terminate, Consumer<T> action){
        while (hasNext() && !terminate.test(nextData)){
            action.accept(nextData);
            nextData = null;
        }
    }

    protected abstract T tryGetNextData();

    public boolean isClose(){
        return close;
    }

    @Override
    public void close(){
        close = true;
    }
}
