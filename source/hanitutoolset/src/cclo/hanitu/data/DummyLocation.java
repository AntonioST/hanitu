/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.stream.Stream;

import cclo.hanitu.Log;
import cclo.hanitu.Executable;
import cclo.hanitu.circuit.Direction;
import cclo.hanitu.gui.FXMain;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.WorldConfigPrinter;
import cclo.hanitu.world.WormConfig;
import cclo.hanitu.world.view.VirtualWorld;

import static cclo.hanitu.HanituInfo.*;

/**
 * @author antonio
 */
@Tip("dummy location generator")
@Description("~*y{EXPERIMENT~} : just use for testing")
public class DummyLocation extends Executable{

    private static final Direction[] DIRECTIONS = Direction.values();
    //
    @Option(shortName = 'w', value = "worm", arg = "COUNT", order = 0)
    @Description("the number of the worms, default is 1")
    public int wormCount = 1;

    @Option(shortName = 'b', value = "boundary", arg = "SIZE", order = 0)
    @Description("the boundary of the world")
    public int boundary = 10;

    @Option(shortName = 's', value = "size", arg = "VALUE", order = 0)
    @Description("the size of the worm")
    public int wormSize = 3;

    @Option(shortName = 'd', value = "decay", arg = "RATE", order = 1)
    @Description("hp decay per ms")
    public double hpDecayRate = 0.1;

    @Option(shortName = 'm', value = "move", arg = "RATE", order = 1)
    @Description("moving rate. value range from 0 to 1")
    public double moveRate = 0.4;

    @Option(shortName = 'f', value = "food", arg = "RATE", order = 1)
    @Description("the rate of eating food. value range from 0 to 1")
    public double getFoodRate = 0.01;

    @Option(shortName = 'e', value = "energyEffect", arg = "RATE", order = 1)
    @Description("energy increase/decrease unit. value range from 0 to 1")
    public double energyEffect = 0.1;

    @Option(shortName = 't', value = "time", arg = "START[:END]", order = 2)
    @Description("time limit, in unit ms")
    public int timeEnd = -1;

    @Option(value = "world", arg = "FILE", order = 3)
    @Description("generate dummy world configuration file")
    public String worldConfigFileName;

    @Option(value = "plot", order = 3)
    @Description("show plotting window")
    public boolean plot;
    //
    private int currentTime;
    private WormData[] worms;

    public WormData[] setupWorms(){
        if (worms == null){
            if (wormCount <= 0){
                throw new IllegalArgumentException("worm count : " + wormCount);
            }
            worms = new WormData[wormCount];
            for (int i = 0; i < wormCount; i++){
                WormData data = new WormData();
                data.user = i;
                data.worm = 0;
                data.size = wormSize;
                data.hp = 100.0;
                worms[i] = data;
            }
            for (int i = 0; i < wormCount; ){
                randomPosition(worms[i]);
                boolean test = false;
                for (int j = 0; j < i; j++){
                    if (worms[i].contact(worms[j])){
                        test = true;
                        break;
                    }
                }
                if (test) continue;
                i++;
            }
        }
        return worms;
    }

    private void randomPosition(WormData data){
        data.x = (int)(2 * boundary * Math.random() - boundary);
        data.y = (int)(2 * boundary * Math.random() - boundary);
    }

    private WorldConfig genWorld(){
        WorldConfig ret = new WorldConfig();
        ret.deltaHealthPoint = (int)(energyEffect * 100);
        ret.boundary = boundary * 10;
        for (WormData w : setupWorms()){
            WormConfig c = new WormConfig();
            c.user = w.user;
            c.worm = w.worm;
            c.size = wormSize;
            ret.addWorm(c);
        }
        return ret;
    }

    @Override
    public void start(){
        setupWorms();
        if (worldConfigFileName != null){
            try {
                new WorldConfigPrinter().write(Paths.get(worldConfigFileName), genWorld());
            } catch (IOException e){
                Log.trace(System.err, e);
            }
        }
        if (plot){
            showPlotFrame();
        } else {
            while (next()){
                System.out.println(new LocationData(currentTime, worms));
            }
        }
    }

    private boolean next(){
        if (timeEnd < 0){
            if (Stream.of(worms).allMatch(w -> w.hp <= 0)){
                return false;
            }
        } else if (currentTime >= timeEnd) return false;
        currentTime += 1;
        PositionData next = new PositionData();
        Stream.of(worms)
          .filter(w -> w.hp > 0)
          .forEach(worm -> {
              for (; ; ){
                  move(worm, next);
                  if (Math.abs(next.x) < boundary &&
                      Math.abs(next.y) < boundary &&
                      Stream.of(worms)
                        .filter(w -> w != worm)
                        .allMatch(w -> !w.contact(next))) break;
              }
              worm.x = next.x;
              worm.y = next.y;
              //
              if (Math.random() < getFoodRate){
                  worm.hp += energyEffect * 100;
                  if (worm.hp > 100) worm.hp = 100;
              } else {
                  worm.hp -= hpDecayRate;
              }
          });
        return true;
    }

    private void move(WormData data, PositionData next){
        double r = Math.random();
        if (r < moveRate){
            switch (DIRECTIONS[(int)(Math.random() * 4) + 1]){
            case FORWARD:
                next.y = data.y - 2;
                break;
            case BACKWARD:
                next.y = data.y + 2;
                break;
            case LEFTWARD:
                next.x = data.x - 2;
                break;
            case RIGHTWARD:
                next.x = data.x + 2;
                break;
            }
        }
    }

    private void showPlotFrame(){
        VirtualWorld world = new VirtualWorld();
        world.setWorldConfig(genWorld());
        world.setLoader(new DataPrinter<>(System.out, getLocationLoader()));
        FXMain.launch(world);
    }

    public LocationLoader getLocationLoader(){
        return new LocationLoader(){

            @Override
            protected LocationData tryGetNextData(){
                if (DummyLocation.this.next()){
                    WormData[] w;
                    w = worms.clone();  // just clone array
                    for (int i = 0, length = w.length; i < length; i++){
                        w[i] = new WormData(worms[i]);
                    }
                    return new LocationData(currentTime, w);
                } else {
                    return null;
                }
            }
        };
    }
}
