/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import cclo.hanitu.Log;
import cclo.hanitu.HanituInfo;
import cclo.hanitu.Executable;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ListIterator;
import java.util.stream.Stream;

import static cclo.hanitu.HelpBuilder.*;

/**
 * @author antonio
 */
@HanituInfo.Tip("Firing Rate")
@HanituInfo.Description("The tool used to calculate the firing rate for spike file")
public class Firingrate extends Executable{

    @HanituInfo.Option(shortName = 'w', value = "window", arg = "VALUE", order = 0)
    @HanituInfo.Description("setting the time window size. default is 10000 ms.")
    public int timeWindow = 10000;

    @HanituInfo.Option(shortName = 's', value = "step", arg = "VALUE", order = 0)
    @HanituInfo.Description("setting time step. default is 1000 ms.")
    public int timeStep = 1000;

    @HanituInfo.Option(value = "test-scalar", order = 2)
    @HanituInfo.Description("Output the given scalar weight value distribution.")
    public boolean testScalar = false;

    @HanituInfo.Argument(index = 0, value = "FILE")
    @HanituInfo.Description("Spike data. default it will read data from the standard input.")
    public String inputFilePath = null;
    public FiringRateLoader loader;
    //
    public FiringRateScalar scalar;

    @Override
    @HanituInfo.Option(value = "scalar", arg = "ARGS", description = "Using special scalar.  default is uniform scalar.", order = 1)
    public boolean extendOption(String key, String value, ListIterator<String> it){
        switch (key){
        case "scalar":
            scalar = parseScalar(value, it);
            break;
        default:
            return false;
        }
        return true;
    }

    @Override
    public String extendHelpDoc(){
        return
          DOC_TITLE + "Scalar argument" +
          DOC_BLOCK_START +
          "type [argument]" +
          DOC_NEWLINE +
          "built-in scalar:" +
          DOC_LIST_0 + "uniform" +
          DOC_LIST_0 + "half" +
          DOC_LIST_0 + "exp [half-time-decay]" +
          DOC_LIST_0 + "weight [weight,...|file]" +
          DOC_LIST_0 + "normal [std]" +
          DOC_NEWLINE +
          "for example:" +
          DOC_NEWLINE +
          DOC_BLOCK_START +
          "create a uniform distribution weight" +
          DOC_LIST_0 + "--scalar=uniform" +
          DOC_LIST_0 + "--scalar=uniform[]" +
          DOC_LIST_0 + "--scalar uniform" +
          DOC_LIST_0 + "--scalar uniform[]" +
          DOC_NEWLINE +
          "create a normal distribution weight with std 100 (ms)." +
          DOC_LIST_0 + "--scalar=normal[100]" +
          DOC_LIST_0 + "--scalar normal[100]" +
          DOC_NEWLINE +
          "create a triangle-shaped weight scalar." +
          DOC_LIST_0 + "--scalar weight[0,1,0]" +
          DOC_BLOCK_END +
          DOC_NEWLINE +
          "custom scalar type:" +
          DOC_BLOCK_START +
          "the class should implement interface cclo.hanitu.tool.firingrate.FiringRateScalar" +
          DOC_BLOCK_END +
          DOC_NEWLINE +
          DOC_TITLE + "Scalar test" +
          DOC_BLOCK_START +
          "When using '--test-scalar', program will test the given scalar. output weight value " +
          "for x range [-<window>/2, <window>/2] with increasing <step>." +
          DOC_BLOCK_END;
    }

    public FiringRateLoader setupLoader() throws IOException{
        if (loader == null){
            if (testScalar){
                loader = new FiringRateLoader.TestScalar();
            } else if (inputFilePath == null){
                loader = new FiringRateLoader.Stream(System.in);
            } else {
                loader = new FiringRateLoader.File(inputFilePath);
            }
            loader.setTimeWindow(timeWindow);
            loader.setTimeStep(timeStep);
            if (scalar != null){
                loader.setScalar(scalar);
            }
        }
        return loader;
    }

    @Override
    public void start() throws IOException{
        FiringRateLoader fl = setupLoader();
        while (fl.hasNext()){
            System.out.println(fl.next());
        }
    }

    private static FiringRateScalar parseScalar(String arg, ListIterator<String> it){
        String type;
        String args;
        if (arg.contains("[")){
            if (!arg.endsWith("]")){
                throw new IllegalArgumentException("scalar args lost ']' : " + arg);
            }
            int index = arg.indexOf("[");
            type = arg.substring(0, index);
            args = arg.substring(index + 1, arg.length() - 1);
        } else if (it.hasNext()){
            type = arg;
            String t = it.next();
            if (t.startsWith("[")){
                if (!t.endsWith("]")){
                    throw new IllegalArgumentException("scalar args lost ']' : " + t);
                }
                args = t.substring(1, t.length() - 1);
            } else {
                it.previous();
                args = "";
            }
        } else {
            type = arg;
            args = "";
        }
        Log.debug(() -> "scalar type : " + type);
        Log.debug(() -> "scalar args : " + args);
        try {
            return createScalarForString(type, args);
        } catch (RuntimeException throwable){
            throw throwable;
        } catch (Throwable throwable){
            throw new RuntimeException(throwable);
        }
    }

    private static FiringRateScalar createScalarForString(String type, String args) throws Throwable{
        switch (type){
        case "uniform":
            if (!args.isEmpty()){
                throw new IllegalArgumentException(args);
            }
            return FiringRateScalar.uniform();
        case "half":
            if (!args.isEmpty()){
                throw new IllegalArgumentException(args);
            }
            return FiringRateScalar.half();
        case "exp":
            if (args.isEmpty()){
                throw new IllegalArgumentException("empty args for scalar type : " + type);
            }
            return FiringRateScalar.expDecay(Double.parseDouble(args));
        case "normal":
            if (args.isEmpty()){
                throw new IllegalArgumentException("empty args for scalar type : " + type);
            }
            return FiringRateScalar.normal(Double.parseDouble(args));
        case "weight":
            if (args.isEmpty()){
                throw new IllegalArgumentException("empty args for scalar type : " + type);
            }
            if (args.contains(",")){
                return FiringRateScalar.weight(Stream.of(args.split(","))
                                                 .mapToDouble(Double::parseDouble)
                                                 .toArray());
            } else {
                return FiringRateScalar.weight(args);
            }
        default:
            Class<FiringRateScalar> c = (Class<FiringRateScalar>)Class.forName(type);
            Constructor<FiringRateScalar> g = c.getConstructor(String.class);
            return g.newInstance(args);
        }
    }
}
