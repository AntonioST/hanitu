/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.*;
import java.util.function.BiConsumer;

import cclo.hanitu.util.Events;

/**
 * @author antonio
 */
public class FRChartDataLoader{

    final DataLoader<SpikeData> loader;
    final List<FRChartData> list = new ArrayList<>();
    final Map<SingleSpikeFilter, DataPusher<SpikeData>> map = new HashMap<>();
    final Map<SingleSpikeFilter, BiConsumer<SingleSpikeFilter, FiringRateData>> downstream = new HashMap<>();

    protected int currentTime = 0;
    protected int timeWindow = 100; //ms
    protected int timeStep = 20; //ms
    protected int timeRemain = 1000; // ms
    protected double maxFiringRate = 1;

    private boolean autoCreateData = true;

    public final Events.ConsumerEvent<FRChartData> newDataEvent = new Events.ConsumerEvent<>();

    public FRChartDataLoader(DataLoader<SpikeData> loader){
        this.loader = Objects.requireNonNull(loader);
    }

    public boolean isAutoCreateData(){
        return autoCreateData;
    }

    public void setAutoCreateData(boolean autoCreateData){
        this.autoCreateData = autoCreateData;
    }

    public List<FRChartData> getData(){
        return Collections.unmodifiableList(list);
    }

    /**
     * time in unit msec
     * @return
     */
    public int getTimeWindow(){
        return timeWindow;
    }

    /**
     * time in unit msec
     * @param timeWindow
     */
    public void setTimeWindow(int timeWindow){
        if (timeWindow <= 0) throw new IllegalArgumentException("negative or zero time window size");
        this.timeWindow = timeWindow;
        list.forEach(d -> d.setTimeWindow(timeWindow));
    }

    /**
     * time in unit msec
     * @return
     */
    public int getTimeStep(){
        return timeStep;
    }

    /**
     * time in unit msec
     * @param timeStep
     */
    public void setTimeStep(int timeStep){
        if (timeStep <= 0) throw new IllegalArgumentException("negative or zero time step mount");
        this.timeStep = timeStep;
        list.forEach(d -> d.setTimeStep(timeStep));
    }


    /**
     * time in unit msec
     * @return
     */
    public int getTimeRemain(){
        return timeRemain;
    }

    /**
     * time in unit msec
     * @param timeRemain
     */
    public void setTimeRemain(int timeRemain){
        if (timeRemain <= 0) throw new IllegalArgumentException("negative or zero time during");
        this.timeRemain = timeRemain;
    }

    public void addDownstream(SingleSpikeFilter f, BiConsumer<SingleSpikeFilter, FiringRateData> c){
        downstream.put(f, Objects.requireNonNull(c));
    }

    /**
     * time in unit sec
     * @return
     */
    public double getStartTime(){
        return (currentTime - timeRemain) / 1000.0;
    }

    /**
     * time in unit sec
     * @return
     */
    public double getLastTime(){
        return (currentTime + timeWindow / 2) / 1000.0;
    }

    public double getMaxFiringRate(){
        return maxFiringRate * 1.2;
    }

    public void updateNextTimeClick(){
        double startTimeStamp = getStartTime();
        double endTimeStamp = getLastTime();
        loader.until(d -> d.time / 1000 > endTimeStamp, this::pushData);
        list.forEach(d -> d.updateTimeRange(startTimeStamp, endTimeStamp, data -> {
            SingleSpikeFilter f = d.getFilter();
            BiConsumer<SingleSpikeFilter, FiringRateData> function = downstream.get(f);
            if (function != null){
                function.accept(f, data);
            }
            function = downstream.get(null);
            if (function != null){
                function.accept(f, data);
            }
        }));
        maxFiringRate = list.stream()
          .flatMapToDouble(d -> d.stream().mapToDouble(fr -> fr.rate))
          .max().orElse(maxFiringRate);
        currentTime += timeStep;
    }

    private void pushData(SpikeData data){
        boolean exist = false;
        for (FRChartData d : list){
            if (d.filter.test(data)){
                exist = true;
                if (map.containsKey(d.filter)){
                    map.get(d.filter).accept(data);
                }
            }
        }
        if (!exist && autoCreateData){
            FRChartData frd = addFilter(new SingleSpikeFilter(data.user,
                                                              data.worm,
                                                              data.neuron,
                                                              data.type));
            map.get(frd.filter).accept(data);
        }
    }

    public FRChartData addFilter(SingleSpikeFilter filter){
        DataPusher<SpikeData> push = new DataPusher<>();
        FRChartData frd = new FRChartData(filter, push);
        frd.setTimeWindow(timeWindow);
        frd.setTimeStep(timeStep);
        map.put(filter, push);
        list.add(frd);
        newDataEvent.accept(frd);
        return frd;
    }

    public void removeFilter(SingleSpikeFilter filter){
        map.remove(filter);
    }

    public void deleteFilter(SingleSpikeFilter filter){
        list.removeIf(data -> data.getFilter().equals(filter));
        map.remove(filter);
    }
}
