/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data.view;

import java.util.*;
import java.util.stream.Collectors;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import cclo.hanitu.Log;
import cclo.hanitu.data.FRChartData;
import cclo.hanitu.data.FRChartDataLoader;
import cclo.hanitu.data.SingleSpikeFilter;
import cclo.hanitu.gui.FXMain;
import cclo.hanitu.gui.FXUIField;

import static cclo.hanitu.gui.MenuSession.createMenuItem;

/**
 * @author antonio
 */
public class FRChart implements FXMain.FXSimpleApplication{


    private FRChartDataLoader dataLoader;

    private Stage primaryStage;
    private VBox content;
    private Timeline timeline;
    private Map<LineChart, List<FRChartData>> charts = new HashMap<>();
    private Map<String, LineChart> identifyMap = new HashMap<>();
    private SimpleDoubleProperty xAxisLowerBound = new SimpleDoubleProperty();
    private SimpleDoubleProperty xAxisUpperBound = new SimpleDoubleProperty();
    //    private SimpleDoubleProperty yAxisLowerBound = new SimpleDoubleProperty();
    private SimpleDoubleProperty yAxisUpperBound = new SimpleDoubleProperty();
    //
    private SimpleDoubleProperty timeWindow = new SimpleDoubleProperty();
    private SimpleDoubleProperty timeStep = new SimpleDoubleProperty();
    private SimpleDoubleProperty timeRemain = new SimpleDoubleProperty();


    private double widthHeightRatio = 0.8;
    private double widthTimeRangeRatio = 2;

    private volatile SimpleBooleanProperty pause = new SimpleBooleanProperty(true);
    private volatile SimpleBooleanProperty animate = new SimpleBooleanProperty(true);

    @Override
    public void start(Stage stage) throws Exception{
        primaryStage = stage;
        stage.setTitle("Firing Rate Chart");
        stage.setMinWidth(500);
        stage.setMinHeight(500);
        //
        content = new VBox();
        content.setSpacing(10);
        content.setFillWidth(true);
        //
        ScrollPane scroll = new ScrollPane(content);
        scroll.setFitToWidth(true);
        Scene scene = new Scene(new VBox(initToolBar(), scroll));
        scene.setOnKeyPressed(this::handleKeyEvent);
        stage.setScene(scene);
        //
        if (dataLoader != null){
            setDataLoader(dataLoader);
        }
        //
        initEvent();
        initTimeline();
        //
        stage.sizeToScene();
        stage.show();
        resetChartSize();
        timeline.play();
    }

    private ToolBar initToolBar(){
        Label timeWindowLabel = new Label("Time window");
        Label timeStepLabel = new Label("Time step");
        Label timeRemainLabel = new Label("Time remain");
        TextField timeWindowField = new TextField(Integer.toString(dataLoader.getTimeWindow()));
        TextField timeStepField = new TextField(Integer.toString(dataLoader.getTimeStep()));
        TextField timeRemainField = new TextField(Integer.toString(dataLoader.getTimeRemain()));
        ToggleButton animateButton = new ToggleButton("Animate");
        Button pauseButton = new Button("Play");
        ToolBar bar = new ToolBar(timeWindowLabel,
                                  timeWindowField,
                                  timeStepLabel,
                                  timeStepField,
                                  timeRemainLabel,
                                  timeRemainField,
                                  new Separator(),
                                  animateButton,
                                  pauseButton);
        //
        timeWindowField.setPrefColumnCount(6);
        timeStepField.setPrefColumnCount(6);
        timeRemainField.setPrefColumnCount(6);
        //
        timeWindow.addListener((ig, old, newV) -> timeWindowField.setText(Integer.toString(newV.intValue())));
        timeStep.addListener((ig, old, newV) -> timeStepField.setText(Integer.toString(newV.intValue())));
        timeRemain.addListener((ig, old, newV) -> timeRemainField.setText(Integer.toString(newV.intValue())));
        timeWindowField.textProperty().addListener(FXUIField.textFormatterChecker(timeWindowField, text -> {
            try {
                int i = Integer.parseInt(text);
                if (i <= 0) return "negative time value";
                timeWindow.set(i);
                return null;
            } catch (NumberFormatException e){
                return "wrong number format";
            }
        }));
        timeStepField.textProperty().addListener(FXUIField.textFormatterChecker(timeStepField, text -> {
            try {
                int i = Integer.parseInt(text);
                if (i <= 0) return "negative time value";
                timeStep.set(i);
                return null;
            } catch (NumberFormatException e){
                return "wrong number format";
            }
        }));
        timeRemainField.textProperty().addListener(FXUIField.textFormatterChecker(timeRemainField, text -> {
            try {
                int i = Integer.parseInt(text);
                if (i <= 0) return "negative time value";
                timeRemain.set(i);
                return null;
            } catch (NumberFormatException e){
                return "wrong number format";
            }
        }));
        //
        animateButton.selectedProperty().addListener((ig, oldV, newV) -> animate.set(newV));
        //
        pauseButton.setOnAction(e -> setPause(!isPause()));
        pause.addListener((observable, oldValue, newValue) -> {
            if (newValue){
                pauseButton.setText("Play");
            } else {
                pauseButton.setText("Pause");
            }
        });
        //
        return bar;
    }

    private void initTimeline(){
        if (timeline == null){
            timeline = new Timeline(new KeyFrame(new Duration(100), e -> {
                updateFiringRate();
            }));
            timeline.setCycleCount(Timeline.INDEFINITE);
        }
    }

    private void initEvent(){
        Scene scene = primaryStage.getScene();
        String identify = ID(scene);
        scene.setOnDragDropped(e -> {
            Log.debug(() -> "drag done : " + identify);
            Dragboard dragboard = e.getDragboard();
            boolean success = false;
            if (dragboard.hasString()){
                LineChart chart = identifyMap.get(dragboard.getString());
                double sceneY = e.getSceneY();
                double offsetY = content.getLayoutY();
done:
                {
                    ObservableList<Node> children = content.getChildren();
                    if (children.size() == 1) break done;
                    for (Node node : children){
                        if (node != chart && (node instanceof LineChart) && node.getLayoutY() + offsetY > sceneY){
                            moveChart(chart, ((LineChart)node));
                            success = true;
                            break done;
                        }
                    }
                    if (children.get(children.size() - 1) != chart){
                        moveChart(chart, null);
                        success = true;
                    }
                }

            }
            e.setDropCompleted(success);
            e.consume();
        });
        scene.setOnDragOver(e -> {
            e.acceptTransferModes(TransferMode.MOVE);
            e.consume();
//            Env.debug(() -> "drag over : " + identify);
        });
        content.widthProperty().addListener((observable, oldValue, newValue) -> {
            resetChartSize();
            timeRemain.set((int)(newValue.doubleValue() * widthHeightRatio));
        });
        //
        timeWindow.addListener((observable, oldValue, newValue) -> dataLoader.setTimeWindow(newValue.intValue()));
        timeStep.addListener((observable, oldValue, newValue) -> dataLoader.setTimeStep(newValue.intValue()));
        timeRemain.addListener((observable, oldValue, newValue) -> dataLoader.setTimeRemain(newValue.intValue()));
    }

    public FRChartDataLoader getDataLoader(){
        return dataLoader;
    }

    public void setDataLoader(FRChartDataLoader dataLoader){
        this.dataLoader = dataLoader;
        if (primaryStage != null){
            dataLoader.getData().forEach(this::addLineChart);
            dataLoader.newDataEvent.add(this::addLineChart);
        }
    }

    public boolean isPause(){
        return pause.get();
    }

    public void setPause(boolean pause){
        this.pause.set(pause);
    }

    private void updateFiringRate(){
        if (pause.get()) return;
        dataLoader.updateNextTimeClick();
        xAxisLowerBound.set(dataLoader.getStartTime());
        xAxisUpperBound.set(dataLoader.getLastTime());
        yAxisUpperBound.set(dataLoader.getMaxFiringRate());
    }

    private void addLineChart(FRChartData data){
        LineChart chart = getLineChart(data);
        if (chart == null){
            Log.debug(() -> "add Filter : " + data.getFilter());
            chart = newLineChart(getChartTitle(data.getFilter()));
            content.getChildren().add(chart);
            resetChartSize();
        }
        XYChart.Series<Double, Double> series = data.getChartSeries();
        chart.getData().add(series);
        charts.computeIfAbsent(chart, k -> new ArrayList<>()).add(data);
    }

    private LineChart newLineChart(String title){
        //
        NumberAxis xAxis = new NumberAxis("time", 0, 0, 0.1);
        NumberAxis yAxis = new NumberAxis("firing rate", 0, 100, 20);
        xAxis.setForceZeroInRange(false);
        yAxis.setForceZeroInRange(true);
        xAxis.setTickLabelsVisible(true);
        xAxis.lowerBoundProperty().bind(xAxisLowerBound);
        xAxis.upperBoundProperty().bind(xAxisUpperBound);
        yAxis.upperBoundProperty().bind(yAxisUpperBound);
        //
        LineChart chart = new LineChart(xAxis, yAxis);
        String identify = ID(chart);
        identifyMap.put(identify, chart);
        Log.debug(() -> "new chart : " + identify + " [" + title + "]");

        chart.setTitle(title);

        chart.setPrefWidth(500);
        chart.setMinHeight(200);
        chart.setPrefHeight(500 * widthHeightRatio);
        chart.setMaxHeight(400);
        animate.addListener((ig, oldV, newV) -> chart.setAnimated(newV));

        chart.setOnMouseClicked(e -> {
            if (e.getButton() == MouseButton.SECONDARY){
                showPopupMenu(chart, e);
            }
        });
        chart.setOnDragDetected(e -> {
            Log.debug(() -> "drag : " + identify);
            Dragboard dragboard = primaryStage.getScene().startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            content.putString(identify);
            dragboard.setContent(content);
            e.consume();
        });
        chart.setOnDragEntered(e -> {
            FXUIField.setBackground(chart, Color.LIGHTGREEN);
        });
        chart.setOnDragExited(e -> {
            FXUIField.setBackground(chart, Color.WHITE);
        });
        chart.setOnDragOver(e -> {
            e.acceptTransferModes(TransferMode.MOVE);
            e.consume();
            //Env.debug(() -> "drag over : " + identify);
        });
        chart.setOnDragDropped(e -> {
            Log.debug(() -> "drag done : " + identify);
            FXUIField.setBackground(chart, Color.WHITE);
            Dragboard dragboard = e.getDragboard();
            boolean success = false;
            if (dragboard.hasString()){
                mergeChart(identifyMap.get(dragboard.getString()), chart);
                success = true;
            }
            e.setDropCompleted(success);
            e.consume();
        });

        return chart;
    }

    private LineChart getLineChart(FRChartData data){
        SingleSpikeFilter filter = data.getFilter();
        for (Map.Entry<LineChart, List<FRChartData>> entry : charts.entrySet()){
            for (FRChartData frd : entry.getValue()){
                if (filter.isSameWorm(frd.getFilter())){
                    return entry.getKey();
                }
            }
        }
        return null;
    }

    private String getChartTitle(SingleSpikeFilter f){
        return String.format("user=%d worm=%d", f.user, f.worm);
    }

    protected String spikeFilterLongFormat(SingleSpikeFilter f){
        return String.format("user=%d worm=%d neuron=%d(%c)", f.user, f.worm, f.neuron, f.type);
    }
//
//    protected String spikeFilterShortFormat(SingleSpikeFilter f){
//        return String.format("%d,%d,%d(%c)", f.user, f.worm, f.neuron, f.type);
//    }

    private void resetChartSize(){
        double v1 = (primaryStage.getHeight() - 50) / charts.size();
        double v2 = content.getWidth() * widthHeightRatio;
        double v = Math.min(v1, v2);
        charts.forEach((k, ignore) -> k.setPrefHeight(v));
    }

    private void showPopupMenu(LineChart chart, MouseEvent e){
        ContextMenu menu = new ContextMenu();
        ObservableList<MenuItem> items = menu.getItems();
        List<FRChartData> ls = charts.get(chart);
        if (ls == null) return;
        setPause(true);
        //
        if (ls.stream().anyMatch(d -> d.getFilter().isSensor())){
            if (!ls.stream().allMatch(d -> d.getFilter().isSensor())){
                items.add(createMenuItem("spread sensor neuron", event -> spreadSensor(chart)));
            }
        }
        //
        if (ls.stream().anyMatch(d -> d.getFilter().isMotor())){
            if (!ls.stream().allMatch(d -> d.getFilter().isMotor())){
                items.add(createMenuItem("spread motor neuron", event -> spreadMotor(chart)));
            }
        }
        //
        if (ls.size() > 1){
            SingleSpikeFilter ff = ls.get(0).getFilter();
            if (ls.stream().anyMatch(d -> !d.getFilter().isSameWorm(ff))){
                items.add(createMenuItem("spread different worm", event -> spreadDifferentWorm(chart)));
            }
        }
        //
        Menu pickOutMenu = new Menu("pick out neuron");
        ObservableList<MenuItem> pickOutMenuItems = pickOutMenu.getItems();
        ls.stream()
          .map(FRChartData::getFilter)
          .map(f -> createMenuItem(spikeFilterLongFormat(f), event -> spreadSingleNeuron(chart, f)))
          .forEach(pickOutMenuItems::add);
        items.add(pickOutMenu);
        //
        items.add(new SeparatorMenuItem());
        //
        items.add(createMenuItem("delete", event -> delete(chart, true)));
        //
        Menu deleteMenu = new Menu("delete neuron");
        ObservableList<MenuItem> deleteItems = pickOutMenu.getItems();
        ls.stream()
          .map(FRChartData::getFilter)
          .map(f -> createMenuItem(spikeFilterLongFormat(f), event -> delete(chart, f)))
          .forEach(deleteItems::add);
        items.add(deleteMenu);
        //
        items.add(new SeparatorMenuItem());
        //
        menu.show(primaryStage, e.getScreenX(), e.getScreenY());
    }

    private void spreadSensor(LineChart source){
        List<FRChartData> ls = charts.get(source);
        if (ls == null) return;
        List<FRChartData> lt = ls.stream().filter(d -> d.getFilter().isSensor()).collect(Collectors.toList());
        if (lt.isEmpty()) return;
        Log.debug(() -> "spread sensor : " + ID(source));
        content.getChildren()
          .add(moveData(source, "Sensor : " + getChartTitle(lt.get(0).getFilter()), lt));
        resetChartSize();
    }

    private void spreadMotor(LineChart source){
        List<FRChartData> ls = charts.get(source);
        if (ls == null) return;
        List<FRChartData> lt = ls.stream().filter(d -> d.getFilter().isMotor()).collect(Collectors.toList());
        if (lt.isEmpty()) return;
        Log.debug(() -> "spread motor : " + ID(source));
        content.getChildren()
          .add(moveData(source, "Motor : " + getChartTitle(lt.get(0).getFilter()), lt));
        resetChartSize();
    }

    private void spreadDifferentWorm(LineChart source){
        List<FRChartData> ls = charts.get(source);
        if (ls == null) return;
        List<Map.Entry<SingleSpikeFilter, List<FRChartData>>> group = ls.stream()
          .collect(Collectors.groupingBy(fr -> fr.getFilter().toWormIdentify()))
          .entrySet()
          .stream()
          .collect(Collectors.toList());
        if (group.isEmpty() || group.size() == 1) return;
        Log.debug(() -> "spread different worms : " + ID(source));
        source.setTitle(getChartTitle(group.get(0).getKey()));
        for (int i = 1, size = group.size(); i < size; i++){
            Map.Entry<SingleSpikeFilter, List<FRChartData>> e = group.get(i);
            content.getChildren()
              .add(moveData(source, getChartTitle(e.getKey()), e.getValue()));
        }
        resetChartSize();
    }

    private void spreadSingleNeuron(LineChart source, SingleSpikeFilter ff){
        List<FRChartData> ls = charts.get(source);
        if (ls == null) return;
        FRChartData data = ls.stream().filter(d -> d.getFilter().equals(ff)).findFirst().orElse(null);
        if (data == null) return;
        Log.debug(() -> "spread single neuron : " + ID(source) + " : " + ff);
        content.getChildren()
          .add(moveData(source, spikeFilterLongFormat(data.getFilter()), Collections.singletonList(data)));
        resetChartSize();
    }

    private LineChart moveData(LineChart source, String newTitle, List<FRChartData> data){
        List<FRChartData> ls = Objects.requireNonNull(charts.get(source));
        if (ls != data){
            ls.removeAll(data);
        }
        LineChart newChart = newLineChart(newTitle);
        data.forEach(s -> {
            source.getData().remove(s.getChartSeries());
            newChart.getData().add(s.newChartSeries());
        });
        charts.put(newChart, new ArrayList<>(data));
        return newChart;
    }

    private void delete(LineChart source, boolean deleteFromLoader){
        String identify = ID(source);
        List<FRChartData> ls = charts.remove(source);
        if (ls == null) return;
        Log.debug(() -> "delete chart : " + ID(source));
        if (deleteFromLoader){
            ls.stream().map(FRChartData::getFilter).forEach(dataLoader::removeFilter);
        }
        content.getChildren().remove(source);
        identifyMap.remove(identify);
        resetChartSize();
    }

    private void delete(LineChart source, SingleSpikeFilter ff){
        List<FRChartData> ls = charts.get(source);
        if (ls == null) return;
        FRChartData data = ls.stream().filter(d -> d.getFilter().equals(ff)).findFirst().orElse(null);
        if (data == null) return;
        Log.debug(() -> "delete chart : " + ID(source) + " : " + ff);
        ls.remove(data);
        source.getData().remove(data.getChartSeries());
        if (ls.isEmpty()){
            delete(source, true);
        }
    }

    private void mergeChart(LineChart source, LineChart target){
        if (source == target) return;
        List<FRChartData> ls = charts.get(source);
        List<FRChartData> lt = charts.get(target);
        if (ls == null || lt == null) return;
        Log.debug(() -> "merge chart : " + ID(source) + " -> " + ID(target));
        lt.addAll(ls);
        ls.forEach(s -> target.getData().addAll(s.newChartSeries()));
        delete(source, false);
    }

    private void moveChart(LineChart source, LineChart target){
        ObservableList<Node> nodes = content.getChildren();
        if (target != null){
            Log.debug(() -> "move chart : " + ID(source) + " ^ " + ID(target));
            int index = nodes.indexOf(target);
            nodes.add(index, moveData(source, source.getTitle(), charts.get(source)));
        } else {
            Log.debug(() -> "move chart : " + ID(source) + " to last");
            nodes.add(moveData(source, source.getTitle(), charts.get(source)));
        }
        delete(source, false);
    }

    private static String ID(Object o){
        return o.getClass().getName() + "@" + o.hashCode();
    }

    private void handleKeyEvent(KeyEvent e){
        switch (e.getCode()){
        //XXX if setPause(true) from mouseEvent, it cause SPACE no response
//        case SPACE:
        case P:
            setPause(!isPause());
            break;
        default:
            return;
        }
        e.consume();
    }
}
