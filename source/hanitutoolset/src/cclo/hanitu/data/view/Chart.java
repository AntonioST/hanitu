/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.function.BiConsumer;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import cclo.hanitu.Log;
import cclo.hanitu.Executable;
import cclo.hanitu.HanituInfo.Argument;
import cclo.hanitu.HanituInfo.Description;
import cclo.hanitu.HanituInfo.Option;
import cclo.hanitu.data.*;
import cclo.hanitu.gui.FXMain;

/**
 * @author antonio
 */
public class Chart extends Executable{

    @Option(shortName = 'w', value = "window", arg = "SIZE", order = 1)
    @Description("set the time window size. default is 100 ms.")
    public int timeWindow = 100;

    @Option(shortName = 's', value = "step", arg = "SIZE", order = 1)
    @Description("set time step. default is 20 ms.")
    public int timeStep = 20;

    @Option(shortName = 'r', value = "remain", arg = "DURING", order = 2)
    @Description("set the total time painting on the chart, default is 1000 ms")
    public int timeRemain = 1000;

    @Option(shortName = 'A', order = 3)
    @Description("auto add")
    public boolean autoAdd;

    @Option(shortName = 'o', order = 3)
    @Description("output firing rate data")
    public boolean output;

    @Argument(index = 0, value = "FILE")
    @Description("Spike data file")
    public String spikeFilename;
    public DataLoader<SpikeData> spikeSource;
    //
    private final List<SingleSpikeFilter> filters = new ArrayList<>();
    private FRChartDataLoader loader;
    private FRChart stage;

    @Option(shortName = 'a', arg = "USER,WORN,NEURON[,TYPE]", order = 0, description = "add filter and see its firingrate chart")
    @Override
    public boolean extendOption(String key, String value, ListIterator<String> it){
        switch (key){
        case "a":
            parseSingleSpikeFilter(value);
            break;
        default:
            return false;
        }
        return true;
    }

    private void parseSingleSpikeFilter(String value){
        String[] ele = value.split(",");
        Stream<SingleSpikeFilter> s;
        if (ele.length == 4){
            s = parseSingleSpikeFilter(ele[0], ele[1], ele[2], ele[3]);
        } else if (ele.length == 3){
            s = parseSingleSpikeFilter(ele[0], ele[1], ele[2], null);
        } else {
            throw new IllegalArgumentException("wrong format : " + value);
        }
        s.forEach(this::addSingleSpikeFilter);
    }

    private Stream<SingleSpikeFilter> parseSingleSpikeFilter(String user, String worm, String neuron, String type){
        return expendExpression(neuron).mapToObj(Integer::valueOf)
          .flatMap(u -> expendExpression(worm).mapToObj(Integer::valueOf)
            .flatMap(w -> expendExpression(neuron).mapToObj(Integer::valueOf)
              .flatMap(n -> {
                  if (type == null){
                      return Stream.of(new SingleSpikeFilter(u, w, n, 'b'));
                  } else {
                      return IntStream.range(0, type.length())
                        .mapToObj(i -> new SingleSpikeFilter(u, w, n, type.charAt(i)));
                  }
              })));
    }

    protected static IntStream expendExpression(String expr){
        if (expr.contains(":")){
            int i = expr.indexOf(":");
            return IntStream.rangeClosed(Integer.parseInt(expr.substring(0, i)),
                                         Integer.parseInt(expr.substring(i + 1)));
        } else {
            return IntStream.of(Integer.parseInt(expr));
        }
    }

    public void addSingleSpikeFilter(SingleSpikeFilter f){
        Log.debug(() -> "add filter : " + f);
        filters.add(f);
    }

    public DataLoader<SpikeData> setupSpikeSource() throws IOException{
        if (spikeSource == null){
            if (spikeFilename == null || spikeFilename.equals("-")){
                spikeSource = new SpikeLoader.Stream(System.in);
            } else {
                spikeSource = new SpikeLoader.File(spikeFilename);
            }
        }
        return spikeSource;
    }

    public FRChartDataLoader setupLoader() throws IOException{
        if (loader == null){
            loader = new FRChartDataLoader(setupSpikeSource());
            loader.setTimeWindow(timeWindow);
            loader.setTimeStep(timeStep);
            loader.setTimeRemain(timeRemain);
            filters.forEach(loader::addFilter);
            loader.setAutoCreateData(autoAdd);
            if (output){
                BiConsumer<SingleSpikeFilter, FiringRateData> outputFunction
                  = (singleSpikeFilter, firingRateData) -> {
                    System.out.printf("%.4f\t%d\t%d\t%d\t%c\t%.4f\n",
                                      firingRateData.time,
                                      singleSpikeFilter.user,
                                      singleSpikeFilter.worm,
                                      singleSpikeFilter.neuron,
                                      singleSpikeFilter.type,
                                      firingRateData.rate);
                };
                if (autoAdd){
                    loader.addDownstream(null, outputFunction);
                } else {
                    filters.forEach(f -> loader.addDownstream(f, outputFunction));
                }
            }
        }
        return loader;
    }

    public FRChart setupChart() throws IOException{
        if (stage == null){
            stage = new FRChart();
            stage.setDataLoader(setupLoader());
        }
        return stage;
    }

    @Override
    public void start() throws IOException{
        try {
            FXMain.launch(setupChart());
        } finally {
            if (spikeSource != null){
                spikeSource.close();
            }
        }
    }
}
