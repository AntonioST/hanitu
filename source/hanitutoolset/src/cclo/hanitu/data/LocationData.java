/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.io.Serializable;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Location data
 *
 * @author antonio
 */
public class LocationData implements Comparable<LocationData>, Serializable{

    /**
     * event time in unit ms.
     */
    public final double time;
    private final WormData[] wormLocData;
    private static final int FACTOR = 10; //mm -> 0.1mm

    /**
     * create location data
     *
     * @param time event time in unit ms
     * @param data location data for worms
     */
    public LocationData(double time, WormData... data){
        this.time = time;
        wormLocData = data;
    }

    /**
     * parsing location line.
     *
     * @param line line in the location files
     * @return location data
     */
    public static LocationData forString(String line){
        if (line == null || line.isEmpty()){
            throw new IllegalArgumentException("empty or null string");
        }
        String[] lines = line.split("\t| +");
        double time = Double.parseDouble(lines[0]);
        WormData[] info = new WormData[(lines.length - 1) / 5];
        int index = 0;
        int offset = 1;
        int length = lines.length;
        while (offset + 4 < length){
            WormData w = new WormData();
            w.user = Integer.parseInt(lines[offset++]);
            w.worm = Integer.parseInt(lines[offset++]);
            w.x = (int)(Double.parseDouble(lines[offset++]) * FACTOR);
            w.y = (int)(Double.parseDouble(lines[offset++]) * FACTOR);
            w.hp = Double.parseDouble(lines[offset++]);
            info[index++] = w;
        }
        if (offset < length){
            throw new IllegalArgumentException("no-complete input data");
        }
        return new LocationData(time, info);
    }

    /**
     * apply each location data of worms
     *
     * @param c
     */
    public void forEach(Consumer<WormData> c){
        for (WormData anInfo : wormLocData){
            c.accept(anInfo);
        }
    }

    @Override
    public int compareTo(LocationData o){
        return Double.compare(time, o.time);
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocationData that = (LocationData)o;

        if (Double.compare(that.time, time) != 0) return false;
        if (!Arrays.equals(wormLocData, that.wormLocData)) return false;

        return true;
    }

    @Override
    public int hashCode(){
        int result;
        long temp;
        temp = Double.doubleToLongBits(time);
        result = (int)(temp ^ (temp >>> 32));
        result = 31 * result + Arrays.hashCode(wormLocData);
        return result;
    }

    @Override
    public String toString(){
        return String.format("%.1f", time) + Stream.of(wormLocData)
          .map(w -> String.format("%d\t%d\t%.1f\t%.1f\t%.4f", w.user, w.worm, w.x / FACTOR, w.y / FACTOR, w.hp))
          .collect(Collectors.joining("\t", "\t", ""));
    }
}
