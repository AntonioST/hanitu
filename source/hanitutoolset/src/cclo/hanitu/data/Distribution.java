/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

/**
 * @author antonio
 */
public class Distribution{

    /**
     * diffusion constant in unit cm * cm / src
     */
    private double diffusion = 1.86e-5;
    /**
     * in unit cm
     */
    private double depth = 0.264;
    /**
     * in unit sec
     */
    private double delayTime;
    /**
     * in unit mM
     */
    private double concentration = 100;
    /**
     *
     */
    private int count = 1;

    public Distribution(){
    }

    public double getDiffusion(){
        return diffusion;
    }

    public void setDiffusion(double diffusion){
        if (diffusion <= 0) throw new IllegalArgumentException("negative or zero diffusion constant : " + diffusion);
        this.diffusion = diffusion;
    }

    public double getDepth(){
        return depth;
    }

    public void setDepth(double depth){
        if (depth <= 0) throw new IllegalArgumentException("negative or zero depth : " + depth);
        this.depth = depth;
    }

    public double getDelayTime(){
        return delayTime;
    }

    public void setDelayTime(double delayTime){
        if (delayTime < 0) throw new IllegalArgumentException("negative delay time");
        if (delayTime == 0){
            this.delayTime = 1e-5;
        } else {
            this.delayTime = delayTime;
        }
    }

    public double getConcentration(){
        return concentration;
    }

    /**
     * @param distance in unit mm
     * @return
     */
    public double getConcentration(double distance){
        if (count == 0) return 0;
        if (concentration == 0) return 0;
        double N = 1e-3 * count * concentration;
        double r2 = distance * distance * 1e-4;
        return N / (4 * Math.PI * diffusion * depth)
               * Math.exp(-r2 / (4 * diffusion * delayTime))
               / delayTime;
    }

    public void setConcentration(double concentration){
        if (concentration < 0) throw new IllegalArgumentException("negative concentration : " + concentration);
        this.concentration = concentration;
    }

    public int getCount(){
        return count;
    }

    public void setCount(int count){
        if (count < 0) throw new IllegalArgumentException("negative count : " + count);
        this.count = count;
    }
}
