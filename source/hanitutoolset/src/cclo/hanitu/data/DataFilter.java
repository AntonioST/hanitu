/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * @author antonio
 */
public class DataFilter<T> extends DataLoader<T>{

    private final DataLoader<T> source;
    private final Predicate<? super T> test;

    public DataFilter(Predicate<? super T> test, DataLoader<T> source){
        this.source = Objects.requireNonNull(source);
        this.test = Objects.requireNonNull(test);
    }

    @Override
    protected T tryGetNextData(){
        T ret;
        for (; ; ){
            ret = source.tryGetNextData();
            if (ret == null) return null;
            if (test.test(ret)) break;
        }
        return ret;
    }

    @Override
    public void close(){
        source.close();
        super.close();
    }
}
