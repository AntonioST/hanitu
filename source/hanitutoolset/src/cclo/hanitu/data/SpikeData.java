/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.io.Serializable;

/**
 * the data class contain the information of the simulation time and the spiking neuron ID.
 *
 * @author antonio
 */
public class SpikeData implements Comparable<SpikeData>, Serializable{

    /**
     * simulation time in unit msec
     */
    public final double time;
    /**
     * neuron ID
     */
    public final int neuron;
    /**
     * user ID
     */
    public final int user;
    /**
     * worm ID
     */
    public final int worm;
    /**
     * neuron type
     */
    public final char type;

    public SpikeData(double time, int neuron){
        this.time = time;
        this.neuron = neuron;
        user = -1;
        worm = -1;
        type = 'b';
    }

    public SpikeData(double time, int user, int worm, int neuron, char type){
        if ("bsm".indexOf(type) < 0) throw new IllegalArgumentException("illegal neuron type : " + type);
        this.time = time;
        this.user = user;
        this.worm = worm;
        this.neuron = neuron;
        this.type = type;
    }

    public static SpikeData parseTimeSerial(String line){
        String[] sp = line.split("[ \t]+", 2);
        try {
            return new SpikeData(Double.parseDouble(sp[0]), -1);
        } catch (NumberFormatException ex){
            throw new IllegalArgumentException("wrong spike data format : " + line, ex);
        }
    }

    public static SpikeData parseWormSpike(String line){
        String[] sp = line.split("[ \t]+");
        int length = sp.length;
        try {
            if (length == 2){
                return new SpikeData(Double.parseDouble(sp[0]), Integer.parseInt(sp[1]));
            } else if (length == 3){
                if (sp[2].length() != 1) throw new IllegalArgumentException("illegal neuron type : " + sp[2]);
                return new SpikeData(Double.parseDouble(sp[0]),
                                     -1,
                                     -1,
                                     Integer.parseInt(sp[1]),
                                     sp[2].charAt(0));
            } else {
                throw new IllegalArgumentException("wrong spike data format");
            }
        } catch (NumberFormatException ex){
            throw new IllegalArgumentException("wrong spike data format : " + line, ex);
        }
    }

    public static SpikeData parseWormsSpike(String line){
        String[] sp = line.split("[ \t]+");
        int length = sp.length;
        try {
            if (length == 4){
                return new SpikeData(Double.parseDouble(sp[0]),
                                     Integer.parseInt(sp[1]),
                                     Integer.parseInt(sp[2]),
                                     Integer.parseInt(sp[3]),
                                     'b');
            } else if (length == 5){
                if (sp[4].length() != 1) throw new IllegalArgumentException("illegal neuron type : " + sp[4]);
                return new SpikeData(Double.parseDouble(sp[0]),
                                     Integer.parseInt(sp[1]),
                                     Integer.parseInt(sp[2]),
                                     Integer.parseInt(sp[3]),
                                     sp[4].charAt(0));
            } else {
                throw new IllegalArgumentException("wrong spike data format");
            }
        } catch (NumberFormatException ex){
            throw new IllegalArgumentException("wrong spike data format : " + line, ex);
        }
    }

    @Override
    public int compareTo(SpikeData o){
        return Double.compare(time, o.time);
    }

    @Override
    public String toString(){
        return String.format("%.1f\t%d\t%d\t%d\t%c", time, user, worm, neuron, type);
    }

}
