/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.function.Consumer;

/**
 * @author antonio
 */
public class DataPusher<T> extends DataLoader<T> implements Consumer<T>{

    private final Deque<T> queue = new LinkedBlockingDeque<>();

    @Override
    public void accept(T t){
        queue.add(t);
    }

    @Override
    protected T tryGetNextData(){
        return queue.pollFirst();
    }

    @Override
    public boolean isClose(){
        return close && queue.isEmpty();
    }
}
