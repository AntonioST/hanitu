/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.util.*;

import cclo.hanitu.gui.FXMain;

import static cclo.hanitu.HanituInfo.*;
import static cclo.hanitu.HelpBuilder.*;
import static cclo.hanitu.Log.PROPERTY_OUTPUT;

/**
 * The default class used to print the general help document.
 */
@HanituInfo.Tip("Main")
@Description("The Information of the Hanitu Tool Set")
@HanituInfo.Usage({"TOOL", "[OPTION]", "[ARGS]"})
public class Default extends Executable{

    @Option(shortName = 'h', value = "help", order = 0)
    @Description("~*r{Global Options~} : show this document.\n" +
                 "you can also use ~*{--help~} with a ~_{TOOL~} name to show detail information.\n" +
                 "use ~*{--help!~} to see the non-standard options of the ~_{TOOL~} which is hidden " +
                 "under the normal help document. Be careful use this options, because those " +
                 "options just be used in testing and will be removed or renamed or anything " +
                 "in the future.")
    public boolean help;

    @Option(value = "version", order = 1)
    @Description("~*r{Global Options~} : show application version and exit.")
    public boolean version;

    @Option(value = "debug", standard = false)
    @Description("~*r{Global Options~} : run program in debug mode\n"
                 + "print debug information to a file or the standard error.\n"
                 + "use system property '" + PROPERTY_OUTPUT + "' to set output target")
    public boolean debug;

    @Option(value = "about", standard = false)
    @Description("show about dialog")
    public boolean about;

    @Option(value = "check", standard = false)
    @Description("checking hanitu environment. no output any message represent all checking passed")
    public boolean checkEnv;

    @Option(value = "print-self", standard = false)
    @Description("print the executable command show in usage section in help document")
    public boolean printUsage;

    @Option(value = "print-version", standard = false)
    @Description("print the version of each sub-programs")
    public boolean printVersion;

    @Option(value = "list-classpath", standard = false)
    @Description("list the classpath")
    public boolean listClassPath;

    @Option(value = "list-path", standard = false)
    @Description("list path which get from system environment")
    public boolean listEnvPath;

    @Option(value = "list-all", standard = false)
    @Description("list all executable tool")
    public boolean listAllTool;

    public final List<String> arguments = new ArrayList<>();

    @Override
    public boolean extendArgument(int index, String value, ListIterator<String> it){
        arguments.add(value);
        return true;
    }

    @Override
    public String extendHelpDoc(){
        StringBuilder sb = new StringBuilder();
        StringBuilder tmp = new StringBuilder();
        sb.append(DOC_TITLE).append("list of ~_{TOOL~}");
        sb.append(DOC_BLOCK_START);
        for (String line : align(Main.SHORTCUT, Comparator.comparing(k -> Main.SHORTCUT.get(k).getName()))){
            sb.append(DOC_LIST_0).append(line);
        }
        sb.append(DOC_BLOCK_END);
        //version
        sb.append(DOC_NEWLINE).append(Message.getMessage("title.version"));
        sb.append(DOC_BLOCK_START).append(VERSION).append(DOC_BLOCK_END);
        //license
        sb.append(DOC_NEWLINE).append(Message.getMessage("title.license"));
        sb.append(DOC_BLOCK_START).append(LICENSE).append(DOC_BLOCK_END);
        //author
        sb.append(DOC_NEWLINE).append(Message.getMessage("title.authors"));
        tmp.setLength(0);
        align(tmp, AUTHORS, 0);
        sb.append(DOC_BLOCK_START).append(tmp).append(DOC_BLOCK_END);
        //group
        sb.append(DOC_NEWLINE).append(Message.getMessage("title.group"));
        sb.append(DOC_BLOCK_START);
        for (String line : DEVELOPER_DESCRIPTION.split("\n")){
            sb.append(line).append("\n");
        }
        sb.append(DOC_BLOCK_END);
        //website
        sb.append(DOC_NEWLINE).append(Message.getMessage("title.website"));
        tmp.setLength(0);
        align(tmp, WEB_SITE, 0);
        sb.append(DOC_BLOCK_START).append(tmp).append(DOC_BLOCK_END);
        return sb.toString();
    }

    @Override
    public void start() throws Throwable{
        if (printUsage){
            if (Base.SELF_0 != null){
                System.out.println(Base.SELF_0);
            } else {
                System.out.println(Base.SELF_J);
            }
        }
        if (listAllTool){
            System.out.println("Tool");
            Map<String, String> map = new HashMap<>();
            Main.SHORTCUT.forEach((k, v) -> map.put(k, v.getName()));
            Main.SHORTCUT_HIDE.forEach((k, v) -> map.put(k, v.getName()));
            HelpBuilder.align(map, Comparator.comparing(map::get))
              .forEach(System.out::println);
        }
        if (listClassPath){
            System.out.println("ClassPath");
            NativeExecutable.listClassPath().forEach(p -> System.out.println("  " + p));
        }
        if (listEnvPath){
            System.out.println("EnvPath");
            NativeExecutable.listEnvPath().forEach(p -> System.out.println("  " + p));
        }
        if (checkEnv){
            System.out.println("jar file location : " + Base.HANITU_JAR);
            if (Base.HOME == null){
                System.err.println("set property '" + Base.PROPERTY_HOME + "' to the hanitu install path");
                System.err.println("  use 'java -D" + Base.PROPERTY_HOME + "=... ...'");
                System.err.println("  or use 'env " + Base.PROPERTY_HOME_ENV + "=... java ...'");
                System.err.println("  otherwise, use the location of the hanitu jar file");
                System.err.println();
            } else {
                System.out.println(Base.PROPERTY_HOME + "=" + Base.HOME);
            }
            if (Base.SELF_0 == null){
                System.err.println("set property '" + Base.PROPERTY_0 + "' the name of the hanitu caller");
                System.err.println("  this property only used in help document.");
                System.err.println("  use 'java -D" + Base.PROPERTY_0 + "=... ...");
                System.err.println("  By default, usage command will use 'java' instead");
                System.err.println();
            }
            try {
                Flysim.getInstance();
            } catch (Exception e){
                System.err.println(e.getMessage());
                System.err.println("  Please set this property to the flysim install path");
                System.err.println("  or put flysim at PATH");
                System.err.println();
            }
            try {
                new Hanitu();
            } catch (Exception e){
                System.err.println(e.getMessage());
                System.err.println("  Please set this property to the hanitu install path");
                System.err.println("  or put hanitu at PATH");
                System.err.println();
            }
        }
        if (printVersion){
            StringBuilder tmp = new StringBuilder();
            HelpBuilder.align(tmp,
                              new String[][]{{"Hanitu.main", HanituInfo.VERSION},
                                             {"Hanitu.toolset", HanituInfo.TOOLSET_VERSION},
                                             {"Hanitu.flysim", Flysim.getInstance().version()},
                                             {"Hanitu.hanitu", new Hanitu().version()}},
                              0);
            System.out.println(tmp);
        }
        if (about){
            FXMain.launch(new About(arguments));
        }
    }
}
