/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cclo.hanitu.util.StringSimilar;

import static cclo.hanitu.FileLoader.META_HEADER;
import static cclo.hanitu.FileLoader.startsWithCaseInsensitive;

/**
 * @author antonio
 */
public abstract class FileLoaderImpl<T>{

    /**
     * terminal loading with error
     */
    public static final int ERROR_STATE = -2;
    /**
     * terminal loading normally.
     */
    public static final int TERMINAL_STATE = -1;
    /**
     * initial state number
     */
    public static final int INIT_STATE = 0;

    protected FileLoader<T> loader;
    int state;

    public FileLoader<T> getLoader(){
        return loader;
    }

    public abstract String getSourceVersion();

    public abstract String versionRequired();

    public void startLoading(FileLoader<T> loader){
        this.loader = loader;
        state = INIT_STATE;
    }

    public abstract T endLoading();

    public abstract void parseLine(String line);

    /**
     * handle the meta data at the whole of the file except the header.
     *
     * @param key   meta key
     * @param value meta value, null if empty
     * @return accept this meta or not.
     */
    protected boolean parseMeta(String key, String value){
        switch (key){
        case FileLoader.META_MESSAGE:
            loader.putResource(FileLoader.META_MESSAGE, value);
            break;
        default:
            return false;
        }
        return true;
    }


    /**
     * get current state
     *
     * @return current state
     */
    public int getCurrentState(){
        return state;
    }

    /**
     * change state.
     *
     * negative state number represent a error state which will stopping the parsing loop and throw a `IOException`.
     *
     * @param currentState new state number
     * @throws IllegalStateException change state in rescue mode
     */
    public void setState(int currentState){
        if (loader.rescueMode){
            throw new IllegalStateException("it is in rescue state");
        }
        if (currentState < TERMINAL_STATE){
            Log.debug(() -> "input : '" + getLoader().getOriginalLine() + "'");
            Log.debug(() -> "~*r{state~} : " + getStateDescription(state)
                            + " ~*y{->~} " + getStateDescription(currentState));
        }
        state = currentState;
    }

    public String getCurrentStatDescription(){
        return getStateDescription(state);
    }

    /**
     * get the description of the state.
     *
     * this method will use reflection to get the the static final field information of the its subclass. Also checking
     * the field name contain `STATE`. If searching fail, return `(state)` instead.
     *
     * @param state state number
     * @return the name of the state
     */
    public String getStateDescription(int state){
        for (Class c = getClass(); c != Object.class; c = c.getSuperclass()){
            String ret = getStateDescription(c, state);
            if (ret != null) return ret;
        }
        return "(" + state + ")";
    }

    private static String getStateDescription(Class c, int state){
        for (Field f : c.getDeclaredFields()){
            //static final int *STATE*
            if (f.getType() != int.class) continue;
            int m = f.getModifiers();
            if (!Modifier.isFinal(m) || !Modifier.isStatic(m)) continue;
            String name = f.getName();
            if (!name.contains("STATE")) continue;
            if (!Modifier.isPublic(m)){
                f.setAccessible(true);
            }
            try {
                if (f.getInt(null) == state) return name;
            } catch (IllegalAccessException e){
            }
        }
        return null;
    }

    protected static String getKey(String line){
        if (startsWithCaseInsensitive(line, META_HEADER)) return META_HEADER;
        int index = line.indexOf("=");
        if (index < 0) return line;
        return line.substring(0, index);
    }

    protected static String getValue(String line){
        int index = line.indexOf("=");
        if (index < 0) return null;
        if (index == line.length() - 1) return "";
        return line.substring(index + 1);
    }


    /**
     * Get the key from the meta data line.
     *
     * @param line meta data line
     * @return key, null if `line` doesn't a meta data line
     */
    protected static String getMetaKey(String line){
        if (!startsWithCaseInsensitive(line, META_HEADER)) throw new IllegalArgumentException("not a meta : " + line);
        int from = META_HEADER.length();
        int index = line.indexOf("=", from);
        if (index < 0) return line.substring(from);
        return line.substring(from, index);
    }


    /**
     * Get the value from the meta data line.
     *
     * @param line meta data line
     * @return value, null if `line` doesn't a meta data line or empty value
     */
    protected static String getMetaValue(String line){
        if (!startsWithCaseInsensitive(line, META_HEADER)) throw new IllegalArgumentException("not a meta : " + line);
        if (!line.contains("=")) return "";
        int index = line.indexOf("=", META_HEADER.length());
        if (index == line.length() - 1) return "";
        return line.substring(index + 1);
    }


    /**
     * Get the int value from the `line`. Default value is `-1` if any exception is thrown.
     *
     * If the format of the value is wrong, it will try to rescue by extra the digit part in the value string.
     *
     * @param line Key=Value
     * @return int value
     */
    protected int getIntValue(String line){
        String ret = getValue(line);
        if (ret == null || ret.isEmpty()){
            throwLostValueException();
        } else {
            try {
                return Integer.parseInt(ret);
            } catch (NumberFormatException ex){
                Matcher m = Pattern.compile("[+-]?\\d+").matcher(ret);
                if (m.find()){
                    int v = Integer.parseInt(m.group());
                    Log.debug(() -> Message.format("loader.rescue.value", line, v));
                    loader.throwException("illegal value",
                                          ex.getMessage(),
                                          "~*g{state~} : " + getStateDescription(state),
                                          "do you mean : " + v);
                    return v;
                } else {
                    Log.debug(() -> "rescue fail : '" + line + "'");
                    loader.throwException("illegal value",
                                          ex.getMessage(),
                                          "~*g{state~} : " + getStateDescription(state));
                }
            }
        }
        return -1;
    }

    /**
     * get the floating value from the `line`. Default value is {@link Double#NaN} if any exception is thrown.
     *
     * If the format of the value is wrong, it will try to rescue by extra the digit part in the value string.
     *
     * @param line Key=Value
     * @return double Value
     */
    protected double getFloatValue(String line){
        String ret = getValue(line);
        if (ret == null || ret.isEmpty()){
            throwLostValueException();
        } else {
            try {
                return Double.parseDouble(ret);
            } catch (NumberFormatException ex){
                Matcher m = Pattern.compile("[+-]?\\d+(\\.\\d+)?([eE][+-]?\\d+)?").matcher(ret);
                if (m.find()){
                    double v = Double.parseDouble(m.group());
                    Log.debug(() -> Message.format("loader.rescue.value", line, v));
                    loader.throwException("illegal value",
                                          ex.getMessage(),
                                          "~*g{state~} : " + getStateDescription(state),
                                          "do you mean : " + v);
                    return v;
                } else {
                    Log.debug(() -> "rescue fail : '" + line + "'");
                    loader.throwException("illegal value",
                                          ex.getMessage(),
                                          "~*g{state~} : " + getStateDescription(state));
                }
            }
        }
        return Double.NaN;
    }

    /**
     * get the value from the `line`. Default value is null if any exception is thrown.
     *
     * @param line Key=Value
     * @return Value
     */
    protected String getNonNullStrValue(String line){
        String ret = getValue(line);
        if (ret == null || ret.isEmpty()){
            throwLostValueException();
            return null;
        } else {
            return ret;
        }
    }


    /**
     * generate a error message and record it. cause a {@link IOException} after parsed file.
     *
     * @param title   error title
     * @param message error message
     */
    public void throwError(String title, String... message){
        loader.throwError(title, message);
    }

    /**
     * generate a warning message and record it. It doesn't cause a {@link IOException} after parsed file if option
     * "fail on load" is false.
     *
     * @param title   warning title
     * @param message warning message
     */
    public void throwException(String title, String... message){
        loader.throwException(title, message);
    }

    /**
     * generate a error message for un-expected keyword appear at certain place.
     *
     * @param expect expected keyword
     */
    public void throwErrorKeywordException(String expect){
        loader.throwError("unexpected line",
                          "~*g{state~} : " + getStateDescription(state),
                          "~*g{expect~} : " + expect);
    }

    public String throwUnexpectedKeywordException(String currentLine, String... expect){
        return throwUnexpectedKeywordException(currentLine, Arrays.asList(expect));
    }

    /**
     * generate a warning message for un-expected keyword appear at certain place.
     *
     * @param currentLine current input line which will be tested
     * @param expectSet candidate expectSet keywords.
     * @return the most similar keyword, null if candidate set is empty.
     */
    public String throwUnexpectedKeywordException(String currentLine, List<String> expectSet){
        if (!expectSet.isEmpty()){
            String expect = StringSimilar.findSimilar(currentLine, expectSet);
            if (StringSimilar.similar(expect, currentLine) > 0.5){
                loader.throwException("unexpected line",
                                      "~*g{state~} : " + getStateDescription(state),
                                      "do you mean : '" + expect + "'");
                return expect;
            }
        }
        loader.throwException("unknown line",
                              "~*g{state~} : " + getStateDescription(state));
        return null;
    }

    /**
     * generate a warning message for a property setting without a value.
     */
    public void throwLostValueException(){
        loader.throwException("value lost",
                              "~*g{state~} : " + getStateDescription(state));
    }
}
