/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import cclo.hanitu.gui.PaintArea;

/**
 * @author antonio
 */
abstract class AbstractCircuitPainter extends PaintArea{

    protected VisualCircuit visual;

    public AbstractCircuitPainter(){
    }

    public AbstractCircuitPainter(int x, int y, int w, int h){
        super(x, y, w, h);
    }

    public AbstractCircuitPainter(int w, int h){
        super(w, h);
    }

    public void setup(VisualCircuit visual){
        this.visual = visual;
    }

    public void destroy(){
        visual = null;
    }

    @Override
    public void paint(GraphicsContext g){
        //worm body
        paintWormBody(g);
        //synapse
        visual.synapseViewers.stream()
          .filter(s -> s.preSynAxon != null && s.postSynRep != null)
          .forEach(s -> paintSynapseLine(g, s));
        //body neuron
        for (int i = 0; i < 4; i++){
            //food sensor
            {
                SensorViewer target = visual.foodSensorViewer[i];
                paintSensor(g, target);
                paintSensorText(g, target);
                paintSensorAxon(g, target.axon);
            }
            //toxicant sensor
            {
                SensorViewer target = visual.toxicantSensorViewer[i];
                paintSensor(g, target);
                paintSensorText(g, target);
                paintSensorAxon(g, target.axon);
            }
            //motor
            {
                MotorViewer target = visual.motorViewer[i];
                paintMotor(g, target);
                paintMotorText(g, target);
                paintMotorReceptor(g, target.rep);
            }
        }
        for (NeuronViewer n : visual.neuronViewers){
            paintNeuronBody(g, n);
            paintNeuronText(g, n);
            paintNeuronAxon(g, n.axon);
        }
        for (ReceptorViewer r : visual.receptorViewers){
            paintReceptorBody(g, r);
            if (isSelectedOrRelative(r)){
                paintReceptorText(g, r);
            }

        }
        visual.synapseViewers.stream()
          .filter(s -> s.synapse != null) //filter out motor synapse which synapse is null
          .filter(this::isSelectedOrRelative)
          .forEach(s -> paintSynapseText(g, s));
    }


    protected abstract void paintWormBody(GraphicsContext g);

    protected abstract void paintNeuronBody(GraphicsContext g, NeuronViewer n);

    protected abstract void paintNeuronText(GraphicsContext g, NeuronViewer n);

    protected abstract void paintNeuronAxon(GraphicsContext g, AxonViewer a);

    protected abstract void paintReceptorBody(GraphicsContext g, ReceptorViewer r);

    protected abstract void paintReceptorText(GraphicsContext g, ReceptorViewer r);

    protected abstract void paintSynapseLine(GraphicsContext g, SynapseViewer s);

    protected abstract void paintSynapseText(GraphicsContext g, SynapseViewer s);

    protected abstract void paintSensor(GraphicsContext g, SensorViewer s);

    protected abstract void paintSensorAxon(GraphicsContext g, AxonViewer a);

    protected abstract void paintSensorText(GraphicsContext g, SensorViewer n);

    protected abstract void paintMotor(GraphicsContext g, MotorViewer m);

    protected abstract void paintMotorReceptor(GraphicsContext g, ReceptorViewer r);

    protected abstract void paintMotorText(GraphicsContext g, MotorViewer n);

    protected boolean isSelected(Viewer target){
        if (visual.hasSelected() && target != null){
            if (visual.selectedViewers.contains(target)){
                return true;
            }
        }
        return false;
    }

    protected boolean isSelectedOrRelative(Viewer target){
        if (visual.hasSelected() && target != null){
            if (visual.selectedViewers.contains(target) || visual.isRelativeToSelect(target)){
                return true;
            }
        }
        return false;
    }

    protected Color getColor(Viewer target, Color unSelected, Color relative, Color selected){
        if (target == null) return unSelected;
        if (visual.isDirectSelected(target)){
            return selected;
        } else if (visual.isSelected(target) || visual.isRelativeToSelect(target)){
            return relative;
        } else if (visual.hasSelected()){
            // fade other unselected
            return new Color(unSelected.getRed(), unSelected.getGreen(), unSelected.getBlue(),
                             visual.env.fadeAlpha);
        } else {
            return unSelected;
        }
    }

    protected Color getColor(Viewer target, Color unSelected, Color selected){
        if (target == null) return unSelected;
        return getColor(target, unSelected, unSelected, selected);
    }

    protected Color getColor(Viewer target, Color c){
        if (target == null) return c;
        return getColor(target, c, c, c);
    }

    protected <T> T get(Viewer target, T unSelected, T selected){
        if (target == null) return unSelected;
        if (visual.isSelected(target)) return selected;
        if (visual.isRelativeToSelect(target)) return selected;
        return unSelected;
    }

}
