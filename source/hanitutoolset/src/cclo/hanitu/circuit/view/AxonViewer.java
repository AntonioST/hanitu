/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.util.Objects;

/**
 * the viewer for axon.
 *
 * @author antonio
 */
public class AxonViewer extends Viewer{

    final AbstractNeuronViewer neuron;

    public AxonViewer(AbstractNeuronViewer host){
        super(host.name + ":Axon");
        this.neuron = Objects.requireNonNull(host);
        size = CircuitViewEnv.ENV.axonSize >> 1;
    }

    @Override
    public boolean isRelative(Viewer other){
        return other == neuron
               || ((other instanceof SynapseViewer) && other.isRelative(this));
    }

    @Override
    public double isTouch(double x, double y){
        return isTaughtSurround(location, size, x, y);
    }

}
