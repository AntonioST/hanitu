/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javafx.scene.control.ContextMenu;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import cclo.hanitu.Base;
import cclo.hanitu.Log;
import cclo.hanitu.circuit.*;
import cclo.hanitu.gui.MenuSession;

import static cclo.hanitu.gui.MenuSession.createMenuItem;

/**
 * @author antonio
 */
public class EditCircuitMode extends VisualCircuitMode{

    Path saveFileName;
    CircuitBuilder builder;

    @Override
    protected void setup(VisualCircuit visual){
        super.setup(visual);
        Circuit circuit = visual.getCircuit();
        if (circuit != null){
            builder = new CircuitBuilder(circuit);
        } else {
            builder = new CircuitBuilder();
        }
        setCircuitInstance(builder);
        if (saveFileName != null){
            setSaveFileName(saveFileName);
        }
        Properties p = Base.loadPropertyInherit("gui");
        addMenuItems(MenuSession.MENU_EDIT, 0,
                     createMenuItem(p.getProperty("circuit.menu.edit.duplicate"), "D",
                                    e -> duplicateSelected(false)),
                     createMenuItem(p.getProperty("circuit.menu.edit.deepduplicate"), "SHIFT+D",
                                    e -> duplicateSelected(true)),
                     createMenuItem(p.getProperty("circuit.menu.edit.delete"), "X",
                                    e -> deleteSelected()));
        setOnFocusEvent("cclo.hanitu.circuit.ficus.select", (e, target) -> {
            if (e.getButton() == MouseButton.PRIMARY){
                if (e.isControlDown()){
                    if (target != null){
                        visual.appendSelect(target);
                    }
                } else {
                    visual.setSelected(target);
                }
            }
        });
        setOnFocusEvent("cclo.hanitu.circuit.focus.popup", (e, target) -> {
            if (e.getButton() == MouseButton.SECONDARY){
                if ((visual.lastSelectedTarget instanceof AxonViewer) && (target instanceof ReceptorViewer)){
                    createNewSynapse((AxonViewer)visual.lastSelectedTarget, (ReceptorViewer)target);
                } else {
                    if (target != null) visual.setSelected(target);
                    showPopupMenu(target, e);
                }
                repaintCanvas();
            }
        });
        setOnDragEvent("cclo.hanitu.circuit.drug.viewer", (e, target) -> {
            if (visual.lastMousePressedButton == MouseButton.PRIMARY){
                if (target != null){
                    visual.file.setCurrentFileModified(true);
                    dragViewer(e, target);
                }
                repaintCanvas();
            }
        });
        setOnSetCircuitEvent("cclo.hanitu.circuit.set", c -> {
            if (c != null){
                builder = new CircuitBuilder(c);
            } else {
                builder = new CircuitBuilder();
            }
            setCircuitInstance(builder);
        });
        visual.info.setEditable(true);
    }

    @Override
    protected void destroy(){
        visual.info.setEditable(false);
        setCircuitInstance(builder.createCircuit());
    }

    public void setSaveFileName(Path filePath){
        saveFileName = filePath;
        if (visual != null){
            visual.file.setCurrentFilePath(filePath);
            visual.file.setCurrentFileModified(true);
        }
    }

    public NeuronViewer createNewNeuron(double x, double y, boolean createReceptor){
        visual.file.setCurrentFileModified(true);
        CentralNeuron n = builder.createNeuron();
        n.set(CentralNeuron.NORMAL);
        Log.debug(() -> "create neuron : " + n.id);
        NeuronViewer v = new NeuronViewer(n);
        visual.neuronViewers.add(v);
        if (createReceptor){
            createNewReceptor(v); //default create a new receptor
        }
        v.setLocation(x, y);
        visual.setSelected(v);
        return v;
    }

    public ReceptorViewer createNewReceptor(NeuronViewer n){
        visual.file.setCurrentFileModified(true);
        Receptor r = builder.createReceptor(n.neuron);
        r.set(Receptor.EXCITATORY);
        Log.debug(() -> "create receptor : " + r.id + " :Neuron[" + n.neuron.id + "]");
        ReceptorViewer v = new ReceptorViewer(r, n);
        visual.receptorViewers.add(v);
        visual.setSelected(v);
        return v;
    }

    public SynapseViewer createNewSynapse(AxonViewer axon, ReceptorViewer receptor){
        //Neuron -> Neuron
        //Neuron -> Motor
        //Sensor -> Neuron
        if ((axon.neuron instanceof NeuronViewer) && (receptor.neuron instanceof NeuronViewer)){
            NeuronViewer pre = (NeuronViewer)axon.neuron;
            NeuronViewer post = (NeuronViewer)receptor.neuron;
            if (builder.getSynapse(pre.neuron, post.neuron) == null){
                Log.debug(() -> "create synapse : " + pre.neuron.id + " -> " + post.neuron.id);
                Synapse ss = builder.createSynapse(pre.neuron, receptor.receptor);
                ss.set(Synapse.NORMAL);
                SynapseViewer sv = new SynapseViewer(pre, ss, axon, receptor);
                visual.synapseViewers.add(sv);
                visual.setSelected(sv);
                return sv;
            }
        } else if ((axon.neuron instanceof NeuronViewer) && (receptor.neuron instanceof MotorViewer)){
            NeuronViewer pre = (NeuronViewer)axon.neuron;
            MotorViewer post = (MotorViewer)receptor.neuron;
            Log.debug(() -> "create synapse : " + pre.neuron.id + " -> M:" + post.direction);
            if (builder.getPreMotorNeuron(post.direction) != pre.neuron){
                visual.synapseViewers.removeIf(v -> v.postSynRep == post.rep);
            }
            builder.setPreMotorNeuron(pre.neuron, post.direction);
            SynapseViewer sv = new SynapseViewer(post, null, axon, receptor);
            visual.synapseViewers.add(sv);
            visual.setSelected(sv);
            return sv;
        } else if ((axon.neuron instanceof SensorViewer) && (receptor.neuron instanceof NeuronViewer)){
            SensorViewer pre = (SensorViewer)axon.neuron;
            NeuronViewer post = (NeuronViewer)receptor.neuron;
            if (builder.getSensorSynapse(pre.sensor.type, pre.direction, post.neuron.id) == null){
                Log.debug(() -> "create synapse : S:" + pre.direction + " -> " + post.neuron.id);
                Synapse ss = builder.createSensorSynapse(pre.sensor.type, pre.direction, receptor.receptor);
                ss.set(Synapse.NORMAL);
                SynapseViewer sv = new SynapseViewer(pre, ss, axon, receptor);
                visual.synapseViewers.add(sv);
                visual.setSelected(sv);
                return sv;
            }
        }
        return null;
    }


    public void duplicateSelected(boolean deep){
        if (visual.lastSelectedTarget instanceof NeuronViewer){
            silenceVisualEvent(true);
            //copy list. setSelect will refresh selected list.
            NeuronViewer nnv = duplicateNeuron((NeuronViewer)visual.lastSelectedTarget, deep);
            List<Viewer> list = visual.selectedViewers.stream()
              .filter(v -> (v instanceof NeuronViewer))
              .map(v -> duplicateNeuron((NeuronViewer)v, deep))
              .collect(Collectors.toList());
            silenceVisualEvent(false);
            visual.setSelected(nnv); // clear selectedList
            visual.selectedViewers.addAll(list);
        }
    }


    /**
     * duplicate the reference neuron viewer. copy neuron parameters value and receptor
     * parameters value.
     *
     * @param onv  reference neuron viewer
     * @param deep true for duplicate synapse connection
     * @return new neuron viewer
     */
    public NeuronViewer duplicateNeuron(NeuronViewer onv, boolean deep){
        Log.debug(() -> "duplicate neuron : " + onv.neuron.id);
        NeuronViewer nnv = createNewNeuron(onv.location[0] + 10, onv.location[1] + 10, false);
        List<ReceptorViewer> nrs = onv.neuron.receptors.stream()
          .map(r -> {
              ReceptorViewer nr = createNewReceptor(nnv);
              nr.receptor.set(r);
              return nr;
          }).collect(Collectors.toList());
        if (deep){
            visual.synapseViewers.stream()
              .filter(s -> s.postSynRep.neuron == onv)
                /*
                createNewSynapse will modify synapseViewers
                and it will throw ConcurrentModificationException
                */
              .collect(Collectors.toList())
              .forEach(s -> createNewSynapse(s.preSynAxon, nrs.get(s.synapse.targetReceptor.id))
                .synapse.set(s.synapse));
            for (Synapse sy : onv.neuron.targets){
                createNewSynapse(nnv.axon, visual.getReceptorViewer(sy.postSynapseNeuron.id, sy.targetReceptor.id))
                  .synapse.set(sy);
            }
        }
        visual.setSelected(nnv);
        return nnv;
    }


    public void deleteSelected(){
        silenceVisualEvent(true);
        for (Viewer v : new ArrayList<>(visual.selectedViewers)){
            if (v == null){
            } else if (v instanceof NeuronViewer){
                deleteNeuron((NeuronViewer)v);
            } else if (v instanceof ReceptorViewer){
                deleteReceptor((ReceptorViewer)v);
            } else if (v instanceof SynapseViewer){
                deleteSynapse((SynapseViewer)v);
            }
        }
        silenceVisualEvent(false);
        visual.setSelected(null);
    }


    public void deleteNeuron(NeuronViewer n){
        visual.file.setCurrentFileModified(true);
        //delete neuron
        visual.neuronViewers.removeIf(v -> {
            if (v == n){
                Log.debug(() -> "remove neuron : " + n.name);
                return true;
            }
            return false;
        });
        //delete receptor
        visual.receptorViewers.removeIf(v -> {
            if (v.neuron == n){
                Log.debug(() -> "remove receptor : " + v.name);
                return true;
            }
            return false;
        });
        //delete synapse
        visual.synapseViewers.removeIf(v -> {
            if (v.neuron instanceof MotorViewer){
                if (v.preSynAxon.neuron == n){
                    Log.debug(() -> "remove synapse : " + v.name);
                    return true;
                }
            } else if (v.preSynAxon.neuron == n || v.postSynRep.neuron == n){
                Log.debug(() -> "remove synapse : " + v.name);
                return true;
            }
            return false;
        });
        //delete
        builder.removeNeuron(n.neuron);
        visual.setSelected(null);
    }


    public void deleteReceptor(ReceptorViewer r){
        if (!(r.neuron instanceof NeuronViewer)) return;
        visual.file.setCurrentFileModified(true);
        visual.receptorViewers.removeIf(v -> {
            if (v == r){
                Log.debug(() -> "remove receptor : " + v.name);
                return true;
            }
            return false;
        });
        visual.synapseViewers.removeIf(v -> {
            if (v.postSynRep == r){
                Log.debug(() -> "remove synapse : " + v.name);
                return true;
            }
            return false;
        });
        builder.removeReceptor(r.receptor);
        visual.setSelected(null);
    }


    public void deleteSynapse(SynapseViewer s){
        visual.file.setCurrentFileModified(true);
        AxonViewer axon = s.preSynAxon;
        ReceptorViewer rep = s.postSynRep;
        if ((axon.neuron instanceof NeuronViewer) && (rep.neuron instanceof NeuronViewer)){
            builder.removeSynapse(s.synapse);
        } else if ((axon.neuron instanceof NeuronViewer) && (rep.neuron instanceof MotorViewer)){
            MotorViewer post = (MotorViewer)rep.neuron;
            builder.removeMotorSynapse(post.direction);
        } else if ((axon.neuron instanceof SensorViewer) && (rep.neuron instanceof NeuronViewer)){
            SensorViewer pre = (SensorViewer)axon.neuron;
            NeuronViewer post = (NeuronViewer)rep.neuron;
            builder.removeSensorSynapse(pre.sensor.type, pre.direction, post.neuron.id);
        }
        visual.synapseViewers.removeIf(v -> {
            if (v == s){
                Log.debug(() -> "remove synapse : " + v.name);
                return true;
            }
            return false;
        });
        visual.setSelected(null);
    }


    private void dragViewer(MouseEvent e, Viewer dragTarget){
        if (dragTarget == null) return;
        //only neuron can be dragged
        if (dragTarget instanceof NeuronViewer){
            double x = visual.getXOnPanel(e);
            double y = visual.getYOnPanel(e);
            NeuronViewer n = (NeuronViewer)dragTarget;
            double dx = x - n.location[0];
            double dy = y - n.location[1];
            n.setLocation(x, y);
            visual.getSelectedList().stream()
              .filter(v -> v instanceof NeuronViewer)
              .filter(v -> v != dragTarget)
              .forEach(v -> v.setLocation(v.location[0] + dx, v.location[1] + dy));
        } else if (dragTarget instanceof AxonViewer){
            AxonViewer av = (AxonViewer)dragTarget;
            if (av.neuron instanceof NeuronViewer){
                double dx = visual.getXOnPanel(e) - av.neuron.location[0];
                double dy = visual.getYOnPanel(e) - av.neuron.location[1];
                ((NeuronViewer)av.neuron).theta = Math.atan2(dy, dx);
            }
        }
    }

    void showPopupMenu(Viewer v, MouseEvent e){
        if (v == null){
            showWormBody(e);
        } else if ((v instanceof NeuronViewer)){
            showNeuron((NeuronViewer)v, e);
        } else if ((v instanceof ReceptorViewer)
                   && (((ReceptorViewer)v).neuron instanceof NeuronViewer)){
            showReceptor((ReceptorViewer)v, e);
        } else if (v instanceof SynapseViewer){
            showSynapse((SynapseViewer)v, e);
        }
        //XXX new method show Menu for multi-target operator
    }

    void showNeuron(NeuronViewer n, MouseEvent e){
        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(createMenuItem("Create New Receptor", event -> createNewReceptor(n)),
                               createMenuItem("Delete Neuron", event -> deleteNeuron(n)));
        showPopupMenu(menu, e.getScreenX(), e.getScreenY());
    }

    void showReceptor(ReceptorViewer r, MouseEvent e){
        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(createMenuItem("Delete Receptor", event -> deleteReceptor(r)));
        showPopupMenu(menu, e.getScreenX(), e.getScreenY());
    }

    void showSynapse(SynapseViewer s, MouseEvent e){
        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(createMenuItem("Delete Synapse", event -> deleteSynapse(s)));
        showPopupMenu(menu, e.getScreenX(), e.getScreenY());
    }

    void showWormBody(MouseEvent e){
        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(createMenuItem("Create New Neuron", event -> {
            createNewNeuron(e.getSceneX() - getPaneX() - getCanvasX(),
                            e.getSceneY() - getPaneY() - getCanvasY(),
                            true);
        }));
        showPopupMenu(menu, e.getScreenX(), e.getScreenY());
    }
}
