/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import cclo.hanitu.circuit.Direction;
import cclo.hanitu.circuit.Sensor;

/**
 * the viewer for sensor
 *
 * @author antonio
 */
public class SensorViewer extends AbstractNeuronViewer{

    final Sensor sensor;
    final Direction direction;
    final AxonViewer axon;

    public SensorViewer(Sensor sensor, Direction d){
        super(sensor.type.name() + ":" + d.name());
        this.sensor = sensor;
        direction = d;
        axon = new AxonViewer(this);
        text = sensor.type.name().substring(0, 1);
        theta = Direction.getTheta(Direction.opposite(d));
    }
}
