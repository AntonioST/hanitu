/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import cclo.hanitu.circuit.Direction;
import cclo.hanitu.circuit.Motor;

/**
 * the viewer for motor
 *
 * @author antonio
 */
public class MotorViewer extends AbstractNeuronViewer{

    final Motor motor;
    final Direction direction;
    final ReceptorViewer rep;

    public MotorViewer(Motor motor, Direction d){
        super("M:" + d.name());
        this.motor = motor;
        direction = d;
        rep = new ReceptorViewer(motor.receptor, this);
        text = "M";
        theta = Direction.getTheta(Direction.opposite(d));
    }
}
