/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Predicate;

import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import cclo.hanitu.About;
import cclo.hanitu.Base;
import cclo.hanitu.HanituInfo;
import cclo.hanitu.circuit.*;
import cclo.hanitu.gui.*;
import cclo.hanitu.util.Events;
import cclo.hanitu.world.WormConfig;

import static cclo.hanitu.circuit.Direction.*;
import static cclo.hanitu.circuit.SensorType.FOOD;
import static cclo.hanitu.circuit.SensorType.TOXICANT;
import static cclo.hanitu.gui.MenuSession.*;

/**
 * @author antonio
 */
public class VisualCircuit implements FXMain.FXSimpleApplication{

    private static final String APPLICATION_TITLE = "Circuit Config";

    CircuitViewEnv env = new CircuitViewEnv();
    FileSession file = new FileSession();
    WormConfig wormConfig;
    Circuit circuit;

    Stage primaryStage;
    PaintingPane canvas;
    AbstractCircuitPainter paint;
    VisualCircuitMode mode;
    MenuSession menu;
    InfoStage info;

    protected final List<NeuronViewer> neuronViewers = new ArrayList<>();
    protected final List<ReceptorViewer> receptorViewers = new ArrayList<>();
    protected final List<SynapseViewer> synapseViewers = new ArrayList<>();
    protected final SensorViewer[] foodSensorViewer = new SensorViewer[4];
    protected final SensorViewer[] toxicantSensorViewer = new SensorViewer[4];
    protected final MotorViewer[] motorViewer = new MotorViewer[4];

    final Events.BiConsumerEvent<MouseEvent, Viewer> focusEvent = new Events.BiConsumerEvent<>();
    final Events.BiConsumerEvent<MouseEvent, Viewer> clickEvent = new Events.BiConsumerEvent<>();
    final Events.BiConsumerEvent<MouseEvent, Viewer> dragEvent = new Events.BiConsumerEvent<>();
    final Events.BiConsumerEvent<MouseEvent, Viewer> doubleClickEvent = new Events.BiConsumerEvent<>();
    final Events.BiConsumerEvent<Viewer, Boolean> selectEvent = new Events.BiConsumerEvent<>();
    final Events.ConsumerEvent<Circuit> setCircuitEvent = new Events.ConsumerEvent<>();
    final Events.RunnableEvent updateEvent = new Events.RunnableEvent();
    final Events.RunnableEvent quitEvent = new Events.RunnableEvent();

    Viewer lastSelectedTarget;
    final List<Viewer> selectedViewers = new ArrayList<>();
    boolean silenceSelectedUpdate;

    double lastMousePressedXOnPanel;
    double lastMousePressedYOnPanel;
    MouseButton lastMousePressedButton;
    private Viewer lastMouseTarget;

    public VisualCircuit(){
        setCircuit(ReadOnlyCircuit.EMPTY);
    }

    @Override
    public void start(Stage primaryStage){
        this.primaryStage = primaryStage;
        primaryStage.setTitle(APPLICATION_TITLE);
        primaryStage.setWidth(env.viewSize + 10);
        primaryStage.setHeight(env.viewSize + 20);
        //
        VBox root = new VBox();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        //
        initFile();
        primaryStage.setOnCloseRequest(e -> file.askQuit());
        root.getChildren().add(initMenu());
        root.getChildren().add(initCanvas());
        //
        if (paint == null){
            int dxy = (env.viewSize - env.bodySize) / 2;
            setCircuitPainter(new CircuitPainter(dxy, dxy, env.viewSize, env.viewSize));
        }
        initEvent();
        //
        initInfoStage();
        //
        if (mode != null){
            mode.setup(this);
        }
        //
        setTitle(null);
        primaryStage.sizeToScene();
        primaryStage.show();
        canvas.repaint();
    }

    private void initFile(){
        file.setStage(primaryStage);
        file.setDefaultFileExtName(HanituInfo.CIRCUIT_EXTEND_FILENAME);
        file.setFileFilter(HanituFileFilters.CIRCUIT_CONFIG_FILTER,
                           HanituFileFilters.NORMAL_TEXT_FILTER,
                           HanituFileFilters.ALL_FILTER);
        file.setOnNewFile(() -> setCircuit(null));
        file.setOnOpenFile(path -> setCircuit(path, false));
        file.setOnSaveFile(path -> FileSession.saveFileByTempReplace(path, os -> {
            Circuit c = getCircuit();
            for (NeuronViewer v : neuronViewers){
                CentralNeuron n = c.getNeuron(v.neuron.id);
                n.putMeta(CentralNeuron.META_X, v.getX());
                n.putMeta(CentralNeuron.META_Y, v.getY());
                n.putMeta(CentralNeuron.META_THETA, v.getTheta());
            }
            CircuitPrinter cp = new CircuitPrinter();
            cp.write(os, c);
        }));
        file.setOnUpdate(() -> setTitle(null));
        file.setOnQuit(() -> {
            quitEvent.run();
            primaryStage.close();
        });
    }

    private MenuBar initMenu(){
        Properties p = Base.loadPropertyInherit("gui");
        MenuBar menuBar = new MenuBar();
        menu = new MenuSession(menuBar, MENU_FILE, MENU_EDIT, MENU_HELP);
        menu.addMenuItems(MENU_FILE, 0, file.getMenuList());
        menu.addMenuItems(MENU_EDIT, 0,
                          createMenuItem(p.getProperty("circuit.menu.edit.info"), "I", e -> {
                              if (info == null){
                                  info = new InfoStage(this);
                              }
                              info.show();
                              info.setX(primaryStage.getX() + primaryStage.getWidth());
                              info.setY(primaryStage.getY());
                          }));
//        menu.addMenuItems(MENU_VIEW, 0,
//                         /*,
//                          createMenuItem("_Property", e -> {
//                              //XXX Unsupported Operation CircuitSession.initViewMenu
//                              throw new UnsupportedOperationException();
//                          })*/);
        menu.addMenuItems(MENU_HELP, 0, About.getMenuItem());
        return menuBar;
    }

    private PaintingPane initCanvas(){
        canvas = new PaintingPane(600, 600);
        canvas.setBackgroundColor(env.backgroundColor);
        canvas.setOnMouseClicked(e -> {
            if (e.getClickCount() == 1){
                clickEvent.accept(e, lastMouseTarget);
            } else {
                doubleClickEvent.accept(e, lastMouseTarget);
            }
        });
        canvas.setOnMousePressed(e -> {
            lastMousePressedXOnPanel = getXOnPanel(e);
            lastMousePressedYOnPanel = getYOnPanel(e);
            lastMousePressedButton = e.getButton();
            lastMouseTarget = findTouch(lastMousePressedXOnPanel,
                                        lastMousePressedYOnPanel,
                                        lastSelectedTarget instanceof SynapseViewer);
            focusEvent.accept(e, lastMouseTarget);
        });
        canvas.setOnMouseDragged(e -> {
            dragEvent.accept(e, lastMouseTarget);
        });
        return canvas;
    }

    private void initEvent(){
        dragEvent.add((e, t) -> {
            if (e.getButton() == MouseButton.MIDDLE){
                dragViewPanel(e);
            }
        });
    }

    private void initInfoStage(){
        info = new InfoStage(this);
        info.setEditable(false);
        quitEvent.add(info::close);
    }

    public void stop(){
        file.askQuit();
    }

    public void setTitle(String message){
        StringBuilder sb = new StringBuilder();
        if (file.isCurrentFileModified()){
            sb.append("* ");
        }
        sb.append("Hanitu Circuit Viewer");
        if (message != null){
            sb.append(" - ").append(message);
        }
        Path path = file.getCurrentFilePath();
        if (path != null){
            Path rp = Paths.get(System.getProperty("user.dir")).relativize(path.toAbsolutePath());
            if (rp.startsWith("..")){
                sb.append(" - ").append(path);
            } else {
                sb.append(" - ").append(rp);
            }
        }
        primaryStage.setTitle(sb.toString());
    }

    public WormConfig getWormConfig(){
        return wormConfig;
    }

    public void setWormConfig(WormConfig config){
        wormConfig = config;
    }

    public Circuit getCircuit(){
        return circuit;
    }

    public void setCircuit(Path filePath, boolean failOnLoad) throws IOException{
        CircuitLoader loader = new CircuitLoader(failOnLoad);
        setCircuit(loader.load(filePath));
        file.setCurrentFilePath(filePath);
        file.setCurrentFileModified(false);
    }

    public void setCircuit(Circuit circuit){
        neuronViewers.clear();
        receptorViewers.clear();
        synapseViewers.clear();
        setSelected(null);
        //
        this.circuit = circuit;
        if (circuit != null){
            initCircuitViewers();
        }
        //
        file.setCurrentFilePath(null);
        setCircuitEvent.accept(circuit);
        updateEvent.run();
    }

    public void setMode(VisualCircuitMode mode){
        if (this.mode != null){
            this.mode.destroy();
        }
        this.mode = mode;
        if (primaryStage != null){
            mode.setup(this);
        }
    }

    public void clear(){
        setCircuit(null);
        file.setCurrentFilePath(null);
        file.setCurrentFileModified(false);
        if (primaryStage != null){
            setTitle(null);
        }
    }

    void initCircuitViewers(){
        int size = circuit.neuronCount();
        for (int i = 0; i < size; i++){
            CentralNeuron n = circuit.getNeuron(i);
            //create neuron
            NeuronViewer v = new NeuronViewer(n);
            //set location, random
            v.setLocation(n.getFloatMeta("x", (int)(Math.random() * CircuitViewEnv.ENV.viewSize)),
                          n.getFloatMeta("y", (int)(Math.random() * CircuitViewEnv.ENV.viewSize)));
            v.theta = n.getFloatMeta("t", v.theta);
            neuronViewers.add(v);
            //create receptor
            n.receptors.stream()
              .map(r -> new ReceptorViewer(r, v))
              .forEach(receptorViewers::add);
        }
        //create synapse
        for (int i = 0; i < size; i++){
            NeuronViewer v = neuronViewers.get(i);
            v.neuron.targets.stream()
              .map(s -> new SynapseViewer(v, s, v.axon, getReceptorViewer(s.postSynapseNeuron.id, s.targetReceptor.id)))
              .forEach(synapseViewers::add);
        }
        //create body neuron and synapse
        foodSensorViewer[0] = new SensorViewer(circuit.sensor(FOOD, FORWARD), FORWARD);
        foodSensorViewer[1] = new SensorViewer(circuit.sensor(FOOD, BACKWARD), BACKWARD);
        foodSensorViewer[2] = new SensorViewer(circuit.sensor(FOOD, LEFTWARD), LEFTWARD);
        foodSensorViewer[3] = new SensorViewer(circuit.sensor(FOOD, RIGHTWARD), RIGHTWARD);
        initBodySynapse(foodSensorViewer[0]);
        initBodySynapse(foodSensorViewer[1]);
        initBodySynapse(foodSensorViewer[2]);
        initBodySynapse(foodSensorViewer[3]);
        toxicantSensorViewer[0] = new SensorViewer(circuit.sensor(TOXICANT, FORWARD), FORWARD);
        toxicantSensorViewer[1] = new SensorViewer(circuit.sensor(TOXICANT, BACKWARD), BACKWARD);
        toxicantSensorViewer[2] = new SensorViewer(circuit.sensor(TOXICANT, LEFTWARD), LEFTWARD);
        toxicantSensorViewer[3] = new SensorViewer(circuit.sensor(TOXICANT, RIGHTWARD), RIGHTWARD);
        initBodySynapse(toxicantSensorViewer[0]);
        initBodySynapse(toxicantSensorViewer[1]);
        initBodySynapse(toxicantSensorViewer[2]);
        initBodySynapse(toxicantSensorViewer[3]);
        motorViewer[0] = new MotorViewer(circuit.motor(FORWARD), FORWARD);
        motorViewer[1] = new MotorViewer(circuit.motor(BACKWARD), BACKWARD);
        motorViewer[2] = new MotorViewer(circuit.motor(LEFTWARD), LEFTWARD);
        motorViewer[3] = new MotorViewer(circuit.motor(RIGHTWARD), RIGHTWARD);
        initBodySynapse(motorViewer[0]);
        initBodySynapse(motorViewer[1]);
        initBodySynapse(motorViewer[2]);
        initBodySynapse(motorViewer[3]);
    }

    private void initBodySynapse(SensorViewer sv){
        circuit.sensor(sv.sensor.type, sv.direction).targets.stream()
          .map(s -> new SynapseViewer(sv, s, sv.axon, getReceptorViewer(s.postSynapseNeuron.id, s.targetReceptor.id)))
          .forEach(synapseViewers::add);
    }

    private void initBodySynapse(MotorViewer m){
        CentralNeuron neu = m.motor.preMotorNeuron;
        if (neu != null){
            SynapseViewer s = new SynapseViewer(m, null, getNeuronViewer(neu.id).axon, m.rep);
            synapseViewers.add(s);
        }
    }

    public AbstractCircuitPainter getCircuitPaint(){
        return paint;
    }

    public void setCircuitPainter(AbstractCircuitPainter cp){
        if (canvas != null && paint != null){
            canvas.removePanel(paint);
            paint.destroy();
        }
        paint = cp;
        paint.setup(this);
        if (canvas != null && paint != null){
            canvas.addPanel(paint);
        }
    }

    public double getXOnPanel(MouseEvent e){
        return e.getX() - (paint == null? 0: paint.getXOnPane());
    }

    public double getYOnPanel(MouseEvent e){
        return e.getY() - (paint == null? 0: paint.getYOnPane());
    }

    public void setXOnPanel(double x){
        if (paint != null){
            paint.setXOnPane(x);
        }
    }

    public void setYOnPanel(double y){
        if (paint != null){
            paint.setYOnPane(y);
        }
    }

    public NeuronViewer getNeuronViewer(int id){
        return neuronViewers.stream()
          .filter(n -> n.neuron.id == id)
          .findFirst()
          .orElse(null);
    }

    public ReceptorViewer getReceptorViewer(int host, int rep){
        return receptorViewers.stream()
          .filter(r -> r != null)
          .filter(r -> r.receptor.neuron.id == host && r.receptor.id == rep)
          .findFirst()
          .orElse(null);
    }

    public void setSelected(Viewer v){
        selectedViewers.clear();
        if (v != null){
            selectedViewers.add(v);
            lastSelectedTarget = v;
            setInfo(v);
        } else {
            lastSelectedTarget = null;
        }
        if (!silenceSelectedUpdate){
            selectEvent.accept(v, false);
            if (canvas != null){
                canvas.repaint();
            }
        }
    }

    public void appendSelect(Viewer v){
        if (v != null){
            if (selectedViewers.contains(v)){
                selectedViewers.remove(v);
            } else {
                selectedViewers.add(v);
            }
            lastSelectedTarget = v;
            setInfo(v);
        }
        if (!silenceSelectedUpdate){
            selectEvent.accept(v, true);
            canvas.repaint();
        }
    }

    public boolean hasSelected(){
        return !selectedViewers.isEmpty();
    }

    public boolean isDirectSelected(Viewer v){
        return v != null && lastSelectedTarget == v;
    }

    public boolean isSelected(Viewer v){
        return selectedViewers.contains(v);
    }

    protected boolean isRelativeToSelect(Viewer v){
        return v != null
               && lastSelectedTarget != null
               && v != lastSelectedTarget
               && lastSelectedTarget.isRelative(v);
    }

    public List<Viewer> getSelectedList(){
        return Collections.unmodifiableList(selectedViewers);
    }

    public void setInfo(Viewer v){
        if (silenceSelectedUpdate) return;
        if (v == null){
            info.reset(null);
        } else if (v instanceof NeuronViewer){
            info.reset(((NeuronViewer)v).neuron);
        } else if (v instanceof ReceptorViewer){
            ReceptorViewer r = (ReceptorViewer)v;
            if (r.neuron instanceof NeuronViewer){
                info.reset(((ReceptorViewer)v).receptor);
            } else if (r.neuron instanceof MotorViewer){
                info.reset(circuit.motor());
            }
        } else if (v instanceof SynapseViewer){
            SynapseViewer s = (SynapseViewer)v;
            if (s.neuron instanceof NeuronViewer){
                info.reset(s.synapse);
            } else if (s.neuron instanceof SensorViewer){
                info.reset(s.synapse);
            } else if (s.neuron instanceof MotorViewer){
                info.reset(circuit.motor());
            }
        } else if (v instanceof AxonViewer){
            AxonViewer a = (AxonViewer)v;
            if (a.neuron instanceof NeuronViewer){
                info.reset(((NeuronViewer)a.neuron).neuron);
            } else if (a.neuron instanceof SensorViewer){
                info.reset(circuit.sensor(((SensorViewer)a.neuron).sensor.type));
            }
        }
        info.setX(primaryStage.getX() + primaryStage.getWidth());
        info.setY(primaryStage.getY());
    }

    private <V extends Viewer> V findTouch(Collection<V> c, double x, double y, boolean switchSelect){
        double min = Double.POSITIVE_INFINITY;
        V ret = null;
        for (V v : c){
            if (!switchSelect || !selectedViewers.contains(v)){
                double d = v.isTouch(x, y);
                if (d < min && d < 10){
                    min = d;
                    ret = v;
                }
            }
        }
        return ret;
    }

    public Viewer findTouch(double x, double y, boolean switchSelect){
        Predicate<Viewer> test = t -> (!switchSelect || !selectedViewers.contains(t)) && t.isTouch(x, y) == 0;
        /*also do equal test to selected, to make select-exchange
        avoid above viewer always be selected and cover behind viewer
        */
        for (ReceptorViewer r : receptorViewers){
            if (test.test(r)) return r;
        }
        for (NeuronViewer n : neuronViewers){
            if (test.test(n.axon)) return n.axon;
            if (test.test(n)) return n;
        }
        for (SensorViewer s : foodSensorViewer){
            if (test.test(s)) return s.axon;
            if (test.test(s.axon)) return s.axon;
        }
        for (SensorViewer s : toxicantSensorViewer){
            if (test.test(s)) return s.axon;
            if (test.test(s.axon)) return s.axon;
        }
        for (MotorViewer m : motorViewer){
            if (test.test(m)) return m.rep;
            if (test.test(m.rep)) return m.rep;
        }
        return findTouch(synapseViewers, x, y, switchSelect);
    }

    protected void dragViewPanel(MouseEvent e){
        //lastMousePressedXOnPanel = e(old).getX - oldPanelX
        //dx = e(new).getX - e(old).getX
        //   = e(new).getX - lastMousePressedXOnPanel - oldPanelX
        //newX = oldPanelX + dx
        //     = e(new).getX - lastMousePressedXOnPanel
        //lastMousePressedX will keep old information during dragging, so it also keep the
        //information of the location of the viewPanel. We don't need the variable to record it.
        setXOnPanel(e.getX() - lastMousePressedXOnPanel);
        setYOnPanel(e.getY() - lastMousePressedYOnPanel);
        canvas.repaint();
    }

}
