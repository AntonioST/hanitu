/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import cclo.hanitu.HanituInfo.VBooleanField;
import cclo.hanitu.HanituInfo.VTextField;
import cclo.hanitu.HanituInfo.VUIField;
import cclo.hanitu.HanituInfo.VValueField;
import cclo.hanitu.gui.UIType;

import static cclo.hanitu.HanituInfo.Tip;

/**
 * all config variable.
 *
 * @author antonio
 */
public class CircuitViewEnv{

    public static CircuitViewEnv ENV = new CircuitViewEnv();
    //
    @Tip("the size of the viable range")
    @VValueField
    public int viewSize = 500;

    @Tip("the size of the worm body")
    @VValueField
    public int bodySize = 430;

    @Tip("the size of the neuron")
    @VValueField
    public int neuronSize = 30;

    @Tip("the distance of the neuron component from the neuron center")
    @VValueField
    public int receptorRadius = 20;

    @Tip("the size of the axon")
    @VValueField
    public int axonSize = 15;

    @Tip("the size of the receptor")
    @VValueField
    public int receptorSize = 15;

    @Tip("the alpha value of the un-selected object when fade option is on")
    @VValueField
    public double fadeAlpha = 0.3;

    @Tip("the color of the background")
    @VUIField
    public Color backgroundColor = Color.WHITE;

    @Tip("the color of the worm body")
    @VUIField
    public Color bodyColor = Color.LIGHTGRAY;

    @Tip("the color the neuron id text")
    @VUIField
    public Color neuronTextColor = Color.BLACK;

    @Tip("the color of the neuron")
    @VUIField
    public Color neuronColor = Color.BLACK;

    @Tip("the color of the neuron when it is selected")
    @VUIField
    public Color neuronHighlightColor = Color.ORANGE;

    @Tip("the color of the axon")
    @VUIField
    public Color axonColor = Color.RED;

    @Tip("the color of the axon when it is selected")
    @VUIField
    public Color axonHighlightColor = Color.ORANGE;

    @Tip("the color of the receptor id text")
    @VUIField
    public Color receptorTextColor = Color.BLACK;

    @Tip("the color of the receptor")
    @VUIField
    public Color receptorColor = Color.GRAY;

    @Tip("the color of the receptor when it is selected")
    @VUIField
    public Color receptorHighlightColor = Color.ORANGE;

    @Tip("the color of the synapse weight text")
    @VUIField
    public Color synapseTextColor = Color.BLACK;

    @Tip("the color of the synapse")
    @VUIField
    public Color synapseColor = Color.LIGHTGREEN;

    @Tip("the color of the synapse when it is selected")
    @VUIField
    public Color synapseHighlightColor = Color.ORANGE;

    @Tip("the color of the food sensor")
    @VUIField
    public Color foodSensorColor = Color.CYAN;

    @Tip("the color of the toxicant sensor")
    @VUIField
    public Color toxicantSensorColor = Color.YELLOW;

    @Tip("the color of the motor")
    @VUIField
    public Color motorColor = Color.RED;

    @Tip("the line width of the worm body")
    @VUIField(UIType.STROKE_WIDTH)
    public double bodyStroke = 50;

    @Tip("the default line width")
    @VUIField(UIType.STROKE_WIDTH)
    public double defaultStroke = 2;

    @Tip("the line width of the neuron")
    @VUIField(UIType.STROKE_WIDTH)
    public double neuronStroke = 4;

    @Tip("the line width of the neuron when it is selected")
    @VUIField(UIType.STROKE_WIDTH)
    public double neuronHighlightStroke = 4;

    @Tip("the line width of the axon")
    @VUIField(UIType.STROKE_WIDTH)
    public double axonStroke = 3;

    @Tip("the line width of the axon when it is selected")
    @VUIField(UIType.STROKE_WIDTH)
    public double axonHighlightStroke = 3;

    @Tip("the line width of the receptor")
    @VUIField(UIType.STROKE_WIDTH)
    public double receptorStroke = 3;

    @Tip("the line width of the receptor when it is selected")
    @VUIField(UIType.STROKE_WIDTH)
    public double receptorHighlightStroke = 3;

    @Tip("the line width of the synapse")
    @VUIField(UIType.STROKE_WIDTH)
    public double synapseStroke = 2;

    @Tip("the line width of the synapse when it is selected")
    @VUIField(UIType.STROKE_WIDTH)
    public double maxSynapseWeightStroke = 5;

    @Tip("the font of the id show in neuron")
    @VUIField
    public Font neuronTextFont = Font.font("FreeMono", FontWeight.BOLD, 20);

    @Tip("the font of the id show near the receptor")
    @VUIField
    public Font receptorTextFont = Font.font("FreeMono", 15);

    @Tip("the font of the id show near by the receptor when it is selected")
    @VUIField
    public Font receptorHighlightTextFont =Font.font("FreeMono", FontWeight.BOLD, 17);

    @Tip("the font of the weight show near by the synapse")
    @VUIField
    public Font synapseTextFont = Font.font("FreeMono", 15);

    @Tip("the font of the weight show near by the synapse when it is selected")
    @VUIField
    public Font synapseHighlightTextFont = Font.font("FreeMono", FontWeight.BOLD, 17);

    @Tip("the format style of the float point number showing")
    @VTextField
    public String floatDisplayFormat = "%.2f";
    //
    private static final Map<String, Field> MAP = new HashMap<>();

    static{
        for (Field f : CircuitViewEnv.class.getDeclaredFields()){
            if (Modifier.isStatic(f.getModifiers())) continue;
            MAP.put(getPropertyName(f.getName()), f);
        }
    }

    //    private static final String DEFAULT_CONFIG_FILE_NAME = ".vbconfig";
//    private static final String CONFIG_FILE_HEADER
//      = "# the line starting with character '#' is a comment line\n"
//        + "# the property file format can reference following link:\n"
//        + "# http://docs.oracle.com/javase/7/docs/api/java/util/Properties.html#load(java.io.Reader)\n"
//        + "\n"
//        + "# this property file is the config of ViewEnv\n"
//        + "# the class used to control all of display setting.\n"
//        + "\n";
    static final Comparator<String> CMP = (o1, o2) -> {
        int i1 = o1.lastIndexOf(".");
        int i2 = o2.lastIndexOf(".");
        if (i1 == -1) return 1;
        if (i2 == -1) return -1;
        int r = o1.substring(i1 + 1).compareTo(o2.substring(i2 + 1));
        if (r == 0){
            i1 = 0;
            i2 = 0;
            while (r == 0){
                int n1 = o1.indexOf(".", i1);
                int n2 = o2.indexOf(".", i2);
                if (n1 == n2 && n1 == -1) return 0;
                if (n1 == -1) return -1;
                if (n2 == -1) return 1;
                r = o1.substring(i1, n1).compareTo(o2.substring(i2, n2));
                i1 = n1 + 1;
                i2 = n2 + 1;
            }
        }
        return r;
    };

    private static String getPropertyName(String name){
        Matcher m = Pattern.compile("[A-Z]").matcher(name);
        if (m.find()){
            do {
                name = m.replaceFirst("." + m.group().toLowerCase());
            } while (m.reset(name).find());
        }
        return name;
    }


}
