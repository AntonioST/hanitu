/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

/**
 * the class contain some drawing data.
 *
 * @author antonio
 */
public abstract class Viewer{

    final String name;
    String text;
    /**
     * center location in x-y coordinate. zero-index for x and one-index for y.
     */
    final double[] location = new double[2];
    /**
     * also known as radius for circle-shape viewer.
     */
    double size;

    public Viewer(String name){
        this.name = name;
        text = name;
    }

    public double getX(){
        return location[0];
    }

    public double getY(){
        return location[1];
    }

    /**
     * set the absolute location.
     *
     * @param x
     * @param y
     */
    public void setLocation(double x, double y){
        location[0] = x;
        location[1] = y;
    }

    /**
     * is point (x, y) locate in the this viewer.
     *
     * @param x
     * @param y
     * @return 0 if point locate in. positive number present the distance between the point and the viewer.
     * @see #isTaughtSurround(double[], double, double, double)
     */
    public double isTouch(double x, double y){
        return Double.POSITIVE_INFINITY;
    }

    /**
     * is point (x, y) locate in the this circle-shape viewer.
     *
     * @param center circle center location
     * @param r      circle radius
     * @param x
     * @param y
     * @return negative number if point doesn't locate in. 0 if point locate in.
     * @see #isTouch(double, double)
     */
    protected static double isTaughtSurround(double[] center, double r, double x, double y){
        x -= center[0];
        y -= center[1];
        if ((x < 0 && x < -r) || x > r){
            return Double.POSITIVE_INFINITY;
        }
        if ((y < 0 && y < -r) || y > r){
            return Double.POSITIVE_INFINITY;
        }
        return x * x + y * y < r * r? 0: Double.POSITIVE_INFINITY;
    }

    /**
     * is viewer {@code other} has the relationship with this viewer.
     *
     * @param other
     * @return
     */
    public boolean isRelative(Viewer other){
        return false;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Viewer viewer = (Viewer)o;

        return name.equals(viewer.name);

    }

    @Override
    public int hashCode(){
        return name.hashCode();
    }

    @Override
    public String toString(){
        return "Viewer:" + name;
    }
}
