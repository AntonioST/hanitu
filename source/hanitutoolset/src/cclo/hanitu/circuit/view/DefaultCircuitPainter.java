/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;


/**
 * @author antonio
 */
public class DefaultCircuitPainter extends AbstractCircuitPainter{

    public DefaultCircuitPainter(){
    }

    public DefaultCircuitPainter(int x, int y, int w, int h){
        super(x, y, w, h);
    }

    public DefaultCircuitPainter(int w, int h){
        super(w, h);
    }

    @Override
    public void paint(GraphicsContext g){
        int center = visual.env.viewSize >> 1;
        int size = visual.env.bodySize >> 1;
        int shift = visual.env.neuronSize;
        visual.foodSensorViewer[0].setLocation(center + shift, center - size);
        visual.foodSensorViewer[1].setLocation(center - shift, center + size);
        visual.foodSensorViewer[2].setLocation(center - size, center - shift);
        visual.foodSensorViewer[3].setLocation(center + size, center + shift);
        visual.toxicantSensorViewer[0].setLocation(center - shift, center - size);
        visual.toxicantSensorViewer[1].setLocation(center + shift, center + size);
        visual.toxicantSensorViewer[2].setLocation(center - size, center + shift);
        visual.toxicantSensorViewer[3].setLocation(center + size, center - shift);
        visual.motorViewer[0].setLocation(center, center - size - shift);
        visual.motorViewer[1].setLocation(center, center + size + shift);
        visual.motorViewer[2].setLocation(center - size - shift, center);
        visual.motorViewer[3].setLocation(center + size + shift, center);
        super.paint(g);
    }

    @Override
    protected void paintWormBody(GraphicsContext g){
        g.setStroke(visual.env.bodyColor);
        g.setLineWidth(visual.env.bodyStroke);
        int c = visual.env.viewSize >> 1;
        int s = visual.env.bodySize;
        c -= s >> 1;
        g.strokeOval(c, c, s, s);
    }

    @Override
    protected void paintNeuronBody(GraphicsContext g, NeuronViewer n){
        drawCircle(g, n);
    }

    @Override
    protected void paintNeuronText(GraphicsContext g, NeuronViewer n){
        int textSize = (int)g.getFont().getSize();
        g.fillText(n.text,
                   n.location[0] - textSize * n.text.length() / 4,
                   n.location[1] + n.size / 2);
    }

    @Override
    protected void paintNeuronAxon(GraphicsContext g, AxonViewer a){
        getNeuronComponentLocation(a.neuron, 0, a.location, visual.env.receptorRadius);
        drawCircle(g, a);
    }

    @Override
    protected void paintReceptorBody(GraphicsContext g, ReceptorViewer r){
        getNeuronComponentLocation(r.neuron, r.receptor.id + 1, r.location, visual.env.receptorRadius);
        drawTriangle(g, r, r.neuron);
    }

    @Override
    protected void paintReceptorText(GraphicsContext g, ReceptorViewer r){
        int id = r.receptor.id;
        double[] t = new double[2];
        getNeuronComponentLocation(r.neuron, id + 1, t, visual.env.receptorRadius + 12);
        Font f = g.getFont();
        g.fillText(Integer.toString(id), t[0], t[1]);
    }

    @Override
    protected void paintSynapseLine(GraphicsContext g, SynapseViewer s){
        g.strokeLine(s.preSynAxon.location[0], s.preSynAxon.location[1],
                     s.postSynRep.location[0], s.postSynRep.location[1]);
    }

    @Override
    protected void paintSynapseText(GraphicsContext g, SynapseViewer s){
        double x;
        double y;
        if (isSelected(s)){
            x = (s.preSynAxon.location[0] + s.postSynRep.location[0]) / 2;
            y = (s.preSynAxon.location[1] + s.postSynRep.location[1]) / 2;
        } else if (isSelectedOrRelative(s.preSynAxon.neuron)){
            x = s.postSynRep.location[0];
            y = s.postSynRep.location[1];
        } else {
            x = s.preSynAxon.location[0];
            y = s.preSynAxon.location[1];
        }
        g.fillText(String.format(visual.env.floatDisplayFormat, s.synapse.weight * s.synapse.conductance), x - 25, y);
    }

    @Override
    protected void paintSensor(GraphicsContext g, SensorViewer s){
        drawCircle(g, s);
    }

    @Override
    protected void paintSensorAxon(GraphicsContext g, AxonViewer a){
        getNeuronComponentLocation(a.neuron, 0, a.location, visual.env.receptorRadius);
        drawCircle(g, a);
    }

    @Override
    protected void paintSensorText(GraphicsContext g, SensorViewer n){
        int textSize = (int)g.getFont().getSize();
        g.fillText(n.text,
                   n.location[0] - textSize * n.text.length() / 4,
                   n.location[1] + n.size / 2);
    }

    @Override
    protected void paintMotor(GraphicsContext g, MotorViewer m){
        drawCircle(g, m);
    }

    @Override
    protected void paintMotorReceptor(GraphicsContext g, ReceptorViewer r){
        getNeuronComponentLocation(r.neuron, 0, r.location, visual.env.receptorRadius);
        drawTriangle(g, r, r.neuron);
    }

    @Override
    protected void paintMotorText(GraphicsContext g, MotorViewer n){
        int textSize = (int)g.getFont().getSize();
        g.fillText(n.text,
                   n.location[0] - textSize * n.text.length() / 4,
                   n.location[1] + n.size / 2);
    }

    protected static void drawTriangle(GraphicsContext g, Viewer viewer, Viewer center){
        double vx = viewer.location[0];
        double vy = viewer.location[1];
        double theta = center != null
                       ? Math.atan2(center.location[1] - vy, center.location[0] - vx)
                       : -Math.PI / 2;
        double inc = 2 * Math.PI / 3;
        double size = viewer.size;
        for (int i = 0; i < 3; i++){
            g.strokeLine(vx + (int)(size * Math.cos(theta)),
                         vy + (int)(size * Math.sin(theta)),
                         vx + (int)(size * Math.cos(theta += inc)),
                         vy + (int)(size * Math.sin(theta)));
        }
    }

    protected static void drawCircle(GraphicsContext g, Viewer v){
        g.strokeOval(v.location[0] - v.size, v.location[1] - v.size, v.size * 2, v.size * 2);
    }

    protected static void drawCircle(GraphicsContext g, int[] place, int radius){
        g.strokeOval(place[0] - radius, place[1] - radius, radius << 1, radius << 1);
    }

    protected static void getNeuronComponentLocation(AbstractNeuronViewer neuron,
                                                     int id,
                                                     double[] location,
                                                     int radius){
        int all;
        if (neuron instanceof NeuronViewer){
            all = ((NeuronViewer)neuron).neuron.receptors.size() + 1;
        } else {
            all = 1;
        }
        double t = 2.0 * Math.PI * id / all + neuron.theta;
        location[0] = (int)(neuron.location[0] + radius * Math.cos(t));
        location[1] = (int)(neuron.location[1] + radius * Math.sin(t));
    }
}
