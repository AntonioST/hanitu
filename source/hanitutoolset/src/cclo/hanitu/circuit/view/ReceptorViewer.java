/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import cclo.hanitu.circuit.Receptor;

import java.util.Objects;

/**
 * the viewer for receptor.
 *
 * @author antonio
 */
public class ReceptorViewer extends Viewer{

    final Receptor receptor;
    final AbstractNeuronViewer neuron;

    public ReceptorViewer(Receptor receptor, AbstractNeuronViewer neuron){
        super(receptor.toString());
        this.receptor = receptor;
        this.neuron = Objects.requireNonNull(neuron);
        size = CircuitViewEnv.ENV.receptorSize >> 1;
    }

    @Override
    public double isTouch(double x, double y){
        return isTaughtSurround(location, size, x, y);
    }

    @Override
    public boolean isRelative(Viewer other){
        return other == neuron
               || ((other instanceof SynapseViewer) && other.isRelative(this));
    }
}
