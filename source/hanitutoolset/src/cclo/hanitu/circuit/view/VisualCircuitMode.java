/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;

import cclo.hanitu.circuit.Circuit;

/**
 * @author antonio
 */
public class VisualCircuitMode{

    protected VisualCircuit visual;
    private final Set<String> eventKey = new HashSet<>();
    private final Map<String, List<String>> menu = new HashMap<>();

    protected void setup(VisualCircuit visual){
        this.visual = visual;
    }

    protected void destroy(){
        if (visual != null){
            menu.forEach(visual.menu::removeMenuItems);
            eventKey.forEach(key -> {
                visual.focusEvent.removeEvent(key);
                visual.clickEvent.removeEvent(key);
                visual.dragEvent.removeEvent(key);
                visual.doubleClickEvent.removeEvent(key);
                visual.selectEvent.removeEvent(key);
                visual.setCircuitEvent.removeEvent(key);
                visual.updateEvent.removeEvent(key);
                visual.quitEvent.removeEvent(key);
            });
        }
    }

    public void setCircuitInstance(Circuit c){
        visual.circuit = c;
    }

    public void addMenuItems(String menu, int index, MenuItem... items){
        Objects.requireNonNull(visual);
        this.menu.computeIfAbsent(menu, m -> new ArrayList<>())
          .addAll(Stream.of(items).map(MenuItem::getText).collect(Collectors.toList()));
        visual.menu.addMenuItems(menu, index, items);
    }

    public void setOnFocusEvent(String keyword, BiConsumer<MouseEvent, Viewer> e){
        Objects.requireNonNull(visual);
        if (eventKey.contains(keyword)){
            throw new RuntimeException("register fail : " + keyword);
        }
        eventKey.add(keyword);
        visual.focusEvent.add(keyword, e);
    }

    public void setOnClickEvent(String keyword, BiConsumer<MouseEvent, Viewer> e){
        Objects.requireNonNull(visual);
        if (eventKey.contains(keyword)){
            throw new RuntimeException("register fail : " + keyword);
        }
        eventKey.add(keyword);
        visual.clickEvent.add(keyword, e);
    }

    public void setOnDragEvent(String keyword, BiConsumer<MouseEvent, Viewer> e){
        Objects.requireNonNull(visual);
        if (eventKey.contains(keyword)){
            throw new RuntimeException("register fail : " + keyword);
        }
        eventKey.add(keyword);
        visual.dragEvent.add(keyword, e);
    }

    public void setOnDoubleClickEvent(String keyword, BiConsumer<MouseEvent, Viewer> e){
        Objects.requireNonNull(visual);
        if (eventKey.contains(keyword)){
            throw new RuntimeException("register fail : " + keyword);
        }
        eventKey.add(keyword);
        visual.doubleClickEvent.add(keyword, e);
    }

    public void setOnSelectEvent(String keyword, BiConsumer<Viewer, Boolean> e){
        Objects.requireNonNull(visual);
        if (eventKey.contains(keyword)){
            throw new RuntimeException("register fail : " + keyword);
        }
        eventKey.add(keyword);
        visual.selectEvent.add(keyword, e);
    }

    public void setOnSetCircuitEvent(String keyword, Consumer<Circuit> e){
        Objects.requireNonNull(visual);
        if (eventKey.contains(keyword)){
            throw new RuntimeException("register fail : " + keyword);
        }
        eventKey.add(keyword);
        visual.setCircuitEvent.add(keyword, e);
    }

    public void setOnUpdateEvent(String keyword, Runnable e){
        Objects.requireNonNull(visual);
        if (eventKey.contains(keyword)){
            throw new RuntimeException("register fail : " + keyword);
        }
        eventKey.add(keyword);
        visual.updateEvent.add(keyword, e);
    }

    public void setOnQuitEvent(String keyword, Runnable e){
        Objects.requireNonNull(visual);
        if (eventKey.contains(keyword)){
            throw new RuntimeException("register fail : " + keyword);
        }
        eventKey.add(keyword);
        visual.quitEvent.add(keyword, e);
    }

    public void silenceVisualEvent(boolean silence){
        visual.silenceSelectedUpdate = silence;
    }

    public void showPopupMenu(ContextMenu menu, double screenX, double screenY){
        menu.show(visual.primaryStage, screenX, screenY);
    }

    public double getPaneX(){
        return visual.canvas.getLayoutX();
    }

    public double getPaneY(){
        return visual.canvas.getLayoutY();
    }

    public double getCanvasX(){
        return visual.paint.getXOnPane();
    }

    public double getCanvasY(){
        return visual.paint.getYOnPane();
    }

    public void repaintCanvas(){
        visual.canvas.repaint();
    }
}
