/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import cclo.hanitu.circuit.CentralNeuron;

/**
 * the viewer for neuron.
 *
 * @author antonio
 */
public class NeuronViewer extends AbstractNeuronViewer{

    final CentralNeuron neuron;
    final AxonViewer axon;

    public NeuronViewer(CentralNeuron neuron){
        super(neuron.toString());
        this.neuron = neuron;
        this.axon = new AxonViewer(this);
        text = Integer.toString(neuron.id);
    }

    @Override
    public boolean isRelative(Viewer other){
        return other == axon
               || ((other instanceof ReceptorViewer) && other.isRelative(this))
               || ((other instanceof SynapseViewer) && other.isRelative(this));
    }
}
