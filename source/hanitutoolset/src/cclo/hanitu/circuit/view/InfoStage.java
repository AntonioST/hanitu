/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import javafx.scene.Scene;
import javafx.stage.Stage;

import cclo.hanitu.FieldInfo;
import cclo.hanitu.gui.PropertySession;

/**
 * @author antonio
 */
public class InfoStage extends Stage{

    private final PropertySession property;

    public InfoStage(VisualCircuit visual){
        property = new PropertySession();
        property.modifyEvent.add(() -> visual.file.setCurrentFileModified(true));
        property.updateEvent.add(visual.canvas::repaint);
        //
        setScene(new Scene(property.getLayout()));
        setWidth(400);
        setHeight(300);
    }

    public void reset(Object v){
        if (v == null){
            hide();
            property.reset(null);
        } else {
            setTitle(v.toString());
            property.reset(FieldInfo.listFieldInfo(v));
            sizeToScene();
        }
    }

    public void setEditable(boolean edit){
        property.setEditable(edit);
    }
}
