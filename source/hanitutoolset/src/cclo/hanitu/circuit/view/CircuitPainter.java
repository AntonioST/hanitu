/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import javafx.scene.canvas.GraphicsContext;

import cclo.hanitu.circuit.SensorType;

/**
 * @author antonio
 */
public class CircuitPainter extends DefaultCircuitPainter{

    private double maxWeight = 1;

    public CircuitPainter(){
    }

    public CircuitPainter(int x, int y, int w, int h){
        super(x, y, w, h);
    }

    public CircuitPainter(int w, int h){
        super(w, h);
    }

    private double getSynapseStroke(SynapseViewer s){
        if (s.synapse == null){
            return visual.env.synapseStroke;
        }
        double strength = visual.env.maxSynapseWeightStroke * s.synapse.weight * s.synapse.conductance / maxWeight;
        if (strength < 1){
            return  1;
        }
        return strength;
    }

    @Override
    public void paint(GraphicsContext g){
        maxWeight = visual.synapseViewers.stream()
          .map(v -> v.synapse)
          .filter(s -> s != null)
          .mapToDouble(s -> s.weight * s.conductance)
          .max().orElse(0);
        if(maxWeight <= 0) maxWeight = 1;
        super.paint(g);
    }

    @Override
    protected void paintNeuronBody(GraphicsContext g, NeuronViewer n){
        g.setStroke(getColor(n, visual.env.neuronColor, visual.env.neuronHighlightColor));
        g.setLineWidth(get(n, visual.env.neuronStroke, visual.env.neuronHighlightStroke));
        super.paintNeuronBody(g, n);
    }

    @Override
    protected void paintNeuronText(GraphicsContext g, NeuronViewer n){
        g.setFont(visual.env.neuronTextFont);
        g.setFill(getColor(n, visual.env.neuronTextColor));
        super.paintNeuronText(g, n);
    }

    @Override
    protected void paintNeuronAxon(GraphicsContext g, AxonViewer a){
        g.setStroke(getColor(a, visual.env.axonColor, visual.env.axonHighlightColor));
        g.setLineWidth(get(a, visual.env.axonStroke, visual.env.axonHighlightStroke));
        super.paintNeuronAxon(g, a);
    }

    @Override
    protected void paintReceptorBody(GraphicsContext g, ReceptorViewer r){
        g.setStroke(getColor(r, visual.env.receptorColor, visual.env.receptorHighlightColor));
        g.setLineWidth(get(r, visual.env.receptorStroke, visual.env.receptorHighlightStroke));
        super.paintReceptorBody(g, r);
    }

    @Override
    protected void paintReceptorText(GraphicsContext g, ReceptorViewer r){
        g.setFont(get(r, visual.env.receptorTextFont, visual.env.receptorHighlightTextFont));
        g.setFill(getColor(r, visual.env.receptorTextColor));
        super.paintReceptorText(g, r);
    }

    @Override
    protected void paintSynapseLine(GraphicsContext g, SynapseViewer s){
        g.setStroke(getColor(s,
                             visual.env.synapseColor,
                             visual.env.synapseColor.darker(),
                             visual.env.synapseHighlightColor));
        g.setLineWidth(getSynapseStroke(s));
        super.paintSynapseLine(g, s);
    }

    @Override
    protected void paintSynapseText(GraphicsContext g, SynapseViewer s){
        g.setFont(get(s, visual.env.synapseTextFont, visual.env.synapseHighlightTextFont));
        g.setFill(getColor(s, visual.env.synapseTextColor));
        super.paintSynapseText(g, s);
    }


    @Override
    protected void paintSensor(GraphicsContext g, SensorViewer s){
        if (s.sensor.type == SensorType.FOOD){
            g.setStroke(getColor(s, visual.env.foodSensorColor));
        } else {
            g.setStroke(getColor(s, visual.env.toxicantSensorColor));
        }
        g.setLineWidth(visual.env.neuronStroke);
        super.paintSensor(g, s);
    }

    @Override
    protected void paintSensorAxon(GraphicsContext g, AxonViewer a){
        g.setStroke(getColor(a, visual.env.axonColor, visual.env.axonHighlightColor));
        g.setLineWidth(get(a, visual.env.axonStroke, visual.env.axonHighlightStroke));
        super.paintSensorAxon(g, a);
    }

    @Override
    protected void paintSensorText(GraphicsContext g, SensorViewer n){
        g.setFont(visual.env.neuronTextFont);
        g.setFill(getColor(n, visual.env.neuronTextColor));
        super.paintSensorText(g, n);
    }

    @Override
    protected void paintMotor(GraphicsContext g, MotorViewer m){
        g.setLineWidth(visual.env.neuronStroke);
        g.setStroke(getColor(m, visual.env.motorColor));
        super.paintMotor(g, m);
    }

    @Override
    protected void paintMotorReceptor(GraphicsContext g, ReceptorViewer r){
        g.setStroke(getColor(r, visual.env.receptorColor, visual.env.receptorHighlightColor));
        g.setLineWidth(get(r, visual.env.receptorStroke, visual.env.receptorHighlightStroke));
        super.paintMotorReceptor(g, r);
    }

    @Override
    protected void paintMotorText(GraphicsContext g, MotorViewer n){
        g.setFont(visual.env.neuronTextFont);
        g.setFill(getColor(n, visual.env.neuronTextColor));
        super.paintMotorText(g, n);
    }

}
