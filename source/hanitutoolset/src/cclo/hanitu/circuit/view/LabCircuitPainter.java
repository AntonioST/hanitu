/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;


import javafx.scene.paint.Color;

/**
 * XXX future feature. Just hold the space
 * @author antonio
 */
//TODO mode 1: default, same with default style
//TODO mode 2: firing rate change update neuron color
public class LabCircuitPainter extends DefaultCircuitPainter{

    public LabCircuitPainter(VisualCircuit builder){
        //XXX LabVisualCircuit
        super();
    }

    protected Color getColor(Viewer v, Color low, Color high){
        //make color change with the activity of the viewer v.
        //XXX Unsupported Operation LabStyle.getColor
        throw new UnsupportedOperationException();
    }
}
