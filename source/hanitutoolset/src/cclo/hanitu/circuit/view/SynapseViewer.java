/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import cclo.hanitu.circuit.Synapse;

import java.util.Objects;

/**
 * the viewer for synapse.
 *
 * @author antonio
 */
public class SynapseViewer extends Viewer{

    private static final int NO_ACT_DIS = 10;
    final AbstractNeuronViewer neuron;
    final Synapse synapse;
    final AxonViewer preSynAxon;
    final ReceptorViewer postSynRep;

    /*
    NeuronViewer pre -> NeuronViewer post
    new SynapseViewer(pre, pre.syn, pre.axon, post.receptor)
    NeuronViewer pre -> MotorViewer post
    new SynapseViewer(post, null, pre.axon, post.receptor)
    SensorViewer pre -> NeuronViewer post
    new SynapseViewer(pre, pre.syn, pre.axon, post.receptor)
     */
    public SynapseViewer(AbstractNeuronViewer neuron, Synapse synapse, AxonViewer axon, ReceptorViewer receptor){
        super(synapse != null? synapse.toString(): "S:" + neuron.name + "->" + receptor.name);
        this.neuron = Objects.requireNonNull(neuron);
        this.synapse = synapse;
        this.preSynAxon = Objects.requireNonNull(axon);
        this.postSynRep = Objects.requireNonNull(receptor);
    }

    @Override
    public double isTouch(double x, double y){
        if (preSynAxon == null || postSynRep == null){
            return Double.POSITIVE_INFINITY;
        }
        //point P (x, y)
        //point S (sx, sy)
        //point T (tx, ty)
        double sx = preSynAxon.location[0];
        double sy = preSynAxon.location[1];
        double tx = postSynRep.location[0];
        double ty = postSynRep.location[1];
        double dx = tx - sx;
        double dy = ty - sy;
        //boundary check
        if (Math.abs(dx) > NO_ACT_DIS && (x < Math.min(sx, tx) || Math.max(sx, tx) < x))
            return Double.POSITIVE_INFINITY;
        if (Math.abs(dy) > NO_ACT_DIS && (y < Math.min(sy, ty) || Math.max(sy, ty) < y))
            return Double.POSITIVE_INFINITY;
        //calculate triangle PST area and bottom line ST length
        //get the triangle PST height, which is the distance between point P and line ST
        //area = bottom * height / 2
        //area = |sx * (ty - y) + tx * (y - sy) + x * (sy - ty)| / 2
        //button = sqrt(dx * dx + dy * dy)
        //height = area * 2 / bottom
        //       = |sx * (ty - y) + tx * (y - sy) + x * (sy - ty)| / sqrt(dx * dx + dy * dy)
        //area = |sx*ty - sx*y + tx*y - tx*sy + x * (sy - ty)| / 2
        //     = |sx*ty - tx*sy + y * (tx - sx) + x * (sy - ty)| / 2
        //     = |sx*ty - tx*sy + y * (dx) + x * (-dy)| / 2
        //     = |sx*ty - tx*sy + y*dx - x*dy| / 2
        return Math.abs(sx * ty - tx * sy + y * dx - x * dy)
               / Math.sqrt(dx * dx + dy * dy);
    }

    @Override
    public boolean isRelative(Viewer other){
        return other == postSynRep
               || other == preSynAxon
               || ((other instanceof NeuronViewer) && other.isRelative(postSynRep))
               || ((other instanceof NeuronViewer) && other.isRelative(preSynAxon));
    }
}
