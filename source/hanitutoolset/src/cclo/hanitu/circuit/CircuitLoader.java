/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cclo.hanitu.FileLoader;
import cclo.hanitu.FileLoaderImpl;

/**
 * The loader of hanitu circuit file
 *
 * @author antonio
 */
public class CircuitLoader extends FileLoader<Circuit>{

    public static final String CIRCUIT_CONFIG_FILE_TYPE = "circuit_config";

    private static final List<Class<? extends FileLoaderImpl<Circuit>>> SUPPORT = new ArrayList<>();

    static{
        SUPPORT.add(CircuitLoaderV0102.class);
    }

    public CircuitLoader(){
    }

    /**
     * @param failOnLoad throw {@link IOException} if any error or warning occurs during loading
     */
    public CircuitLoader(boolean failOnLoad){
        super(failOnLoad);
    }

    @Override
    protected boolean isFileTypeSupport(String value){
        return value.equals(CIRCUIT_CONFIG_FILE_TYPE);
    }

    @Override
    public FileLoaderImpl<Circuit> getImplByVersion(String expr){
        FileLoaderImpl<Circuit> impl;
        for (Class<? extends FileLoaderImpl<Circuit>> cls : SUPPORT){
            if ((impl = isVersionCompatibility(expr, cls)) != null){
                return impl;
            }
        }
        return null;
    }

    @Override
    public FileLoaderImpl<Circuit> getImplByFileContent(List<String> content){
        return new CircuitLoaderV0102();
    }

    /**
     * support version `<=1.2`
     *
     * @param expr version expression
     * @return
     */
    @Override
    public boolean isVersionSupport(String expr){
        return isVersionCompatibility(expr, "<=1.2");
    }
}
