/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cclo.hanitu.FileLoader;
import cclo.hanitu.Log;
import cclo.hanitu.FileLoaderImpl;

import static cclo.hanitu.FileLoader.META_HEADER;

/**
 * @author antonio
 */
public class CircuitLoaderV0102 extends FileLoaderImpl<Circuit>{

    /**
     * Number of neurons in the circuit.
     * The sensory and motor neurons are not included.
     *
     * @since 1.0
     */
    public static final String TOTAL_NEURON_NUMBER = "Total_neuron_number";

    /**
     * the central neuron id.
     * the neuron ID should start from 0.
     *
     * @since 1.0
     */
    public static final String NEURON = "NeuronID";

    /**
     * the post-sensor or pre-motor neuron id.
     *
     * @since 1.0
     */
    public static final String BODY_TARGET_NEURON = "NeuID";

    /**
     * the receptor id, starting from 0
     *
     * @since 1.0
     */
    public static final String RECEPTOR = "Receptor";

    /**
     * the post-synapse neuron id.
     *
     * @since 1.0
     */
    public static final String TARGET_NEURON = "Targetneuron";

    /**
     * Membrane capacitance in nF.
     *
     * @since 1.0
     */
    public static final String CAPACITANCE = "C";

    /**
     * conductance in nS.
     * for neuron, it is Membrane leak conductance.
     * for synapse, it is Synaptic conductance.
     *
     * @since 1.0
     */
    public static final String CONDUCTANCE = "G";

    /**
     * the reversal potential of the neuron in mV.
     *
     * @since 1.0
     */
    public static final String NEURON_REVERSAL_POTENTIAL = "NRevPot";
    /**
     * @since 1.2
     */
    public static final String NEURON_RESET_POTENTIAL = "ResetPot";

    /**
     * the reversal potential of the receptor in mV.
     *
     * @since 1.0
     */
    public static final String RECEPTOR_REVERSAL_POTENTIAL = "RRevPot";

    /**
     * the spike threshold of the neuron spiking in mV.
     *
     * @since 1.0
     */
    public static final String SPIKING_THRESHOLD = "Threshold";

    /**
     * the refractory period of the neuron in 0.1 ms.
     *
     * @since 1.0
     */
    public static final String REFRACTORY_PERIOD = "Refperiod";

    /**
     * the Axonal spike delay of the neuron in 0.1 ms.
     *
     * @since 1.0
     */
    public static final String SPIKE_DELAY = "Spikedelay";

    /**
     * Synaptic weight.
     * it is a scaling factor.
     *
     * @since 1.0
     */
    public static final String CONNECTION_WEIGHT = "Weight";

    /**
     * Time constant of the gating variable in ms.
     *
     * @since 1.0
     */
    public static final String TIME_CONSTANT = "Tau";

    /**
     * the direction.
     *
     * ### numeric Value
     *
     * Value    | Direction
     * -----    | ---------
     * 0        | {@link Direction#FORWARD}
     * 1        | {@link Direction#BACKWARD}
     * 2        | {@link Direction#LEFTWARD}
     * 3        | {@link Direction#RIGHTWARD}
     *
     * @since 1.0
     */
    public static final String BODY_DIRECTION = "Direction";

    /**
     * Type of model.
     * currently only support the type 0: one exponential decay, voltage independent
     *
     * @since 1.0
     */
    public static final String MODEL_TYPE = "Type";

    /**
     * The mean of the fluctuation in mV.
     *
     * @since 1.0
     */
    public static final String NOISE_MEAN = "mean";

    /**
     * The standard deviation of the membrane fluctuation due to the noise in mV.
     *
     * @since 1.0
     */
    public static final String NOISE_STD = "std";

    /**
     * the membrane capacitance of the motor neuron in nF.
     *
     * @since 1.0
     */
    public static final String MOTOR_CAPACITANCE = "Mcm";

    /**
     * the synaptic time constant of the motor neuron.
     *
     * @since 1.0
     */
    public static final String MOTOR_TIME_CONSTANT = "Mtau";

    /**
     * the synaptic weight of the motor neuron.
     *
     * @since 1.0
     */
    public static final String MOTOR_WEIGHT = "Mweight";

    /**
     * the silence period of the motor neuron.
     *
     * @since 1.0
     */
    public static final String MOTOR_SILENCE_PERIOD = "Msilence";

    /**
     * the threshold of the motor neuron in mV.
     *
     * @since 1.0
     */
    public static final String MOTOR_SPIKING_THRESHOLD = "Mvth";

    /**
     * the membrane resting potential of the motor neuron in mV.
     *
     * @since 1.0
     */
    public static final String MOTOR_REVERSAL_POTENTIAL = "Mvl";
    /**
     * @since 1.2
     */
    public static final String MOTOR_RESET_POTENTIAL = "Mreset";

    /**
     * the membrane capacitance of the food sensor neuron in nF.
     *
     * @since 1.0
     */
    public static final String FOOD_SENSOR_CAPACITANCE = "SFcm";

    /**
     * the synaptic time constant of the food sensor neuron.
     *
     * @since 1.0
     */
    public static final String FOOD_SENSOR_TIME_CONSTANT = "SFtau";

    /**
     * the synaptic weight of the food sensor neuron.
     *
     * @since 1.0
     */
    public static final String FOOD_SENSOR_WEIGHT = "SFweight";

    /**
     * the silence period of the food sensor neuron.
     *
     * @since 1.0
     */
    public static final String FOOD_SENSOR_SILENCE_PERIOD = "SFsilence";

    /**
     * the threshold of the food sensor neuron in mV.
     *
     * @since 1.0
     */
    public static final String FOOD_SENSOR_SPIKING_THRESHOLD = "SFvth";

    /**
     * the membrane resting potential of the food sensor neuron in mV.
     *
     * @since 1.0
     */
    public static final String FOOD_SENSOR_REVERSAL_POTENTIAL = "SFvl";
    /**
     * @since 1.2
     */
    public static final String FOOD_SENSOR_RESET_POTENTIAL = "SFreset";

    /**
     * the membrane capacitance of the toxicant sensor neuron in nF.
     *
     * @since 1.0
     */
    public static final String TOXICANT_SENSOR_CAPACITANCE = "SCcm";

    /**
     * the synaptic time constant of the toxicant sensor neuron.
     *
     * @since 1.0
     */
    public static final String TOXICANT_SENSOR_TIME_CONSTANT = "SCtau";

    /**
     * the synaptic weight of the toxicant sensor neuron.
     *
     * @since 1.0
     */
    public static final String TOXICANT_SENSOR_WEIGHT = "SCweight";

    /**
     * the silence period of the toxicant sensor neuron.
     *
     * @since 1.0
     */
    public static final String TOXICANT_SENSOR_SILENCE_PERIOD = "SCsilence";

    /**
     * the threshold of the food toxicant neuron in mV.
     *
     * @since 1.0
     */
    public static final String TOXICANT_SENSOR_SPIKING_THRESHOLD = "SCvth";

    /**
     * the membrane resting potential of the toxicant sensor neuron in mV.
     *
     * @since 1.0
     */
    public static final String TOXICANT_SENSOR_REVERSAL_POTENTIAL = "SCvl";
    /**
     * @since 1.2
     */
    public static final String TOXICANT_SENSOR_RESET_POTENTIAL = "SCreset";

    /**
     * the end of the neuron parameters setting.
     *
     * @since 1.0
     */
    public static final String CLOSE_NEURON = "EndNeupar";

    /**
     * the end of the receptor parameters setting.
     *
     * @since 1.0
     */
    public static final String CLOSE_RECEPTOR = "EndReceptor";

    /**
     * the end of the synapse parameters setting.
     *
     * @since 1.0
     */
    public static final String CLOSE_SYNAPSE = "EndTargetneuron";
    //

    /**
     * the end of the neuron parameters setting, including receptor, synapse, noise setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_END_NEURON = "Endneuron";

    /**
     * the start of a series of receptors setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_BEGIN_RECEPTOR = "ReceptorPar";

    /**
     * the end of a series of receptors setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_END_RECEPTOR = "EndReceptorPar";

    /**
     * the start of noise setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_BEGIN_NOISE = "MembranceNoise";

    /**
     * the end of noise setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_END_NOISE = "EndMembranceNoise";

    /**
     * the start of input(sensor), output(motor) neurons setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_BEGIN_COMMUNICATION = "Communication";

    /**
     * the end of input(sensor), output(motor) neurons setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_END_COMMUNICATION = "EndCommunication";

    /**
     * the start of a series of input(sensor) neurons setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_BEGIN_SENSOR = "Inputneuron";

    /**
     * the end of a series of input(sensor) neurons setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_END_SENSOR = "EndInputneuron";

    /**
     * the start of a series of output(motor) neurons setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_BEGIN_MOTOR = "OutputNeuron";

    /**
     * the end of a series of output(motor) neurons setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_END_MOTOR = "EndOutputNeuron";

    /**
     * the start of a series of body neurons setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_BEGIN_BODY = "BodyPar";

    /**
     * the end of a series of body neurons setting.
     *
     * @since 1.0
     */
    public static final String BLOCK_END_BODY = "EndBodyPar";

    /**
     * initial state
     */
    private static final int HEAD_STATE = 1;
    /**
     * parsing the parameter of the neuron
     */
    private static final int NEURON_PAR_STATE = 2;
    /**
     * parsing the membrane noise parameter of the neuron
     */
    private static final int NEURON_NOISE_STATE = 3;
    /**
     * parsing the components of the neuron
     */
    private static final int NEURON_COM_STATE = 4;
    /**
     * parsing the parameters of the receptor on the neuron
     */
    private static final int NEURON_REP_STATE = 5;
    /**
     * parsing the parameters of connection(synapse) of the neuron
     */
    private static final int NEURON_TARGET_STATE = 6;
    /**
     * parsing the body neurons
     */
    private static final int COMMUNICATION_STATE = 7;
    /**
     * parsing post-sensor synapse
     */
    private static final int INPUT_STATE = 9;
    /**
     * parsing pre-motor synapse
     */
    private static final int OUTPUT_STATE = 10;
    /**
     * parsing the parameters of the body neuron
     */
    private static final int BODY_STATE = 11;

    /**
     * circuit builder
     */
    private CircuitBuilder builder;
    //temporary field
    /**
     * temporary neuron
     */
    protected CentralNeuron neuron;
    /**
     * temporary receptor
     */
    private Receptor receptor;
    /**
     * temporary synapse
     */
    private DummySynapse dummySynapse;
    /**
     * the collection of temporary synapse
     */
    private ArrayList<DummySynapse> dummySynapseList;
    /**
     * neuron counter
     */
    private int neuronCounter;
    /**
     * temporary post synapse neuron id
     */
    private int postSynapseNeuronID;
    /**
     * temporary receptor id
     */
    private int receptorID;
    /**
     * temporary direction
     */
    private int direction;
    /**
     * temporary field
     */
    private int type;
    /**
     * temporary field
     */
    private double g;
    /**
     * temporary field
     */
    private double weight;

    @Override
    public String getSourceVersion(){
        return "1.2";
    }

    @Override
    public String versionRequired(){
        return "<1.4";
    }


    @Override
    public void startLoading(FileLoader<Circuit> loader){
        if (!(loader instanceof CircuitLoader)){
            throw new IllegalArgumentException();
        }
        super.startLoading(loader);
        builder = new CircuitBuilder();
        neuron = null;
        receptor = null;
        dummySynapse = null;
        dummySynapseList = null;
    }

    @Override
    public Circuit endLoading(){
        return builder.createCircuit();
    }

    @Override
    public void parseLine(String line){
        switch (getCurrentState()){
        case INIT_STATE:
            setState(HEAD_STATE);
        case HEAD_STATE:
            head(line);
            break;
        case NEURON_PAR_STATE:
            neuronParameter(line);
            break;
        case NEURON_NOISE_STATE:
            neuronNoise(line);
            break;
        case NEURON_COM_STATE:
            neuronComponent(line);
            break;
        case NEURON_REP_STATE:
            receptor(line);
            break;
        case NEURON_TARGET_STATE:
            target(line);
            break;
        case COMMUNICATION_STATE:
            communication(line);
            break;
        case INPUT_STATE:
            input(line);
            break;
        case OUTPUT_STATE:
            output(line);
            break;
        case BODY_STATE:
            body(line);
            break;
        }
    }

    /**
     * change state according block-beginning keyword.
     */
    private void setStateByKeyword(String line){
        switch (line){
        case META_HEADER:
            break;
        case BLOCK_BEGIN_BODY:
            setState(BODY_STATE);
            break;
        case BLOCK_BEGIN_COMMUNICATION:
            setState(COMMUNICATION_STATE);
            break;
        case BLOCK_BEGIN_MOTOR:
            setState(OUTPUT_STATE);
            break;
        case BLOCK_BEGIN_NOISE:
            setState(NEURON_NOISE_STATE);
            break;
        case BLOCK_BEGIN_RECEPTOR:
            setState(NEURON_REP_STATE);
            break;
        case BLOCK_BEGIN_SENSOR:
            setState(INPUT_STATE);
            break;
        }
    }

    /**
     * parsing method for {@link #HEAD_STATE}
     *
     * ### State Transfer Rule
     *
     * Parsing Keywords or State            | state transfer or handle
     * -------------------------            | ------------------------
     * {@link #INIT_STATE}                  | {@link #HEAD_STATE}
     * {@link #TOTAL_NEURON_NUMBER}         | _no change_
     * {@link #NEURON}                      | {@link #NEURON_PAR_STATE}
     * {@link #BLOCK_BEGIN_COMMUNICATION}   | {@link #COMMUNICATION_STATE}
     * _null_                               | {@link #TERMINAL_STATE}
     */
    protected void head(String currentLine){
        if (currentLine == null){
            setState(TERMINAL_STATE);
            return;
        }
        int value;
        switch (getKey(currentLine)){
        case META_HEADER:
            break;
        case TOTAL_NEURON_NUMBER:
            Log.debug(() -> "circuit file : " + getLoader().getFileName());
            value = getIntValue(currentLine);
            if (value < 0){
                throwError("negative total neuron number");
                setState(ERROR_STATE);
            } else {
                Log.debug(() -> "total neuron : " + value);
                // create _value_ neurons
                for (int i = 0; i < value; i++){
                    builder.createNeuron();
                }
                dummySynapseList = new ArrayList<>();
                neuronCounter = 0;
            }
            break;
        case NEURON:
            value = getIntValue(currentLine);
            try {
                if (value >= 0){
                    neuron = builder.getNeuron(value);
                    neuronCounter++;
                    setState(NEURON_PAR_STATE);
                    break;
                }
            } catch (IndexOutOfBoundsException ex){
                throwError("wrong neuron ID : " + value,
                           "total neuron number : " + builder.neuronCount());
            }
            Log.debug("skip neuron");
            String line = getLoader().tryInRescueMode(() -> getLoader().skipUntil(ln -> {
                String k = getKey(ln);
                return k.equals(NEURON)
                       || k.equals(BLOCK_END_NEURON)
                       || k.equals(BLOCK_BEGIN_COMMUNICATION);
            }));
            if (line != null){
                String key = getKey(line);
                switch (key){
                case NEURON:
                    getLoader().reParseLine();
                    break;
                case BLOCK_END_NEURON:
                    neuron = null;
                    setState(HEAD_STATE);
                    break;
                case BLOCK_BEGIN_COMMUNICATION:
                    getLoader().reParseLine();
                    break;
                }
            } else {
                setState(ERROR_STATE);
            }
            break;
        case BLOCK_BEGIN_COMMUNICATION:
            if (neuronCounter != builder.neuronCount()){
                throwException("wrong neuron count",
                               "expect total neuron number : " + builder.neuronCount(),
                               "only defined : " + neuronCounter);
            }
            //all neuron parse over, all receptor have created
            //create connection and remove resource
            for (DummySynapse ds : dummySynapseList){
                try {
                    Synapse ret = builder.createSynapse(ds.pre, ds.post, ds.rep);
                    ret.conductance = ds.conductance;
                    ret.weight = ds.weight;
                } catch (Exception e){
                    throwException(e.getClass().getName(),
                                   e.getMessage(),
                                   "synapse at line : " + ds.lineNumber,
                                   "prev neuron : " + ds.pre,
                                   "post neuron : " + ds.post,
                                   "target rep : " + ds.rep);
                }
            }
            dummySynapseList = null;
            setState(COMMUNICATION_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(currentLine,
                                                       TOTAL_NEURON_NUMBER,
                                                       NEURON,
                                                       BLOCK_BEGIN_COMMUNICATION);
            assert r != null;
            getLoader().reParseLine(r, null);
        }
    }

    /**
     * parsing method for {@link #NEURON_PAR_STATE}
     *
     * ### State Transfer Rule
     *
     * Parsing Keywords or State    | state transfer or handle
     * -------------------------    | ------------------------
     * {@link #BLOCK_BEGIN_NOISE}   | {@link #NEURON_NOISE_STATE}
     * {@link #CLOSE_NEURON}        | {@link #NEURON_COM_STATE}
     * BLOCK_BEGIN*                 | **error** transfer to BLOCK_BEGIN*
     * BLOCK_END*                   | **error** transfer to {@link #NEURON_COM_STATE}
     * CLOSE*                       | **error** transfer to {@link #NEURON_COM_STATE}
     */
    protected void neuronParameter(String line){
        switch (getKey(line)){
        case META_HEADER:{
            switch (getMetaKey(line)){
            case CentralNeuron.META_X:
                neuron.putMeta(CentralNeuron.META_X, Integer.parseInt(getMetaValue(line)));
                break;
            case CentralNeuron.META_Y:
                neuron.putMeta(CentralNeuron.META_Y, Integer.parseInt(getMetaValue(line)));
                break;
            case CentralNeuron.META_THETA:
                neuron.putMeta(CentralNeuron.META_THETA, Math.toRadians(Double.parseDouble(getMetaValue(line))));
                break;
            }
            break;
        }
        case CAPACITANCE:
            neuron.capacitance = getFloatValue(line);
            break;
        case CONDUCTANCE:
            neuron.conductance = getFloatValue(line);
            break;
        case NEURON_REVERSAL_POTENTIAL:
            neuron.reversalPotential = getFloatValue(line);
            break;
        case NEURON_RESET_POTENTIAL:
            neuron.resetPotential = getFloatValue(line);
            break;
        case SPIKING_THRESHOLD:
            neuron.threshold = getFloatValue(line);
            break;
        case REFRACTORY_PERIOD:
            neuron.refractoryPeriod = getIntValue(line);
            break;
        case SPIKE_DELAY:
            neuron.spikeDelay = getIntValue(line);
            break;
        case BLOCK_BEGIN_NOISE:
            setState(NEURON_NOISE_STATE);
            break;
        case BLOCK_BEGIN_BODY:
        case BLOCK_BEGIN_COMMUNICATION:
        case BLOCK_BEGIN_SENSOR:
        case BLOCK_BEGIN_MOTOR:
        case BLOCK_BEGIN_RECEPTOR:
            throwErrorKeywordException(CLOSE_NEURON);
            setStateByKeyword(line);
            break;
        case BLOCK_END_BODY:
        case BLOCK_END_COMMUNICATION:
        case BLOCK_END_SENSOR:
        case BLOCK_END_MOTOR:
        case BLOCK_END_RECEPTOR:
        case BLOCK_END_NOISE:
        case CLOSE_RECEPTOR:
        case CLOSE_SYNAPSE:
            throwErrorKeywordException(CLOSE_NEURON);
        case CLOSE_NEURON:
            setState(NEURON_COM_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(line,
                                                       CAPACITANCE,
                                                       CONDUCTANCE,
                                                       NEURON_REVERSAL_POTENTIAL,
                                                       NEURON_RESET_POTENTIAL,
                                                       SPIKING_THRESHOLD,
                                                       REFRACTORY_PERIOD,
                                                       SPIKE_DELAY,
                                                       BLOCK_BEGIN_NOISE,
                                                       CLOSE_NEURON);
            assert r != null;
            switch (r){
            case CLOSE_NEURON:
                setState(NEURON_COM_STATE);
                break;
            case BLOCK_BEGIN_NOISE:
                setState(NEURON_NOISE_STATE);
                break;
            default:
                getLoader().tryRescue(r, null);
            }
        }
    }

    /**
     * parsing method for {@link #NEURON_NOISE_STATE}
     *
     * ### State Transfer Rule
     *
     * Parsing Keywords or State    | state transfer or handle
     * -------------------------    | ------------------------
     * {@link #BLOCK_BEGIN_NOISE}   | {@link #NEURON_NOISE_STATE}
     * {@link #BLOCK_END_NOISE}     | {@link #NEURON_PAR_STATE}
     * BLOCK_BEGIN*                 | **error** transfer to BLOCK_BEGIN*
     * BLOCK_END*                   | **error** transfer to {@link #NEURON_PAR_STATE}
     * CLOSE*                       | **error** transfer to {@link #NEURON_PAR_STATE}
     */
    protected void neuronNoise(String line){
        switch (getKey(line)){
        case META_HEADER:
            break;
        case NOISE_MEAN:
            neuron.noiseMean = getFloatValue(line);
            break;
        case NOISE_STD:
            neuron.noiseStd = getFloatValue(line);
            break;
        case BLOCK_BEGIN_BODY:
        case BLOCK_BEGIN_COMMUNICATION:
        case BLOCK_BEGIN_SENSOR:
        case BLOCK_BEGIN_MOTOR:
        case BLOCK_BEGIN_RECEPTOR:
        case BLOCK_BEGIN_NOISE:
            throwErrorKeywordException(BLOCK_END_NOISE);
            setStateByKeyword(line);
            break;
        case BLOCK_END_BODY:
        case BLOCK_END_COMMUNICATION:
        case BLOCK_END_SENSOR:
        case BLOCK_END_MOTOR:
        case BLOCK_END_RECEPTOR:
        case CLOSE_NEURON:
        case CLOSE_RECEPTOR:
        case CLOSE_SYNAPSE:
            throwErrorKeywordException(BLOCK_END_NOISE);
        case BLOCK_END_NOISE:
            setState(NEURON_PAR_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(line,
                                                       NOISE_MEAN,
                                                       NOISE_STD,
                                                       BLOCK_END_NOISE);
            assert r != null;
            if (r.equals(BLOCK_END_NOISE)){
                setState(NEURON_PAR_STATE);
            } else {
                getLoader().tryRescue(r, null);
            }
        }
    }

    /**
     * parsing method for {@link #NEURON_COM_STATE}
     *
     * ### State Transfer Rule
     *
     * Parsing Keywords or State        | state transfer or handle
     * -------------------------        | ------------------------
     * {@link #BLOCK_BEGIN_RECEPTOR}    | {@link #NEURON_REP_STATE}
     * {@link #TARGET_NEURON}           | {@link #NEURON_TARGET_STATE}
     * {@link #BLOCK_END_NEURON}        | {@link #HEAD_STATE}
     */
    protected void neuronComponent(String currentLine){
        switch (getKey(currentLine)){
        case META_HEADER:
            break;
        case BLOCK_BEGIN_RECEPTOR:
            setState(NEURON_REP_STATE);
            break;
        case TARGET_NEURON:
            postSynapseNeuronID = getIntValue(currentLine);
            if (0 <= postSynapseNeuronID && postSynapseNeuronID < builder.neuronCount()){
                setState(NEURON_TARGET_STATE);
            } else {
                if (postSynapseNeuronID > 0){
                    throwException("wrong target neuron ID",
                                   "total neuron number : " + builder.neuronCount());
                }
                Log.debug("skip synapse");
                String line = getLoader().tryInRescueMode(() -> getLoader().skipUntil(ln -> {
                    String key = getKey(ln);
                    return key.equals(TARGET_NEURON)
                           || key.equals(BLOCK_BEGIN_RECEPTOR)
                           || key.equals(CLOSE_SYNAPSE)
                           || key.equals(BLOCK_END_NEURON);
                }));
                if (line != null){
                    String key = getKey(line);
                    switch (key){
                    case TARGET_NEURON:
                        getLoader().reParseLine(key, null);
                        break;
                    case CLOSE_SYNAPSE:
                        break;
                    case BLOCK_BEGIN_RECEPTOR:
                        setState(NEURON_REP_STATE);
                        break;
                    case BLOCK_END_NEURON:
                        neuron = null;
                        setState(HEAD_STATE);
                        break;
                    }
                } else {
                    setState(ERROR_STATE);
                }
            }
            break;
        case BLOCK_END_NEURON:
            neuron = null;
            setState(HEAD_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(currentLine,
                                                       BLOCK_BEGIN_RECEPTOR,
                                                       TARGET_NEURON,
                                                       BLOCK_END_NEURON);
            assert r != null;
            getLoader().reParseLine(r, null);
        }
    }

    /**
     * parsing method for {@link #NEURON_REP_STATE}
     *
     * ### State Transfer Rule
     *
     * Parsing Keywords or State    | state transfer or handle
     * -------------------------    | ------------------------
     * {@link #BLOCK_END_RECEPTOR}  | {@link #NEURON_COM_STATE}
     * {@link #CLOSE_RECEPTOR}      | _no change_
     * BLOCK_BEGIN*                 | **error** transfer to BLOCK_BEGIN*
     * BLOCK_END*                   | **error** transfer to {@link #NEURON_COM_STATE}
     * CLOSE*                       | **error** transfer to {@link #NEURON_COM_STATE}
     */
    protected void receptor(String line){
        switch (getKey(line)){
        case META_HEADER:
            break;
        case RECEPTOR:{
            CircuitLoaderV0102 self = this;
            getLoader().tryDo(() -> self.receptor = builder.createReceptor(neuron.id));
            break;
        }
        case MODEL_TYPE:
            //XXX hanitu of current version not implement receptor QDE type
            break;
        case TIME_CONSTANT:
            receptor.timeConstant = getFloatValue(line);
            break;
        case RECEPTOR_REVERSAL_POTENTIAL:
            receptor.reversalPotential = getFloatValue(line);
            break;
        case CLOSE_RECEPTOR:
            receptor = null;
            break;
        case BLOCK_BEGIN_BODY:
        case BLOCK_BEGIN_COMMUNICATION:
        case BLOCK_BEGIN_SENSOR:
        case BLOCK_BEGIN_MOTOR:
        case BLOCK_BEGIN_NOISE:
        case BLOCK_BEGIN_RECEPTOR:
            throwErrorKeywordException(BLOCK_END_RECEPTOR);
            setStateByKeyword(line);
            break;
        case BLOCK_END_BODY:
        case BLOCK_END_COMMUNICATION:
        case BLOCK_END_SENSOR:
        case BLOCK_END_MOTOR:
        case BLOCK_END_NOISE:
        case CLOSE_SYNAPSE:
        case CLOSE_NEURON:
            throwErrorKeywordException(BLOCK_END_RECEPTOR);
        case BLOCK_END_RECEPTOR:
            setState(NEURON_COM_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(line,
                                                       RECEPTOR,
                                                       MODEL_TYPE,
                                                       TIME_CONSTANT,
                                                       RECEPTOR_REVERSAL_POTENTIAL,
                                                       CLOSE_RECEPTOR,
                                                       BLOCK_END_RECEPTOR);
            assert r != null;
            switch (r){
            case RECEPTOR:
            case CLOSE_RECEPTOR:
            case BLOCK_END_RECEPTOR:
                getLoader().reParseLine(r, null);
                break;
            default:
                getLoader().tryRescue(r, null);
            }
        }
    }

    /**
     * parsing method for {@link #NEURON_TARGET_STATE}
     *
     * ### State Transfer Rule
     *
     * Parsing Keywords or State    | state transfer or handle
     * -------------------------    | ------------------------
     * {@link #CLOSE_SYNAPSE}       | {@link #NEURON_COM_STATE}
     * BLOCK_BEGIN*                 | **error** transfer to BLOCK_BEGIN*
     * BLOCK_END*                   | **error** transfer to {@link #NEURON_COM_STATE}
     * CLOSE*                       | **error** transfer to {@link #NEURON_COM_STATE}
     */
    protected void target(String line){
        switch (getKey(line)){
        case META_HEADER:
            break;
        case RECEPTOR:
            dummySynapse = new DummySynapse();
            dummySynapse.pre = neuron.id;
            dummySynapse.post = postSynapseNeuronID;
            dummySynapse.rep = getIntValue(line);
            dummySynapse.lineNumber = getLoader().getCurrentLineNumber();
            dummySynapseList.add(dummySynapse);
            break;
        case CONNECTION_WEIGHT:
            dummySynapse.weight = getFloatValue(line);
            break;
        case CONDUCTANCE:
            dummySynapse.conductance = getFloatValue(line);
            break;
        case BLOCK_BEGIN_BODY:
        case BLOCK_BEGIN_COMMUNICATION:
        case BLOCK_BEGIN_SENSOR:
        case BLOCK_BEGIN_MOTOR:
        case BLOCK_BEGIN_NOISE:
        case BLOCK_BEGIN_RECEPTOR:
            throwErrorKeywordException(CLOSE_SYNAPSE);
            setStateByKeyword(line);
            break;
        case BLOCK_END_BODY:
        case BLOCK_END_COMMUNICATION:
        case BLOCK_END_SENSOR:
        case BLOCK_END_MOTOR:
        case BLOCK_END_NOISE:
        case BLOCK_END_RECEPTOR:
        case CLOSE_RECEPTOR:
        case CLOSE_NEURON:
            throwErrorKeywordException(CLOSE_SYNAPSE);
        case CLOSE_SYNAPSE:
            dummySynapse = null;
            setState(NEURON_COM_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(line,
                                                       RECEPTOR,
                                                       CONNECTION_WEIGHT,
                                                       CONDUCTANCE,
                                                       CLOSE_SYNAPSE);
            assert r != null;
            if (r.equals(CLOSE_SYNAPSE)){
                dummySynapse = null;
                setState(NEURON_COM_STATE);
            } else {
                getLoader().tryRescue(r, null);
            }
        }
    }

    /**
     * parsing method for {@link #COMMUNICATION_STATE}
     *
     * ### State Transfer Rule
     *
     * Parsing Keywords or State        | state transfer or handle
     * -------------------------        | ------------------------
     * {@link #BLOCK_BEGIN_SENSOR}      | {@link #INPUT_STATE}
     * {@link #BLOCK_BEGIN_MOTOR}       | {@link #OUTPUT_STATE}
     * {@link #BLOCK_BEGIN_BODY}        | {@link #BODY_STATE}
     * {@link #BLOCK_END_COMMUNICATION} | {@link #HEAD_STATE}
     */
    protected void communication(String line){
        switch (line){
        case META_HEADER:
            break;
        case BLOCK_BEGIN_SENSOR:
            postSynapseNeuronID = -1;
            setState(INPUT_STATE);
            break;
        case BLOCK_BEGIN_MOTOR:
            direction = 0;
            setState(OUTPUT_STATE);
            break;
        case BLOCK_BEGIN_BODY:
            setState(BODY_STATE);
            break;
        case BLOCK_END_COMMUNICATION:
            setState(HEAD_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(line,
                                                       BLOCK_BEGIN_SENSOR,
                                                       BLOCK_BEGIN_MOTOR,
                                                       BLOCK_BEGIN_BODY,
                                                       BLOCK_END_COMMUNICATION);
            assert r != null;
            getLoader().reParseLine(r, null);
        }
    }

    /**
     * parsing method for {@link #INPUT_STATE}
     *
     * ### State Transfer Rule
     *
     * Parsing Keywords or State    | state transfer or handle
     * -------------------------    | ------------------------
     * {@link #BLOCK_END_SENSOR}    | {@link #COMMUNICATION_STATE}
     * BLOCK_BEGIN*                 | **error** transfer to BLOCK_BEGIN*
     * BLOCK_END*                   | **error** transfer to {@link #COMMUNICATION_STATE}
     * CLOSE*                       | **error** transfer to {@link #COMMUNICATION_STATE}
     */
    protected void input(String line){
        switch (getKey(line)){
        case META_HEADER:
            break;
        case BODY_TARGET_NEURON:
            if (postSynapseNeuronID != -1){
                createSensorSynapse(postSynapseNeuronID);
            }
            postSynapseNeuronID = getIntValue(line);
            break;
        case RECEPTOR:
            receptorID = getIntValue(line);
            break;
        case MODEL_TYPE:
            type = getIntValue(line);
            break;
        case BODY_DIRECTION:
            direction = getIntValue(line);
            break;
        case CONNECTION_WEIGHT:
            weight = getFloatValue(line);
            break;
        case CONDUCTANCE:
            g = getFloatValue(line);
            break;
        case BLOCK_BEGIN_BODY:
        case BLOCK_BEGIN_COMMUNICATION:
        case BLOCK_BEGIN_SENSOR:
        case BLOCK_BEGIN_MOTOR:
        case BLOCK_BEGIN_NOISE:
        case BLOCK_BEGIN_RECEPTOR:
            throwErrorKeywordException(BLOCK_END_SENSOR);
            setStateByKeyword(line);
            break;
        case BLOCK_END_BODY:
        case BLOCK_END_COMMUNICATION:
        case BLOCK_END_MOTOR:
        case BLOCK_END_NOISE:
        case BLOCK_END_RECEPTOR:
        case CLOSE_RECEPTOR:
        case CLOSE_NEURON:
        case CLOSE_SYNAPSE:
            throwErrorKeywordException(BLOCK_END_SENSOR);
        case BLOCK_END_SENSOR:
            if (postSynapseNeuronID >= 0){
                createSensorSynapse(postSynapseNeuronID);
            }
            setState(COMMUNICATION_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(line,
                                                       BODY_TARGET_NEURON,
                                                       RECEPTOR,
                                                       MODEL_TYPE,
                                                       BODY_DIRECTION,
                                                       CONNECTION_WEIGHT,
                                                       CONDUCTANCE,
                                                       BLOCK_END_SENSOR);
            assert r != null;
            if (r.equals(BLOCK_END_SENSOR)){
                if (postSynapseNeuronID >= 0){
                    createSensorSynapse(postSynapseNeuronID);
                }
                setState(COMMUNICATION_STATE);
            } else {
                getLoader().tryRescue(r, null);
            }
        }
    }

    protected void createSensorSynapse(int post){
        getLoader().tryDo(() -> {
            Synapse s = builder.createSensorSynapse(SensorType.values()[type],
                                                    Direction.values()[direction + 1],
                                                    post,
                                                    receptorID);
            s.weight = weight;
            s.conductance = g;
        });
    }

    /**
     * parsing method for {@link #OUTPUT_STATE}
     *
     * ### State Transfer Rule
     *
     * Parsing Keywords or State    | state transfer or handle
     * -------------------------    | ------------------------
     * {@link #BLOCK_END_MOTOR}     | {@link #COMMUNICATION_STATE}
     * BLOCK_BEGIN*                 | **error** transfer to BLOCK_BEGIN*
     * BLOCK_END*                   | **error** transfer to {@link #COMMUNICATION_STATE}
     * CLOSE*                       | **error** transfer to {@link #COMMUNICATION_STATE}
     */
    protected void output(String line){
        switch (getKey(line)){
        case META_HEADER:
            break;
        case BODY_TARGET_NEURON:
            int preNeuronId = getIntValue(line);
            if (preNeuronId >= 0){
                getLoader().tryDo(() -> builder.setPreMotorNeuron(preNeuronId, Direction.values()[direction + 1]));
            }
            direction = (direction + 1) % 4;
            break;
        case BLOCK_BEGIN_BODY:
        case BLOCK_BEGIN_COMMUNICATION:
        case BLOCK_BEGIN_SENSOR:
        case BLOCK_BEGIN_MOTOR:
        case BLOCK_BEGIN_NOISE:
        case BLOCK_BEGIN_RECEPTOR:
            throwErrorKeywordException(BLOCK_END_MOTOR);
            setStateByKeyword(line);
            break;
        case BLOCK_END_BODY:
        case BLOCK_END_COMMUNICATION:
        case BLOCK_END_SENSOR:
        case BLOCK_END_NOISE:
        case BLOCK_END_RECEPTOR:
        case CLOSE_RECEPTOR:
        case CLOSE_NEURON:
        case CLOSE_SYNAPSE:
            throwErrorKeywordException(BLOCK_END_MOTOR);
        case BLOCK_END_MOTOR:
            setState(COMMUNICATION_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(line,
                                                       BODY_TARGET_NEURON,
                                                       BLOCK_END_MOTOR);
            assert r != null;
            getLoader().reParseLine(r, null);
        }
    }

    /**
     * parsing method for {@link #BODY_STATE}
     *
     * ### State Transfer Rule
     *
     * Parsing Keywords or State    | state transfer or handle
     * -------------------------    | ------------------------
     * {@link #BLOCK_END_BODY}      | {@link #COMMUNICATION_STATE}
     * BLOCK_BEGIN*                 | **error** transfer to BLOCK_BEGIN*
     * BLOCK_END*                   | **error** transfer to {@link #COMMUNICATION_STATE}
     * CLOSE*                       | **error** transfer to {@link #COMMUNICATION_STATE}
     */
    protected void body(String line){
        switch (getKey(line)){
        case META_HEADER:
            break;
        case MOTOR_CAPACITANCE:
            builder.motor().capacitance = getFloatValue(line);
            break;
        case FOOD_SENSOR_CAPACITANCE:
            builder.sensor(SensorType.FOOD).capacitance = getFloatValue(line);
            break;
        case TOXICANT_SENSOR_CAPACITANCE:
            builder.sensor(SensorType.TOXICANT).capacitance = getFloatValue(line);
            break;
        case MOTOR_TIME_CONSTANT:
            builder.motor().timeConstant = getFloatValue(line);
            break;
        case FOOD_SENSOR_TIME_CONSTANT:
            builder.sensor(SensorType.FOOD).timeConstant = getFloatValue(line);
            break;
        case TOXICANT_SENSOR_TIME_CONSTANT:
            builder.sensor(SensorType.TOXICANT).timeConstant = getFloatValue(line);
            break;
        case MOTOR_WEIGHT:
            builder.motor().weight = getFloatValue(line);
            break;
        case FOOD_SENSOR_WEIGHT:
            builder.sensor(SensorType.FOOD).weight = getFloatValue(line);
            break;
        case TOXICANT_SENSOR_WEIGHT:
            builder.sensor(SensorType.TOXICANT).weight = getFloatValue(line);
            break;
        case MOTOR_SILENCE_PERIOD:
            builder.motor().refractoryPeriod = getIntValue(line);
            break;
        case FOOD_SENSOR_SILENCE_PERIOD:
            builder.sensor(SensorType.FOOD).refractoryPeriod = getIntValue(line);
            break;
        case TOXICANT_SENSOR_SILENCE_PERIOD:
            builder.sensor(SensorType.TOXICANT).refractoryPeriod = getIntValue(line);
            break;
        case MOTOR_SPIKING_THRESHOLD:
            builder.motor().threshold = getFloatValue(line);
            break;
        case FOOD_SENSOR_SPIKING_THRESHOLD:
            builder.sensor(SensorType.FOOD).threshold = getFloatValue(line);
            break;
        case TOXICANT_SENSOR_SPIKING_THRESHOLD:
            builder.sensor(SensorType.TOXICANT).threshold = getFloatValue(line);
            break;
        case MOTOR_REVERSAL_POTENTIAL:
            builder.motor().reversalPotential = getFloatValue(line);
            break;
        case FOOD_SENSOR_REVERSAL_POTENTIAL:
            builder.sensor(SensorType.FOOD).reversalPotential = getFloatValue(line);
            break;
        case TOXICANT_SENSOR_REVERSAL_POTENTIAL:
            builder.sensor(SensorType.TOXICANT).reversalPotential = getFloatValue(line);
            break;
        case MOTOR_RESET_POTENTIAL:
            builder.motor().resetPotential = getFloatValue(line);
            break;
        case FOOD_SENSOR_RESET_POTENTIAL:
            builder.sensor(SensorType.FOOD).resetPotential = getFloatValue(line);
            break;
        case TOXICANT_SENSOR_RESET_POTENTIAL:
            builder.sensor(SensorType.TOXICANT).resetPotential = getFloatValue(line);
            break;
        case BLOCK_BEGIN_BODY:
        case BLOCK_BEGIN_COMMUNICATION:
        case BLOCK_BEGIN_SENSOR:
        case BLOCK_BEGIN_MOTOR:
        case BLOCK_BEGIN_NOISE:
        case BLOCK_BEGIN_RECEPTOR:
            throwErrorKeywordException(BLOCK_END_BODY);
            setStateByKeyword(line);
            break;
        case BLOCK_END_COMMUNICATION:
        case BLOCK_END_MOTOR:
        case BLOCK_END_SENSOR:
        case BLOCK_END_NOISE:
        case BLOCK_END_RECEPTOR:
        case CLOSE_RECEPTOR:
        case CLOSE_NEURON:
        case CLOSE_SYNAPSE:
            throwErrorKeywordException(BLOCK_END_BODY);
        case BLOCK_END_BODY:
            setState(COMMUNICATION_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(line,
                                                       MOTOR_CAPACITANCE,
                                                       FOOD_SENSOR_CAPACITANCE,
                                                       TOXICANT_SENSOR_CAPACITANCE,
                                                       MOTOR_TIME_CONSTANT,
                                                       FOOD_SENSOR_TIME_CONSTANT,
                                                       TOXICANT_SENSOR_TIME_CONSTANT,
                                                       MOTOR_WEIGHT,
                                                       FOOD_SENSOR_WEIGHT,
                                                       TOXICANT_SENSOR_WEIGHT,
                                                       MOTOR_SILENCE_PERIOD,
                                                       FOOD_SENSOR_SILENCE_PERIOD,
                                                       TOXICANT_SENSOR_SILENCE_PERIOD,
                                                       MOTOR_SPIKING_THRESHOLD,
                                                       FOOD_SENSOR_SPIKING_THRESHOLD,
                                                       TOXICANT_SENSOR_SPIKING_THRESHOLD,
                                                       MOTOR_REVERSAL_POTENTIAL,
                                                       FOOD_SENSOR_REVERSAL_POTENTIAL,
                                                       TOXICANT_SENSOR_REVERSAL_POTENTIAL,
                                                       MOTOR_RESET_POTENTIAL,
                                                       FOOD_SENSOR_RESET_POTENTIAL,
                                                       TOXICANT_SENSOR_RESET_POTENTIAL,
                                                       BLOCK_END_BODY);
            assert r != null;
            if (r.equals(BLOCK_END_BODY)){
                getLoader().reParseLine(r, null);
            } else {
                getLoader().tryRescue(r, null);
            }
        }
    }
}
