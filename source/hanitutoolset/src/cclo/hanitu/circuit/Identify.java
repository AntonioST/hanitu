/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

/**
 * provide a identify field.
 *
 * @author antonio
 */
abstract class Identify implements Comparable<Identify>{

    /**
     * identify
     */
    public int id = -1;

    /**
     * create a with negative default identify
     */
    Identify(){
    }

    /**
     * @param id identify number
     */
    Identify(int id){
        this.id = id;
    }

    @Override
    public int compareTo(Identify o){
        return Integer.compare(id, o.id);
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Identify identify1 = (Identify)o;

        return id == identify1.id;

    }

    @Override
    public int hashCode(){
        return id;
    }
}
