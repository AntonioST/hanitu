/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.io.PrintStream;

import cclo.hanitu.FilePrinterImpl;

/**
 * print the text content of the circuit to the output stream
 *
 * @author antonio
 */
public abstract class CircuitPrinterImpl extends FilePrinterImpl<Circuit>{

    @Override
    public CircuitPrinter getPrinter(){
        return (CircuitPrinter)super.getPrinter();
    }

    @Override
    public void write(PrintStream ps, Circuit c){
        begin();
        {
            preCircuit();
            processCircuit(c);
            for (CentralNeuron n : c.neurons()){
                preNeuron();
                processNeuron(n);
                postNeuronParameter(n);
                preReceptor();
                for (Receptor r : n.receptors){
                    processReceptor(r);
                    postReceptorParameter(r);
                }
                postReceptor();
                for (Synapse s : n.targets){
                    processSynapse(s);
                    postSynapseParameter(s);
                }
                postNeuron();
            }
            postCircuit();
        }
        {
            pareCommunication();
            {
                preInput();
                c.sensor(SensorType.FOOD, Direction.FORWARD).targets
                  .forEach(s -> processInput(s, Direction.FORWARD, SensorType.FOOD));
                c.sensor(SensorType.FOOD, Direction.BACKWARD).targets
                  .forEach(s -> processInput(s, Direction.BACKWARD, SensorType.FOOD));
                c.sensor(SensorType.FOOD, Direction.LEFTWARD).targets
                  .forEach(s -> processInput(s, Direction.LEFTWARD, SensorType.FOOD));
                c.sensor(SensorType.FOOD, Direction.RIGHTWARD).targets
                  .forEach(s -> processInput(s, Direction.RIGHTWARD, SensorType.FOOD));
                c.sensor(SensorType.TOXICANT, Direction.FORWARD).targets
                  .forEach(s -> processInput(s, Direction.FORWARD, SensorType.TOXICANT));
                c.sensor(SensorType.TOXICANT, Direction.BACKWARD).targets
                  .forEach(s -> processInput(s, Direction.BACKWARD, SensorType.TOXICANT));
                c.sensor(SensorType.TOXICANT, Direction.LEFTWARD).targets
                  .forEach(s -> processInput(s, Direction.LEFTWARD, SensorType.TOXICANT));
                c.sensor(SensorType.TOXICANT, Direction.RIGHTWARD).targets
                  .forEach(s -> processInput(s, Direction.RIGHTWARD, SensorType.TOXICANT));
                postInput();
            }
            {
                preOutput();
                processOutput(c.motor(Direction.FORWARD).preMotorNeuron, Direction.FORWARD);
                processOutput(c.motor(Direction.BACKWARD).preMotorNeuron, Direction.BACKWARD);
                processOutput(c.motor(Direction.LEFTWARD).preMotorNeuron, Direction.LEFTWARD);
                processOutput(c.motor(Direction.RIGHTWARD).preMotorNeuron, Direction.RIGHTWARD);
                postOutput();
            }
            {
                preBody();
                processBody(c.motor());
                processBody(c.sensor(SensorType.FOOD));
                processBody(c.sensor(SensorType.TOXICANT));
                postBody();
            }
            postCommunication();
        }
        end();
    }

    /**
     * print the header of the meta data
     */
    public void begin(){
    }

    public void end(){
    }

    /**
     * do nothing
     */
    public void preCircuit(){
    }

    /**
     * do nothing
     */
    public void postCircuit(){
    }

    /**
     * do nothing
     */
    public void preNeuron(){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#BLOCK_END_NEURON}
     */
    public void postNeuron(){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#BLOCK_BEGIN_RECEPTOR}
     */
    public void preReceptor(){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#BLOCK_END_RECEPTOR}
     */
    public void postReceptor(){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#BLOCK_BEGIN_COMMUNICATION}
     */
    public void pareCommunication(){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#BLOCK_END_COMMUNICATION}
     */
    public void postCommunication(){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#BLOCK_BEGIN_SENSOR}
     */
    public void preInput(){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#BLOCK_END_SENSOR}
     */
    public void postInput(){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#BLOCK_BEGIN_MOTOR}
     */
    public void preOutput(){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#BLOCK_END_MOTOR}
     */
    public void postOutput(){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#BLOCK_BEGIN_BODY}
     */
    public void preBody(){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#BLOCK_END_BODY}
     */
    public void postBody(){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#TOTAL_NEURON_NUMBER}
     */
    public void processCircuit(Circuit c){
    }

    /**
     * print neuron parameters
     *
     * @param n
     */
    public void processNeuron(CentralNeuron n){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#CLOSE_NEURON}
     */
    public void postNeuronParameter(CentralNeuron n){
    }

    protected void processNeuronMetaLocation(CentralNeuron n){
        if (getPrinter().metaNeuronLocation){
            if (n.containMeta(CentralNeuron.META_X)
                && n.containMeta(CentralNeuron.META_Y)
                && n.containMeta(CentralNeuron.META_THETA)){
                metaf(CentralNeuron.META_X, "%d", n.getIntMeta(CentralNeuron.META_X));
                metaf(CentralNeuron.META_Y, "%d", n.getIntMeta(CentralNeuron.META_Y));
                metaf(CentralNeuron.META_THETA, "%.0f", Math.toDegrees(n.getFloatMeta(CentralNeuron.META_THETA)));
            }
        }
    }

    /**
     * print receptor parameters
     *
     * @param r
     */
    public void processReceptor(Receptor r){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#CLOSE_RECEPTOR}
     */
    public void postReceptorParameter(Receptor r){
    }

    /**
     * print synapse parameters
     *
     * @param s
     */
    public void processSynapse(Synapse s){
    }

    /**
     * print keyword {@link CircuitLoaderV0102#CLOSE_SYNAPSE}
     */
    public void postSynapseParameter(Synapse s){
    }

    /**
     * print post-sensor synapse parameters
     */
    public void processInput(Synapse c, Direction dir, SensorType type){
    }

    /**
     * print pre-motor neuron id
     *
     * @param preMotor
     * @param dir
     */
    public void processOutput(CentralNeuron preMotor, Direction dir){
    }

    /**
     * print motor parameters
     *
     * @param n
     */
    public void processBody(Motor n){
    }

    /**
     * print sensor parameters
     *
     * @param n
     */
    public void processBody(Sensor n){
    }

}
