/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.util.List;

/**
 * worn circuit network.
 *
 * @author antonio
 */
public interface Circuit{


    /**
     * @return the number of the neuron in circuit, except body neurons.
     */
    int neuronCount();

    /**
     * Get the neuron at index {@code id}. In common case, index {@code id} also mean neuron ID {@code id}.
     *
     * @param id the id of the neuron
     * @return neuron
     * @throws IndexOutOfBoundsException if not found
     */
    CentralNeuron getNeuron(int id);

    /**
     * central neurons
     *
     * @return
     */
    List<CentralNeuron> neurons();

    /**
     * @param host the host neuron id
     * @param id   the receptor id
     * @return correspond receptor, null if not found
     * @throws IndexOutOfBoundsException if host neuron not found
     */
    Receptor getReceptor(int host, int id);

    /**
     * all receptor.
     *
     * @return
     */
    List<Receptor> receptors();

    /**
     * @param pre  pre-synapse neuron id
     * @param post post-synapse neuron id
     * @return correspond synapse, null if not found
     * @throws IndexOutOfBoundsException if pre-synapse neuron not found
     */
    Synapse getSynapse(int pre, int post);

    /**
     * all synapses
     *
     * @return
     */
    List<Synapse> synapses();

    /**
     * value-reference sensor
     *
     * @param type sensor type
     * @return
     */
    Sensor sensor(SensorType type);

    /**
     * connection reference sensor
     *
     * @param type sensor type
     * @param dir  direction
     * @return
     */
    Sensor sensor(SensorType type, Direction dir);

    /**
     * value-reference motor
     *
     * @return
     */
    Motor motor();

    /**
     * connection-reference motor
     *
     * @param dir direction
     * @return
     */
    Motor motor(Direction dir);

}
