/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cclo.hanitu.circuit.Direction.SILENCE;

/**
 * @author antonio
 */
public class ReadOnlyCircuit implements Circuit{

    public static final ReadOnlyCircuit EMPTY;

    static{
        Sensor fs = new Sensor(SensorType.FOOD);
        Sensor ts = new Sensor(SensorType.TOXICANT);
        Motor m = new Motor();
        EMPTY = new ReadOnlyCircuit(new CentralNeuron[0],
                                    new Sensor[]{fs, fs, fs, fs, fs},
                                    new Sensor[]{ts, ts, ts, ts, ts},
                                    new Motor[]{m, m, m, m, m});
    }

    /**
     * central neurons
     */
    private final CentralNeuron[] neurons;
    /**
     * food sensor neurons
     */
    private final Sensor[] foodSensor = new Sensor[5];
    /**
     * toxicant sensor neurons
     */
    private final Sensor[] toxicantSensor = new Sensor[5];
    /**
     * motor neurons
     */
    private final Motor[] motor = new Motor[5];

    /**
     * create a circuit
     *
     * **Notice** : for body neuron (sensor and motor), the size of the array should be 5. 0-index represent
     * value-reference sensor, and 1-index to 4-index represent connection-reference sensor.
     *
     * @param neurons        central neurons
     * @param foodSensor     food sensor neurons
     * @param toxicantSensor toxicant sensor neuron
     * @param motor          motor neuron
     */
    public ReadOnlyCircuit(CentralNeuron[] neurons, Sensor[] foodSensor, Sensor[] toxicantSensor, Motor[] motor){
        this.neurons = Arrays.copyOf(neurons, neurons.length);
        Arrays.sort(neurons, Comparator.comparingInt(n -> n.id));
        System.arraycopy(foodSensor, 0, this.foodSensor, 0, 5);
        System.arraycopy(toxicantSensor, 0, this.toxicantSensor, 0, 5);
        System.arraycopy(motor, 0, this.motor, 0, 5);
    }

    @Override
    public int neuronCount(){
        return neurons.length;
    }

    @Override
    public CentralNeuron getNeuron(int id){
        for (CentralNeuron n : neurons){
            if (n.id == id) return n;
        }
        return null;
    }

    @Override
    public List<CentralNeuron> neurons(){
        return Arrays.asList(neurons);
    }

    @Override
    public Receptor getReceptor(int host, int id){
        try {
            for (Receptor r : getNeuron(host).receptors){
                if (r.id == id) return r;
            }
            return null;
        } catch (NullPointerException e){
            throw new IllegalArgumentException("neuron id : " + host);
        }
    }

    @Override
    public List<Receptor> receptors(){
        return Stream.of(neurons).flatMap(n -> n.receptors.stream()).collect(Collectors.toList());
    }

    @Override
    public Synapse getSynapse(int pre, int post){
        try {
            for (Synapse s : getNeuron(pre).targets){
                if (s.postSynapseNeuron.id == post) return s;
            }
            return null;
        } catch (NullPointerException e){
            throw new IllegalArgumentException("pre-synapse neuron id : " + pre);
        }
    }

    @Override
    public List<Synapse> synapses(){
        return Stream.of(neurons).flatMap(n -> n.targets.stream()).collect(Collectors.toList());
    }

    @Override
    public Sensor sensor(SensorType type){
        switch (type){
        case FOOD:
            return foodSensor[0];
        case TOXICANT:
            return toxicantSensor[0];
        default:
            throw new IllegalArgumentException("type not support : " + type);
        }
    }

    @Override
    public Sensor sensor(SensorType type, Direction dir){
        if (dir == SILENCE) throw new IllegalArgumentException();
        switch (type){
        case FOOD:
            return foodSensor[dir.ordinal()];
        case TOXICANT:
            return toxicantSensor[dir.ordinal()];
        default:
            throw new IllegalArgumentException("type not support : " + type);
        }
    }

    @Override
    public Motor motor(){
        return motor[0];
    }

    @Override
    public Motor motor(Direction dir){
        if (dir == SILENCE) throw new IllegalArgumentException();
        return motor[dir.ordinal()];
    }
}
