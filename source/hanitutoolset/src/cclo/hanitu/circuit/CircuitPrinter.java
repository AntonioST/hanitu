/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.io.PrintStream;
import java.util.Map;

import cclo.hanitu.FileLoader;
import cclo.hanitu.FilePrinter;
import cclo.hanitu.FilePrinterImpl;
import cclo.hanitu.circuit.view.NeuronViewer;

import static cclo.hanitu.FileLoader.*;

/**
 * print the text content of the circuit to the output stream
 *
 * @author antonio
 */
public class CircuitPrinter extends FilePrinter<Circuit>{

    boolean humanReadable;
    boolean metaNeuronLocation = true;

    @Override
    public FilePrinterImpl<Circuit> getImplWithTargetVersion(String version){
         if (FileLoader.isVersionCompatibility(version, ">=1.2")){
            return new CircuitPrinterV0102();
        } else if (FileLoader.isVersionCompatibility(version, ">=1")){
            return new CircuitPrinterV0100();
        }
        return null;
    }

    @Override
    public FilePrinterImpl<Circuit> getImplForLatestVersion(){
        return new CircuitPrinterV0102();
    }

    @Override
    protected void startWriting(PrintStream ps){
        meta(META_FILE_HEADER, CircuitLoader.CIRCUIT_CONFIG_FILE_TYPE);
        meta(META_VERSION, getTargetVersionExpression());
        meta(META_CREATE_TIME, Long.toString(System.currentTimeMillis()));
        globalMeta();
        line();
    }

    public boolean isHumanReadable(){
        return humanReadable;
    }

    public void setHumanReadable(boolean humanReadable){
        this.humanReadable = humanReadable;
    }

    public void setMetaNeuronLotaion(boolean v){
        metaNeuronLocation = v;
    }
}
