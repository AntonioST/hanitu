/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cclo.hanitu.circuit;

import java.util.Objects;

/**
 * Direction. Use for worm move and body neuron site.
 *
 * @author antonio
 */
public enum Direction{

    /**
     * no move
     */
    SILENCE,
    /**
     * upward
     */
    FORWARD,
    /**
     * downward
     */
    BACKWARD,
    /**
     * leftward
     */
    LEFTWARD,
    /**
     * rightward
     */
    RIGHTWARD;

    public static Direction opposite(Direction d){
        switch (Objects.requireNonNull(d)){
        case SILENCE:
            return SILENCE;
        case FORWARD:
            return BACKWARD;
        case BACKWARD:
            return FORWARD;
        case LEFTWARD:
            return RIGHTWARD;
        case RIGHTWARD:
            return LEFTWARD;
        }
        return null;
    }

    public static double getTheta(Direction d){
        switch (Objects.requireNonNull(d)){
        case SILENCE:
            return 0;
        case FORWARD:
            return -Math.PI / 2;
        case BACKWARD:
            return Math.PI / 2;
        case LEFTWARD:
            return Math.PI;
        case RIGHTWARD:
            return 0;
        }
        return 0;
    }
}
