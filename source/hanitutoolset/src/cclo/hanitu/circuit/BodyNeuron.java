/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import cclo.hanitu.HanituInfo.Description;
import cclo.hanitu.HanituInfo.VValueField;

/**
 * The neuron on the body, like sensor and motor, do the character of input/output of the circuit.
 * This type neuron have the information of the {@link Direction}. However, all of their parameters
 * are same, except the synapse connections.
 *
 * @author antonio
 */
public class BodyNeuron{

    /**
     * neuron with (biologic) default parameter value
     */
    public static final BodyNeuron NORMAL = new BodyNeuron(){
        {
            capacitance = 0.5;
            timeConstant = 25;
            reversalPotential = -70;
            resetPotential = -55;
            threshold = -50;
            refractoryPeriod = 20;
        }
    };
    /**
     * refractory period in 0.1 ms.
     */
    @Description(resource="gui", value="neuron.refractoryPeriod")
    @VValueField(resource="gui", value="neuron.refractoryPeriod.unit")
    public int refractoryPeriod;
    /**
     * capacitance in nF, also known as `C`
     */
    @Description(resource="gui", value="neuron.capacitance")
    @VValueField(resource="gui", value="neuron.capacitance.unit")
    public double capacitance;
    /**
     * time constant in ms
     */
    @Description(resource="gui", value="neuron.timeconstant")
    @VValueField(resource="gui", value="neuron.timeconstant.unit")
    public double timeConstant;
    /**
     * the reversal potential in mV, also known as NRevP.
     */
    @Description(resource="gui", value="neuron.reversalpotential")
    @VValueField(resource="gui", value="neuron.reversalpotential.unit")
    public double reversalPotential;
    /**
     * unit mV
     */
    @Description(resource="gui", value="neuron.resetpotential")
    @VValueField(resource="gui", value="neuron.resetpotential.unit")
    public double resetPotential;
    /**
     * the spike threshold in mV.
     */
    @Description(resource="gui", value="neuron.threshold")
    @VValueField(resource="gui", value="neuron.threshold.unit")
    public double threshold;
    /**
     * body synaptic weight.
     */
    @Description(resource="gui", value="neuron.weight")
    @VValueField
    public double weight;

    /**
     * create a neuron with (program) default value
     */
    BodyNeuron(){
    }

    /**
     * create a neuron with value copy from reference
     *
     * @param neuron reference neuron
     */
    public BodyNeuron(BodyNeuron neuron){
        if (neuron != null){
            refractoryPeriod = neuron.refractoryPeriod;
            capacitance = neuron.capacitance;
            timeConstant = neuron.timeConstant;
            reversalPotential = neuron.reversalPotential;
            resetPotential = neuron.resetPotential;
            threshold = neuron.threshold;
            weight = neuron.weight;
        }
    }

    /**
     * copy value from reference
     *
     * @param n reference neuron
     */
    public void set(BodyNeuron n){
        if (n != null){
            refractoryPeriod = n.refractoryPeriod;
            capacitance = n.capacitance;
            timeConstant = n.timeConstant;
            reversalPotential = n.reversalPotential;
            resetPotential = n.resetPotential;
            threshold = n.threshold;
            weight = n.weight;
        }
    }
}
