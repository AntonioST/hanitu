/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cclo.hanitu.circuit;

import cclo.hanitu.HanituInfo.VValueField;

import java.util.Objects;

import static cclo.hanitu.HanituInfo.*;
import static cclo.hanitu.HanituInfo.Description;

/**
 * receptor.
 *
 * @author antonio
 */
public class Receptor extends Identify{

    /**
     * (biologic) default parameter for excitatory type receptor.
     */
    public static final Receptor EXCITATORY = new Receptor(){
        {
            type = ReceptorType.ACH;
            timeConstant = 20;
            reversalPotential = 0;
        }
    };

    /**
     * (biologic) default parameter for inhibitory type receptor.
     */
    public static final Receptor INHIBITORY = new Receptor(){
        {
            type = ReceptorType.GABA;
            timeConstant = 20;
            reversalPotential = -70;
        }
    };

    /**
     * the neuron owner.
     */
    public CentralNeuron neuron;
    /**
     * XXX type in circuit file mean math model, not bio type
     */
    public ReceptorType type;
    /**
     * the time constant in ms, also known as `tau`
     */
    @Description(resource="gui", value="receptor.timeconstant")
    @VValueField(resource="gui", value="receptor.timeconstant.unit")
    public double timeConstant;
    /**
     * the reversal potential in mV, also known as `RRevP`
     */
    @Description(resource="gui", value="receptor.reversalpotential")
    @VValueField(resource="gui", value="receptor.reversalpotential.unit")
    public double reversalPotential;

    /**
     * create a receptor with (program) default value
     */
    public Receptor(){
    }

    /**
     * create a receptor with copy value from reference.
     *
     * @param host host neuron
     * @param r    reference receptor
     */
    public Receptor(CentralNeuron host, Receptor r){
        neuron = host;
        if (r != null){
            timeConstant = r.timeConstant;
            reversalPotential = r.reversalPotential;
        }
        if (host != null){
            id = host.receptors.size();
            host.receptors.add(this);
        }
    }

    /**
     * copy value from reference
     *
     * @param r reference receptor
     */
    public void set(Receptor r){
        if (r != null){
            timeConstant = r.timeConstant;
            reversalPotential = r.reversalPotential;
        }
    }

    @VAction("Excitatory default")
    public void resetDefaultExcitatory(){
        set(EXCITATORY);
    }

    @VAction("Inhibitory default")
    public void resetDefaultInhibitory(){
        set(INHIBITORY);
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Receptor receptor = (Receptor)o;

        if (id != receptor.id) return false;
        if (neuron != null? !neuron.equals(receptor.neuron): receptor.neuron != null) return false;

        return true;
    }

    @Override
    public int hashCode(){
        int result = id;
        result = 31 * result + (neuron != null? neuron.hashCode(): 0);
        return result;
    }

    @Override
    public String toString(){
        return "Receptor[" + id + "<" + Objects.toString(neuron, "?") + "]";
    }
}
