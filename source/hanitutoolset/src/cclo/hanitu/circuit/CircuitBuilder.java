/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static cclo.hanitu.circuit.Direction.*;
import static cclo.hanitu.circuit.SensorType.FOOD;
import static cclo.hanitu.circuit.SensorType.TOXICANT;

/**
 * the builder of the circuit.
 *
 * @author antonio
 */
public class CircuitBuilder implements Circuit{

    private final List<CentralNeuron> neurons = new ArrayList<>();
    private final Sensor[] foodSensor = new Sensor[5];
    private final Sensor[] toxicantSensor = new Sensor[5];
    private final Motor[] motor = new Motor[5];

    public CircuitBuilder(){
        for (int i = 0; i < 5; i++){
            foodSensor[i] = new Sensor(FOOD);
            toxicantSensor[i] = new Sensor(TOXICANT);
            motor[i] = new Motor();
        }
        foodSensor[0].set(BodyNeuron.NORMAL);
        toxicantSensor[0].set(BodyNeuron.NORMAL);
        motor[0].set(BodyNeuron.NORMAL);
    }

    public CircuitBuilder(Circuit c){
        //central neuron
        for (CentralNeuron n : c.neurons()){
            //create neuron
            CentralNeuron t = new CentralNeuron(n.id, n);
            t.setMeta(n);
            neurons.add(t);
            //create receptor
            n.receptors.forEach(r -> new Receptor(t, r).id = r.id);
            paddingNull(t.receptors);
        }
        paddingNull(neurons);
        //create synapse
        for (CentralNeuron n : c.neurons()){
            for (Synapse s : n.targets){
                CentralNeuron pre = neurons.get(s.preSynapseNeuron.id);
                CentralNeuron post = neurons.get(s.postSynapseNeuron.id);
                Receptor rep = post.receptors.get(s.targetReceptor.id);
                new Synapse(pre, post, rep, s);
            }
        }
        //body neuron
        foodSensor[0] = new Sensor(c.sensor(FOOD));
        toxicantSensor[0] = new Sensor(c.sensor(TOXICANT));
        motor[0] = new Motor(c.motor());
        for (int i = 1; i < 5; i++){
            Direction d = Direction.values()[i];
            Sensor cfs = c.sensor(FOOD, d);
            Sensor ccs = c.sensor(TOXICANT, d);
            Motor cm = c.motor(d);
            foodSensor[i] = new Sensor(cfs);
            toxicantSensor[i] = new Sensor(ccs);
            motor[i] = new Motor(cm);
            CentralNeuron n;
            Receptor r;
            Synapse t;
            for (Synapse s : cfs.targets){
                n = neurons.get(s.postSynapseNeuron.id);
                r = n.receptors.get(s.targetReceptor.id);
                t = new Synapse(null, n, r, s);
                foodSensor[i].targets.add(t);
            }
            for (Synapse s : ccs.targets){
                n = neurons.get(s.postSynapseNeuron.id);
                r = n.receptors.get(s.targetReceptor.id);
                t = new Synapse(null, n, r, s);
                toxicantSensor[i].targets.add(t);
            }
            if (cm.preMotorNeuron != null){
                motor[i].preMotorNeuron = neurons.get(cm.preMotorNeuron.id);
            }
        }
        reduceListAndReassignID(neurons);
        neurons.forEach(n -> reduceListAndReassignID(n.receptors));
    }

    static void paddingNull(List<? extends Identify> list){
        list.removeIf(item -> item == null);
        int size = list.size();
        if (size == 0) return;
        Collections.sort(list);
        int max = list.get(size - 1).id;
        for (int i = 0; i < max; i++){
            int id = list.get(i).id;
            if (id > i){
                list.add(i, null);
            } else if (id < i){
                throw new RuntimeException("duplicate id in list");
            }
        }
    }

    static void reduceListAndReassignID(List<? extends Identify> list){
        list.removeIf(item -> item == null);
        int size = list.size();
        if (size == 0) return;
        Collections.sort(list);
        for (int i = 0; i < size; i++){
            list.get(i).id = i;
        }
    }

    /**
     * @return the number of the neuron in circuit, except body neurons.
     */
    @Override
    public int neuronCount(){
        return neurons.size();
    }

    /**
     * create a new neuron with initial parameters except id.
     * id may correspond the neuron count.
     *
     * @return new neuron.
     */
    public CentralNeuron createNeuron(){
        CentralNeuron n = new CentralNeuron(neurons.size(), null);
        neurons.add(n);
        return n;
    }

    /**
     * Get the neuron at index {@code id}. In common case, index {@code id} also mean neuron ID {@code id}.
     *
     * @param id the id of the neuron
     * @return neuron
     * @throws IndexOutOfBoundsException if not found
     */
    @Override
    public CentralNeuron getNeuron(int id){
        return neurons.get(id);
    }

    /**
     * Remove neuron.
     * all neuron will re-assign id number.
     *
     * @param id the id of the neuron
     * @return correspond neuron
     * @throws IndexOutOfBoundsException if not found
     */
    public CentralNeuron removeNeuron(int id){
        CentralNeuron n;
        try {
            n = neurons.set(id, null);
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("neuron id : " + id);
        }
        //remove synapse which is central neuron and target to this neuron
        neurons.stream()
          .filter(neu -> neu != null)
          .forEach(c -> c.targets.removeIf(s -> s.postSynapseNeuron.id == id));
        //test and try remove synapse which is body neuron and target to this neuron
        removeSensorSynapse(FOOD, FORWARD, id);
        removeSensorSynapse(FOOD, BACKWARD, id);
        removeSensorSynapse(FOOD, LEFTWARD, id);
        removeSensorSynapse(FOOD, RIGHTWARD, id);
        removeSensorSynapse(TOXICANT, FORWARD, id);
        removeSensorSynapse(TOXICANT, BACKWARD, id);
        removeSensorSynapse(TOXICANT, LEFTWARD, id);
        removeSensorSynapse(TOXICANT, RIGHTWARD, id);
        for (int i = 0; i < 4; i++){
            if (motor[i].preMotorNeuron != null && motor[i].preMotorNeuron.id == id){
                motor[i].preMotorNeuron = null;
            }
        }
        //refresh Neuron ID
        reduceListAndReassignID(neurons);
        n.targets.clear();
        return n;
    }

    /**
     * @param n the neuron will delete from the circuit.
     * @return true when neuron is deleted
     * @see #removeNeuron(int)
     */
    public boolean removeNeuron(CentralNeuron n){
        return removeNeuron(n.id) != null;
    }

    @Override
    public List<CentralNeuron> neurons(){
        return Collections.unmodifiableList(neurons);
    }

    /**
     * create a new synapse
     *
     * @param pre  pre-synapse neuron id
     * @param post post-synapse neuron id
     * @param rep  the id of the receptor of the post-synapse neuron
     * @return a new synapse
     * @throws IndexOutOfBoundsException
     */
    public Synapse createSynapse(int pre, int post, int rep){
        CentralNeuron f, t;
        Receptor r;
        try {
            f = neurons.get(pre);
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("pre-synapse neuron id : " + pre);
        }
        try {
            t = neurons.get(post);
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("post-synapse neuron id : " + post);
        }
        try {
            r = t.receptors.get(rep);
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("post-synapse receptor id : " + rep);
        }
        return new Synapse(f, t, r, null);
    }

    /**
     * create a new synapse
     *
     * @param pre pre-synapse neuron
     * @param rep the receptor of the post-synapse neuron
     * @return a new synapse
     * @throws IllegalArgumentException if {@code rep} not a receptor of {@code post}
     * @see #createSynapse(int, int, int)
     */
    public Synapse createSynapse(CentralNeuron pre, Receptor rep){
        if (rep.neuron == null)
            throw new IllegalArgumentException("receptor not belong to any neuron");
        return createSynapse(pre.id, rep.neuron.id, rep.id);
    }

    public Synapse createSensorSynapse(SensorType ty, Direction dir, Receptor rep){
        return createSensorSynapse(ty, dir, rep.neuron.id, rep.id);
    }

    /**
     * create a new synapse from sensor to a central neuron.
     *
     * @param ty   sensor type
     * @param dir  sensor direction
     * @param post post-synapse neuron id
     * @param rep  the id of the receptor of the post-synapse neuron
     * @return a new synapse
     */
    public Synapse createSensorSynapse(SensorType ty, Direction dir, int post, int rep){
        Sensor f = sensor(ty, dir);
        CentralNeuron t;
        Synapse s = new Synapse();
        try {
            s.postSynapseNeuron = t = neurons.get(post);
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("post-synapse neuron id : " + post);
        }
        try {
            s.targetReceptor = t.receptors.get(rep);
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("post-synapse receptor id : " + rep);
        }
        f.targets.add(s);
        return s;
    }

    public void setPreMotorNeuron(CentralNeuron pre, Direction dir){
        setPreMotorNeuron(pre.id, dir);
    }

    /**
     * set synapse from a central to motor neuron.
     * <P>
     * motor synapse doesn't need to create.
     *
     * @param pre pre-motor neuron id
     * @param dir motor direction
     */
    public void setPreMotorNeuron(int pre, Direction dir){
        if (dir == SILENCE) throw new IllegalArgumentException();
        try {
            motor(dir).preMotorNeuron = neurons.get(pre);
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("pre-synapse neuron id : " + pre);
        }
    }

    /**
     * @param pre  pre-synapse neuron id
     * @param post post-synapse neuron id
     * @return correspond synapse, null if not found
     * @throws IndexOutOfBoundsException if pre-synapse neuron not found
     */
    @Override
    public Synapse getSynapse(int pre, int post){
        try {
            for (Synapse s : neurons.get(pre).targets){
                if (s.postSynapseNeuron.id == post) return s;
            }
            return null;
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("pre-synapse neuron id : " + pre);
        }
    }

    /**
     * @param pre  pre-synapse neuron
     * @param post post-synapse neuron
     * @return correspond synapse, null if not found
     * @see #getSynapse(int, int)
     */
    public Synapse getSynapse(CentralNeuron pre, CentralNeuron post){
        return getSynapse(pre.id, post.id);
    }

    /**
     * @param ty   sensor type
     * @param dir  sensor direction
     * @param post post-sensor neuron id
     * @return synapse. null if not found.
     */
    public Synapse getSensorSynapse(SensorType ty, Direction dir, int post){
        return sensor(ty, dir).targets.stream().filter(n -> n.postSynapseNeuron.id == post).findFirst().orElse(null);
    }

    /**
     * @param ty  sensor type
     * @param dir sensor direction
     * @return a list of corresponding sensor synapses
     */
    public List<Synapse> getSensorSynapses(SensorType ty, Direction dir){
        return Collections.unmodifiableList(sensor(ty, dir).targets);
    }

    public CentralNeuron getPreMotorNeuron(Direction dir){
        return motor(dir).preMotorNeuron;
    }

    /**
     * @param pre  pre-synapse neuron id
     * @param post post-synapse neuron id
     * @return correspond synapse, null if not found.
     * @throws IndexOutOfBoundsException if pre-synapse neuron not found
     */
    public Synapse removeSynapse(int pre, int post){
        CentralNeuron pren;
        try {
            pren = neurons.get(pre);
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("pre-synapse neuron id : " + pre);
        }
        Iterator<Synapse> it = pren.targets.iterator();
        Synapse s;
        while (it.hasNext()){
            if ((s = it.next()).postSynapseNeuron.id == post){
                it.remove();
                return s;
            }
        }
        return null;
    }


    /**
     * @param ty   sensor type
     * @param dir  sensor direction
     * @param post post-sensor neuron id
     * @return deleted synapse, null if not found
     */
    public Synapse removeSensorSynapse(SensorType ty, Direction dir, int post){
        Iterator<Synapse> it = sensor(ty, dir).targets.iterator();
        Synapse s;
        while (it.hasNext()){
            if ((s = it.next()).postSynapseNeuron.id == post){
                it.remove();
                return s;
            }
        }
        return null;
    }

    /**
     * @param dir motor direction
     */
    public void removeMotorSynapse(Direction dir){
        motor(dir).preMotorNeuron = null;
    }

    /**
     * @param s the synapse will delete from the circuit
     * @return true if delete
     * @see #removeSynapse(int, int)
     * @see #removeSensorSynapse(SensorType, Direction, int)
     */
    public boolean removeSynapse(Synapse s){
        CentralNeuron pre = s.preSynapseNeuron;
        CentralNeuron post = s.postSynapseNeuron;
        if (post == null){
        } else if (pre != null){
            return removeSynapse(pre.id, post.id) != null;
        } else if (sensor(FOOD, FORWARD).targets.contains(s)){
            return removeSensorSynapse(FOOD, FORWARD, post.id) != null;
        } else if (sensor(FOOD, BACKWARD).targets.contains(s)){
            return removeSensorSynapse(FOOD, BACKWARD, post.id) != null;
        } else if (sensor(FOOD, LEFTWARD).targets.contains(s)){
            return removeSensorSynapse(FOOD, LEFTWARD, post.id) != null;
        } else if (sensor(FOOD, RIGHTWARD).targets.contains(s)){
            return removeSensorSynapse(FOOD, RIGHTWARD, post.id) != null;
        } else if (sensor(TOXICANT, FORWARD).targets.contains(s)){
            return removeSensorSynapse(TOXICANT, FORWARD, post.id) != null;
        } else if (sensor(TOXICANT, BACKWARD).targets.contains(s)){
            return removeSensorSynapse(TOXICANT, BACKWARD, post.id) != null;
        } else if (sensor(TOXICANT, LEFTWARD).targets.contains(s)){
            return removeSensorSynapse(TOXICANT, LEFTWARD, post.id) != null;
        } else if (sensor(TOXICANT, RIGHTWARD).targets.contains(s)){
            return removeSensorSynapse(TOXICANT, RIGHTWARD, post.id) != null;
        }
        return false;
    }

    @Override
    public List<Synapse> synapses(){
        List<Synapse> ret = new ArrayList<>();
        neurons.stream().flatMap(n -> n.targets.stream()).forEach(ret::add);
        Stream.of(foodSensor).flatMap(f -> f.targets.stream()).forEach(ret::add);
        Stream.of(toxicantSensor).flatMap(f -> f.targets.stream()).forEach(ret::add);
        return ret;
    }

    /**
     * create a new receptor with initial parameters except id.
     *
     * @param host the host neuron id
     * @return new receptor
     */
    public Receptor createReceptor(int host){
        CentralNeuron n;
        try {
            n = neurons.get(host);
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("neuron id : " + host);
        }
        return new Receptor(n, null);
    }

    /**
     * create a new receptor with initial parameters except id.
     *
     * @param host the host neuron
     * @return new receptor
     * @see #createReceptor(int)
     */
    public Receptor createReceptor(CentralNeuron host){
        return createReceptor(host.id);
    }

    /**
     * @param host the host neuron id
     * @param id   the receptor id
     * @return correspond receptor, null if not found
     * @throws IndexOutOfBoundsException if host neuron not found
     */
    @Override
    public Receptor getReceptor(int host, int id){
        try {
            for (Receptor r : neurons.get(host).receptors){
                if (r.id == id) return r;
            }
            return null;
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("neuron id : " + host);
        }
    }

    /**
     * @param host the host neuron
     * @param id   the receptor id
     * @return correspond receptor, null if not found
     * @see #getReceptor(int, int)
     */
    public Receptor getReceptor(CentralNeuron host, int id){
        return getReceptor(host.id, id);
    }

    /**
     * @param host the host neuron id
     * @param id   the receptor id
     * @return correspond receptor
     * @throws IndexOutOfBoundsException
     */
    public Receptor removeReceptor(int host, int id){
        CentralNeuron n;
        try {
            n = neurons.get(host);
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("neuron id : " + host);
        }
        //remove synapse which target to this receptor
        neurons.stream()
          .forEach(c -> c.targets.removeIf(s -> s.postSynapseNeuron.id == host && s.targetReceptor.id == id));
        Receptor r = n.receptors.remove(id);
        if (r.id != id){
            n.receptors.add(r);
            paddingNull(n.receptors);
            r = n.receptors.remove(id);
            assert r.id == id;
        }
        reduceListAndReassignID(n.receptors);
        return r;
    }

    /**
     * @param r the receptor will delete from the circuit
     * @return true if delete
     */
    public boolean removeReceptor(Receptor r){
        CentralNeuron n = r.neuron;
        return n != null && removeReceptor(n.id, r.id) != null;
    }


    @Override
    public List<Receptor> receptors(){
        List<Receptor> ret = new ArrayList<>();
        neurons.stream().flatMap(n -> n.receptors.stream()).forEach(ret::add);
        Stream.of(motor).map(m -> m.receptor).forEach(ret::add);
        return ret;
    }

    @Override
    public Sensor sensor(SensorType type){
        switch (type){
        case FOOD:
            return foodSensor[0];
        case TOXICANT:
            return toxicantSensor[0];
        default:
            throw new IllegalArgumentException("type not support : " + type);
        }
    }

    /**
     * @param type Sensor Type
     * @return correspond sensor
     */
    @Override
    public Sensor sensor(SensorType type, Direction dir){
        if (dir == SILENCE) throw new IllegalArgumentException();
        switch (type){
        case FOOD:
            return foodSensor[dir.ordinal()];
        case TOXICANT:
            return toxicantSensor[dir.ordinal()];
        default:
            throw new IllegalArgumentException("type not support : " + type);
        }
    }

    @Override
    public Motor motor(){
        return motor[0];
    }

    /**
     * @return correspond motor
     */
    @Override
    public Motor motor(Direction dir){
        if (dir == SILENCE) throw new IllegalArgumentException();
        return motor[dir.ordinal()];
    }

    /**
     * create a new circuit
     *
     * @return new circuit
     */
    public Circuit createCircuit(){
        return new ReadOnlyCircuit(neurons.toArray(new CentralNeuron[neurons.size()]),
                                   foodSensor, toxicantSensor, motor);
    }

    public void clear(){
        neurons.clear();
        for (int i = 0; i < 5; i++){
            foodSensor[i] = new Sensor(FOOD);
            toxicantSensor[i] = new Sensor(TOXICANT);
            motor[i] = new Motor();
        }
    }
}
