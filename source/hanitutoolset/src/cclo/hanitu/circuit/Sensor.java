/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author antonio
 */
public class Sensor extends BodyNeuron{

    /**
     * the type of the sensor.
     */
    public final SensorType type;

    /**
     * the synapse on the axon of the sensor.
     */
    public final List<Synapse> targets = new ArrayList<>();

    public Sensor(SensorType type){
        this.type = type;
    }

    public Sensor(Sensor s){
        super(s);
        this.type = s.type;
    }

    @Override
    public String toString(){
        return "Sensor[" + type.name() + ">"
               + targets.stream()
          .map(s -> s.postSynapseNeuron)
          .map(n -> Integer.toString(n.id))
          .collect(Collectors.joining(","))
               + "]";
    }
}
