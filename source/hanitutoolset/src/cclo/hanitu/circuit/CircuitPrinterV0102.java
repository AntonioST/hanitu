/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import static cclo.hanitu.circuit.CircuitLoaderV0102.*;

/**
 * print the text content of the circuit to the output stream
 *
 * @author antonio
 */
public class CircuitPrinterV0102 extends CircuitPrinterImpl{

    @Override
    public String getTargetVersion(){
        return "1.2";
    }

    @Override
    public void postNeuron(){
        line(BLOCK_END_NEURON);
        line();
    }

    @Override
    public void preReceptor(){
        line(BLOCK_BEGIN_RECEPTOR);
    }

    @Override
    public void postReceptor(){
        line(BLOCK_END_RECEPTOR);
    }

    @Override
    public void pareCommunication(){
        line(BLOCK_BEGIN_COMMUNICATION);
        line();
    }

    @Override
    public void postCommunication(){
        line(BLOCK_END_COMMUNICATION);
    }

    @Override
    public void preInput(){
        line(BLOCK_BEGIN_SENSOR);
    }

    @Override
    public void postInput(){
        line(BLOCK_END_SENSOR);
        line();
    }

    @Override
    public void preOutput(){
        line(BLOCK_BEGIN_MOTOR);
    }

    @Override
    public void postOutput(){
        line(BLOCK_END_MOTOR);
    }

    @Override
    public void preBody(){
        line(BLOCK_BEGIN_BODY);
        line();
    }

    @Override
    public void postBody(){
        line(BLOCK_END_BODY);
    }

    @Override
    public void processCircuit(Circuit c){
        pair(TOTAL_NEURON_NUMBER, c.neurons().size());
    }

    @Override
    public void processNeuron(CentralNeuron n){
        pair(NEURON, n.id);
        pair(CAPACITANCE, n.capacitance);
        pair(CONDUCTANCE, n.conductance);
        pair(NEURON_REVERSAL_POTENTIAL, n.reversalPotential);
        pair(SPIKING_THRESHOLD, n.threshold);
        pair(NEURON_RESET_POTENTIAL, n.resetPotential);
        pair(REFRACTORY_PERIOD, n.refractoryPeriod);
        pair(SPIKE_DELAY, n.spikeDelay);
        line(BLOCK_BEGIN_NOISE);
        pair(NOISE_STD, n.noiseStd);
        pair(NOISE_MEAN, n.noiseMean);
        line(BLOCK_END_NOISE);
        processNeuronMetaLocation(n);

    }

    @Override
    public void postNeuronParameter(CentralNeuron n){
        line(CLOSE_NEURON);
        line();
    }

    @Override
    public void processReceptor(Receptor r){
        pair(RECEPTOR, r.id);
        pair(MODEL_TYPE, 0);
        pair(TIME_CONSTANT, r.timeConstant);
        pair(RECEPTOR_REVERSAL_POTENTIAL, r.reversalPotential);
    }

    @Override
    public void postReceptorParameter(Receptor r){
        line(CLOSE_RECEPTOR);
    }

    @Override
    public void processSynapse(Synapse s){
        pair(TARGET_NEURON, s.postSynapseNeuron.id);
        pair(RECEPTOR, s.targetReceptor.id);
        pair(CONNECTION_WEIGHT, s.weight);
        pair(CONDUCTANCE, s.conductance);
    }

    @Override
    public void postSynapseParameter(Synapse s){
        line(CLOSE_SYNAPSE);
        line();
    }

    @Override
    public void processInput(Synapse c, Direction dir, SensorType type){
        if (c.postSynapseNeuron != null){
            pair(BODY_TARGET_NEURON, c.postSynapseNeuron.id);
            pair(RECEPTOR, c.targetReceptor.id);
            pair(CONNECTION_WEIGHT, c.weight);
            pair(CONDUCTANCE, c.conductance);
            pair(MODEL_TYPE, type.ordinal());
            pair(BODY_DIRECTION, dir.ordinal() - 1);
        }
        line();
    }

    @Override
    public void processOutput(CentralNeuron preMotor, Direction dir){
        if (preMotor != null){
            pair(BODY_TARGET_NEURON, preMotor.id);
        } else {
            pair(BODY_TARGET_NEURON, -1);
        }
    }

    @Override
    public void processBody(Motor n){
        pair(MOTOR_CAPACITANCE, n.capacitance);
        pair(MOTOR_TIME_CONSTANT, n.timeConstant);
        pair(MOTOR_WEIGHT, n.weight);
        pair(MOTOR_SILENCE_PERIOD, n.refractoryPeriod);
        pair(MOTOR_SPIKING_THRESHOLD, n.threshold);
        pair(MOTOR_REVERSAL_POTENTIAL, n.reversalPotential);
        pair(MOTOR_RESET_POTENTIAL, n.resetPotential);
        line();
    }

    @Override
    public void processBody(Sensor n){
        if (n.type == SensorType.FOOD){
            pair(FOOD_SENSOR_CAPACITANCE, n.capacitance);
            pair(FOOD_SENSOR_TIME_CONSTANT, n.timeConstant);
            pair(FOOD_SENSOR_WEIGHT, n.weight);
            pair(FOOD_SENSOR_SILENCE_PERIOD, n.refractoryPeriod);
            pair(FOOD_SENSOR_SPIKING_THRESHOLD, n.threshold);
            pair(FOOD_SENSOR_REVERSAL_POTENTIAL, n.reversalPotential);
            pair(FOOD_SENSOR_RESET_POTENTIAL, n.resetPotential);
        } else if (n.type == SensorType.TOXICANT){
            pair(TOXICANT_SENSOR_CAPACITANCE, n.capacitance);
            pair(TOXICANT_SENSOR_TIME_CONSTANT, n.timeConstant);
            pair(TOXICANT_SENSOR_WEIGHT, n.weight);
            pair(TOXICANT_SENSOR_SILENCE_PERIOD, n.refractoryPeriod);
            pair(TOXICANT_SENSOR_SPIKING_THRESHOLD, n.threshold);
            pair(TOXICANT_SENSOR_REVERSAL_POTENTIAL, n.reversalPotential);
            pair(TOXICANT_SENSOR_RESET_POTENTIAL, n.resetPotential);
        } else {
            throw new IllegalArgumentException();
        }
        line();
    }

}
