/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

/**
 * synapse create after all neuron have created.
 *
 * {@link CircuitBuilder } cannot create a synapse to a un-created neuron or un-created receptor.
 * However all neuron will be create when parsing `Total_neuron_number`, empty neuron contain no receptors.
 * You cannot create a synapse from a neuron to another neuron without giving target receptor.
 */
class DummySynapse{

    /**
     * pre-synapse neuron ID
     */
    int pre;
    /**
     * post-synapse neuron ID
     */
    int post;
    /**
     * target receptor ID
     */
    int rep;
    /**
     * connection weight
     */
    double weight;
    /**
     * conductance
     */
    double conductance;
    /**
     * the line number in the source file where define the synapse
     */
    int lineNumber;
}
