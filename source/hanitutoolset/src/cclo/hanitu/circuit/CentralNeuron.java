/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cclo.hanitu.Meta;

import static cclo.hanitu.HanituInfo.*;

/**
 * Neuron, the master role in the circuit, contain several receptors and synapses.
 * Unlike {@link BodyNeuron}, this type neuron doesn't has the information of the {@link Direction}.
 *
 * @author antonio
 */
public class CentralNeuron extends Identify implements Meta{

    /**
     * neuron with default parameter value.
     */
    public static final CentralNeuron NORMAL = new CentralNeuron(){
        {
            capacitance = 0.5;
            conductance = 25;
            reversalPotential = -70;
            resetPotential = -55;
            threshold = -50;
            refractoryPeriod = 20;
            spikeDelay = 18;
            noiseStd = 0.0;
            noiseMean = 0.0;
        }
    };
    /**
     * refractory period in 0.1 ms.
     */
    @Description(resource = "gui", value = "neuron.refractoryPeriod")
    @VValueField(resource = "gui", value = "neuron.refractoryPeriod.unit")
    public int refractoryPeriod;
    /**
     * the delay after a spike in 0.1 ms
     */
    @Description(resource = "gui", value = "neuron.spikedelay")
    @VValueField(resource = "gui", value = "neuron.spikedelay.unit")
    public int spikeDelay;
    /**
     * conductance, also known as `G`
     */
    @Description(resource = "gui", value = "neuron.conductance")
    @VValueField(resource = "gui", value = "neuron.conductance.unit")
    public double conductance;
    /**
     * capacitance, also known as `C`
     */
    @Description(resource = "gui", value = "neuron.capacitance")
    @VValueField(resource = "gui", value = "neuron.capacitance.unit")
    public double capacitance;
    /**
     * the reversal potential in mV, also known as `NRevP`.
     */
    @Description(resource = "gui", value = "neuron.reversalpotential")
    @VValueField(resource = "gui", value = "neuron.reversalpotential.unit")
    public double reversalPotential;
    /**
     */
    @Description(resource = "gui", value = "neuron.resetpotential")
    @VValueField(resource = "gui", value = "neuron.resetpotential.unit")
    public double resetPotential;
    /**
     * the spike threshold.
     */
    @Description(resource = "gui", value = "neuron.threshold")
    @VValueField(resource = "gui", value = "neuron.threshold.unit")
    public double threshold;
    /**
     * the standard deviation of the noise in mV.
     */
    @Description(resource = "gui", value = "neuron.noisestd")
    @VValueField(resource = "gui", value = "neuron.noisestd.unit")
    public double noiseStd;
    /**
     * the mean of the membrane potential noise in mV.
     */
    @Description(resource = "gui", value = "neuron.noisemean")
    @VValueField(resource = "gui", value = "neuron.noisemean.unit")
    public double noiseMean;
    /**
     * the receptors on the neuron.
     */
    public final List<Synapse> targets = new ArrayList<>();
    /**
     * the synapse on the axon of the neuron.
     */
    public final List<Receptor> receptors = new ArrayList<>();

    public static final String META_X = "x";
    public static final String META_Y = "y";
    public static final String META_THETA = "t";
    protected Map<String, Object> meta;

    /**
     * create a neuron with (program) default value
     */
    private CentralNeuron(){
    }

    /**
     * create a neuron with value copy from reference
     *
     * @param id     identify number
     * @param neuron reference neuron
     */
    public CentralNeuron(int id, CentralNeuron neuron){
        this.id = id;
        if (neuron != null){
            refractoryPeriod = neuron.refractoryPeriod;
            spikeDelay = neuron.spikeDelay;
            conductance = neuron.conductance;
            capacitance = neuron.capacitance;
            reversalPotential = neuron.reversalPotential;
            resetPotential = neuron.resetPotential;
            threshold = neuron.threshold;
            noiseStd = neuron.noiseStd;
            noiseMean = neuron.noiseMean;
        }
    }

    /**
     * copy value from reference
     *
     * @param n reference neuron
     */
    public void set(CentralNeuron n){
        if (n != null){
            refractoryPeriod = n.refractoryPeriod;
            spikeDelay = n.spikeDelay;
            conductance = n.conductance;
            capacitance = n.capacitance;
            reversalPotential = n.reversalPotential;
            resetPotential = n.resetPotential;
            threshold = n.threshold;
            noiseStd = n.noiseStd;
            noiseMean = n.noiseMean;
        }
    }

    public void setMeta(CentralNeuron n){
        if(n != null && n.meta != null){
            if(meta == null){
                meta = new HashMap<>();
            }
            meta.put(META_X, n.meta.get(META_X));
            meta.put(META_Y, n.meta.get(META_Y));
            meta.put(META_THETA, n.meta.get(META_THETA));
        }
    }

    @VAction("reset to default")
    public void resetDefault(){
        set(NORMAL);
    }

    @Override
    public String toString(){
        return "Neuron[" + id + "]";
    }

    @Override
    public boolean containMeta(String key){
        return meta != null && meta.containsKey(key);
    }

    @Override
    public Object getMeta(String key){
        return meta == null? null: meta.get(key);
    }

    @Override
    public Object putMeta(String key, Object value){
        if (meta == null){
            meta = new HashMap<>();
        }
        return meta.put(key, value);
    }

    @Override
    public Object removeMeta(String key){
        return meta == null? null: meta.remove(key);
    }
}
