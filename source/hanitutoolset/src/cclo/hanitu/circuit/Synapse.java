/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cclo.hanitu.circuit;

import cclo.hanitu.HanituInfo.VValueField;

import java.util.Objects;

import static cclo.hanitu.HanituInfo.Description;

/**
 * synapse.
 *
 * @author antonio
 */
public class Synapse{

    /**
     * zero strength synapse.
     */
    public static final Synapse ZERO = new Synapse(){
        {
            conductance = 0;
            weight = 0;
        }
    };

    /**
     * one unit strength synapse.
     */
    public static final Synapse NORMAL = new Synapse(){
        {
            conductance = 1e-9;
            weight = 1.0;
        }
    };
    /**
     * pre-synapse neuron
     */
    public CentralNeuron preSynapseNeuron;

    /**
     * post-synapse neuron
     */
    public CentralNeuron postSynapseNeuron;

    /**
     * the receptor of the post-synapse neuron
     */
    public Receptor targetReceptor;

    /**
     * the weight of the connection.
     */
    @Description(resource="gui", value="synapse.weight")
    @VValueField
    public double weight;

    /**
     * conductance in nS, also known as `G`
     */
    @Description(resource="gui", value="synapse.conductance")
    @VValueField(resource="gui", value="synapse.conductance.unit")
    public double conductance;

    /**
     * create a synapse with (program) default value and empty reference target.
     */
    public Synapse(){
    }

    /**
     * create a synapse with copy value from reference.
     *
     * @param pre  pre-synapse neuron
     * @param post post-synapse neuron
     * @param rep  target-receptor
     * @param syn  reference synapse
     */
    public Synapse(CentralNeuron pre, CentralNeuron post, Receptor rep, Synapse syn){
        preSynapseNeuron = pre;
        postSynapseNeuron = post;
        targetReceptor = rep;
        if (syn != null){
            weight = syn.weight;
            conductance = syn.conductance;
        }
        if (post != null && rep != null){
            if (!post.equals(rep.neuron)){
                throw new IllegalArgumentException("post neuron " + post + "doesn't contain receptor : " + rep);
            }
        }
        if (pre != null){
            pre.targets.add(this);
        }
    }

    /**
     * copy value from reference
     *
     * @param syn reference synapse
     */
    public void set(Synapse syn){
        if (syn != null){
            weight = syn.weight;
            conductance = syn.conductance;
        }
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Synapse synapse = (Synapse)o;

        if (postSynapseNeuron != null? !postSynapseNeuron.equals(synapse.postSynapseNeuron)
                                     : synapse.postSynapseNeuron != null) return false;
        if (preSynapseNeuron != null? !preSynapseNeuron.equals(synapse.preSynapseNeuron)
                                    : synapse.preSynapseNeuron != null)
            return false;
        if (targetReceptor != null? !targetReceptor.equals(synapse.targetReceptor): synapse.targetReceptor != null)
            return false;

        return true;
    }

    @Override
    public int hashCode(){
        int result = preSynapseNeuron != null? preSynapseNeuron.hashCode(): 0;
        result = 31 * result + (postSynapseNeuron != null? postSynapseNeuron.hashCode(): 0);
        result = 31 * result + (targetReceptor != null? targetReceptor.hashCode(): 0);
        return result;
    }

    @Override
    public String toString(){
        return "Synapse[" + Objects.toString(preSynapseNeuron, "?")
               + ">>" + Objects.toString(targetReceptor, "?");
    }
}
