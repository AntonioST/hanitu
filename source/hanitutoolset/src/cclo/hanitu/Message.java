/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import cclo.hanitu.util.Strings;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author antonio
 */
public class Message{

    public static final String EXPRESSION_REGEX = "~?~(}|(?<S>.*?)\\{)";
    private static final Pattern EXPRESSION_PATTERN = Pattern.compile(EXPRESSION_REGEX);

    static boolean COLORLESS = Base.getBooleanProperty("message.colorless", false);
    private static final Properties property = Base.loadPropertyInherit(Base.getProperty("message", "message/default"));

    static{
        if (COLORLESS){
            for (Map.Entry<Object, Object> e : property.entrySet()){
                e.setValue(removeExpression((String)e.getValue()));
            }
        } else {
            for (Map.Entry<Object, Object> e : property.entrySet()){
                e.setValue(mapTermExpression((String)e.getValue()));
            }
        }
    }

    public static final int WEIGHT_LIGHT = 0;
    public static final int WEIGHT_THIN = 1;
    public static final int WEIGHT_NORMAL = 2;
    public static final int WEIGHT_BOLD = 3;
    public static final int WEIGHT_BLACK = 4;

    public static final int STYLE_NORMAL = 0;
    public static final int STYLE_ITALIC = 1;

    public final String text;
    public boolean normal = true;
    public String foregroundColor = "";
    public String backgroundColor = "";
    public String font = "";
    public int weight = WEIGHT_NORMAL;
    public int style = STYLE_NORMAL;
    public int size = -1;
    public boolean underLine;
    public boolean strikeThrough;

    private Message(String text, Message copy){
        this.text = text;
        if (copy != null){
            normal = copy.normal;
            foregroundColor = Objects.requireNonNull(copy.foregroundColor);
            backgroundColor = Objects.requireNonNull(copy.backgroundColor);
            font = Objects.requireNonNull(copy.font);
            weight = copy.weight;
            style = copy.style;
            size = copy.size;
            underLine = copy.underLine;
            strikeThrough = copy.strikeThrough;
        }
    }

    @Override
    public String toString(){
        if (normal){
            return text;
        } else {
            StringBuilder sb = new StringBuilder("~");
            if (!font.isEmpty()){
                sb.append("<").append(font).append(">");
            }
            if (size >= 0){
                sb.append(size);
            }
            if (weight != WEIGHT_NORMAL){
                if (weight == WEIGHT_LIGHT){
                    sb.append("!**");
                } else if (weight == WEIGHT_THIN){
                    sb.append("!*");
                } else if (weight == WEIGHT_BOLD){
                    sb.append("*");
                } else if (weight == WEIGHT_BLACK){
                    sb.append("**");
                }
            }
            if (style != STYLE_NORMAL){
                if (style == STYLE_ITALIC){
                    sb.append("/");
                }
            }
            if (!foregroundColor.isEmpty()){
                sb.append(foregroundColor.toLowerCase());
            }
            if (!backgroundColor.isEmpty()){
                sb.append(backgroundColor.toUpperCase());
            }
            if (underLine){
                sb.append("_");
            }
            if (strikeThrough){
                sb.append("-");
            }
            sb.append("{").append(text).append("~}");
            return sb.toString();
        }
    }

    public static List<Message> parse(String line){
        if (line == null) return Collections.EMPTY_LIST;
        if (line.isEmpty()) return Collections.singletonList(new Message("", null));
        List<Message> ret = new ArrayList<>();
        Matcher match = EXPRESSION_PATTERN.matcher(line);
        if (match.find()){
            int offset = 0;
            Message msg = new Message("", null);
            do {
                int start = match.start();
                if (start != offset){
                    ret.add(new Message(line.substring(offset, start), msg));
                }
                String group = match.group();
                if (group.startsWith("~~")){
                    ret.add(new Message(group.substring(1), msg));
                } else if (group.equals("~}")){
                    msg = new Message("", null);
                } else {
                    msg.format(match.group("S"));
                }
            } while (match.find(offset = match.end()));
            if (offset != line.length()){
                ret.add(new Message(line.substring(offset), msg));
            }
        } else {
            ret.add(new Message(line, null));
        }
        return ret;
    }

    private void format(String format){
        if (format.isEmpty()) return;
        normal = false;
        StringBuilder builder = new StringBuilder(format);
        String m;
        if ((m = Strings.delete("<(.*)>", builder)) != null){
            font = m;
        }
        if ((m = Strings.delete("\\d+", builder)) != null){
            size = Integer.parseInt(m);
        }
        if ((m = Strings.delete("!?[*]+", builder)) != null){
            boolean inverse = m.startsWith("!");
            switch (m.length() - (inverse? 1: 0)){
            case 1:
                weight = inverse? WEIGHT_LIGHT: WEIGHT_BOLD;
                break;
            default:
                weight = inverse? WEIGHT_THIN: WEIGHT_BLACK;
                break;
            }
        }
        if ((m = Strings.delete("[rgybmckw]", builder)) != null){
            foregroundColor = m;
        }
        if ((m = Strings.delete("[RGYBMCKW]", builder)) != null){
            backgroundColor = m.toLowerCase();
        }
        if (Strings.delete("/", builder) != null){
            style = STYLE_ITALIC;
        }
        if (Strings.delete("-", builder) != null){
            strikeThrough = true;
        }
        if (Strings.delete("_", builder) != null){
            underLine = true;
        }
        if (builder.length() != 0){
            throw new IllegalArgumentException("unknown rich text format : " + builder);
        }
    }

    public static String mapForegroundColor(String color){
        switch (color){
        case "":
        case "w":
        case "k":
            return "\033[1m";
        case "r":
            return "\033[1;31m";
        case "g":
            return "\033[1;32m";
        case "y":
            return "\033[1;33m";
        case "b":
            return "\033[1;34m";
        case "m":
            return "\033[1;35m";
        case "c":
            return "\033[1;36m";
        }
        return null;
    }

    public static String mapBackgroundColor(String color){
        switch (color){
        case "":
        case "w":
        case "k":
            return "\033[1m";
        case "r":
            return "\033[1;41m";
        case "g":
            return "\033[1;42m";
        case "y":
            return "\033[1;43m";
        case "b":
            return "\033[1;44m";
        case "m":
            return "\033[1;45m";
        case "c":
            return "\033[1;46m";
        }
        return null;
    }

    public static String mapTermExpression(String line){
        return parse(line).stream().map(message -> {
            if (message.normal) return message.text;
            List<String> ls = new LinkedList<>();
            if (message.weight > WEIGHT_NORMAL){
                ls.add("1");
            }
            if (message.strikeThrough){
                ls.add("2");
            }
            if (message.underLine){
                ls.add("4");
            }
            switch (message.foregroundColor){
            case "":
                ls.add("39");
                break;
            case "w":
                ls.add("97");
                break;
            case "k":
                ls.add("30");
                break;
            case "r":
                ls.add("31");
                break;
            case "g":
                ls.add("32");
                break;
            case "y":
                ls.add("33");
                break;
            case "b":
                ls.add("34");
                break;
            case "m":
                ls.add("35");
                break;
            case "c":
                ls.add("36");
                break;
            }
            switch (message.backgroundColor){
            case "":
                ls.add("49");
                break;
            case "w":
                ls.add("107");
                break;
            case "k":
                ls.add("40");
                break;
            case "r":
                ls.add("41");
                break;
            case "g":
                ls.add("42");
                break;
            case "y":
                ls.add("43");
                break;
            case "b":
                ls.add("44");
                break;
            case "m":
                ls.add("45");
                break;
            case "c":
                ls.add("46");
                break;
            }
            return "\033[" + String.join(";", ls) + "m" + message.text + "\033[m";
        }).collect(Collectors.joining(""));
    }

    public static String removeExpression(String line){
        return EXPRESSION_PATTERN.matcher(line).replaceAll("");
    }

    /**
     * remove color escape character (\\033[.*m)
     *
     * @param line
     * @return
     */
    public static String unescape(String line){
        return line.replaceAll("\\033\\[.*?m", "");
    }

    public static boolean containExpression(String line){
        return EXPRESSION_PATTERN.matcher(line).find();
    }

    public static String getMessage(String key){
        return property.getProperty(key);
    }

    public static String getMessage(String key, String def){
        return property.getProperty(key, def);
    }

    public static String echo(String line){
        return COLORLESS? removeExpression(line): mapTermExpression(line);
    }

    public static String format(String key, Object... args){
        String format = getMessage(key);
        if (format != null){
            return String.format(format, args);
        }
        return key + " " + Arrays.toString(args);
    }


}
