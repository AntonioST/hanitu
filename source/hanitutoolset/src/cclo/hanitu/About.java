/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import cclo.hanitu.gui.FXMain;
import cclo.hanitu.gui.MenuSession;
import cclo.hanitu.gui.MessageSession;

import static cclo.hanitu.HanituInfo.AUTHORS;

/**
 * @author antonio
 */
public class About implements FXMain.FXSimpleApplication{

    private static String TITLE;

    static{
        Properties p = Base.loadPropertyInherit("gui");
        TITLE = p.getProperty("menu.help.about");
    }

    public static final String HELP_DOC;

    static{
        StringBuilder sb = new StringBuilder();
        sb.append("~**{Hanitu~}  ").append(HanituInfo.VERSION).append("\n");
        sb.append("~*{website~} : ~_b{").append(HanituInfo.WEB_SITE[0][1]).append("~}\n");
        sb.append("~*{authors~} : \n");
        for (String[] author : AUTHORS){
            sb.append("  ").append(author[0]).append(" ~_b{").append(author[1]).append("~}\n");
        }
        sb.append("~*{license~} : ").append(HanituInfo.LICENSE).append("\n");
        HELP_DOC = sb.toString();
    }

    private List<String> appendMessage = new ArrayList<>();

    public About(String... appendMessages){
        appendMessage.addAll(Arrays.asList(appendMessages));
    }

    public About(List<String> appendMessage){
        this.appendMessage.addAll(appendMessage);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        show();
    }

    public void show(){
        String message;
        if (appendMessage.isEmpty()){
            message = HELP_DOC;
        } else {
            StringBuilder builder = new StringBuilder();
            builder.append(HELP_DOC);
            for (String arg : appendMessage){
                builder.append("\n").append(arg);
            }
            message = builder.toString();
        }
        //
        Stage stage = new Stage(StageStyle.UTILITY);
        stage.setTitle(TITLE);
        stage.setMinWidth(200);
        stage.setMinHeight(100);
        //
        VBox textRegion = MessageSession.initTextRegion(message);
        Scene scene = new Scene(textRegion);
        textRegion.getChildren().stream()
          .filter(t -> (t instanceof TextFlow))
          .flatMap(t -> ((TextFlow)t).getChildren().stream())
          .filter(t -> (t instanceof Text))
          .map(t -> (Text)t)
          .filter((text) -> text.isUnderline() || text.getText().contains("@"))
          .forEach(t -> {
              t.setOnMouseEntered(e -> {
                  scene.setCursor(Cursor.HAND);
              });
              t.setOnMouseExited(e -> {
                  scene.setCursor(Cursor.DEFAULT);
              });
              t.setOnMouseClicked(e -> {
                  Clipboard clipboard = Clipboard.getSystemClipboard();
                  ClipboardContent content = new ClipboardContent();
                  content.putString(t.getText());
                  content.putHtml(t.getText());
                  clipboard.setContent(content);
              });
          });
        //
        HBox controlRegion = MessageSession.initControlRegion(MessageSession.NONE_OPTION, null);
        //
        textRegion.getChildren().addAll(controlRegion);
        Insets value = new Insets(5);
        textRegion.getChildren().forEach(c -> VBox.setMargin(c, value));
        //
        scene.setOnKeyPressed(e -> {
            switch (e.getCode()){
            case ENTER:
            case ESCAPE:
                stage.close();
                break;
            }
        });
        //
        stage.setScene(scene);
        //
        stage.requestFocus();
        stage.showAndWait();
    }

    public static MenuItem getMenuItem(String... appendMessages){
        return MenuSession.createMenuItem(TITLE, "F1",
                                          e -> new About(appendMessages).show());
    }
}
