/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.IOException;
import java.nio.file.Paths;

import cclo.hanitu.data.DataLoader;
import cclo.hanitu.data.DataPrinter;
import cclo.hanitu.data.LocationData;
import cclo.hanitu.data.LocationLoader;
import cclo.hanitu.gui.FXMain;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.WorldConfigLoader;
import cclo.hanitu.world.view.AbstractWorldCanvas;
import cclo.hanitu.world.view.DefaultWorldCanvas;
import cclo.hanitu.world.view.ShadowWorldCanvas;
import cclo.hanitu.world.view.VirtualWorld;

import static cclo.hanitu.HanituInfo.WORLD_CONFIG_DEFAULT_FILENAME;

/**
 * @author antonio
 */
@HanituInfo.Tip("plot")
@HanituInfo.Description("Displaying the virtual world and worm movements for Hanitu.")
public class Plot extends Executable{

    @HanituInfo.Option(shortName = 'w', value = "world", arg = "FILE")
    @HanituInfo.Description(resource = "cli", value = "plot.option.world")
    public String worldConfigFileName = WORLD_CONFIG_DEFAULT_FILENAME;
    public WorldConfig config;

    @HanituInfo.Option(shortName = 'o', value = "output", arg = "FILE")
    @HanituInfo.Description("Writing the content the program received from the input into a file.\n"
                            + "This is useful when the program receives the locations directly from"
                            + "Hanitu but want to save a copy for the input. use '-' output to"
                            + "standard output.")
    public String outputFileName = null;
    public DataLoader<LocationData> loader;

    @HanituInfo.Option(value = "style", arg = "CLASS", order = 1)
    @HanituInfo.Description("choose the paint style.\n" +
                            "It could be ~*{default~} or ~*{shadow~} two style. ")
    public String style;

    @HanituInfo.Option(value = "pause", order = 2)
    @HanituInfo.Description("pause when the plotting window starting reading location.")
    public boolean pauseWhenStart = false;

    @HanituInfo.Argument(index = 0, value = "FILE")
    @HanituInfo.Description("Location data, default is reading the data from the standard input.")
    public String locationFileName = null;
    //
    public VirtualWorld frame;

    public WorldConfig setupWorldConfig() throws IOException{
        if (config == null){
            WorldConfigLoader loader = new WorldConfigLoader(true);
            loader.setIgnoreCircuitFile(true);
            config = loader.load(Paths.get(worldConfigFileName));
//                if (config == null){
//                    throw new RuntimeException("loading config error");
//                }
        }
        return config;
    }

    public DataLoader<LocationData> setupLocationLoader() throws IOException{
        if (loader == null){
            // location loading
            if (locationFileName == null || locationFileName.equals("-")){
                loader = new LocationLoader.Stream(System.in);
            } else {
                loader = new LocationLoader.File(locationFileName);
            }
            // setting output stream
            if (outputFileName != null){
                if (outputFileName.equals("-")){
                    loader = new DataPrinter<>(System.out, loader);
                } else {
                    loader = new DataPrinter<>(outputFileName, loader);
                }
            }
        }
        return loader;
    }

    public static AbstractWorldCanvas getCanvasStyle(String style){
        if (style == null || style.equals("shadow")){
            return new ShadowWorldCanvas();
        } else if (style.equals("default")){
            return new DefaultWorldCanvas();
        } else {
            throw new RuntimeException("unknown style : " + style);
        }
    }

    public VirtualWorld setupFrame() throws IOException{
        if (frame == null){
            AbstractWorldCanvas canvas = getCanvasStyle(style);
            frame = new VirtualWorld();
            frame.setWorldConfig(setupWorldConfig());
            //event setting
            if (pauseWhenStart){
                frame.startEvent.add(() -> frame.setPause(true));
            }
            frame.setLoader(setupLocationLoader());
            frame.setCanvas(canvas);
        }
        return frame;
    }

    @Override
    public void start() throws IOException{
        FXMain.launch(setupFrame());
    }
}
