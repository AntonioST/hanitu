/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

import cclo.hanitu.util.ErrorFunction;

/**
 * The Abstract Class of Hanitu Parameter File Parser.
 *
 * It is a line-base, state parser. Each parameter setting following the `KEY=VALUE` pattern, and use Keyword to label
 * the begin of data structure and the end.
 *
 * @author antonio
 */
public abstract class FileLoader<T>{

    private static class RescueJump extends RuntimeException{}

    private static class ReParseLine extends RuntimeException{

        private final String line;

        ReParseLine(String line){
            this.line = line;
        }
    }

    private static class ReloadingJump extends RuntimeException{}

    //meta data
    /**
     * meta data keyword, also the meta identify in file.
     */
    public static final String META_HEADER = "%meta.";
    /**
     * the header meta data represent the file type of the file.
     */
    public static final String META_FILE_HEADER = "header";
    /**
     * the header meta data represent the version of the file format or structure.
     */
    public static final String META_VERSION = "version";
    /**
     * the header meta data represent the name of the file.
     * In general, they equal to the file name.
     */
    public static final String META_NAME = "name";
    /**
     * the header meta data represent the author of the file.
     */
    public static final String META_AUTHOR = "author";
    /**
     * the header meta data represent the create time of the file in Unix format.
     */
    public static final String META_CREATE_TIME = "createtime";
    /**
     * the global meta data just represent the human readable message, and it will be record during loading by default.
     */
    public static final String META_MESSAGE = "message";
    //
    private Deque<String> readBuffer = new LinkedList<>();
    private BufferedReader reader;
    private Path fileDir;
    private String filename;
    private int lineNumber;
    private String originalLine;
    private String currentLine;
    private String rescueLine;
    private String reParseLine;
    private Map<String, Object> resource;
    private String sourceVersionExpression;
    private FileLoaderImpl<T> impl;
    //
    boolean rescueMode;
    boolean failOnLoad = false;
    boolean skipEmptyLine = true;
    private List<WarningRecord> errorCollect;
    private List<WarningRecord> warningCollect;

    public FileLoader(){
    }

    public String getSourceVersion(){
        return sourceVersionExpression;
    }

    public void setSourceVersion(String expr){
        if (!isVersionSupport(Objects.requireNonNull(expr))){
            throw new RuntimeException("not support for version : " + expr);
        }
        sourceVersionExpression = expr;
    }

    /**
     * @param failOnLoad true for throwing a {@link IOException} if any error or warning occurs during loading.
     */
    public FileLoader(boolean failOnLoad){
        this.failOnLoad = failOnLoad;
    }

    public T load(String filePath) throws IOException{
        return load(Paths.get(filePath));
    }

    /**
     * load file and return a instance `T`.
     *
     * @param path file path
     * @return instance of T
     * @throws IOException
     * @see #load(BufferedReader)
     */
    public T load(Path path) throws IOException{
        fileDir = path.toAbsolutePath().getParent();
        filename = path.getFileName().toString();
        Log.debug(() -> "load file : " + filename);
        Log.debug(() -> "at dir : " + fileDir);
        reader = Files.newBufferedReader(path);
        try {
            while (true){
                initLoading();
                try {
                    reloadingBlock();
                    break;
                } catch (ReloadingJump e){
                    try {
                        reader.close();
                    } catch (IOException j){
                    }
                    reader = Files.newBufferedReader(path);
                    //do loading again
                }
            }
        } catch (Throwable e){
            errorCollect.add(new WarningRecord(this, e));
        } finally {
            try {
                reader.close();
            } catch (IOException e){
            }
        }
        errorReport();
        return endLoading(impl.endLoading());
    }

    public T load(InputStream is) throws IOException{
        return load(new BufferedReader(new InputStreamReader(is)));
    }

    /**
     * load from a {@link BufferedReader} and return a instance `T`.
     *
     * @param r Reader
     * @return instance of T
     * @throws IOException
     */
    public T load(BufferedReader r) throws IOException{
        reader = r;
        initLoading();
        try {
            reloadingBlock();
        } catch (ReloadingJump e){
            throwError("reloading error", "cannot reloading from stream source");
        }
        errorReport();
        return endLoading(impl.endLoading());
    }

    private void initLoading(){
        resource = new HashMap<>();
        errorCollect = new ArrayList<>();
        warningCollect = new ArrayList<>();
        lineNumber = 0;
        impl = null;
        startLoading();
    }

    private boolean nextLine() throws IOException{
        if (!readBuffer.isEmpty()){
            originalLine = readBuffer.removeFirst();
            lineNumber++;
            return true;
        }
        if ((originalLine = reader.readLine()) != null){
            lineNumber++;
            return true;
        }
        return false;
    }

    private List<String> readAllIntoBuffer() throws IOException{
        LinkedList<String> temp = new LinkedList<>();
        int currentLineNumber = lineNumber - 1;
        do {
            temp.add(originalLine);
        } while (nextLine());
        lineNumber = currentLineNumber;
        readBuffer.addAll(temp);
        return Collections.unmodifiableList(new ArrayList<>(temp));
    }

    private void reloadingBlock() throws IOException{
        //reading header meta
        headerBlock();
        //decide impl by file version
        if (sourceVersionExpression != null){
            impl = getImplByVersion(sourceVersionExpression);
            Log.debug(() -> "~*r{file version~} (by set) : " + sourceVersionExpression);
        } else if (containResource(META_VERSION)){
            sourceVersionExpression = (String)getResource(META_VERSION);
            impl = getImplByVersion(sourceVersionExpression);
            Log.debug(() -> "~*r{file version~} (by meta) : " + sourceVersionExpression);
        } else {
            currentLine = null;
            impl = getImplByFileContent(readAllIntoBuffer());
            sourceVersionExpression = impl.getSourceVersion();
            Log.debug(() -> "~*r{file version~} (by file) : " + sourceVersionExpression);
        }
        if (impl == null){
            throw new RuntimeException("cannot determine the version of the file");
        }
        Log.debug(() -> "impl class : " + impl.getClass().getSimpleName());
        Log.debug(() -> "impl version : " + impl.versionRequired());
        //
        impl.startLoading(this);
        while (impl.state >= FileLoaderImpl.INIT_STATE){
            if (currentLine == null){
                if (!nextLine()) break;
                if (startsWithCaseInsensitive(originalLine, META_HEADER)){
                    if (impl.parseMeta(FileLoaderImpl.getMetaKey(originalLine),
                                       FileLoaderImpl.getMetaValue(originalLine))){
                        continue;
                    }
                    currentLine = originalLine;
                } else {
                    currentLine = ignoreLine(originalLine);
                }
            }
            if (!currentLine.isEmpty() || !skipEmptyLine){
                while (retryBlock()) ;
            }
            currentLine = null;
        }
    }

    private void headerBlock() throws IOException{
        while (nextLine()){
            if (originalLine.isEmpty()) continue;
            if (startsWithCaseInsensitive(originalLine, META_HEADER)){
                parseHeadMeta(FileLoaderImpl.getMetaKey(originalLine), FileLoaderImpl.getMetaValue(originalLine));
            } else {
                currentLine = ignoreLine(originalLine);
                break;
            }
        }
    }

    /**
     * handle the meta data at the header of the file.
     *
     * Default handle and check {@link #META_FILE_HEADER}, {@link #META_VERSION}, {@link #META_NAME}, {@link #META_AUTHOR}
     * and {@link #META_CREATE_TIME}. The other meta will put into resource ({@link #putResource(String, Object)}).
     *
     * {@link #META_FILE_HEADER} will call {@link #isFileTypeSupport(String)}.
     *
     * {@link #META_VERSION} will call {@link #isVersionSupport(String)}.
     *
     * @param key   meta key
     * @param value meta value, null if empty
     * @return Is the meta data accepted.
     * @throws RuntimeException if checked meta with a null (empty) value, or {@link #isFileTypeSupport(String)},
     *                          {@link #isVersionSupport(String)} return false,
     * @see #putResource(String, Object)
     * @see #isFileTypeSupport(String)
     * @see #isVersionSupport(String)
     */
    protected boolean parseHeadMeta(String key, String value){
        switch (key){
        //default meta header
        case META_FILE_HEADER:
            if (value == null){
                throw new RuntimeException("bad file : lost file type");
            } else if (!isFileTypeSupport(value)){
                throw new RuntimeException("the type of the file is not supported : " + value);
            }
            putResource(key, value);
            return true;
        case META_VERSION:
            if (value == null){
                throw new RuntimeException("bad file : lost version");
            } else if (!isVersionSupport(value)){
                throw new RuntimeException("the version of the file is not supported : " + value);
            }
            putResource(key, value);
            return true;
        case META_NAME:
        case META_AUTHOR:
        case META_CREATE_TIME:
            if (value == null){
                throw new RuntimeException("bad file : lost " + key);
            }
            putResource(key, value);
            return true;
        }
        return false;
    }

    private boolean retryBlock(){
        try {
            if (rescueMode){
//                Env.debug(() -> "[rescue] : " + rescueLine);
                impl.parseLine(rescueLine);
            } else if (reParseLine != null){
//                Env.debug(() -> "[reParse] : " + reParseLine);
                impl.parseLine(reParseLine);
            } else {
//                Env.debug(() -> "[input] : " + currentLine);
                impl.parseLine(currentLine);
            }
            reParseLine = null;
        } catch (ReParseLine e){
            reParseLine = e.line;
            return true;
        } catch (RescueJump e){
            throw e;
        } catch (RuntimeException e){
            warningCollect.add(new WarningRecord(this, e));
        } catch (Exception e){
            errorCollect.add(new WarningRecord(this, e));
        }
        return false;
    }

    private void errorReport() throws IOException{
        if (!warningCollect.isEmpty()){
            String head = Message.getMessage("loader.head.warning");
            for (WarningRecord w : warningCollect){
                w.print(head, System.err);
            }
        }
        if (!errorCollect.isEmpty()){
            String head = Message.getMessage("loader.head.error");
            for (WarningRecord w : errorCollect){
                w.print(head, System.err);
            }
        }
        if (!errorCollect.isEmpty()
            || (failOnLoad && !warningCollect.isEmpty())
            || (impl != null && impl.state < FileLoaderImpl.TERMINAL_STATE)){
            StringBuilder message = new StringBuilder();
            message.append("terminal loading : ").append(filename);
            if (impl != null){
                message.append(" (exit:").append(impl.getCurrentStatDescription()).append(")");
            }
            for (WarningRecord er : errorCollect){
                message.append("\n  ").append(er.title);
                er.message.forEach(m -> message.append("\n    ").append(m));
            }
            for (WarningRecord wr : warningCollect){
                message.append("\n  ").append(wr.title);
                wr.message.forEach(m -> message.append("\n    ").append(m));
            }
            throw new IOException(message.toString());
        }
    }

    public static boolean startsWithCaseInsensitive(String input, String match){
        int length = match.length();
        if (input.length() < length) return false;
        for (int i = 0; i < length; i++){
            if (Character.toLowerCase(input.charAt(i)) != Character.toLowerCase(match.charAt(i))) return false;
        }
        return true;
    }

    /**
     * checking the file type of the file is supported by this loader.
     *
     * @param value file type identify text
     * @return Is this loader supported?
     */
    protected abstract boolean isFileTypeSupport(String value);

    public abstract FileLoaderImpl<T> getImplByVersion(String expr);

    public abstract FileLoaderImpl<T> getImplByFileContent(List<String> content);

    /**
     * checking the version of the file is supported by this loader.
     *
     * @param expr version expression
     * @return Is this loader supported?
     */
    protected abstract boolean isVersionSupport(String expr);

    /**
     * test target version is compatibility to the required version
     *
     * Pattern
     * -------
     *
     * *Target Version*
     *
     * number[.number]*[-text]
     *
     * text after '-' will be ignored.
     *
     * *Required Version*
     *
     * [op]number[.number]*[.*]?
     *
     * op could be:
     *
     * + `=`, `==`
     * + `>=`
     * + `<=`
     * + `>`, `>>`
     * + `<`, `<<`
     * + '!`, `!=`
     *
     * @param version target version expression
     * @param require required version expression
     * @return
     */
    public static boolean isVersionCompatibility(String version, String require){
        Objects.requireNonNull(version);
        if (require.contains(",")){
            return Stream.of(require.split(",")).allMatch(r -> isVersionCompatibility(version, r));
        }
        String requiredVersion;
        int op;
        if (require.startsWith("==")){
            requiredVersion = require.substring(2).trim();
            op = 0;
        } else if (require.startsWith("=")){
            requiredVersion = require.substring(1).trim();
            op = 0;
        } else if (require.startsWith(">=")){
            requiredVersion = require.substring(2).trim();
            op = 3;
        } else if (require.startsWith(">>")){
            requiredVersion = require.substring(2).trim();
            op = 2;
        } else if (require.startsWith(">")){
            requiredVersion = require.substring(1).trim();
            op = 2;
        } else if (require.startsWith("<=")){
            requiredVersion = require.substring(2).trim();
            op = 5;
        } else if (require.startsWith("<<")){
            requiredVersion = require.substring(2).trim();
            op = 4;
        } else if (require.startsWith("<")){
            requiredVersion = require.substring(1).trim();
            op = 4;
        } else if (require.startsWith("!=")){
            requiredVersion = require.substring(2).trim();
            op = 1;
        } else if (require.startsWith("!")){
            requiredVersion = require.substring(1).trim();
            op = 1;
        } else {
            throw new IllegalArgumentException("unknown version require expression : " + require);
        }
        if (!version.matches("\\d+(\\.\\d+)*(-.+)?")){
            throw new IllegalArgumentException("unknown version expression : " + version);
        } else if (!requiredVersion.matches("\\d+(\\.\\d+)*(\\.\\*)?")){
            throw new IllegalArgumentException("unknown version expression : " + requiredVersion);
        }
        Iterator<String> t1;
        if (version.contains("-")){
            int index = version.indexOf("-");
            t1 = Arrays.asList(version.substring(0, index).split("\\.")).iterator();
        } else {
            t1 = Arrays.asList(version.split("\\.")).iterator();
        }
        Iterator<String> t2 = Arrays.asList(requiredVersion.split("\\.")).iterator();
        String padding = "0";
        while (t1.hasNext() || t2.hasNext()){
            String s1 = t1.hasNext()? t1.next(): "0";
            String s2 = t2.hasNext()? t2.next(): padding;
            if ("*".equals(s2)){
                s2 = s1;
                padding = "*";
            }
            int v1 = Integer.parseInt(s1);
            int v2 = Integer.parseInt(s2);
            // 0 ==
            // 1 !=
            // 2 >>
            // 3 >=
            // 4 <<
            // 5 <=
            if (op == 0 && v1 != v2){
                return false;
            } else if (op == 1 && v1 != v2){
                return true;
            } else if (op == 2 && v1 > v2){
                return true;
            } else if (op == 3 && v1 < v2){
                return false;
            } else if (op == 4 && v1 < v2){
                return true;
            } else if (op == 5 && v1 > v2){
                return false;
            }
        }
        return (op == 0 || op == 3 || op == 5);
    }

    public static <T> FileLoaderImpl<T> isVersionCompatibility(String version,
                                                               Class<? extends FileLoaderImpl<T>> cls){
        try {
            FileLoaderImpl<T> impl = cls.newInstance();
            if (isVersionCompatibility(version, impl.versionRequired())) return impl;
        } catch (IllegalAccessException | InstantiationException e){
        }
        return null;
    }

    /**
     * call before parsing. Normally used to init the template objects.
     */
    protected void startLoading(){
    }

    /**
     * ignore current input line and continue parsing next line in file.
     *
     * Default ignore line with leading character `'%'`
     *
     * @param line current parsed line
     * @return modify line
     */
    protected String ignoreLine(String line){
        int i = line.indexOf("%");
        if (i == 0){
            return "";
        } else if (i > 0){
            return line.substring(0, i).trim();
        } else {
            return line;
        }
    }

    /**
     * call after parsing.
     *
     * @return instance of T
     */
    protected T endLoading(T ret){
        return ret;
    }

    /**
     * generate a error message and record it. cause a {@link IOException} after parsed file.
     *
     * @param title   error title
     * @param message error message
     */
    void throwError(String title, String... message){
        errorCollect.add(new WarningRecord(this, title, message));
    }

    /**
     * generate a warning message and record it. It doesn't cause a {@link IOException} after parsed file if option
     * "fail on load" is false.
     *
     * @param title   warning title
     * @param message warning message
     */
    void throwException(String title, String... message){
        warningCollect.add(new WarningRecord(this, title, message));
    }

    private String newAlterLine(String alterKey, String alterValue){
        if (alterKey != null && alterValue != null){
            return alterKey + "=" + alterValue;
        } else if (alterKey != null){
            if (currentLine.contains("=")){
                return alterKey + currentLine.substring(currentLine.indexOf("="));
            } else {
                return alterKey;
            }
        } else if (alterValue != null){
            int index = currentLine.indexOf("=");
            if (index < 0){
                return currentLine + "=" + alterValue;
            } else {
                return currentLine.substring(0, index + 1) + alterValue;
            }
        } else {
            return null;
        }
    }

    /**
     * re-parsing current line. This method will throw a {@link RuntimeException} (a private Exception extend from
     * RuntimeException actually) and caught by the parsing loop in {@link #load(BufferedReader)}. Therefore,
     * be
     * careful use this method because the code after this method won't be execute.
     */
    public void reParseLine(){
        throw new ReParseLine(currentLine);
    }

    /**
     * re-parsing a modified line. This method will throw a {@link RuntimeException} (a private Exception extend from
     * RuntimeException actually) and caught by the parsing loop in {@link #load(BufferedReader)}. Therefore,
     * be
     * careful use this method because the code after this method won't be execute.
     *
     * @param alterKey   modified key, can null
     * @param alterValue modified value, can null
     */
    public void reParseLine(String alterKey, String alterValue){
        String line = newAlterLine(alterKey, alterValue);
        if (line != null){
            Log.debug(() -> Message.format("loader.reparse", currentLine, line));
            throw new ReParseLine(line);
        } else {
            Log.debug(() -> "re-parse line fail");
        }
    }

    /**
     * run a un-secure code, and catch the exception to recording system. {@link RuntimeException} will record to
     * warning message collection, and `Exception` will record to error message collection.
     *
     * @param r un-secure code
     */
    public void tryDo(ErrorFunction.Action<Throwable> r){
        Objects.requireNonNull(r);
        try {
            r.run();
        } catch (RuntimeException e){
            warningCollect.add(new WarningRecord(this, e));
        } catch (Throwable e){
            errorCollect.add(new WarningRecord(this, e));
        }
    }

    /**
     * do something in rescue mode, and return the result.
     *
     * @param r
     * @param <R> return result type
     * @return result
     */
    public <R> R tryInRescueMode(Supplier<R> r){
        Objects.requireNonNull(r);
        boolean old = rescueMode;
        rescueMode = true;
        try {
            return r.get();
        } finally {
            rescueMode = old;
        }
    }

    public void tryRescue(String alterKey, String alterValue){
        if (rescueMode){
            Log.debug("rescue fail : fail twice");
            throw new RescueJump();
        }
        rescueLine = newAlterLine(alterKey, alterValue);
        if (rescueLine != null){
            rescueMode = true;
            Log.debug(() -> Message.format("loader.rescue.line", currentLine, rescueLine));
            try {
                impl.parseLine(rescueLine);
            } catch (RescueJump e){
            } catch (Throwable e){
                Log.debug("rescue fail : error occurs");
            } finally {
                doneRescue();
            }
        } else {
            Log.debug("nothing rescue");
        }
    }

    /**
     * read line from file and skip go into parsing loop until `p` test true. This method should call in rescue mode.
     *
     * @param p test
     * @return first line which `p` test true
     */
    public String skipUntil(Predicate<String> p){
        Objects.requireNonNull(p);
        if (!rescueMode){
            throw new IllegalStateException("not in a rescue mode");
        }
        try {
            while (nextLine()){
                if (p.test(currentLine = ignoreLine(originalLine))){
                    Log.debug(() -> "skip until : " + currentLine);
                    return currentLine;
                }
                Log.debug(() -> "skip : " + originalLine);
            }
        } catch (IOException e){
        }
        return null;
    }

    /**
     * turn off the rescue mode.
     */
    public void doneRescue(){
        rescueMode = false;
        rescueLine = null;
    }

    /**
     * put a key and a value pair into resource collection.
     *
     * @param key   key
     * @param value value
     */
    public void putResource(String key, Object value){
        resource.put(key, value);
        Log.debug(() -> "~*r{resource~} : ~*y{" + key + "~} = " + value);
    }

    /**
     * Is key store in the resource collection?
     *
     * @param key key
     * @return true if contain.
     */
    public boolean containResource(String key){
        return resource.containsKey(key);
    }

    /**
     * get the correspond value store in the resource collection.
     *
     * @param key key
     * @return value, null if not found or null actually.
     */
    public Object getResource(String key){
        return resource.get(key);
    }

    /**
     * remove correspond value from the resource collection.
     *
     * @param key key
     * @return value
     */
    public Object removeResource(String key){
        Log.debug(() -> "~*r{resource~} : " + key + " ~*y{(remove)}");
        return resource.remove(key);
    }


    /**
     * get current line number.
     *
     * @return current line number
     */
    public int getCurrentLineNumber(){
        return lineNumber;
    }

    /**
     * get current input line actually.
     *
     * @return current input line.
     */
    public String getOriginalLine(){
        return originalLine;
    }

    public Path getFileDirectory(){
        return fileDir;
    }

    /**
     * get source file name
     *
     * @return file name
     */
    public String getFileName(){
        return filename;
    }


    public boolean isFailOnLoad(){
        return failOnLoad;
    }

    protected FileLoaderImpl<T> getImpl(){
        return impl;
    }
}
