/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import cclo.hanitu.HanituInfo.Option;
import cclo.hanitu.circuit.CircuitPrinter;
import cclo.hanitu.world.WorldConfigPrinter;

import static cclo.hanitu.HanituInfo.Description;

/**
 * @author antonio
 */
@HanituInfo.Tip("hanitu interface")
@HanituInfo.Description("~*y{WARNING~} : don't execute this program directly\n" +
                        "hanitu main program for configuring virtual environment and worms")
public class Hanitu extends NativeExecutable{

    /**
     * property of the location of the hanitu
     */
    public static final String PROPERTY_PATH = "cclo.hanitu.hanitu";
    /**
     * property of the output stream of the hanitu
     */
    public static final String PROPERTY_OUT = "cclo.hanitu.hanitu.out";
    /**
     * property of the error output stream of the hanitu
     */
    public static final String PROPERTY_ERR = "cclo.hanitu.hanitu.err";
    public static final Path PATH;

    static{
        Path path = null;
        String property = System.getProperty(PROPERTY_PATH);
        if (property != null){
            path = Paths.get(property).toAbsolutePath().normalize();
        } else if (Base.HOME != null){
            try {
                path = findExecutable("Hanitu.*out");
            } catch (IOException e){
                path = null;
            }
        }
        PATH = path;
        if (PATH != null){
            Log.debug(() -> "~*r{find~} " + Hanitu.class.getSimpleName() + " ~*y{at~} " + PATH);
        } else {
            Log.debug(() -> "~*r{un-find~} " + Hanitu.class.getSimpleName());
        }
    }

    public static final String PROPERTY_REQUEST_WORLD_CONFIG_VERSION = "cclo.hanitu.hanitu.version.world";
    public static final String PROPERTY_REQUEST_CIRCUIT_VERSION = "cclo.hanitu.hanitu.version.circuit";
    public static final String REQUIRE_WORLD_CONFIG_VERSION;
    public static final String REQUIRE_CIRCUIT_VERSION;

    static{
        String world = new WorldConfigPrinter().getImplForLatestVersion().getTargetVersion();
        String circuit = new CircuitPrinter().getImplForLatestVersion().getTargetVersion();
        REQUIRE_WORLD_CONFIG_VERSION = System.getProperty(PROPERTY_REQUEST_WORLD_CONFIG_VERSION, world);
        REQUIRE_CIRCUIT_VERSION = System.getProperty(PROPERTY_REQUEST_CIRCUIT_VERSION, circuit);
        Log.debug(() -> "hanitu require world version : " + REQUIRE_WORLD_CONFIG_VERSION);
        Log.debug(() -> "hanitu require circuit version : " + REQUIRE_CIRCUIT_VERSION);
        if (!FileLoader.isVersionCompatibility(world, ">=1.3")){
            throw new RuntimeException("native hanitu version is too old to be supported");
        }
    }

    @Option(shortName = 'w', value = "world", arg = "FILE", order = 0)
    @Description(resource = "cli", value = "hanitu.option.world")
    public Path worldConfigPath;

    @Option(shortName = 'l', value = "location", arg = "FILE", order = 0)
    @Description(resource = "cli", value = "hanitu.option.location")
    public Path locationOutputPath;

    @Option(shortName = 's', value = "spike", arg = "FILE", order = 0)
    @Description(resource = "cli", value = "hanitu.option.spike")
    public Path spikeOutputPath;

    @Option(value = "timeout", arg = "TIME", order = 1)
    @Description("set simulation timeout in unit ms")
    public int timeout = -1;

    @Option(value = "loc-time-step", arg = "TIME", order = 1)
    @Description("set the time step of the location data when outputting")
    public int locTimeStep = 2;

    @Option(value = "overlap-ignore", order = 1)
    @Description("allow overlapping the location of the worms")
    public boolean overlapIgnore = false;


    public Hanitu(){
        super(PATH);
        outStream = System.getProperty(PROPERTY_OUT, "null");
        errStream = System.getProperty(PROPERTY_ERR, "null");
        Log.debug(() -> "hanitu=" + PATH.getFileName());
        Log.debug(() -> "~*y{1>~} " + outStream);
        Log.debug(() -> "~*y{2>~} " + errStream);
    }

    @Override
    public String version() throws Throwable{
        return getProgramOutput("--version").replaceFirst("(?m:.*version:)", "").trim();
    }

    @Override
    public String help() throws Throwable{
        return getProgramOutput("--help");
    }

    @Override
    protected ProcessBuilder createProcessBuilder(){
        Log.debug("setup hanitu");
        setupWorkingDirectory();
        List<String> args = new ArrayList<>();
        if (worldConfigPath != null){
            args.add("--world");
            args.add(worldConfigPath.toAbsolutePath().toString());
        }
        if (locationOutputPath != null){
            args.add("--location");
            args.add(locationOutputPath.toAbsolutePath().toString());
        }
        if (spikeOutputPath != null){
            args.add("--spike");
            args.add(spikeOutputPath.toAbsolutePath().toString());
        }
        if (timeout > 0){
            args.add("--timeout");
            args.add(Integer.toString(timeout));
        }
        args.add("--loc-time-step");
        args.add(Integer.toString(locTimeStep));
        return createProcessBuilder(args);
    }
}
