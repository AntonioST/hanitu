/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.PrintStream;
import java.util.Objects;

/**
 * Hanitu file Printer for printing text content to output stream.
 *
 * @author antonio
 */
public abstract class FilePrinterImpl<T> implements AutoCloseable{

    private FilePrinter<T> print;

    public FilePrinter<T> getPrinter(){
        return print;
    }

    void setPrinter(FilePrinter<T> print){
        this.print = print;
    }

    public abstract String getTargetVersion();

    public void putGlobalMeta(String key, String value){
        print.globalMeta.put(Objects.requireNonNull(key), value);
    }

    /**
     * write correspond target to a file.
     *
     * @param target
     */
    public abstract void write(PrintStream ps, T target);

    @Override
    public void close(){
        print.close();
    }

    public void meta(String key, String value){
        print.meta(key, value);
    }

    public void metaf(String key, String format, Object... args){
        print.metaf(key, format, args);
    }

    public void pair(String key, int value){
        print.pair(key, value);
    }

    public void pair(String key, double value){
        print.pair(key, value);
    }

    public void pair(String key, String value){
        print.pair(key, value);
    }

    public void line(String key){
        print.line(key);
    }

    public void line(){
        print.line();
    }

    public void comment(String content){
        print.comment(content);
    }
}
