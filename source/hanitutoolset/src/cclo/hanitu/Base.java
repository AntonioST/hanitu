/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author antonio
 */
public class Base{

    public static final String HANITU_JAR = Base.class.getProtectionDomain().getCodeSource().getLocation().getPath();

    public static final String PROPERTY_HOME = "cclo.hanitu.home";
    public static final String PROPERTY_HOME_ENV = "HANITU_HOME";
    public static final String HOME;

    static{
        String home = System.getProperty(PROPERTY_HOME);
        if (home != null){
            home = Paths.get(home).toAbsolutePath().toString();
        }
        if (home == null && System.getenv(PROPERTY_HOME_ENV) != null){
            home = Paths.get(System.getenv(PROPERTY_HOME_ENV)).toAbsolutePath().toString();
        }
        if (home == null){
            home = Paths.get(HANITU_JAR).getParent().toString();
        }
        HOME = home;
    }

    public static final String PROPERTY_0 = "cclo.hanitu.0";
    public static final String PROPERTY_1 = "cclo.hanitu.1";
    public static final String SELF_0; // hanitu_tool
    public static final String SELF_1; // hanitu_?
    public static final String SELF_J = "java -jar " + HANITU_JAR;

    static{
        String tmp = System.getProperty(PROPERTY_0);
        SELF_0 = tmp == null? null: tmp.replaceAll(".*/", "").intern();
        tmp = System.getProperty(PROPERTY_1);
        SELF_1 = tmp == null? null: tmp.replaceAll(".*/", "").intern();
    }

    private static Pattern REF = Pattern.compile("(\\$+)\\{(([a-zA-Z0-9_.]+::)?[a-zA-Z0-9_.]+?)}");
    private static Map<String, Properties> PROPERTIES = new HashMap<>();
    private static final String PROPERTY = System.getProperty("cclo.hanitu.property", "default");
    private static final Properties property;

    static{
        Properties p;
        try {
            p = loadPropertyInherit(PROPERTY);
        } catch (RuntimeException e){
            e.printStackTrace();
            p = new Properties();
        }
        property = p;
    }

    /**
     * load properties file from system resource.
     *
     * ### searching path
     *
     * 1. META-INF/ *path* .property
     * 2. ./ *path* .property
     * 3. *path*
     *
     * ### property **inherit**
     *
     * If properties file contain property **inherit**, this function also loading *inherited* properties file and
     * overwrite its content with current properties.
     *
     * ### property reference
     *
     * Use the pattern `${property}` to reference a property in current properties file or inherited. It will eval
     * and be replaced with the value of the referenced property when loaded.
     *
     * Use the pattern `${namespase::property}` to reference a property in another properties file. Load the properties
     * file with path *namespase*.
     *
     * @param path could null (return empty content properties).
     * @return properties, empty content if loading error.
     * @throws RuntimeException if recursive properties inherit or recursive reference. or IO error.
     */
    public static Properties loadPropertyInherit(String path){
        Properties properties = new Properties();
        if (path == null){
            return properties;
        }
        if (PROPERTIES.containsKey(path)){
            Properties ret = PROPERTIES.get(path);
            if (ret == null){
                throw new RuntimeException("recursive properties inherit");
            }
            return ret;
        }
        PROPERTIES.put(path, null);
        try {
            properties.load(ClassLoader.getSystemResourceAsStream("META-INF/" + path + ".properties"));
//            System.err.println("load property : META-INF/" + path + ".properties");
        } catch (IOException | NullPointerException e){
            try {
                properties.load(ClassLoader.getSystemResourceAsStream(path + ".properties"));
//                System.err.println("load property : " + path + ".properties");
            } catch (IOException | NullPointerException e1){
                try {
                    properties.load(ClassLoader.getSystemResourceAsStream(path));
                } catch (IOException | NullPointerException e2){
                    e.addSuppressed(e1);
                    e.addSuppressed(e2);
                    throw new RuntimeException("loading properties fail : " + path, e);
                }
            }
        }
        try {
            Properties inherit = loadPropertyInherit((String)properties.remove("inherit"));
            //avoid parent properties be overwrite
            Properties ret = resolveProperties(new Properties(inherit), properties);
            PROPERTIES.put(path, ret);
            return ret;
        } catch (RuntimeException e){
            throw new RuntimeException("loading : " + path, e);
        }
    }

    static Properties resolveProperties(Properties override, Properties current){
        List<String> it = current.stringPropertyNames().stream()
          .filter(key -> {
              if (!current.getProperty(key).contains("$")){
                  override.put(key, current.getProperty(key));
                  return false;
              }
              return true;
          }).collect(Collectors.toList());
        int size = it.size();
        while (!it.isEmpty()){
            it.removeIf(k -> resolveProperty(override, k, current.getProperty(k)));
            if (it.size() == size) throw new RuntimeException("un-resolve reference : " + it);
            size = it.size();
        }
        return override;
    }

    static boolean resolveProperty(Properties override, String key, String value){
        Matcher m = REF.matcher(value);
        if (m.find()){
            StringBuilder buffer = new StringBuilder();
            int start = 0;
            do {
                buffer.append(value.substring(start, m.start()));
                String g0 = m.group();
                String g1 = m.group(1);
                if (g1.length() != 1){
                    buffer.append(g0.substring(1));
                } else {
                    String g2 = m.group(2);
                    if (g2.contains("::")){
                        int i = g2.indexOf("::");
                        buffer.append(loadPropertyInherit(g2.substring(0, i))
                                        .getProperty(g2.substring(i + 2), g0));
                    } else if (override.containsKey(g2)){
                        buffer.append(override.getProperty(g2));
                    } else {
                        return false;
                    }
                }
                start = m.end();
            } while (m.find(start));
            buffer.append(value.substring(start));
            value = buffer.toString();
        }
        override.put(key, value);
        return true;
    }

    /**
     * get property from default.
     *
     * @param key
     * @return
     */
    public static String getProperty(String key){
        if (property.containsKey(key)){
            return property.getProperty(key);
        } else {
            return System.getProperty(key);
        }
    }

    /**
     * get property from default.
     *
     * @param key
     * @param def
     * @return
     */
    public static String getProperty(String key, String def){
        if (property.containsKey(key)){
            return property.getProperty(key);
        } else {
            return System.getProperty(key, def);
        }
    }

    /**
     * get property from default.
     *
     * @param key
     * @param def
     * @return
     */
    public static String getAndSetProperty(String key, String def){
        if (property.containsKey(key)){
            return property.getProperty(key);
        } else {
            String ret = System.getProperty(key, def);
            if (ret == null){
                property.put(key, def);
                return def;
            }
            return ret;
        }
    }

    /**
     * get property from default.
     *
     * @param key
     * @param def
     * @return
     */
    public static int getIntegerProperty(String key, int def){
        try {
            return Integer.parseInt(getProperty(key));
        } catch (NumberFormatException | NullPointerException e){
            return def;
        }
    }

    /**
     * get property from default.
     *
     * @param key
     * @param def
     * @return
     */
    public static boolean getBooleanProperty(String key, boolean def){
        try {
            return Boolean.parseBoolean(getProperty(key));
        } catch (NumberFormatException | NullPointerException e){
            return def;
        }
    }

    public static boolean osWin(){
        return System.getProperty("os.name").toLowerCase().contains("win");
    }

    public static boolean osLinux(){
        String name = System.getProperty("os.name").toLowerCase();
        return name.contains("mux") || name.contains("nix");
    }

    public static boolean osMac(){
        return System.getProperty("os.name").toLowerCase().contains("mac");
    }
}
