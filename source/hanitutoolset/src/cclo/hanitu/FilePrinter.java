/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Objects;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;

/**
 * Hanitu file Printer for printing text content to output stream.
 *
 * @author antonio
 */
public abstract class FilePrinter<T> implements AutoCloseable{

    final HashMap<String, String> globalMeta = new HashMap<>();
    private String targetVersionExpression;
    PrintStream out;

    public void putGlobalMeta(String key, String value){
        globalMeta.put(Objects.requireNonNull(key), value);
    }

    public String getTargetVersionExpression(){
        return targetVersionExpression;
    }

    public void setTargetVersion(String version){
        if (getImplWithTargetVersion(Objects.requireNonNull(version)) == null){
            throw new RuntimeException("version not supported : " + version);
        }
        targetVersionExpression = version;
    }

    public abstract FilePrinterImpl<T> getImplWithTargetVersion(String version);

    public abstract FilePrinterImpl<T> getImplForLatestVersion();


    public void write(String filePath, T target) throws IOException{
        write(Paths.get(filePath), target);
    }

    public void write(Path file, T target) throws IOException{
        Log.debug(() -> "write : " + file);
        try (OutputStream os = Files.newOutputStream(file, CREATE, TRUNCATE_EXISTING)) {
            write(new PrintStream(os), target);
        }
    }

    public void write(OutputStream os, T target){
        write(new PrintStream(os), target);
    }

    /**
     * write correspond target to a file.
     *
     * @param target
     */
    public void write(PrintStream ps, T target){
        Log.debug("write to stream ");
        out = ps;
        FilePrinterImpl<T> impl;
        if (targetVersionExpression != null){
            impl = getImplWithTargetVersion(targetVersionExpression);
            if (impl == null){
                throw new RuntimeException("version not supported : " + targetVersionExpression);
            }
        } else {
            impl = getImplForLatestVersion();
            if (impl == null){
                throw new IllegalStateException("version not supported : un-reachable branch");
            }
        }
        targetVersionExpression = impl.getTargetVersion();
        Log.debug(() -> "impl class : " + impl.getClass().getSimpleName());
        Log.debug(() -> "impl version : " + targetVersionExpression);
        impl.setPrinter(this);
        startWriting(out);
        impl.write(ps, target);
    }

    protected abstract void startWriting(PrintStream ps);

    protected void globalMeta(){
        globalMeta.forEach(this::meta);
    }

    @Override
    public void close(){
        out.close();
    }

    /**
     * add comment.
     *
     * @param content comment content
     */
    protected void comment(String content){
        out.printf("%% %s%n", content);
    }

    /**
     * add meta data.
     *
     * @param key
     * @param value
     */
    protected void meta(String key, String value){
        if (value != null){
            out.printf("%s%s=%s%n", FileLoader.META_HEADER, key, value);
        } else {
            out.printf("%s%s%n", FileLoader.META_HEADER, key);
        }
    }

    /**
     * add meta data. with formatted value.
     *
     * @param key
     * @param format
     * @param args   arguments for format expression
     * @see PrintStream#printf(String, Object...)
     */
    protected void metaf(String key, String format, Object... args){
        out.printf("%s%s=%s%n", FileLoader.META_HEADER, key, String.format(format, args));
    }

    /**
     * add key-value pair data
     *
     * @param key
     * @param value
     */
    protected void pair(String key, int value){
        out.printf("%s=%d%n", key, value);
    }

    /**
     * add key-value pair data
     *
     * @param key
     * @param value
     */
    protected void pair(String key, double value){
        out.printf("%s=%f%n", key, value);
    }

    /**
     * add key-value pair data
     *
     * @param key
     * @param value
     */
    protected void pair(String key, String value){
        out.printf("%s=%s%n", key, value);
    }

    /**
     * print a line.
     *
     * @param key
     */
    protected void line(String key){
        out.println(key);
    }

    /**
     * new line
     */
    protected void line(){
        out.println();
    }
}
