/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Text;

/**
 * @author antonio
 */
public abstract class PaintArea{

    PaintingPane pane;
    protected double x;
    protected double y;
    protected double width;
    protected double height;
    int layer;
    boolean visible = true;

    public PaintArea(){
    }

    public PaintArea(double x, double y, double w, double h){
        this.x = x;
        this.y = y;
        width = w;
        height = h;
    }

    public PaintArea(double w, double h){
        width = w;
        height = h;
    }

    void setPanel(PaintingPane pane){
        this.pane = pane;
    }

    public abstract void paint(GraphicsContext g);

    public boolean isVisible(){
        return visible;
    }

    public void setVisible(boolean visible){
        this.visible = visible;
    }

    public double getXOnPane(){
        return x;
    }

    public void setXOnPane(double x){
        this.x = x;
    }

    public double getYOnPane(){
        return y;
    }

    public void setYOnPane(double y){
        this.y = y;
    }

    public double getWidth(){
        return width;
    }

    public void setWidth(double width){
        this.width = width;
    }

    public double getPaneWidth(){
        return pane.getWidth();
    }

    public double getHeight(){
        return height;
    }

    public void setHeight(double height){
        this.height = height;
    }

    public double getPaneHeight(){
        return pane.getHeight();
    }

    public int getLayer(){
        return layer;
    }

    public void setLayer(int layer){
        this.layer = layer;
    }

    public static double getTextHeight(GraphicsContext g){
        Text t = new Text("test");
        t.setFont(g.getFont());
        return t.getLayoutBounds().getHeight();
    }

    public static double getTextWidth(GraphicsContext g, String text){
        Text t = new Text(text);
        t.setFont(g.getFont());
        return t.getLayoutBounds().getWidth();
    }
}
