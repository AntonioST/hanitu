/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.input.KeyCombination;

import cclo.hanitu.Base;

/**
 * @author antonio
 */
public class MenuSession{

    /**
     * menu `File` text
     */
    public static final String MENU_FILE;
    /**
     * menu `Edit` text
     */
    public static final String MENU_EDIT;
    /**
     * menu `View` text
     */
    public static final String MENU_VIEW;
    /**
     * menu `Help` text
     */
    public static final String MENU_HELP;

    static {
        Properties p = Base.loadPropertyInherit("gui");
        MENU_FILE = p.getProperty("menu.file", "File");
        MENU_EDIT = p.getProperty("menu.edit", "Edit");
        MENU_VIEW = p.getProperty("menu.view", "View");
        MENU_HELP = p.getProperty("menu.help", "Help");
    }

    private final MenuBar menuBar;

    public MenuSession(MenuBar menuBar, String... menuTitle){
        this.menuBar = menuBar;
        menuBar.setUseSystemMenuBar(true);
        //
        ObservableList<Menu> menus = menuBar.getMenus();
        for (String title : menuTitle){
            menus.add(createMenu(title));
        }
    }

    public void addMenuItems(String menuTitle, int insertGroupIndex, MenuItem... items){
        addMenuItems(menuTitle, insertGroupIndex, Arrays.asList(items));
    }

    /**
     * @param menuTitle        menu title
     * @param insertGroupIndex the index of the menu items block which spread by separator, can negative.
     * @param list             the list of the menu items.
     */
    public void addMenuItems(String menuTitle, int insertGroupIndex, List<MenuItem> list){
        ObservableList<Menu> menus = menuBar.getMenus();
        Menu menu = null;
        for (Menu m : menus){
            if (m.getText().equals(menuTitle)){
                menu = m;
            }
        }
        if (menu == null){
            menu = createMenu(menuTitle);
            menus.add(menus.size() - 1, menu);
        }

        int index = getRealMenuInsertIndex(menu, insertGroupIndex);
        ObservableList<MenuItem> items = menu.getItems();
        if (!items.isEmpty()){
            items.add(index, new SeparatorMenuItem());
        }
        if (index != 0){
            index++;
        }
        for (int i = 0, size = list.size(); i < size; i++){
            MenuItem item = list.get(i);
            if (item == null){
                items.add(index + i, new SeparatorMenuItem());
            } else {
                items.add(index + i, item);
            }
        }
    }

    private int getRealMenuInsertIndex(Menu menu, int insertGroupIndex){
        if (insertGroupIndex == 0) return 0;
        ObservableList<MenuItem> items = menu.getItems();
        int size = items.size();
        if (insertGroupIndex > 0){
            int ret = 0;
            for (int i = 0; i < size; i++){
                if ((items.get(i) instanceof SeparatorMenuItem) && ++ret == insertGroupIndex) return i - 1;
            }
        } else if (insertGroupIndex < -1){
            int ret = -1;
            for (int i = size - 1; i >= 0; i--){
                if ((items.get(i) instanceof SeparatorMenuItem) && --ret == insertGroupIndex) return i - 1;
            }
            return 0;
        }
        // insertGroupIndex == -1
        return size;
    }

    /**
     * @param menuTitle menu title
     * @param set       menu item names
     */
    public void removeMenuItems(String menuTitle, Collection<String> set){
        ObservableList<Menu> menus = menuBar.getMenus();
        Menu menu = null;
        for (Menu m : menus){
            if (m.getText().equals(menuTitle)){
                menu = m;
            }
        }
        if (menu == null){
            return;
        }
        ObservableList<MenuItem> items = menu.getItems();
        items.removeIf(t -> !(t instanceof SeparatorMenuItem) && set.contains(t.getText()));
        for (int i = items.size() - 1; i > 0; i--){
            if (items.get(i) instanceof SeparatorMenuItem){
                if (items.get(i - 1) instanceof SeparatorMenuItem){
                    items.remove(i);
                }
            }
        }
        while (!items.isEmpty() && (items.get(0) instanceof SeparatorMenuItem)){
            items.remove(0);
        }
    }

    /**
     * @param text
     * @param action
     * @return
     * @see #createMenuItem(String, KeyCombination, EventHandler<ActionEvent>)
     */
    public static MenuItem createMenuItem(String text, EventHandler<ActionEvent> action){
        return createMenuItem(text, (KeyCombination)null, action);
    }

    /**
     * @param text
     * @param shortcut keyboard shortcut, for {@link KeyCombination} value
     * @param action
     * @return
     * @see #createMenuItem(String, KeyCombination, EventHandler<ActionEvent>)
     */
    public static MenuItem createMenuItem(String text, String shortcut, EventHandler<ActionEvent> action){
        return createMenuItem(text, shortcut == null? null : KeyCombination.keyCombination(shortcut), action);
    }

    /**
     * @param text
     * @param shortcut keyboard shortcut
     * @param action
     * @return
     */
    public static MenuItem createMenuItem(String text, KeyCombination shortcut, EventHandler<ActionEvent> action){
        MenuItem ret = new MenuItem(text);
        ret.setMnemonicParsing(true);
        if (shortcut != null){
            ret.setAccelerator(shortcut);
        }
        if (action != null){
            ret.setOnAction(action);
        }
        return ret;
    }

    /**
     * @param text
     * @param getter
     * @param action
     * @return
     * @see #createCheckBoxMenuItem(String, KeyCombination, Supplier, BiConsumer)
     */
    public static CheckMenuItem createCheckBoxMenuItem(String text,
                                                       Supplier<Boolean> getter,
                                                       BiConsumer<ActionEvent, Boolean> action){
        return createCheckBoxMenuItem(text, (KeyCombination)null, getter, action);
    }

    /**
     * @param text
     * @param shortcut keyboard shortcut
     * @param getter
     * @param action
     * @return
     * @see #createCheckBoxMenuItem(String, KeyCombination, Supplier, BiConsumer)
     */
    public static CheckMenuItem createCheckBoxMenuItem(String text,
                                                       String shortcut,
                                                       Supplier<Boolean> getter,
                                                       BiConsumer<ActionEvent, Boolean> action){
        return createCheckBoxMenuItem(text, KeyCombination.keyCombination(shortcut), getter, action);
    }

    /**
     * @param text
     * @param shortcut keyboard shortcut
     * @param getter
     * @param action
     * @return
     */
    public static CheckMenuItem createCheckBoxMenuItem(String text,
                                                       KeyCombination shortcut,
                                                       Supplier<Boolean> getter,
                                                       BiConsumer<ActionEvent, Boolean> action){
        Objects.requireNonNull(getter);
        Objects.requireNonNull(action);
        CheckMenuItem item = new CheckMenuItem(text);
        item.setMnemonicParsing(true);
        if (shortcut != null){
            item.setAccelerator(shortcut);
        }
        item.setOnAction(e -> {
            action.accept(e, item.isSelected());
            item.setSelected(getter.get());
        });
        item.setSelected(getter.get());
        return item;
    }

    private static Menu createMenu(String text){
        Menu ret = new Menu(text);
        ret.setMnemonicParsing(true);
        return ret;
    }
}
