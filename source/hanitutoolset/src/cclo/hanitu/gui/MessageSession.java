/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.util.Objects;
import java.util.Properties;
import java.util.StringJoiner;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import cclo.hanitu.Base;
import cclo.hanitu.Log;
import cclo.hanitu.Message;

/**
 * @author antonio
 */
public class MessageSession{

    public static final int DEFAULT_OPTION = 0;
    public static final int NONE_OPTION = -1;
    public static final int YES_NO_OPTION = 3;
    public static final int YES_NO_CANCEL_OPTION = 7;
    public static final int OK_CANCEL_OPTION = 5;

    public static final int YES_OPTION = 1;
    public static final int NO_OPTION = 2;
    public static final int CANCEL_OPTION = 4;
    public static final int CLOSE_OPTION = 8;

    private static String YES_TEXT;
    private static String NO_TEXT;
    private static String CANCEL_TEXT;
    private static String CLOSE_TEXT;
    private static String OK_TEXT;
    private static String ERROR_TEXT;
    private static String MESSAGE_TEXT;

    static{
        Properties p = Base.loadPropertyInherit("gui");
        YES_TEXT = p.getProperty("message.control.yes");
        NO_TEXT = p.getProperty("message.control.no");
        CANCEL_TEXT = p.getProperty("message.control.cancel");
        CLOSE_TEXT = p.getProperty("message.control.close");
        OK_TEXT = p.getProperty("message.control.ok");
        ERROR_TEXT = p.getProperty("message.error.title");
        MESSAGE_TEXT = p.getProperty("message.message.title");
    }

    private MessageSession(){
        throw new RuntimeException();
    }

    private static class CallBack<T>{

        T returnAction;
    }

    public static void showMessageDialog(String title, String message){
        Stage stage = new Stage(StageStyle.UTILITY);
        stage.setIconified(false);
        stage.setMaximized(false);
        stage.setTitle(title);
        stage.setMinWidth(200);
        stage.setMinHeight(100);
        //
        VBox root = initTextRegion(message);
        root.setOnKeyPressed(e -> {
            switch (e.getCode()){
            case ENTER:
            case ESCAPE:
                stage.close();
                break;
            }
        });
        Insets value = new Insets(15);
        root.getChildren().forEach(c -> VBox.setMargin(c, value));
        //
        Scene scene = new Scene(root);
        scene.setOnMouseClicked(e -> stage.close());
        stage.setScene(scene);
        stage.sizeToScene();
        //
        stage.show();
        root.requestFocus();
    }

    public static int showConfirmDialog(String title, String message, int option){
        CallBack<Integer> session = new CallBack<>();
        session.returnAction = CLOSE_OPTION;
        //
        Stage stage = new Stage(StageStyle.UTILITY);
        stage.setTitle(title);
        stage.setMinWidth(400);
        stage.setMinHeight(100);
        //
        VBox textRegion = initTextRegion(message);
        //
        BiConsumer<ActionEvent, Integer> action = (e, code) -> {
            session.returnAction = code;
            stage.close();
        };
        HBox controlRegion = initControlRegion(option, action);
        //
        textRegion.getChildren().addAll(controlRegion);
        Insets value = new Insets(15);
        textRegion.getChildren().forEach(c -> VBox.setMargin(c, value));
        //
        Scene scene = new Scene(textRegion);
        scene.setOnKeyPressed(e -> {
            switch (e.getCode()){
            case ENTER:
                controlRegion.getChildren().stream()
                  .filter(Node::isFocused)
                  .findFirst()
                  .ifPresent(child -> {
                      Event.fireEvent(child, new ActionEvent(child, child));
                  });
                break;
            case ESCAPE:
                if (option == YES_NO_CANCEL_OPTION || option == OK_CANCEL_OPTION){
                    action.accept(null, CANCEL_OPTION);
                } else {
                    action.accept(null, CLOSE_OPTION);
                }
                break;
            }
        });
        //
        stage.setScene(scene);
        //
        stage.requestFocus();
        stage.showAndWait();
        return session.returnAction;
    }

    public static String showInputDialog(String title, String message){
        return showInputDialog(title, message, input -> null);
    }

    public static String showInputDialog(String title, String message, Pattern pattern){
        Matcher m = pattern.matcher("");
        return showInputDialog(title, message, text -> {
            if (m.reset(text).matches()){
                return null;
            } else {
                return "illegal format";
            }
        });
    }

    public static String showInputDialog(String title, String message, Function<String, String> errorFunc){
        Objects.requireNonNull(errorFunc);
        //
        CallBack<String> session = new CallBack<>();
        Stage stage = new Stage(StageStyle.UTILITY);
        stage.setTitle(title);
        stage.setMinWidth(200);
        stage.setMinHeight(100);
        //
        TextField field = new TextField();
        Tooltip tooltip = new Tooltip();
        field.textProperty().addListener((observable, oldValue, newValue) -> {
            String error = errorFunc.apply(newValue);
            if (error == null){
                field.setTooltip(null);
                FXUIField.setBackground(field, Color.WHITE);
            } else {
                tooltip.setText(error);
                field.setTooltip(tooltip);
                FXUIField.setBackground(field, Color.PINK);
            }
        });
        //
        BiConsumer<ActionEvent, Integer> action = (e, code) -> {
            if (code == YES_OPTION){
                String text = field.getText();
                String error = errorFunc.apply(text);
                if (error == null){
                    session.returnAction = text;
                } else {
                    showErrorMessageDialog(error);
                    return;
                }
            }
            stage.close();
        };
        field.setOnAction(e -> action.accept(e, YES_OPTION));
        field.setOnKeyPressed(e -> {
            switch (e.getCode()){
            case ESCAPE:
                action.accept(null, CLOSE_OPTION);
                break;
            }
        });
        HBox controlRegion = initControlRegion(YES_NO_OPTION, action);
        //
        VBox textRegion = initTextRegion(message);
        textRegion.getChildren().addAll(field, controlRegion);
        Insets value = new Insets(15);
        textRegion.getChildren().forEach(c -> VBox.setMargin(c, value));
        //
        Scene scene = new Scene(textRegion);
        stage.setScene(scene);
        stage.requestFocus();
        stage.showAndWait();
        return session.returnAction;
    }

    public static void showTextDialog(String title, String message, String content){
        Stage stage = new Stage(StageStyle.UTILITY);
        stage.setIconified(false);
        stage.setMaximized(false);
        stage.setTitle(title);
        stage.setMinWidth(200);
        stage.setMinHeight(200);
        //
        VBox root = initTextRegion(message);
        TextArea text = new TextArea(content);
        text.setEditable(false);
        HBox control = initControlRegion(CLOSE_OPTION, (actionEvent, integer) -> stage.close());
        root.getChildren().addAll(text, control);
        root.setOnKeyPressed(e -> {
            switch (e.getCode()){
            case ESCAPE:
                stage.close();
                break;
            }
        });
        Insets value = new Insets(15);
        root.getChildren().forEach(c -> VBox.setMargin(c, value));
        //
        Scene scene = new Scene(root);
        scene.setOnMouseClicked(e -> stage.close());
        stage.setScene(scene);
        stage.sizeToScene();
        //
        stage.show();
        root.requestFocus();
    }

    public static HBox initControlRegion(int option, BiConsumer<ActionEvent, Integer> action){
        HBox control = new HBox();
        control.setAlignment(Pos.BOTTOM_RIGHT);
        control.setSpacing(5);
        Button button;
        switch (option){
        case DEFAULT_OPTION:{
            button = new Button(YES_TEXT);
            button.setMinWidth(70);
            button.setOnAction(e -> action.accept(e, YES_OPTION));
            control.getChildren().add(button);
            break;
        }
        case YES_NO_OPTION:{
            button = new Button(NO_TEXT);
            button.setMinWidth(70);
            button.setOnAction(e -> action.accept(e, NO_OPTION));
            control.getChildren().add(button);
            button = new Button(YES_TEXT);
            button.setMinWidth(70);
            button.setOnAction(e -> action.accept(e, YES_OPTION));
            control.getChildren().add(button);
            break;
        }
        case YES_NO_CANCEL_OPTION:{
            button = new Button(CANCEL_TEXT);
            button.setMinWidth(70);
            button.setOnAction(e -> action.accept(e, CANCEL_OPTION));
            control.getChildren().add(button);
            button = new Button(NO_TEXT);
            button.setMinWidth(70);
            button.setOnAction(e -> action.accept(e, NO_OPTION));
            control.getChildren().add(button);
            button = new Button(YES_TEXT);
            button.setMinWidth(70);
            button.setOnAction(e -> action.accept(e, YES_OPTION));
            control.getChildren().add(button);
            break;
        }
        case OK_CANCEL_OPTION:{
            button = new Button(CANCEL_TEXT);
            button.setMinWidth(70);
            button.setOnAction(e -> action.accept(e, CANCEL_OPTION));
            control.getChildren().add(button);
            button = new Button(OK_TEXT);
            button.setMinWidth(70);
            button.setOnAction(e -> action.accept(e, YES_OPTION));
            control.getChildren().add(button);
            break;
        }
        case CLOSE_OPTION:{
            button = new Button(CLOSE_TEXT);
            button.setMinWidth(70);
            button.setOnAction(e -> action.accept(e, CLOSE_OPTION));
            control.getChildren().add(button);
            break;
        }
        case NONE_OPTION:
            break;
        default:
            throw new IllegalArgumentException("unknown option : " + option);
        }
        return control;
    }

    public static VBox initTextRegion(String message){
        VBox textRegion = new VBox();
        for (String line : message.split("\n")){
            TextFlow text = RichMessage.mapTextExpression(line);
            text.setTextAlignment(TextAlignment.JUSTIFY);
            textRegion.getChildren().add(text);
        }
        textRegion.setAlignment(Pos.TOP_CENTER);
        textRegion.setFillWidth(true);
        return textRegion;
    }

    /**
     * show a error information dialog
     *
     * @param message
     */
    public static void showErrorMessageDialog(String... message){
        if (Log.DEBUG){
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.debug(p -> {
                p.print(Message.echo("~*y{message~} : "));
                for (String line : message){
                    p.println("  " + line);
                }
                p.println(Message.echo("~*y{trace~} : "));
                Log.trace(p, st, 2, st.length, 0);
            });
        }
        StringJoiner sj = new StringJoiner("\n");
        for (String s : message){
            sj.add(s);
        }
        showMessageDialog(ERROR_TEXT, sj.toString());
    }

    /**
     * show a error information dialog
     *
     * @param ex
     */
    public static void showErrorMessageDialog(Throwable ex){
        Log.debug(ex);
        StringBuilder sb = new StringBuilder();
        for (Throwable e = ex; e != null; e = e.getCause()){
            sb.append(e.getClass()).append("\n");
            sb.append(e.getMessage()).append("\n");
        }
        showTextDialog(ERROR_TEXT, ex.getClass().getName(), sb.toString());
    }

    /**
     * show a error information dialog
     *
     * @param message
     * @param ex
     */
    public static void showErrorMessageDialog(String message, Throwable ex){
        Log.debug(message, ex);
        StringBuilder sb = new StringBuilder();
        sb.append(message).append("\n");
        for (Throwable e = ex; e != null; e = e.getCause()){
            sb.append(e.getClass()).append("\n");
            sb.append(e.getMessage()).append("\n");
        }
        showTextDialog(ERROR_TEXT, ex.getClass().getName(), sb.toString());
    }

    /**
     * show a information dialog.
     *
     * @param message
     */
    public static void showDebugMessageDialog(String... message){
        if (Log.DEBUG){
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.debug(p -> {
                p.print(Message.echo("~*y{message~} : "));
                for (String line : message){
                    p.println("  " + line);
                }
                p.println(Message.echo("~*y{trace~} : "));
                Log.trace(p, st, 2, st.length, 0);
            });
        }
        StringJoiner sj = new StringJoiner("\n");
        for (String s : message){
            sj.add(s);
        }
        showMessageDialog(MESSAGE_TEXT, sj.toString());
    }


    /**
     * show a dialog and tell user this feature doesn't implement.
     */
    public static void notImplementYet(){
        StackTraceElement st = Thread.currentThread().getStackTrace()[3];
        Log.debug(() -> "fixme:" + st.getClassName() + "." + st.getMethodName());
        Properties p = Base.loadPropertyInherit("gui");
        showMessageDialog(p.getProperty("message.notimplement.title"),
                          p.getProperty("message.notimplement.content"));
    }
}
