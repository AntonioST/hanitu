/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

import cclo.hanitu.Base;
import cclo.hanitu.Log;
import cclo.hanitu.util.ErrorFunction;


/**
 * @author antonio
 */
public class FileSession{

    private final Path defaultWorkDirectory = Paths.get(System.getProperty("user.dir")).toAbsolutePath();
    private Path currentWorkDirectory = defaultWorkDirectory;

    {
        Log.debug(() -> "default pwd : " + defaultWorkDirectory);
    }

    private Stage stage;
    private String defaultSaveFileName;
    private String defaultFileExtName;
    private final List<ExtensionFilter> fileFilters = new ArrayList<>();
    private boolean currentFileModified;
    private ExtensionFilter selectedExt;
    /**
     * current file saving path.
     *
     * **Notice** : make sure assign it with absolute path
     */
    private Path currentFilePath;

    private Runnable newFileEvent;
    private ErrorFunction.EConsumer<Path, IOException> openFileEvent;
    private ErrorFunction.EConsumer<Path, IOException> saveFileEvent;
    private Runnable updateEvent;
    private Runnable quitEvent;

    public void setStage(Stage host){
        stage = host;
    }

    public void setOnNewFile(Runnable r){
        newFileEvent = r;
    }

    public void setOnOpenFile(ErrorFunction.EConsumer<Path, IOException> e){
        openFileEvent = e;
    }

    public void setOnSaveFile(ErrorFunction.EConsumer<Path, IOException> e){
        saveFileEvent = e;
    }

    public void setOnUpdate(Runnable r){
        updateEvent = r;
    }

    public void setOnQuit(Runnable r){
        quitEvent = r;
    }

    protected void newFile(){
        if (newFileEvent != null){
            newFileEvent.run();
        }
    }

    protected void openFile(Path path) throws IOException{
        if (openFileEvent != null){
            openFileEvent.accept(path);
        }
    }

    protected void saveFile(Path path) throws IOException{
        if (saveFileEvent != null){
            saveFileEvent.accept(path);
        }
    }

    protected void update(){
        if (updateEvent != null){
            updateEvent.run();
        }
    }

    protected void quit(){
        if (quitEvent != null){
            quitEvent.run();
        }
    }


    /**
     * @return
     */
    public boolean isCurrentFileModified(){
        return currentFileModified;
    }

    /**
     * @param isCurrentFileModified
     */
    public void setCurrentFileModified(boolean isCurrentFileModified){
        this.currentFileModified = isCurrentFileModified;
        update();
    }

    /**
     * current working directory
     *
     * @return directory path, null if current file not associate with any file.
     */
    public Path getCurrentWorkDirectory(){
        return currentWorkDirectory;
    }

    /**
     * get the file saving path relative to current working directory.
     *
     * @return file saving path, may null
     */
    public Path getCurrentFilePath(){
        if (currentWorkDirectory == null){
            currentWorkDirectory = defaultWorkDirectory;
        }
        if (currentFilePath == null) return null;
        if (currentFilePath.startsWith(currentWorkDirectory)){
            return currentWorkDirectory.relativize(currentFilePath);
        }
        return currentFilePath;
    }

    /**
     * set the file saving path.
     *
     * @param currentFilePath path, null for dis-associating file and reset to default.
     */
    public void setCurrentFilePath(Path currentFilePath){
        if (currentFilePath != null){
            this.currentFilePath = currentFilePath.toAbsolutePath().normalize();
            currentWorkDirectory = currentFilePath.getParent();
        } else {
            this.currentFilePath = null;
            currentWorkDirectory = defaultWorkDirectory;
        }
        update();
    }

    /**
     * get the default file name
     *
     * @return
     */
    public String getDefaultSaveFileName(){
        return defaultSaveFileName;
    }

    /**
     * set the default file name
     *
     * @param defaultSaveFileName
     */
    public void setDefaultSaveFileName(String defaultSaveFileName){
        this.defaultSaveFileName = defaultSaveFileName;
        if (defaultFileExtName == null && defaultSaveFileName.contains(".")){
            defaultFileExtName = defaultSaveFileName.substring(defaultSaveFileName.lastIndexOf(".") + 1);
        }
    }

    public String getDefaultFileExtName(){
        return defaultFileExtName;
    }

    public void setDefaultFileExtName(String defaultFileExtName){
        this.defaultFileExtName = defaultFileExtName;
    }

    public void setFileFilter(ExtensionFilter... ff){
        fileFilters.clear();
        if (ff.length > 0){
            fileFilters.addAll(Arrays.asList(ff));
        }
    }

    public ExtensionFilter getSelectedExtension(){
        return selectedExt;
    }

    /**
     * new a empty, clear session.
     *
     * If current file is modify, will ask user save it or not?
     */
    public void newEmptyFile(){
        Log.debug("new a empty file");
        if (askSaveCurrentFile()){
            newFile();
        }
        currentFilePath = null;
        currentWorkDirectory = defaultWorkDirectory;
        currentFileModified = false;
        update();
    }


    /**
     * load file and start a new session
     *
     * If current file is modify, will ask user save it or not?
     */
    public void openFile(){
        if (!askSaveCurrentFile()) return;

        FileChooser jfc = new FileChooser();
        jfc.setInitialDirectory(currentWorkDirectory.toFile());
        if (!fileFilters.isEmpty()){
            jfc.getExtensionFilters().addAll(fileFilters);
        }
        File file = jfc.showOpenDialog(stage);
        if (file == null) return;
        String fileName = file.getAbsolutePath();
        try {
            currentFilePath = Paths.get(fileName).toAbsolutePath();
            currentWorkDirectory = currentFilePath.getParent();
            Log.debug("open file : " + currentFilePath);
            openFile(currentFilePath);
            currentFileModified = false;
        } catch (IOException | RuntimeException ex){
            MessageSession.showErrorMessageDialog("cannot load from file : " + fileName, ex);
        }
        update();
    }

    /**
     * ask user save file or not
     *
     * @param saveNewFile save as a new file?
     */
    public boolean trySaveCurrentFile(boolean saveNewFile){
        if (saveNewFile || currentFilePath == null){
            setCurrentFileModified(true);
            if (askSaveFile()){
                doSavingFile();
            } else {
                return false;
            }
        } else {
            doSavingFile();
        }
        return true;
    }

    /**
     * ask user save modify to file?
     *
     * The panel has four choose, there are yes, no, cancel and close (the dialog). Save file only when user choose yes.
     * Do Nothing when choosing others.
     *
     * @return true if this action doesn't be interrupted.
     */
    public boolean askSaveCurrentFile(){
        if (!currentFileModified) return true;
        Properties p = Base.loadPropertyInherit("gui");
        int choose = MessageSession.showConfirmDialog(p.getProperty("message.savewhenexit.title"),
                                                      p.getProperty("message.savewhenexit.content"),
                                                      MessageSession.YES_NO_CANCEL_OPTION);
        if (choose == MessageSession.YES_OPTION){
            return trySaveCurrentFile(false);
        }
        return choose == MessageSession.NO_OPTION;
    }

    private boolean askSaveFile(){
        FileChooser jfc = new FileChooser();
        if (currentWorkDirectory == null){
            jfc.setInitialDirectory(defaultWorkDirectory.toFile());
        } else {
            jfc.setInitialDirectory(currentWorkDirectory.toFile());
            if (currentFilePath != null){
                jfc.setInitialFileName(currentFilePath.getFileName().toString());
            } else if (defaultSaveFileName != null){
                jfc.setInitialFileName(defaultSaveFileName);
            }
        }
        if (!fileFilters.isEmpty()){
            jfc.getExtensionFilters().addAll(fileFilters);
        }
        File file = jfc.showSaveDialog(stage);
        if (file == null) return false;
        Path newFilePath = file.getAbsoluteFile().toPath();
        selectedExt = jfc.getSelectedExtensionFilter();
        if (selectedExt != HanituFileFilters.ALL_FILTER){
            String filename = newFilePath.getFileName().toString();
            if (selectedExt == null || !filename.endsWith(defaultFileExtName)){
                newFilePath = newFilePath.getParent().resolve(filename + "." + defaultFileExtName);
            } else if (selectedExt.getExtensions().stream().map(e -> e.substring(1)).noneMatch(filename::endsWith)){
                newFilePath = newFilePath.getParent()
                  .resolve(filename + selectedExt.getExtensions().get(0).substring(1));
            }
        }
        currentFilePath = newFilePath;
        return true;
    }

    /**
     * show saving file dialog.
     *
     * @return true if this action doesn't be interrupted.
     */
    public boolean doSavingFile(){
        if (currentFilePath == null) return false;
        try {
            saveFile(currentFilePath);
            currentFileModified = false;
            currentWorkDirectory = currentFilePath.getParent();
        } catch (IOException | RuntimeException e){
            Properties p = Base.loadPropertyInherit("gui");
            MessageSession.showErrorMessageDialog(
              String.format(p.getProperty("message.saveerror.content"), currentFilePath), e);
            return false;
        }
        update();
        return true;
    }

    /**
     * saving file to path
     *
     * ### saving file step
     * 1. open file stream to `savePath.tmp`
     * 2. write content to `savePath.tmp`
     * 3. delete `savePath`
     * 4. rename `savePath.tmp` to `savePath`
     *
     * @param savePath
     * @param action
     * @return success or not
     * @throws IOException
     */
    public static void saveFileByTempReplace(Path savePath,
                                             ErrorFunction.EConsumer<OutputStream, IOException> action)
      throws IOException{
        Path tmp = savePath.getParent().resolve(savePath.getFileName().toString() + ".tmp");
        try (OutputStream os = Files.newOutputStream(tmp)) {
            action.accept(os);
            Files.deleteIfExists(savePath);
            Files.move(tmp, savePath);
        } finally {
            Files.deleteIfExists(tmp);
        }
    }

    /**
     * quit
     *
     * If current file is modify, will ask user save it or not?
     */
    public void askQuit(){
        if (askSaveCurrentFile()){
            Log.debug(() -> "quit : " + stage.getTitle());
            quit();
        }
    }

    /**
     * register action to menu.
     *
     * ### menu
     *
     * name     | shortcut  | action
     * ----     | --------  | ------
     * New      | ctrl + N  | {@link #newEmptyFile()}
     * Open     | ctrl + O  | {@link #openFile()}
     * S ave    | ctrl + S  | {@link #trySaveCurrentFile(boolean)} with false
     * Save as  | ctrl + shift + S | {@link #trySaveCurrentFile(boolean)} with true
     * Exit     |           | {@link #quit()}
     *
     */
    public List<MenuItem> getMenuList(){
        Properties p = Base.loadPropertyInherit("gui");
        return Arrays.asList(
          MenuSession.createMenuItem(p.getProperty("menu.file.new"), "Shortcut+N", e -> newEmptyFile()),
          MenuSession.createMenuItem(p.getProperty("menu.file.open"), "Shortcut+O", e -> openFile()),
          null,
          MenuSession.createMenuItem(p.getProperty("menu.file.save"), "Shortcut+S", e -> trySaveCurrentFile(false)),
          MenuSession.createMenuItem(p.getProperty("menu.file.saveas"), "Shortcut+Shift+S",
                                     e -> trySaveCurrentFile(true)),
          null,
          MenuSession.createMenuItem(p.getProperty("menu.file.exit"), e -> askQuit()));
    }
}
