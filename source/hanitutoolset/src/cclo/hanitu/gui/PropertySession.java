/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import cclo.hanitu.FieldInfo;
import cclo.hanitu.util.Events.RunnableEvent;

/**
 * manage property setting panel.
 *
 * @author antonio
 */
public class PropertySession{

    private final GridPane layout = new GridPane();
    protected final List<FieldInfo> fields = new ArrayList<>();
    protected final List<Control> components = new ArrayList<>();
    public final RunnableEvent modifyEvent = new RunnableEvent();
    public final RunnableEvent updateEvent = new RunnableEvent();
    public final RunnableEvent updateFieldEvent = new RunnableEvent();
    public Font labelFont = Font.font("FreeMono", FontWeight.BOLD, 16);
    public Font textFont = Font.font("FreeMono", 16);
    private boolean editable = true;

    {
        layout.setHgap(5);
        layout.setVgap(5);
        ColumnConstraints c0 = new ColumnConstraints();
        ColumnConstraints c1 = new ColumnConstraints();
        ColumnConstraints c2 = new ColumnConstraints();
//        c0.setMinWidth(100);
//        c0.setPrefWidth(200);
        c1.setMinWidth(200);
        c1.setHgrow(Priority.SOMETIMES);
//        c2.setMinWidth(50);
        layout.getColumnConstraints().addAll(c0, c1, c2);
    }

    public Control getField(int index){
        return components.get(index);
    }

    public GridPane getLayout(){
        return layout;
    }


    public void setEditable(boolean edit){
        editable = edit;
        components.forEach(c -> {
            if (c instanceof TextInputControl){
                ((TextInputControl)c).setEditable(edit);
            } else if (c instanceof ComboBox){
                ((ComboBox)c).setEditable(edit);
            } else {
                c.setDisable(!edit);
            }
        });
        if (edit){
            updateEvent.run();
        }
    }

    public void reset(List<FieldInfo> fields){
        clear();
        if (fields == null || fields.isEmpty()) return;
        this.fields.addAll(fields);
        //setting
        addInfo(fields);
        layout.requestLayout();
        //
        updateFieldEvent.run();
        updateEvent.run();
    }

    protected void clear(){
        layout.getChildren().clear();
        fields.clear();
        components.clear();
        updateFieldEvent.clear();
        updateEvent.run();
    }

    protected void addInfo(List<FieldInfo> fields){
        Map<String, HBox> groups = new HashMap<>();
        for (FieldInfo info : fields){
            String group = null;
            Label leftSideLabel = null;
            if (info.group != null){
                group = info.group;
                if (!groups.containsKey(info.group)){
                    leftSideLabel = new Label(info.group);
                }
            } else if (!(info instanceof FieldInfo.ActionFieldInfo)){
                group = info.name;
                leftSideLabel = new Label(info.name);
                if (info.tip != null){
                    leftSideLabel.setTooltip(new Tooltip(info.tip));
                }
            }
            if (leftSideLabel != null){
                leftSideLabel.setFont(labelFont);
                layout.add(leftSideLabel, 0, groups.size());
            }
            //
            HBox box = groups.computeIfAbsent(group, g -> {
                HBox ret = new HBox(5);
                ret.setAlignment(Pos.BASELINE_LEFT);
                layout.add(ret, 1, groups.size());
                GridPane.setFillWidth(ret, true);
                return ret;
            });
            Control target = getUI(info);
            if (info.group != null){
                if (info.simpleName != null){
                    Label simple = new Label(info.simpleName);
                    simple.setFont(textFont);
                    box.getChildren().add(simple);
                    if (info.tip != null){
                        simple.setTooltip(new Tooltip(info.tip));
                    }
                }
                box.getChildren().add(target);
                target.setPrefWidth(50);
                HBox.setHgrow(target, Priority.ALWAYS);
            } else {
                HBox.setHgrow(target, Priority.ALWAYS);
                box.getChildren().add(target);
            }
            //
            if ((info instanceof FieldInfo.ValueFieldInfo)){
                FieldInfo.ValueFieldInfo value = (FieldInfo.ValueFieldInfo)info;
                if (value.unit != null){
                    Label unit = new Label(value.unit);
                    unit.setFont(textFont);
                    layout.add(unit, 3, groups.size() - 1);
                    GridPane.setHalignment(unit, HPos.LEFT);
                }
            }
        }
    }

    protected Control getUI(FieldInfo info){
        FXUIField<? extends Control> field = FXUIField.create(info);
        Control ui = field.ui;
        if (ui instanceof TextField){
            ((TextField)ui).setOnAction(warpAction(field::action));
        } else if (ui instanceof Button){
            if (info instanceof FieldInfo.ActionFieldInfo){
                ((Button)ui).setOnAction(warpAction(e -> {
                    field.action(e);
                    updateFieldEvent.run();
                }));
            } else {
                ((Button)ui).setOnAction(warpAction(field::action));
            }
        } else if (ui instanceof ComboBoxBase){
            ((ComboBoxBase)ui).setOnAction(warpAction(field::action));
        }
        updateFieldEvent.add(field::update);
        components.add(ui);
        return ui;
    }

    private <E extends Event> EventHandler<E> warpAction(EventHandler<E> action){
        return e -> {
            try {
                if (editable){
                    action.handle(e);
                    modifyEvent.run();
                    updateEvent.run();
                }
            } catch (RuntimeException ex){
                MessageSession.showErrorMessageDialog(ex);
            }
        };
    }

    public void update(){
        updateFieldEvent.run();
        modifyEvent.run();
        updateEvent.run();
    }
}
