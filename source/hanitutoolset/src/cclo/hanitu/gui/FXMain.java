/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.util.concurrent.atomic.AtomicBoolean;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import cclo.hanitu.Log;

/**
 * @author antonio
 */
public class FXMain{

    private FXMain(){
        throw new RuntimeException();
    }

    public interface FXSimpleApplication{

        void start(Stage stage) throws Exception;

        default Stage start() throws Exception{
            Stage stage = new Stage();
            start(stage);
            return stage;
        }
    }

    public interface FXApplication{

        void start(Application app, Stage stage) throws Exception;

        default void init(Application app) throws Exception{
        }

        default void stop(Application app) throws Exception{
        }
    }

    public static class FXFirstApplication extends Application{

        static FXApplication instance;

        @Override
        public void init() throws Exception{
            if (instance != null){
                instance.init(this);
            }
        }

        @Override
        public void start(Stage primaryStage) throws Exception{
            if (instance != null){
                instance.start(this, primaryStage);
            }
        }

        @Override
        public void stop() throws Exception{
            if (instance != null){
                instance.stop(this);
            }
        }
    }

    public static void launch(FXApplication app){
        Log.debug(() -> "Fx launch : " + app);
        if (FXFirstApplication.instance == null){
            FXFirstApplication.instance = app;
            Application.launch(FXFirstApplication.class);
        } else {
            throw new IllegalStateException("Application launch must not be called more than once");
        }
    }

    public static <A extends FXSimpleApplication> void launch(A application){
        Log.debug(() -> "Fx launch : " + application);
        if (FXFirstApplication.instance == null){
            launch((FXApplication)(app, stage) -> application.start(stage));
        } else {
            final AtomicBoolean escape = new AtomicBoolean(true);
            Platform.runLater(() -> {
                try {
                    Stage stage = application.start();
                    EventHandler<WindowEvent> onCloseRequest = stage.getOnCloseRequest();
                    stage.setOnCloseRequest(e -> {
                        onCloseRequest.handle(e);
                        synchronized (escape){
                            escape.set(false);
                            escape.notifyAll();
                        }
                    });
                } catch (Exception e){
                    throw new RuntimeException(e);
                }
            });
            //make sure this method is a blocking method
            //block current thread until stage closed
            while (escape.get()){
                synchronized (escape){
                    try {
                        escape.wait();
                    } catch (InterruptedException e){
                    }
                }
            }
        }
    }
}
