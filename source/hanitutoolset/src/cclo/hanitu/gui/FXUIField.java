/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.awt.BasicStroke;
import java.util.List;
import java.util.function.Function;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;

import cclo.hanitu.FieldInfo;

import static cclo.hanitu.FieldInfo.*;

/**
 * @author antonio
 */
public abstract class FXUIField<U extends Control>{

    public final FieldInfo field;
    public final U ui;

    private FXUIField(FieldInfo field, U ui){
        this.field = field;
        this.ui = ui;
    }

    public abstract void update();

    public abstract void action(ActionEvent e);

    public static FXUIField<? extends Control> create(FieldInfo info){
        if (info instanceof TextFieldInfo){
            return create((TextFieldInfo)info);
        } else if (info instanceof ValueFieldInfo){
            return create((ValueFieldInfo)info);
        } else if (info instanceof BooleanFieldInfo){
            return create((BooleanFieldInfo)info);
        } else if (info instanceof ArrayFieldInfo){
            return create((ArrayFieldInfo)info);
        } else if (info instanceof ActionFieldInfo){
            return create((ActionFieldInfo)info);
        } else if (info instanceof UIFieldField){
            return create((UIFieldField)info);
        } else {
            throw new IllegalArgumentException("unsupported Field type : " + info.getClass());
        }
    }

    public static FXUIField<TextField> create(TextFieldInfo info){
        TextField text = new TextField();
        setOnFocusUpdateEvent(text);
        return new FXUIField<TextField>(info, text){
            @Override
            public void update(){
                text.setText(info.get());
            }

            @Override
            public void action(ActionEvent e){
                info.set(text.getText());
            }
        };
    }

    private static void setOnFocusUpdateEvent(TextField text){
        text.focusedProperty().addListener(new ChangeListener<Boolean>(){
            String contentCache;

            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue){
                if (newValue){ //focusGained
                    contentCache = text.getText();
                } else {
                    String newContent = text.getText();
                    if (contentCache == null || !contentCache.equals(newContent)){
                        Event.fireEvent(text, new ActionEvent(text, text));
                    }
                }

            }
        });
    }

    public static FXUIField<TextField> create(ValueFieldInfo info){
        TextField text = new TextField();
        setOnFocusUpdateEvent(text);
        if (info.type == int.class){
            text.textProperty().addListener(intTextFormatterChecker(text, "wrong number format"));
        } else {
            text.textProperty().addListener(doubleTextFormatterChecker(text, "wrong number format"));
        }
        return new FXUIField<TextField>(info, text){
            @Override
            public void update(){
                text.setText(info.get());
            }

            @Override
            public void action(ActionEvent e){
                info.set(text.getText());
                text.setText(info.get());
            }
        };
    }

    public static ChangeListener<String> intTextFormatterChecker(TextField text, String message){
        Tooltip wrongFormatToolTip = new Tooltip(message);
        return (observable, oldValue, newValue) -> {
            try {
                Integer.parseInt(newValue);
                FXUIField.setBackground(text, Color.WHITE);
                text.setTooltip(null);
            } catch (NumberFormatException e1){
                FXUIField.setBackground(text, Color.PINK);
                text.setTooltip(wrongFormatToolTip);
            }
        };
    }

    public static ChangeListener<String> doubleTextFormatterChecker(TextField text, String message){
        Tooltip wrongFormatToolTip = new Tooltip(message);
        return (observable, oldValue, newValue) -> {
            try {
                Double.parseDouble(newValue);
                FXUIField.setBackground(text, Color.WHITE);
                text.setTooltip(null);
            } catch (NumberFormatException e1){
                FXUIField.setBackground(text, Color.PINK);
                text.setTooltip(wrongFormatToolTip);
            }
        };
    }

    public static ChangeListener<String> textFormatterChecker(TextField text, Function<String, String> test){
        Tooltip wrongFormatToolTip = new Tooltip("");
        return (observable, oldValue, newValue) -> {
            String error = test.apply(newValue);
            if (error != null){
                FXUIField.setBackground(text, Color.PINK);
                wrongFormatToolTip.setText(error);
                text.setTooltip(wrongFormatToolTip);
            } else {
                FXUIField.setBackground(text, Color.WHITE);
                text.setTooltip(null);
            }
        };
    }

    public static FXUIField<Button> create(BooleanFieldInfo info){
        Button button = new Button();
        button.setMinWidth(100);
        return new FXUIField<Button>(info, button){
            @Override
            public void update(){
                button.setText(info.get());
            }

            @Override
            public void action(ActionEvent e){
                info.inverseSet(button.getText());
                button.setText(info.get());
            }
        };
    }

    public static FXUIField<ComboBox<String>> create(ArrayFieldInfo info){
        ComboBox<String> box = new ComboBox<>();
        box.getItems().addAll(info.set);
        box.setEditable(false);
        box.setMinWidth(100);
        return new FXUIField<ComboBox<String>>(info, box){
            @Override
            public void update(){
                box.getSelectionModel().select(info.getWithType());
            }

            @Override
            public void action(ActionEvent e){
                info.setWithType(box.getSelectionModel().getSelectedIndex());
            }
        };
    }

    public static FXUIField<Button> create(ActionFieldInfo info){
        Button button = new Button(info.name);
        if (info.tip != null){
            button.setTooltip(new Tooltip(info.tip));
        }
        return new FXUIField<Button>(info, button){
            @Override
            public void update(){
            }

            @Override
            public void action(ActionEvent e){
                info.fire();
            }
        };
    }

    public static FXUIField<? extends Control> create(UIFieldField info){
        switch (info.uiType){
        case DEFAULT:
            return createUI(info, info.primaryType);
        case COLOR:
            return createColorUI(info, info.primaryType);
        case STROKE_WIDTH:
            return createLineWidthUI(info, info.primaryType);
        case FONT:
            return createFontUI(info, info.primaryType);
        default:
            //XXX Unsupported Operation FXUIField.create
            throw new UnsupportedOperationException(info.uiType.name());
        }
    }

    public static FXUIField<? extends Control> createUI(UIFieldField info, Class type){
        if (type == Color.class || type == java.awt.Color.class){
            return createColorUI(info, type);
        } else if (type == Font.class || type == java.awt.Font.class){
            return createFontUI(info, type);
        } else if (type == BasicStroke.class){
            return createLineWidthUI(info, type);
        } else {
            throw new IllegalArgumentException("unsupported UI type by DEFAULT");
        }
    }

    public static FXUIField<ColorPicker> createColorUI(UIFieldField info, Class type){
        FieldInfo.ReflectFieldInfo<Color> colorInfo;
        if (type == int.class){
            colorInfo = new RGBColorFieldInfo(info);
        } else if (type == Color.class){
            colorInfo = new ColorFieldInfo(info);
        } else if (type == String.class){
            colorInfo = new ColorLiteralFieldInfo(info);
        } else if (type == java.awt.Color.class){
            colorInfo = new AwtColorFieldInfo(info);
        } else {
            throw new IllegalArgumentException("unsupported color type : " + type.getName());
        }
        ColorPicker button = new ColorPicker(colorInfo.getWithType());
        return new FXUIField<ColorPicker>(colorInfo, button){
            @Override
            public void update(){
                button.setValue(colorInfo.getWithType());
            }

            @Override
            public void action(ActionEvent e){
                Color color = button.getValue();
                if (color != null){
                    colorInfo.setWithType(color);
                }
            }
        };
    }

    public static FXUIField<Button> createFontUI(UIFieldField info, Class type){
        FieldInfo.ReflectFieldInfo<Font> fontInfo;
        if (type == Font.class){
            fontInfo = new FontFieldInfo(info);
        } else if (type == String.class){
            fontInfo = new FontIiteralFieldInfo(info);
        } else if (type == java.awt.Font.class){
            fontInfo = new AWTFontFieldInfo(info);
        } else {
            throw new IllegalArgumentException("unsupported color type : " + type.getName());
        }
        Button button = new Button();
        return new FXUIField<Button>(fontInfo, button){
            @Override
            public void update(){
                Font font = fontInfo.getWithType();
                if (font != null){
                    button.setFont(font);
                    button.setText(font.getName());
                }
            }

            @Override
            public void action(ActionEvent e){
                Font font = showFontChooser(button.getFont());
                if (font != null){
                    button.setFont(font);
                    button.setText(font.getName());
                    fontInfo.setWithType(font);
                }
            }
        };
    }

    public static FXUIField<TextField> createLineWidthUI(UIFieldField info, Class type){
        FieldInfo.ReflectFieldInfo<Double> lineWidthInfo;
        if (type == int.class || type == double.class){
            lineWidthInfo = new ValueFieldInfo(info);
        } else if (type == BasicStroke.class){
            lineWidthInfo = new AWTStrokeFieldInfo(info);
        } else {
            throw new IllegalArgumentException("unsupported color type : " + type.getName());
        }
        TextField text = new TextField();
        setOnFocusUpdateEvent(text);
        return new FXUIField<TextField>(lineWidthInfo, text){
            @Override
            public void update(){
                text.setText(lineWidthInfo.get());
            }

            @Override
            public void action(ActionEvent e){
                lineWidthInfo.set(text.getText());
            }
        };
    }

    public static class ColorFieldInfo extends FieldInfo.ReflectFieldInfo<Color>{

        public ColorFieldInfo(UIFieldField info){
            super(info);
        }

        @Override
        public Color forString(String expr){
            return Color.valueOf(expr);
        }

        @Override
        public String toString(Color color){
            return color.toString();
        }
    }

    public static class AwtColorFieldInfo extends FieldInfo.Warp<Color, java.awt.Color>{

        public AwtColorFieldInfo(UIFieldField info){
            super(java.awt.Color.class, new ColorFieldInfo(info));
        }

        @Override
        public Color a2t(java.awt.Color color){
            return Color.rgb(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
        }

        @Override
        public java.awt.Color t2a(Color color){
            return new java.awt.Color((float)color.getRed(),
                                      (float)color.getGreen(),
                                      (float)color.getBlue(),
                                      (float)color.getOpacity());
        }
    }

    public static class RGBColorFieldInfo extends FieldInfo.Warp<Color, Integer>{

        public RGBColorFieldInfo(UIFieldField info){
            super(int.class, new ColorFieldInfo(info));
        }

        @Override
        public Color a2t(Integer v){
            double a = (double)((v >> 24) & 0xFF) / 255;
            double r = (double)((v >> 16) & 0xFF) / 255;
            double g = (double)((v >> 8) & 0xFF) / 255;
            double b = (double)((v) & 0xFF) / 255;
            return new Color(r, g, b, a);
        }

        @Override
        public Integer t2a(Color color){
            int a = (int)(color.getOpacity() * 255);
            int r = (int)(color.getRed() * 255);
            int g = (int)(color.getGreen() * 255);
            int b = (int)(color.getBlue() * 255);
            return ((a << 24) | (r << 16) | (g << 8) | b);
        }
    }

    public static class ColorLiteralFieldInfo extends FieldInfo.Warp<Color, String>{

        public ColorLiteralFieldInfo(UIFieldField info){
            super(String.class, new ColorFieldInfo(info));
        }

        @Override
        public Color a2t(String s){
            return Color.valueOf(s);
        }

        @Override
        public String t2a(Color color){
            return color.toString();
        }
    }

    public static class FontFieldInfo extends FieldInfo.ReflectFieldInfo<Font>{

        public FontFieldInfo(UIFieldField info){
            super(info);
        }

        @Override
        public Font forString(String expr){
            return Font.font(expr);
        }

        @Override
        public String toString(Font font){
            return font.getFamily();
        }
    }

    public static class AWTFontFieldInfo extends FieldInfo.Warp<Font, java.awt.Font>{

        public AWTFontFieldInfo(UIFieldField info){
            super(java.awt.Font.class, new FontFieldInfo(info));
        }

        @Override
        public Font a2t(java.awt.Font f){
            return Font.font(f.getFamily(), f.getSize());
        }

        @Override
        public java.awt.Font t2a(Font f){
            return new java.awt.Font(f.getFamily(), java.awt.Font.PLAIN, (int)f.getSize());
        }
    }

    public static class FontIiteralFieldInfo extends FieldInfo.Warp<Font, String>{

        public FontIiteralFieldInfo(UIFieldField info){
            super(String.class, new FontFieldInfo(info));
        }

        @Override
        public Font a2t(String s){
            return Font.font(s);
        }

        @Override
        public String t2a(Font font){
            return font.getFamily();
        }
    }

    public static class AWTStrokeFieldInfo extends FieldInfo.Warp<Double, BasicStroke>{

        public AWTStrokeFieldInfo(UIFieldField info){
            super(BasicStroke.class, new ValueFieldInfo(info));
        }

        @Override
        public Double a2t(BasicStroke basicStroke){
            return (double)basicStroke.getLineWidth();
        }

        @Override
        public BasicStroke t2a(Double width){
            return new BasicStroke(width.floatValue());
        }
    }

    public static void setBackground(Control target, Color color){
        target.setStyle("-fx-control-inner-background: #" + color.toString().substring(2));
    }

    public static void setBackground(Node target, Color color){
        target.setStyle("-fx-background: #" + color.toString().substring(2));
    }

    public static Font showFontChooser(Font initFont){
        FontChooser fc = new FontChooser();
        if (initFont != null){
            fc.setSelectedFont(initFont);
        }
        int ret = fc.showDialog();
        if (ret == MessageSession.YES_OPTION){
            return fc.getSelectedFont();
        }
        return null;
    }
}
