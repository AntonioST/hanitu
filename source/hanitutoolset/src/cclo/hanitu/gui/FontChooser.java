/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.Stage;

/**
 * @author antonio
 */
public class FontChooser extends Stage{

    private static final Integer[] DEFAULT_FONT_SIZE_STRINGS = {8, 9, 10, 11, 12,
                                                                14, 16, 18, 20, 22,
                                                                24, 26, 28, 36, 48, 72};

    private final List<String> fontFamilyNames = Font.getFamilies();

    private final TextField fontFamilyTextField = new TextField();
    private final TextField fontStyleTextField = new TextField();
    private final TextField fontSizeTextField = new TextField();
    private final ListView<String> fontFamilyList
      = new ListView<>(FXCollections.observableList(fontFamilyNames));
    private final ListView<FontWeight> fontStyleList
      = new ListView<>(FXCollections.observableArrayList(FontWeight.values()));
    private final ListView<Integer> fontSizeList
      = new ListView<>(FXCollections.observableArrayList(DEFAULT_FONT_SIZE_STRINGS));
    private final CheckBox fontItalic = new CheckBox("italic");
    private final CheckBox fontUnderLine = new CheckBox("under line");
    private final CheckBox fontStrikeThrough = new CheckBox("strike through");
    private final Text sampleTextField = new Text("Sample Preview ABC abc");

    private boolean italic;
    protected int dialogResultValue = MessageSession.CLOSE_OPTION;

    public FontChooser(){
        setTitle("Font Chooser");
        //
        VBox root = new VBox(initTopPane(),
                             initMiddlePane(),
                             initBottomPane(),
                             initControl());
        root.setFillWidth(true);
        VBox.setVgrow(root.getChildren().get(0), Priority.ALWAYS);
        Insets insets = new Insets(15);
        VBox.setMargin(root.getChildren().get(0), insets);
        VBox.setMargin(root.getChildren().get(1), insets);
        VBox.setMargin(root.getChildren().get(2), insets);
        VBox.setMargin(root.getChildren().get(3), insets);
        //
        Scene scene = new Scene(root);
        setScene(scene);
        sizeToScene();
    }

    private Node initTopPane(){
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        //
        grid.add(new Label("Font"), 0, 0);
        grid.add(new Label("Weight"), 1, 0);
        grid.add(new Label("Size"), 2, 0);
        grid.add(fontFamilyTextField, 0, 1);
        grid.add(fontStyleTextField, 1, 1);
        grid.add(fontSizeTextField, 2, 1);
        grid.add(fontFamilyList, 0, 2);
        grid.add(fontStyleList, 1, 2);
        grid.add(fontSizeList, 2, 2);
        //
        grid.getColumnConstraints().add(new ColumnConstraints(200, 200, Double.POSITIVE_INFINITY,
                                                              Priority.ALWAYS, HPos.LEFT, true));
        grid.getColumnConstraints().add(new ColumnConstraints(130));
        grid.getColumnConstraints().add(new ColumnConstraints(70));
        grid.getRowConstraints().addAll(new RowConstraints(20));
        grid.getRowConstraints().addAll(new RowConstraints(20));
        grid.getRowConstraints().addAll(new RowConstraints(130, 130, Double.POSITIVE_INFINITY,
                                                           Priority.ALWAYS, VPos.BOTTOM, true));
        //
        fontFamilyList.setEditable(false);
        fontStyleList.setEditable(false);
        fontSizeList.setEditable(false);
        //
        fontStyleList.getSelectionModel().select(FontWeight.NORMAL);
        fontStyleTextField.setText(FontWeight.NORMAL.name());
        fontSizeList.getSelectionModel().select(12);
        fontSizeTextField.setText("12");
        //
        fontFamilyList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null){
                fontFamilyTextField.setText(newValue);
                sampleTextField.setFont(getSelectedFont());
            }
        });
        fontStyleList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            fontStyleTextField.setText(newValue.name());
            sampleTextField.setFont(getSelectedFont());
        });
        fontSizeList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            fontSizeTextField.setText(Integer.toString(newValue));
            sampleTextField.setFont(getSelectedFont());
            sizeToScene();
        });
        //
        bindKeyMove(fontFamilyTextField, fontFamilyList);
        bindKeyMove(fontStyleTextField, fontStyleList);
        bindKeyMove(fontSizeTextField, fontSizeList);
        fontFamilyTextField.setOnKeyTyped(e -> {
            String c = e.getCharacter();
            if (Character.isAlphabetic(c.codePointAt(0)) || c.equals(" ")){
                int position = fontFamilyTextField.getSelection().getStart();
                String text = fontFamilyTextField.getText().substring(0, position);
                String match = (text + c).toLowerCase();
                ObservableList<String> list = fontFamilyList.getItems();
                fontFamilyList.getSelectionModel().clearSelection();
                for (int i = 0, size = list.size(); i < size; i++){
                    if (list.get(i).toLowerCase().startsWith(match)){
                        fontFamilyList.getSelectionModel().select(i);
                        fontFamilyList.scrollTo(i);
                        fontFamilyTextField.selectRange(list.get(i).length(), match.length());
                        e.consume(); //avoid text change by origin typing event
                        return;
                    }
                }
                fontFamilyTextField.setText(text);
                fontFamilyTextField.positionCaret(text.length());
            }
        });

        return grid;
    }

    private void bindKeyMove(TextField field, ListView list){
        field.setOnKeyPressed(e -> {
            switch (e.getCode()){
            case UP:
                list.getSelectionModel().selectPrevious();
                break;
            case DOWN:
                list.getSelectionModel().selectNext();
                break;
            }
            list.scrollTo(list.getSelectionModel().getSelectedIndex());
        });
    }

    private Node initMiddlePane(){
        HBox box = new HBox(new Label("Style"), fontItalic, fontUnderLine, fontStrikeThrough);
        box.setSpacing(15);
        box.setAlignment(Pos.BASELINE_LEFT);
        //
        fontItalic.setOnAction(e -> {
            italic = fontItalic.isSelected();
            sampleTextField.setFont(getSelectedFont());
        });
        fontUnderLine.setOnAction(e -> sampleTextField.setUnderline(fontUnderLine.isSelected()));
        fontStrikeThrough.setOnAction(e -> sampleTextField.setStrikethrough(fontStrikeThrough.isSelected()));
        //
        return box;
    }

    private Node initBottomPane(){
        TextFlow flow = new TextFlow(sampleTextField);
        flow.setTextAlignment(TextAlignment.CENTER);
        flow.setMaxHeight(100);
        flow.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        return flow;
    }

    private Node initControl(){
        HBox box = MessageSession.initControlRegion(MessageSession.YES_NO_CANCEL_OPTION, (e, code) -> {
            dialogResultValue = code;
            FontChooser.this.close();
        });
        box.setAlignment(Pos.BASELINE_RIGHT);
        box.setSpacing(15);
        return box;
    }

    public Font getSelectedFont(){
        return Font.font(getSelectedFontFamily(),
                         getSelectedFontStyle(),
                         italic? FontPosture.ITALIC: FontPosture.REGULAR, //XXX italic not work
                         getSelectedFontSize());
    }

    public Text formatText(String text){
        Text ret = new Text(text);
        ret.setFont(getSelectedFont());
        ret.setUnderline(fontUnderLine.isSelected());
        ret.setStrikethrough(fontStrikeThrough.isSelected());
        return ret;
    }

    public void setSelectedFont(Font font){
        setSelectedFontFamily(font.getFamily());
        fontStyleList.getSelectionModel().select(FontWeight.NORMAL);
        setSelectedFontSize((int)font.getSize());
    }

    public void setSelectedFontFamily(String family){
        fontFamilyList.getSelectionModel().select(family);
        fontFamilyList.scrollTo(fontFamilyList.getSelectionModel().getSelectedIndex());
    }

    public void setSelectedFontStyle(FontWeight w){
        fontStyleList.getSelectionModel().select(w);
    }

    public void setSelectedFontSize(int size){
        for (int i = 0, length = DEFAULT_FONT_SIZE_STRINGS.length; i < length; i++){
            if (DEFAULT_FONT_SIZE_STRINGS[i] > size){
                fontSizeList.getSelectionModel().select(i);
                fontSizeList.scrollTo(i);
                break;
            }
        }
    }

    private String getSelectedFontFamily(){
        return fontFamilyList.getSelectionModel().getSelectedItem();
    }

    private FontWeight getSelectedFontStyle(){
        try {
            return FontWeight.values()[fontStyleList.getSelectionModel().getSelectedIndex()];
        } catch (IllegalArgumentException e){
            System.err.println(e);
            return FontWeight.NORMAL;
        }
    }

    private double getSelectedFontSize(){
        return fontSizeList.getSelectionModel().getSelectedItem();
    }

    public int showDialog(){
        showAndWait();
        return dialogResultValue;
    }

    public Font showDialogAngGetFont(){
        showAndWait();
        if (dialogResultValue == MessageSession.YES_OPTION){
            return getSelectedFont();
        }
        return null;
    }
}
