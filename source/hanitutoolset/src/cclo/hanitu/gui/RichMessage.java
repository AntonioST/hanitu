/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.*;

import cclo.hanitu.Base;
import cclo.hanitu.Message;

/**
 * @author antonio
 */
public class RichMessage{

    static String DEFAULT_FONT_FAMILY = Base.getProperty("cclo.hanitu.rich.font", "FreeMono");
    static int DEFAULT_FONT_SIZE = Base.getIntegerProperty("cclo.hanitu.rich.fontsize", 16);

    private static FontWeight[] WEIGHTS
      = {FontWeight.LIGHT, FontWeight.THIN, FontWeight.NORMAL, FontWeight.BOLD, FontWeight.BLACK};
    private static FontPosture[] STYLES
      = {FontPosture.REGULAR, FontPosture.ITALIC};

    private static Paint mapColor(String m){
        switch (m){
        case "r":
            return Color.RED;
        case "g":
            return Color.GREEN;
        case "y":
            return Color.YELLOW;
        case "b":
            return Color.BLUE;
        case "m":
            return Color.MAGENTA;
        case "c":
            return Color.CYAN;
        case "":
        case "k":
            return Color.BLACK;
        case "w":
            return Color.WHITE;
        }
        return null;
    }

    private static Text map(Message message){
        Text text = new Text(message.text);
        String font = message.font.isEmpty()? DEFAULT_FONT_FAMILY: message.font;
        FontWeight weight = WEIGHTS[message.weight];
        FontPosture style = STYLES[message.style];
        int fontsize = message.size < 0? DEFAULT_FONT_SIZE: message.size;
        text.setFont(Font.font(font, weight, style, fontsize));
        Paint color = mapColor(message.foregroundColor);
        if (color != null){
            text.setFill(color);
        }
        text.setUnderline(message.underLine);
        text.setStrikethrough(message.strikeThrough);
        return text;
    }

    public static TextFlow mapTextExpression(String line){
        TextFlow ret = new TextFlow();
        Message.parse(line).stream().map(RichMessage::map).forEach(ret.getChildren()::add);
        return ret;
    }
}
