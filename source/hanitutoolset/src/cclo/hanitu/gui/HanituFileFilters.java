/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.util.Properties;

import cclo.hanitu.Base;
import cclo.hanitu.HanituInfo;

import static javafx.stage.FileChooser.ExtensionFilter;

/**
 * @author antonio
 */
public class HanituFileFilters{

    public static final ExtensionFilter WORLD_CONFIG_FILTER;

    public static final ExtensionFilter CIRCUIT_CONFIG_FILTER;

    public static ExtensionFilter NORMAL_TEXT_FILTER;

    public static ExtensionFilter ALL_FILTER;

    static{
        Properties p = Base.loadPropertyInherit("cli");
        WORLD_CONFIG_FILTER = new ExtensionFilter(p.getProperty("filter.world.desp", "Hanitu World Configuration")
                                                  + "(" + HanituInfo.WORLD_CONFIG_EXTEND_FILENAME + ")",
                                                  "*." + HanituInfo.WORLD_CONFIG_EXTEND_FILENAME);
        CIRCUIT_CONFIG_FILTER = new ExtensionFilter(p.getProperty("filter.circuit.desp", "Hanitu Circuit")
                                                    + "(" + HanituInfo.CIRCUIT_EXTEND_FILENAME + ")",
                                                    "*." + HanituInfo.CIRCUIT_EXTEND_FILENAME);
        NORMAL_TEXT_FILTER = new ExtensionFilter(p.getProperty("filter.text.desp", "Text File") + " (txt)", "*.txt");
        ALL_FILTER = new ExtensionFilter(p.getProperty("filter.all.desp", "All Files"), "*.*");
    }
}
