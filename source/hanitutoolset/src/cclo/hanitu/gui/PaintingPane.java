/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.gui;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;

/**
 * @author antonio
 */
public class PaintingPane extends Canvas{

    private Color backgroundColor;
    private final List<PaintArea> panels = new ArrayList<>();

    public PaintingPane(double w, double h){
        super(w, h);
//        if (Env.DEBUG){
//            StackTraceElement[] st = Thread.currentThread().getStackTrace();
//            Env.debug(st[2].getClassName() + " ~*y{create~} " + this);
//        }
    }

    public void paint(){
        GraphicsContext g = getGraphicsContext2D();
        if (backgroundColor != null){
            g.setFill(backgroundColor);
            g.fillRect(0, 0, getWidth(), getHeight());
        }
        Affine origin = g.getTransform();
        panels.forEach(p -> {
            if (p.visible){
                g.translate(p.x, p.y);
                p.paint(g);
                g.setTransform(origin);
            }
        });
    }

    public void repaint(){
        Platform.runLater(this::paint);
    }

    public Color getBackgroundColor(){
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor){
        this.backgroundColor = backgroundColor;
    }

    public boolean addPanel(PaintArea viewSubPanel){
        boolean ret = panels.add(viewSubPanel);
        if(ret){
//            Env.debug(() -> this + " ~*y{add~} [" + viewSubPanel.layer + "] " + viewSubPanel);
            panels.sort(Comparator.comparingInt(p -> p.layer));
            viewSubPanel.setPanel(this);
        }
        return ret;
    }

    public boolean removePanel(PaintArea o){
        if (panels.remove(o)){
//            Env.debug(() -> this + " ~*y{remove~} [" + o.layer + "] " + o);
            o.setPanel(null);
            return true;
        }
        return false;
    }

    public void removeLayer(int layer){
        panels.removeIf(p -> p.layer == layer);
    }
}
