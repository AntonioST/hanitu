/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import cclo.hanitu.util.Strings;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Error/Warning message record. If store the information at the exception occurs during file loading.
 *
 * ### Format Flag
 * It use character `'%'` with one identify character the find insert place. It support
 *
 * #### Unit Flag
 *
 * see method {@link #unitFormatGeneral(char)}
 *
 * #### List Type Flag
 *
 * **syntax** : %{LIST|FORMAT}
 *
 * detail `LIST` syntax, see method {@link #getListType(String)}
 *
 * `FORMAT` is the format expression for the content of the list, there are some flag which is according by the list type.
 *
 *
 * By default, the format of the title is `"%t (%f :%n)"`, and the format of the message is
 * `"  for line : %c\n%{m|  %s\n}"`.
 *
 * @author antonio
 */
public class WarningRecord{

    private static final String TITLE_FORMAT = Message.getMessage("record.title");
    private static final String MESSAGE_FORMAT = Message.getMessage("record.message");
    /**
     * loading file name.
     */
    public final String filename;
    /**
     * line content.
     */
    public final String lineContent;
    /**
     * line number.
     */
    public final int lineNumber;
    /**
     * message title
     */
    public final String title;
    /**
     * message content.
     */
    public final List<String> message;
    private final StackTraceElement[] stacks;


    /**
     * create a warning record and record the caller method stack.
     *
     * @param loader  file loader
     * @param title   message title
     * @param message message content.
     */
    WarningRecord(FileLoader loader, String title, String... message){
        filename = loader.getFileName() != null? loader.getFileName(): "???";
        lineContent = Objects.requireNonNull(loader.getOriginalLine());
        lineNumber = loader.getCurrentLineNumber();
        this.title = title;
        this.message = Arrays.asList(message);
        StackTraceElement[] st = Thread.currentThread().getStackTrace();
        stacks = Arrays.copyOfRange(st, 2, st.length);
        Log.debug(() -> format(TITLE_FORMAT) + " \"" + lineContent + "\"");
        Log.debug(() -> "meta message : " + loader.getResource(FileLoader.META_MESSAGE));
        Log.debugTrace("trace");
    }

    /**
     * create a warning record with a exception.
     *
     * @param loader file loader
     * @param th     exception
     */
    WarningRecord(FileLoader loader, Throwable th){
        filename = loader.getFileName() != null? loader.getFileName(): "???";
        lineContent = Objects.requireNonNull(loader.getOriginalLine());
        lineNumber = loader.getCurrentLineNumber();
        title = th.getClass().getName();
        message = Collections.singletonList(th.getMessage());
        stacks = th.getStackTrace();
        Log.debug(() -> format(TITLE_FORMAT) + " \"" + lineContent + "\"");
        Log.debug(() -> "meta message : " + loader.getResource(FileLoader.META_MESSAGE));
        Log.debug(th);
    }

    /**
     * format the warning record.
     *
     * @param format format expression
     * @return format result
     */
    protected String format(String format){
        Strings expr = new Strings();
        expr.setSingleReplace(this::unitFormatGeneral);
        expr.setReplace((ff, sb) -> {
            if (ff.length() <= 2) return -1;
            if (ff.charAt(0) != '%') return -1;
            char c = ff.charAt(1);
            if (c == '{'){
                int ij = ff.indexOf("%}", 2);
                if (ij == -1){
                    throw new IllegalArgumentException("lost %} : " + ff);
                }
                int ik = ff.indexOf("|");
                if (ik == -1){
                    throw new IllegalArgumentException("lost | : " + ff);
                } else if (ik < ij){
                    formatList(sb, ff.substring(ik + 1, ij), getListType(ff.substring(2, ik)));
                } else {
                    //empty iterator, represent empty string
                    //ignore
                }
                return ij + 2;
            }
            return -1;
        });
        return expr.format(format);
    }

    /**
     * format list content.
     *
     * @param builder result buffer
     * @param format  format expression
     * @param list    list
     */
    protected void formatList(StringBuilder builder, String format, List<?> list){
        if (list.isEmpty()) return;
        Object c = list.get(0);
        Strings expr = new Strings();
        if (c instanceof String){
            for (String s : (List<String>)list){
                expr.setSingleReplace(MessageFormatOperator.unitFormatMessageOp(s));
                expr.formatAppend(builder, format);
            }
        } else if (c instanceof StackTraceElement){
            for (StackTraceElement s : ((List<StackTraceElement>)list)){
                expr.setSingleReplace(MessageFormatOperator.unitFormatStackElementOp(s));
                expr.formatAppend(builder, format);
            }
        }
    }

    /**
     * general unit format flag.
     *
     * **format flag**
     *
     * Flag     | Description
     * ----     | -----------
     * %%       | character '%'
     * %S       | loader state number
     * %f       | file name
     * %n       | line number
     * %c       | line content
     * %t       | message title
     * %_digit_ | message content in index `digit`
     *
     * @param formatFlag format flag
     * @return result, null if flag not matched
     */
    protected String unitFormatGeneral(char formatFlag){
        switch (formatFlag){
        case 'f':
            return filename;
        case 'n':
            return Integer.toString(lineNumber);
        case 'c':
            return lineContent;
        case 't':
            return title;
        case '%':
            return "%";
        default:
            if (Character.isDigit(formatFlag)){
                return message.get(formatFlag - '0');
            }
        }
        return null;
    }

    /**
     * get list type.
     *
     * **syntax**
     *
     * + `TYPE`
     * + `TYPE[..]`
     * + `TYPE[INDEX]`
     * + `TYPE[START..END]`
     *
     * list index allow negative index.
     *
     * **list type**
     *
     * supported type   | description           | unit function
     * --------------   | -----------           | -------------
     * m                | message               | {@link MessageFormatOperator#unitFormatMessageOp(String)}
     * s                | stack trace element   | {@link MessageFormatOperator#unitFormatStackElementOp(StackTraceElement)}
     *
     * @param i format flag
     * @return list instance
     */
    protected List<?> getListType(String i){
        List<?> ret;
        int from = 0;
        int end = -1;
        if (i.contains("[")){
            int ii = i.indexOf("[");
            int ij = i.indexOf("]", ii);
            if (ij < 0){
                throw new IllegalArgumentException("lost ]");
            }
            String range = i.substring(ii + 1, ij);
            i = i.substring(0, ii);
            if (range.contains("..")){
                ii = range.indexOf("..");
                String tmp = range.substring(0, ii);
                from = tmp.isEmpty()? 0: Integer.parseInt(tmp);
                tmp = range.substring(ii + 2);
                end = tmp.isEmpty()? -1: Integer.parseInt(tmp);
            } else {
                from = end = Integer.parseInt(range);
            }
        }
        switch (i){
        case "m":
            ret = message;
            break;
        case "s":
            ret = Arrays.asList(stacks);
            break;
        default:
            throw new IllegalArgumentException("unsupported list-type flag : " + i);
        }
        if (from == 0 && end == -1) return ret;
        if (end < 0){
            end += ret.size();
        }
        if (end < from){
            throw new IllegalArgumentException("illegal range : " + i);
        }
        return ret.subList(from, end);
    }

    /**
     * print warning record to {@link PrintStream}
     *
     * @param head start header string
     * @param ps   output stream
     */
    public void print(String head, PrintStream ps){
        ps.print(head);
        ps.print(" ");
        ps.println(format(TITLE_FORMAT));
        ps.println(format(MESSAGE_FORMAT));
    }
}
