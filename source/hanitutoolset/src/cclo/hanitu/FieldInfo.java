/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Stream;

import cclo.hanitu.gui.UIType;

import static cclo.hanitu.HanituInfo.*;
import static java.util.Objects.requireNonNull;

/**
 * @author antonio
 */
public abstract class FieldInfo{

    public final String group;
    public final String simpleName;
    public final String name;
    public final String tip;

    public FieldInfo(String group, String simpleName, String name, String tip){
        this.group = group;
        this.simpleName = simpleName;
        this.name = Objects.requireNonNull(name);
        this.tip = tip;
    }

    public FieldInfo(String name, String tip){
        group = null;
        simpleName = null;
        this.name = Objects.requireNonNull(name);
        this.tip = tip;
    }

    public abstract String get();

    public abstract boolean set(String value);

    public static List<FieldInfo> listFieldInfo(Object target){
        List<FieldInfo> ls = new ArrayList<>();
        Class<?> cls = target.getClass();
//        Env.debug(() -> "Info class : " + cls.getName());
        for (Field field : cls.getFields()){
            if (field.getAnnotation(VTextField.class) != null){
                ls.add(new TextFieldInfo(target, field));
            } else if (field.getAnnotation(VValueField.class) != null){
                ls.add(new ValueFieldInfo(target, field));
            } else if (field.getAnnotation(VBooleanField.class) != null){
                ls.add(new BooleanFieldInfo(target, field));
            } else if (field.getAnnotation(VUIField.class) != null){
                ls.add(new UIFieldField(target, field));
            } else if (field.getAnnotation(VArrayField.class) != null){
                ls.add(new ArrayFieldInfo(target, field));
            }
        }
        for (Method method : cls.getMethods()){
            if (method.getAnnotation(VAction.class) != null){
                ls.add(new ActionFieldInfo(target, method));
            }
        }
        return ls;
    }

    private static String getGroupName(Field field){
        GroupField gf = field.getAnnotation(GroupField.class);
        if (gf == null) return null;
        if (gf.resource().isEmpty()) return gf.value();
        return Base.loadPropertyInherit(gf.resource()).getProperty(gf.value());
    }

    public static String getFieldSimpleName(Field field){
        GroupField gf = field.getAnnotation(GroupField.class);
        if (gf == null) return null;
        String s = gf.simpleName();
        if (s.isEmpty()) return null;
        if (gf.resource().isEmpty()) return s;
        return Base.loadPropertyInherit(gf.resource()).getProperty(s);
    }

    private static String getFieldName(Field field){
        Description d = field.getAnnotation(Description.class);
        if (d == null){
            return getFieldName(field.getName());
        } else if (d.resource().isEmpty()){
            return d.value();
        }
        return Base.loadPropertyInherit(d.resource()).getProperty(d.value());
    }

    /*
    default pattern
    thisIsAField -> 'this is a field'
    */
    private static String getFieldName(String name){
        StringBuilder tmp = new StringBuilder();
        for (int i = 0, length = name.length(); i < length; i++){
            char c = name.charAt(i);
            if (Character.isUpperCase(c)){
                tmp.append(' ').append(Character.toLowerCase(c));
            } else {
                tmp.append(c);
            }
        }
        return tmp.toString();
    }

    private static String getFieldTip(Field field){
        Tip t = field.getAnnotation(Tip.class);
        if (t == null) return null;
        if (t.resource().isEmpty()){
            return t.value();
        }
        return Base.loadPropertyInherit(t.resource()).getProperty(t.value());
    }

    public static abstract class ReflectFieldInfo<T> extends FieldInfo{

        public final Class<?> type;
        protected final Object target;
        protected final Field field;

        public ReflectFieldInfo(Object target, Class<?> type, Field field){
            super(getGroupName(field), getFieldSimpleName(field), getFieldName(field), getFieldTip(field));
            //
            if (!Modifier.isPublic(field.getModifiers())){
                throw new IllegalArgumentException("field " + field.getName() + " doesn't public");
            }
            Class<?> primaryType = field.getType();
            if (type != null){
                if (primaryType != type && !type.isAssignableFrom(primaryType)){
                    throw new IllegalArgumentException("field " + field.getName() + " type isn't " + type.getName());
                }
                this.type = type;
            } else {
                this.type = primaryType;
            }
            //
            this.target = target;
            this.field = field;
        }

        public ReflectFieldInfo(ReflectFieldInfo info){
            super(info.group, info.simpleName, info.name, info.tip);
            //
            if (!Modifier.isPublic(info.field.getModifiers())){
                throw new IllegalArgumentException("field " + info.field.getName() + " doesn't public");
            }
            target = info.target;
            type = info.type;
            field = info.field;
        }

        @Override
        public String get(){
            return toString(getWithType());
        }

        @Override
        public boolean set(String value){
            return setWithType(forString(value));
        }

        public T getWithType(){
            try {
                return (T)field.get(target);
            } catch (IllegalAccessException e){
            }
            return null;
        }

        public boolean setWithType(T t){
            if (t != null){
                try {
                    Log.debug(() -> "~*r{set~} " + name + " = " + t);
                    field.set(target, t);
                    return true;
                } catch (IllegalAccessException e){
                }
            }
            return false;
        }

        public abstract T forString(String expr);

        public abstract String toString(T t);
    }

    public static abstract class Warp<T, A> extends ReflectFieldInfo<T>{

        private final ReflectFieldInfo<T> info;

        public Warp(Class<A> type, ReflectFieldInfo<T> info){
            super(info.target, type, info.field);
            this.info = info;
        }

        public abstract T a2t(A a);

        public abstract A t2a(T t);

        @Override
        public T getWithType(){
            try {
                return a2t((A)field.get(target));
            } catch (IllegalAccessException e){
            }
            return null;
        }

        @Override
        public boolean setWithType(T t){
            if (t != null){
                try {
                    Log.debug(() -> "~*r{set~} " + name + " = " + t + " ~*y{->~} " + t2a(t));
                    field.set(target, t2a(t));
                    return true;
                } catch (IllegalAccessException e){
                }
            }
            return false;
        }

        @Override
        public T forString(String expr){
            return info.forString(expr);
        }

        @Override
        public String toString(T t){
            return info.toString(t);
        }
    }

    public static class TextFieldInfo extends ReflectFieldInfo<String>{

        public TextFieldInfo(Object target, Field field){
            super(target, String.class, field);
        }

        @Override
        public String forString(String expr){
            return expr;
        }

        @Override
        public String toString(String s){
            return s;
        }
    }

    public static class ValueFieldInfo extends ReflectFieldInfo<Double>{

        public final String unit;

        public ValueFieldInfo(Object target, Field field){
            super(target, null, field);
            Class ptype = field.getType();
            if (ptype != int.class && ptype != double.class){
                throw new IllegalArgumentException("field " + field.getName() + " doesn't numeric type");
            }
            VValueField annotation = field.getAnnotation(VValueField.class);
            if (annotation != null && !annotation.value().isEmpty()){
                if (annotation.resource().isEmpty()){
                    unit = annotation.value();
                } else {
                    unit = Base.loadPropertyInherit(annotation.resource()).getProperty(annotation.value());
                }
            } else {
                unit = null;
            }
        }

        public ValueFieldInfo(ReflectFieldInfo info){
            this(info.target, info.field);
        }

        @Override
        public Double getWithType(){
            try {
                Object o = field.get(target);
                if (o != null){
                    if (field.getType() == int.class){
                        return ((Integer)o).doubleValue();
                    } else {
                        return (Double)o;
                    }
                }
            } catch (IllegalAccessException e){
            }
            return null;
        }

        @Override
        public boolean setWithType(Double v){
            try {
                if (v != null){
                    if (field.getType() == int.class){
                        Log.debug(() -> "~*r{set~} " + name + " = " + v.intValue());
                        field.set(target, v.intValue());
                    } else {
                        Log.debug(() -> "~*r{set~} " + name + " = " + v);
                        field.set(target, v);
                    }
                }
            } catch (IllegalAccessException e){
            }
            return false;
        }

        @Override
        public Double forString(String expr){
            return Double.parseDouble(expr.trim());
        }

        @Override
        public String toString(Double v){
            if (type == int.class){
                return Integer.toString(v.intValue());
            } else {
                return Double.toString(v);
            }
        }
    }

    public static class BooleanFieldInfo extends ReflectFieldInfo<Boolean>{

        public final String[] set;

        public BooleanFieldInfo(Object target, Field field){
            super(target, boolean.class, field);
            VBooleanField annotation = requireNonNull(field.getAnnotation(VBooleanField.class));
            String[] v = annotation.value();
            if (v.length != 2){
                throw new IllegalArgumentException("wrong stage number for button");
            }
            if (annotation.resource().isEmpty()){
                set = v;
            } else {
                Properties p = Base.loadPropertyInherit(annotation.resource());
                set = new String[]{p.getProperty(v[0]),
                                   p.getProperty(v[1])
                };
            }
        }

        public boolean inverseSet(String value){
            return setWithType(!forString(value));
        }

        @Override
        public Boolean forString(String expr){
            return Objects.equals(expr, set[1]);
        }

        @Override
        public String toString(Boolean v){
            return v? set[1]: set[0];
        }
    }

    public static class ArrayFieldInfo extends ReflectFieldInfo<Integer>{

        public final String[] set;

        public ArrayFieldInfo(Object target, Field field){
            super(target, int.class, field);
            VArrayField annotation = requireNonNull(field.getAnnotation(VArrayField.class));
            String[] v = annotation.value();
            if (annotation.resource().isEmpty()){
                set = v;
            } else {
                Properties p = Base.loadPropertyInherit(annotation.resource());
                set = Stream.of(v).map(p::getProperty).toArray(String[]::new);
            }
        }

        @Override
        public Integer forString(String expr){
            for (int i = 0, length = set.length; i < length; i++){
                if (set[i].equals(expr)){
                    return i;
                }
            }
            return null;
        }

        @Override
        public String toString(Integer v){
            return v < 0? null: set[v];
        }
    }

    public static class ActionFieldInfo extends FieldInfo{

        private final Object target;
        private final Method method;

        public ActionFieldInfo(Object target, Method method){
            super(getName(method), null);
            if (!Modifier.isPublic(method.getModifiers())){
                throw new IllegalArgumentException("method " + method.getName() + " doesn't public");
            }
            this.target = target;
            this.method = method;
        }

        private static String getName(Method method){
            VAction annotation = requireNonNull(method.getAnnotation(VAction.class));
            if (annotation != null) return annotation.value();
            return getFieldName(method.getName());
        }

        @Override
        public String get(){
            return null;
        }

        @Override
        public boolean set(String value){
            return false;
        }

        public void fire(){
            try {
                method.invoke(target);
            } catch (IllegalAccessException e){
            } catch (InvocationTargetException e){
                throw new RuntimeException(e.getCause());
            }
        }
    }

    public static class UIFieldField extends ReflectFieldInfo<Object>{

        public final Class primaryType;
        public final UIType uiType;

        public UIFieldField(Object target, Field field){
            super(target, null, field);
            primaryType = field.getType();
            uiType = field.getAnnotation(VUIField.class).value();
        }

        @Override
        public Object forString(String expr){
            throw new RuntimeException();
        }

        @Override
        public String toString(Object t){
            return Objects.toString(t);
        }
    }
}
