/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

/**
 * @author antonio
 */
public interface Meta{

    boolean containMeta(String key);

    Object getMeta(String key);

    Object putMeta(String key, Object value);

    Object removeMeta(String key);

    default int getIntMeta(String key){
        Object ret = getMeta(key);
        if (ret instanceof Integer){
            return (Integer)ret;
        } else if (ret instanceof Number){
            return ((Number)ret).intValue();
        } else if (ret instanceof String){
            return Integer.parseInt((String)ret);
        }
        throw new RuntimeException("meta '" + key + "' not a integer : " + (ret == null? "null": ret.getClass()));
    }

    default int getIntMeta(String key, int def){
        Object ret = getMeta(key);
        if (ret == null) return def;
        if (ret instanceof Integer){
            return (Integer)ret;
        } else if (ret instanceof Number){
            return ((Number)ret).intValue();
        } else if (ret instanceof String){
            return Integer.parseInt((String)ret);
        }
        return def;
    }

    default double getFloatMeta(String key){
        Object ret = getMeta(key);
        if (ret instanceof Double){
            return (Double)ret;
        } else if (ret instanceof Number){
            return ((Number)ret).doubleValue();
        } else if (ret instanceof String){
            return Double.parseDouble((String)ret);
        }
        throw new RuntimeException(
          "meta '" + key + "' not a floating number : " + (ret == null? "null": ret.getClass()));
    }

    default double getFloatMeta(String key, double def){
        Object ret = getMeta(key);
        if (ret == null) return def;
        if (ret instanceof Double){
            return (Double)ret;
        } else if (ret instanceof Number){
            return ((Number)ret).doubleValue();
        } else if (ret instanceof String){
            return Double.parseDouble((String)ret);
        }
        return def;
    }

    default String getStrMeta(String key){
        Object ret = getMeta(key);
        if (ret instanceof String){
            return (String)ret;
        }
        throw new RuntimeException("meta '" + key + "' not a string : " + (ret == null? "null": ret.getClass()));
    }

    default <T> T getMeta(String key, T def){
        Object ret = getMeta(key);
        if (ret == null) return def;
        return (T)ret;
    }
}
