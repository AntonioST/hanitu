/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.util.ListIterator;

/**
 * Runnable class used for Hanitu Tool Set.
 *
 * @author antonio
 */
public abstract class Executable implements Runnable{

    /**
     * handle options for special case.
     *
     * @param key   matching option name
     * @param value option value, null if correspond option doesn't need argument
     * @param it    arguments list iterator.
     * @return accept this option or not
     */
    public boolean extendOption(String key, String value, ListIterator<String> it){
        return false;
    }

    /**
     * handle arguments for special case.
     *
     * @param index the place of the argument
     * @param value argument
     * @param it    arguments list iterator.
     * @return accept this argument or not
     */
    public boolean extendArgument(int index, String value, ListIterator<String> it){
        return false;
    }

    /**
     * extend help document with document formatting keyword.
     *
     * @return help document, null represent no extend
     */
    public String extendHelpDoc(){
        return null;
    }

    public abstract void start() throws Throwable;

    @Override
    public void run(){
        try {
            start();
        } catch (RuntimeException e){
            throw e;
        } catch (Throwable e){
            throw new RuntimeException(e);
        }
    }
}
