/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import cclo.hanitu.util.ErrorFunction;
import cclo.hanitu.util.Events;

/**
 * @author antonio
 */
public abstract class NativeExecutable extends Executable implements Callable<Integer>, AutoCloseable{

    protected static final Events.RunnableEvent SHUT_DOWN_EVENT = new Events.RunnableEvent();

    private static final Deque<Path> TEMP_DIR = new LinkedBlockingDeque<>();

    static{
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {

            SHUT_DOWN_EVENT.run();

            for (Path path : TEMP_DIR){
                try {
                    if (Files.exists(path)){
                        Log.debug(() -> "delete temp : " + path);
                        deleteDirectoryRecursive(path);
                    }
                } catch (IOException e){
                }
            }
        }, "NativeCloser"));
    }

    private static final String NULL;
    private static final String SPLIT;

    static{
        if (Base.osWin()){
            // windows
            NULL = ":NUL";
            SPLIT = ";";
        } else {
            // Unix, Linux
            NULL = "/dev/null";
            SPLIT = ":";
        }
    }

    private static ProcessBuilder.Redirect getStream(String file){
        if (file == null || file.isEmpty() || file.equals("null") || file.equals("/dev/null")){
            return ProcessBuilder.Redirect.to(new File(NULL));
        } else if (file.equals("-")){
            return ProcessBuilder.Redirect.INHERIT;
        }
        return ProcessBuilder.Redirect.to(new File(file));
    }

    static Stream<String> listClassPath(){
        return Stream.of(System.getProperty("java.class.path").split(SPLIT));
    }

    private static final Map<Integer, String> SIGNAL_TEXT = new HashMap<>();

    static Stream<String> listEnvPath(){
        String envPath = System.getenv("PATH");
        if (envPath == null) return Stream.empty();
        return Stream.of(envPath.split(SPLIT));
    }

    static{
        // man 7 signal for linux
        SIGNAL_TEXT.put(1, "SIGHUP");
        SIGNAL_TEXT.put(2, "SIGINT");
        SIGNAL_TEXT.put(3, "SIGQUIT");
        SIGNAL_TEXT.put(4, "SIGILL");
        SIGNAL_TEXT.put(6, "SIGABRT");
        SIGNAL_TEXT.put(8, "SIGFPE");
        SIGNAL_TEXT.put(9, "SIGKILL");
        SIGNAL_TEXT.put(11, "SIGSEGV");
        SIGNAL_TEXT.put(13, "SIGPIPE");
        SIGNAL_TEXT.put(14, "SIGALRM");
        SIGNAL_TEXT.put(15, "SIGTERM");
    }

    /**
     * find the native executable file.
     *
     * ### search path order
     *
     * 1. _Base.HOME_ /lib
     * 2. /usr/local/lib/hanitu/lib
     * 3. $HOME/.local/share/hanitu/lib
     * 4. classpath
     * 5. $PATH
     *
     * @param matchName  native executable file
     * @return path of file
     * @throws IOException
     */
    protected static Path findExecutable(String matchName) throws IOException{
        Path path = findExecutable(Paths.get(Base.HOME), matchName);
        if (path == null){
            if(Base.osWin()){
                path = findExecutable(Paths.get(System.getenv("ProgramFiles")), matchName);
            }else {
                path = findExecutable(Paths.get("/usr/local/lib/hanitu"), matchName);
            }
        }
        if (path == null){
            path = findExecutable(Paths.get(System.getProperty("user.home")).resolve(".local/share/hanitu"),
                                  matchName);
        }
        if (path == null){
            Stream<Path> cp = listClassPath()
              .map(Paths::get)
              .map(Path::toAbsolutePath)
              .map(p -> {
                  if (Files.isRegularFile(p)){
                      return p.getParent();
                  }
                  return p;
              }).distinct();
            path = Stream.concat(cp, listEnvPath().map(Paths::get))
              .map(p -> {
                  try {
                      return findExecutable(p, matchName);
                  } catch (IOException e){
                      return null;
                  }
              }).filter(r -> r != null)
              .findFirst()
              .orElse(null);
        }
        return path;
    }

    protected static Path findExecutable(Path dir, String matchName) throws IOException{
        return Files.list(dir)
          .filter(p -> p.getFileName().toString().matches(matchName))
          .findFirst()
          .map(Path::toAbsolutePath)
          .orElse(null);
    }

    protected final Path program;
    protected final String programName;

    private Path workingDirectory;
    public String outStream = "null";
    public String errStream = "null";

    private ProcessBuilder builder;
    private Process process;
    private Integer exitCode;

    private boolean keepTempDir = false;

    public NativeExecutable(Path program){
        if (program == null){
            throw new RuntimeException(getClass().getName() + " not found program");
        } else if (!Files.exists(program) || Files.isDirectory(program)){
            // if program is directory, it also is executable in general case.
            throw new RuntimeException(getClass().getName() + " not found program : " + program);
        } else if (!Files.isExecutable(program)){
            throw new RuntimeException(getClass().getName() + " cannot execute program : " + program);
        }
        this.program = program;
        programName = program.getFileName().toString();
    }

    @Override
    public String extendHelpDoc(){
        String help;
        try {
            help = help();
        } catch (Throwable throwable){
            help = "(" + throwable + ")";
        }
        return HelpBuilder.DOC_TITLE
               + "Original Help"
               + HelpBuilder.DOC_BLOCK_START
               + help
               + HelpBuilder.DOC_BLOCK_END;
    }

    public abstract String version() throws Throwable;

    public abstract String help() throws Throwable;

    @Override
    public void start() throws IOException{
        if (process == null){
            setupProcess();
        } else {
            throw new IllegalStateException("process is running");
        }
    }

    @Override
    public Integer call() throws Exception{
        start();
        exitCode = process.waitFor();
        return exitCode;
    }

    void setKeepingTempDirectory(boolean keep){
        keepTempDir = keep;
    }

    public Path setupWorkingDirectory(){
        if (workingDirectory == null){
            try {
                workingDirectory = Files.createTempDirectory(getClass().getName() + ".").toAbsolutePath();
                if (keepTempDir){
                    Log.debug(() -> programName + " temp : " + workingDirectory);
                } else {
                    TEMP_DIR.add(workingDirectory);
                }
            } catch (IOException e){
            }
        }
        return workingDirectory;
    }

    public void setupProcess() throws IOException{
        if (process == null){
            setupProcessBuilder();
            process = builder.start();
            Log.debug(() -> "~*r{" + programName + "~} ~*y{PID~} : " + getPID(process).orElse(-1));
        }
    }

    public void setupProcessBuilder(){
        if (builder == null){
            builder = createProcessBuilder();
        }
    }

    protected abstract ProcessBuilder createProcessBuilder();

    public boolean isAlive(){
        if (builder == null) throw new IllegalStateException();
        return process != null && process.isAlive();
//        if (process != null && process.isAlive()){
//            return true;
//        }
//        if (process == null){
//            Env.debug(() -> programName + " process null? " + (process == null));
//        } else {
//            Env.debug(() -> programName + " process alive? " + (process.isAlive()));
//        }
//        return false;
    }

    protected ProcessBuilder createProcessBuilder(String... args){
        return createProcessBuilder(Arrays.asList(args));
    }

    protected ProcessBuilder createProcessBuilder(List<String> args){
        List<String> a = new ArrayList<>(args);
        a.add(0, program.toString());
        ProcessBuilder builder = new ProcessBuilder(a);
        if (setupWorkingDirectory() != null){
            Log.debug(() -> program + " : " + workingDirectory);
            builder.directory(workingDirectory.toFile());
        }
        if (outStream != null){
            builder.redirectOutput(getStream(outStream));
        }
        if (errStream != null){
            builder.redirectError(getStream(errStream));
        }
        Log.debug(() -> "create process : " + args);
        return builder;
    }

    public Stream<String> getOutputSteam(){
        if (process == null) throw new IllegalStateException();
        return getOutputStream(process);
    }

    private static Stream<String> getOutputStream(Process p){
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        Iterator<String> it = new Iterator<String>(){
            boolean init;
            String nextLine;

            @Override
            public boolean hasNext(){
                try {
                    if (!init){
                        nextLine = reader.readLine();
                        if (nextLine != null){
                            init = true;
                        }
                    } else if (nextLine == null){
                        nextLine = reader.readLine();
                        if (nextLine == null){
                            reader.close();
                        }
                    }
                } catch (IOException e){
                }
                return nextLine != null;
            }

            @Override
            public String next(){
                if (nextLine == null) throw new NoSuchElementException();
                String ret = nextLine;
                nextLine = null;
                return ret;
            }
        };
        int c = Spliterator.IMMUTABLE | Spliterator.NONNULL;
        Stream<String> ret = StreamSupport.stream(Spliterators.spliteratorUnknownSize(it, c), false);
        ret.onClose(() -> {
            try {
                reader.close();
            } catch (IOException e){
            }
        });
        return ret;

    }

    protected String getProgramOutput(String... args) throws IOException{
        return getProgramOutput(Arrays.asList(args));
    }

    protected String getProgramOutput(List<String> args) throws IOException{
        List<String> a = new ArrayList<>(args);
        a.add(0, program.toString());
        Path tmp = Files.createTempDirectory(".tmp");
        ProcessBuilder builder = new ProcessBuilder(a);
        builder.directory(tmp.toFile());
        try (Stream<String> s = getOutputStream(builder.start())) {
            return s.collect(Collectors.joining("\n"));
        } finally {
            Files.delete(tmp);
        }
    }

    @Override
    public void close() throws IOException{
        if (process != null){
            Process ps = process;
            process = null;
            try {
                Log.debug(() -> "destroy : " + programName);
                while (ps.isAlive()){
                    ps.destroy();
                    try {
                        if (ps.waitFor(1, TimeUnit.SECONDS)){
                            break;
                        } else {
                            Log.debug(() -> "destroy again : " + programName);
                            ps.destroyForcibly();
                        }
                    } catch (InterruptedException e){
                        Log.debug(e);
                    }
                }
                exitCode = ps.exitValue();
            } finally {
                if (!keepTempDir && workingDirectory != null){
                    Log.debug(() -> "delete temp : " + workingDirectory);
                    deleteDirectoryRecursive(workingDirectory);
                }
            }
            if (exitCode == 0){
                Log.debug(() -> programName + " exit : 0");
            } else {
                int e = exitCode > 128? exitCode - 128: exitCode;
                Log.debug(() -> "~*r{" + programName + "~} ~*y{exit~} : " + exitCode +
                                " (" + SIGNAL_TEXT.getOrDefault(e, "unknown") + ")");
            }
        }
    }

    public OptionalInt exitCode(){
        if (exitCode == null) return OptionalInt.empty();
        return OptionalInt.of(exitCode);
    }

    private static void deleteDirectoryRecursive(Path path) throws IOException{
        ErrorFunction.forEach(Files.list(path), p -> {
            Path rp = path.resolve(p);
            if (Files.isDirectory(rp)){
                deleteDirectoryRecursive(rp);
            } else {
//                    System.err.println("delete " + rp);
                Files.delete(rp);
            }
        });
//            System.err.println("delete " + path);
        Files.delete(path);
    }

    private static OptionalInt getPID(Process ps){
        Class<? extends Process> cls = ps.getClass();
        try {
            Field field = cls.getDeclaredField("pid");
            field.setAccessible(true);
            return OptionalInt.of(field.getInt(ps));
        } catch (NoSuchFieldException | IllegalAccessException e){
        }
        return OptionalInt.empty();
    }
}
