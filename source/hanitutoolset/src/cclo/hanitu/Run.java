/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import cclo.hanitu.circuit.*;
import cclo.hanitu.data.*;
import cclo.hanitu.gui.FXMain;
import cclo.hanitu.util.ErrorFunction;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.WorldConfigLoader;
import cclo.hanitu.world.WorldConfigPrinter;
import cclo.hanitu.world.view.VirtualWorld;

import static cclo.hanitu.HanituInfo.WORLD_CONFIG_DEFAULT_FILENAME;

/**
 * @author antonio
 */
@HanituInfo.Description("Run native Flysim/Hanitu and plot")
public class Run extends Executable{

    @HanituInfo.Option(shortName = 'l', value = "location", arg = "FILE", order = 1)
    @HanituInfo.Description(resource = "cli", value = "run.option.location")
    public Path locationFile = Paths.get(HanituInfo.LOCATION_DEFAULT_FILENAME);

    @HanituInfo.Option(shortName = 's', value = "spike", arg = "FILE", order = 1)
    @HanituInfo.Description(resource = "cli", value = "run.option.spike")
    public Path spikeFile = Paths.get(HanituInfo.SPIKE_DEFAULT_FILENAME);

    @HanituInfo.Option(value = "timeout", arg = "TIME", order = 2)
    @HanituInfo.Description("set simulation timeout in unit ms")
    public int timeout = -1;

    @HanituInfo.Option(value = "loc-time-step", arg = "TIME", order = 2)
    @HanituInfo.Description("set the time step of the location data when outputting")
    public int locTimeStep = 2;

    @HanituInfo.Option(value = "no-plot", order = 3)
    @HanituInfo.Description("doesn't show hanitu plot")
    public boolean noPlot = false;

    @HanituInfo.Option(value = "backup", order = 3)
    @HanituInfo.Description("backup file for those program output file")
    public boolean backup = false;

    @HanituInfo.Option(value = "keep-temp", standard = false)
    @HanituInfo.Description("keep temp directory after executable terminated")
    public boolean keepTemp = false;

    @HanituInfo.Option(value = "eval-world", standard = false)
    @HanituInfo.Description("eval program flow as called by hanitu_world")
    public boolean evalWorld = false;

    @HanituInfo.Option(value = "keep-daemon", standard = false)
    @HanituInfo.Description("keep flysim as daemon instead of killing it when closing the hanitu")
    public boolean keepFlysimDaemon;

    @HanituInfo.Argument(index = 0, value = "FILE")
    @HanituInfo.Description(resource = "cli", value = "run.option.world")
    public String worldConfigFileName = WORLD_CONFIG_DEFAULT_FILENAME;
    private Path worldConfigDir;
    private WorldConfig worldConfig;
    private String worldConfigFileVersion;
    //
    private Flysim processFlysim;
    private Hanitu processHanitu;
    private DataLoader<LocationData> locationLoader;
    public VirtualWorld plotFrame;
    private boolean loopFlag;

    public WorldConfig setupWorldConfig() throws IOException{
        if (worldConfig == null){
            Path p = Paths.get(worldConfigFileName);
            if (!Files.isRegularFile(p)){
                return null;
            }
            worldConfigDir = p.toAbsolutePath().getParent();
            WorldConfigLoader loader = new WorldConfigLoader(true);
            worldConfig = loader.load(p);
            worldConfigFileVersion = loader.getSourceVersion();
        }
        return worldConfig;
    }

    public void setupWorldConfig(Path workingDir, WorldConfig wc){
        worldConfigDir = Objects.requireNonNull(workingDir).toAbsolutePath();
        worldConfig = wc;
        worldConfigFileName = null;
    }

    public void setupWorldConfig(WorldConfig wc){
        worldConfigDir = Paths.get(System.getProperty("user.dir"));
        worldConfig = wc;
        worldConfigFileName = null;
    }

    private static void backup(Path file) throws IOException{
        if (Files.exists(file)){
            Path dir = file.toAbsolutePath().getParent();
            Path targetFile;
            String filename = file.getFileName().toString();
            int i = 0;
            while (Files.exists(targetFile = dir.resolve(filename + "." + (i++)))) ;
            Log.debug("backup : " + file + " -> " + targetFile);
            Files.move(file, targetFile);
        }
    }

    public Hanitu setupHanitu() throws IOException{
        if (processHanitu == null){
            if (setupWorldConfig() == null){
                throw new RuntimeException("world config file path not set or file not found : "
                                           + worldConfigFileName);
            }
            processHanitu = new Hanitu();
            if (keepTemp){
                processHanitu.setKeepingTempDirectory(true);
            }
            if (worldConfigFileName != null && !evalWorld){
                if (isWorldConfigFileVersionCompatibility()){
                    processHanitu.worldConfigPath = Paths.get(worldConfigFileName);
                } else {
                    Log.debug(() -> "~*r{version not match~} : world config = " + worldConfigFileVersion
                                    + ", ~*y{but require~} : " + Hanitu.REQUIRE_WORLD_CONFIG_VERSION);
                    processHanitu.worldConfigPath = writeTempWorldConfig();
                }
            } else if (worldConfig != null || evalWorld){
                processHanitu.worldConfigPath = writeTempWorldConfig();
            }
            processHanitu.locationOutputPath = locationFile;
            processHanitu.spikeOutputPath = spikeFile;
            processHanitu.timeout = timeout;
            processHanitu.locTimeStep = locTimeStep;
            if (backup){
                Log.debug(() -> "backup : " + locationFile);
                Log.debug(() -> "backup : " + spikeFile);
                backup(locationFile);
                backup(spikeFile);
            } else {
                Log.debug(() -> "delete : " + locationFile);
                Log.debug(() -> "delete : " + spikeFile);
                Files.deleteIfExists(locationFile);
                Files.deleteIfExists(spikeFile);
            }
            processHanitu.setupProcessBuilder();
            //
            ErrorFunction.forEach(Files.list(setupFlysim().setupWorkingDirectory()), path -> {
                Log.debug(() -> "delete : " + path);
                Files.delete(path);
            });
        }
        return processHanitu;
    }

    private boolean isWorldConfigFileVersionCompatibility(){
        return FileLoader.isVersionCompatibility(worldConfigFileVersion,
                                                 new WorldConfigLoader()
                                                   .getImplByVersion(Hanitu.REQUIRE_WORLD_CONFIG_VERSION)
                                                   .versionRequired());
    }

    private Path writeTempWorldConfig() throws IOException{
        Path tempDir = processHanitu.setupWorkingDirectory();
        Path temp = tempDir.resolve(HanituInfo.WORLD_CONFIG_DEFAULT_FILENAME);
        Log.debug(() -> "write world config to : " + tempDir);
        WorldConfigPrinter printer = new WorldConfigPrinter();
        printer.setTargetVersion(Hanitu.REQUIRE_WORLD_CONFIG_VERSION);
        printer.write(temp, worldConfig);
        ErrorFunction.forEach(worldConfig.worms().stream()
                                .map(w -> w.circuitFileName)
                                .distinct(), c -> {
            Path s = worldConfigDir.resolve(c);
            Path t = tempDir.resolve(s.getFileName());
//            Env.debug(() -> "copy : " + s + " -> " + tempDir);
//            Files.copy(s, tempDir.resolve(s.getFileName()));
            Log.debug(() -> "translate : " + s + " -> " + t);
            CircuitTranslator.translate(s, null, t, Hanitu.REQUIRE_CIRCUIT_VERSION, true);
        });
        return temp;
    }

    public Flysim setupFlysim() throws IOException{
        if (processFlysim == null){
            processFlysim = Flysim.getInstance();
            if (keepTemp){
                processFlysim.setKeepingTempDirectory(true);
            }
            processFlysim.setupProcessBuilder();
        }
        return processFlysim;
    }

    private boolean waitNativeProgramRunning(long wait){
        long start = System.currentTimeMillis();
        while (!Files.exists(locationFile)){
            if (System.currentTimeMillis() - start > wait){
                return false;
            }
        }
        return true;
    }

    public DataLoader<LocationData> setupLocationLoader() throws IOException{
        if (locationLoader == null){
            locationLoader = new LocationLoader.Stream(locationFile);
        }
        return locationLoader;
    }

//    public SpikeSplitter setupSpikeLoader() throws IOException{
//        if (spikeLoader == null){
//            spikeLoader = new SpikeSplitter(new SpikeLoader.Stream(spikeFile));
//        }
//        return spikeLoader;
//    }

    public VirtualWorld setupPlot() throws IOException{
        if (plotFrame == null){
            plotFrame = new VirtualWorld();
            plotFrame.setCanvas(Plot.getCanvasStyle(null));
            plotFrame.setWorldConfig(setupWorldConfig());
            plotFrame.setLoader(setupLocationLoader());
            plotFrame.quitEvent.add(() -> loopFlag = false);
        }
        return plotFrame;
    }

    @Override
    public void start() throws IOException{
        setupWorldConfig();
        setupFlysim();
        setupHanitu();
        processFlysim.start();
        processHanitu.start();
        try {
            loopFlag = true;
            while (!waitNativeProgramRunning(100)){
                if (!isNativeProcessAlive()){
                    loopFlag = false;
                }
            }
            if (!loopFlag){
                Log.debug("closing before start");
            } else if (noPlot){
                Log.debug("start in no plot mode");
                startInNoPlotMode();
            } else {
                Log.debug("start in plot mode");
                startInPlotMode();
            }
        } catch (IOException e){
            throw new RuntimeException(e);
        } finally {
            Log.debug("closing");
            close();
        }
    }

    private void startInNoPlotMode() throws IOException{
        DataLoader<LocationData> loader = setupLocationLoader();
        while (loopFlag){
            loopFlag = isNativeProcessAlive();
            while (loader.hasNext()){
                System.out.println(loader.next());
            }
        }
    }

    private void startInPlotMode() throws IOException{
        FXMain.launch(setupPlot());
    }

    private boolean isNativeProcessAlive(){
        if (processFlysim == null || processHanitu == null) return false;
        return processFlysim.isAlive() && processHanitu.isAlive();
    }

    public void close(){
        loopFlag = false;
        if (processHanitu != null){
            Hanitu ps = processHanitu;
            processHanitu = null;
            try {
                ps.close();
            } catch (IOException e){
            }
        }
        if (!keepFlysimDaemon && processFlysim != null){
            Flysim ps = processFlysim;
            processFlysim = null;
            try {
                ps.close();
            } catch (IOException e){
            }
        }
        if (locationLoader != null){
            DataLoader<LocationData> ll = this.locationLoader;
            locationLoader = null;
            ll.close();
        }
//        if (spikeLoader != null){
//            try {
//                spikeLoader.close();
//            } finally {
//                spikeLoader = null;
//            }
//        }
    }
}
