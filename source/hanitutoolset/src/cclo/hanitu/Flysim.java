/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author antonio
 */
@HanituInfo.Tip("flysim interface")
@HanituInfo.Description("~*y{WARNING~} : don't execute this program directly\n" +
                        "hanitu main program for simulating virtual neural network")
public class Flysim extends NativeExecutable{

    /**
     * property of the location of the flysim
     */
    public static final String PROPERTY_PATH = "cclo.hanitu.flysim";
    /**
     * property of the port of the flysim daemon.
     *
     * It depends on the native hanitu program.
     */
    public static final String PROPERTY_PORT = "cclo.hanitu.flysim.port";
    /**
     * property of the output stream of the flysim
     */
    public static final String PROPERTY_OUT = "cclo.hanitu.flysim.out";
    /**
     * property of the error output stream of the flysim
     */
    public static final String PROPERTY_ERR = "cclo.hanitu.flysim.err";
    public static final Path PATH;

    static{
        /*
        in this block, it will execute when HanituBoot add shortcut with class
        this block will be execute before main(), that is error will be thrown without meaningful message.
        just print message to std err and shutdown system.
         */
        Path path = null;
        String property = System.getProperty(PROPERTY_PATH);
        if (property != null){
            path = Paths.get(property).toAbsolutePath().normalize();
        } else if (Base.HOME != null){
            try {
                path = findExecutable("flysim.*out");
            } catch (IOException e){
            }
        }
        PATH = path;
        if (PATH != null){
            Log.debug(() -> "~*r{find~} " + Flysim.class.getSimpleName() + " ~*y{at~} " + PATH);
        } else {
            Log.debug(() -> "~*r{un-find~} " + Flysim.class.getSimpleName());
        }
    }

    private static Flysim SELF;

    static{
        SHUT_DOWN_EVENT.add(() -> {
            if (SELF != null){
                try {
                    SELF.close();
                } catch (IOException e){
                }
            }
        });
    }

    @HanituInfo.Option(value = "daemon", arg = "PORT")
    @HanituInfo.Description("start in daemon mode and listen PORT")
    public int port = Integer.getInteger(PROPERTY_PORT, 8889);

    private Flysim(){
        super(PATH);
        outStream = System.getProperty(PROPERTY_OUT, "null");
        errStream = System.getProperty(PROPERTY_ERR, "null");
        Log.debug(() -> "flysim=" + PATH.getFileName());
        Log.debug(() -> "~*y{1>~} " + outStream);
        Log.debug(() -> "~*y{2>~} " + errStream);
    }

    public static synchronized Flysim getInstance(){
        if (SELF == null){
            SELF = new Flysim();
        }
        return SELF;
    }

    @Override
    public String version() throws Throwable{
        String output = getProgramOutput("-h");
        for (String line : output.split("\n")){
            if (line.startsWith("version")){
                return line.substring(8);
            }
        }
        return "unknown";
    }

    @Override
    public String help() throws Throwable{
        return getProgramOutput("-h");
    }

    @Override
    public void start() throws IOException{
        setupProcess();
    }

    @Override
    protected ProcessBuilder createProcessBuilder(){
        Log.debug(() -> "setup : " + Flysim.class.getName());
        return createProcessBuilder("-daemon", Integer.toString(port));
    }

    @Override
    public void close() throws IOException{
        super.close();
        SELF = null;
    }
}
