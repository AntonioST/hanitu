/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.IOException;
import java.nio.file.Path;

import cclo.hanitu.circuit.view.EditCircuitMode;
import cclo.hanitu.circuit.view.VisualCircuit;
import cclo.hanitu.gui.FXMain;

/**
 * @author antonio
 */
@HanituInfo.Tip("Circuit Builder")
@HanituInfo.Description("The java application used to build Hanitu Circuit file with GUI.")
public class Circuit extends Executable{

    @HanituInfo.Option(value = "new", arg = "FILE")
    @HanituInfo.Description("start a empty circuit with file name.")
    public Path newFileName = null;

    @HanituInfo.Option(value = "fail-on-load", standard = false)
    @HanituInfo.Description("stop program and print message if anr error during loading circuit file.")
    public boolean failOnLoad = false;

    @HanituInfo.Option(value = "read-only", standard = false)
    @HanituInfo.Description("display circuit under read-only mode, you cannot modify circuit")
    public boolean readOnly = false;

    @HanituInfo.Option(value = "lab-mode", arg = "FILE", standard = false)
    @HanituInfo.Description("~*y{WARNING~} : this feature is under development\n" + //XXX
                            "start lab mode, give spike data file to plotting firing rate change of the circuit")
    public boolean labMode;

    @HanituInfo.Argument(index = 0, value = "FILE")
    @HanituInfo.Description("circuit file, open a circuit from a file. omitted it will create a new empty circuit file.\n"
                            + "If use option ~*{--new~} without setting this argument, program will load this circuit "
                            + "without associating any file.")
    public Path circuitFilePath = null;
    public VisualCircuit visual;

    public VisualCircuit setupBuilder() throws IOException{
        if (visual == null){
            visual = new VisualCircuit();
            EditCircuitMode mode = null;
            if (!readOnly){
                mode = new EditCircuitMode();
                visual.setMode(mode);
            }
            if (newFileName != null && circuitFilePath != null){
                visual.setCircuit(circuitFilePath, failOnLoad);
                if (!readOnly){
                    mode.setSaveFileName(newFileName);
                }
            } else if (newFileName != null){
                // circuitFilePath == null
                visual.setCircuit(newFileName, failOnLoad);
                if (!readOnly){
                    mode.setSaveFileName(null);
                }
            } else if (circuitFilePath != null){
                // newFileName == null
                visual.setCircuit(circuitFilePath, failOnLoad);
            }
        }
        return visual;
    }

    @Override
    public void start() throws IOException{
        FXMain.launch(setupBuilder());
    }
}
