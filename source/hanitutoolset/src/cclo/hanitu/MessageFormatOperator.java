/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.WormData;

import java.util.Properties;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author antonio
 */
public class MessageFormatOperator{

    /**
     * match pattern : (j)ava.(u)til.ArrayList
     */
    private final static Pattern PATTERN_Q = Pattern.compile("(.).*?\\.");

    private static String WORLD_FORMAT_TIME;
    private static String WORLD_FORMAT_LOC;
    private static String WORLD_FORMAT_HP;

    static {
        WORLD_FORMAT_TIME = Base.getProperty("world.message.format.time", "%5.3f");
        WORLD_FORMAT_LOC = Base.getProperty("world.message.format.location", "%.1f");
        WORLD_FORMAT_HP = Base.getProperty("world.message.format.hp", "%.2f");
    }

    /**
     * message (fot List\<String>) unit format flag
     *
     * **format flag**
     *
     * Flag     | Description
     * ----     | -----------
     * %s       | message content
     *
     * @param message format target
     * @return result, null if flag not matched
     */
    public static Function<Character, String> unitFormatMessageOp(String message){
        return op -> {
            if (op == 's') return Message.echo(message);
            return null;
        };
    }

    /**
     * {@link StackTraceElement} (for List\<StackTraceElement>) unit format flag
     *
     * **format flag**
     *
     * Flag     | Description
     * ----     | -----------
     * %s       | toString
     * %M       | method name
     * %C       | class name
     * %F       | source file name
     * %N       | line number at source file
     *
     * @param element format target
     * @return result, null if flag not matched
     */
    public static Function<Character, String> unitFormatStackElementOp(StackTraceElement element){
        return op -> {
            switch (op){
            case 'Q':
                return element.getClassName();
            case 'C':
                return element.getClassName().replaceAll("^.*\\.", "");
            case 'q':
                return getReduceQName(element.getClassName());
            case 'c':{
                String c = element.getClassName().replaceAll("^.*\\.", "");
                int i = c.indexOf("$");
                if (i < 0) return c;
                return c.substring(0, i).replaceAll("[a-z]+", "") + "$" + c.substring(i + 1);
            }
            case 'M':
                return element.getMethodName();
            case 'm':{
                String name = element.getMethodName();
                if (name.startsWith("lambda")){
                    return "lambda:" + element.getLineNumber();
                } else if (name.length() < 10){
                    return name;
                }
                return getReduceMethodName(name);
            }
            case 'l':
                return Integer.toString(element.getLineNumber());
            case 'f':{
                String f = element.getFileName();
                return f == null? "???": f;
            }
            case '%':
                return "%";
            }
            return null;
        };
    }

    public static Function<Character, String> unitFormatThrowableOp(Throwable th){
        return op -> {
            switch (op){
            case 'Q':
                return th.getClass().getName();
            case 'C':
                return th.getClass().getName().replaceAll("^.*\\.", "");
            case 'q':
                return getReduceQName(th.getClass().getName());
            case 'c':{
                String c = th.getClass().getName().replaceAll("^.*\\.", "");
                int i = c.indexOf("$");
                if (i < 0) return c;
                return c.substring(0, i).replaceAll("[a-z]+", "") + "$" + c.substring(i + 1);
            }
            case 'M':{
                String message = th.getMessage();
                return message == null? "": message;
            }
            case 'm':{
                String message = th.getLocalizedMessage();
                return message == null? "": message;
            }
            case '%':
                return "%";
            }
            return null;
        };
    }

    public static Function<Character, String> unitFormatWorm(double time, WormData wd){
        return op -> {
            switch (op){
            case 't':
                return String.format(WORLD_FORMAT_TIME, time);
            case 'u':
                return Integer.toString(wd.user);
            case 'w':
                return Integer.toString(wd.worm);
            case 'x':
                return String.format(WORLD_FORMAT_LOC, wd.x);
            case 'y':
                return String.format(WORLD_FORMAT_LOC, wd.y);
            case 's':
                return Integer.toString(wd.size);
            case 'h':
                return String.format(WORLD_FORMAT_HP, wd.hp);
            }
            return null;
        };
    }

    public static Function<Character, String> unitFormatMolecule(double time, MoleculeData md){
        return op -> {
            switch (op){
            case 't':
                return String.format(WORLD_FORMAT_TIME, time);
            case 'm':
                return Integer.toString(md.id);
            case 'N':
                return md.type.name();
            case 'n':
                return md.type.name().toLowerCase();
            case 'x':
                return String.format(WORLD_FORMAT_LOC, md.x);
            case 'y':
                return String.format(WORLD_FORMAT_LOC, md.y);
            }
            return null;
        };
    }

    static String getReduceQName(String c){
        Matcher m = PATTERN_Q.matcher(c);
        if (m.find()){
            StringBuilder sb = new StringBuilder();
            int offset;
            do {
                sb.append(m.group(1));
                sb.append(".");
                offset = m.end();
            } while (m.find(offset));
            sb.append(c.substring(offset));
            c = sb.toString();
        }
        int i = c.indexOf("$");
        if (i < 0) return c;
        int j = c.lastIndexOf(".");
        if (j < 0){
            return c.substring(0, i).replaceAll("[a-z]+", "") + "$" + c.substring(i + 1);
        } else {
            return c.substring(0, j + 1)
                   + c.substring(j + 1, i).replaceAll("[a-z]+", "")
                   + "$" + c.substring(i + 1);
        }
    }

    static String getReduceMethodName(String name){
        StringBuilder sb = new StringBuilder();
        for (int i = 0, len = name.length(); i < len; i++){
            char c = name.charAt(i);
            if (Character.isUpperCase(c) || c == '_'){
                if (sb.length() == 0){
                    sb.append(name.substring(0, i));
                }
                if (c == '_'){
                    if (i + 1 < len){
                        sb.append(Character.toUpperCase(name.charAt(++i)));
                    }
                } else {
                    sb.append(c);
                }
            }
        }
        if (sb.length() == 0) return name;
        return sb.toString();
    }
}
