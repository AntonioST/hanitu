/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.*;
import java.util.Properties;

import cclo.hanitu.gui.UIType;

/**
 * the static, global information of the Hanitu Tool Set. And defined information annotation.
 *
 * @author antonio
 */
public class HanituInfo{

    public static final String HANITU_TOOL_SET_NAME = Base.getProperty("name.toolset", "Hanitu Tool Set");
    public static final String WORLD_CONFIG_EXTEND_FILENAME;
    public static final String CIRCUIT_EXTEND_FILENAME;
    public static final String LOCATION_EXTEND_FILENAME;
    public static final String SPIKE_EXTEND_FILENAME;
    public static final String WORLD_CONFIG_DEFAULT_FILENAME;
    public static final String LOCATION_DEFAULT_FILENAME;
    public static final String SPIKE_DEFAULT_FILENAME;

    static{
        Properties p = Base.loadPropertyInherit("cli");
        WORLD_CONFIG_EXTEND_FILENAME = p.getProperty("filter.world.extend");
        CIRCUIT_EXTEND_FILENAME = p.getProperty("filter.circuit.extend");
        LOCATION_EXTEND_FILENAME = p.getProperty("filter.location.extend");
        SPIKE_EXTEND_FILENAME = p.getProperty("filter.spike.extend");
        WORLD_CONFIG_DEFAULT_FILENAME = p.getProperty("filter.world.default");
        LOCATION_DEFAULT_FILENAME = p.getProperty("filter.location.default");
        SPIKE_DEFAULT_FILENAME = p.getProperty("filter.spike.default");
    }

    /**
     * actually program version. loading from jar resource.
     */
    public static final String VERSION;
    public static final String TOOLSET_VERSION;

    static{
        String line;
        try (InputStream is = ClassLoader.getSystemResourceAsStream("META-INF/version");
             BufferedReader r = new BufferedReader(new InputStreamReader(is))) {
            line = r.readLine();
        } catch (IOException | NullPointerException e){
            line = Base.getProperty("version");
        }
        VERSION = line;
        try (InputStream is = ClassLoader.getSystemResourceAsStream("META-INF/toolset.version");
             BufferedReader r = new BufferedReader(new InputStreamReader(is))) {
            line = r.readLine();
        } catch (IOException | NullPointerException e){
            line = Base.getProperty("version.toolset");
        }
        TOOLSET_VERSION = line;
    }

    /**
     * version description.
     */
    public static final String VERSION_DESCRIPTION = HANITU_TOOL_SET_NAME + " " + VERSION;

    /**
     * web-site information
     */
    public static final String[][] WEB_SITE;

    static {
        int count = Base.getIntegerProperty("website.count", 0);
        WEB_SITE = new String[count][2];
        for (int i = 0; i < count; i++){
            WEB_SITE[i][0] = Base.getProperty("website." + i + ".name");
            WEB_SITE[i][1] = Base.getProperty("website." + i + ".url");
        }
    }

    /**
     * authors information
     */
    public static final String[][] AUTHORS;

    static {
        int count = Base.getIntegerProperty("author.count", 0);
        AUTHORS = new String[count][2];
        for (int i = 0; i < count; i++){
            AUTHORS[i][0] = Base.getProperty("author." + i + ".name");
            AUTHORS[i][1] = Base.getProperty("author." + i + ".mail");
        }
    }
    /**
     * license name
     */
    public static final String LICENSE = "GPLv3";
    /**
     * development group description
     */
    public static final String DEVELOPER_DESCRIPTION
      = "This application is developed by CCLo lab, National Tsing Hua University, Taiwan.";


    /**
     * the description of the class or a field.
     */
    @Target({ElementType.FIELD, ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface Description{

        String value();

        String resource() default "";
    }

    /**
     * the tooltip to a field.
     */
    @Target({ElementType.FIELD, ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface Tip{

        String value();

        String resource() default "";
    }

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface Usage{

        String[] value();

        String[] description() default {};
    }

    /**
     * option annotation.
     */
    @Target({ElementType.FIELD, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Repeatable(ExtendOptions.class)
    @Documented
    public @interface Option{

        /**
         * name, which length large than 1.
         *
         * @return long name
         */
        String value() default "";

        /**
         * name, whose length equal to 1.
         *
         * @return short name
         */
        char shortName() default 0;

        /**
         * option's argument. Only support zero or one argument.
         *
         * @return the name of the argument.
         */
        String arg() default "";

        /**
         * option group and order in the help document.
         *
         * @return
         */
        int order() default -1;

        /**
         * The description document of this options. You also can use {@link Description} instead,
         * but {@link Description} cannot repeat when you annotation {@link Option}
         * on a method.
         *
         * @return document
         */
        String description() default "";

        /**
         * is this option is standard options which will present in the help document.
         *
         * @return
         */
        boolean standard() default true;
    }

    /**
     * argument annotation.
     */
    @Target({ElementType.FIELD, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Repeatable(ExtendArguments.class)
    @Documented
    public @interface Argument{

        /**
         * @return argument place
         */
        int index();

        /**
         * @return argument name
         */
        String value();

        /**
         * just work for help document printing. please check the value by yourself.
         *
         * @return is required?
         */
        boolean required() default false;

        /**
         * The description document of this argument. You also can use {@link Description} instead,
         * but {@link Description} cannot repeat when you annotation {@link Option}
         * on a method.
         *
         * @return document
         */
        String description() default "";
    }

    /**
     * {@link Option} collector on a method
     */
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface ExtendOptions{

        Option[] value();
    }

    /**
     * {@link Argument} collector on a method.
     */
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface ExtendArguments{

        Argument[] value();
    }

    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface GroupField{

        String value();

        String simpleName() default "";

        String resource() default "";
    }

    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface VTextField{
    }

    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface VValueField{

        /**
         * @return unit used by this field
         */
        String value() default "";

        String resource() default "";
    }

    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface VBooleanField{

        /**
         * two stage switch text.
         *
         * @return text content
         */
        String[] value() default {"off", "on"};

        String resource() default "";
    }

    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface VArrayField{

        /**
         * @return text content
         */
        String[] value();

        String resource() default "";
    }

    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface VUIField{

        UIType value() default UIType.DEFAULT;
    }

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface VAction{

        String value() default "";
    }
}
