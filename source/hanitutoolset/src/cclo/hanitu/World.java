/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import cclo.hanitu.gui.FXMain;
import cclo.hanitu.world.view.WorldBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

/**
 * @author antonio
 */
@HanituInfo.Tip("World Configuration Builder")
@HanituInfo.Description("The java application used to build Hanitu World config file.")
public class World extends Executable{

    @HanituInfo.Option(value = "fail-on-load", standard = false)
    @HanituInfo.Description("stop program and print message if any error during loading world config file.")
    public boolean failOnLoad = false;

    @HanituInfo.Option(value = "run-options", arg = "OPTIONS", standard = false)
    @HanituInfo.Description("the options used by ~*g{hanitu_run~}*, use ',' to spread each options")
    public String runOptions;

    @HanituInfo.Argument(index = 0, value = "FILE")
    @HanituInfo.Description("world configuration file, default it will create a new empty configuration file.")
    public Path worldConfigFilePath = null;
    public WorldBuilder builder;


    public WorldBuilder setupBuilder() throws IOException{
        if (builder == null){
            builder = new WorldBuilder();
            if (worldConfigFilePath != null){
                builder.setWorldConfig(worldConfigFilePath, failOnLoad);
            }
            if (runOptions != null){
                builder.setRunHanituOptions(Arrays.asList(runOptions.split(",")));
            }
        }
        return builder;
    }

    @Override
    public void start() throws IOException{
        FXMain.launch(setupBuilder());
    }
}
