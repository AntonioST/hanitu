/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import cclo.hanitu.util.Strings;

import static cclo.hanitu.Message.echo;

/**
 * Hanitu Runtime Environment.
 *
 * ### Output Header Flag
 *
 * Flag | Description                   | Example
 * ---- | -----------                   | -------
 * %%   | character `%`                 |
 * %Q   | fully qualified name          | java.util.ArrayList$Iter
 * %q   | simplify fully qualified name | j.u.ArrayList$Iter
 * %C   | class name                    | ArrayList$Iter
 * %c   | simplify class name           | AL$Iter
 * %M   | method name                   | thisMethodHaveLongName
 * %m   | simplify method name *        | thisMHLN
 * %l   | line number                   |
 * %f   | file name                     |
 * %T   | thread name                   |
 * %t   | alternative thread name       |
 *
 * __*__ : If method is a lambda method, use `'lambda'` instead.
 *
 * ### Alternative thread Name Table
 *
 * Alternative  | Origin
 * -----------  | ------
 * event        | \*AWT-EventQueue\*
 *
 * @author antonio
 */
public class Log{

    public static final OutputStream NULL = new OutputStream(){
        @Override
        public void write(int b) throws IOException{
        }

        @Override
        public void write(byte[] b) throws IOException{
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException{
        }
    };

    /**
     * property of debug output file.
     *
     * **output rule**
     *
     * Value    | Description
     * -----    | -----------
     * \-       | output to standard output steam
     * -2       | output to standard error stream
     * _file_   | output to a file
     * __@__ _file_   | output to file with random generated number
     * _file_ __&__ | append to a file
     * _null_   | drop output
     */
    public static final String PROPERTY_OUTPUT = "cclo.hanitu.output";
    private static final PrintStream OUTPUT_STREAM;

    static{
        String output = System.getProperty(PROPERTY_OUTPUT);
        if (output == null || output.equals("-2")){
            OUTPUT_STREAM = System.err;
        } else if (output.equals("-")){
            OUTPUT_STREAM = System.out;
        } else if (output.equals("null")){
            //NULL device
            OUTPUT_STREAM = new PrintStream(NULL);
        } else {
            PrintStream s;
            try {
                Path file;
                boolean append = output.endsWith("&");
                if (append){
                    output = output.substring(output.length() - 1);
                }
                if (output.startsWith("@")){
                    file = Files.createTempFile(output.substring(1), ".log");
                } else {
                    file = Paths.get(output);
                }
                StandardOpenOption o = append? StandardOpenOption.APPEND: StandardOpenOption.TRUNCATE_EXISTING;
                s = new PrintStream(Files.newOutputStream(file, StandardOpenOption.CREATE, o));
            } catch (IOException e){
                System.err.println(e.getMessage());
                System.err.println("redirect to standard error stream");
                s = System.err;
            }
            OUTPUT_STREAM = s;
            // set colorless
            Message.COLORLESS = true;
        }
        if(Base.osWin()){
            Message.COLORLESS = true;
        }
    }

    private static final String DEBUG_HEADER = Message.getMessage("head.debug", "[%t:%c.%m] \t");
    private static final String TRACE_HEADER = Message.getMessage("head.trace", "%Q.%M \t(%f :%l)");
    private static final String TRACE_TRACE_HEADER = Message.getMessage("trace.trace", "trace %Q : %M");
    private static final String TRACE_CAUSE_HEADER = Message.getMessage("trace.cause", "cause %Q : %M");
    private static final String TRACE_SUPPRESSED_HEADER = Message.getMessage("trace.suppressed",
                                                                             "  suppressed %Q : %M");

    private static final boolean TRACE_ALL = Base.getBooleanProperty("trace.all", false);
    private static final Map<String, String> TRACE_IGNORE_SET = new HashMap<>();

    static{
        for (String set : Base.getProperty("trace.ignore", "").split(";")){
            if (set.isEmpty()) continue;
            List<String> ls = Arrays.asList(set.split(","));
            ls.forEach(t -> TRACE_IGNORE_SET.put(t, ls.get(0)));
        }
    }

    private static String getIgnorePackageName(String name){
        return TRACE_IGNORE_SET.entrySet().stream()
          .filter(e -> name.startsWith(e.getKey()))
          .map(Map.Entry::getValue)
          .findFirst().orElse(null);
    }

    private static final Map<String, String> THREAD_MAP_SET = new HashMap<>();

    static{
        for (String ele : Base.getProperty("thread.map", "").split(";")){
            int i = ele.indexOf(":");
            if (i < 0){
                THREAD_MAP_SET.put(ele, ele);
            } else {
                THREAD_MAP_SET.put(ele.substring(0, i), ele.substring(i + 1));
            }
        }
    }

    /**
     * debug mode flag
     */
    public static boolean DEBUG = false;


    /**
     * format
     *
     * @param format
     * @return
     */
    public static String formatHead(String format){
        if (format == null || format.isEmpty()) return "";
        if (!format.contains("%")) return format;
        //0:currentThread
        //1:formatHead
        //2:verbose|debug
        //3:caller
        Thread currentThread = Thread.currentThread();
        Function<Character, String> f = MessageFormatOperator.unitFormatStackElementOp(
          currentThread.getStackTrace()[3]);
        Strings expr = new Strings();
        expr.setSingleReplace(op -> {
            switch (op){
            case 'T':
                return currentThread.getName();
            case 't':{
                String name = currentThread.getName();
                for (Map.Entry<String, String> e : THREAD_MAP_SET.entrySet()){
                    if (name.contains(e.getKey())) return e.getValue();
                }
                return name;
            }
            case '%':
                return "%";
            }
            return f.apply(op);
        });
        return expr.format(format);
    }

    /**
     * use default tracing header {@link #TRACE_HEADER} to format stack element `st`.
     *
     * @param st
     * @return
     */
    public static String formatStackTraceHead(StackTraceElement st){
        return formatStackTraceHead(TRACE_HEADER, st);
    }

    /**
     * use `format` to format stack element `st`.
     *
     * @param format
     * @param st
     * @return
     */
    public static String formatStackTraceHead(String format, StackTraceElement st){
        Strings expr = new Strings();
        expr.setSingleReplace(MessageFormatOperator.unitFormatStackElementOp(st));
        return expr.format(format == null? TRACE_HEADER: format);
    }

    public static String formatThrowable(String format, Throwable th){
        Strings expr = new Strings();
        expr.setSingleReplace(MessageFormatOperator.unitFormatThrowableOp(th));
        return expr.format(format);
    }

    private static String colorLine(String line, String color){
        int i;
        if (line.contains("\033")){
            return Message.COLORLESS? Message.unescape(line): line;
        } else if (Message.COLORLESS){
            return line;
        } else if ((i = line.indexOf(":")) >= 0){
        } else if ((i = line.indexOf("=")) >= 0){
        }
        if (i >= 0){
            String title = line.substring(0, i).trim();
            String value = line.substring(i + 1).trim();
            return Message.mapForegroundColor(color) + title + "\033[m " + line.charAt(i) + " " + value;
        } else {
            return Message.mapForegroundColor(color) + line + "\033[m";
        }
    }

    /**
     * debug message
     *
     * @param message
     */
    public static void debug(String message){
        if (DEBUG){
            String head = formatHead(DEBUG_HEADER);
            synchronized (OUTPUT_STREAM){
                for (String line : message.split("\n")){
                    OUTPUT_STREAM.print(head);
                    if (Message.containExpression(line)){
                        OUTPUT_STREAM.println(echo(line));
                    } else {
                        OUTPUT_STREAM.println(colorLine(line, "r"));
                    }
                }
            }
        }
    }

    /**
     * into debug mode and print something
     *
     * @param action
     */
    public static void debug(Consumer<PrintStream> action){
        if (DEBUG){
            String head = formatHead(DEBUG_HEADER);
            synchronized (OUTPUT_STREAM){
                OUTPUT_STREAM.print(head);
                action.accept(OUTPUT_STREAM);
            }
        }
    }

    /**
     * debug something with lazy execute.
     *
     * @param object
     */
    public static void debug(Supplier<?> object){
        if (DEBUG){
            String head = formatHead(DEBUG_HEADER);
            synchronized (OUTPUT_STREAM){
                for (String line : Objects.toString(object.get()).split("\n")){
                    OUTPUT_STREAM.print(head);
                    if (Message.containExpression(line)){
                        OUTPUT_STREAM.println(echo(line));
                    } else {
                        OUTPUT_STREAM.println(colorLine(line, "r"));
                    }
                }
            }
        }
    }

    public static void debug(Throwable th){
        if (DEBUG){
            String head = formatHead(DEBUG_HEADER);
            synchronized (OUTPUT_STREAM){
                OUTPUT_STREAM.print(head);
                OUTPUT_STREAM.println(Message.format("debug.class", th.getClass().getName()));
                OUTPUT_STREAM.println(Message.format("debug.message", th.getMessage()));
                Log.trace(OUTPUT_STREAM, th);
            }
        }
    }

    public static void debug(String message, Throwable th){
        if (DEBUG){
            String head = formatHead(DEBUG_HEADER);
            synchronized (OUTPUT_STREAM){
                OUTPUT_STREAM.print(head);
                OUTPUT_STREAM.println(Message.format("debug.class", th.getClass().getName()));
                OUTPUT_STREAM.println(Message.format("debug.title", message));
                OUTPUT_STREAM.println(Message.format("debug.message", th.getMessage()));
                Log.trace(OUTPUT_STREAM, th);
            }
        }
    }

    public static void debugTrace(String message){
        if (DEBUG){
            String head = formatHead(DEBUG_HEADER);
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            synchronized (OUTPUT_STREAM){
                OUTPUT_STREAM.print(head);
                OUTPUT_STREAM.println(Message.format("debug.title", message));
                trace(OUTPUT_STREAM, st, 2, st.length, 0);
            }
        }
    }

    /**
     * trace stack element
     *
     * @param ps output stream
     * @param st
     */
    public static void trace(PrintStream ps, StackTraceElement[] st){
        trace(ps, st, 0, st.length, 0);
    }

    public static void trace(PrintStream ps, StackTraceElement[] st, int start, StackTraceElement until, int level){
        synchronized (ps){
            String preIgnorePkg = null;
            int end = st.length;
            for (int i = start; i < end; i++){
                StackTraceElement s = st[i];
                String pkg;
                if (!TRACE_ALL && (pkg = getIgnorePackageName(s.getClassName())) != null){
                    if (preIgnorePkg == null || !preIgnorePkg.equals(pkg)){
                        for (int j = 0; j <= level; j++){
                            ps.print("  ");
                        }
                        ps.println(Message.format("trace.skip", pkg));
                        preIgnorePkg = pkg;
                    }
                } else {
                    for (int j = 0; j <= level; j++){
                        ps.print("  ");
                    }
                    ps.println(formatStackTraceHead(TRACE_HEADER, s));
                    preIgnorePkg = null;
                }
                if (s.getClassName().equals(until.getClassName()) && s.getMethodName().equals(until.getMethodName())){
                    break;
                }
            }
        }
    }

    /**
     * trace stack element
     *
     * @param ps    output stream
     * @param st    stack trace elements
     * @param start start index
     * @param end   end index
     * @param level indent level
     */
    public static void trace(PrintStream ps, StackTraceElement[] st, int start, int end, int level){
        synchronized (ps){
            String preIgnorePkg = null;
            for (int i = start; i < end; i++){
                StackTraceElement s = st[i];
                String pkg;
                if (!TRACE_ALL && (pkg = getIgnorePackageName(s.getClassName())) != null){
                    if (preIgnorePkg == null || !preIgnorePkg.equals(pkg)){
                        for (int j = 0; j <= level; j++){
                            ps.print("  ");
                        }
                        ps.println(Message.format("trace.skip", pkg));
                        preIgnorePkg = pkg;
                    }
                } else {
                    for (int j = 0; j <= level; j++){
                        ps.print("  ");
                    }
                    ps.println(formatStackTraceHead(TRACE_HEADER, s));
                    preIgnorePkg = null;
                }
            }
        }
    }

    public static void trace(PrintStream ps, Throwable th){
        synchronized (ps){
            Throwable cause = th;
            StackTraceElement until = null;
            String formatHead = TRACE_TRACE_HEADER;
            while (cause != null){
                ps.println(formatThrowable(formatHead, cause));
                StackTraceElement[] stackTrace = cause.getStackTrace();
                int startIndex = 0;
                int endIndex = stackTrace.length;
                if (cause instanceof StackOverflowError){
                    StackTraceElement last = stackTrace[0];
                    for (int i = 0; i < endIndex; i++){
                        if (stackTrace[i].equals(last)){
                            startIndex = i;
                        }
                    }
                    if (startIndex == 0){
                        trace(ps, stackTrace, 0, 1, 0);
                        last = stackTrace[1];
                        for (int i = 1; i < endIndex; i++){
                            if (stackTrace[i].equals(last)){
                                startIndex = i;
                            }
                        }
                    }
                    ps.println("  " + Message.getMessage("trace.stackoverflow"));
                }
                if (until == null){
                    trace(ps, stackTrace, startIndex, endIndex, 0);
                    formatHead = TRACE_CAUSE_HEADER;
                } else {
                    trace(ps, stackTrace, startIndex, until, 0);
                }
                until = cause.getStackTrace()[0];
                cause = cause.getCause();
            }
            for (Throwable e : th.getSuppressed()){
                ps.println(formatThrowable(TRACE_SUPPRESSED_HEADER, e));
                StackTraceElement[] st = e.getStackTrace();
                trace(ps, st, 0, st.length, 1);
            }
        }
    }

}
