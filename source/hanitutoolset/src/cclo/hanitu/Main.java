/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import static cclo.hanitu.Log.DEBUG;
import static cclo.hanitu.Log.debug;
import static cclo.hanitu.HanituInfo.Option;
import static cclo.hanitu.HelpBuilder.tryGetHelpDoc;

/**
 * Main class. Parsing command line argument and setting variable in {@link Executable} and run them
 *
 * @author antonio
 */
public class Main{

    /**
     * cause a interrupt (exception) when command-line argument contain `--help` or `-h`.
     */
    public static class HanituHelpInterruption extends RuntimeException{

        public static final int NORMAL = 0;
        public static final int NON_STANDARD = 1;

        public final int mode;

        public HanituHelpInterruption(){
            mode = NORMAL;
        }

        public HanituHelpInterruption(int mode){
            this.mode = mode;
        }
    }

    /**
     * cause a interrupt (exception) when command-line argument contain `--version`.
     */
    public static class HanituVersionInterruption extends RuntimeException{}

    /**
     * short-cut {@link Executable}. Use short-cut name as key map to correspond class.
     */
    static final Map<String, Class> SHORTCUT = new HashMap<>();
    static final Map<String, Class> SHORTCUT_HIDE = new HashMap<>();

    static{
        addShortCut(SHORTCUT, null, "cclo.hanitu.Circuit");
        addShortCut(SHORTCUT, null, "cclo.hanitu.World");
        addShortCut(SHORTCUT, null, "cclo.hanitu.Plot");
        addShortCut(SHORTCUT, null, "cclo.hanitu.Run");
        addShortCut(SHORTCUT, null, "cclo.hanitu.data.Filter");
        addShortCut(SHORTCUT, null, "cclo.hanitu.data.Firingrate");
        addShortCut(SHORTCUT, null, "cclo.hanitu.data.Concentration");
//        addShortCut(SHORTCUT, null, "cclo.hanitu.data.view.Chart");
        addShortCut(SHORTCUT_HIDE, "_hw", "cclo.hanitu.world.view.HelloWorld");
        addShortCut(SHORTCUT_HIDE, "_ct", "cclo.hanitu.circuit.CircuitTranslator");
        addShortCut(SHORTCUT_HIDE, "_wt", "cclo.hanitu.world.WorldConfigTranslator");
        addShortCut(SHORTCUT_HIDE, "_dl", "cclo.hanitu.data.DummyLocation");
        addShortCut(SHORTCUT_HIDE, "_ds", "cclo.hanitu.data.DummySpike");
        addShortCut(SHORTCUT_HIDE, "_nh", "cclo.hanitu.Hanitu");
        addShortCut(SHORTCUT_HIDE, "_nf", "cclo.hanitu.Flysim");
    }

    private static void addShortCut(Map<String, Class> map, String shortName, String className){
        try {
            Class<?> cls = Class.forName(className);
            if (!Executable.class.isAssignableFrom(cls)){
                System.err.println("class import error (not Executable) : " + className);
            } else {
                if (shortName == null){
                    map.put(cls.getSimpleName().toLowerCase(), cls);
                } else {
                    map.put(shortName, cls);
                }
            }
        } catch (ClassNotFoundException e){
            System.err.println("class import error : " + className);
        }
    }


    /**
     * main method.
     *
     * @param args command-line argument with class name at first place.
     */
    public static void main(String[] args){
        if (args.length == 0){
            tryGetHelpDoc(System.out, new Default());
            System.exit(0);
        } else {
            Executable run = null;
            Class cls;
            try {
                int startIndex = 0;
                if (SHORTCUT.containsKey(args[0])){
                    cls = SHORTCUT.get(args[0]);
                    startIndex = 1;
                } else if (SHORTCUT_HIDE.containsKey(args[0])){
                    cls = SHORTCUT_HIDE.get(args[0]);
                    startIndex = 1;
                } else if (args[0].startsWith("-")){
                    cls = Default.class;
                } else {
                    cls = Class.forName(args[0]);
                    startIndex = 1;
                }
                try {
                    run = (Executable)cls.newInstance();
                } catch (IllegalAccessException e){
                    try {
                        Method method = cls.getMethod("getInstance");
                        run = (Executable)method.invoke(null);
                    } catch (NoSuchMethodException | SecurityException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e1){
                        e.addSuppressed(e1);
                        throw e;
                    }
                }
                try {
                    List<String> ls = parseGlobalArguments(Arrays.asList(args), startIndex);
                    Log.debug(() -> "version : " + HanituInfo.VERSION);
                    ExecutableData.set(run, ls);
                    run.run();
                } catch (HanituHelpInterruption e){
                    Log.debug("help interruption");
                    switch (e.mode){
                    default:
                    case HanituHelpInterruption.NORMAL:
                        tryGetHelpDoc(System.out, run);
                        break;
                    case HanituHelpInterruption.NON_STANDARD:{
                        HelpBuilder help = new HelpBuilder();
                        Iterator<Option> it = ExecutableData.getOptionInfo(run.getClass()).stream()
                          .filter(op -> !op.standard())
                          .iterator();
                        while (it.hasNext()){
                            help.appendOption(System.out, it.next());
                        }
                        break;
                    }
                    }
                } catch (HanituVersionInterruption e){
                    Log.debug("version interruption");
                    if (run instanceof NativeExecutable){
                        try {
                            System.out.println(((NativeExecutable)run).version());
                        } catch (Throwable th){
                            debug(th);
                        }
                    } else {
                        System.out.println(HanituInfo.VERSION_DESCRIPTION);
                    }
                }
            } catch (Throwable e){
                if (DEBUG){
                    debug(e);
                } else {
                    e.printStackTrace();
                }
                System.exit(1);
            }
        }
    }

    private static List<String> parseGlobalArguments(List<String> args, int startIndex){
        Iterator<String> it = args.subList(startIndex, args.size()).iterator();
        ArrayList<String> ret = new ArrayList<>(args.size());
        while (it.hasNext()){
            String arg = it.next();
            switch (arg){
            case "-h":
            case "--help":
                throw new HanituHelpInterruption();
            case "--help!":
                throw new HanituHelpInterruption(HanituHelpInterruption.NON_STANDARD);
            case "--version":
                throw new HanituVersionInterruption();
            case "--debug":
                DEBUG = true;
                break;
            default:
                ret.add(arg);
                break;
            }
        }
        return ret;
    }
}
