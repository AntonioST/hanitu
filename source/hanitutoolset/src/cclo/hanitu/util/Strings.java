/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.util;

import java.lang.reflect.Field;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author antonio
 */
public class Strings{

    //language=RegExp
    private static final String VAR_NAME = "[a-zA-Z_][a-zA-Z_0-9]*";
    /**
     * pattern.
     *
     * match pattern
     * -------------
     *
     * * $ _NAME_
     * * ${ _NAME_ }
     * * ${ _NAME_ . _FIELD_}
     */
    private static final Pattern VAR = Pattern.compile("\\$(" + VAR_NAME
                                                       + "|\\{" + VAR_NAME + "}"
                                                       + "|\\{" + VAR_NAME + "\\?(\\\\.|[^}])+}"
                                                       + ")");
    private Function<Character, String> single;
    private BiFunction<String, StringBuilder, Integer> expression;

    /**
     * replace single flag.
     *
     * match pattern
     * -------------
     *
     * * % _c_
     *
     * @param single replace function, return null represent replace fail.
     */
    public void setSingleReplace(Function<Character, String> single){
        this.single = single;
    }

    /**
     * replace expression flag.
     *
     * match pattern
     * -------------
     *
     * * % _expression_
     *
     * replace function arguments
     * --------------------------
     *
     * 1. the string after matched character '%'.
     *
     * 2. the buffer used to get the result of replacing
     *
     * 3. (return value) the length of the original string (the first argument) will be replaced.
     *
     * @param expression replace function.
     */
    public void setReplace(BiFunction<String, StringBuilder, Integer> expression){
        this.expression = expression;
    }

    public String format(String format){
        return format(format, 0);
    }

    public String format(String format, int start){
        StringBuilder buffer = new StringBuilder();
        formatPrivate(buffer, format, start);
        return buffer.toString();
    }

    public StringBuilder formatAppend(StringBuilder builder, String format){
        formatPrivate(builder, format, 0);
        return builder;
    }

    int formatPrivate(StringBuilder buffer, String format, int offset){
        int next = 0;
        int length = format.length();
        while (offset < length && (next = format.indexOf("%", offset)) >= 0){
            if (offset != next){
                buffer.append(format.substring(offset, next));
            }
            char op = format.charAt(next + 1);
            if (single != null){
                String back = single.apply(op);
                if (back != null){
                    buffer.append(back);
                    offset = next + 2;
                    continue;
                }
            }
            if (expression != null){
                StringBuilder tmp = new StringBuilder();
                int ret = expression.apply(format.substring(next), tmp);
                if (ret > 0){
                    buffer.append(tmp);
                    offset = next + ret;
                    continue;
                }
            }
            throw new IllegalArgumentException("unknown format flag : %" + op + " in '" + format + "'");
        }
        if (next < length){
            buffer.append(format.substring(offset));
            next = length;
        }
        return next;
    }

    /**
     * use {@code regex} to search {@code input} and replace matched string with {@code map} which accept the content
     * of the match group 1. If {@code regex} doesn't have any match group, use matched string instead.
     *
     * @param regex regular expression.
     * @param input replaced string
     * @param map replace mapping function
     * @return replaced result
     * @throws PatternSyntaxException
     */
    public static String replace(String regex, String input, Function<String, String> map){
        Matcher m = Pattern.compile(regex).matcher(input);
        if (!m.find()) return input;
        StringBuilder buffer = new StringBuilder();
        int start = 0;
        do {
            buffer.append(input.substring(start, m.start()));
            String rep;
            if (m.groupCount() == 1){
                rep = map.apply(m.group(1));
            } else {
                rep = map.apply(m.group());
            }
            buffer.append(rep);
            start = m.end();
        } while (m.find(start));
        buffer.append(input.substring(start));
        return buffer.toString();
    }

    /**
     * use {@code regex} to match {@code input} and return the match group 1.
     * @param regex regular expression.
     * @param input matched string
     * @return match group 1, null if match fail.
     */
    public static String match(String regex, String input){
        Matcher match = Pattern.compile(regex).matcher(input);
        if (match.matches()){
            if (match.groupCount() > 1){
                return match.group(1);
            } else {
                return match.group();
            }
        }
        return null;
    }

    /**
     * delete matched string from buffer {@code input} with pattern {@code regex}.
     *
     * @param regex  regular expression.
     * @param input matched string
     * @return match group 1, null if match fail.
     */
    public static String delete(String regex, StringBuilder input){
        Matcher match = Pattern.compile(regex).matcher(input);
        if (match.find()){
            String ret;
            if (match.groupCount() > 0){
                ret = match.group(1);
            } else {
                ret = match.group();
            }
            input.delete(match.start(), match.end());
            return ret;
        }
        return null;
    }

    /**
     * enhance toString.
     *
     * @param target
     * @param input
     * @return
     */
    public static String r(Object target, String input){
        Matcher m = VAR.matcher(input);
        if (!m.find()) return input;
        StringBuilder builder = new StringBuilder();
        int start = 0;
        do {
            builder.append(input.substring(start, m.start()));
            String match = m.group();
            String value;
            if (!match.startsWith("${")){
                value = rToString(target, match.substring(1));
            } else {
                match = match.substring(2, match.length() - 1);
                if (match.contains("?")){
                    int i = match.indexOf("?");
                    value = rExpression(target, match.substring(0, i), match.substring(i + 1));
                } else {
                    value = rToString(target, match);
                }
            }
            builder.append(value);
            start = m.end();
        } while (m.find(start));
        builder.append(input.substring(start));
        return builder.toString();
    }

    private static String rExpression(Object target, String name, String expr){
        Object o = rField(target, name);
        if (o == null) return "";
        return expr.replaceAll("\\$", o.toString());
    }

    private static Object rField(Object target, String name){
        Class<?> cls = target.getClass();
        Field field;
        try {
            field = cls.getField(name);
        } catch (NoSuchFieldException e){
            try {
                field = cls.getDeclaredField(name);
                field.setAccessible(true);
            } catch (NoSuchFieldException e1){
                return null;
            }
        }
        try {
            return field.get(target);
        } catch (IllegalAccessException e){
        }
        return null;
    }

    private static String rToString(Object target, String name){
        return Objects.toString(rField(target, name), "");
    }


}
