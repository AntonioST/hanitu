/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.util;

import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author antonio
 */
public class ErrorFunction{

    @FunctionalInterface
    public interface Action<E extends Throwable>{

        void run() throws E;
    }

    @FunctionalInterface
    public interface EConsumer<T, E extends Throwable>{

        void accept(T t) throws E;
    }

    @FunctionalInterface
    public interface EBiConsumer<T, U, E extends Throwable>{

        void accept(T t, U u) throws E;
    }

    @FunctionalInterface
    public interface EFunction<T, R, E extends Throwable>{

        R accept(T t) throws E;
    }

    @FunctionalInterface
    public interface EBiFunction<T, U, R, E extends Throwable>{

        R accept(T t, U u) throws E;
    }

    @FunctionalInterface
    public interface EPredicate<T, E extends Throwable>{

        boolean accept(T t) throws E;
    }

    @FunctionalInterface
    public interface EBiPredicate<T, U, E extends Throwable>{

        boolean accept(T t, U u) throws E;
    }

    public static <T, E extends Throwable> void forEach(Stream<T> src, EConsumer<T, E> action) throws E{
        Objects.requireNonNull(action);
        List<E> errors = src.map(t -> {
            try {
                action.accept(t);
                return null;
            } catch (Throwable e){
                return (E)e;
            }
        }).filter(e -> e != null)
          .collect(Collectors.toList());
        if (!errors.isEmpty()){
            E e = errors.get(0);
            for (int i = 1, size = errors.size(); i < size; i++){
                e.addSuppressed(errors.get(i));
            }
            throw e;
        }
    }

    public static <T, R, E extends Throwable> Function<T, R> warpError(EFunction<T, R, E> function, R nil){
        return t -> {
            try {
                return function.accept(t);
            } catch (Throwable e){
                return nil;
            }
        };
    }

    public static <T, U, R, E extends Throwable> BiFunction<T, U, R> warpError(EBiFunction<T, U, R, E> function, R nil){
        return (t, u) -> {
            try {
                return function.accept(t, u);
            } catch (Throwable e){
                return nil;
            }
        };
    }

    public static <T, R, E extends Throwable> Predicate<T> warpError(EPredicate<T, E> function){
        return t -> {
            try {
                return function.accept(t);
            } catch (Throwable e){
                return false;
            }
        };
    }

    public static <T, U, R, E extends Throwable> BiPredicate<T, U> warpError(EBiPredicate<T, U, E> function){
        return (t, u) -> {
            try {
                return function.accept(t, u);
            } catch (Throwable e){
                return false;
            }
        };
    }
}
