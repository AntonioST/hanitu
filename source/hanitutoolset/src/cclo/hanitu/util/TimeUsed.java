/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.util;

/**
 * @author antonio
 */
public class TimeUsed{

    private long start;

    public long start(){
        start = System.currentTimeMillis();
        return start;
    }

    public long get(){
        return System.currentTimeMillis() - start;
    }

    public long step(){
        long current = System.currentTimeMillis();
        long ret = current - start;
        start = current;
        return ret;
    }
}
