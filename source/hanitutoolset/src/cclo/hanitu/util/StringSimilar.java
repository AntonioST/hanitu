/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.util;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author antonio
 */
public class StringSimilar{

    private static class Key{

        final int i;
        final int j;

        public Key(int i, int j){
            this.i = i;
            this.j = j;
        }

        @Override
        public boolean equals(Object o){
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Key key = (Key)o;

            return i == key.i && j == key.j;

        }

        @Override
        public int hashCode(){
            int result = i;
            result = 31 * result + j;
            return result;
        }

        @Override
        public String toString(){
            return "(" + i + "," + j + ")";
        }
    }

    private static final Map<String, Map<String, Integer>> CACHE = new HashMap<>();
    private final String s1;
    private final String s2;


    private StringSimilar(String s1, String s2){
        this.s1 = s1;
        this.s2 = s2;
    }

    public static double similar(String reference, String str){
        Objects.requireNonNull(reference);
        Objects.requireNonNull(str);
        int l1 = reference.length();
        int l2 = str.length();
        if (l1 == 0 && l2 == 0) return 1.0;
        if (l1 == 0 || l2 == 0) return 0.0;
        return CACHE.computeIfAbsent(reference, s1 -> new HashMap<>())
                 .computeIfAbsent(str, s1 -> new StringSimilar(reference, str).compute())
               / (double)Math.max(reference.length(), str.length());
    }

    public static String findSimilar(String reference, List<String> ls){
        return ls.stream()
          .collect(Collectors.toMap(Function.<String>identity(), str -> similar(reference, str)))
          .entrySet().stream()
          .max(Comparator.comparing(Map.Entry::getValue))
          .map(Map.Entry::getKey)
          .orElse(null);
    }

    private int compute(){
        int l1 = s1.length();
        int l2 = s2.length();
        return compute(new HashMap<>(), l1 - 1, l2 - 1);
    }

    private int compute(Map<Key, Integer> cache, int i, int j){
        if (i < 0 || j < 0){
            return 0;
        } else {
            int r1 = cache.computeIfAbsent(new Key(i - 1, j - 1), k -> compute(cache, i - 1, j - 1))
                     + ((s1.charAt(i) == s2.charAt(j))? 1: 0);
            int r2 = cache.computeIfAbsent(new Key(i, j - 1), k -> compute(cache, i, j - 1));
            int r3 = cache.computeIfAbsent(new Key(i - 1, j), k -> compute(cache, i - 1, j));
            return Math.max(r1, Math.max(r2, r3));
        }
    }

}
