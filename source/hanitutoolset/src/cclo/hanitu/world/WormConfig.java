/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import cclo.hanitu.HanituInfo.Description;
import cclo.hanitu.HanituInfo.Tip;
import cclo.hanitu.HanituInfo.VTextField;
import cclo.hanitu.HanituInfo.VValueField;

/**
 * @author antonio
 */
public class WormConfig implements Comparable<WormConfig>{

    /**
     * identify number, different user can shared same worm.
     */
    public int worm;
    /**
     * identify number for user
     */
    public int user;

    @Description(resource = "gui", value = "worm.x")
    @Tip(resource = "gui", value = "worm.x.tip")
    @VValueField(resource = "gui", value = "worm.x.unit")
    public int x;

    @Description(resource = "gui", value = "worm.y")
    @Tip(resource = "gui", value = "worm.y.tip")
    @VValueField(resource = "gui", value = "worm.y.unit")
    public int y;

    @Description(resource = "gui", value = "worm.size")
    @Tip(resource = "gui", value = "worm.size.tip")
    @VValueField(resource = "gui", value = "worm.size.unit")
    public int size = 3;

    @Description(resource = "gui", value = "worm.timedecay")
    @Tip(resource = "gui", value = "worm.timedecay.tip")
    @VValueField(resource = "gui", value = "worm.timedecay.unit")
    public double timeDecay = 100;

    @Description(resource = "gui", value = "worm.stepdecay")
    @Tip(resource = "gui", value = "worm.stepdecay.tip")
    @VValueField(resource = "gui", value = "worm.stepdecay.unit")
    public double stepDecay = 0.01;

    @Description(resource = "gui", value = "worm.circuit")
    @Tip(resource = "gui", value = "worm.circuit.tip")
    @VTextField
    public String circuitFileName;

    /**
     * create a worm with (program) default value
     */
    public WormConfig(){
    }

    /**
     * create a worm with copy value from reference
     *
     * @param user identify number for user
     * @param worm   identify number
     * @param wc   reference worm
     */
    public WormConfig(int user, int worm, WormConfig wc){
        this.user = user;
        this.worm = worm;
        if (wc != null){
            x = wc.x;
            y = wc.y;
            size = wc.size;
            timeDecay = wc.timeDecay;
            stepDecay = wc.stepDecay;
            circuitFileName = wc.circuitFileName;
        }
    }

    /**
     * is two worn contact each other?
     *
     * @param wc
     * @param other
     * @return true if contact
     */
    public static boolean isLocationOverlap(WormConfig wc, WormConfig other){
        int dx = wc.x - other.x;
        int dy = wc.y - other.y;
        int dd = wc.size + other.size;
        return dx * dx + dy * dy < dd * dd;
    }

    @Override
    public int compareTo(WormConfig o){
        int ret = Integer.compare(user, o.user);
        if (ret == 0) ret = Integer.compare(worm, o.worm);
        return ret;
    }

    @Override
    public int hashCode(){
        int hash = 5;
        hash = 19 * hash + worm;
        hash = 19 * hash + user;
        return hash;
    }

    @Override
    public boolean equals(Object obj){
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final WormConfig other = (WormConfig)obj;
        if (this.worm != other.worm) return false;
        return this.user == other.user;
    }
}
