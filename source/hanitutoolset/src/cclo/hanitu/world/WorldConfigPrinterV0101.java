/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.util.Objects;

import static cclo.hanitu.world.WorldConfigLoaderV0103.*;

/**
 * output world configuration, accept {@link  WorldConfig}.
 *
 * @author antonio
 */
public class WorldConfigPrinterV0101 extends WorldConfigPrinterImpl{


    @Override
    public String getTargetVersion(){
        return "1.1";
    }


    @Override
    protected void preWorm(){
        line(BLOCK_START_WORM);
    }

    @Override
    protected void processWorm(WormConfig worm){
        pair(USER, worm.user);
        pair(WORM_ID, worm.worm);
        pair(WORM_INIT_X, worm.x);
        pair(WORM_INIT_Y, worm.y);
        pair(WORM_SIZE, worm.size);
        pair(WORM_TIME_DECAY, worm.timeDecay);
        pair(WORM_STEP_DECAY, worm.stepDecay);
        pair(WORM_CIRCUIT_FILE, Objects.toString(worm.circuitFileName, ""));
    }

    @Override
    protected void postWorm(){
        line(BLOCK_END_WORM);
    }

    @Override
    protected void preWorld(){
        line(BLOCK_START_WORLD);
        line(BLOCK_START_WORLD_PARAMETER);
    }

    @Override
    protected void processWordParameter(WorldConfig world){
        pair(NUTRIENT, world.deltaHealthPoint);
        pair(TRANSFORM_A, world.gainFF);
        pair(TRANSFORM_B, world.baselineFF);
        //
        pair(WORLD_BOUNDARY, world.boundary);
        pair(WORLD_TYPE, world.type);
        pair(WORLD_DEPTH, world.depth);
        pair(FOOD_COUNT_MODE, world.fixFoodCount? 1: 0);
        pair(FIXED_MODE, world.fixWormLocation? 1: 0);
    }

    @Override
    protected void postWorld(){
        line(BLOCK_END_WORLD_PARAMETER);
        line(BLOCK_END_WORLD);
    }

    @Override
    protected void preFood(){
        line(BLOCK_START_FOOD);
    }

    @Override
    protected void processMolecule(MoleculeConfig molecule){
        if (molecule.type == MoleculeType.FOOD){
            pair(MOLECULE_FOOD_ID, molecule.id);
        } else if (molecule.type == MoleculeType.TOXICANT){
            pair(MOLECULE_TOXICANT_ID, molecule.id);
        } else {
            throw new RuntimeException();
        }
        pair(MOLECULE_X, molecule.x);
        pair(MOLECULE_Y, molecule.y);
        pair(MOLECULE_COUNT, molecule.count);
        pair(MOLECULE_DIFFUSE, molecule.diffuse);
        pair(MOLECULE_CONCENTRATION, molecule.concentration);
        pair(MOLECULE_DELAY, molecule.delay);

    }

    @Override
    protected void postFood(){
        line(BLOCK_END_FOOD);
    }

    @Override
    protected void preToxicant(){
        line(BLOCK_START_TOXICANT);
    }

    @Override
    protected void postToxicant(){
        line(BLOCK_END_TOXICANT);
    }
}
