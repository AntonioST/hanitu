/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.io.PrintStream;

import cclo.hanitu.FilePrinterImpl;

/**
 * output world configuration, accept {@link  WorldConfig}.
 *
 * @author antonio
 */
public abstract class WorldConfigPrinterImpl extends FilePrinterImpl<WorldConfig>{

    @Override
    public WorldConfigPrinter getPrinter(){
        return (WorldConfigPrinter)super.getPrinter();
    }

    /**
     * write all of the world config.
     *
     * @param world
     */
    @Override
    public void write(PrintStream out, WorldConfig world){
        begin();
        preWorm();
        for (WormConfig w : world.worms()){
            line();
            processWorm(w);
        }
        line();
        postWorm();

        preWorld();
        line();
        processWordParameter(world);
        line();

        preFood();
        world.molecule().stream()
          .filter(m -> m.type == MoleculeType.FOOD)
          .forEach(this::processMolecule);
        postFood();
        line();

        preToxicant();
        world.molecule().stream()
          .filter(m -> m.type == MoleculeType.TOXICANT)
          .forEach(this::processMolecule);
        postToxicant();
        line();

        postWorld();
    }

    /**
     * print meta header
     */
    public void begin(){
    }

    /**
     * begin to output Worms
     */
    protected void preWorm(){
    }

    /**
     * output the parameter in {@link WormConfig}
     *
     * @param worm
     */
    protected void processWorm(WormConfig worm){
    }

    /**
     * ending output worms
     */
    protected void postWorm(){
    }

    /**
     * begin to output world
     */
    protected void preWorld(){
    }

    /**
     * output the parameter in {@link WorldConfig}
     *
     * @param world
     */
    protected void processWordParameter(WorldConfig world){

    }

    /**
     * end to output world
     */
    protected void postWorld(){
    }

    /**
     * begin to output {@link MoleculeConfig} for {@link MoleculeType#FOOD }
     */
    protected void preFood(){
    }

    /**
     * output the parameter in {@link MoleculeConfig}
     *
     * @param molecule
     */
    protected void processMolecule(MoleculeConfig molecule){
    }

    /**
     * end to output {@link MoleculeConfig} for {@link MoleculeType#FOOD }
     */
    protected void postFood(){
    }

    /**
     * begin to output {@link MoleculeConfig} for {@link MoleculeType#TOXICANT }
     */
    protected void preToxicant(){
    }

    /**
     * end to output {@link MoleculeConfig} for {@link MoleculeType#TOXICANT }
     */
    protected void postToxicant(){
    }
}
