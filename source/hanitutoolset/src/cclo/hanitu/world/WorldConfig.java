/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import cclo.hanitu.HanituInfo.*;

/**
 * world configuration.
 *
 * @author antonio
 */
public class WorldConfig{

    public static final WorldConfig EMPTY = new WorldConfig();

    @Description(resource = "gui", value = "world.deltahp")
    @Tip(resource = "gui", value = "world.deltahp.tip")
    @VValueField
    public int deltaHealthPoint = 10;

    @Description(resource = "gui", value = "world.gainff")
    @Tip(resource = "gui", value = "world.gainff.tip")
    @GroupField(resource = "gui",value = "world.gainf", simpleName = "world.gainff.label")
    @VValueField
    public double gainFF = 10;

    @Description(resource = "gui", value = "world.baselineff")
    @Tip(resource = "gui", value = "world.baselineff.tip")
    @GroupField(resource = "gui",value = "world.baselinef", simpleName = "world.baselineff.label")
    @VValueField
    public double baselineFF = 5;

    @Description(resource = "gui", value = "world.gainft")
    @Tip(resource = "gui", value = "world.gainft.tip")
    @GroupField(resource = "gui",value = "world.gainf", simpleName = "world.gainft.label")
    @VValueField
    public double gainFT = 1;

    @Description(resource = "gui", value = "world.baselineft")
    @Tip(resource = "gui", value = "world.baselineft.tip")
    @GroupField(resource = "gui",value = "world.baselinef", simpleName = "world.baselineft.label")
    @VValueField
    public double baselineFT = 5;

    @Description(resource = "gui", value = "world.gaintf")
    @Tip(resource = "gui", value = "world.gaintf.tip")
    @GroupField(resource = "gui",value = "world.gaint", simpleName = "world.gaintf.label")
    @VValueField
    public double gainTF = 1;

    @Description(resource = "gui", value = "world.baselinetf")
    @Tip(resource = "gui", value = "world.baselinetf.tip")
    @GroupField(resource = "gui",value = "world.baselinet", simpleName = "world.baselinetf.label")
    @VValueField
    public double baselineTF = 5;

    @Description(resource = "gui", value = "world.gaintt")
    @Tip(resource = "gui", value = "world.gaintt.tip")
    @GroupField(resource = "gui",value = "world.gaint", simpleName = "world.gaintt.label")
    @VValueField
    public double gainTT = 10;

    @Description(resource = "gui", value = "world.baselinett")
    @Tip(resource = "gui", value = "world.baselinett.tip")
    @GroupField(resource = "gui",value = "world.baselinet", simpleName = "world.baselinett.label")
    @VValueField
    public double baselineTT = 5;

    @Description(resource = "gui", value = "world.boundary")
    @Tip(resource = "gui", value = "world.boundary.tip")
    @VValueField(resource = "gui", value = "world.boundary.unit")
    public int boundary = 100;

    @Description(resource = "gui", value = "world.type")
    @Tip(resource = "gui", value = "world.type.tip")
    @VArrayField(value = {"world.type.0", "world.type.1"}, resource = "gui")
    public int type = 0;

    @Description(resource = "gui", value = "world.depth")
    @Tip(resource = "gui", value = "world.depth.tip")
    @VValueField(resource = "gui", value = "world.depth.unit")
    public double depth = 0.264;

    @Description(resource = "gui", value = "world.fixfoodcount")
    @Tip(resource = "gui", value = "world.fixfoodcount.tip")
    @VBooleanField({"No", "Yes"})
    public boolean fixFoodCount = true;

    @Description(resource = "gui", value = "world.fixwormlocation")
    @Tip(resource = "gui", value = "world.fixwormlocation.tip")
    @VBooleanField({"No", "Yes"})
    public boolean fixWormLocation = false;
    //
    /**
     * molecule in world
     */
    private final List<MoleculeConfig> molecule = new ArrayList<>();
    /**
     * worms in world
     */
    private final List<WormConfig> worms = new ArrayList<>();

    /**
     * create a world with (program) default value
     */
    public WorldConfig(){
    }

    /**
     * create a world which copy from reference
     *
     * @param wc reference world
     */
    public WorldConfig(WorldConfig wc){
        deltaHealthPoint = wc.deltaHealthPoint;
        gainFF = wc.gainFF;
        gainFT = wc.gainFT;
        gainTF = wc.gainTF;
        gainTT = wc.gainTT;
        baselineFF = wc.baselineFF;
        baselineFT = wc.baselineFT;
        baselineTF = wc.baselineTF;
        baselineTT = wc.baselineTT;
        boundary = wc.boundary;
        type = wc.type;
        depth = wc.depth;
        fixFoodCount = wc.fixFoodCount;
        fixWormLocation = wc.fixWormLocation;
        wc.molecule.stream().map(f -> new MoleculeConfig(f.id, f)).forEach(molecule::add);
        wc.worms.stream().map(w -> new WormConfig(w.user, w.worm, w)).forEach(worms::add);
    }

    public List<WormConfig> worms(){
        return Collections.unmodifiableList(worms);
    }

    public List<MoleculeConfig> molecule(){
        return Collections.unmodifiableList(molecule);
    }

    public int userCount(){
        return (int)worms.stream().mapToInt(w -> w.user).distinct().count();
    }

    public int wormCount(){
        return worms.size();
    }

    public int foodCount(){
        return (int)molecule().stream().filter(m -> m.type == MoleculeType.FOOD).count();
    }

    public int toxicantCount(){
        return (int)molecule().stream().filter(m -> m.type == MoleculeType.TOXICANT).count();
    }

    /**
     * get worm in world
     *
     * @param user user identify number
     * @param id   worm identify number
     * @return worm, null if not found
     */
    public WormConfig getWorm(int user, int id){
        for (WormConfig w : worms){
            if (w.user == user && w.worm == id) return w;
        }
        return null;
    }

    /**
     * get molecule in the world
     *
     * @param type molecule type
     * @param id   identify number
     * @return food or toxicant or null if not found
     */
    public MoleculeConfig getMolecule(MoleculeType type, int id){
        for (MoleculeConfig m : molecule){
            if (m.type == type && m.id == id) return m;
        }
        return null;
    }

    public void addWorm(WormConfig config){
        if (getWorm(config.user, config.worm) != null){
            throw new RuntimeException("Worm ID duplicate : user=" + config.user + ", ID=" + config.worm);
        }
        worms.add(config);
        worms.sort(null);
    }

    public void addMolecule(MoleculeConfig config){
        if (getMolecule(config.type, config.id) != null){
            throw new RuntimeException("Molecule ID duplicate : type" + config.type + ", ID=" + config.id);
        }
        molecule.add(config);
        molecule.sort(null);
    }

    public WormConfig removeWorm(WormConfig config){
        return removeWorm(config.user, config.worm);
    }

    public WormConfig removeWorm(int user, int id){
        Iterator<WormConfig> it = worms.iterator();
        while (it.hasNext()){
            WormConfig next = it.next();
            if (next.user == user && next.worm == id){
                it.remove();
                return next;
            }
        }
        return null;
    }

    public MoleculeConfig removeMolecule(MoleculeConfig config){
        return removeMolecule(config.type, config.id);
    }

    public MoleculeConfig removeMolecule(MoleculeType type, int id){
        Iterator<MoleculeConfig> it = molecule.iterator();
        while (it.hasNext()){
            MoleculeConfig next = it.next();
            if (next.type == type && next.id == id){
                it.remove();
                return next;
            }
        }
        return null;
    }

    public void reassignID(){
        int user = 0;
        int count = 0;
        worms.sort(null);
        for (WormConfig worm : worms){
            if (worm.user != user){
                count = 0;
                user = worm.user;
            }
            worm.worm = count++;
        }
        MoleculeType type = MoleculeType.FOOD;
        count = 1;
        for (MoleculeConfig config : molecule){
            if (config.type != type){
                count = 1;
                type = config.type;
            }
            config.id = count++;
        }
    }
//
//    @Override
//    public boolean equals(Object o){
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        WorldConfig config = (WorldConfig)o;
//
//        if (deltaHealthPoint != config.deltaHealthPoint) return false;
//        if (Double.compare(config.gainFF, gainFF) != 0) return false;
//        if (Double.compare(config.baselineFF, baselineFF) != 0) return false;
//        if (Double.compare(config.gainFT, gainFT) != 0) return false;
//        if (Double.compare(config.baselineFT, baselineFT) != 0) return false;
//        if (Double.compare(config.gainTF, gainTF) != 0) return false;
//        if (Double.compare(config.baselineTF, baselineTF) != 0) return false;
//        if (Double.compare(config.gainTT, gainTT) != 0) return false;
//        if (Double.compare(config.baselineTT, baselineTT) != 0) return false;
//        if (boundary != config.boundary) return false;
//        if (type != config.type) return false;
//        if (Double.compare(config.depth, depth) != 0) return false;
//        if (fixFoodCount != config.fixFoodCount) return false;
//        if (fixWormLocation != config.fixWormLocation) return false;
//        if (!foods.equals(config.foods)) return false;
//        if (!toxicant.equals(config.toxicant)) return false;
//        return worms.equals(config.worms);
//
//    }
//
//    @Override
//    public int hashCode(){
//        int result;
//        long temp;
//        result = deltaHealthPoint;
//        temp = Double.doubleToLongBits(gainFF);
//        result = 31 * result + (int)(temp ^ (temp >>> 32));
//        temp = Double.doubleToLongBits(baselineFF);
//        result = 31 * result + (int)(temp ^ (temp >>> 32));
//        temp = Double.doubleToLongBits(gainFT);
//        result = 31 * result + (int)(temp ^ (temp >>> 32));
//        temp = Double.doubleToLongBits(baselineFT);
//        result = 31 * result + (int)(temp ^ (temp >>> 32));
//        temp = Double.doubleToLongBits(gainTF);
//        result = 31 * result + (int)(temp ^ (temp >>> 32));
//        temp = Double.doubleToLongBits(baselineTF);
//        result = 31 * result + (int)(temp ^ (temp >>> 32));
//        temp = Double.doubleToLongBits(gainTT);
//        result = 31 * result + (int)(temp ^ (temp >>> 32));
//        temp = Double.doubleToLongBits(baselineTT);
//        result = 31 * result + (int)(temp ^ (temp >>> 32));
//        result = 31 * result + boundary;
//        result = 31 * result + type;
//        temp = Double.doubleToLongBits(depth);
//        result = 31 * result + (int)(temp ^ (temp >>> 32));
//        result = 31 * result + (fixFoodCount? 1: 0);
//        result = 31 * result + (fixWormLocation? 1: 0);
//        result = 31 * result + foods.hashCode();
//        result = 31 * result + toxicant.hashCode();
//        result = 31 * result + worms.hashCode();
//        return result;
//    }
}
