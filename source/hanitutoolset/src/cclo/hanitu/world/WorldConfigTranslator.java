/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import cclo.hanitu.Executable;
import cclo.hanitu.HanituInfo;
import cclo.hanitu.HanituInfo.Argument;
import cclo.hanitu.HanituInfo.Description;
import cclo.hanitu.HanituInfo.Option;
import cclo.hanitu.HelpBuilder;

/**
 * @author antonio
 */
@HanituInfo.Tip("world config file translator")
@Description("The tool used to convert (translator) world config file between different version.")
public class WorldConfigTranslator extends Executable{

    @Option(shortName = 's', value = "source", arg = "VERSION", order = 0)
    @Description("set the source version of the ~_{SRC~} world config file. It will override the meta data of version")
    public String source;

    @Option(shortName = 't', value = "target", arg = "VERSION", order = 0)
    @Description("set the target version of the ~_{DEST~} world config file. default is the latest version.")
    public String target;

    @Option(value = "fail-on-load", order = 1)
    @Description("stop program and print message if fail during loading world config file.")
    public boolean failOnLoad;

    @Option(shortName = 'f', value = "force", order = 1)
    @Description("overwrite the ~_{DEST~} file if it is existed.")
    public boolean force;

    @Option(shortName = 'H', order = 1)
    @Description("use human readable keyword")
    public boolean humanReadable;

    @Argument(index = 0, value = "SRC", required = true)
    @Description("source world config file name. use '-' to read file from standard input stream")
    public String src;

    @Argument(index = 1, value = "DEST")
    @Description("destination world config file name. default is output to standard output stream.")
    public Path dest;

    @Override
    public String extendHelpDoc(){
        return HelpBuilder.DOC_TITLE + "Changeset for world configuration file" +
               HelpBuilder.DOC_NEWLINE +
               HelpBuilder.DOC_BLOCK_START +
               HelpBuilder.DOC_LIST_0 + "version 1.4" +
               HelpBuilder.DOC_BLOCK_START +
               HelpBuilder.DOC_LIST_1 + "rename the keyword and all of them are case insensitive." +
               HelpBuilder.DOC_BLOCK_END + HelpBuilder.DOC_NEWLINE +
               HelpBuilder.DOC_LIST_0 + "version 1.3" +
               HelpBuilder.DOC_BLOCK_START +
               HelpBuilder.DOC_LIST_1 + "add transform A/B FT, TF, TT" +
               HelpBuilder.DOC_BLOCK_END + HelpBuilder.DOC_NEWLINE +
               HelpBuilder.DOC_LIST_0 + "version 1.1" +
               HelpBuilder.DOC_BLOCK_START +
               HelpBuilder.DOC_LIST_1 + "rename Molecule to Toxicant" +
               HelpBuilder.DOC_BLOCK_END + HelpBuilder.DOC_NEWLINE +
               HelpBuilder.DOC_LIST_0 + "version 1" +
               HelpBuilder.DOC_BLOCK_START +
               HelpBuilder.DOC_LIST_1 + "original format" +
               HelpBuilder.DOC_BLOCK_END +
               HelpBuilder.DOC_BLOCK_END;
    }

    public Path getSourcePath(){
        if (src == null){
            throw new RuntimeException("lost SRC");
        } else if (src.equals("-")){
            return null;
        } else {
            Path path = Paths.get(src);
            if (!Files.exists(path)){
                throw new RuntimeException("file not found : " + src);
            } else if (!Files.isRegularFile(path)){
                throw new RuntimeException("not a file : " + src);
            }
            return path;
        }
    }

    public Path getDestPath(){
        if (dest == null){
            throw new RuntimeException("lost DEST");
        } else if (Files.exists(dest) && !force){
            throw new RuntimeException("file is existed : " + dest);
        }
        return dest;
    }

    public OutputStream getDestStream() throws IOException{
        if (dest == null) return System.out;
        return Files.newOutputStream(getDestPath(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    public WorldConfigLoader setupLoader(){
        WorldConfigLoader loader = new WorldConfigLoader(failOnLoad);
        if (source != null){
            loader.setSourceVersion(source);
        }
        return loader;
    }

    public WorldConfigPrinter setupPrinter(){
        WorldConfigPrinter printer = new WorldConfigPrinter();
        if (target != null){
            printer.setTargetVersion(target);
        }
        printer.setHumanReadable(humanReadable);
        return printer;
    }

    public WorldConfig getWorldConfig() throws IOException{
        Path sourcePath = getSourcePath();
        WorldConfigLoader loader = setupLoader();
        if (sourcePath != null){
            return loader.load(sourcePath);
        } else {
            return loader.load(System.in);
        }
    }

    @Override
    public void start() throws Throwable{
        Path sourcePath = getSourcePath();
        OutputStream os = getDestStream();
        WorldConfigLoader loader = setupLoader();
        WorldConfigPrinter printer = setupPrinter();
        try {
            WorldConfig config;
            if (sourcePath != null){
                config = loader.load(sourcePath);
            } else {
                config = loader.load(System.in);
            }
            printer.write(os, config);
        } finally {
            if (os != System.out){
                try {
                    os.close();
                } catch (IOException e){
                }
            }
        }
    }

    public static void translate(Path s, String srcVersion, Path t, String targetVersion, boolean failOnLoad)
      throws IOException{
        WorldConfigLoader loader = new WorldConfigLoader(failOnLoad);
        if (srcVersion != null){
            loader.setSourceVersion(srcVersion);
        }
        WorldConfigPrinter printer = new WorldConfigPrinter();
        if (targetVersion != null){
            printer.setTargetVersion(targetVersion);
        }
        WorldConfig config;
        if (s != null){
            config = loader.load(s);
        } else {
            config = loader.load(System.in);
        }
        printer.write(t, config);
    }
}
