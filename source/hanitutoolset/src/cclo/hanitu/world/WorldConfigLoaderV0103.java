/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.util.LinkedList;
import java.util.List;

import cclo.hanitu.FileLoader;
import cclo.hanitu.FileLoaderImpl;

import static cclo.hanitu.FileLoader.META_HEADER;

/**
 * @author antonio
 */
public class WorldConfigLoaderV0103 extends FileLoaderImpl<WorldConfig>{

    public static final String WORLD_CONFIG_VERSION = "1.3";

    /**
     * @since 1.0
     */
    public static final String BLOCK_START_WORM = "SetWormInf";
    /**
     * @since 1.0
     */
    public static final String BLOCK_END_WORM = "EndSetWormInf";
    /**
     * @since 1.0
     */
    public static final String USER = "UserID";
    /**
     * @since 1.0
     */
    public static final String WORM_ID = "WormID";
    /**
     * @since 1.0
     */
    public static final String WORM_INIT_X = "InitialX";
    /**
     * @since 1.0
     */
    public static final String WORM_INIT_Y = "InitialY";
    /**
     * @since 1.0
     */
    public static final String WORM_SIZE = "Wormsize";
    /**
     * @since 1.0
     */
    public static final String WORM_TIME_DECAY = "Time_decay";
    /**
     * @since 1.0
     */
    public static final String WORM_STEP_DECAY = "Step_decay";
    /**
     * @since 1.0
     */
    public static final String WORM_CIRCUIT_FILE = "Filename";
    //
    /**
     * @since 1.0
     */
    public static final String BLOCK_START_WORLD = "SetWorld";
    /**
     * @since 1.0
     */
    public static final String BLOCK_END_WORLD = "EndSetWorld";
    /**
     * @since 1.0
     */
    public static final String BLOCK_START_WORLD_PARAMETER = "WorldPar";
    /**
     * @since 1.0
     */
    public static final String BLOCK_END_WORLD_PARAMETER = "EndWorldPar";
    /**
     * @since 1.0
     */
    public static final String NUTRIENT = "Nutrient";
    /**
     * @since 1.0
     */
    public static final String TRANSFORM_A = "TransformA";
    /**
     * @since 1.0
     */
    public static final String TRANSFORM_B = "TransformB";
    /**
     * @since 1.0
     */
    public static final String WORLD_BOUNDARY = "Boundary";
    /**
     * @since 1.0
     */
    public static final String WORLD_TYPE = "Type";
    /**
     * @since 1.0
     */
    public static final String WORLD_DEPTH = "Depth";
    /**
     * @since 1.0
     */
    public static final String FOOD_COUNT_MODE = "CountMode";
    /**
     * @since 1.0
     */
    public static final String FIXED_MODE = "Fixed";

    /**
     * @since 1.0
     */
    public static final String BLOCK_START_FOOD = "FoodLocation";
    /**
     * @since 1.0
     */
    public static final String BLOCK_END_FOOD = "EndFoodLocation";
    /**
     *
     * *Notice*
     *
     * for hanitu version before 1.3, use keyword `MoleculeLocation`.
     *
     * @since 1.0
     */
    public static final String BLOCK_START_TOXICANT = "ToxicantLocation";
    /**
     *
     * *Notice*
     *
     * for hanitu version before 1.3, use keyword `EndMoleculeLocation`.
     *
     * @since 1.0
     */
    public static final String BLOCK_END_TOXICANT = "EndToxicantLocation";
    /**
     * @since 1.0
     */
    public static final String MOLECULE_FOOD_ID = "FID";
    /**
     *
     * *Notice*
     *
     * for hanitu version before 1.3, use keyword `CID`.
     *
     * @since 1.0
     */
    public static final String MOLECULE_TOXICANT_ID = "TID";
    /**
     * @since 1.0
     */
    public static final String MOLECULE_X = "X";
    /**
     * @since 1.0
     */
    public static final String MOLECULE_Y = "Y";
    /**
     * @since 1.0
     */
    public static final String MOLECULE_COUNT = "Count";
    /**
     * @since 1.0
     */
    public static final String MOLECULE_DIFFUSE = "Diffuse";
    /**
     * @since 1.0
     */
    public static final String MOLECULE_CONCENTRATION = "Concentration";
    /**
     * @since 1.0
     */
    public static final String MOLECULE_DELAY = "Delay_time";

    /**
     * @since 1.3
     */
    public static final String TRANSFORM_A_FF = "TransformA_FF";
    /**
     * @since 1.3
     */
    public static final String TRANSFORM_A_FT = "TransformA_FT";
    /**
     * @since 1.3
     */
    public static final String TRANSFORM_A_TF = "TransformA_TF";
    /**
     * @since 1.3
     */
    public static final String TRANSFORM_A_TT = "TransformA_TT";
    /**
     * @since 1.3
     */
    public static final String TRANSFORM_B_FF = "TransformB_FF";
    /**
     * @since 1.3
     */
    public static final String TRANSFORM_B_FT = "TransformB_FT";
    /**
     * @since 1.3
     */
    public static final String TRANSFORM_B_TF = "TransformB_TF";
    /**
     *
     * @since 1.3
     */
    public static final String TRANSFORM_B_TT = "TransformB_TT";

    private static final int WORM_STATE = 1;
    private static final int WORLD_STATE = 2;
    private static final int WORLD_PARA_STATE = 3;
    private static final int FOOD_STATE = 4;
    private static final int TOXICANT_STATE = 5;

    private WorldConfig world;
    private List<WormBlock> worms;
    private WormBlock worm;
    private MoleculeConfig molecule;

    private static class WormBlock{

        int line;
        private WormConfig config;

        public WormBlock(int line){
            this.line = line;
            config = new WormConfig();
        }
    }

    @Override
    public WorldConfigLoader getLoader(){
        return (WorldConfigLoader)super.getLoader();
    }

    @Override
    public String getSourceVersion(){
        return "1,3";
    }

    @Override
    public String versionRequired(){
        return "<=1.3";
    }

    @Override
    public void startLoading(FileLoader<WorldConfig> loader){
        if (!(loader instanceof WorldConfigLoader)){
            throw new IllegalArgumentException();
        }
        super.startLoading(loader);
        world = new WorldConfig();
        worms = new LinkedList<>();
        worm = null;
        molecule = null;
    }

    @Override
    public WorldConfig endLoading(){
        return world;
    }

    @Override
    public void parseLine(String line){
        switch (getCurrentState()){
        case INIT_STATE:
            parseInit(line);
            break;
        case WORM_STATE:
            parseWorms(line);
            break;
        case WORLD_STATE:
            parseWorld(line);
            break;
        case WORLD_PARA_STATE:
            parseWorldParameter(line);
            break;
        case FOOD_STATE:
        case TOXICANT_STATE:
            parseMolecule(line);
            break;
        }
    }


    private void setStateByKeyword(String line){
        switch (line){
        case BLOCK_START_TOXICANT:
            setState(TOXICANT_STATE);
            break;
        case BLOCK_START_FOOD:
            setState(FOOD_STATE);
            break;
        case BLOCK_START_WORLD:
            setState(WORLD_STATE);
            break;
        case BLOCK_START_WORLD_PARAMETER:
            setState(WORLD_PARA_STATE);
            break;
        case BLOCK_START_WORM:
            setState(WORM_STATE);
            break;
        }
    }

    protected void parseInit(String line){
        switch (line){
        case META_HEADER:
            break;
        case BLOCK_START_WORM:
            setState(WORM_STATE);
            break;
        case BLOCK_START_WORLD:
            setState(WORLD_STATE);
            break;
        default:
            //noinspection unchecked
            throwUnexpectedKeywordException(line);
        }
    }

    protected void parseWorms(String line){
        switch (getKey(line)){
        case META_HEADER:
            break;
        case USER:
            worm = new WormBlock(getLoader().getCurrentLineNumber());
            worm.config.user = getIntValue(line);
            worms.add(worm);
            break;
        case WORM_ID:
            worm.config.worm = getIntValue(line);
            break;
        case WORM_INIT_X:
            worm.config.x = getIntValue(line);
            break;
        case WORM_INIT_Y:
            worm.config.y = getIntValue(line);
            break;
        case WORM_SIZE:
            worm.config.size = getIntValue(line);
            break;
        case WORM_TIME_DECAY:
            worm.config.timeDecay = getFloatValue(line);
            break;
        case WORM_STEP_DECAY:
            worm.config.stepDecay = getFloatValue(line);
            break;
        case WORM_CIRCUIT_FILE:
            worm.config.circuitFileName = getLoader().checkCircuitFile(getValue(line));
            break;
        case BLOCK_START_WORLD:
        case BLOCK_START_FOOD:
        case BLOCK_START_TOXICANT:
        case BLOCK_START_WORM:
        case BLOCK_START_WORLD_PARAMETER:
            throwErrorKeywordException(BLOCK_END_WORM);
            setStateByKeyword(line);
            break;
        case BLOCK_END_TOXICANT:
        case BLOCK_END_FOOD:
        case BLOCK_END_WORLD:
            throwErrorKeywordException(BLOCK_END_WORM);
        case BLOCK_END_WORM:
            worms.forEach(w -> {
                try {
                    world.addWorm(w.config);
                } catch (RuntimeException e){
                    throwError(e.getMessage(),
                               "worm at line : " + w.line);
                }
            });
            setState(INIT_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(line,
                                                       USER,
                                                       WORM_ID,
                                                       WORM_INIT_X,
                                                       WORM_INIT_Y,
                                                       WORM_SIZE,
                                                       WORM_TIME_DECAY,
                                                       WORM_STEP_DECAY,
                                                       WORM_CIRCUIT_FILE,
                                                       BLOCK_END_WORM);
            assert r != null;
            getLoader().tryRescue(r, null);
        }
    }

    protected void parseWorld(String line){
        switch (line){
        case META_HEADER:
            break;
        case BLOCK_START_WORLD_PARAMETER:
            setState(WORLD_PARA_STATE);
            break;
        case BLOCK_END_WORLD:
            setState(TERMINAL_STATE);
            break;
        default:
            //noinspection unchecked
            throwUnexpectedKeywordException(line);
        }
    }

    protected void parseWorldParameter(String line){
        switch (getKey(line)){
        case META_HEADER:
            break;
        case NUTRIENT:
            world.deltaHealthPoint = getIntValue(line);
            break;
        case TRANSFORM_A:
        case TRANSFORM_A_FF:
            world.gainFF = getFloatValue(line);
            break;
        case TRANSFORM_A_FT:
            world.gainFT = getFloatValue(line);
            break;
        case TRANSFORM_A_TF:
            world.gainTF = getFloatValue(line);
            break;
        case TRANSFORM_A_TT:
            world.gainTT = getFloatValue(line);
            break;
        case TRANSFORM_B:
        case TRANSFORM_B_FF:
            world.baselineFF = getFloatValue(line);
            break;
        case TRANSFORM_B_FT:
            world.baselineFT = getFloatValue(line);
            break;
        case TRANSFORM_B_TF:
            world.baselineTF = getFloatValue(line);
            break;
        case TRANSFORM_B_TT:
            world.baselineTT = getFloatValue(line);
            break;
        case WORLD_BOUNDARY:
            world.boundary = getIntValue(line);
            break;
        case WORLD_TYPE:
            world.type = getIntValue(line);
            break;
        case WORLD_DEPTH:
            world.depth = getFloatValue(line);
            break;
        case FOOD_COUNT_MODE:
            world.fixFoodCount = getIntValue(line) != 0;
            break;
        case FIXED_MODE:
            world.fixWormLocation = getIntValue(line) != 0;
            break;
        case BLOCK_START_FOOD:
            setState(FOOD_STATE);
            break;
        case "MoleculeLocation":  // old version hanitu (<1.3) keyword
        case BLOCK_START_TOXICANT:
            setState(TOXICANT_STATE);
            break;
        case BLOCK_START_WORLD:
        case BLOCK_START_WORM:
        case BLOCK_START_WORLD_PARAMETER:
            throwErrorKeywordException(BLOCK_END_WORLD_PARAMETER);
            setStateByKeyword(line);
            break;
        case BLOCK_END_TOXICANT:
        case BLOCK_END_FOOD:
        case BLOCK_END_WORLD:
        case BLOCK_END_WORM:
            throwErrorKeywordException(BLOCK_END_WORLD_PARAMETER);
        case BLOCK_END_WORLD_PARAMETER:
            setState(WORLD_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(line,
                                                       NUTRIENT,
                                                       TRANSFORM_A,
                                                       TRANSFORM_A_FF,
                                                       TRANSFORM_A_FT,
                                                       TRANSFORM_A_TF,
                                                       TRANSFORM_A_TT,
                                                       TRANSFORM_B,
                                                       TRANSFORM_B_FF,
                                                       TRANSFORM_B_FT,
                                                       TRANSFORM_B_TF,
                                                       TRANSFORM_B_TT,
                                                       WORLD_BOUNDARY,
                                                       WORLD_TYPE,
                                                       WORLD_DEPTH,
                                                       FOOD_COUNT_MODE,
                                                       FIXED_MODE,
                                                       BLOCK_START_FOOD,
                                                       BLOCK_START_TOXICANT,
                                                       BLOCK_END_WORLD_PARAMETER);
            getLoader().tryRescue(r, null);
        }
    }

    protected void parseMolecule(String line){
        switch (getKey(line)){
        case META_HEADER:
            break;
        case MOLECULE_FOOD_ID:
            if (getCurrentState() == TOXICANT_STATE){
                throwErrorKeywordException(MOLECULE_TOXICANT_ID);
            }
            molecule = new MoleculeConfig();
            molecule.type = MoleculeType.FOOD;
            molecule.id = getIntValue(line);
            world.addMolecule(molecule);
            break;
        case "CID": // old version hanitu (<1.3) keyword
        case MOLECULE_TOXICANT_ID:
            if (getCurrentState() == FOOD_STATE){
                throwErrorKeywordException(MOLECULE_FOOD_ID);
            }
            molecule = new MoleculeConfig();
            molecule.type = MoleculeType.TOXICANT;
            molecule.id = getIntValue(line);
            world.addMolecule(molecule);
            break;
        case MOLECULE_X:
            molecule.x = getIntValue(line);
            break;
        case MOLECULE_Y:
            molecule.y = getIntValue(line);
            break;
        case MOLECULE_COUNT:
            molecule.count = getIntValue(line);
            break;
        case MOLECULE_DIFFUSE:
            molecule.diffuse = getFloatValue(line);
            break;
        case MOLECULE_CONCENTRATION:
            molecule.concentration = getIntValue(line);
            break;
        case MOLECULE_DELAY:
            molecule.delay = getIntValue(line);
            break;
        case BLOCK_END_FOOD:
            if (getCurrentState() == TOXICANT_STATE){
                throwErrorKeywordException(BLOCK_END_TOXICANT);
            }
            setState(WORLD_PARA_STATE);
            break;
        case "EndMoleculeLocation":  // old version hanitu (<1.3) keyword
        case BLOCK_END_TOXICANT:
            if (getCurrentState() == FOOD_STATE){
                throwErrorKeywordException(BLOCK_END_FOOD);
            }
            setState(WORLD_PARA_STATE);
            break;
        case BLOCK_START_WORLD:
        case BLOCK_START_FOOD:
        case BLOCK_START_TOXICANT:
        case BLOCK_START_WORM:
        case BLOCK_START_WORLD_PARAMETER:
            if (getCurrentState() == TOXICANT_STATE){
                throwErrorKeywordException(BLOCK_END_TOXICANT);
            } else if (getCurrentState() == FOOD_STATE){
                throwErrorKeywordException(BLOCK_END_FOOD);
            }
            setStateByKeyword(line);
            break;
        case BLOCK_END_WORLD:
        case BLOCK_END_WORM:
            if (getCurrentState() == TOXICANT_STATE){
                throwErrorKeywordException(BLOCK_END_TOXICANT);
            } else if (getCurrentState() == FOOD_STATE){
                throwErrorKeywordException(BLOCK_END_FOOD);
            } else {
                throw new IllegalArgumentException("un-reachable");
            }
            setState(WORLD_PARA_STATE);
            break;
        default:
            String r = throwUnexpectedKeywordException(line,
                                                       MOLECULE_FOOD_ID,
                                                       MOLECULE_TOXICANT_ID,
                                                       MOLECULE_X,
                                                       MOLECULE_Y,
                                                       MOLECULE_COUNT,
                                                       MOLECULE_DIFFUSE,
                                                       MOLECULE_CONCENTRATION,
                                                       MOLECULE_DELAY,
                                                       BLOCK_END_FOOD,
                                                       BLOCK_END_TOXICANT);
            assert r != null;
            getLoader().tryRescue(r, null);
        }
    }
}
