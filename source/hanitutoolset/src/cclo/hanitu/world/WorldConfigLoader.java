/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import cclo.hanitu.FileLoader;
import cclo.hanitu.FileLoaderImpl;

/**
 * The loader of world_config file.This class is reusable but not thread-safe.
 */
public class WorldConfigLoader extends FileLoader<WorldConfig>{

    /**
     * identify file type for meta header
     */
    public static final String WORLD_CONFIG_TYPE = "world_config";

    private static final List<Class<? extends FileLoaderImpl<WorldConfig>>> SUPPORT = new ArrayList<>();

    static{
        SUPPORT.add(WorldConfigLoaderV0103.class);
    }

    boolean ignoreCircuitFile;

    public WorldConfigLoader(){
    }

    public WorldConfigLoader(boolean failOnLoad){
        super(failOnLoad);
    }

    @Override
    protected boolean isFileTypeSupport(String value){
        return value.equals(WORLD_CONFIG_TYPE);
    }

    @Override
    public FileLoaderImpl<WorldConfig> getImplByVersion(String expr){
        FileLoaderImpl<WorldConfig> impl;
        for (Class<? extends FileLoaderImpl<WorldConfig>> cls : SUPPORT){
            if ((impl = isVersionCompatibility(expr, cls)) != null){
                return impl;
            }
        }
        return null;
    }

    @Override
    public FileLoaderImpl<WorldConfig> getImplByFileContent(List<String> content){
        return new WorldConfigLoaderV0103();
    }

    @Override
    public boolean isVersionSupport(String expr){
        return isVersionCompatibility(expr, "<=1.4");
    }


    public boolean isIgnoreCircuitFile(){
        return ignoreCircuitFile;
    }

    public void setIgnoreCircuitFile(boolean ignoreCircuitFile){
        this.ignoreCircuitFile = ignoreCircuitFile;
    }

    public String checkCircuitFile(String circuitFile){
        if (ignoreCircuitFile){
            if (circuitFile != null && circuitFile.isEmpty()) return null;
        } else if (circuitFile == null){
            getImpl().throwLostValueException();
        } else if (isFailOnLoad()){
            Path path;
            if (getFileDirectory() != null){
                path = getFileDirectory();
            } else {
                path = Paths.get(System.getProperty("user.dir"));
            }
            if (!Files.isRegularFile(path.resolve(circuitFile))){
                getImpl().throwException("file not found", "for file : " + circuitFile);
            }
        }
        return circuitFile;
    }
}
