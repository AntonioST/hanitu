/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.io.PrintStream;

import cclo.hanitu.FileLoader;
import cclo.hanitu.FilePrinter;
import cclo.hanitu.FilePrinterImpl;

import static cclo.hanitu.FileLoader.*;

/**
 * output world configuration, accept {@link  WorldConfig}.
 *
 * @author antonio
 */
public class WorldConfigPrinter extends FilePrinter<WorldConfig>{

    boolean humanReadable;

    @Override
    public FilePrinterImpl<WorldConfig> getImplWithTargetVersion(String version){
        if (FileLoader.isVersionCompatibility(version, ">=1.3")){
            return new WorldConfigPrinterV0103();
        } else if (FileLoader.isVersionCompatibility(version, ">=1.1")){
            return new WorldConfigPrinterV0101();
        } else if (FileLoader.isVersionCompatibility(version, ">=1")){
            return new WorldConfigPrinterV0100();
        }
        return null;
    }

    @Override
    public FilePrinterImpl<WorldConfig> getImplForLatestVersion(){
        return new WorldConfigPrinterV0103();
    }

    @Override
    protected void startWriting(PrintStream ps){
        meta(META_FILE_HEADER, WorldConfigLoader.WORLD_CONFIG_TYPE);
        meta(META_VERSION, getTargetVersionExpression());
        meta(META_CREATE_TIME, Long.toString(System.currentTimeMillis()));
        globalMeta();
        line();
    }

    public boolean isHumanReadable(){
        return humanReadable;
    }

    public void setHumanReadable(boolean humanReadable){
        this.humanReadable = humanReadable;
    }
}
