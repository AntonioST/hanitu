/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world;

import java.util.Objects;

import cclo.hanitu.HanituInfo.Tip;
import cclo.hanitu.HanituInfo.VValueField;

import static cclo.hanitu.HanituInfo.Description;

/**
 * molecule configuration, has sub-type food and toxicant.
 *
 * @author antonio
 */
public class MoleculeConfig implements Comparable<MoleculeConfig>{

    /**
     * molecule type
     */
    public MoleculeType type;
    /**
     * identify number, different type can shared same id.
     */
    public int id;

    @Description(resource="gui", value="molecule.x")
    @Tip(resource="gui", value="molecule.x.tip")
    @VValueField(resource="gui", value="molecule.x.unit")
    public int x;

    @Description(resource="gui", value="molecule.y")
    @Tip(resource="gui", value="molecule.y.tip")
    @VValueField(resource="gui", value="molecule.y.unit")
    public int y;

    @Description(resource="gui", value="molecule.count")
    @Tip(resource="gui", value="molecule.count.tip")
    @VValueField
    public int count = 10;

    @Description(resource="gui", value= "molecule.diffusioncoef")
    @Tip(resource="gui", value="molecule.diffusioncoef.tip")
    @VValueField(resource="gui", value="molecule.diffusioncoef.unit")
    public double diffuse = 0.000015;

    @Description(resource="gui", value="molecule.concentration")
    @Tip(resource="gui", value="molecule.concentration.tip")
    @VValueField(resource="gui", value="molecule.concentration.unit")
    public int concentration = 100;

    @Description(resource="gui", value="molecule.delay")
    @Tip(resource="gui", value="molecule.delay.tip")
    @VValueField(resource="gui", value="molecule.delay.unit")
    public int delay = 1000;

    /**
     * create a molecule with (program) default value
     */
    public MoleculeConfig(){
    }

    /**
     * create a molecule with copy the value from the reference
     * @param id identify number
     * @param molecule reference molecule
     */
    public MoleculeConfig(int id, MoleculeConfig molecule){
        this.id = id;
        if(molecule != null){
            x = molecule.x;
            y = molecule.y;
            type = molecule.type;
            count = molecule.count;
            diffuse = molecule.diffuse;
            concentration = molecule.concentration;
            delay = molecule.delay;
        }
    }

    @Override
    public int compareTo(MoleculeConfig o){
        int ret = Integer.compare(type.ordinal(), o.type.ordinal());
        if (ret == 0) ret = Integer.compare(id, o.id);
        return ret;
    }

    @Override
    public int hashCode(){
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(type);
        hash = 11 * hash + id;
        return hash;
    }

    @Override
    public boolean equals(Object obj){
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final MoleculeConfig other = (MoleculeConfig)obj;
        if (type != other.type) return false;
        return id == other.id;
    }
}
