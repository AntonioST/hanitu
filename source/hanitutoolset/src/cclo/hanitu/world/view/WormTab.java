/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import cclo.hanitu.HanituInfo;
import cclo.hanitu.circuit.view.EditCircuitMode;
import cclo.hanitu.circuit.view.VisualCircuit;
import cclo.hanitu.gui.FXUIField;
import cclo.hanitu.gui.HanituFileFilters;
import cclo.hanitu.gui.MessageSession;
import cclo.hanitu.world.WormConfig;

/**
 * @author antonio
 */
public class WormTab extends AbstractTab<WormConfig>{

    private static final Tooltip OVERLAP = new Tooltip("overlap with other worms");

    TextField locationXField;
    TextField locationYField;
    TextField circuitField;

    public WormTab(WorldBuilder host, WormConfig worm){
        super(host, getTitle(worm), worm);
        //
        Button browse = new Button("Browse");
        Button edit = new Button("Edit");
        browse.setMinWidth(70);
        edit.setMinWidth(70);
        browse.setOnAction(this::browseCircuitFile);
        edit.setOnAction(this::openCircuitFile);

        GridPane layout = properties.getLayout();
        layout.add(new HBox(browse, edit), 3, 5);

        locationXField = (TextField)properties.getField(0);
        locationYField = (TextField)properties.getField(1);
        locationXField.textProperty().addListener((observable, oldValue, newValue) -> {
            checkLocationOverlap(newValue, locationYField.getText());
        });
        locationYField.textProperty().addListener((observable, oldValue, newValue) -> {
            checkLocationOverlap(locationXField.getText(), newValue);
        });
        circuitField = (TextField)properties.getField(5);
        circuitField.textProperty().addListener((observable, oldValue, newValue) -> {
            checkCircuitFileExist(newValue, true);
        });
        tab.setOnSelectionChanged(event -> {
            checkLocationOverlap(locationXField.getText(), locationYField.getText());
            checkCircuitFileExist(circuitField.getText(), true);
        });

    }

    private static String getTitle(WormConfig w){
        return String.format("Worm [u=%d,%d]", w.user, w.worm);
    }

    @Override
    protected int getIndexOf(){
        return 1 + (int)host.config.worms().stream()
          .filter(w -> w.compareTo(target) < 0)
          .count();
    }

    @Override
    void update(){
        super.update();
        setTitle(getTitle(target));
    }

    @Override
    void close(){
        host.deleteWorm(target.user, target.worm);
    }

    void browseCircuitFile(ActionEvent e){
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(HanituFileFilters.CIRCUIT_CONFIG_FILTER,
                                        HanituFileFilters.NORMAL_TEXT_FILTER,
                                        HanituFileFilters.ALL_FILTER);
        if (host.file.getCurrentWorkDirectory() != null){
            fc.setInitialDirectory(host.file.getCurrentWorkDirectory().toFile());
        }
        if (target.circuitFileName != null){
            fc.setInitialFileName(target.circuitFileName);
        }
        File file = fc.showOpenDialog(host.primaryStage);
        if (file != null){
            target.circuitFileName = file.getName();
            properties.updateFieldEvent.run();
        }
        checkCircuitFileExist(target.circuitFileName, true);
    }

    void openCircuitFile(ActionEvent e){
        VisualCircuit circuit = null;
        if (target.circuitFileName == null){
            String filename = MessageSession.showInputDialog("Create Circuit File",
                                                             "Please enter a file name to create a new empty circuit file.");
            if (filename != null){
                if (filename.endsWith(HanituInfo.CIRCUIT_EXTEND_FILENAME)){
                    target.circuitFileName = filename;
                } else {
                    target.circuitFileName = filename + "." + HanituInfo.CIRCUIT_EXTEND_FILENAME;
                }
                circuit = new VisualCircuit();
                EditCircuitMode mode = new EditCircuitMode();
                circuit.setMode(mode);
                mode.setSaveFileName(host.file.getCurrentWorkDirectory().resolve(target.circuitFileName));
            }
        } else {
            circuit = new VisualCircuit();
            EditCircuitMode mode = new EditCircuitMode();
            circuit.setMode(mode);
            try {
                circuit.setCircuit(host.file.getCurrentWorkDirectory().resolve(target.circuitFileName), false);
            } catch (IOException e1){
                MessageSession.showErrorMessageDialog("open circuit file error", e1);
                circuit = null;
                checkCircuitFileExist(target.circuitFileName, true);
            }
        }
        if (circuit != null){
            circuit.setWormConfig(target);
            try {
                circuit.start();
            } catch (Exception ex){
                MessageSession.showErrorMessageDialog(ex);
            }
        }
    }

    void checkCircuitFileExist(String circuitFilePath, boolean ignoreNull){
        FXUIField.setBackground(circuitField, Color.WHITE);
        circuitField.setTooltip(null);
        if (circuitFilePath == null){
            if (ignoreNull) return;
            FXUIField.setBackground(circuitField, Color.PINK);
            circuitField.setTooltip(new Tooltip("circuit file not set"));
            return;
        }
        Path p = host.file.getCurrentWorkDirectory().resolve(circuitFilePath);
        if (!Files.exists(p) || !Files.isRegularFile(p)){
            FXUIField.setBackground(circuitField, Color.PINK);
            circuitField.setTooltip(new Tooltip(circuitFilePath + " does not exist"));
        }
    }

    boolean isCircuitFileExist(){
        String circuitFile = target.circuitFileName;
        if (circuitFile == null || circuitFile.isEmpty()) return false;
        Path p = host.file.getCurrentWorkDirectory().resolve(circuitFile);
        return Files.exists(p) && Files.isRegularFile(p);
    }

    void checkLocationOverlap(String x, String y){
        if (x == null || x.isEmpty() || y == null || y.isEmpty()) return;
        try {
            int xx = Integer.parseInt(x.trim());
            int yy = Integer.parseInt(y.trim());
            boolean result = host.config.worms().stream()
              .filter(w -> w != target)
              .anyMatch(w -> {
                  int dx = w.x - xx;
                  int dy = w.y - yy;
                  int dd = target.size + w.size;
                  if (dx > dd || dy > dd) return false;
                  return dx * dx + dy * dy < dd * dd;
              });
            if (result){
                FXUIField.setBackground(locationXField, Color.PINK);
                FXUIField.setBackground(locationYField, Color.PINK);
                locationXField.setTooltip(OVERLAP);
                locationYField.setTooltip(OVERLAP);
            } else {
                FXUIField.setBackground(locationXField, Color.WHITE);
                FXUIField.setBackground(locationYField, Color.WHITE);
                locationXField.setTooltip(null);
                locationYField.setTooltip(null);
            }
        } catch (NumberFormatException e){
        }
    }
}
