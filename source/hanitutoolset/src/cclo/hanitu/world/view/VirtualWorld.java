/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.util.*;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

import cclo.hanitu.About;
import cclo.hanitu.Base;
import cclo.hanitu.Log;
import cclo.hanitu.MessageFormatOperator;
import cclo.hanitu.data.*;
import cclo.hanitu.gui.FXMain;
import cclo.hanitu.gui.PaintArea;
import cclo.hanitu.gui.PaintingPane;
import cclo.hanitu.util.Events.ConsumerEvent;
import cclo.hanitu.util.Events.RunnableEvent;
import cclo.hanitu.util.FpsControl;
import cclo.hanitu.util.ReadLineInt;
import cclo.hanitu.util.Strings;
import cclo.hanitu.world.WorldConfig;

/**
 * @author antonio
 */
public class VirtualWorld implements FXMain.FXSimpleApplication{

    private static final String PROGRAM_TITLE = "Hanitu Virtual World";
    private final static String SHORTCUT_TEXT =
      "~*{Keyboard Shortcut Table~}\n" +
      "space : \tpause/un-pause\n" +
      "Left/Right : \tincrease/decrease amount of time (100 ms).\n" +
      "Ctrl+Left/Right : \tgo to the head/end of the data\n" +
      "Shift+Left/Right : \tincrease/decrease greater amount of time\n" +
      "(~/{number~})+Enter : \tinput a number and go to time at\n" +
      "Backspace : \tdelete goto time input\n" +
      "PageUp/Down : \t increase/decrease the FPS.\n" +
      "C : \tremove system message\n" +
      "Ctrl+C : \tclean message\n" +
      "D : \tshow all message\n" +
      "Shift+D : \tshow/hide message\n";

    private static final int VIEW_LEFT = 20;
    private static final int VIEW_TOP = 20;
    private static final int VIEW_LENGTH = 500;
    private static final int VIEW_RIGHT = VIEW_LEFT + VIEW_LENGTH;
    private static final int VIEW_BOTTOM = VIEW_TOP + VIEW_LENGTH;
    private static final int VIEW_WIDTH = VIEW_RIGHT + VIEW_LEFT;
    private static final int VIEW_HEIGHT = VIEW_BOTTOM + 130;

    //Display font
    private Font textFont;
    private Font messageFont;

    private void updateFont(Font f){
        textFont = Font.font(f.getFamily(), Base.getIntegerProperty("world.plot.fontsize", 16));
        messageFont = Font.font(f.getFamily(), Base.getIntegerProperty("world.plot.message.fontsize", 14));
    }

    {
        updateFont(Font.font(Base.getProperty("world.plot.font", "FreeMomo")));
    }

    private Stage primaryStage;
    private PaintingPane content;
    private AbstractWorldCanvas canvas;
    private MessageArea messageArea;
    private InformationArea informationArea;
    private Timeline timeline;

    private WorldConfig world = new WorldConfig();
    private DataRecorder<LocationData> loader;
    private int loaderState;

    private volatile boolean pause = false;
//    private volatile boolean escape = false;

    private double simulationTime;
    private Double jumpTime;
    private double jumpTimeStep = 100;
    private final ReadLineInt jumpTimeBuffer = new ReadLineInt();
    /*
     * use JavaFX Timeline to control animate, i cannot control fps directly.
     * this field just used to record fps value and information.
     */
    private final FpsControl fps = new FpsControl();

    public final RunnableEvent startEvent = new RunnableEvent();
    public final RunnableEvent updateEvent = new RunnableEvent();
    public final RunnableEvent quitEvent = new RunnableEvent();
    public final ConsumerEvent<WormData> wormEatEvent = new ConsumerEvent<>();
    public final ConsumerEvent<WormData> wormDieEvent = new ConsumerEvent<>();
    public final ConsumerEvent<WormData> wormClickedEvent = new ConsumerEvent<>();
    public final ConsumerEvent<MoleculeData> moleculeClickedEvent = new ConsumerEvent<>();
    public final ConsumerEvent<PositionData> worldClickedEvent = new ConsumerEvent<>();
    public final ConsumerEvent<WormData> wormDoubleClickedEvent = new ConsumerEvent<>();
    public final ConsumerEvent<MoleculeData> moleculeDoubleClickedEvent = new ConsumerEvent<>();
    public final ConsumerEvent<PositionData> worldDoubleClickedEvent = new ConsumerEvent<>();

    private final Deque<Message> messageQueue = new LinkedList<>();
    private String messageEat;
    private String messageDie;
    private String messageTouchWorm;
    private String messageTouchMolecule;

    {
        Properties p = Base.loadPropertyInherit("gui");
        messageEat = p.getProperty("world.worm.eat");
        messageDie = p.getProperty("world.worm.die");
        messageTouchWorm = p.getProperty("world.worm.touch");
        messageTouchMolecule = p.getProperty("world.molecule.touch");
    }

    private final int messageDisplayCount = Base.getIntegerProperty("world.plot.message.count", 5);
    private final int messageFadeDelay = Base.getIntegerProperty("world.plot.message.delay", 5000);
    private final int messageFadeDecay = Base.getIntegerProperty("world.plot.message.decay", 1000);
    private transient long showAllMessageTime = -1;

    @Override
    public void start(Stage primaryStage){
        this.primaryStage = primaryStage;
        primaryStage.setTitle(PROGRAM_TITLE);
        primaryStage.setResizable(false);
        primaryStage.setOnCloseRequest(e -> stop());
        VBox root = new VBox();
        Scene scene = new Scene(root);
        scene.setOnKeyPressed(this::handleKeyPress);
        primaryStage.setScene(scene);
        //
        root.getChildren().addAll(initCanvas());
        initTimeLine();
        initEvent();
        //
        primaryStage.sizeToScene();
        primaryStage.show();
        startEvent.run();
        startEvent.clear();
        fps.start();
        timeline.play();
    }

    private PaintingPane initCanvas(){
        content = new PaintingPane(VIEW_WIDTH, VIEW_HEIGHT);
        if (canvas == null){
            setCanvas(new DefaultWorldCanvas());
//            setCanvas(new ShadowWorldCanvas());
        } else {
            setCanvas(canvas);
        }
        content.addPanel(messageArea = new MessageArea());
        content.addPanel(informationArea = new InformationArea());
        primaryStage.getScene().setOnMouseMoved(e -> {
            if (messageArea != null && canvas != null && canvas.isDrawGridLine()){
                messageArea.mouseXInPane = e.getSceneX();
                messageArea.mouseYInPane = e.getSceneY();
            }
        });
        return content;
    }

    private void initEvent(){
        wormEatEvent.add(w -> {
            if (messageEat != null){
                try {
                    messageQueue.addFirst(formatMessage(messageEat, w, true));
                } catch (IllegalFormatException e){
                    messageEat = null;
                }
            }
        });
        wormDieEvent.add(w -> {
            if (messageDie != null){
                try {
                    messageQueue.addFirst(formatMessage(messageDie, w, true));
                } catch (IllegalFormatException e){
                    messageDie = null;
                }
            }
        });
    }

    private void initTimeLine(){
        timeline = new Timeline(new KeyFrame(new Duration(20), e -> {
            fps.update();
            updateLocation();
            content.repaint();
            updateEvent.run();
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        fps.setFPS(50);
    }

    private void resetUpdateCycleFPS(double fps){
        timeline.stop();
        timeline.getKeyFrames().clear();
        timeline.getKeyFrames().addAll(new KeyFrame(new Duration(1000 / fps), e -> {
            this.fps.update();
            updateLocation();
            content.repaint();
            updateEvent.run();
        }));
        this.fps.setFPS(fps);
        timeline.play();
    }

    public void stop(){
        timeline.stop();
        quitEvent.run();
        quitEvent.clear();
        messageQueue.clear();
        Platform.runLater(primaryStage::close);
    }


    public void setCanvas(AbstractWorldCanvas canvas){
        if (this.canvas != null){
            content.removePanel(this.canvas);
        }
        this.canvas = canvas;
        if (content != null){
            content.addPanel(canvas);
            content.setBackgroundColor(canvas.getBackgroundColor());
        }
        canvas.setLayer(5);
        canvas.setWorldConfig(world);
        canvas.setXOnPane(VIEW_LEFT);
        canvas.setYOnPane(VIEW_TOP);
        canvas.setWidth(VIEW_LENGTH);
        canvas.setHeight(VIEW_LENGTH);
        canvas.setEditable(false);
        canvas.setDrawBoundary(true);
        canvas.setTextFont(textFont);
        //
        if (primaryStage != null){
            canvas.setup(primaryStage.getScene());
            canvas.focusEvent.add(this::handleFocusEvent);
            canvas.clickEvent.add(this::handleDoubleClickEvent);
        }
    }

    public void setWorldConfig(WorldConfig config){
        world = config;
        if (canvas != null){
            canvas.setWorldConfig(config);
        }
    }

    public void setLoader(DataLoader<LocationData> loader){
        if (timeline != null){
            timeline.stop();
            jumpTime = null;
        }
        this.loader = new DataRecorder<>(loader);
        if (timeline != null){
            timeline.play();
        }
    }


    /**
     * get current simulation time
     *
     * @return simulation time
     */
    public double getSimulationTime(){
        return simulationTime;
    }

    /**
     * Is pause?
     *
     * @return Is pause?
     */
    public boolean isPause(){
        return pause;
    }

    /**
     * set pause
     *
     * If pause, system will continue render, but doesn't update the location information.
     *
     * @param pause true for stopping update location information in updating loop
     */
    public void setPause(boolean pause){
        this.pause = pause;
    }

    private void updateLocation(){
        if (pause || loader == null) return;
        LocationData loc = null;
        if (jumpTime != null){
            LocationData key = null;
            if (jumpTime >= 0){
                key = new LocationData(jumpTime);
            }
            if (loader.hasNext(key)){
                loaderState = 0;
                loc = loader.next();
                loc.forEach(d -> updateWormLocation(d, false));
                messageQueue.addFirst(new Message(Color.BLACK, String.format("go to time : %.1f", loc.time), false));
            } else if (loader.isClose()){
                loaderState = 2;
            } else {
                loaderState = 1;
            }
            jumpTime = null;
        } else {
            if (loader.hasNext()){
                loaderState = 0;
                loc = loader.next();
                loc.forEach(d -> updateWormLocation(d, false));
            } else if (loader.isClose()){
                loaderState = 2;
            } else {
                loaderState = 1;
            }
        }
        if (loc != null){
            simulationTime = loc.time;
        }
    }

    private void updateWormLocation(WormData newData, boolean silenceEvent){
        WormData w = canvas.getWorm(newData.user, newData.worm);
        if (w != null){
            w.x = newData.x;
            w.y = newData.y;
            double oldHp = w.hp;
            w.hp = newData.hp;
            if (!silenceEvent){
                if (w.hp > oldHp){
                    wormEatEvent.accept(w);
                }
                if (w.hp == 0 && oldHp > 0){
                    wormDieEvent.accept(w);
                }
            }
        }
    }

    private void handleFocusEvent(PositionData data, MouseEvent e){
        if (e.getButton() == MouseButton.PRIMARY){
            if (data instanceof WormData){
                if (messageTouchWorm != null){
                    try {
                        messageQueue.addFirst(formatMessage(messageTouchWorm, (WormData)data, false));
                    } catch (IllegalFormatException ex){
                        messageTouchWorm = null;
                    }
                }
                wormClickedEvent.accept((WormData)data);
            } else if (data instanceof MoleculeData){
                if (messageTouchMolecule != null){
                    try {
                        messageQueue.addFirst(formatMessage(messageTouchMolecule, (MoleculeData)data, false));
                    } catch (IllegalFormatException ex){
                        messageTouchMolecule = null;
                    }
                }
                moleculeClickedEvent.accept((MoleculeData)data);
            } else {
                worldClickedEvent.accept(new PositionData(data.x, data.y));
            }
        }
    }

    private void handleDoubleClickEvent(PositionData data, MouseEvent e){
        if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2){
            if (data instanceof WormData){
                wormDoubleClickedEvent.accept((WormData)data);
            } else if (data instanceof MoleculeData){
                moleculeDoubleClickedEvent.accept((MoleculeData)data);
            } else {
                worldDoubleClickedEvent.accept(new PositionData(data.x, data.y));
            }
        }
    }

    private void handleKeyPress(KeyEvent e){
        switch (e.getCode()){
        case F1:
            new About(SHORTCUT_TEXT).show();
            break;
        case SPACE:
            setPause(!pause);
            break;
        case LEFT:{
            double time = 0;
            if (!e.isControlDown()){
                if (jumpTimeStep > 0){
                    jumpTimeStep = -100;
                } else if (e.isShiftDown()){
                    jumpTimeStep -= 100;
                    messageQueue.addFirst(new Message(Color.BLACK, "time " + jumpTimeStep, false));
                }
                time = simulationTime + jumpTimeStep;
            }
            if (time <= 0){
                time = 0;
                messageQueue.clear();
            }
            jumpTime = time;
            break;
        }
        case RIGHT:{
            double time = -1;
            if (!e.isControlDown()){
                if (jumpTimeStep < 0){
                    jumpTimeStep = 100;
                } else if (e.isShiftDown()){
                    jumpTimeStep += 100;
                    messageQueue.addFirst(new Message(Color.BLACK, "time +" + jumpTimeStep, false));
                }
                time = simulationTime + jumpTimeStep;
            }
            jumpTime = time;
            break;
        }
        case ENTER:{
            double time = jumpTimeBuffer.clear().getAsInt();
            if (time == 0){
                messageQueue.clear();
            }
            jumpTime = time;
            break;
        }
        case DIGIT0:
        case NUMPAD0:
        case DIGIT1:
        case NUMPAD1:
        case DIGIT2:
        case NUMPAD2:
        case DIGIT3:
        case NUMPAD3:
        case DIGIT4:
        case NUMPAD4:
        case DIGIT5:
        case NUMPAD5:
        case DIGIT6:
        case NUMPAD6:
        case DIGIT7:
        case NUMPAD7:
        case DIGIT8:
        case NUMPAD8:
        case DIGIT9:
        case NUMPAD9:
        case BACK_SPACE:
            jumpTimeBuffer.processKeyEvent(e.getCode());
            break;
        case PAGE_UP:{
            int v = ((int)fps.getFPS() / 10 + 1) * 10;
            if (v <= 100){
                messageQueue.addFirst(new Message(Color.BLACK, "fps = " + v, false));
                resetUpdateCycleFPS(v);
            }
            break;
        }
        case PAGE_DOWN:{
            int v = ((int)fps.getFPS() / 10 - 1) * 10;
            if (v < 10){
                v = 10;
                messageQueue.addFirst(new Message(Color.BLACK, "fps = " + v, false));
            } else {
                messageQueue.addFirst(new Message(Color.BLACK, "fps = " + v, false));
            }
            resetUpdateCycleFPS(v);
            break;
        }
        case C:
            if (e.isControlDown()){
                messageQueue.clear();
            } else {
                messageQueue.removeIf(msg -> !msg.isNormalMessage);
            }
            break;
        case D:
            if (e.isShiftDown()){
                messageArea.setVisible(!messageArea.isVisible());
            } else {
                long time = System.currentTimeMillis();
                messageQueue.forEach(m -> m.create = time);
                showAllMessageTime = time;
            }
            break;
        case G:
            canvas.setDrawGridLine(!canvas.isDrawGridLine());
            break;
        }
    }

    private Message formatMessage(String format, WormData data, boolean normal){
        Strings expr = new Strings();
        expr.setSingleReplace(MessageFormatOperator.unitFormatWorm(simulationTime / 1000.0, data));
        return new Message(canvas.getUserRepresentColor(data.user),
                           expr.format(format),
                           normal);
    }

    private Message formatMessage(String format, MoleculeData data, boolean normal){
        Strings expr = new Strings();
        expr.setSingleReplace(MessageFormatOperator.unitFormatMolecule(simulationTime / 1000.0, data));
        return new Message(canvas.getMoleculeRepresentColor(data.type),
                           expr.format(format),
                           normal);
    }

    private class MessageArea extends PaintArea{

        private double mouseXInPane;
        private double mouseYInPane;

        public MessageArea(){
            super(VIEW_LEFT, VIEW_TOP, VIEW_LENGTH, VIEW_LENGTH);
            setLayer(6);
        }

        @Override
        public void paint(GraphicsContext g){
            renderMessageText(g);
            if (canvas.isDrawGridLine()){
                renderMouseLocationText(g);
            }
        }

        private void renderMessageText(GraphicsContext g){
            g.setFont(messageFont);
            Iterator<Message> it = messageQueue.iterator();
            double button = getHeight() - 5;
            int count = 0;
            double dec = messageFont.getSize();
            while (it.hasNext()){
                Message msg = it.next();
                Color c = msg.getFadeColor();
                if (c == null){
                    showAllMessageTime = -1;
                    break;
                } else {
                    g.setFill(c);
                    g.fillText(msg.message, 5, button);
                    button -= dec;
                    count++;
                }
                if (button - dec < VIEW_TOP) break;
                if (showAllMessageTime < 0 && count >= messageDisplayCount) break;
            }
        }

        private void renderMouseLocationText(GraphicsContext g){
            if (mouseXInPane > getXOnPane()
                && mouseXInPane < getWidth() + getXOnPane()
                && mouseYInPane > getYOnPane()
                && mouseYInPane < getHeight() + getYOnPane()){
                g.setFont(messageFont);
                g.setFill(Color.DARKGRAY);
                g.fillText(String.format("(%.2f,%.2f)", canvas.getXFromPane(mouseXInPane),
                                         canvas.getYFromPane(mouseYInPane)),
                           mouseXInPane, mouseYInPane);
            }
        }
    }

    private class InformationArea extends PaintArea{

        private transient int userCount;
        private transient String currentTimeText;
        private transient String gotoTimeText;
        private transient String fpsText;

        public InformationArea(){
            super(VIEW_LEFT, VIEW_BOTTOM + 10, VIEW_LENGTH, VIEW_HEIGHT - VIEW_BOTTOM - 10);
            setLayer(2);
            userCount = world.userCount();
        }

        @Override
        public void paint(GraphicsContext g){
            String state;
            switch (loaderState){
            case 0:
            default:
                state = pause? " (paused)": "";
                break;
            case 1:
                state = " (end)";
                break;
            case 2:
                state = "(close)";
                break;
            }
            currentTimeText = String.format("Time: %.4f s %s", simulationTime / 1000.0, state);
            if (jumpTimeBuffer.isValid()){
                gotoTimeText = String.format("goto: %d ms", jumpTimeBuffer.get().getAsInt());
            } else {
                gotoTimeText = null;
            }
            fpsText = String.format("FPS: %d", fps.getAvgFPS());
            userCount = (int)canvas.worms.stream().mapToInt(w -> w.user).distinct().count();
            g.setFont(textFont);
            g.setFill(Color.BLACK);
            if (canvas instanceof ShadowWorldCanvas){
                paintShadow(g);
            } else {
                paintDefault(g);
            }
        }

        private void paintDefault(GraphicsContext g){
            double size = g.getFont().getSize();
            double left = 0;
            double top = 10;
            g.fillText(currentTimeText, 0, top);
            if (gotoTimeText != null){
                g.fillText(gotoTimeText, size * 10, top);
            }
            g.fillText(fpsText, getWidth() - size * 7, top);
            top += size;
            left += 20;
            for (WormData w : canvas.worms){
                g.setFill(canvas.getUserRepresentColor(w.user));
                g.fillText(String.format("%2.2f", w.hp),
                           left + w.worm * 52, top + w.user * size);
            }
            left = left + (canvas.worms.stream().mapToInt(w -> w.worm).max().orElse(0) + 1) * 52;
            for (int i = 0; i < userCount; i++){
                int user = i;
                double avg = canvas.worms.stream().filter(w -> w.user == user).mapToDouble(w -> w.hp).average().orElse(
                  0);
                g.setFill(canvas.getUserRepresentColor(i));
                g.fillText(String.format("%d", i), 0, top);
                g.fillText(String.format("avg: %2.2f", avg), left, top);
                top += size;
            }
        }

        private void paintShadow(GraphicsContext g){
            ShadowWorldCanvas shadowCanvas = (ShadowWorldCanvas)canvas;
            double size = g.getFont().getSize();
            double left = 10;
            double top = 10 + size;
            double height = canvas.getCacheTextHeight(g);
            Paint fill = g.getFill();
            g.setEffect(shadowCanvas.worldShadow);
            g.fillRect(0, 0, getWidth(), height * (1 + userCount) + size);
            g.setEffect(null);
            g.setFill(Color.WHITE);
            g.fillRect(0, 0, getWidth(), height * (1 + userCount) + size);
            g.setFill(fill);
            //
            g.fillText(currentTimeText, left, top);
            if (gotoTimeText != null){
                g.fillText(gotoTimeText, size * 10, top);
            }
            g.fillText(fpsText, getWidth() - size * 7, top);
            top += size;
            left += 20;
            for (WormData w : canvas.worms){
                g.setFill(canvas.getUserRepresentColor(w.user));
                g.fillText(String.format("%2.2f", w.hp),
                           left + w.worm * 52, top + w.user * size);
            }
            left = left + (canvas.worms.stream().mapToInt(w -> w.worm).max().orElse(0) + 1) * 52;
            for (int i = 0; i < userCount; i++){
                int user = i;
                double avg = canvas.worms.stream().filter(w -> w.user == user).mapToDouble(w -> w.hp).average().orElse(
                  0);
                g.setFill(canvas.getUserRepresentColor(i));
                g.fillText(String.format("%d", i), 10, top);
                g.fillText(String.format("avg: %2.2f", avg), left, top);
                top += size;
            }
        }
    }

    private class Message{

        long create;
        final Color color;
        boolean isNormalMessage = true;
        final String message;

        public Message(Color color, String message, boolean normal){
            create = System.currentTimeMillis();
            this.color = color;
            this.message = message;
            isNormalMessage = normal;
            Log.debug(message);
        }

        Color getFadeColor(){
            int time = (int)(System.currentTimeMillis() - create);
            if (time < messageFadeDelay) return color;
            time -= messageFadeDelay;
            if (time > messageFadeDecay) return null;
            return new Color(color.getRed(), color.getGreen(), color.getBlue(), 1 - (double)time / messageFadeDecay);
        }

        @Override
        public String toString(){
            return message;
        }
    }
}
