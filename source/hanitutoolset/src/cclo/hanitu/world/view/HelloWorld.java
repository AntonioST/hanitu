/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

import cclo.hanitu.Executable;
import cclo.hanitu.data.WormData;
import cclo.hanitu.gui.FXMain;
import cclo.hanitu.gui.PaintingPane;
import cclo.hanitu.world.MoleculeConfig;
import cclo.hanitu.world.MoleculeType;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.WormConfig;

/**
 * @author antonio
 */
public class HelloWorld extends Executable{

    private WorldConfig world;
    private WormData worm;

    private Stage primaryStage;
    private PaintingPane content;
    private AbstractWorldCanvas canvas;
    private Timeline timeline;

    public HelloWorld(){
        world = new WorldConfig();
        world.boundary = 60;
        world.addWorm(new WormConfig(0, 0, null){
            {
                x = -40;
            }
        });
        world.addMolecule(new MoleculeConfig(1, null){
            {
                type = MoleculeType.FOOD;
            }
        });
    }

    @Override
    public void start() throws Exception{
        FXMain.launch(stage -> {
            primaryStage = stage;
            primaryStage.setTitle("Hello World");
            primaryStage.setResizable(false);
            primaryStage.setOnCloseRequest(e -> stage.close());
            VBox root = new VBox();
            Scene scene = new Scene(root);
            scene.setOnKeyPressed(this::processKeyEvent);
            primaryStage.setScene(scene);
            //
            root.getChildren().addAll(initCanvas());
            initTimeLine();
            //
            primaryStage.sizeToScene();
            primaryStage.show();
            timeline.play();
        });
    }

    private PaintingPane initCanvas(){
        content = new PaintingPane(540, 600);
        if (canvas == null){
//            setCanvas(new DefaultWorldCanvas());
            setCanvas(new ShadowWorldCanvas());
        } else {
            setCanvas(canvas);
        }
        return content;
    }

    private void initTimeLine(){
        timeline = new Timeline(new KeyFrame(new Duration(20), e -> {
            content.repaint();
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
    }

    public void setCanvas(AbstractWorldCanvas canvas){
        if (this.canvas != null){
            content.removePanel(this.canvas);
        }
        this.canvas = canvas;
        if (content != null){
            content.addPanel(canvas);
            content.setBackgroundColor(canvas.getBackgroundColor());
        }
        canvas.setLayer(5);
        canvas.setWorldConfig(world);
        worm = canvas.getWorm(0, 0);
        canvas.setXOnPane(20);
        canvas.setYOnPane(20);
        canvas.setWidth(500);
        canvas.setHeight(500);
        canvas.setEditable(true);
        canvas.setDrawBoundary(true);
        //
        if (primaryStage != null){
            canvas.setup(primaryStage.getScene());
            canvas.focusEvent.add((target, mouseEvent) -> {
                System.out.println(target);
            });
            canvas.locationUpdateEvent.add((target, newPosition) -> {
                target.x = newPosition.x;
                target.y = newPosition.y;
            });
        }
    }

    private void processKeyEvent(KeyEvent e){
        if (worm == null) return;
        switch (e.getCode()){
        case PAGE_UP:{
            double v = worm.hp + 10;
            if (v > 100){
                v = 100;
            }
            worm.hp = v;
            break;
        }
        case PAGE_DOWN:{
            double v = worm.hp - 10;
            if (v < 0){
                v = 0;
            }
            worm.hp = v;
            break;
        }
        }
    }
}
