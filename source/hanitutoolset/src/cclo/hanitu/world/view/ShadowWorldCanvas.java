/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.util.Map;
import java.util.WeakHashMap;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.*;
import javafx.scene.paint.Color;

import cclo.hanitu.HanituInfo;
import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.WormData;
import cclo.hanitu.world.MoleculeType;

/**
 * @author antonio
 */
@HanituInfo.Description("Shadow Style")
public class ShadowWorldCanvas extends DefaultWorldCanvas{

    public Color backgroundColor = Color.WHITE;
    public Color[] deltaHpColor = {Color.color(0, 0, 0, 0.5),
                                   Color.color(0, 1, 0, 0.5)};
    public Color[] hpColor = {Color.color(1, 0, 0, 0.5),
                              Color.color(1, 0.64705884, 0.0, 0.5),
                              Color.color(0.5647059, 0.93333334, 0.5647059, 0.5)};
    public Shadow worldShadow = new Shadow(BlurType.GAUSSIAN, Color.BLACK, 10);
    public Effect wormShadow = new DropShadow(5, 3, 3, Color.DARKGRAY);
    public Effect foodShadow = new DropShadow(5, 3, 3, Color.DARKGRAY);
    public Effect toxicantShadow = new InnerShadow(5, 3, 3, Color.DARKGRAY);
    private final Map<WormData, Double> previousHpCache = new WeakHashMap<>();

    {
        setBackgroundColor(Color.color(0.9, 0.9, 0.9));
    }

    public ShadowWorldCanvas(){
    }

    public ShadowWorldCanvas(int x, int y, int w, int h){
        super(x, y, w, h);
    }

    public ShadowWorldCanvas(int w, int h){
        super(w, h);
    }

    @Override
    protected void renderBoundary(GraphicsContext g){
        g.setEffect(worldShadow);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setEffect(null);
        g.setFill(backgroundColor);
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    @Override
    protected void renderMolecule(GraphicsContext g, MoleculeData data){
        if (data.type == MoleculeType.FOOD){
            g.setFill(foodColor);
            g.setEffect(foodShadow);
        } else {
            g.setFill(toxicantColor);
            g.setEffect(toxicantShadow);
        }
        int d = FOOD_SIZE >> 1;
        double x = getXFromVirtual(data.x) - d;
        double y = getYFromVirtual(data.y) - d;
        g.fillOval(x, y, FOOD_SIZE, FOOD_SIZE);
        g.setEffect(null);
    }

    @Override
    protected void renderWorm(GraphicsContext g, WormData data){
        double sz = data.size * getWidth() / getBoundary();
        double sz2 = sz > 10? sz - 10: sz / 2;
        double x = getXFromVirtual(data.x);
        double y = getYFromVirtual(data.y);
        Color fill = getUserRepresentColor(data.user);
        //XXX new worm drawing way
        g.setFill(fill);
        double d = sz / 2;
        g.setEffect(wormShadow);
        g.fillOval(x - d, y - d, sz, sz);
        g.setEffect(null);
        d = sz2 / 2;
        g.setFill(Color.gray(data.hp / 100));
        g.fillOval(x - d, y - d, sz2 - 1, sz2 - 1);
        //
        if (data.hp > 0){
            renderWormHp(g, data, x, y, fill);
        }
    }

    protected void renderWormHp(GraphicsContext g, WormData data, double x, double y, Color fill){
        g.setFill(fill);
        g.setFont(getTextFont());
        if (data.hp < 20){
            g.setFill(hpColor[0]);
        } else if (data.hp < 40){
            g.setFill(hpColor[1]);
        } else {
            g.setFill(hpColor[2]);
        }
        //
        double offsetX = 20;
        double offsetY = 20;
        double hpW = 50;
        double hpH = 6;
        //
        boolean mirrorX = x + offsetX + hpW + getXOnPane() > getPaneWidth();
        boolean mirrorY = y - offsetY - hpH + getYOnPane() < 0;
        double hp = data.hp / 2;
        double delta = Math.floor(previousHpCache.compute(data,
                                                          (d, v) -> v == null? hp: (v - (v - hp) / 20)) - hp);
        if (mirrorX && mirrorY){
            g.fillRect(x - offsetX - hp, y + offsetY, hp, hpH);
            if (delta > 0){
                g.setFill(deltaHpColor[0]);
                g.fillRect(x - offsetX - hp - delta, y + offsetY, delta, hpH);
            } else if (delta < 0){
                g.setFill(deltaHpColor[1]);
                g.fillRect(x - offsetX - hp + delta, y + offsetY, -delta, hpH);
            }
            //
            g.setStroke(Color.BLACK);
            g.strokeLine(x, y, x - offsetX + 1, y + offsetY - 1);
            g.strokeLine(x - offsetX + 1, y + offsetY - 1, x - offsetX + 1, y + offsetY + hpH);
            g.setEffect(wormShadow);
            g.strokeLine(x - offsetX + 1, y + offsetY + hpH, x - offsetX - hpW, y + offsetY + hpH);
        } else if (mirrorX){
            g.fillRect(x - offsetX - hp, y - offsetY - hpH, hp, hpH);
            if (delta > 0){
                g.setFill(deltaHpColor[0]);
                g.fillRect(x - offsetX - hp - delta, y - offsetY - hpH, delta, hpH);
            } else if (delta < 0){
                g.setFill(deltaHpColor[1]);
                g.fillRect(x - offsetX - hp + delta, y - offsetY - hpH, -delta, hpH);
            }
            //
            g.setStroke(Color.BLACK);
            g.strokeLine(x, y, x - offsetX + 1, y - offsetY + 1);
            g.strokeLine(x - offsetX + 1, y - offsetY + 1, x - offsetX + 1, y - offsetY - hpH);
            g.setEffect(wormShadow);
            g.strokeLine(x - offsetX + 1, y - offsetY + 1, x - offsetX - hpW, y - offsetY + 1);
        } else if (mirrorY){
            g.fillRect(x + offsetX, y + offsetY, hp, hpH);
            if (delta > 0){
                g.setFill(deltaHpColor[0]);
                g.fillRect(x + offsetX + hp, y + offsetY, delta, hpH);
            } else if (delta < 0){
                g.setFill(deltaHpColor[1]);
                g.fillRect(x + offsetX + hp + delta, y + offsetY, -delta, hpH);
            }
            //
            g.setStroke(Color.BLACK);
            g.strokeLine(x, y, x + offsetX - 1, y + offsetY - 1);
            g.strokeLine(x + offsetX - 1, y + offsetY - 1, x + offsetX - 1, y + offsetY +hpH);
            g.setEffect(wormShadow);
            g.strokeLine(x + offsetX - 1, y + offsetY +hpH, x + offsetX + hpW, y + offsetY +hpH);
        } else {
            g.fillRect(x + offsetX, y - offsetY - hpH, hp, hpH);
            if (delta > 0){
                g.setFill(deltaHpColor[0]);
                g.fillRect(x + offsetX + hp, y - offsetY - hpH, delta, hpH);
            } else if (delta < 0){
                g.setFill(deltaHpColor[1]);
                g.fillRect(x + offsetX + hp + delta, y - offsetY - hpH, -delta, hpH);
            }
            //
            g.setStroke(Color.BLACK);
            g.strokeLine(x, y, x + offsetX - 1, y - offsetY + 1);
            g.strokeLine(x + offsetX - 1, y - offsetY + 1, x + offsetX - 1, y - offsetY - hpH);
            g.setEffect(wormShadow);
            g.strokeLine(x + offsetX - 1, y - offsetY + 1, x + offsetX + hpW, y - offsetY + 1);
        }
        g.setEffect(null);
    }
}
