/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;

import cclo.hanitu.HanituInfo;
import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.WormData;
import cclo.hanitu.world.MoleculeType;

/**
 * @author antonio
 */
@HanituInfo.Description("Default Style")
public class DefaultWorldCanvas extends AbstractWorldCanvas{

    //color
    public Color[] userColors = {Color.RED,
                                 Color.GREEN,
                                 Color.BLUE,
                                 Color.CYAN,
                                 Color.ORANGE,
                                 Color.PINK,
                                 Color.GRAY,
                                 Color.YELLOW};
    public Color foodColor = Color.BLACK;
    public Color toxicantColor = Color.MAGENTA;
    public Color boundaryColor = Color.BLACK;
    public Color gridLineColor = Color.GRAY;

    public double boundaryLineWidth = 3;

    public DefaultWorldCanvas(){
        super(500, 500);
    }

    public DefaultWorldCanvas(int x, int y, int w, int h){
        super(x, y, w, h);
    }

    public DefaultWorldCanvas(int w, int h){
        super(w, h);
    }

    @Override
    public Color getUserRepresentColor(int userID){
        return userColors[userID];
    }

    @Override
    public Color getMoleculeRepresentColor(MoleculeType type){
        return foodColor;
    }

    @Override
    protected void renderBoundary(GraphicsContext g){
        g.setStroke(boundaryColor);
        g.setLineWidth(boundaryLineWidth);
        g.setLineCap(StrokeLineCap.ROUND);
        g.strokeRect(0, 0, getWidth(), getHeight());
    }

    @Override
    protected void renderGridLine(GraphicsContext g){
        g.setStroke(gridLineColor);
        g.setLineDashes(10, 10);
        g.setLineWidth(1);
        double w = getWidth();
        double h = getHeight();
        double o = getXFromVirtual(0);
        int d = (int)Math.abs(getXFromVirtual(getGridLineSpacing()) - o);
        for (int i = (int)o; i < w; i += d){
            g.strokeLine(i, 0, i, h);
        }
        for (int i = (int)o - d; i >= 0; i -= d){
            g.strokeLine(i, 0, i, h);
        }
        for (int i = (int)o; i < h; i += d){
            g.strokeLine(0, i, w, i);
        }
        for (int i = (int)o - d; i >= 0; i -= d){
            g.strokeLine(0, i, w, i);
        }
        g.setLineDashes();
    }

    @Override
    protected void renderMolecule(GraphicsContext g, MoleculeData data){
        if (data.type == MoleculeType.FOOD){
            g.setFill(foodColor);
        } else {
            g.setFill(toxicantColor);
        }
        int d = FOOD_SIZE >> 1;
        double x = getXFromVirtual(data.x) - d;
        double y = getYFromVirtual(data.y) - d;
        g.fillRect(x, y, FOOD_SIZE, FOOD_SIZE);
    }

    @Override
    protected void renderWorm(GraphicsContext g, WormData data){
        double sz = data.size * getWidth() / getBoundary();
        double sz2 = sz > 10? sz - 10: sz / 2;
        double x = getXFromVirtual(data.x);
        double y = getYFromVirtual(data.y);
        g.setFill(userColors[data.user % userColors.length]);
        double d = sz / 2;
        g.fillOval(x - d, y - d, sz, sz);
        d = sz2 / 2;
        g.setFill(Color.gray(data.hp / 100));
        g.fillOval(x - d, y - d, sz2 - 1, sz2 - 1);
    }
}
