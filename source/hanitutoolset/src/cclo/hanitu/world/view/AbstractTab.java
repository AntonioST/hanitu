/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.util.Objects;
import java.util.function.Consumer;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import cclo.hanitu.FieldInfo;
import cclo.hanitu.gui.PropertySession;

/**
 * @author antonio
 */
abstract class AbstractTab<T>{

    protected final WorldBuilder host;
    protected final T target;
    protected final TabPane pane;
    protected final PropertySession properties;

    protected final Tab tab;
    protected final VBox contentPane;

    protected AbstractTab(WorldBuilder host, String title, T target){
        this.host = host;
        this.target = Objects.requireNonNull(target);
        pane = host.contentPane;
        //
        properties = new PropertySession();
        properties.reset(FieldInfo.listFieldInfo(target));
        properties.modifyEvent.add(() -> host.file.setCurrentFileModified(true));
        //
        GridPane layout = properties.getLayout();
        contentPane = new VBox(layout);
        VBox.setMargin(layout, new Insets(10));
        //
        tab = new Tab(title);
        tab.setContent(contentPane);
        tab.setClosable(true);
        tab.setOnSelectionChanged(e -> update());
        tab.setOnCloseRequest(e -> close());

    }

    protected abstract int getIndexOf();

    void add(){
        int index = getIndexOf();
        ObservableList<Tab> tabs = pane.getTabs();
        if (index < tabs.size()){
            tabs.add(index, tab);
        } else {
            tabs.add(tab);
        }
        pane.getSelectionModel().select(index);
    }

    void select(){
        pane.getSelectionModel().select(tab);
    }

    void setTitle(String title){
        tab.setText(title);
    }

    void update(){
        properties.updateFieldEvent.run();
    }

    void remove(){
        pane.getTabs().remove(tab);
    }

    void close(){
        //do nothing
    }

    void setOnUpdate(Consumer<T> update){
        properties.updateEvent.add(() -> update.accept(target));
    }
}
