/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.data.WormData;
import cclo.hanitu.gui.PaintArea;
import cclo.hanitu.util.Events.BiConsumerEvent;
import cclo.hanitu.util.Events.ConsumerEvent;
import cclo.hanitu.world.MoleculeType;
import cclo.hanitu.world.WorldConfig;

/**
 * @author antonio
 */
public abstract class AbstractWorldCanvas extends PaintArea{

    public static final int FOOD_SIZE = 8;

    private int boundary = 100;
    public final List<WormData> worms = new ArrayList<>();
    public final List<MoleculeData> molecule = new ArrayList<>();

    private Font textFont;
    private Color backgroundColor = Color.WHITE;
    private boolean drawBoundary = false;
    private boolean drawGridLine = false;
    private int gridLineSpacing = 10;
    private boolean editable = false;

    private PositionData target;
    public final BiConsumerEvent<PositionData, MouseEvent> focusEvent = new BiConsumerEvent<>();
    public final BiConsumerEvent<PositionData, MouseEvent> clickEvent = new BiConsumerEvent<>();
    public final BiConsumerEvent<PositionData, PositionData> locationUpdateEvent = new BiConsumerEvent<>();
    public final ConsumerEvent<PositionData> modifyEvent = new ConsumerEvent<>();

    private transient Font cacheFont;
    private transient double cacheTextSize;
    private transient double cacheTextHeight;

    protected double getCacheTextSize(GraphicsContext g){
        getCacheTextHeight(g);
        return cacheTextSize;
    }

    protected double getCacheTextHeight(GraphicsContext g){
        if (cacheFont == null || g.getFont() != cacheFont){
            Text t = new Text("test");
            t.setFont(cacheFont = g.getFont());
            cacheTextHeight = t.getLayoutBounds().getHeight();
            cacheTextSize = cacheFont.getSize();
        }
        return cacheTextHeight;
    }

    public AbstractWorldCanvas(){
    }

    public AbstractWorldCanvas(int x, int y, int w, int h){
        super(x, y, w, h);
    }

    public AbstractWorldCanvas(int w, int h){
        super(w, h);
    }

    public void setup(Scene scene){
        scene.setOnMouseClicked(this::processMouseClick);
        scene.setOnMousePressed(this::processMousePress);
        scene.setOnMouseDragged(this::processMouseDrag);
    }

    @Override
    public void paint(GraphicsContext g){
        if (drawBoundary){
            renderBoundary(g);
        }
        if (drawGridLine){
            renderGridLine(g);
        }
        for (MoleculeData data : molecule){
            renderMolecule(g, data);
        }
        for (WormData data : worms){
            renderWorm(g, data);
        }
    }

    protected void renderBoundary(GraphicsContext g){
    }

    protected void renderGridLine(GraphicsContext g){
    }

    protected abstract void renderMolecule(GraphicsContext g, MoleculeData data);

    protected abstract void renderWorm(GraphicsContext g, WormData data);

    public abstract Color getUserRepresentColor(int userID);

    public abstract Color getMoleculeRepresentColor(MoleculeType type);

    /**
     * set world with world configuration
     *
     * @param world
     */
    public void setWorldConfig(WorldConfig world){
        worms.clear();
        molecule.clear();
        boundary = world.boundary;
        world.worms().stream().map(WormData::new).forEach(worms::add);
        world.molecule().stream().map(MoleculeData::new).forEach(molecule::add);
    }

    public Font getTextFont(){
        return textFont;
    }

    public void setTextFont(Font textFont){
        this.textFont = textFont;
    }

    public Color getBackgroundColor(){
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor){
        this.backgroundColor = backgroundColor;
    }

    public int getBoundary(){
        return boundary;
    }

    public boolean isDrawBoundary(){
        return drawBoundary;
    }

    public void setDrawBoundary(boolean drawBoundary){
        this.drawBoundary = drawBoundary;
    }

    public boolean isDrawGridLine(){
        return drawGridLine;
    }

    public void setDrawGridLine(boolean drawGridLine){
        this.drawGridLine = drawGridLine;
    }

    public int getGridLineSpacing(){
        return gridLineSpacing;
    }

    public void setGridLineSpacing(int gridLineSpacing){
        this.gridLineSpacing = gridLineSpacing;
    }

    public boolean isEditable(){
        return editable;
    }

    public void setEditable(boolean editable){
        if (this.editable != editable){
            this.editable = editable;
            if (editable){
                locationUpdateEvent.add("cclo.hanitu.plot.location", (target, newPlace) -> {
                    target.x = newPlace.x;
                    target.y = newPlace.y;
                    modifyEvent.accept(target);
                });
            } else {
                locationUpdateEvent.removeEvent("cclo.hanitu.plot.location");
            }
        }
    }


    /**
     * get the worm data.
     *
     * @param user
     * @param id
     * @return
     */
    public WormData getWorm(int user, int id){
        for (WormData w : worms){
            if (w.user == user && w.worm == id) return w;
        }
        return null;
    }

    /**
     * get the molecule data
     *
     * @param type
     * @param id
     * @return
     */
    public MoleculeData getMolecule(MoleculeType type, int id){
        for (MoleculeData m : molecule){
            if (m.type == type && m.id == id) return m;
        }
        return null;
    }


    /**
     * @param v x location in virtual world in unit 0.1mm
     * @return x location on screen
     */
    protected double getXFromVirtual(double v){
        return (v / boundary + 1) * getWidth() / 2;
    }

    /**
     * @param v y location in virtual world in unit 0.1mm
     * @return y location on screen
     */
    protected double getYFromVirtual(double v){
        return (1 - v / boundary) * getHeight() / 2;
    }

    /**
     * @param v x location on the screen
     * @return x location in the virtual in unit 0.1mm
     */
    protected double getXFromPane(double v){
        return (v * 2.0 / getWidth() - 1) * boundary;
    }

    /**
     * @param v y location on the screen
     * @return y location in the virtual in unit 0.1mm
     */
    protected double getYFromPane(double v){
        return -((v * 2.0 / getHeight() - 1) * boundary);
    }


    public void processMouseClick(MouseEvent e){
        clickEvent.accept(findTarget(getXFromPane(e.getSceneX() - getXOnPane()),
                                     getYFromPane(e.getSceneY() - getYOnPane())), e);
    }

    public void processMousePress(MouseEvent e){
        focusEvent.accept(target = findTarget(getXFromPane(e.getSceneX() - getXOnPane()),
                                              getYFromPane(e.getSceneY() - getYOnPane())), e);
    }

    public void processMouseDrag(MouseEvent e){
        if (target != null && target.getClass() != PositionData.class){
            locationUpdateEvent.accept(target, new PositionData(getXFromPane(e.getSceneX() - getXOnPane()),
                                                                getYFromPane(e.getSceneY() - getYOnPane())));
        }
    }

    private PositionData findTarget(double x, double y){
        for (WormData w : worms){
            if (w.contact(x, y, w.size)) return w;
        }
        for (MoleculeData m : molecule){
            if (m.contact(x, y, FOOD_SIZE)) return m;
        }
        return new PositionData(x, y);
    }
}
