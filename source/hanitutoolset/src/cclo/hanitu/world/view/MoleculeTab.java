/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import cclo.hanitu.world.MoleculeConfig;
import cclo.hanitu.world.WorldConfig;

/**
 * @author antonio
 */
public class MoleculeTab extends AbstractTab<MoleculeConfig>{

    public MoleculeTab(WorldBuilder host, MoleculeConfig molecule){
        super(host, getTitle(molecule), molecule);
    }

    private static String getTitle(MoleculeConfig mc){
        String name = mc.type.name();
        return String.format("%c%s [%d]", name.charAt(0), name.substring(1).toLowerCase(), mc.id);
    }

    @Override
    protected int getIndexOf(){
        WorldConfig world = host.config;
        int base = 1 + world.wormCount();
        return base + (int)world.molecule().stream()
          .filter(f -> f.compareTo(target) < 0)
          .count();
    }

    @Override
    void update(){
        super.update();
        setTitle(getTitle(target));
    }

    @Override
    void close(){
        host.deleteMolecule(target.type, target.id);
    }
}
