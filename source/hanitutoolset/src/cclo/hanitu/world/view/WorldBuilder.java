/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import cclo.hanitu.*;
import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.WormData;
import cclo.hanitu.gui.*;
import cclo.hanitu.util.Events;
import cclo.hanitu.world.*;

import static cclo.hanitu.gui.MenuSession.*;

/**
 * @author antonio
 */
public class WorldBuilder implements FXMain.FXSimpleApplication{

    private static final String APPLICATION_TITLE = "World Config";

    WorldConfig config = WorldConfig.EMPTY;
    FileSession file;
    MenuSession menu;
    private List<String> runOptions;

    Stage primaryStage;
    TabPane contentPane;
    private Stage worldPlotWindow;

    private WorldTab world;
    private final List<WormTab> worms = new ArrayList<>();
    private final List<MoleculeTab> foods = new ArrayList<>();
    private final List<MoleculeTab> toxicant = new ArrayList<>();

    private final Events.ConsumerEvent<WormConfig> newWormEvent = new Events.ConsumerEvent<>();
    private final Events.ConsumerEvent<MoleculeConfig> newMoleculeEvent = new Events.ConsumerEvent<>();
    private final Events.ConsumerEvent<WormConfig> wormUpdateEvent = new Events.ConsumerEvent<>();
    private final Events.ConsumerEvent<MoleculeConfig> moleculeUpdateEvent = new Events.ConsumerEvent<>();
    private final Events.ConsumerEvent<WorldConfig> worldUpdateEvent = new Events.ConsumerEvent<>();
    private final Events.ConsumerEvent<WormConfig> deleteWormEvent = new Events.ConsumerEvent<>();
    private final Events.ConsumerEvent<MoleculeConfig> deleteMoleculeEvent = new Events.ConsumerEvent<>();

    public WorldBuilder(){
        file = new FileSession();
    }

    @Override
    public void start(Stage primaryStage){
        this.primaryStage = primaryStage;
        primaryStage.setTitle(APPLICATION_TITLE);
        VBox root = new VBox();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        //
        initFile();
        primaryStage.setOnCloseRequest(e -> stop());
        scene.setOnKeyPressed(this::handleKeyEvent);
        //
        root.getChildren().addAll(initMenu());
        //
        contentPane = new TabPane();
        root.getChildren().add(contentPane);
        //
        setWorldConfig(config);
        //
        primaryStage.show();
    }

    private void initFile(){
        file.setStage(primaryStage);
        file.setDefaultSaveFileName(HanituInfo.WORLD_CONFIG_DEFAULT_FILENAME);
        file.setFileFilter(HanituFileFilters.WORLD_CONFIG_FILTER,
                           HanituFileFilters.NORMAL_TEXT_FILTER,
                           HanituFileFilters.ALL_FILTER);
        file.setOnNewFile(() -> {
            stopWorldPlotStage();
            clear();
        });
        file.setOnOpenFile(path -> {
            stopWorldPlotStage();
            setWorldConfig(new WorldConfigLoader().load(path));
        });
        file.setOnSaveFile(path ->
                             FileSession.saveFileByTempReplace(path, os -> new WorldConfigPrinter().write(os, config)));
        file.setOnUpdate(() -> {
            setTitle(null);
        });
        file.setOnQuit(() -> {
            stopWorldPlotStage();
            primaryStage.close();
        });
    }

    private MenuBar initMenu(){
        Properties p = Base.loadPropertyInherit("gui");
        MenuBar menuBar = new MenuBar();
        String MENU_RUN = p.getProperty("world.menu.run", "Run");
        menu = new MenuSession(menuBar, MENU_FILE, MENU_EDIT, MENU_VIEW, MENU_RUN, MENU_HELP);
        menu.addMenuItems(MENU_FILE, 0, file.getMenuList());
        menu.addMenuItems(MENU_EDIT, 0,
                          createMenuItem(p.getProperty("world.menu.edit.createworm"), "SHORTCUT+W", e -> {
                              WormConfig config = askNewWorm();
                              if (config != null){
                                  createNewWorm(config);
                              }
                          }),
                          createMenuItem(p.getProperty("world.menu.edit.createfood"), "SHORTCUT+F",
                                         e -> createNewMolecule(askNewFood())),
                          createMenuItem(p.getProperty("world.menu.edit.createtoxicant"), "SHORTCUT+T",
                                         e -> createNewMolecule(askNewToxicant())));
        menu.addMenuItems(MENU_VIEW, 0,
                          createMenuItem(p.getProperty("world.menu.view.preview"), "F5", e -> {
                              if (worldPlotWindow == null){
                                  initWorldPlotWindow();
                              }
                              worldPlotWindow.show();
                          }));
        //
        MenuItem item = createMenuItem(p.getProperty("world.menu.run.run"), "F6", null);
        item.setOnAction(e -> {
            if (!checkWorldConfigRunnable()) return;
            Run run = new Run();
            if (runOptions != null){
                ExecutableData.set(run, runOptions);
            }
            run.setupWorldConfig(file.getCurrentWorkDirectory(), config);
            Thread th = new Thread(() -> {
                try {
                    item.setDisable(true);
                    run.run();
                } catch (Throwable ex){
                    Platform.runLater(() -> MessageSession.showErrorMessageDialog(ex));
                } finally {
                    item.setDisable(false);
                }
            }, "Run");
            th.start();
        });
        menu.addMenuItems(MENU_RUN, 0,
                          createMenuItem(p.getProperty("world.menu.run.circuit"), (String)null, e -> {
                              new Thread(new Circuit()).start();
                          }),
                          item);
        menu.addMenuItems(MENU_HELP, 0, About.getMenuItem());
        return menuBar;
    }

    public void stop(){
        file.askQuit();
    }

    private Stage initWorldPlotWindow(){
        worldPlotWindow = new Stage();
        worldPlotWindow.setTitle("Preview World");
        //
        PaintingPane pane = new PaintingPane(500, 500);
        pane.setBackgroundColor(Color.WHITE);
        //
        DefaultWorldCanvas canvas = new DefaultWorldCanvas(500, 500);
        pane.addPanel(canvas);
        //
        Scene s = new Scene(new VBox(pane));
        canvas.setup(s);
        canvas.setWorldConfig(config);
        canvas.setEditable(true);
        //
        worldPlotWindow.setScene(s);
        //
        String key = worldPlotWindow.toString();

        newWormEvent.add(key, w -> {
            canvas.setWorldConfig(config);
            pane.repaint();
        });
        newMoleculeEvent.add(key, w -> {
            canvas.setWorldConfig(config);
            pane.repaint();
        });
        worldUpdateEvent.add(key, w -> {
            canvas.setWorldConfig(config);
            pane.repaint();
        });
        deleteWormEvent.add(key, w -> {
            canvas.setWorldConfig(config);
            pane.repaint();
        });
        deleteMoleculeEvent.add(key, w -> {
            canvas.setWorldConfig(config);
            pane.repaint();
        });
        wormUpdateEvent.add(key, w -> {
            WormData data = canvas.getWorm(w.user, w.worm);
            if (data != null){
                data.x = w.x;
                data.y = w.y;
                data.size = w.size;
                pane.repaint();
            }
        });
        moleculeUpdateEvent.add(key, m -> {
            MoleculeData data = canvas.getMolecule(m.type, m.id);
            if (data != null){
                data.x = m.x;
                data.y = m.y;
                pane.repaint();
            }
        });
        canvas.focusEvent.add((positionData, mouseEvent) -> {
            if (positionData instanceof WormData){
                WormData data = (WormData)positionData;
                getWorm(data.user, data.worm).select();
            } else if (positionData instanceof MoleculeData){
                MoleculeData data = (MoleculeData)positionData;
                getMolecule(data.type, data.id).select();
            } else {
                world.select();
            }
        });
        canvas.clickEvent.add((positionData, mouseEvent) -> {
            if (mouseEvent.getButton() == MouseButton.PRIMARY
                && mouseEvent.getClickCount() == 2
                && !(positionData instanceof WormData)
                && !(positionData instanceof MoleculeData)){
                WormConfig config = askNewWorm();
                if (config != null){
                    config.x = (int)canvas.getXFromPane(mouseEvent.getSceneX());
                    config.y = (int)canvas.getYFromPane(mouseEvent.getSceneY());
                    createNewWorm(config);
                    pane.repaint();
                }
            }
        });
        canvas.locationUpdateEvent.add((positionData, newPlace) -> {
            if (positionData instanceof WormData){
                WormData data = (WormData)positionData;
                WormConfig worm = config.getWorm(data.user, data.worm);
                if (worm != null){
                    worm.x = (int)newPlace.x;
                    worm.y = (int)newPlace.y;
                    getWorm(data.user, data.worm).update();
                    pane.repaint();
                }
            } else if (positionData instanceof MoleculeData){
                MoleculeData data = (MoleculeData)positionData;
                MoleculeConfig molecule = config.getMolecule(data.type, data.id);
                if (molecule != null){
                    molecule.x = (int)newPlace.x;
                    molecule.y = (int)newPlace.y;
                    getMolecule(data.type, data.id).update();
                    pane.repaint();
                }
            }
        });
        pane.repaint();
        return worldPlotWindow;
    }

    private void stopWorldPlotStage(){
        if (worldPlotWindow != null){
            String key = worldPlotWindow.toString();
            worldPlotWindow.close();
            newWormEvent.removeEvent(key);
            newMoleculeEvent.removeEvent(key);
            deleteWormEvent.removeEvent(key);
            deleteMoleculeEvent.removeEvent(key);
            wormUpdateEvent.removeEvent(key);
            moleculeUpdateEvent.removeEvent(key);
            worldUpdateEvent.removeEvent(key);
            worldPlotWindow = null;
        }
    }


    public void setWorldConfig(Path worldConfigPath, boolean failOnLoad) throws IOException{
        setWorldConfig(new WorldConfigLoader(failOnLoad).load(worldConfigPath));
        file.setCurrentFilePath(worldConfigPath);
        file.setCurrentFileModified(false);
    }

    public void setWorldConfig(WorldConfig config){
        worms.clear();
        foods.clear();
        toxicant.clear();
        //
        this.config = null;
        stopWorldPlotStage();
        if (primaryStage != null){
            world = new WorldTab(this, config);
            config.worms().forEach(this::createNewWorm);
            config.molecule().forEach(this::createNewMolecule);
            world.setOnUpdate(worldUpdateEvent);
        }
        //
        this.config = config;
        if (primaryStage != null){
            contentPane.getTabs().clear();
            world.add();
            worms.forEach(AbstractTab::add);
            foods.forEach(AbstractTab::add);
            toxicant.forEach(AbstractTab::add);
            //
            world.select();
        }
        file.setCurrentFileModified(false);
        worldUpdateEvent.accept(config);
    }

    public void setSaveFileName(Path filePath){
        file.setCurrentFilePath(filePath);
        file.setCurrentFileModified(true);
    }

    public void setTitle(String title){
        if (primaryStage == null) return;
        StringBuilder sb = new StringBuilder();
        if (file.isCurrentFileModified()){
            sb.append("* ");
        }
        sb.append(APPLICATION_TITLE);
        if (title != null){
            sb.append("[").append(title).append("]");
        }
        Path path = file.getCurrentFilePath();
        if (path != null){
            sb.append(" - ").append(path);
        }
        primaryStage.setTitle(sb.toString());
    }

    public void setRunHanituOptions(List<String> options){
        runOptions = options;
    }

    private void handleKeyEvent(KeyEvent e){
        if (e.isControlDown()){
            WormConfig config = null;
            switch (e.getCode()){
            case DIGIT0:
                config = askNewWorm(0);
                break;
            case DIGIT1:
                config = askNewWorm(1);
                break;
            case DIGIT2:
                config = askNewWorm(2);
                break;
            case DIGIT3:
                config = askNewWorm(3);
                break;
            case DIGIT4:
                config = askNewWorm(4);
                break;
            case DIGIT5:
                config = askNewWorm(5);
                break;
            case DIGIT6:
                config = askNewWorm(6);
                break;
            case DIGIT7:
                config = askNewWorm(7);
                break;
            case DIGIT8:
                config = askNewWorm(8);
                break;
            case DIGIT9:
                config = askNewWorm(9);
                break;
            }
            if (config != null){
                createNewWorm(config);
            }
        }
    }

    public WormTab createNewWorm(WormConfig config){
        Log.debug(() -> "create: worm {user=" + config.user + ", id=" + config.worm + "}");
        WormTab ret = new WormTab(this, config);
        worms.add(ret);
        if (this.config != null){
            this.config.addWorm(config);
            ret.add();
            ret.select();
        }
        ret.setOnUpdate(wormUpdateEvent);
        file.setCurrentFileModified(true);
        newWormEvent.accept(config);
        return ret;
    }

    public MoleculeTab createNewMolecule(MoleculeConfig config){
        Log.debug(() -> "create: " + config.type + " {id=" + config.id + "}");
        MoleculeTab ret = new MoleculeTab(this, config);
        switch (config.type){
        case FOOD:
            foods.add(ret);
            if (this.config != null){
                this.config.addMolecule(config);
            }
            break;
        case TOXICANT:
            toxicant.add(ret);
            if (this.config != null){
                this.config.addMolecule(config);
            }
            break;
        default:
            throw new IllegalArgumentException("unknown molecule type : " + config.type);
        }
        if (this.config != null){
            ret.add();
            ret.select();
        }
        ret.setOnUpdate(moleculeUpdateEvent);
        file.setCurrentFileModified(true);
        newMoleculeEvent.accept(config);
        return ret;
    }

    WormTab getWorm(int user, int worm){
        for (WormTab w : worms){
            if (w.target.user == user && w.target.worm == worm) return w;
        }
        return null;
    }

    MoleculeTab getMolecule(MoleculeType type, int id){
        if (type == MoleculeType.FOOD){
            for (MoleculeTab f : foods){
                if (f.target.id == id) return f;
            }
        } else if (type == MoleculeType.TOXICANT){
            for (MoleculeTab t : toxicant){
                if (t.target.id == id) return t;
            }
        }
        return null;
    }

    public WormConfig deleteWorm(int user, int worm){
        Log.debug(() -> "delete : worm {user=" + user + ", id=" + worm + "}");
        WormTab target = getWorm(user, worm);
        if (target == null) return null;

        if (!worms.remove(target) || config.removeWorm(target.target) == null){
            throw new RuntimeException("delete worm error");
        }

        //re-assign ID
        config.reassignID();
        //update tab
        target.remove();
        worms.forEach(AbstractTab::update);

        file.setCurrentFileModified(true);
        deleteWormEvent.accept(target.target);
        return target.target;
    }

    public MoleculeConfig deleteMolecule(MoleculeType type, int id){
        Log.debug(() -> "delete : " + type + " {id=" + id + "}");
        if (type != MoleculeType.FOOD && type != MoleculeType.TOXICANT){
            throw new IllegalArgumentException("unknown molecule type : " + type);
        }

        MoleculeTab target = getMolecule(type, id);
        if (target == null) return null;

        boolean isFood = type == MoleculeType.FOOD;

        if (isFood){
            if (!foods.remove(target) || config.removeMolecule(target.target) == null){
                throw new RuntimeException("delete food error");
            }
        } else {
            if (!toxicant.remove(target) || config.removeMolecule(target.target) == null){
                throw new RuntimeException("delete toxicant error");
            }
        }

        //re-assign ID, start from 1
        config.reassignID();

        //update tab
        target.remove();
        (isFood? foods: toxicant).forEach(AbstractTab::update);

        file.setCurrentFileModified(true);
        deleteMoleculeEvent.accept(target.target);
        return target.target;
    }

    public void clear(){
        setWorldConfig(new WorldConfig());
        file.setCurrentFilePath(null);
        file.setCurrentFileModified(false);
    }

    public int foodCount(){
        return config == null? 0: config.foodCount();
    }

    public int toxicantCount(){
        return config == null? 0: config.toxicantCount();
    }

    WormConfig askNewWorm(){
        String input = MessageSession.showInputDialog("Create new worm", "Please user ID", text -> {
            try {
                int i = Integer.parseInt(text);
                if (i >= 0){
                    return null;
                }
                return "negative ID value";
            } catch (NumberFormatException e){
                return e.getMessage();
            }
        });
        if (input == null) return null;
        int user = Integer.parseInt(input);
        return askNewWorm(user);
    }

    WormConfig askNewWorm(int user){
        if (user < 0){
            MessageSession.showErrorMessageDialog("negative user ID");
            return null;
        }
        int id = (int)config.worms().stream()
          .filter(w -> w.user == user)
          .count();
        if (id == 0){
            WormConfig c = new WormConfig();
            c.user = user;
            c.worm = 0;
            c.size = 3;
            if (config.wormCount() != 0){
                WormConfig another = config.worms().get(0);
                c.timeDecay = another.timeDecay;
                c.stepDecay = another.stepDecay;
            }
            return c;
        } else {
            return new WormConfig(user, id, getWorm(user, 0).target);
        }
    }

    MoleculeConfig askNewFood(){
        int count = foodCount();
        if (count == 0){
            MoleculeConfig config = new MoleculeConfig();
            config.type = MoleculeType.FOOD;
            config.id = 1;
            return config;
        } else {
            return new MoleculeConfig(count + 1, getMolecule(MoleculeType.FOOD, count).target);
        }
    }

    MoleculeConfig askNewToxicant(){
        int count = toxicantCount();
        if (count == 0){
            MoleculeConfig config = new MoleculeConfig();
            config.type = MoleculeType.TOXICANT;
            config.id = 1;
            return config;
        } else {
            return new MoleculeConfig(count + 1, getMolecule(MoleculeType.TOXICANT, count).target);
        }
    }

    void showPlotWindow(){
        //XXX Unsupported Operation WorldSession.showPlotWindow
        throw new UnsupportedOperationException();
    }

    boolean checkWorldConfigRunnable(){
        for (WormConfig worm : config.worms()){
            WormTab tab = getWorm(worm.user, worm.worm);
            if (tab.isCircuitFileExist()){
                continue;
            }
            tab.select();
            tab.checkCircuitFileExist(null, false);
            return false;
        }
        return true;
    }


}
