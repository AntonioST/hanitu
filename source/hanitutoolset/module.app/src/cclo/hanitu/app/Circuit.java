/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.app;

import java.io.IOException;
import java.nio.file.Path;
import java.util.OptionalInt;
import java.util.Properties;

import cclo.hanitu.Base;
import cclo.hanitu.circuit.CircuitInfo;
import cclo.hanitu.circuit.CircuitInfoLoader;
import cclo.hanitu.circuit.Synapse;
import cclo.hanitu.circuit.view.EditCircuitMode;
import cclo.hanitu.circuit.view.LabCircuitMode;
import cclo.hanitu.circuit.view.VisualCircuit;
import cclo.hanitu.circuit.view.VisualCircuitMode;
import cclo.hanitu.data.FiringrateFunction;
import cclo.hanitu.data.SingleSpikeFilter;
import cclo.hanitu.exe.Argument;
import cclo.hanitu.exe.Executable;
import cclo.hanitu.exe.Option;
import cclo.hanitu.gui.FXApplication;

/**
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class Circuit extends Executable{

    @Option(value = "new", arg = "FILE", description = "option.new")
    public Path newFileName = null;

    @Option(value = "fail-on-load", standard = false,
      description = "stop program and print message if anr error during loading circuit file.")
    public boolean failOnLoad = false;

    @Option(value = "read-only", standard = false,
      description = "display circuit under read-only mode, you cannot modify circuit")
    public boolean readOnly = false;

    @Option(value = "info", standard = false, order = 1,
      description = "~*y{WARNING~} : this feature is under development.")
    @Deprecated
    public boolean infoMode;

    @Option(value = "mode", arg = "MODE", standard = false, order = 2,
      description = "~*y{WARNING~} : this feature is under development.")
    public String mode = "default";

    @Option(value = "spike", arg = "FILE", standard = false, order = 3,
      description = "~*y{WARNING~} : this feature is under development.")
    public String labModeSpikeFile;

    @Option(shortName = 'W', value = "window", standard = false, order = 4,
      description = "~*y{WARNING~} : this feature is under development.")
    public int labModeTimeWindow = 100;

    @Option(shortName = 's', value = "step", standard = false, order = 4,
      description = "~*y{WARNING~} : this feature is under development.")
    public int labModeTimeStep = 10;

    @Option(shortName = 'u', value = "user", arg = "ID", standard = false, order = 5,
      description = "~*y{WARNING~} : this feature is under development.")
    public String labModeSpikeUser;

    @Option(shortName = 'w', value = "worm", arg = "ID", standard = false, order = 5,
      description = "~*y{WARNING~} : this feature is under development.")
    public String labModeSpikeWorm;

    @Option(value = "event", arg = "FILE", standard = false, order = 6,
      description = "~*y{WARNING~} : this feature is under development.")
    public String labModeEventFile;

    @Option(value = "fps", arg = "VALUE", standard = false, order = 7,
      description = "~*y{WARNING~} : this feature is under development.")
    public int labModeFps = 10;

    @Argument(index = 0, value = "FILE", description = "args.file")
    public Path circuitFilePath = null;

    @Override
    public String getName(){
        return Base.loadProperties().getProperty("name");
    }

    @Override
    public String getDescription(){
        return Base.loadProperties().getProperty("desp");
    }

    @Override
    public Properties getProperties(){
        return Base.loadProperties();
    }

    public VisualCircuitMode getMode(String mode){
        if (mode == null) return null;
        switch (mode){
        case "lab":
            return new LabCircuitMode(new SingleSpikeFilter(labModeSpikeUser, labModeSpikeWorm));
        case "readonly":
            return null;
        default:
        case "default":
        case "edit":
            return new EditCircuitMode();
        }
    }

    @Override
    public void start() throws IOException{
        if (infoMode){
            printCircuitInfo();
        } else {
            startCircuit();
        }
    }

    private void startCircuit() throws IOException{
        VisualCircuit instance = FXApplication.launch(VisualCircuit.class);
        VisualCircuitMode circuitMode = readOnly? null: getMode(mode);
        instance.setMode(circuitMode);
        if (newFileName != null && circuitFilePath != null){
            instance.setCircuit(circuitFilePath, failOnLoad);
            if (circuitMode != null){
                circuitMode.setSaveFileName(newFileName);
            }
        } else if (newFileName != null){
            // circuitFilePath == null
            instance.setCircuit(newFileName, failOnLoad);
            if (circuitMode != null){
                circuitMode.setSaveFileName(null);
            }
        } else if (circuitFilePath != null){
            // newFileName == null
            instance.setCircuit(circuitFilePath, failOnLoad);
        }
        if (circuitMode instanceof LabCircuitMode){
            runLabMode((LabCircuitMode)circuitMode);
        }
    }

    private void runLabMode(LabCircuitMode mode){
        if (labModeSpikeFile != null){
            LabCircuitMode.LabPlayer source = new LabCircuitMode.LabPlayer();
            source.addUpdateListener(mode);
            source.setSpikeFile(labModeSpikeFile);
            if (labModeEventFile != null){
                source.setEventFile(labModeEventFile);
            }
            source.setFiringrateFunction(new FiringrateFunction(labModeTimeWindow, labModeTimeStep));
            source.setFps(labModeFps);
            source.play();
        }
    }

    @Deprecated
    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    private void printCircuitInfo() throws IOException{
        if (circuitFilePath != null){
            CircuitInfoLoader loader = new CircuitInfoLoader();
            CircuitInfo info = loader.load(circuitFilePath);
            System.out.println("source : " + info.source());
            System.out.println("total neuron : " + info.neuronCount());
            System.out.println("neuron ID : receptor count");
            for (String id : info.getNeuronIDSet()){
                OptionalInt r = info.receptorCount(id);
                if (r.isPresent()){
                    System.out.println("  " + id + " : " + r.getAsInt());
                } else {
                    System.out.println("  " + id + " : -");
                }
            }
            System.out.println("communicate");
            for (Synapse synapse : info.getCommunicateSynapse()){
                System.out.println("  " + synapse.prevSynNeuID.get() + " -> " + synapse.postSynNeuID.get());
            }
        }
    }
}
