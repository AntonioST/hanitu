/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.app;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.Properties;

import cclo.hanitu.Base;
import cclo.hanitu.exe.Executable;
import cclo.hanitu.exe.Option;

/**
 * @author antonio
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class Flysim extends Executable{

    public static final String LOCALHOST = "localhost";

    @Option(value = "host", arg = "IP", description = "host ip address")
    public String address = LOCALHOST;

    @Option(shortName = 'p', value = "port", arg = "PORT", description = "server port")
    public int port = 8889;

    @Option(shortName = 'd', value = "daemon", description = "daemon mode")
    public boolean daemon;

    @Option(value = "response", description = "check flysim server response")
    public boolean response;

    private InetSocketAddress socketAddress;

    @Override
    public String getName(){
        return Base.loadProperties().getProperty("name");
    }

    @Override
    public String getDescription(){
        return Base.loadProperties().getProperty("desp");
    }

    @Override
    public Properties getProperties(){
        return Base.loadProperties();
    }

    @Override
    public void start() throws Throwable{
        if (response){
            if (checkFlysimResponse()){
                //noinspection UseOfSystemOutOrSystemErr
                System.err.println("flysim is running : " + socketAddress);
                exit(0);
            } else {
                //noinspection UseOfSystemOutOrSystemErr
                System.err.println("flysim not running : " + socketAddress);
                exit(1);

            }
        } else {
            FlysimServer server = new FlysimServer(daemon);
            server.port = port;
            server.start();
        }
    }

    public boolean checkFlysimResponse(){
        socketAddress = new InetSocketAddress(address, port);
        Socket socket = new Socket();
        try {
            socket.connect(socketAddress);
            //
            byte[] command = "END_CLIENT".getBytes();
            byte[] buffer = new byte[32];
            Arrays.fill(buffer, (byte)0);
            System.arraycopy(command, 0, buffer, 0, command.length);
            socket.getOutputStream().write(buffer);
        } catch (IOException ex){
            return false;
        } finally {
            //noinspection EmptyCatchBlock
            try {
                socket.close();
            } catch (IOException e){
            }
        }
        return true;
    }

}
