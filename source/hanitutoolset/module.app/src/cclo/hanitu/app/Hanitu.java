/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.app;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import cclo.hanitu.Base;
import cclo.hanitu.exe.NativeExecutable;
import cclo.hanitu.exe.Option;

/**
 * @author antonio
 */
@SuppressWarnings({"WeakerAccess"})
public class Hanitu extends NativeExecutable{

    /**
     * property of the location of the hanitu
     */
    public static final String PROPERTY_PATH = "cclo.hanitu.hanitu";
    /**
     * property of the output stream of the hanitu
     */
    public static final String PROPERTY_OUT = "cclo.hanitu.hanitu.out";
    /**
     * property of the error output stream of the hanitu
     */
    public static final String PROPERTY_ERR = "cclo.hanitu.hanitu.err";
    public static final Path PATH;

    static{
        Path path = null;
        String property = System.getProperty(PROPERTY_PATH);
        if (property != null){
            path = Paths.get(property).toAbsolutePath().normalize();
        } else if (Base.HOME != null){
            try {
                path = findExecutable("Hanitu.*out");
            } catch (IOException e){
                path = null;
            }
        }
        PATH = path;
        if (PATH != null){
            LOG.debug("find {} at {}", Hanitu.class.getSimpleName(), PATH);
        } else {
            LOG.debug("not-find {}", Hanitu.class.getSimpleName());
        }
    }

    @Option(shortName = 'w', value = "world", arg = "FILE", order = 0, description = "option.world")
    public Path worldConfigPath;

    @Option(shortName = 'l', value = "location", arg = "FILE", order = 0, description = "option.location")
    public Path locationOutputPath;

    @Option(shortName = 's', value = "spike", arg = "FILE", order = 0, description = "option.spike")
    public Path spikeOutputPath;

    @Option(value = "timeout", arg = "TIME", order = 2, description = "option.timeout")
    public int timeout = -1;

    @Option(value = "loc-time-step", arg = "TIME", order = 2, description = "option.loctimestep")
    public int locTimeStep = 2;

    @Option(value = "overlap-ignore", order = 2, description = "option.overlapignore")
    public boolean overlapIgnore = false;

    @Option(shortName = 'p', value = "port", arg = "PORT", order = 1, description = "option.port")
    public int port = 8889;

    public Hanitu(){
        super(PATH);
        outStream = System.getProperty(PROPERTY_OUT, "null");
        errStream = System.getProperty(PROPERTY_ERR, "null");
    }

    @Override
    public String getName(){
        return Base.loadProperties().getProperty("name");
    }

    @Override
    public String getDescription(){
        return Base.loadProperties().getProperty("desp");
    }

    @Override
    public Properties getProperties(){
        return Base.loadProperties();
    }

    @Override
    public String version() throws Throwable{
        return getProgramOutput("--version").replaceFirst("(?m:.*version:)", "").trim();
    }

    @Override
    public String help() throws Throwable{
        return getProgramOutput("--help");
    }

    @Override
    protected ProcessBuilder createProcessBuilder(){
        LOG.trace("{} setup", Hanitu.class.getSimpleName());
        setupWorkingDirectory();
        List<String> args = new ArrayList<>();
        args.add("--port");
        args.add(Integer.toString(port));
        if (worldConfigPath != null){
            args.add("--world");
            args.add(worldConfigPath.toAbsolutePath().toString());
        }
        if (locationOutputPath != null){
            args.add("--location");
            args.add(locationOutputPath.toAbsolutePath().toString());
        }
        if (spikeOutputPath != null){
            args.add("--spike");
            args.add(spikeOutputPath.toAbsolutePath().toString());
        }
        if (timeout > 0){
            args.add("--timeout");
            args.add(Integer.toString(timeout));
        }
        args.add("--loc-time-step");
        args.add(Integer.toString(locTimeStep));
        if (overlapIgnore){
            args.add("--overlap-ignore");
        }
        return createProcessBuilder(args);
    }
}
