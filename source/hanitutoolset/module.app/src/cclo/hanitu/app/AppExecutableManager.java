/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.app;

import java.util.Map;

import cclo.hanitu.exe.Executable;
import cclo.hanitu.exe.ExecutableManager;

/**
 * @author antonio
 */
public class AppExecutableManager implements ExecutableManager{

    @Override
    public Map<String, Class<Executable>> getExecutables(){
        return mapOf("circuit", Circuit.class,
                     "world", World.class,
                     "plot", Plot.class,
                     "run", Run.class,
                     "chart", Chart.class);
    }
}
