/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.app;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import cclo.hanitu.Base;
import cclo.hanitu.data.EventData;
import cclo.hanitu.data.LocationData;
import cclo.hanitu.exe.Argument;
import cclo.hanitu.exe.Executable;
import cclo.hanitu.exe.Option;
import cclo.hanitu.gui.FXApplication;
import cclo.hanitu.gui.HanituFile;
import cclo.hanitu.io.TailStream;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.WorldConfigLoader;
import cclo.hanitu.world.view.*;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;

/**
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class Plot extends Executable{

    @Option(shortName = 'w', value = "world", arg = "FILE", description = "option.world")
    public String worldConfigFileName;

    @Option(shortName = 'o', value = "output", arg = "FILE", description = "option.output")
    public String outputFileName = null;

    @Option(value = "style", arg = "CLASS", order = 1, description = "option.style")
    public String style = "shadow";

    @Option(value = "fps", arg = "VALUE", order = 0, description = "option.fps")
    public int fps = -1;

    @Option(value = "static", standard = false,
      description = "display a static world. don't not load Locations file.\n" +
                    "should specific given world config file path. Otherwise program will create a simple world")
    public boolean staticWorld = false;

    @Argument(index = 0, value = "FILE", description = "args.loc")
    public String locationFileName = null;

    @Override
    public String getName(){
        return Base.loadProperties().getProperty("name");
    }

    @Override
    public String getDescription(){
        return Base.loadProperties().getProperty("desp");
    }

    @Override
    public Properties getProperties(){
        return Base.loadProperties();
    }

    public WorldConfig setupWorldConfig() throws IOException{
        if (worldConfigFileName == null){
            worldConfigFileName = HanituFile.WORLD_CONFIG_DEFAULT_FILENAME;
            if (locationFileName != null && locationFileName.contains("/")){
                worldConfigFileName = locationFileName.substring(0, locationFileName.lastIndexOf("/") + 1)
                                      + worldConfigFileName;
            }
        }
        LOG.debug("world config path : {}", worldConfigFileName);
        WorldConfigLoader loader = new WorldConfigLoader();
        loader.setFailOnLoad(true);
        loader.setIgnoreCircuitFile(true);
        return loader.load(Paths.get(worldConfigFileName));
    }

    public TailStream<LocationData> setupLocationStream() throws IOException{
        TailStream<String> source = TailStream.followCommandLineFile(locationFileName);
        source = source.autoClose();
        if (outputFileName != null){
            if (outputFileName.equals("-")){
                //noinspection UseOfSystemOutOrSystemErr
                source = source.peek(System.out::println);
            } else {
                OutputStream os = Files.newOutputStream(Paths.get(outputFileName), CREATE, TRUNCATE_EXISTING);
                PrintStream out = new PrintStream(os);
                source = source.peek(out::println).onClose(out::close);
            }
        }
        return source.mapIgnoreError(LocationData::parseLine);
    }

    private Path findSpikeFile(){
        String file;
        if (locationFileName != null && locationFileName.contains("/")){
            file = locationFileName.substring(0, locationFileName.lastIndexOf("/") + 1)
                   + HanituFile.SPIKE_DEFAULT_FILENAME;
        } else {
            file = HanituFile.SPIKE_DEFAULT_FILENAME;
        }
        return Paths.get(file);
    }

    private Path findEventFile(){
        String file;
        if (locationFileName != null && locationFileName.contains("/")){
            file = locationFileName.substring(0, locationFileName.lastIndexOf("/") + 1) + "Event.dat";
        } else {
            file = "Event.dat";
        }
        return Paths.get(file);
    }

    public TailStream<EventData> setupEventStream(){
        Path path = findEventFile();
        if (!Files.exists(path)) return TailStream.nullStream();
        LOG.debug("setup event file : {}", path);
        return TailStream.follow(path).mapIgnoreError(EventData::parseLine);
    }

    @Override
    public void start() throws IOException{
        //event setting
        if (staticWorld){
            startInStaticMode();
        } else {
            startInNormalMode();
        }
    }

    private void startInStaticMode(){
        AbstractWorldCanvas canvas = WorldCanvasProvider.getCanvas(style);
        WorldViewer frame = FXApplication.launch(WorldViewer.class);
        frame.module.add(new WorldViewerModuleStatic());
        frame.setCanvas(canvas);
    }

    private void startInNormalMode() throws IOException{
        WorldConfig config = setupWorldConfig();
        AbstractWorldCanvas canvas = WorldCanvasProvider.getCanvas(style);
        WorldViewer frame = FXApplication.launch(WorldViewer.class);
        //
        WorldViewerModulePlot plot = frame.module.add(new WorldViewerModulePlot());
        plot.module.add(new WorldPlotModuleInformation());
        plot.module.add(new WorldPlotModuleMessage());
        WorldPlotModuleChart chart = plot.module.add(new WorldPlotModuleChart());
        plot.module.add(new WorldPlotModuleLab());
        //
        Path spikeFilePath = findSpikeFile();
        if (Files.exists(spikeFilePath)) chart.setSpikeFilePath(spikeFilePath);
        //
        if (fps > 0){
            plot.setFPS(fps);
        }
        frame.setCanvas(canvas);
        frame.setWorld(config);
        frame.setEditable(false);
        //
        plot.setLocationStream(setupLocationStream());
        plot.setEventStream(setupEventStream());
        //
        plot.play();
    }
}
