/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.app;

import java.nio.file.Path;
import java.util.Properties;

import cclo.hanitu.Base;
import cclo.hanitu.chart.FiringrateChartViewer;
import cclo.hanitu.exe.Argument;
import cclo.hanitu.exe.Executable;
import cclo.hanitu.gui.FXApplication;

/**
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class Chart extends Executable{

    @Argument(index = 0, value = "FILE", description = "args.file")
    public Path spikeFilePath = null;

    @Override
    public String getName(){
        return Base.loadProperties().getProperty("name");
    }

    @Override
    public String getDescription(){
        return Base.loadProperties().getProperty("desp");
    }

    @Override
    public Properties getProperties(){
        return Base.loadProperties();
    }

    @Override
    public void start() throws Throwable{
        FiringrateChartViewer viewer = FXApplication.launch(FiringrateChartViewer.class);
        viewer.setSpikeFile(spikeFilePath);
        viewer.fetchAllFiringrateResult();
    }
}
