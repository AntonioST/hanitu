/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.app;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Properties;

import cclo.hanitu.Base;
import cclo.hanitu.exe.Argument;
import cclo.hanitu.exe.Executable;
import cclo.hanitu.exe.Option;
import cclo.hanitu.gui.FXApplication;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.builder.WorldConfigModuleEdit;
import cclo.hanitu.world.builder.WorldConfigModulePreview;
import cclo.hanitu.world.builder.WorldConfigModuleRun;
import cclo.hanitu.world.builder.WorldConfigViewer;

/**
 * @author antonio
 */
@SuppressWarnings({"CanBeFinal", "WeakerAccess", "unused"})
public class World extends Executable{

    @Option(value = "read-only", description = "option.readonly")
    public boolean readOnly = false;

    @Option(value = "fail-on-load", standard = false,
      description = "stop program and print message if any error during loading world config file.")
    public boolean failOnLoad = false;

    @Option(value = "disable-edit-switch", standard = false,
      description = "disable world edit mode switch.")
    public boolean disableEditSwitch = false;

    @Option(value = "disable-run-hanitu", standard = false,
      description = "disable run hanitu function.")
    public boolean disableRunHanitu = false;

    @Option(value = "run-options", arg = "OPTIONS", standard = false,
      description = "the options used by ~*g{hanitu_run~}*, use ',' to spread each options")
    public String runOptions;

    @Argument(index = 0, value = "FILE", description = "args.file")
    public Path worldConfigFilePath = null;
    public WorldConfig config;

    @Override
    public String getName(){
        return Base.loadProperties().getProperty("name");
    }

    @Override
    public String getDescription(){
        return Base.loadProperties().getProperty("desp");
    }

    @Override
    public Properties getProperties(){
        return Base.loadProperties();
    }

    @Override
    public void start() throws IOException{
        WorldConfigViewer builder = FXApplication.launch(WorldConfigViewer.class);
        if (config != null){
            builder.setWorldConfig(config);
            if (worldConfigFilePath != null){
                builder.setSaveFileName(worldConfigFilePath);
            }
        } else if (worldConfigFilePath != null){
            builder.setWorldConfig(worldConfigFilePath, failOnLoad);
        }
        WorldConfigModuleEdit edit = builder.module.add(new WorldConfigModuleEdit());
        builder.module.add(new WorldConfigModulePreview());
        WorldConfigModuleRun run = builder.module.add(new WorldConfigModuleRun());
        if (runOptions != null){
            run.setRunHanituOptions(Arrays.asList(runOptions.split(",")));
        }
        edit.setEditable(!readOnly);
        edit.setEditModeSwitch(!disableEditSwitch);
        run.setEnableRunHanitu(!disableRunHanitu);
    }
}
