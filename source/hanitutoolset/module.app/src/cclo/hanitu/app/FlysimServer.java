/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.app;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import cclo.hanitu.Base;
import cclo.hanitu.exe.Argument;
import cclo.hanitu.exe.NativeExecutable;

/**
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class FlysimServer extends NativeExecutable{

    /**
     * property of the location of the flysim
     */
    public static final String PROPERTY_PATH = "cclo.hanitu.flysim";
    /**
     * property of the port of the flysim daemon.
     *
     * It depends on the native hanitu program.
     */
    public static final String PROPERTY_PORT = "cclo.hanitu.flysim.port";
    /**
     * property of the output stream of the flysim
     */
    public static final String PROPERTY_OUT = "cclo.hanitu.flysim.out";
    /**
     * property of the error output stream of the flysim
     */
    public static final String PROPERTY_ERR = "cclo.hanitu.flysim.err";
    public static final Path PATH;

    static{
        Path path = null;
        String property = System.getProperty(PROPERTY_PATH);
        if (property != null){
            path = Paths.get(property).toAbsolutePath().normalize();
        } else if (Base.HOME != null){
            try {
                path = findExecutable("flysim.*out");
            } catch (IOException e){
                NativeExecutable.LOG.warn("find flysim path", e);
            }
        }
        PATH = path;
        if (PATH != null){
            NativeExecutable.LOG.debug("find {} at {}", FlysimServer.class.getSimpleName(), PATH);
        } else {
            NativeExecutable.LOG.debug("not-find {}", FlysimServer.class.getSimpleName());
        }
    }

    @Argument(value = "PORT", index = 0, description = "option.daemon")
    public int port = Integer.getInteger(PROPERTY_PORT, 8889);

    public FlysimServer(boolean daemon){
        super(PATH);
        outStream = System.getProperty(PROPERTY_OUT, "null");
        errStream = System.getProperty(PROPERTY_ERR, "null");
        if (!daemon){
            SHUT_DOWN_EVENT.add(() -> {
                //noinspection EmptyCatchBlock
                try {
                    close();
                } catch (IOException e){
                }
            });
        }
    }

    @Override
    public String getName(){
        return Base.loadProperties(Flysim.class).getProperty("name");
    }

    @Override
    public String getDescription(){
        return Base.loadProperties(Flysim.class).getProperty("desp");
    }

    @Override
    public Properties getProperties(){
        return Base.loadProperties(Flysim.class);
    }

    @Override
    public String version() throws Throwable{
        String output = getProgramOutput("-h");
        for (String line : output.split("\n")){
            if (line.startsWith("version")){
                return line.substring(8);
            }
        }
        return "unknown";
    }

    @Override
    public String help() throws Throwable{
        return getProgramOutput("-h");
    }

    @Override
    public void start() throws IOException{
        setupProcess();
    }

    @Override
    protected ProcessBuilder createProcessBuilder(){
        NativeExecutable.LOG.trace("{} setup", FlysimServer.class.getSimpleName());
        return createProcessBuilder("-daemon", Integer.toString(port));
    }
}
