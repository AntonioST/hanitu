/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.Base;
import cclo.hanitu.data.EventData;
import cclo.hanitu.data.LocationData;
import cclo.hanitu.exe.Argument;
import cclo.hanitu.exe.Executable;
import cclo.hanitu.exe.Option;
import cclo.hanitu.gui.FXApplication;
import cclo.hanitu.gui.HanituFile;
import cclo.hanitu.io.TailStream;
import cclo.hanitu.util.ErrorFunction;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.WorldConfigLoader;
import cclo.hanitu.world.WorldConfigPrinter;
import cclo.hanitu.world.view.*;

/**
 * @author antonio
 */
@SuppressWarnings({"WeakerAccess", "UnusedReturnValue", "unused"})
public class Run extends Executable{

    protected static final Logger LOG = LoggerFactory.getLogger(Executable.class);

    @Option(shortName = 'l', value = "location", arg = "FILE", order = 1, description = "option.location")
    public Path locationFile = Paths.get(HanituFile.LOCATION_DEFAULT_FILENAME);

    @Option(shortName = 's', value = "spike", arg = "FILE", order = 1, description = "option.spike")
    public Path spikeFile = Paths.get(HanituFile.SPIKE_DEFAULT_FILENAME);

    @Option(value = "timeout", arg = "TIME", order = 2, description = "option.timeout")
    public int timeout = -1;

    @Option(value = "loc-time-step", arg = "TIME", order = 2, description = "option.loctimestep")
    public int locTimeStep = 2;

    @Option(value = "server", arg = "IP", order = 3, standard = false, description = "option.server")
    public String address = Flysim.LOCALHOST;

    @Option(shortName = 'p', value = "port", arg = "PORT", order = 3, description = "option.port")
    public int port = 8889;

    @Option(value = "no-plot", order = 4, description = "option.noplot")
    public boolean noPlot = false;

    @Option(value = "quiet", order = 4, standard = false,
      description = "silence output anf disable display virtual world")
    public boolean quiet = false;

    @Option(value = "backup", order = 4, description = "option.backup")
    public boolean backup = false;

    @Option(value = "style", arg = "CLASS", order = 5, description = "option.style")
    public String style = "shadow";

    @Option(value = "disable-routing-port", standard = false,
      description = "disable port routing if hanitu or flysim start fail.")
    public boolean disablePortRouting = false;

    @Option(value = "keep-temp", standard = false,
      description = "keep temp directory after executable terminated")
    public boolean keepTemp = false;

    @Option(value = "keep-output", standard = false,
      description = "copy all hanitu output file to current directory")
    public boolean keepOutput;

    @Option(value = "eval-world", standard = false,
      description = "eval program flow as called by hanitu_world")
    public boolean evalWorld = false;

    @Option(shortName = 'd', value = "daemon", standard = false,
      description = "keep flysim as daemon instead of killing it when closing the hanitu")
    public boolean daemonFlysim;

    @Argument(index = 0, value = "FILE", description = "args.world")
    public String worldConfigFileName = HanituFile.WORLD_CONFIG_DEFAULT_FILENAME;
    private Path worldConfigDir;
    private WorldConfig worldConfig;
    //
    private FlysimServer processFlysim;
    private Hanitu processHanitu;
    private TailStream<LocationData> locationStream;

    @Override
    public String getName(){
        return Base.loadProperties().getProperty("name");
    }

    @Override
    public String getDescription(){
        return Base.loadProperties().getProperty("desp");
    }

    @Override
    public Properties getProperties(){
        return Base.loadProperties();
    }

    public WorldConfig setupWorldConfig() throws IOException{
        if (worldConfig == null){
            Path p = Paths.get(worldConfigFileName);
            if (!Files.isRegularFile(p)){
                return null;
            }
            worldConfigDir = p.toAbsolutePath().getParent();
            WorldConfigLoader loader = new WorldConfigLoader();
            loader.setFailOnLoad(true);
            worldConfig = loader.load(p);
        }
        return worldConfig;
    }

    public void setupWorldConfig(Path workingDir, WorldConfig wc){
        worldConfigDir = Objects.requireNonNull(workingDir, "working directory").toAbsolutePath();
        worldConfig = wc;
        worldConfigFileName = null;
    }

    public void setupWorldConfig(WorldConfig wc){
        worldConfigDir = Paths.get(System.getProperty("user.dir"));
        worldConfig = wc;
        worldConfigFileName = null;
    }

    private static void backup(Path file) throws IOException{
        if (Files.exists(file)){
            Path dir = file.toAbsolutePath().getParent();
            Path targetFile;
            String filename = file.getFileName().toString();
            int i = 0;
            //noinspection StatementWithEmptyBody
            while (Files.exists(targetFile = dir.resolve(filename + "." + (i++)))) ;
            LOG.debug("backup {} -> {}", file, targetFile);
            Files.move(file, targetFile);
        }
    }

    public Hanitu setupHanitu() throws IOException{
        if (processHanitu == null){
            if (setupWorldConfig() == null){
                throw new RuntimeException("world config file path not set or file not found : "
                                           + worldConfigFileName);
            }
            locationFile = Objects.requireNonNull(locationFile, "location file path").toAbsolutePath();
            spikeFile = Objects.requireNonNull(spikeFile, "spike file path").toAbsolutePath();
            processHanitu = new Hanitu();
            processHanitu.setKeepingTempDirectory(true); // keep Event.dat file
            if (worldConfigFileName != null && !evalWorld){
                processHanitu.worldConfigPath = Paths.get(worldConfigFileName);
            } else if (worldConfig != null || evalWorld){
                processHanitu.worldConfigPath = writeTempWorldConfig();
            }
            processHanitu.locationOutputPath = locationFile;
            processHanitu.spikeOutputPath = spikeFile;
            processHanitu.timeout = timeout;
            processHanitu.locTimeStep = locTimeStep;
            processHanitu.port = port;
            if (backup){
                backup(locationFile);
                backup(spikeFile);
            } else {
                LOG.debug("delete {}", locationFile);
                LOG.debug("delete {}", spikeFile);
                Files.deleteIfExists(locationFile);
                Files.deleteIfExists(spikeFile);
            }
            processHanitu.setupProcessBuilder();
            //
            processHanitu.start();
        }
        return processHanitu;
    }

    private Path writeTempWorldConfig() throws IOException{
        Path tempDir = processHanitu.setupWorkingDirectory();
        Path temp = tempDir.resolve(HanituFile.WORLD_CONFIG_DEFAULT_FILENAME);
        LOG.debug("write world config to : {}", tempDir);
        WorldConfigPrinter printer = new WorldConfigPrinter();
        printer.write(temp, worldConfig);
        ErrorFunction.forEach(worldConfig.worms().stream()
                                .map(w -> w.circuitFileName.get())
                                .distinct(), c -> {
            Path s = worldConfigDir.resolve(c);
            Path t = tempDir.resolve(s.getFileName());
            LOG.debug("translate : {} -> {}", s, t);
            Files.copy(s, t);
        });
        return temp;
    }

    private void copyAllHanituOutputFileToCurrentDirectory() throws IOException{
        Path tempDir = processHanitu.setupWorkingDirectory();
        Path currentDir = Paths.get(System.getProperty("user.dir"));
        ErrorFunction.forEach(Files.list(tempDir), source -> {
            Path target = currentDir.resolve(source.getFileName());
            LOG.debug("move {} -> {}", source, target);
            Files.move(source, target, StandardCopyOption.REPLACE_EXISTING);
        });
    }

    public FlysimServer setupFlysim() throws IOException{
        if (!Flysim.LOCALHOST.equals(address)){
            daemonFlysim = true;
        }
        if (checkFlysimServerResponse()){
            LOG.debug("Flysim Server {}:{} response", address, port);
            return null;
        }
        LOG.debug("Flysim Server {}:{} no response", address, port);
        if (daemonFlysim){
            if (!Flysim.LOCALHOST.equals(address)){
                throw new IOException("Flysim server " + address + ":" + port + " no response");
            }
        }
        // only localhost can start Flysim
        if (processFlysim == null){
            address = Flysim.LOCALHOST;
            LOG.debug("create local flysim server");
            while (true){
                processFlysim = new FlysimServer(daemonFlysim);
                processFlysim.setKeepingTempDirectory(keepTemp);
                processFlysim.port = port;
                processFlysim.setupProcessBuilder();
                processFlysim.start();
                //noinspection EmptyCatchBlock
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e){
                }
                if (checkFlysimServerResponse()){
                    break;
                } else if (disablePortRouting){
                    throw new IOException("start Flysim server fail");
                }
                try {
                    processFlysim.close();
                    processFlysim = null;
                } catch (IOException e){
                    LOG.warn("flysim create error", e);
                }
                port++;
                LOG.debug("Flysim Server change port : {}", port);
            }
        }
        return processFlysim;
    }

    private boolean checkFlysimServerResponse(){
        Flysim fy = new Flysim();
        fy.address = address;
        fy.port = port;
        fy.response = true;
        return fy.checkFlysimResponse();
    }

    public TailStream<LocationData> setupLocationStream(){
        LOG.debug("follow location file : {}", locationFile);
        return TailStream.follow(locationFile).mapIgnoreError(LocationData::parseLine);
    }

    public Path getEventFilePath(){
        return processHanitu.setupWorkingDirectory().resolve(HanituFile.EVENT_DEFAULT_FILENAME);
    }

    public TailStream<EventData> setupEventStream(){
        Path eventFile = getEventFilePath();
        LOG.debug("follow event file : {}", eventFile);
        return TailStream.follow(eventFile).mapIgnoreError(EventData::parseLine);
    }

    @Override
    public void start() throws IOException{
        setupWorldConfig();
        setupFlysim();
        setupHanitu();
        try {
            if (quiet){
                LOG.trace("start in quiet mode");
                while (isNativeProcessAlive()){
                    //noinspection EmptyCatchBlock
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e){
                    }
                }
            } else if (noPlot){
                LOG.trace("start in no plot mode");
                startInNoPlotMode();
            } else {
                LOG.trace("start in plot mode");
                startInPlotMode();
            }
        } catch (IOException e){
            throw new RuntimeException(e);
        } finally {
            LOG.trace("closing");
            close();
        }
    }

    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    private void startInNoPlotMode(){
        TailStream<LocationData> stream = setupLocationStream();
        while (true){
            if (stream.hasNext()){
                System.out.println(stream.next());
            } else if (!isNativeProcessAlive()){
                break;
            }
        }
    }

    private void startInPlotMode() throws IOException{
        WorldViewer frame = FXApplication.launch(WorldViewer.class);
        //
        WorldViewerModulePlot plot = frame.module.add(new WorldViewerModulePlot());
        plot.module.add(new WorldPlotModuleInformation());
        plot.module.add(new WorldPlotModuleMessage());
        WorldPlotModuleChart chart = plot.module.add(new WorldPlotModuleChart());
        WorldPlotModuleLab lab = plot.module.add(new WorldPlotModuleLab());
        //
        frame.setCanvas(WorldCanvasProvider.getCanvas(style));
        frame.setWorld(setupWorldConfig());
        frame.setEditable(false);
        //
        plot.setLocationStream(setupLocationStream());
        plot.setEventStream(setupEventStream());
        //
        chart.setSpikeFilePath(spikeFile);
        //
        lab.setSpikeFilePath(spikeFile);
        lab.setEventFilePath(getEventFilePath());
        //
        try {
            while (!frame.isClose()){
                if (!isNativeProcessAlive()){
                    closeNative();
                }
                //noinspection EmptyCatchBlock
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e){
                }
            }
        } finally {
            closeNative();
            closeStream();
            frame.close();
        }
    }

    private boolean isNativeProcessAlive(){
        return processHanitu != null && processHanitu.isAlive();
    }

    public void closeNative(){
        if (processHanitu != null){
            if (keepOutput){
                try {
                    copyAllHanituOutputFileToCurrentDirectory();
                } catch (IOException e){
                    LOG.warn("copy file fail", e);
                }
            }
            Hanitu ps = processHanitu;
            processHanitu = null;
            //noinspection EmptyCatchBlock
            try {
                ps.close();
            } catch (IOException e){
            }
        }
        if (!daemonFlysim && processFlysim != null){
            FlysimServer ps = processFlysim;
            processFlysim = null;
            //noinspection EmptyCatchBlock
            try {
                ps.close();
            } catch (IOException e){
            }
        }
    }

    public void closeStream(){
        if (locationStream != null){
            TailStream<LocationData> ll = locationStream;
            locationStream = null;
            //noinspection EmptyCatchBlock
            try {
                ll.close();
            } catch (Exception e){
            }
        }
    }

    public void close(){
        closeNative();
        closeStream();
    }
}
