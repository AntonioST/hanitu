/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.builder;

import java.io.IOException;
import java.util.List;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.app.Circuit;
import cclo.hanitu.app.Run;
import cclo.hanitu.exe.ExecutableData;
import cclo.hanitu.gui.MessageSession;
import cclo.hanitu.world.WormConfig;

/**
 * @author antonio
 */
public class WorldConfigModuleRun extends AbstractWorldConfigModule{

    private final Logger LOG = LoggerFactory.getLogger(WorldConfigModuleRun.class);

    private MenuItem menuItemRunHanitu;

    private List<String> runOptions;
    private boolean enableRunHanitu = true;

    @Override
    public String getName(){
        return "run";
    }

    @Override
    public void setupModule(WorldConfigViewer world){
        world.menu.addMenu("menu.run");
        menuItemRunHanitu = world.menu.getById("menu.run.hanitu");
        setEnableRunHanitu(enableRunHanitu);
    }

    @Override
    public void destroy(){
        host.menu.removeMenu("menu.run");
        super.destroy();
    }

    @Override
    public boolean processMenuEvent(ActionEvent e, String action){
        switch (action){
        case "menu.run.circuit":
            openEmptyCircuit();
            break;
        case "menu.run.hanitu":
            runHanitu();
            break;
        default:
            return false;
        }
        return true;
    }

    public void setEnableRunHanitu(boolean enable){
        enableRunHanitu = enable;
        if (host != null){
            menuItemRunHanitu.setDisable(!enable);
        }
    }

    public void setRunHanituOptions(List<String> options){
        runOptions = options;
    }

    private void runHanitu(){
        if (enableRunHanitu){
            if (!checkWorldConfigRunnable()) return;
            Run run = new Run();
            if (runOptions != null){
                ExecutableData.set(run, runOptions);
            }
            run.setupWorldConfig(host.file.getCurrentWorkDirectory(), host.config);
            new Thread(() -> {
                try {
                    run.start();
                } catch (Throwable ex){
                    Platform.runLater(() -> MessageSession.showErrorMessageDialog(ex));
                }
            }, "Run").start();
        }
    }

    private boolean checkWorldConfigRunnable(){
        for (WormConfig worm : host.config.worms()){
            WorldConfigTabWorm tab = host.getWorm(worm).get();
            if (tab.isCircuitFileExist()){
                continue;
            }
            tab.select();
            tab.checkCircuitFileExist(null, false);
            return false;
        }
        return true;
    }

    private void openEmptyCircuit(){
        try {
            Circuit circuit = new Circuit();
            circuit.start();
        } catch (IOException e){
            LOG.warn("open empty circuit fail", e);
        }
    }
}
