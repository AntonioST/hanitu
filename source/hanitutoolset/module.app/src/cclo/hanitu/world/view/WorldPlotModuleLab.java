/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Menu;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import cclo.hanitu.circuit.view.LabCircuitMode;
import cclo.hanitu.circuit.view.VisualCircuit;
import cclo.hanitu.data.FiringrateFunction;
import cclo.hanitu.data.SingleSpikeFilter;
import cclo.hanitu.data.WormData;
import cclo.hanitu.gui.FXApplication;
import cclo.hanitu.gui.MenuSession;
import cclo.hanitu.gui.MessageSession;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.WormConfig;

/**
 * @author antonio
 */
public class WorldPlotModuleLab extends AbstractWorldPlotModule{

    private Map<SingleSpikeFilter, Path> wormMapCircuitFile;
    private Map<SingleSpikeFilter, VisualCircuit> wormMapCircuitViewer;
    //
    private Path spikeFilePath;
    private Path eventFilePath;
    private LabCircuitMode.LabDataSource source;

    @Override
    public String getName(){
        return "lab";
    }

    @Override
    protected void setupModule(WorldViewerModulePlot host){
        host.getWorld().menu.getMenuSession().addTopMenu("menu.lab", "Lab");
        wormMapCircuitFile = new HashMap<>();
        wormMapCircuitViewer = new HashMap<>();
    }

    @Override
    public void destroy(){
        wormMapCircuitViewer.values().forEach(VisualCircuit::close);
        wormMapCircuitViewer.clear();
        wormMapCircuitFile.clear();
        super.destroy();
    }

    @Override
    public void processSetWorldConfig(WorldConfig world){
        Menu menu = host.getWorld().menu.getMenuSession().getTopMenu("menu.lab");
        menu.getItems().clear();
        if (world.file != null){
            for (WormConfig worm : world.worms()){
                SingleSpikeFilter filter = new SingleSpikeFilter(worm.user.get(), worm.worm.get());
                Path parent = world.file.getParent();
                Path path;
                if (parent == null){
                    path = Paths.get(worm.circuitFileName.get());
                } else {
                    path = parent.resolve(worm.circuitFileName.get());
                }
                wormMapCircuitFile.put(filter, path);
                menu.getItems().add(MenuSession.createMenuItem(filter.toString(), e -> processMenuEvent(e, filter)));
            }
        } else {
            MessageSession.showErrorMessageDialog("World Config Lost",
                                                  "World Config File not found.",
                                                  "Make sure save");
        }
    }

    @Override
    public void processMouseClicked(MouseEvent _e, WormData worm){
        SingleSpikeFilter filter = new SingleSpikeFilter(worm.user, worm.worm);
        wormMapCircuitViewer.entrySet().stream()
          .filter(v -> v.getValue().isClose())
          .map(Map.Entry::getKey)
          .collect(Collectors.toList())
          .forEach(f -> wormMapCircuitViewer.remove(f));
        for (Map.Entry<SingleSpikeFilter, VisualCircuit> e : wormMapCircuitViewer.entrySet()){
            if (e.getKey().isSameWorm(filter)){
                Platform.runLater(() -> {
                    e.getValue().primaryStage.toFront();
                    host.getWorld().primaryStage.toFront();
                });
                break;
            }
        }
    }

    @SuppressWarnings("unused")
    public FiringrateFunction getFiringrateFunction(){
        return source.getFiringrateFunction();
    }

    @SuppressWarnings("unused")
    public void setFiringrateFunction(FiringrateFunction function){
        source.setFiringrateFunction(function);
    }

    public void setSpikeFilePath(Path spikeFilePath){
        this.spikeFilePath = spikeFilePath;
    }

    public void setEventFilePath(Path eventFilePath){
        this.eventFilePath = eventFilePath;
    }

    private void processMenuEvent(ActionEvent e, SingleSpikeFilter filter){
        VisualCircuit circuit = wormMapCircuitViewer.computeIfAbsent(filter, this::initLabCircuit);
        if (circuit.isClose()){
            circuit = wormMapCircuitViewer.put(filter, initLabCircuit(filter));
        }
        circuit.primaryStage.show();
    }

    private LabCircuitMode.LabDataSource initLabSource(){
        if (source == null){
            source = new LabCircuitMode.LabDataSource();
            if (spikeFilePath != null) source.setSpikeFile(spikeFilePath);
            if (eventFilePath != null) source.setEventFile(eventFilePath);
            host.addUpdateListener(source);
        }
        return source;
    }

    private VisualCircuit initLabCircuit(SingleSpikeFilter filter){
        try {
            VisualCircuit circuit = FXApplication.launch(VisualCircuit.class);
            Stage stage = host.getWorld().primaryStage;
            circuit.primaryStage.setX(stage.getX() + stage.getWidth());
            circuit.primaryStage.setY(stage.getY());
            stage.toFront();
            //
            LabCircuitMode mode = new LabCircuitMode(filter);
            circuit.setMode(mode);
            circuit.setCircuit(wormMapCircuitFile.get(filter), true);
            initLabSource().addUpdateListener(mode);
            return circuit;
        } catch (IOException e){
            MessageSession.showErrorMessageDialog("load Circuit File", e);
            return null;
        }
    }

}
