/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.scene.control.Menu;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.circuit.view.LabCircuitMode;
import cclo.hanitu.circuit.view.VisualCircuit;
import cclo.hanitu.data.*;
import cclo.hanitu.gui.FXApplication;
import cclo.hanitu.gui.MenuSession;
import cclo.hanitu.world.MoleculeConfig;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.WormConfig;

/**
 * @author antonio
 */
public class WorldViewerModuleStatic extends AbstractWorldViewerModule{

    private final Logger LOG = LoggerFactory.getLogger(WorldViewerModuleStatic.class);
    //
    private final String userID = "static";
    private final String wormID = "0";
    private final int boundary = 60;
    //
    private WormData worm;
    //
    private VisualCircuit viewer;
    private LabCircuitMode mode;
    private Timeline timeline;

    @Override
    public String getName(){
        return "static";
    }

    @Override
    protected void setupModule(WorldViewer host){
        host.setEditable(false);
        //
        host.menu.getMenuSession().addTopMenu("menu.lab", "Lab");
        Menu menu = host.menu.getMenuSession().getTopMenu("menu.lab");
        menu.getItems().clear();
        menu.getItems().add(MenuSession.createMenuItem("show circuit", this::processMenuEvent));
        //
        timeline = new Timeline(new KeyFrame(new Duration(50), event -> {
            if (worm != null){
                host.repaintCanvas();
            }
            if (mode != null){
                mode.repaintCanvas();
                mode.resetEvent();
            }
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
        //
        host.setWorld(createStaticWorldConfig());
        host.repaintCanvas();
    }

    @Override
    public void destroy(){
        if (viewer != null){
            timeline.stop();
            timeline = null;
            viewer.close();
        }
    }

    @Override
    public void processSetWorldConfig(WorldConfig data){
        worm = host.getWorm(userID, wormID);
        if (worm == null){
            worm = host.getWorms().get(0);
        }
    }

    private WorldConfig createStaticWorldConfig(){
        WorldConfig world = new WorldConfig();
        world.boundary.set(boundary);
        world.addWorm(new WormConfig(userID, wormID, null){
            {
                x.set(-40);
            }
        });
        world.addMolecule(new MoleculeConfig(MoleculeType.FOOD, "1"));
        world.addMolecule(new MoleculeConfig(MoleculeType.TOXICANT, "2"){
            {
                x.set(10);
                y.set(10);
            }
        });
        return world;
    }

    private VisualCircuit initLabCircuit(){
        VisualCircuit circuit = FXApplication.launch(VisualCircuit.class);
        Stage stage = host.primaryStage;
        circuit.primaryStage.setX(stage.getX() + stage.getWidth());
        circuit.primaryStage.setY(stage.getY());
        //
        mode = new LabCircuitMode();
        circuit.setMode(mode);
        //
        host.primaryStage.toFront();
        return circuit;
    }

    @SuppressWarnings("UnusedParameters")
    private void processMenuEvent(ActionEvent e){
        if (viewer != null && viewer.isClose()) viewer = null;
        if (viewer == null){
            viewer = initLabCircuit();
        }
    }

    @Override
    public void processMouseDragging(MouseEvent e, WormData worm, PositionData position){
        double dx = position.x - worm.x;
        double dy = position.y - worm.y;
        //
        if (mode != null){
            if (dx > 0) move(Direction.RIGHTWARD, dx);
            else if (dx < 0) move(Direction.LEFTWARD, -dx);
            if (dy > 0) move(Direction.FORWARD, dy);
            else if (dy < 0) move(Direction.BACKWARD, -dy);
        } else {
            worm.setLocation(position);
        }
        host.repaintCanvas();
    }

    @Override
    public void processMouseDragging(MouseEvent e, MoleculeData molecule, PositionData position){
        molecule.setLocation(position);
    }

    @Override
    public boolean processKeyboardEvent(KeyEvent e){
        if (worm == null) return false;
        switch (e.getCode()){
        case PAGE_UP:
            worm.changeHp(10);
            if (mode != null){
                mode.updateWormHp(worm.hp);
            }
            LOG.debug("hp +{} -> {}", 10, worm.hp);
            break;
        case PAGE_DOWN:
            worm.changeHp(-10);
            if (mode != null){
                mode.updateWormHp(worm.hp);
            }
            LOG.debug("hp -{} -> {}", 10, worm.hp);
            break;
        case UP:
            move(Direction.FORWARD, 10);
            break;
        case DOWN:
            move(Direction.BACKWARD, 10);
            break;
        case LEFT:
            move(Direction.LEFTWARD, 10);
            break;
        case RIGHT:
            move(Direction.RIGHTWARD, 10);
            break;
        default:
            return false;
        }
        host.repaintCanvas();
        return true;
    }

    private void move(Direction direction, double mount){
        boolean block = false;
        switch (direction){
        case FORWARD:
            worm.y += mount;
            if (worm.y > boundary){
                worm.y = boundary;
                block = true;
            }
            break;
        case BACKWARD:
            worm.y -= mount;
            if (worm.y < -boundary){
                worm.y = -boundary;
                block = true;
            }
            break;
        case LEFTWARD:
            worm.x -= mount;
            if (worm.x < -boundary){
                worm.x = -boundary;
                block = true;
            }
            break;
        case RIGHTWARD:
            worm.x += mount;
            if (worm.x > boundary){
                worm.x = boundary;
                block = true;
            }
            break;
        case SILENCE:
        default:
            return;
        }
        LOG.trace("move {} {}", direction, mount);
        if (mode != null){
            mode.updateEvent(new EventData.MoveEvent(0, userID, wormID, direction));
            if (block){
                mode.updateEvent(new EventData.WormTouchWallEvent(0, userID, wormID, direction));
                LOG.trace("block {}", direction);
            }
            host.getMolecule().stream()
              .filter(m -> worm.contact(m))
              .map(m -> new EventData.MoleculeEvent(0, userID, wormID, m.type, m.ID, 0))
              .peek(e -> LOG.debug("eat {}", e.getMoleculeType()))
              .forEach(mode::updateEvent);
            mode.repaintCanvas();
        }
    }
}
