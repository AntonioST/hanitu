/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.view;

import java.io.File;
import java.nio.file.Path;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import cclo.hanitu.chart.FiringrateChartViewer;
import cclo.hanitu.data.SingleSpikeFilter;
import cclo.hanitu.data.WormData;
import cclo.hanitu.gui.FXApplication;

/**
 * @author antonio
 */
public class WorldPlotModuleChart extends AbstractWorldPlotModule{

    private FiringrateChartViewer viewer;
    private Path spikeFilePath;

    @Override
    public String getName(){
        return "chart";
    }

    @Override
    protected void setupModule(WorldViewerModulePlot host){
        host.getWorld().menu.addMenu("menu.chart");
    }

    @Override
    public void destroy(){
        host.getWorld().menu.removeMenu("menu.chart");
        destroyChart();
        super.destroy();
    }

    public void setSpikeFilePath(Path spikeFilePath){
        this.spikeFilePath = spikeFilePath;
    }

    @Override
    public boolean processMenuEvent(ActionEvent e, String action){
        switch (action){
        case "menu.chart.open":
            if (viewer != null && !viewer.isClose()){
                viewer.primaryStage.show();
            } else {
                if (viewer != null) destroyChart();
                if (spikeFilePath == null){
                    askSpikeFile();
                }
                if (spikeFilePath != null){
                    Platform.runLater(() -> initChart(spikeFilePath));
                }
            }
            break;
        default:
            return false;
        }
        return true;
    }

    @Override
    public void processMouseClicked(MouseEvent e, WormData worm){
        if (viewer != null){
            viewer.select(new SingleSpikeFilter(worm.user, worm.worm));
        }
    }

    private void initChart(Path spikeFilePath){
        viewer = FXApplication.launch(FiringrateChartViewer.class);
        Stage stage = host.getWorld().primaryStage;
        viewer.primaryStage.setX(stage.getX() + stage.getWidth());
        viewer.primaryStage.setY(stage.getY());
        stage.toFront();
        viewer.setSpikeFile(spikeFilePath);
        host.addUpdateListener(viewer);
    }

    private void destroyChart(){
        if (viewer != null){
            viewer.close();
            viewer = null;
        }
    }

    private void askSpikeFile(){
        FileChooser jfc = new FileChooser();
        File file = jfc.showOpenDialog(host.getWorld().primaryStage);
        if (file != null){
            spikeFilePath = file.getAbsoluteFile().toPath();
        }
    }
}
