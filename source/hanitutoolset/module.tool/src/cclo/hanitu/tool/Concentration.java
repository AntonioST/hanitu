/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.tool;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import cclo.hanitu.Base;
import cclo.hanitu.exe.Argument;
import cclo.hanitu.exe.Executable;
import cclo.hanitu.exe.Option;

import static cclo.hanitu.exe.HelpBuilder.*;

/**
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class Concentration extends Executable{

    @Option(shortName = 'd', value = "diffusion", arg = "VALUE", description = "option.diffusion")
    public double diffusion = 1.86e-5;

    @Option(shortName = 'p', value = "depth", arg = "VALUE", description = "option.depth")
    public double depth = 0.264;

    @Option(shortName = 't', value = "delaytime", arg = "TIME", description = "option.delaytime")
    public double delayTime = 0;

    @Option(shortName = 'c', value = "concentration", arg = "VALUE", description = "option.concentration")
    public double concentration = 100;

    @Option(shortName = 'C', value = "count", arg = "VALUE", description = "option.count")
    public int count = 1;

    @Option(value = "start", arg = "VALUE", order = 0, description = "option.start")
    public double start = 0.0;

    @Option(value = "end", arg = "VALUE", order = 0, description = "option.end")
    public double end = 200.0;

    @Option(value = "inc", arg = "VALUE", order = 0, description = "option.inc")
    public double inc = 1.0;

    @Option(value = "no-header", order = 1, description = "option.noheader")
    public boolean headerInformation = true;

    @Argument(index = 0, value = "FILE", description = "args.file")
    public String inputFileName = null;
    //
    public Distribution distribution;

    @Override
    public String getName(){
        return Base.loadProperties().getProperty("name");
    }

    @Override
    public String getDescription(){
        return Base.loadProperties().getProperty("desp");
    }

    @Override
    public Properties getProperties(){
        return Base.loadProperties();
    }

    @Override
    public String extendHelpDoc(){
        return
          DOC_TITLE + "FILE Format" +
          DOC_BLOCK_START +
          "If ~_{FILE~} is given, program will ignore all the setting in command line." +
          DOC_NEWLINE +
          "every setting should be match this pattern KEY=VALUE. you can add any command which " +
          "start with //. Otherwise, program will throw error when parsing file." +
          DOC_NEWLINE +
          "the list of KEY:" +
          DOC_LIST_0 + "Diffusion" +
          DOC_LIST_0 + "Depth" +
          DOC_LIST_0 + "Delaytime" +
          DOC_LIST_0 + "Concentration" +
          DOC_LIST_0 + "Count" +
          DOC_LIST_0 + "Time" +
          DOC_BLOCK_END;
    }

    public Distribution setupDistribution(){
        if (distribution == null){
            distribution = new Distribution();
            if (inputFileName != null){
                setByFile(distribution, inputFileName);
            } else {
                distribution.diffusion = diffusion;
                distribution.depth = depth;
                distribution.delayTime = delayTime;
                distribution.concentration = concentration;
                distribution.count = count;
            }
        }
        return distribution;
    }

    @Override
    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public void start() throws IOException{
        if (start > end){
            throw new IllegalArgumentException(String.format("error distance range :[%.4f, %.4f]\n", start, end));
        } else if (inc <= 0){
            throw new IllegalArgumentException(String.format("error distance increasing :%.4f\n", inc));
        }
        Distribution d = setupDistribution();
        if (headerInformation){
            System.out.println("# Distant (mm)\tConcentration (mM)");
        }
        for (double distance = start; distance <= end; distance += inc){
            System.out.printf("%.1f\t%.5g\n", distance, d.getConcentration(distance));
        }
    }

    private static void setByFile(Distribution d, String fileName){
        int lineNum = 0;
        try (BufferedReader r = Files.newBufferedReader(Paths.get(fileName))) {
            String line;
            while ((line = r.readLine()) != null){
                lineNum++;
                line = line.replaceFirst("//.*$", "").trim();
                if (line.isEmpty()) continue;
                if (line.startsWith("Diffusion")){
                    d.diffusion = Double.parseDouble(line.substring(line.indexOf("=") + 1));
                } else if (line.startsWith("Depth")){
                    d.depth = Double.parseDouble(line.substring(line.indexOf("=") + 1));
                } else if (line.startsWith("Delaytime")){
                    d.delayTime = Double.parseDouble(line.substring(line.indexOf("=") + 1));
                } else if (line.startsWith("Concentration")){
                    d.concentration = Double.parseDouble(line.substring(line.indexOf("=") + 1));
                } else if (line.startsWith("Count")){
                    d.count = Integer.parseInt(line.substring(line.indexOf("=") + 1));
                } else if (line.startsWith("Time")){
                    d.delayTime += Double.parseDouble(line.substring(line.indexOf("=") + 1));
                } else {
                    throw new RuntimeException("unknown line " + "@" + fileName + ":" + lineNum);
                }
            }
        } catch (RuntimeException ex){
            throw ex;
        } catch (Throwable ex){
            throw new RuntimeException(ex.getMessage() + ", at " + fileName + ":" + lineNum, ex);
        }
    }
}
