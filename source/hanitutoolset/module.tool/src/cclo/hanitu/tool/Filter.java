/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.tool;

import java.io.IOException;
import java.util.ListIterator;
import java.util.Properties;
import java.util.stream.Stream;

import cclo.hanitu.Base;
import cclo.hanitu.data.SpikeData;
import cclo.hanitu.data.SpikeFilter;
import cclo.hanitu.exe.Argument;
import cclo.hanitu.exe.Executable;
import cclo.hanitu.exe.Option;
import cclo.hanitu.io.FileStream;

import static cclo.hanitu.exe.HelpBuilder.*;

/**
 * @author antonio
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class Filter extends Executable{

    @Option(value = "time", arg = "START[:END]", order = 1, description = "option.time")
    public String time;

    @Argument(index = 0, value = "FILE", description = "args.file")
    public String inputFilePath = null;
    //
    private SpikeFilter filter;
    //
    private boolean inverse = false;
    private boolean inverseAll = false;

    @Override
    public String getName(){
        return Base.loadProperties().getProperty("name");
    }

    @Override
    public String getDescription(){
        return Base.loadProperties().getProperty("desp");
    }

    @Override
    public Properties getProperties(){
        return Base.loadProperties();
    }

    @Override
    @Option(shortName = 'u', value = "user", arg = "ID", description = "option.user")
    @Option(shortName = 'w', value = "worm", arg = "ID", description = "option.worm")
    @Option(shortName = 'n', value = "neuron", arg = "ID", description = "option.neuron")
    @Option(shortName = 't', value = "type", arg = "ID", description = "option.type")
    @Option(shortName = 'i', description = "option.inverse", order = 0)
    @Option(shortName = 'I', description = "option.inverseall", order = 0)
    public boolean extendOption(String key, String value, ListIterator<String> it){
        SpikeFilter sf;
        switch (key){
        case "u":
        case "user":
            sf = SpikeFilter.testUser(value, inverseAll || inverse);
            break;
        case "w":
        case "worm":
            sf = SpikeFilter.testWorm(value, inverseAll || inverse);
            break;
        case "n":
        case "neuron":
            sf = SpikeFilter.testNeuron(value, inverseAll || inverse);
            break;
        case "t":
        case "type":
            sf = SpikeFilter.testType(value, inverseAll || inverse);
            break;
        case "i":
            inverse = true;
            return true;
        case "I":
            inverseAll = true;
            return true;
        default:
            return false;
        }
        inverse = false;
        if (filter == null){
            filter = sf;
        } else {
            filter = filter.and(sf);
        }
        return true;
    }

    @Override
    public String extendHelpDoc(){
        return
          DOC_TITLE + "ID filter format" +
          DOC_BLOCK_START +
          "For single id filter, you can only specific use a integer value, such as '-w 0', and this " +
          "mean you want to get all of the spike data of the user 0. However, you can filter id with " +
          "a range with format 'START:END'. For example '-w 0:4', it mean you want to get all of the " +
          "spike data of user from 0 to 4 (include). Finally, you also can choose a list of id with " +
          "format 'ID,ID,...' (use ',' spread each other without and space). For example '-w 0,2,4', " +
          "it mean you want to get all of the spike data of user 0, 2 and 4." +
          DOC_BLOCK_END +
          DOC_NEWLINE +
          DOC_TITLE + "TYPE filter format" +
          DOC_BLOCK_START +
          "There three kinds of neurons and each one has one character to present. b for brain circuit " +
          "neuron. s for sensor neuron. m for motor neuron. You can use '-t sm' to get all of the sensor " +
          "and motor neurons' spike data." +
          DOC_BLOCK_END +
          DOC_NEWLINE +
          DOC_TITLE + "Spike File format" +
          DOC_BLOCK_START +
          "In program arguments, using '-' to reading data from standard input." +
          DOC_NEWLINE +
          "For common hanitu spike output file, it is Nx4 data." +
          DOC_NEWLINE +
          "First column is time in virtual time (simulation time). Second column is user id. Third column " +
          "is worm id. Forth column is neuron id. Fifth is optional column and mean neuron type." +
          DOC_NEWLINE +
          "so it can present as : time user worm neuron type." +
          DOC_NEWLINE +
          "for other spike format, program also detect other spike file including:" +
          DOC_LIST_0 + "time neuron" +
          DOC_LIST_0 + "time" +
          DOC_BLOCK_END;
    }

    public Stream<SpikeData> setupStream() throws IOException{
        return FileStream.loadCommandLineFile(inputFilePath).map(SpikeData::parseLine);
    }

    public Stream<SpikeData> filterStream(Stream<SpikeData> source){
        if (filter == null) return source;
        return source.filter(filter);
    }

    public Stream<SpikeData> filterTime(Stream<SpikeData> source){
        if (time == null) return source;
        if (!time.contains(":")){
            double start = Double.parseDouble(time);
            return source.filter(data -> data.time >= start);
        } else if (time.startsWith(":")){
            double limit = Double.parseDouble(time.substring(1));
            return source.filter(data -> data.time <= limit);
        } else {
            int index = time.indexOf(":");
            double start = Double.parseDouble(time.substring(0, index));
            double limit = Double.parseDouble(time.substring(index + 1));
            return source.filter(data -> data.time >= start && data.time <= limit);
        }
    }

    @Override
    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public void start() throws IOException{
        try (Stream<SpikeData> stream = setupStream()) {
            filterStream(filterTime(stream)).forEach(System.out::println);
        }
    }
}
