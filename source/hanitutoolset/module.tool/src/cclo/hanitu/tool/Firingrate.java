/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.tool;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import cclo.hanitu.Base;
import cclo.hanitu.data.FiringRateData;
import cclo.hanitu.data.FiringRateScalar;
import cclo.hanitu.data.FiringrateFunction;
import cclo.hanitu.data.SpikeData;
import cclo.hanitu.exe.Argument;
import cclo.hanitu.exe.Executable;
import cclo.hanitu.exe.Option;
import cclo.hanitu.io.TailStream;

import static cclo.hanitu.exe.HelpBuilder.*;

/**
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class Firingrate extends Executable{

    @Option(shortName = 'w', value = "window", arg = "VALUE", order = 0, description = "option.window")
    public int timeWindow = 10000;

    @Option(shortName = 's', value = "step", arg = "VALUE", order = 0, description = "option.step")
    public int timeStep = 1000;

    @Option(value = "test-scalar", standard = false, description = "option.testscalar")
    public boolean testScalar = false;

    @Argument(index = 0, value = "FILE", description = "args.file")
    public String inputFilePath = null;
    //
    public FiringrateFunction function;
    //
    public FiringRateScalar scalar;

    @Override
    public String getName(){
        return Base.loadProperties().getProperty("name");
    }

    @Override
    public String getDescription(){
        return Base.loadProperties().getProperty("desp");
    }

    @Override
    public Properties getProperties(){
        return Base.loadProperties();
    }

    @Override
    @Option(value = "scalar", arg = "ARGS", description = "option.scalar", order = 1)
    public boolean extendOption(String key, String value, ListIterator<String> it){
        switch (key){
        case "scalar":
            scalar = parseScalar(value, it);
            break;
        default:
            return false;
        }
        return true;
    }

    @Override
    public String extendHelpDoc(){
        return
          DOC_TITLE + "Scalar argument" +
          DOC_BLOCK_START +
          "type [argument]" +
          DOC_NEWLINE +
          "built-in scalar:" +
          DOC_LIST_0 + "uniform" +
          DOC_LIST_0 + "half" +
          DOC_LIST_0 + "exp [half-time-decay]" +
          DOC_LIST_0 + "weight [weight,...|file]" +
          DOC_LIST_0 + "normal [std]" +
          DOC_NEWLINE +
          "for example:" +
          DOC_NEWLINE +
          DOC_BLOCK_START +
          "create a uniform distribution weight" +
          DOC_LIST_0 + "--scalar=uniform" +
          DOC_LIST_0 + "--scalar=uniform[]" +
          DOC_LIST_0 + "--scalar uniform" +
          DOC_LIST_0 + "--scalar uniform[]" +
          DOC_NEWLINE +
          "create a normal distribution weight with std 100 (ms)." +
          DOC_LIST_0 + "--scalar=normal[100]" +
          DOC_LIST_0 + "--scalar normal[100]" +
          DOC_NEWLINE +
          "create a triangle-shaped weight scalar." +
          DOC_LIST_0 + "--scalar weight[0,1,0]" +
          DOC_BLOCK_END +
          DOC_NEWLINE +
          "custom scalar type:" +
          DOC_BLOCK_START +
          "the class should implement interface cclo.hanitu.tool.firingrate.FiringRateScalar" +
          DOC_BLOCK_END +
          DOC_NEWLINE +
          DOC_TITLE + "Scalar test" +
          DOC_BLOCK_START +
          "When using '--test-scalar', program will test the given scalar. output weight value " +
          "for x range [-<window>/2, <window>/2] with increasing <step>." +
          DOC_BLOCK_END;
    }

    public FiringrateFunction setupFunction(){
        if (function == null){
            function = new FiringrateFunction();
            function.setTimeWindow(timeWindow);
            function.setTimeStep(timeStep);
            if (scalar != null){
                function.setScalar(scalar);
            }
        }
        return function;
    }

    public TailStream<SpikeData> setupSpikeStream(){
        return TailStream.followCommandLineFile(inputFilePath)
          .map(SpikeData::parseLine, e -> LOG.warn("illegal line", e));
    }

    public TailStream<FiringRateData> testScalar(){
        Objects.requireNonNull(scalar, "scalar");
        Stream<FiringRateData> stream = IntStream.rangeClosed(0, timeWindow / timeStep)
          .map(i -> i - timeWindow / 2)
          .mapToObj(t -> new FiringRateData(t, scalar.rate(0, t, timeWindow)));
        return TailStream.of(stream);
    }

    @Override
    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public void start() throws IOException{
        TailStream<FiringRateData> stream;
        if (testScalar){
            if (scalar == null){
                exit(1, "need specific scalar");
            }
            stream = testScalar();
        } else {
            TailStream<SpikeData> source = setupSpikeStream();
            stream = setupFunction().apply(source).onClose(source::close);
        }
        while (stream.hasNext()){
            System.out.println(stream.next());
        }
    }

    private static FiringRateScalar parseScalar(String arg, ListIterator<String> it){
        String type;
        String args;
        if (arg.contains("[")){
            if (!arg.endsWith("]")){
                throw new IllegalArgumentException("scalar args lost ']' : " + arg);
            }
            int index = arg.indexOf("[");
            type = arg.substring(0, index);
            args = arg.substring(index + 1, arg.length() - 1);
        } else if (it.hasNext()){
            type = arg;
            String t = it.next();
            if (t.startsWith("[")){
                if (!t.endsWith("]")){
                    throw new IllegalArgumentException("scalar args lost ']' : " + t);
                }
                args = t.substring(1, t.length() - 1);
            } else {
                it.previous();
                args = "";
            }
        } else {
            type = arg;
            args = "";
        }
        try {
            return createScalarForString(type, args);
        } catch (RuntimeException throwable){
            throw throwable;
        } catch (Throwable throwable){
            throw new RuntimeException(throwable);
        }
    }

    private static FiringRateScalar createScalarForString(String type, String args) throws Throwable{
        switch (type){
        case "uniform":
            if (!args.isEmpty()){
                throw new IllegalArgumentException(args);
            }
            return FiringRateScalar.uniform();
        case "half":
            if (!args.isEmpty()){
                throw new IllegalArgumentException(args);
            }
            return FiringRateScalar.half();
        case "exp":
            if (args.isEmpty()){
                throw new IllegalArgumentException("empty args for scalar type : " + type);
            }
            return FiringRateScalar.expDecay(Double.parseDouble(args));
        case "normal":
            if (args.isEmpty()){
                throw new IllegalArgumentException("empty args for scalar type : " + type);
            }
            return FiringRateScalar.normal(Double.parseDouble(args));
        case "weight":
            if (args.isEmpty()){
                throw new IllegalArgumentException("empty args for scalar type : " + type);
            }
            if (args.contains(",")){
                return FiringRateScalar.weight(Stream.of(args.split(","))
                                                 .mapToDouble(Double::parseDouble)
                                                 .toArray());
            } else {
                return FiringRateScalar.weight(args);
            }
        default:
            Class<FiringRateScalar> c = (Class<FiringRateScalar>)Class.forName(type);
            Constructor<FiringRateScalar> g = c.getConstructor(String.class);
            return g.newInstance(args);
        }
    }
}
