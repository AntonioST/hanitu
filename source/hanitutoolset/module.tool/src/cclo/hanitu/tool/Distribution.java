/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.tool;

import java.io.Serializable;

import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.MoleculeType;
import cclo.hanitu.data.PositionData;

/**
 * @author antonio
 */
public class Distribution extends MoleculeData implements Serializable{

    /**
     * diffusion constant in unit cm * cm / src
     */
    public double diffusion = 1.86e-5;
    /**
     * in unit cm
     */
    public double depth = 0.264;
    /**
     * in unit sec
     */
    public double delayTime;
    /**
     * in unit mM
     */
    public double concentration = 100;
    /**
     *
     */
    public int count = 1;

    public Distribution(){
        super(MoleculeType.FOOD, "");
    }

    @SuppressWarnings("unused")
    public double getConcentration(PositionData data){
        if (count == 0) return 0;
        if (concentration == 0) return 0;
        double xx = data.x - x;
        double yy = data.y - y;
        return getConcentration(Math.sqrt(xx * xx + yy * yy));
    }

    @SuppressWarnings("unused")
    public double getConcentration(double x, double y){
        if (count == 0) return 0;
        if (concentration == 0) return 0;
        double xx = x - this.x;
        double yy = y - this.y;
        return getConcentration(Math.sqrt(xx * xx + yy * yy));
    }

    /**
     * @param distance in unit mm
     */
    public double getConcentration(double distance){
        if (count == 0) return 0;
        if (concentration == 0) return 0;
        double delay = delayTime == 0? 1e-5: delayTime;
        double N = 1e-3 * count * concentration;
        double r2 = distance * distance * 1e-4;
        return N / (4 * Math.PI * diffusion * depth)
               * Math.exp(-r2 / (4 * diffusion * delay))
               / delay;
    }

}
