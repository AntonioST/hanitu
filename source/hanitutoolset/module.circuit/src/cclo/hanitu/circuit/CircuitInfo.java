/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.util.List;
import java.util.OptionalInt;
import java.util.Set;

import cclo.hanitu.data.Direction;
import cclo.hanitu.data.SensorType;

import static cclo.hanitu.data.Direction.SILENCE;

/**
 * @author antonio
 */
public interface CircuitInfo{

    String SENSOR = "sensor.";
    String MOTOR = "motor.";

    String source();

    int neuronCount();

    Set<String> getNeuronIDSet();

    OptionalInt receptorCount(String neuronId);

    List<Synapse> getCommunicateSynapse();

    static String resolveSensorNeuronID(SensorType type, Direction dir){
        if (SensorType.isDirectSpecific(type) && dir == SILENCE){
            throw new IllegalArgumentException("sensor " + type + " lost direction");
        }
        if (type == SensorType.NPY) return SENSOR + "npy";
        return SENSOR + type.name().toLowerCase() + "." + (dir.ordinal() - 1);
    }

    static String resolveMotorNeuronID(Direction dir){
        if (dir == SILENCE){
            throw new IllegalArgumentException("motor lost direction");
        }
        return MOTOR + (dir.ordinal() - 1);
    }

    static boolean isSensorID(String neuronID){
        return neuronID.startsWith(SENSOR);
    }

    static boolean isMotorID(String neuronID){
        return neuronID.startsWith(MOTOR);
    }
}
