/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

import cclo.hanitu.data.SensorType;

/**
 * @author antonio
 */
public class Sensor extends BodyNeuron{

    /**
     * the type of the sensor.
     */
    public final SensorType type;

    public final SimpleIntegerProperty metaX = new SimpleIntegerProperty(this, "meta.x");
    public final SimpleIntegerProperty metaY = new SimpleIntegerProperty(this, "meta.y");
    public final SimpleDoubleProperty metaTheta = new SimpleDoubleProperty(this, "meta.theta");

    public Sensor(SensorType type){
        this.type = type;
    }

    @SuppressWarnings("unused")
    public Sensor(Sensor s){
        super(s);
        type = s.type;
    }

    @SuppressWarnings("unused")
    public Sensor(SensorType type, BodyNeuron reference){
        super(reference);
        this.type = type;
        if (reference != null && (reference instanceof Sensor)){
            Sensor s = (Sensor)reference;
            metaX.set(s.metaX.get());
            metaY.set(s.metaY.get());
            metaTheta.set(s.metaTheta.get());
        }
    }

    @Override
    public void set(BodyNeuron neuron){
        super.set(neuron);
        if (neuron instanceof Sensor){
            Sensor sensor = (Sensor)neuron;
            metaX.set(sensor.metaX.get());
            metaY.set(sensor.metaY.get());
            metaTheta.set(sensor.metaTheta.get());
        }
    }

    @Override
    public String toString(){
        return "Sensor[" + type.name() + "]";
    }
}
