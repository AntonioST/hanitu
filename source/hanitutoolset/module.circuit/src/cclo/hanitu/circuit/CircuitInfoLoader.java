/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.io.IOException;
import java.util.*;

import cclo.hanitu.data.Direction;
import cclo.hanitu.data.SensorType;
import cclo.hanitu.io.FileLoader;

import static cclo.hanitu.circuit.CircuitInfo.resolveMotorNeuronID;
import static cclo.hanitu.circuit.CircuitInfo.resolveSensorNeuronID;
import static cclo.hanitu.circuit.CircuitLoader.*;

/**
 * @author antonio
 */
@Deprecated
public class CircuitInfoLoader extends FileLoader<CircuitInfo>{

    private static final int STATE_NEURON = 2;
    private static final int STATE_RECEPTOR = 3;
    private static final int STATE_SYNAPSE = 4;
    private static final int STATE_COMMUNICATION = 5;
    private static final int STATE_INPUT = 6;
    private static final int STATE_NPY_INPUT = 7;
    private static final int STATE_OUTPUT = 8;

    private String source;
    private int totalNeuron;
    private Map<String, Integer> receptorCountTable;
    private List<Synapse> communication;

    private String currentNeuronId;
    private String postSynapseNeuronID;
    private String receptorID;
    private int type;
    private int direction;


    @Override
    public String getFileType(){
        return CircuitLoader.CIRCUIT_CONFIG_FILE_TYPE;
    }

    @Override
    public boolean isFileTypeSupport(String value){
        return CircuitLoader.CIRCUIT_CONFIG_FILE_TYPE.equals(value);
    }

    @Override
    public String getFileVersionExpression(){
        return ">=1.4";
    }

    @Override
    public void prevProcess() throws IOException{
        super.prevProcess();
        source = sourcePath.toString();
        receptorCountTable = new HashMap<>();
        communication = new ArrayList<>();
    }

    @Override
    public CircuitInfo postProcess() throws IOException{
        return new CircuitInfo(){

            @Override
            public String source(){
                return source;
            }

            @Override
            public int neuronCount(){
                return totalNeuron;
            }

            @Override
            public Set<String> getNeuronIDSet(){
                return Collections.unmodifiableSet(receptorCountTable.keySet());
            }

            @Override
            public OptionalInt receptorCount(String neuronId){
                return receptorCountTable.containsKey(neuronId)?
                       OptionalInt.of(receptorCountTable.get(neuronId)):
                       OptionalInt.empty();
            }

            @Override
            public List<Synapse> getCommunicateSynapse(){
                return communication;
            }
        };
    }

    @Override
    public void processLine(String line) throws IOException{
        switch (currentState){
        case STATE_INIT:
            init(line);
            break;
        case STATE_NEURON:
            neuron(line);
        case STATE_RECEPTOR:
            receptor(line);
            break;
        case STATE_SYNAPSE:
            synapse(line);
            break;
        case STATE_COMMUNICATION:
            communication(line);
            break;
        case STATE_INPUT:
            input(line);
            break;
        case STATE_OUTPUT:
            output(line);
            break;
        }
    }


    private void init(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        switch (getKey(line).toLowerCase()){
        case TOTAL_NEURON_NUMBER:
            totalNeuron = getIntValue(line);
            currentState = STATE_NEURON;
            break;
        default:
            addError("expect {}", CircuitPrinter.getHumanReadable(TOTAL_NEURON_NUMBER));
        }
    }

    private void neuron(String line) throws HanituLoadingException{
        switch (getKey(line).toLowerCase()){
        case NEURON_ID:
            currentNeuronId = getStrValue(line);
            break;
        case BLOCK_BEGIN_RECEPTOR:
            currentState = STATE_RECEPTOR;
            break;
        case TARGET_NEURON:
            currentState = STATE_SYNAPSE;
            break;
        case BLOCK_BEGIN_COMMUNICATION:
            currentState = STATE_COMMUNICATION;
            break;
        }
    }

    private void receptor(String line){
        switch (getKey(line).toLowerCase()){
        case RECEPTOR:
            receptorCountTable.compute(currentNeuronId, (k, v) -> v == null? 1: v + 1);
            break;
        case BLOCK_END_RECEPTOR:
            currentState = STATE_NEURON;
            break;
        }
    }

    private void synapse(String line){
        switch (getKey(line).toLowerCase()){
        case CLOSE_SYNAPSE:
            currentState = STATE_NEURON;
            break;
        }
    }

    private void communication(String line){
        switch (getKey(line).toLowerCase()){
        case BLOCK_BEGIN_SENSOR:
            currentState = STATE_INPUT;
            break;
        case BLOCK_BEGIN_NPY:
            currentState = STATE_NPY_INPUT;
            break;
        case BLOCK_BEGIN_MOTOR:
            direction = 0;
            currentState = STATE_OUTPUT;
            break;
        case BLOCK_END_COMMUNICATION:
            currentState = STATE_TERMINATE;
            break;
        }
    }

    private void input(String line) throws HanituLoadingException{
        switch (getKey(line).toLowerCase()){
        case NEURON_ID:
            if (postSynapseNeuronID != null){
                createSensorSynapse();
            }
            postSynapseNeuronID = getStrValue(line);
            return;
        case RECEPTOR:
            receptorID = getStrValue(line);
            return;
        case MODEL_TYPE:
            type = getIntValue(line);
            return;
        case BODY_DIRECTION:
            direction = getIntValue(line);
            return;
        case BLOCK_END_SENSOR:
            if (currentState == STATE_INPUT){
                if (postSynapseNeuronID != null){
                    createSensorSynapse();
                }
                currentState = STATE_COMMUNICATION;
                return;
            }
            break;
        case BLOCK_END_NPY:
            if (currentState == STATE_NPY_INPUT){
                if (postSynapseNeuronID != null){
                    createSensorSynapse();
                }
                currentState = STATE_COMMUNICATION;
                return;
            }
            break;
        }
    }

    private void createSensorSynapse(){
        Synapse s;
        if (currentState == STATE_NPY_INPUT){
            s = new Synapse(resolveSensorNeuronID(SensorType.NPY, Direction.SILENCE),
                            postSynapseNeuronID,
                            receptorID,
                            null);
        } else {
            s = new Synapse(resolveSensorNeuronID(SensorType.values()[type], Direction.values()[direction + 1]),
                            postSynapseNeuronID,
                            receptorID,
                            null);

        }
        communication.add(s);
    }

    private void output(String line) throws HanituLoadingException{
        switch (getKey(line).toLowerCase()){
        case NEURON_ID:
            postSynapseNeuronID = getStrValue(line);
            if (!postSynapseNeuronID.startsWith("-")){
                communication.add(new Synapse(postSynapseNeuronID,
                                              resolveMotorNeuronID(Direction.values()[direction + 1]),
                                              "",
                                              null));
            }
            direction = (direction + 1) % 4;
            break;
        case BLOCK_END_MOTOR:
            currentState = STATE_COMMUNICATION;
            break;
        }
    }

    /**
     * testing main
     * @param args circuit files
     */
    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public static void main(String[] args) throws IOException{
        for (String arg : args){
            System.out.println(arg);
            CircuitInfoLoader load = new CircuitInfoLoader();
            load.setFailOnLoad(true);
            load.load(arg);
        }

    }
}
