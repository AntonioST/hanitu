/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.util.*;
import java.util.stream.Collectors;

import cclo.hanitu.data.Direction;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.data.SensorType;

/**
 * worn circuit network.
 *
 * @author antonio
 */
@SuppressWarnings("unused")
public interface Circuit extends CircuitInfo{

    String META_BOX = "box.";

    /**
     * @return the number of the neuron in circuit, except body neurons.
     */
    @Override
    default int neuronCount(){
        return neurons().size();
    }

    List<CentralNeuron> neurons();

    @Override
    default Set<String> getNeuronIDSet(){
        return toNeuronID(neurons());
    }

    /**
     * Get the neuron at index {@code id}. In common case, index {@code id} also mean neuron ID {@code id}.
     *
     * @param neuronID the id of the neuron
     * @return neuron
     * @throws IndexOutOfBoundsException if not found
     */
    CentralNeuron getNeuron(String neuronID);

    @Override
    default OptionalInt receptorCount(String neuronID){
        List<Receptor> list = getReceptor(neuronID);
        return list == null? OptionalInt.empty(): OptionalInt.of(list.size());
    }

    List<Receptor> receptors();

    List<Receptor> getReceptor(String neuronID);

    default List<Receptor> getReceptor(CentralNeuron neuron){
        return getReceptor(neuron.ID.get());
    }

    /**
     * @param neuronID the host neuron id
     * @param ID   the receptor id
     * @return correspond receptor, null if not found
     */
    Receptor getReceptor(String neuronID, String ID);

    default Receptor getReceptor(CentralNeuron neuron, String ID){
        return getReceptor(neuron.ID.get(), ID);
    }

    default Receptor getReceptor(Synapse synapse){
        return getReceptor(synapse.postSynNeuID.get(), synapse.targetRepID.get());
    }

    List<Synapse> synapses();

    @Override
    default List<Synapse> getCommunicateSynapse(){
        return synapses().stream()
          .filter(Synapse::isCommunicateSynapse)
          .collect(Collectors.toList());
    }

    default List<Synapse> getLocalSynapse(){
        return synapses().stream()
          .filter(Synapse::isLocalSynapse)
          .collect(Collectors.toList());
    }

    List<Synapse> getSynapse(String prev);

    default List<Synapse> getSynapse(CentralNeuron prev){
        return getSynapse(prev.ID.get());
    }

    List<Synapse> getSynapseTargetTo(String post);

    default List<Synapse> getSynapseTargetTo(CentralNeuron post){
        return getSynapseTargetTo(post.ID.get());
    }

    List<Synapse> getSynapseTargetTo(String post, String rep);

    default List<Synapse> getSynapseTargetTo(Receptor rep){
        return getSynapseTargetTo(rep.hostNeuID.get(), rep.ID.get());
    }

    /**
     * @param prev  prev-synapse neuron id
     * @param post post-synapse neuron id
     * @return correspond synapse, null if not found
     * @throws IndexOutOfBoundsException if prev-synapse neuron not found
     */
    Synapse getSynapse(String prev, String post);

    default Synapse getSynapse(CentralNeuron pre, CentralNeuron post){
        return getSynapse(pre.ID.get(), post.ID.get());
    }

    List<Synapse> getSynapseBetween(Set<String> neuronSet);

    default List<Synapse> getSynapseBetweenNeuron(Set<CentralNeuron> neuronSet){
        return getSynapseBetween(toNeuronID(neuronSet));
    }

    List<Synapse> getSynapseRelate(Set<String> neuronSet);

    default List<Synapse> getSynapseRelate(String neuron){
        return getSynapseRelate(Collections.singleton(neuron));
    }

    default List<Synapse> getSynapseRelateNeuron(Set<CentralNeuron> neuronSet){
        return getSynapseBetween(toNeuronID(neuronSet));
    }

    List<Synapse> getSensorSynapse(SensorType ty, Direction dir);

    Synapse getSensorSynapse(SensorType ty, Direction dir, String post);

    default Synapse getSensorSynapse(SensorType ty, Direction dir, CentralNeuron post){
        return getSensorSynapse(ty, dir, post.ID.get());
    }

    Synapse getPreMotorSynapse(Direction dir);

    /**
     * parameter reference sensor
     *
     * @param type sensor type
     * @return parameter reference sensor
     */
    Sensor sensor(SensorType type);

    /**
     * parameter reference motor
     *
     * @return parameter reference sensor
     */
    Motor motor();

    Set<String> getBoxNameSet();

    PositionData getBoxMeta(String name);

    Set<String> getBoxNeuron(String name);

    default Set<CentralNeuron> getNeuronInBox(String name){
        return getBoxNeuron(name).stream().map(this::getNeuron).collect(Collectors.toSet());
    }

    default Set<Receptor> getReceptorOnBox(String name){
        return getBoxNeuron(name).stream()
          .flatMap(n -> getReceptor(n).stream())
          .filter(r -> r.metaBoxRep.get())
          .collect(Collectors.toSet());
    }

    default Set<CentralNeuron> getAxonOnBox(String name){
        return getBoxNeuron(name).stream()
          .map(this::getNeuron)
          .filter(n -> n.metaBoxAxon.get())
          .collect(Collectors.toSet());
    }

    // XXX return circuit should be a module circuit, but it is not support it this version.
    Circuit subCircuit(Set<String> neuronSet);

    static Set<String> toNeuronID(Collection<CentralNeuron> neurons){
        return neurons.stream().map(n -> n.ID.get()).collect(Collectors.toSet());
    }
}
