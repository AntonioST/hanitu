/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cclo.hanitu.circuit;

import java.util.Objects;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

import cclo.hanitu.data.DataClass;

/**
 * synapse.
 *
 * @author antonio
 */
public class Synapse implements DataClass{

    public final ReadOnlyStringProperty prevSynNeuID = new SimpleStringProperty(this, "prevsynapseneuron");
    public final ReadOnlyStringProperty postSynNeuID = new SimpleStringProperty(this, "postsynapseneuron");
    public final ReadOnlyStringProperty targetRepID = new SimpleStringProperty(this, "targetreceptor");

    /**
     * the weight of the connection.
     */
    public final SimpleDoubleProperty weight = new SimpleDoubleProperty(this, "weight", 1);

    /**
     * conductance in nS, also known as `G`
     */
    public final SimpleDoubleProperty conductance = new SimpleDoubleProperty(this, "conductance", 1);

    public Synapse(String prev, String post, String rep, Synapse syn){
        ((SimpleStringProperty)prevSynNeuID).set(Objects.requireNonNull(prev, "prev-synapse neuron ID"));
        ((SimpleStringProperty)postSynNeuID).set(Objects.requireNonNull(post, "post-synapse neuron ID"));
        ((SimpleStringProperty)targetRepID).set(Objects.requireNonNull(rep, "target receptor ID"));
        if (syn != null){
            weight.set(syn.weight.get());
            conductance.set(syn.conductance.get());
        }
    }

    public Synapse(String sensorID, ReadOnlyStringProperty post, ReadOnlyStringProperty rep, Synapse syn){
        ((SimpleStringProperty)prevSynNeuID).set(Objects.requireNonNull(sensorID, "prev-synapse neuron ID"));
        ((SimpleStringProperty)postSynNeuID).bind(post);
        ((SimpleStringProperty)targetRepID).bind(rep);
        if (syn != null){
            weight.set(syn.weight.get());
            conductance.set(syn.conductance.get());
        }
    }

    public Synapse(ReadOnlyStringProperty prev, String motorID, Synapse syn){
        ((SimpleStringProperty)prevSynNeuID).bind(prev);
        ((SimpleStringProperty)postSynNeuID).set(Objects.requireNonNull(motorID, "post-synapse neuron ID"));
        if (syn != null){
            weight.set(syn.weight.get());
            conductance.set(syn.conductance.get());
        }
    }

    public Synapse(ReadOnlyStringProperty prev, ReadOnlyStringProperty post, ReadOnlyStringProperty rep, Synapse syn){
        ((SimpleStringProperty)prevSynNeuID).bind(prev);
        ((SimpleStringProperty)postSynNeuID).bind(post);
        ((SimpleStringProperty)targetRepID).bind(rep);
        if (syn != null){
            weight.set(syn.weight.get());
            conductance.set(syn.conductance.get());
        }
    }

    /**
     * copy value from reference
     *
     * @param syn reference synapse
     */
    @SuppressWarnings("unused")
    public void set(Synapse syn){
        Objects.requireNonNull(syn);
        weight.set(syn.weight.get());
        conductance.set(syn.conductance.get());
    }

    @Deprecated
    public boolean isLocalSynapse(){
        return !prevSynNeuID.get().startsWith(CircuitInfo.SENSOR) &&
               !postSynNeuID.get().startsWith(CircuitInfo.MOTOR);
    }

    @Deprecated
    public boolean isNonLocalSynapse(){
        // module circuit not support currently
        // all non-local synapse is communicate synapse
        return isCommunicateSynapse();
    }

    @Deprecated
    public boolean isCommunicateSynapse(){
        return prevSynNeuID.get().startsWith(CircuitInfo.SENSOR) ||
               postSynNeuID.get().startsWith(CircuitInfo.MOTOR);
    }

    @Override
    public String toString(){
        return "Synapse:" + prevSynNeuID.get() + "->" + postSynNeuID.get() + "[" + targetRepID.get() + "]";
    }
}
