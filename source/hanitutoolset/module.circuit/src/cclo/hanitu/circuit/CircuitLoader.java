/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import cclo.hanitu.data.Direction;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.data.SensorType;
import cclo.hanitu.io.FileLoader;

/**
 * The loader of hanitu circuit file
 *
 * @author antonio
 */
public class CircuitLoader extends FileLoader<Circuit>{

    public static final String CIRCUIT_CONFIG_FILE_TYPE = "circuit_config";

    static final String TOTAL_NEURON_NUMBER = "totalneuronnumber";
    static final String NEURON_ID = "neuronid";
    static final String RECEPTOR = "receptor";
    static final String TARGET_NEURON = "targetneuron";
    static final String CAPACITANCE = "c";
    static final String CONDUCTANCE = "g";
    static final String NEURON_REVERSAL_POTENTIAL = "mrevpot";
    static final String NEURON_RESET_POTENTIAL = "resetpot";
    static final String RECEPTOR_REVERSAL_POTENTIAL = "rrevpot";
    static final String SPIKING_THRESHOLD = "threshold";
    static final String REFRACTORY_PERIOD = "refperiod";
    static final String SPIKE_DELAY = "spikedelay";
    static final String CONNECTION_WEIGHT = "weight";
    static final String TIME_CONSTANT = "tau";
    static final String BODY_DIRECTION = "direction";
    static final String MODEL_TYPE = "type";
    static final String NOISE_MEAN = "mean";
    static final String NOISE_STD = "std";
    static final String BODY_NEU_MOTOR = "m";
    static final String BODY_NEU_SENSOR_FOOD = "sf";
    static final String BODY_NEU_SENSOR_TOXICANT = "st";
    static final String BODY_NEU_SENSOR_NPY = "npy";
    static final String BODY_NEU_CAPACITANCE = "cm";
    static final String BODY_NEU_SILENCE_PERIOD = "silence";
    static final String BODY_NEU_SPIKING_THRESHOLD = "vth";
    static final String BODY_NEU_REVERSAL_POTENTIAL = "vl";
    static final String BODY_NEU_RESET_POTENTIAL = "reset";
    static final String CLOSE_NEURON = "endneupar";
    static final String CLOSE_RECEPTOR = "endreceptor";
    static final String CLOSE_SYNAPSE = "endtargetneuron";
    static final String BLOCK_END_NEURON = "endneuron";
    static final String BLOCK_BEGIN_RECEPTOR = "receptorpar";
    static final String BLOCK_END_RECEPTOR = "endreceptorpar";
    static final String BLOCK_BEGIN_NOISE = "membrancenoise";
    static final String BLOCK_END_NOISE = "endmembrancenoise";
    static final String BLOCK_BEGIN_COMMUNICATION = "communication";
    static final String BLOCK_END_COMMUNICATION = "endcommunication";
    static final String BLOCK_BEGIN_SENSOR = "inputneuron";
    static final String BLOCK_END_SENSOR = "endinputneuron";
    static final String BLOCK_BEGIN_NPY = "npytargetneuron";
    static final String BLOCK_END_NPY = "endnpypar";
    static final String BLOCK_BEGIN_MOTOR = "outputneuron";
    static final String BLOCK_END_MOTOR = "endoutputneuron";
    static final String BLOCK_BEGIN_BODY = "bodypar";
    static final String BLOCK_END_BODY = "endbodypar";

    private static final int STATE_IMPORT = 1;
    private static final int STATE_HEAD = 2;
    private static final int STATE_NEURON_PAR = 3;
    private static final int STATE_NEURON_NOISE = 4;
    private static final int STATE_NEURON_COM = 5;
    private static final int STATE_NEURON_REP = 6;
    private static final int STATE_NEURON_TARGET = 7;
    private static final int STATE_COMMUNICATION = 8;
    private static final int STATE_INPUT = 9;
    private static final int STATE_NPY_INPUT = 10;
    private static final int STATE_OUTPUT = 11;
    private static final int STATE_BODY = 12;

    private CircuitBuilder builder;
    //
    private int totalNeuron;
    private String currentNeuronID;
    private CentralNeuron neuron;
    private BodyNeuron bodyNeuron;
    private Receptor receptor;
    private Synapse synapse;
    private String postSynNeuID;
    private String receptorID;
    private int direction;
    private int type;
    private double g;
    private double weight;

    @Override
    public String getFileType(){
        return CircuitLoader.CIRCUIT_CONFIG_FILE_TYPE;
    }

    @Override
    public boolean isFileTypeSupport(String value){
        return CircuitLoader.CIRCUIT_CONFIG_FILE_TYPE.equals(value);
    }

    @Override
    public String getFileVersionExpression(){
        return ">=1.4";
    }

    @Deprecated
    public Circuit load(CircuitInfo info) throws IOException{
        return load(info.source());
    }

    @Override
    public void prevProcess() throws IOException{
        super.prevProcess();
        builder = new CircuitBuilder();
        String source = sourcePath.toString();
        builder.setSource(source);
        neuron = null;
        receptor = null;
        synapse = null;
    }

    @Override
    public Circuit postProcess() throws IOException{
        if (builder.neuronCount() != totalNeuron){
            addWarning("total neuron mis-match, expect {}, but {}", totalNeuron, builder.neuronCount());
        }
        List<Synapse> missingSynapse = builder.getMissingSynapse();
        if (!missingSynapse.isEmpty()){
            addError("Synapse connection resolve failure",
                     missingSynapse.stream().map(Synapse::toString).collect(Collectors.joining("\n")));
        }
        return builder;
    }

    @Override
    public void processLine(String line) throws IOException{
        switch (currentState){
        case STATE_INIT:
            init(line);
            break;
        case STATE_IMPORT:
            module(line);
            break;
        case STATE_HEAD:
            head(line);
            break;
        case STATE_NEURON_PAR:
            neuronParameter(line);
            break;
        case STATE_NEURON_NOISE:
            neuronNoise(line);
            break;
        case STATE_NEURON_COM:
            neuronComponent(line);
            break;
        case STATE_NEURON_REP:
            receptor(line);
            break;
        case STATE_NEURON_TARGET:
            target(line);
            break;
        case STATE_COMMUNICATION:
            communication(line);
            break;
        case STATE_INPUT:
        case STATE_NPY_INPUT:
            input(line);
            break;
        case STATE_OUTPUT:
            output(line);
            break;
        case STATE_BODY:
            body(line);
            break;
        }
    }

    private void init(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        switch (getKey(line).toLowerCase()){
        case TOTAL_NEURON_NUMBER:
            totalNeuron = getNonNegIntValue(line);
            currentState = STATE_IMPORT;
            break;
        default:
            addError("expect {}", CircuitPrinter.getHumanReadable(TOTAL_NEURON_NUMBER));
        }
    }

    private void module(String line) throws IOException{
        if (isMetaLine(line)){
            String metaKey = getMetaKey(line);
            if (metaKey.startsWith(Circuit.META_BOX)){
                processBoxMeta(line);
            }
            return;
        }
        switch (getKey(line).toLowerCase()){
        case NEURON_ID:
            currentState = STATE_HEAD;
            reParsing();
            break;
        case BLOCK_BEGIN_COMMUNICATION:
            currentState = STATE_COMMUNICATION;
            break;
        default:
            addError("expect {} or {}",
                     CircuitPrinter.getHumanReadable(NEURON_ID),
                     CircuitPrinter.getHumanReadable(BLOCK_BEGIN_COMMUNICATION));
        }
    }

    private void processBoxMeta(String origin){
        // %meta.box.name.key=value
        int index = origin.indexOf(".", META_HEAD.length());
        if (index == -1) return;
        String line = origin.substring(index + 1);
        // name.key=value
        index = line.indexOf(".");
        if (index == -1) return;
        String name = line.substring(0, index);
        line = line.substring(index + 1);
        String key = getKey(line);
        String value = getValue(line);

        PositionData box = builder.getBoxMeta(name);
        if (value != null){
            switch (key){
            case "x":
                box.x = Integer.parseInt(value);
                break;
            case "y":
                box.y = Integer.parseInt(value);
                break;
            }
        }
    }

    private void head(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        currentNeuronID = null;
        switch (getKey(line).toLowerCase()){
        case NEURON_ID:
            currentNeuronID = getStrValue(line);
            neuron = builder.createNeuron(currentNeuronID, null);
            currentState = STATE_NEURON_PAR;
            break;
        case BLOCK_BEGIN_COMMUNICATION:
            currentState = STATE_COMMUNICATION;
            break;
        default:
            addError("expect {} or {}",
                     CircuitPrinter.getHumanReadable(NEURON_ID),
                     CircuitPrinter.getHumanReadable(BLOCK_BEGIN_COMMUNICATION));
        }
    }

    private void neuronParameter(String line) throws HanituLoadingException{
        if (isMetaLine(line)){
            processNeuronMeta(line);
            return;
        }
        switch (getKey(line).toLowerCase()){
        case CAPACITANCE:
            neuron.capacitance.set(getFloatValue(line));
            break;
        case CONDUCTANCE:
            neuron.conductance.set(getFloatValue(line));
            break;
        case NEURON_REVERSAL_POTENTIAL:
            neuron.reversalPotential.set(getFloatValue(line));
            break;
        case NEURON_RESET_POTENTIAL:
            neuron.resetPotential.set(getFloatValue(line));
            break;
        case SPIKING_THRESHOLD:
            neuron.threshold.set(getFloatValue(line));
            break;
        case REFRACTORY_PERIOD:
            neuron.refractoryPeriod.set(getIntValue(line));
            break;
        case SPIKE_DELAY:
            neuron.spikeDelay.set(getIntValue(line));
            break;
        case BLOCK_BEGIN_NOISE:
            currentState = STATE_NEURON_NOISE;
            break;
        case CLOSE_NEURON:
            currentState = STATE_NEURON_COM;
            break;
        default:
            addError("unknown keyword : '{}'", getKey(line));
        }
    }


    private void processNeuronMeta(String line){
        String value = getMetaValue(line);
        if (value != null){
            switch (getMetaKey(line)){
            case CentralNeuron.META_X:
                neuron.metaX.set(Integer.parseInt(value));
                break;
            case CentralNeuron.META_Y:
                neuron.metaY.set(Integer.parseInt(value));
                break;
            case CentralNeuron.META_THETA:
                neuron.metaTheta.set(Math.toRadians(Double.parseDouble(value)));
                break;
            case CentralNeuron.META_BOX:
                builder.joinBox(neuron.ID.get(), value);
                break;
            case CentralNeuron.META_BOX_AXON:
                neuron.metaBoxAxon.set(true);
                break;
            }
        }
    }

    private void neuronNoise(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        switch (getKey(line).toLowerCase()){
        case NOISE_MEAN:
            neuron.noiseMean.set(getFloatValue(line));
            break;
        case NOISE_STD:
            neuron.noiseStd.set(getFloatValue(line));
            break;
        case BLOCK_END_NOISE:
            currentState = STATE_NEURON_PAR;
            break;
        default:
            addError("unknown keyword : '{}'", getKey(line));
        }
    }

    private void neuronComponent(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        switch (getKey(line).toLowerCase()){
        case BLOCK_BEGIN_RECEPTOR:
            currentState = STATE_NEURON_REP;
            break;
        case TARGET_NEURON:
            postSynNeuID = getStrValue(line);
            currentState = STATE_NEURON_TARGET;
            break;
        case BLOCK_END_NEURON:
            neuron = null;
            currentState = STATE_HEAD;
            break;
        default:
            addError("unknown keyword : '{}'", getKey(line));
        }
    }

    private void receptor(String line) throws HanituLoadingException{
        if (isMetaLine(line)){
            processReceptorMeta(line);
            return;
        }
        switch (getKey(line).toLowerCase()){
        case RECEPTOR:
            receptor = builder.createReceptor(currentNeuronID, getStrValue(line), null);
            break;
        case MODEL_TYPE:
            //XXX hanitu of current version not implement receptor QDE type
            break;
        case TIME_CONSTANT:
            receptor.timeConstant.set(getFloatValue(line));
            break;
        case RECEPTOR_REVERSAL_POTENTIAL:
            receptor.reversalPotential.set(getFloatValue(line));
            break;
        case CLOSE_RECEPTOR:
            receptor = null;
            break;
        case BLOCK_END_RECEPTOR:
            currentState = STATE_NEURON_COM;
            break;
        default:
            addError("unknown keyword : '{}'", getKey(line));
        }
    }

    private void processReceptorMeta(String line){
        switch (getMetaKey(line)){
        case Receptor.META_BOX_REP:
            receptor.metaBoxRep.set(true);
            break;
        }
    }


    private void target(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        switch (getKey(line).toLowerCase()){
        case RECEPTOR:
            synapse = builder.createUnResolvedSynapse(currentNeuronID,
                                                      postSynNeuID,
                                                      getStrValue(line));
            break;
        case CONNECTION_WEIGHT:
            synapse.weight.set(getFloatValue(line));
            break;
        case CONDUCTANCE:
            synapse.conductance.set(getFloatValue(line));
            break;
        case CLOSE_SYNAPSE:
            synapse = null;
            currentState = STATE_NEURON_COM;
            break;
        default:
            addError("unknown keyword : '{}'", getKey(line));
        }
    }


    private void communication(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        postSynNeuID = null;
        direction = -1;
        switch (line.toLowerCase()){
        case BLOCK_BEGIN_SENSOR:
            currentState = STATE_INPUT;
            break;
        case BLOCK_BEGIN_NPY:
            currentState = STATE_NPY_INPUT;
            break;
        case BLOCK_BEGIN_MOTOR:
            direction = 0;
            currentState = STATE_OUTPUT;
            break;
        case BLOCK_BEGIN_BODY:
            currentState = STATE_BODY;
            break;
        case BLOCK_END_COMMUNICATION:
            currentState = STATE_HEAD;
            break;
        default:
            addError("unknown keyword : '{}'", getKey(line));
        }
    }

    private void input(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        switch (getKey(line).toLowerCase()){
        case NEURON_ID:
            if (postSynNeuID != null){
                createSensorSynapse();
            }
            postSynNeuID = getStrValue(line);
            break;
        case RECEPTOR:
            receptorID = getStrValue(line);
            break;
        case MODEL_TYPE:
            type = getIntValue(line);
            break;
        case BODY_DIRECTION:
            direction = getIntValue(line);
            break;
        case CONNECTION_WEIGHT:
            weight = getFloatValue(line);
            break;
        case CONDUCTANCE:
            g = getFloatValue(line);
            break;
        case BLOCK_END_SENSOR:
            if (currentState == STATE_INPUT){
                if (postSynNeuID != null){
                    createSensorSynapse();
                }
                currentState = STATE_COMMUNICATION;
            } else {
                addError("unexpected keyword : '{}'", getKey(line));
            }
            break;
        case BLOCK_END_NPY:
            if (currentState == STATE_NPY_INPUT){
                if (postSynNeuID != null){
                    createSensorSynapse();
                }
                currentState = STATE_COMMUNICATION;
            } else {
                addError("unexpected keyword : '{}'", getKey(line));
            }
            break;
        default:
            addError("unknown keyword '{}'", getKey(line));
        }
    }

    private void createSensorSynapse(){
        Synapse s;
        if (currentState == STATE_NPY_INPUT){
            s = builder.createSensorSynapse(SensorType.NPY,
                                            Direction.SILENCE,
                                            postSynNeuID,
                                            receptorID,
                                            null);
        } else {
            s = builder.createSensorSynapse(SensorType.values()[type],
                                            Direction.values()[direction + 1],
                                            postSynNeuID,
                                            receptorID,
                                            null);
        }
        s.weight.set(weight);
        s.conductance.set(g);
    }


    private void output(String line) throws HanituLoadingException{
        if (isMetaLine(line)) return;
        switch (getKey(line).toLowerCase()){
        case NEURON_ID:
            postSynNeuID = getStrValue(line);
            if (!postSynNeuID.startsWith("-")){
                builder.setPreMotorSynapse(postSynNeuID,
                                           Direction.values()[direction + 1]);
            }
            direction = (direction + 1) % 4;
            break;
        case BLOCK_END_MOTOR:
            currentState = STATE_COMMUNICATION;
            break;
        default:
            addError("unknown keyword : '{}'", getKey(line));
        }
    }


    private void body(String line) throws HanituLoadingException{
        if (isMetaLine(line)){
            processNPYSensorMeta(line);
            return;
        }
        String key = getKey(line).toLowerCase();
        String match;
        if (key.startsWith(BODY_NEU_MOTOR)){
            bodyNeuron = builder.motor();
            match = key.substring(1);
        } else if (key.startsWith(BODY_NEU_SENSOR_FOOD)){
            bodyNeuron = builder.sensor(SensorType.FOOD);
            match = key.substring(2);
        } else if (key.startsWith(BODY_NEU_SENSOR_TOXICANT)){
            bodyNeuron = builder.sensor(SensorType.TOXICANT);
            match = key.substring(2);
        } else if (key.startsWith(BODY_NEU_SENSOR_NPY)){
            bodyNeuron = builder.sensor(SensorType.NPY);
            match = key.substring(3);
        } else {
            bodyNeuron = null;
            match = key;
        }
        switch (match.toLowerCase()){
        case BODY_NEU_CAPACITANCE:
            bodyNeuron.capacitance.set(getFloatValue(line));
            break;
        case TIME_CONSTANT:
            bodyNeuron.timeConstant.set(getFloatValue(line));
            break;
        case CONNECTION_WEIGHT:
            bodyNeuron.weight.set(getFloatValue(line));
            break;
        case BODY_NEU_SILENCE_PERIOD:
            bodyNeuron.refractoryPeriod.set(getIntValue(line));
            break;
        case BODY_NEU_SPIKING_THRESHOLD:
            bodyNeuron.threshold.set(getFloatValue(line));
            break;
        case BODY_NEU_REVERSAL_POTENTIAL:
            bodyNeuron.reversalPotential.set(getFloatValue(line));
            break;
        case BODY_NEU_RESET_POTENTIAL:
            bodyNeuron.resetPotential.set(getFloatValue(line));
            break;
        case BLOCK_END_BODY:
            currentState = STATE_COMMUNICATION;
            bodyNeuron = null;
            break;
        default:
            addError("unknown keyword : '{}'", getKey(line));
        }
    }

    private void processNPYSensorMeta(String line){
        if (bodyNeuron instanceof Sensor){
            Sensor sensor = (Sensor)bodyNeuron;
            String value = getMetaValue(line);
            if (value != null){
                switch (getMetaKey(line)){
                case CentralNeuron.META_X:
                    sensor.metaX.set(Integer.parseInt(value));
                    break;
                case CentralNeuron.META_Y:
                    sensor.metaY.set(Integer.parseInt(value));
                    break;
                case CentralNeuron.META_THETA:
                    sensor.metaTheta.set(Math.toRadians(Double.parseDouble(value)));
                    break;
                }
            }
        }
    }

    /**
     * testing main.
     * @param args circuit file path
     * @throws IOException
     */
    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public static void main(String[] args) throws IOException{
        for (String arg : args){
            System.out.println(arg);
            CircuitLoader loader = new CircuitLoader();
            loader.setFailOnLoad(true);
            loader.load(arg);
        }

    }
}
