/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.io.IOException;
import java.nio.file.Paths;

import cclo.hanitu.data.Direction;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.data.SensorType;
import cclo.hanitu.io.FilePrinter;
import cclo.hanitu.io.Version;

import static cclo.hanitu.circuit.CircuitLoader.*;

/**
 * print the text content of the circuit to the output stream
 *
 * @author antonio
 */
@SuppressWarnings("unused")
public class CircuitPrinter extends FilePrinter<Circuit>{

    private boolean humanReadable = true;
    private boolean boxCircuit = true;
    private boolean meta = true;

    @Override
    public String getFileType(){
        return CircuitLoader.CIRCUIT_CONFIG_FILE_TYPE;
    }

    @Override
    public String getTargetVersionExpression(){
        return "1.4";
    }

    public void setHumanReadable(boolean humanReadable){
        this.humanReadable = humanReadable;
    }

    public void setPrintMeta(boolean v){
        meta = v;
    }

    public void setBoxCircuit(boolean boxCircuit){
        this.boxCircuit = boxCircuit;
    }

    public void write(Circuit target) throws IOException{
        super.write(Paths.get(target.source()), target);
    }

    private String get(String k){
        return humanReadable? getHumanReadable(k): k;
    }

    public static String getHumanReadable(String k){
        switch (k.toLowerCase()){
        case TOTAL_NEURON_NUMBER:
            return "TotalNeuronNumber";
        case NEURON_ID:
            return "NeuronID";
        case RECEPTOR:
            return "Receptor";
        case TARGET_NEURON:
            return "TargetNeuron";
        case CAPACITANCE:
            return "C";
        case CONDUCTANCE:
            return "G";
        case NEURON_REVERSAL_POTENTIAL:
            return "MRevPot";
        case NEURON_RESET_POTENTIAL:
            return "ResetPot";
        case RECEPTOR_REVERSAL_POTENTIAL:
            return "RRevPot";
        case SPIKING_THRESHOLD:
            return "Threshold";
        case REFRACTORY_PERIOD:
            return "RefPeriod";
        case SPIKE_DELAY:
            return "SpikeDelay";
        case CONNECTION_WEIGHT:
            return "Weight";
        case TIME_CONSTANT:
            return "Tau";
        case BODY_DIRECTION:
            return "Direction";
        case MODEL_TYPE:
            return "Type";
        case NOISE_MEAN:
            return "Mean";
        case NOISE_STD:
            return "STD";
        case BODY_NEU_MOTOR:
            return "M";
        case BODY_NEU_SENSOR_FOOD:
            return "SF";
        case BODY_NEU_SENSOR_TOXICANT:
            return "ST";
        case BODY_NEU_CAPACITANCE:
            return "Cm";
        case BODY_NEU_SILENCE_PERIOD:
            return "Silence";
        case BODY_NEU_SPIKING_THRESHOLD:
            return "VTh";
        case BODY_NEU_REVERSAL_POTENTIAL:
            return "Vl";
        case BODY_NEU_RESET_POTENTIAL:
            return "Reset";
        case CLOSE_NEURON:
            return "EndNeuPar";
        case CLOSE_RECEPTOR:
            return "EndReceptor";
        case CLOSE_SYNAPSE:
            return "EndTargetNeuron";
        case BLOCK_END_NEURON:
            return "EndNeuron";
        case BLOCK_BEGIN_RECEPTOR:
            return "ReceptorPar";
        case BLOCK_END_RECEPTOR:
            return "EndReceptorPar";
        case BLOCK_BEGIN_NOISE:
            return "MembranceNoise";
        case BLOCK_END_NOISE:
            return "EndMembranceNoise";
        case BLOCK_BEGIN_COMMUNICATION:
            return "Communication";
        case BLOCK_END_COMMUNICATION:
            return "EndCommunication";
        case BLOCK_BEGIN_SENSOR:
            return "InputNeuron";
        case BLOCK_END_SENSOR:
            return "EndInputNeuron";
        case BLOCK_BEGIN_NPY:
            return "NPYTargetNeuron";
        case BLOCK_END_NPY:
            return "EndNpyPar";
        case BLOCK_BEGIN_MOTOR:
            return "OutputNeuron";
        case BLOCK_END_MOTOR:
            return "EndOutputNeuron";
        case BLOCK_BEGIN_BODY:
            return "BodyPar";
        case BLOCK_END_BODY:
            return "EndBodyPar";
        default:
            throw new IllegalArgumentException("unknown keyword : " + k);
        }
    }

    @Override
    protected void process(Circuit c){
//        preCircuit();
        processTotalNeuron(c.neuronCount());
        line();
        processCircuit(c);
        line();
        for (CentralNeuron n : c.neurons()){
            processNeuron(n);
            processNeuronMeta(n);
            postNeuronParameter(/*n*/);
            preReceptor();
            for (Receptor r : c.getReceptor(n.ID.get())){
                processReceptor(r);
                processReceptorMeta(r);
                postReceptorParameter(/*r*/);
            }
            postReceptor();
            for (Synapse s : c.getSynapse(n.ID.get())){
                if (s.isLocalSynapse()){
                    processSynapse(s);
                    postSynapseParameter(/*s*/);
                }
            }
            postNeuron();
        }
        //
//        postCircuit();
        preCommunication();
        //
        preInput();
        c.getSensorSynapse(SensorType.FOOD, Direction.FORWARD)
          .forEach(s -> processInput(s, Direction.FORWARD, SensorType.FOOD));
        c.getSensorSynapse(SensorType.FOOD, Direction.BACKWARD)
          .forEach(s -> processInput(s, Direction.BACKWARD, SensorType.FOOD));
        c.getSensorSynapse(SensorType.FOOD, Direction.LEFTWARD)
          .forEach(s -> processInput(s, Direction.LEFTWARD, SensorType.FOOD));
        c.getSensorSynapse(SensorType.FOOD, Direction.RIGHTWARD)
          .forEach(s -> processInput(s, Direction.RIGHTWARD, SensorType.FOOD));
        c.getSensorSynapse(SensorType.TOXICANT, Direction.FORWARD)
          .forEach(s -> processInput(s, Direction.FORWARD, SensorType.TOXICANT));
        c.getSensorSynapse(SensorType.TOXICANT, Direction.BACKWARD)
          .forEach(s -> processInput(s, Direction.BACKWARD, SensorType.TOXICANT));
        c.getSensorSynapse(SensorType.TOXICANT, Direction.LEFTWARD)
          .forEach(s -> processInput(s, Direction.LEFTWARD, SensorType.TOXICANT));
        c.getSensorSynapse(SensorType.TOXICANT, Direction.RIGHTWARD)
          .forEach(s -> processInput(s, Direction.RIGHTWARD, SensorType.TOXICANT));
        postInput();
        //
        if (Version.isCompatibility(getTargetVersionExpression(), ">=1.4")){
            preNpyTarget();
            c.getSensorSynapse(SensorType.NPY, Direction.SILENCE)
              .forEach(s -> processInput(s, Direction.SILENCE, SensorType.NPY));
            postNpyTarget();
        }
        //
        preOutput();
        processOutput(c.getPreMotorSynapse(Direction.FORWARD)/*, Direction.FORWARD*/);
        processOutput(c.getPreMotorSynapse(Direction.BACKWARD)/*, Direction.BACKWARD*/);
        processOutput(c.getPreMotorSynapse(Direction.LEFTWARD)/*, Direction.LEFTWARD*/);
        processOutput(c.getPreMotorSynapse(Direction.RIGHTWARD)/*, Direction.RIGHTWARD*/);
        postOutput();
        //
        preBody();
        processBody(c.motor());
        processBody(c.sensor(SensorType.FOOD));
        processBody(c.sensor(SensorType.TOXICANT));
        if (Version.isCompatibility(getTargetVersionExpression(), ">=1.4")){
            processBody(c.sensor(SensorType.NPY));
            processNeuronMeta(c.sensor(SensorType.NPY));
        }
        postBody();
        postCommunication();
    }

//    private void preCircuit(){
//    }
//
//    private void postCircuit(){
//    }

    private void postNeuron(){
        decIndentLevel();
        line(get(BLOCK_END_NEURON));
        line();
    }

    private void preReceptor(){
        line(get(BLOCK_BEGIN_RECEPTOR));
        incIndentLevel();
    }

    private void postReceptor(){
        decIndentLevel();
        line(get(BLOCK_END_RECEPTOR));
        line();
    }

    private void preCommunication(){
        line(get(BLOCK_BEGIN_COMMUNICATION));
        line();
    }

    private void postCommunication(){
        line(get(BLOCK_END_COMMUNICATION));
        incIndentLevel();
    }

    private void preInput(){
        line(get(BLOCK_BEGIN_SENSOR));
        incIndentLevel();
    }

    private void postInput(){
        decIndentLevel();
        line(get(BLOCK_END_SENSOR));
        line();
    }

    private void preNpyTarget(){
        line(get(BLOCK_BEGIN_NPY));
        line();
        incIndentLevel();
    }

    private void postNpyTarget(){
        decIndentLevel();
        line(get(BLOCK_END_NPY));
        line();
    }

    private void preOutput(){
        line(get(BLOCK_BEGIN_MOTOR));
        incIndentLevel();
    }

    private void postOutput(){
        decIndentLevel();
        line(get(BLOCK_END_MOTOR));
    }

    private void preBody(){
        line(get(BLOCK_BEGIN_BODY));
        line();
        incIndentLevel();
    }

    private void postBody(){
        decIndentLevel();
        line(get(BLOCK_END_BODY));
    }

    private void processTotalNeuron(int c){
        pair(get(TOTAL_NEURON_NUMBER), c);
    }

    private void processCircuit(Circuit c){
        if (boxCircuit){
            c.getBoxNameSet().forEach(name -> {
                PositionData box = c.getBoxMeta(name);
                metaf(Circuit.META_BOX + name + ".x", "%d", (int)box.x);
                metaf(Circuit.META_BOX + name + ".y", "%d", (int)box.y);
            });
        }
    }

    private void processNeuron(CentralNeuron n){
        pair(get(NEURON_ID), n.ID);
        incIndentLevel();
        pair(get(CAPACITANCE), n.capacitance);
        pair(get(CONDUCTANCE), n.conductance);
        pair(get(NEURON_REVERSAL_POTENTIAL), n.reversalPotential);
        pair(get(SPIKING_THRESHOLD), n.threshold);
        pair(get(NEURON_RESET_POTENTIAL), n.resetPotential);
        pair(get(REFRACTORY_PERIOD), n.refractoryPeriod);
        pair(get(SPIKE_DELAY), n.spikeDelay);
        line(get(BLOCK_BEGIN_NOISE));
        incIndentLevel();
        pair(get(NOISE_STD), n.noiseStd);
        pair(get(NOISE_MEAN), n.noiseMean);
        decIndentLevel();
        line(get(BLOCK_END_NOISE));

    }

    private void processNeuronMeta(CentralNeuron n){
        if (meta){
            metaf(CentralNeuron.META_X, "%d", n.metaX);
            metaf(CentralNeuron.META_Y, "%d", n.metaY);
            metaf(CentralNeuron.META_THETA, "%.0f", Math.toDegrees(n.metaTheta.get()));
            if (boxCircuit && n.metaBox.get() != null){
                metaf(CentralNeuron.META_BOX, "%s", n.metaBox);
                if (n.metaBoxAxon.get()){
                    meta(CentralNeuron.META_BOX_AXON);
                }
            }
        }
    }

    private void processNeuronMeta(Sensor n){
        if (meta){
            metaf(CentralNeuron.META_X, "%d", n.metaX);
            metaf(CentralNeuron.META_Y, "%d", n.metaY);
            metaf(CentralNeuron.META_THETA, "%.0f", Math.toDegrees(n.metaTheta.get()));
        }
    }

    private void postNeuronParameter(/*CentralNeuron n*/){
        line(get(CLOSE_NEURON));
        line();
    }

    private void processReceptor(Receptor r){
        pair(get(RECEPTOR), r.ID);
        incIndentLevel();
        pair(get(MODEL_TYPE), 0);
        pair(get(TIME_CONSTANT), r.timeConstant);
        pair(get(RECEPTOR_REVERSAL_POTENTIAL), r.reversalPotential);
    }

    private void postReceptorParameter(/*Receptor r*/){
        decIndentLevel();
        line(get(CLOSE_RECEPTOR));
    }

    private void processReceptorMeta(Receptor r){
        if (meta){
            if (r.metaBoxRep.get()){
                meta(Receptor.META_BOX_REP);
            }
        }
    }

    private void processSynapse(Synapse s){
        pair(get(TARGET_NEURON), s.postSynNeuID);
        incIndentLevel();
        pair(get(RECEPTOR), s.targetRepID);
        pair(get(CONNECTION_WEIGHT), s.weight);
        pair(get(CONDUCTANCE), s.conductance);
    }

    private void postSynapseParameter(/*Synapse s*/){
        decIndentLevel();
        line(get(CLOSE_SYNAPSE));
        line();
    }

    private void processInput(Synapse c, Direction dir, SensorType type){
        if (SensorType.isDirectSpecific(type)){
            pair(get(NEURON_ID), c.postSynNeuID);
            incIndentLevel();
            pair(get(RECEPTOR), c.targetRepID);
            pair(get(CONNECTION_WEIGHT), c.weight);
            pair(get(CONDUCTANCE), c.conductance);
            pair(get(MODEL_TYPE), type.ordinal());
            pair(get(BODY_DIRECTION), dir.ordinal() - 1);
            decIndentLevel();
        } else {
            pair(get(NEURON_ID), c.postSynNeuID);
            incIndentLevel();
            pair(get(RECEPTOR), c.targetRepID);
            pair(get(CONNECTION_WEIGHT), c.weight);
            pair(get(CONDUCTANCE), c.conductance);
            decIndentLevel();
        }
        line();
    }

    private void processOutput(Synapse synapse/*, Direction dir*/){
        if (synapse != null){
            pair(get(NEURON_ID), synapse.prevSynNeuID);
        } else {
            pair(get(NEURON_ID), -1);
        }
    }

    private void processBody(Motor n){
        pair(BODY_NEU_MOTOR + get(BODY_NEU_CAPACITANCE), n.capacitance);
        pair(BODY_NEU_MOTOR + get(TIME_CONSTANT), n.timeConstant);
        pair(BODY_NEU_MOTOR + get(CONNECTION_WEIGHT), n.weight);
        pair(BODY_NEU_MOTOR + get(BODY_NEU_SILENCE_PERIOD), n.refractoryPeriod);
        pair(BODY_NEU_MOTOR + get(BODY_NEU_SPIKING_THRESHOLD), n.threshold);
        pair(BODY_NEU_MOTOR + get(BODY_NEU_REVERSAL_POTENTIAL), n.reversalPotential);
        pair(BODY_NEU_MOTOR + get(BODY_NEU_RESET_POTENTIAL), n.resetPotential);
        line();
    }

    private void processBody(Sensor n){
        String prefix;
        switch (n.type){
        case FOOD:
            prefix = BODY_NEU_SENSOR_FOOD;
            break;
        case TOXICANT:
            prefix = BODY_NEU_SENSOR_TOXICANT;
            break;
        case NPY:
            prefix = BODY_NEU_SENSOR_NPY;
            break;
        default:
            return;
        }
        pair(prefix + get(BODY_NEU_CAPACITANCE), n.capacitance);
        pair(prefix + get(TIME_CONSTANT), n.timeConstant);
        pair(prefix + get(CONNECTION_WEIGHT), n.weight);
        pair(prefix + get(BODY_NEU_SILENCE_PERIOD), n.refractoryPeriod);
        pair(prefix + get(BODY_NEU_SPIKING_THRESHOLD), n.threshold);
        pair(prefix + get(BODY_NEU_REVERSAL_POTENTIAL), n.reversalPotential);
        pair(prefix + get(BODY_NEU_RESET_POTENTIAL), n.resetPotential);
        line();
    }
}
