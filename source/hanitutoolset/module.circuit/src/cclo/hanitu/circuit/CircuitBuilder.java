/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import javafx.beans.property.SimpleStringProperty;

import cclo.hanitu.data.Direction;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.data.SensorType;

import static cclo.hanitu.data.Direction.SILENCE;
import static cclo.hanitu.data.SensorType.*;

/**
 * the builder of the circuit.
 *
 * @author antonio
 */
@SuppressWarnings({"WeakerAccess", "UnusedReturnValue", "unused"})
public class CircuitBuilder implements Circuit{

    private String source;
    private final Map<String, CentralNeuron> neurons = new HashMap<>();
    private final Map<String, List<Receptor>> receptors = new HashMap<>();
    private final List<Synapse> synapses = new ArrayList<>();
    private Sensor foodSensor;
    private Sensor toxicantSensor;
    private Motor motor;
    private Sensor npySensor;

    private static final Comparator<CentralNeuron> CMP
      = Comparator.<CentralNeuron>comparingInt(n -> n.ID.get().length())
      .thenComparing(n -> n.ID.get());

    private final Map<String, PositionData> boxMeta = new WeakHashMap<>();

    public CircuitBuilder(){
        clear();
    }

    public CircuitBuilder(Circuit c){
        source = c.source();
        clear();
        //
        //box
        for (String boxName : c.getBoxNameSet()){
            boxMeta.put(boxName, new PositionData(c.getBoxMeta(boxName)));
        }
        //central neuron
        for (CentralNeuron n : c.neurons()){
            //create neuron
            CentralNeuron t = new CentralNeuron(n.ID.get(), n);
            neurons.put(t.ID.get(), t);
        }
        //create receptor
        c.receptors().stream()
          .map(r -> new Receptor(neurons.get(r.hostNeuID.get()).ID, r.ID.get(), r))
          .forEach(r -> receptors.computeIfAbsent(r.hostNeuID.get(), _k -> new ArrayList<>()).add(r));
        c.synapses().forEach(s -> synapses.add(new Synapse(s.prevSynNeuID, s.postSynNeuID, s.targetRepID, s)));
        foodSensor.set(c.sensor(FOOD));
        toxicantSensor.set(c.sensor(TOXICANT));
        npySensor.set(c.sensor(NPY));
        motor.set(c.motor());
    }

    public void clear(){
        neurons.clear();
        receptors.clear();
        synapses.clear();
        foodSensor = new Sensor(FOOD);
        toxicantSensor = new Sensor(TOXICANT);
        npySensor = new Sensor(NPY);
        motor = new Motor();
        boxMeta.clear();
    }

    @Override
    public String source(){
        return source;
    }

    void setSource(String source){
        this.source = source;
    }

    /**
     * @return the number of the neuron in circuit, except body neurons.
     */
    @Override
    public int neuronCount(){
        return neurons.size();
    }

    @Override
    public List<CentralNeuron> neurons(){
        return neurons.values().stream().sorted(CMP).collect(Collectors.toList());
    }

    @Override
    public Set<String> getNeuronIDSet(){
        return Collections.unmodifiableSet(neurons.keySet());
    }

    /**
     * Get the neuron by its {@code ID}.
     *
     * @param neuronID the ID of the neuron
     * @return neuron, null if not found
     */
    @Override
    public CentralNeuron getNeuron(String neuronID){
        return neurons.get(Objects.requireNonNull(neuronID));
    }

    private String genNeuronID(){
        long time = System.currentTimeMillis();
        return String.valueOf(time % 100) + (time / 100);
    }

    /**
     * create a new neuron with initial parameters except ID.
     * ID may correspond the neuron count.
     *
     * @return new neuron.
     */
    public CentralNeuron createNeuron(){
        String ID;
        do {
            ID = genNeuronID();
        } while (neurons.containsKey(ID));
        CentralNeuron n = new CentralNeuron(ID, null);
        neurons.put(n.ID.get(), n);
        return n;
    }

    public CentralNeuron createNeuron(CentralNeuron ref){
        Objects.requireNonNull(ref, "reference neuron");
        String ID;
        do {
            ID = genNeuronID();
        } while (neurons.containsKey(ID));
        CentralNeuron n = new CentralNeuron(ID, ref);
        neurons.put(n.ID.get(), n);
        return n;
    }

    public CentralNeuron createNeuron(String ID, CentralNeuron ref){
        if (neurons.containsKey(ID)) throw new RuntimeException("neuron " + ID + " has existed");
        CentralNeuron n = new CentralNeuron(ID, ref);
        neurons.put(n.ID.get(), n);
        return n;
    }

    /**
     * Remove neuron.
     *
     * all neuron will re-assign ID number.
     *
     * @param ID the ID of the neuron
     * @return correspond neuron, only keep the parameter and the receptor, loss connection.
     * @throws IndexOutOfBoundsException if not found
     */
    public CentralNeuron removeNeuron(String ID){
        Objects.requireNonNull(ID);
        //remove neuron
        CentralNeuron n = neurons.remove(ID);
        if (n != null){
            //remove receptor relative to this neuron
            receptors.remove(ID);
            //remove synapse relative to this neuron
            synapses.removeIf(s -> ID.equals(s.prevSynNeuID.get()) || ID.equals(s.postSynNeuID.get()));
        }
        return n;
    }

    public boolean removeNeuron(CentralNeuron neuron){
        return removeNeuron(neuron.ID.get()) != null;
    }

    public void reassignNeuronID(String oldID, String newID){
        Objects.requireNonNull(oldID);
        Objects.requireNonNull(newID);
        if (oldID.equals(newID)) return;
        if (neurons.containsKey(newID)) throw new RuntimeException("neuron " + newID + " has existed");
        CentralNeuron neuron = neurons.remove(oldID);
        if (neuron == null) return;
        ((SimpleStringProperty)neuron.ID).set(newID);
        neurons.put(newID, neuron);
        receptors.put(newID, receptors.remove(oldID));
        // for resolved Receptor and Synapse
        // their Id are bind together
        // so change neuron ID also update their relative property
    }

    public void reassignNeuronID(CentralNeuron neuron, String newID){
        reassignNeuronID(neuron.ID.get(), newID);
    }

    public void reassignNeuronID(Function<String, String> rename){
        Objects.requireNonNull(rename);
        Map<String, String> map = neurons.values().stream()
          .map(n -> n.ID.get())
          .collect(Collectors.toMap(Function.identity(), rename));
        if (map.size() != new HashSet<>(map.values()).size()) throw new IllegalArgumentException("duplicate name");
        List<CentralNeuron> neus = new ArrayList<>(neurons.values());
        HashMap<String, List<Receptor>> reps = new HashMap<>(receptors);
        neurons.clear();
        receptors.clear();
        for (CentralNeuron neuron : neus){
            String oID = neuron.ID.get();
            String nID = map.get(oID);
            if (!oID.equals(nID)){
                ((SimpleStringProperty)neuron.ID).set(nID);
            }
            neurons.put(nID, neuron);
            receptors.put(nID, reps.get(oID));
        }
    }

    @Override
    public List<Receptor> receptors(){
        return receptors.values().stream().flatMap(List::stream).collect(Collectors.toList());
    }

    @Override
    public List<Receptor> getReceptor(String neuronID){
        Objects.requireNonNull(neuronID);
        List<Receptor> list = receptors.get(neuronID);
        return list == null? Collections.emptyList(): Collections.unmodifiableList(list);
    }

    /**
     * @param neuronID the host neuron ID
     * @param ID   the receptor ID
     * @return correspond receptor, null if not found
     */
    @Override
    public Receptor getReceptor(String neuronID, String ID){
        Objects.requireNonNull(neuronID);
        Objects.requireNonNull(ID);
        List<Receptor> list = receptors.get(neuronID);
        if (list == null) return null;
        return list.stream()
          .filter(r -> ID.equals(r.ID.get()))
          .findFirst().orElse(null);
    }

    /**
     * create a new receptor with initial parameters except ID.
     *
     * @param neuronID the host neuron ID
     * @return new receptor
     */
    public Receptor createReceptor(String neuronID, Receptor ref){
        Objects.requireNonNull(neuronID);
        CentralNeuron neuron = Objects.requireNonNull(neurons.get(neuronID), "neuron not found : " + neuronID);
        String ID;
        do {
            ID = genNeuronID();
        } while (getReceptor(neuronID, ID) != null);
        Receptor r = new Receptor(neuron.ID, ID, ref);
        receptors.computeIfAbsent(neuronID, _k -> new ArrayList<>()).add(r);
        return r;
    }

    public Receptor createReceptor(String neuronID, String ID, Receptor ref){
        Objects.requireNonNull(neuronID);
        Objects.requireNonNull(ID);
        CentralNeuron neuron = Objects.requireNonNull(neurons.get(neuronID), "neuron not found : " + neuronID);
        if (getReceptor(neuronID, ID) != null) throw new RuntimeException("receptor " + ID + " has existed");
        Receptor r = new Receptor(neuron.ID, ID, ref);
        receptors.computeIfAbsent(neuronID, _k -> new ArrayList<>()).add(r);
        return r;
    }

    public Receptor createReceptor(CentralNeuron neuron, Receptor ref){
        Objects.requireNonNull(neuron);
        return createReceptor(neuron.ID.get(), ref);
    }

    public List<Receptor> removeReceptor(String neuronID){
        Objects.requireNonNull(neuronID);
        return receptors.remove(neuronID);
    }

    public List<Receptor> removeReceptor(CentralNeuron neuron){
        return removeReceptor(neuron.ID.get());
    }

    /**
     * @param neuronID the host neuron ID
     * @param ID   the receptor ID
     * @return correspond receptor
     * @throws IndexOutOfBoundsException
     */
    public Receptor removeReceptor(String neuronID, String ID){
        Objects.requireNonNull(neuronID);
        Objects.requireNonNull(ID);
        List<Receptor> receptors = new ArrayList<>(4);
        Iterator<Receptor> it = receptors.iterator();
        Receptor ret = null;
        while (it.hasNext()){
            Receptor r = it.next();
            if (neuronID.equals(r.hostNeuID.get())){
                receptors.add(r);
                if (ID.equals(r.ID.get())){
                    it.remove();
                    ret = r;
                }
            }
        }
        return ret;
    }

    public boolean removeReceptor(Receptor receptor){
        return removeReceptor(receptor.hostNeuID.get(), receptor.ID.get()) != null;
    }

    public void reassignReceptorID(String host, String oldID, String newID){
        Objects.requireNonNull(host);
        Objects.requireNonNull(oldID);
        Objects.requireNonNull(newID);
        if (oldID.equals(newID)) return;
        List<Receptor> list = receptors.get(host);
        if (list != null){
            if (list.stream().anyMatch(r -> newID.equals(r.ID.get())))
                throw new IllegalArgumentException("receptor ID '" + newID + "' has been used");
            list.stream()
              .filter(rep -> oldID.equals(rep.ID.get()))
              .forEach(rep -> ((SimpleStringProperty)rep.ID).set(newID));
        }
    }

    public void reassignReceptorID(String host, Function<String, String> rename){
        Objects.requireNonNull(host);
        Objects.requireNonNull(rename);
        List<Receptor> list = receptors.get(host);
        if (list != null){
            Map<String, String> map = list.stream()
              .map(r -> r.ID.get())
              .collect(Collectors.toMap(Function.identity(), rename));
            if (map.size() != new HashSet<>(map.values()).size()) throw new IllegalArgumentException("name duplicate");
            for (Receptor rep : list){
                String oID = rep.ID.get();
                String nID = map.get(oID);
                if (!oID.equals(nID)) ((SimpleStringProperty)rep.ID).set(nID);
            }
        }
    }

    public void reassignReceptorID(Receptor receptor, String newID){
        reassignReceptorID(receptor.hostNeuID.get(), receptor.ID.get(), newID);
    }

    public void reassignReceptorID(CentralNeuron neuron, Function<String, String> rename){
        reassignReceptorID(neuron.ID.get(), rename);
    }

    @Override
    public List<Synapse> synapses(){
        return Collections.unmodifiableList(synapses);
    }

    @Override
    public List<Synapse> getSynapse(String prev){
        Objects.requireNonNull(prev);
        return synapses.stream()
          .filter(s -> prev.equals(s.prevSynNeuID.get()))
          .collect(Collectors.toList());
    }

    @Override
    public List<Synapse> getSynapseTargetTo(String post){
        Objects.requireNonNull(post);
        return synapses.stream()
          .filter(s -> post.equals(s.postSynNeuID.get()))
          .collect(Collectors.toList());
    }

    @Override
    public List<Synapse> getSynapseTargetTo(String post, String rep){
        Objects.requireNonNull(post);
        Objects.requireNonNull(rep);
        return synapses.stream()
          .filter(s -> post.equals(s.postSynNeuID.get()))
          .filter(s -> rep.equals(s.targetRepID.get()))
          .collect(Collectors.toList());
    }

    /**
     * @param prev  pre-synapse neuron ID
     * @param post post-synapse neuron ID
     * @return correspond synapse, null if not found
     * @throws IndexOutOfBoundsException if pre-synapse neuron not found
     */
    @Override
    public Synapse getSynapse(String prev, String post){
        Objects.requireNonNull(prev);
        Objects.requireNonNull(post);
        return synapses.stream()
          .filter(s -> prev.equals(s.prevSynNeuID.get()) && s.postSynNeuID.get().equals(post))
          .findFirst().orElse(null);
    }

    @Override
    public List<Synapse> getSynapseBetween(Set<String> neuronSet){
        Objects.requireNonNull(neuronSet);
        return synapses.stream()
          .filter(s -> neuronSet.contains(s.prevSynNeuID.get()) && neuronSet.contains(s.postSynNeuID.get()))
          .collect(Collectors.toList());
    }

    @Override
    public List<Synapse> getSynapseRelate(Set<String> neuronSet){
        Objects.requireNonNull(neuronSet);
        return synapses.stream()
          .filter(s -> neuronSet.contains(s.prevSynNeuID.get()) || neuronSet.contains(s.postSynNeuID.get()))
          .collect(Collectors.toList());
    }

    @Override
    public List<Synapse> getSynapseRelate(String neuron){
        Objects.requireNonNull(neuron);
        return synapses.stream()
          .filter(s -> neuron.equals(s.prevSynNeuID.get()) || neuron.equals(s.postSynNeuID.get()))
          .collect(Collectors.toList());
    }

    /**
     * @param ty  sensor type
     * @param dir sensor direction
     * @return a list of corresponding sensor synapses
     */
    @Override
    public List<Synapse> getSensorSynapse(SensorType ty, Direction dir){
        checkSensorDirection(ty, dir);
        String sensorID = CircuitInfo.resolveSensorNeuronID(ty, dir);
        return synapses.stream()
          .filter(s -> sensorID.equals(s.prevSynNeuID.get()))
          .collect(Collectors.toList());
    }

    /**
     * @param ty   sensor type
     * @param dir  sensor direction
     * @param post post-sensor neuron ID
     * @return synapse. null if not found.
     */
    @Override
    public Synapse getSensorSynapse(SensorType ty, Direction dir, String post){
        Objects.requireNonNull(post);
        return getSensorSynapse(ty, dir).stream()
          .filter(s -> post.equals(s.postSynNeuID.get()))
          .findFirst().orElse(null);
    }

    @Override
    public Synapse getPreMotorSynapse(Direction dir){
        if (dir == SILENCE) throw new IllegalArgumentException("sensor direction");
        String motorID = CircuitInfo.resolveMotorNeuronID(dir);
        return synapses.stream()
          .filter(s -> motorID.equals(s.postSynNeuID.get()))
          .findFirst().orElse(null);
    }

    /**
     * create a new synapse
     *
     * @param prev  prev-synapse neuron ID
     * @param post post-synapse neuron ID
     * @param rep  the ID of the receptor of the post-synapse neuron
     * @return a new synapse
     * @throws IndexOutOfBoundsException
     */

    public Synapse createSynapse(String prev, String post, String rep, Synapse ref){
        Objects.requireNonNull(prev);
        Objects.requireNonNull(post);
        Objects.requireNonNull(rep);
        CentralNeuron prevNeu = Objects.requireNonNull(neurons.get(prev), "prev-synapse neuron not found : " + prev);
        CentralNeuron postNeu = Objects.requireNonNull(neurons.get(post), "post-synapse neuron not found : " + post);
        Receptor target = Objects.requireNonNull(getReceptor(post, rep), "receptor not found : " + rep);
        Synapse s = new Synapse(prevNeu.ID, postNeu.ID, target.ID, ref);
        synapses.add(s);
        return s;
    }

    public Synapse createSynapse(CentralNeuron prev, CentralNeuron target, Synapse ref){
        String post = target.ID.get();
        List<Receptor> list = getReceptor(post);
        if (list.isEmpty()) throw new IllegalArgumentException("neuron '" + post + "' has no receptors");
        return createSynapse(prev.ID.get(), post, list.get(0).ID.get(), ref);
    }

    public Synapse createSynapse(CentralNeuron prev, Receptor target, Synapse ref){
        return createSynapse(prev.ID.get(), target.hostNeuID.get(), target.ID.get(), ref);
    }

    Synapse createUnResolvedSynapse(String prev, String post, String rep){
        Objects.requireNonNull(prev);
        Objects.requireNonNull(post);
        Objects.requireNonNull(rep);
        Synapse s = new Synapse(prev, post, rep, null);
        synapses.add(s);
        return s;
    }

    /**
     * create a new synapse from sensor to a central neuron.
     *
     * @param ty   sensor type
     * @param dir  sensor direction
     * @param post post-synapse neuron ID
     * @param rep  the ID of the receptor of the post-synapse neuron
     * @return a new synapse
     */
    public Synapse createSensorSynapse(SensorType ty, Direction dir, String post, String rep, Synapse ref){
        checkSensorDirection(ty, dir);
        Objects.requireNonNull(post);
        Objects.requireNonNull(rep);
        CentralNeuron postNeu = Objects.requireNonNull(neurons.get(post), "post-synapse neuron not found : " + post);
        Receptor target = Objects.requireNonNull(getReceptor(post, rep), "receptor not found : " + rep);
        Synapse s = new Synapse(CircuitInfo.resolveSensorNeuronID(ty, dir), postNeu.ID, target.ID, ref);
        synapses.add(s);
        return s;
    }

    public Synapse createSensorSynapse(SensorType ty, Direction dir, CentralNeuron post, Synapse ref){
        String postNeuID = post.ID.get();
        List<Receptor> list = getReceptor(postNeuID);
        if (list.isEmpty()) throw new IllegalArgumentException("neuron '" + postNeuID + "' has no receptors");
        return createSensorSynapse(ty, dir, postNeuID, list.get(0).ID.get(), ref);

    }

    public Synapse createSensorSynapse(SensorType ty, Direction dir, Receptor rep, Synapse ref){
        return createSensorSynapse(ty, dir, rep.hostNeuID.get(), rep.ID.get(), ref);
    }

    /**
     * set synapse from a central to motor neuron.
     * <P>
     * motor synapse doesn't need to create.
     *
     * @param prev pre-motor neuron ID
     * @param dir motor direction
     */
    public Synapse setPreMotorSynapse(String prev, Direction dir){
        if (dir == SILENCE) throw new IllegalArgumentException();
        Objects.requireNonNull(prev);
        CentralNeuron prevNeu = Objects.requireNonNull(neurons.get(prev), "prev-synapse neuron not found : " + prev);
        String motorID = CircuitInfo.resolveMotorNeuronID(dir);
        Optional<Synapse> ref = synapses.stream()
          .filter(s -> motorID.equals(s.postSynNeuID.get()))
          .findFirst();
        Synapse s;
        if (ref.isPresent()){
            s = ref.get();
            SimpleStringProperty property = (SimpleStringProperty)s.prevSynNeuID;
            property.bind(prevNeu.ID);
        } else {
            s = new Synapse(prevNeu.ID, motorID, ref.orElse(null));
            synapses.add(s);
        }
        return s;
    }

    public Synapse setPreMotorSynapse(CentralNeuron prev, Direction dir){
        return setPreMotorSynapse(prev.ID.get(), dir);
    }

    private void checkSensorDirection(SensorType type, Direction dir){
        if (SensorType.isDirectSpecific(type) && dir == SILENCE){
            throw new IllegalArgumentException("sensor direction : " + type);
        }
    }

    public List<Synapse> getMissingSynapse(){
        return synapses.stream()
          .filter(s -> {
              //noinspection StatementWithEmptyBody
              if (s.prevSynNeuID.get().startsWith(SENSOR)){
              } else if (!neurons.containsKey(s.prevSynNeuID.get())){
                  return true;
              } else {
                  ((SimpleStringProperty)s.prevSynNeuID).bind(neurons.get(s.prevSynNeuID.get()).ID);
              }
              //noinspection StatementWithEmptyBody
              if (s.postSynNeuID.get().startsWith(MOTOR)){
              } else if (!neurons.containsKey(s.postSynNeuID.get())){
                  return true;
              } else {
                  ((SimpleStringProperty)s.postSynNeuID).bind(neurons.get(s.postSynNeuID.get()).ID);
              }
              return false;
          })
          .collect(Collectors.toList());
    }

    public List<Synapse> removeSynapse(String prev){
        Objects.requireNonNull(prev);
        List<Synapse> ret = new LinkedList<>();
        Iterator<Synapse> it = synapses.iterator();
        while (it.hasNext()){
            Synapse s = it.next();
            if (prev.equals(s.prevSynNeuID.get())){
                it.remove();
                ret.add(s);
            }
        }
        return new ArrayList<>(ret);
    }

    public List<Synapse> removeSynapse(CentralNeuron prev){
        return removeSynapse(prev.ID.get());
    }

    public List<Synapse> removeSynapseTargetTo(String post){
        Objects.requireNonNull(post);
        List<Synapse> ret = new LinkedList<>();
        Iterator<Synapse> it = synapses.iterator();
        while (it.hasNext()){
            Synapse s = it.next();
            if (post.equals(s.postSynNeuID.get())){
                it.remove();
                ret.add(s);
            }
        }
        return new ArrayList<>(ret);
    }

    public List<Synapse> removesynapseTargetTo(CentralNeuron post){
        return removeSynapseTargetTo(post.ID.get());
    }

    public List<Synapse> removeSynapseTargetTo(String post, String rep){
        Objects.requireNonNull(post);
        Objects.requireNonNull(rep);
        List<Synapse> ret = new LinkedList<>();
        Iterator<Synapse> it = synapses.iterator();
        while (it.hasNext()){
            Synapse s = it.next();
            if (post.equals(s.postSynNeuID.get()) && rep.equals(s.targetRepID.get())){
                it.remove();
                ret.add(s);
            }
        }
        return new ArrayList<>(ret);
    }

    public List<Synapse> removesynapseTargetTo(Receptor receptor){
        return removeSynapseTargetTo(receptor.hostNeuID.get(), receptor.ID.get());
    }

    /**
     * @param prev pre-synapse neuron ID
     * @param post post-synapse neuron ID
     * @return correspond synapse, null if not found.
     * @throws IndexOutOfBoundsException if pre-synapse neuron not found
     */
    public Synapse removeSynapse(String prev, String post){
        Objects.requireNonNull(prev);
        Objects.requireNonNull(post);
        Iterator<Synapse> it = synapses.iterator();
        while (it.hasNext()){
            Synapse s = it.next();
            if (prev.equals(s.prevSynNeuID.get()) && post.equals(s.postSynNeuID.get())){
                it.remove();
                return s;
            }
        }
        return null;
    }

    public Synapse removeSynapse(CentralNeuron prev, CentralNeuron post){
        return removeSynapse(prev.ID.get(), post.ID.get());
    }

    public boolean removeSynapse(Synapse synapse){
        return removeSynapse(synapse.prevSynNeuID.get(), synapse.postSynNeuID.get()) != null;
    }

    public List<Synapse> removeSensorSynapse(SensorType ty, Direction dir){
        checkSensorDirection(ty, dir);
        String sensorID = CircuitInfo.resolveSensorNeuronID(ty, dir);
        List<Synapse> ret = new LinkedList<>();
        Iterator<Synapse> it = synapses.iterator();
        while (it.hasNext()){
            Synapse s = it.next();
            if (sensorID.equals(s.prevSynNeuID.get())){
                it.remove();
                ret.add(s);
            }
        }
        return new ArrayList<>(ret);
    }

    /**
     * @param ty   sensor type
     * @param dir  sensor direction
     * @param post post-sensor neuron ID
     * @return deleted synapse, null if not found
     */
    public Synapse removeSensorSynapse(SensorType ty, Direction dir, String post){
        checkSensorDirection(ty, dir);
        Objects.requireNonNull(post);
        String sensorID = CircuitInfo.resolveSensorNeuronID(ty, dir);
        Iterator<Synapse> it = synapses.iterator();
        while (it.hasNext()){
            Synapse s = it.next();
            if (sensorID.equals(s.prevSynNeuID.get()) && post.equals(s.postSynNeuID.get())){
                it.remove();
                return s;
            }
        }
        return null;
    }

    public Synapse removeSensorSynapse(SensorType ty, Direction dir, CentralNeuron post){
        return removeSensorSynapse(ty, dir, post.ID.get());
    }

    /**
     * @param dir motor direction
     */
    public Synapse removeMotorSynapse(Direction dir){
        String motorID = CircuitInfo.resolveMotorNeuronID(dir);
        Iterator<Synapse> it = synapses.iterator();
        while (it.hasNext()){
            Synapse s = it.next();
            if (motorID.equals(s.prevSynNeuID.get())){
                it.remove();
                return s;
            }
        }
        return null;
    }

    @Override
    public Sensor sensor(SensorType type){
        switch (type){
        case FOOD:
            return foodSensor;
        case TOXICANT:
            return toxicantSensor;
        case NPY:
            return npySensor;
        default:
            throw new IllegalArgumentException("type not support : " + type);
        }
    }

    @Override
    public Motor motor(){
        return motor;
    }

    @Override
    public Set<String> getBoxNameSet(){
        return neurons.values().stream()
          .map(n -> n.metaBox.get())
          .filter(name -> name != null)
          .distinct()
          .collect(Collectors.toSet());
    }

    @Override
    public PositionData getBoxMeta(String name){
        return boxMeta.computeIfAbsent(name, _ignore -> new PositionData());
    }

    @Override
    public Set<String> getBoxNeuron(String name){
        Objects.requireNonNull(name, "box name");
        return neurons.entrySet().stream()
          .filter(e -> name.equals(e.getValue().metaBox.get()))
          .map(Map.Entry::getKey)
          .collect(Collectors.toSet());
    }

    @Override
    public Set<CentralNeuron> getNeuronInBox(String name){
        Objects.requireNonNull(name, "box name");
        return neurons.entrySet().stream()
          .filter(e -> name.equals(e.getValue().metaBox.get()))
          .map(Map.Entry::getValue)
          .collect(Collectors.toSet());
    }

    @Override
    public Set<Receptor> getReceptorOnBox(String name){
        Objects.requireNonNull(name, "box name");
        return neurons.entrySet().stream()
          .filter(e -> name.equals(e.getValue().metaBox.get()))
          .map(Map.Entry::getKey)
          .flatMap(n -> getReceptor(n).stream())
          .filter(r -> r.metaBoxRep.get())
          .collect(Collectors.toSet());
    }

    @Override
    public Set<CentralNeuron> getAxonOnBox(String name){
        Objects.requireNonNull(name, "box name");
        return neurons.entrySet().stream()
          .filter(e -> name.equals(e.getValue().metaBox.get()))
          .map(Map.Entry::getValue)
          .filter(n -> n.metaBoxAxon.get())
          .collect(Collectors.toSet());
    }

    public void joinBox(String neuronID, String boxName){
        getBoxMeta(boxName);
        getNeuron(neuronID).metaBox.set(boxName);
    }

    public void joinBox(CentralNeuron neuron, String boxName){
        joinBox(neuron.ID.get(), boxName);
    }

    public void kickBox(String neuronID){
        CentralNeuron n = getNeuron(neuronID);
        n.metaBox.set(null);
        n.metaBoxAxon.set(false);
        for (Receptor r : getReceptor(neuronID)){
            r.metaBoxRep.set(false);
        }
    }

    public void kickBox(CentralNeuron neuron){
        kickBox(neuron.ID.get());
    }

    public void renameBox(String oldName, String newName){
        if (!boxMeta.containsKey(Objects.requireNonNull(oldName, "old box name"))){
            throw new RuntimeException("Box " + oldName + " not found");
        }
        if (boxMeta.containsKey(Objects.requireNonNull(newName, "new box name"))){
            throw new RuntimeException("Box " + newName + " is in-used");
        }
        boxMeta.put(newName, boxMeta.remove(oldName));
        neurons.values().stream()
          .filter(n -> oldName.equals(n.metaBox.get()))
          .forEach(n -> n.metaBox.set(newName));
    }

    public void removeBox(String name){
        boxMeta.remove(Objects.requireNonNull(name, "box name"));
        neurons.values().stream()
          .filter(n -> name.equals(n.metaBox.get()))
          .peek(n -> {
              n.metaBox.set(null);
              n.metaBoxAxon.set(false);
          }).flatMap(n -> getReceptor(n).stream())
          .forEach(r -> r.metaBoxRep.set(false));
    }

    public boolean setBoxAxon(String neuronID, boolean set){
        CentralNeuron neuron = getNeuron(neuronID);
        if (neuron.metaBox != null){
            neuron.metaBoxAxon.set(set);
            return true;
        }
        return false;
    }

    public boolean setBoxAxon(CentralNeuron neuron, boolean set){
        return setBoxAxon(neuron.ID.get(), set);
    }

    public boolean setBoxReceptor(String neuronID, String rep, boolean set){
        if (getNeuron(neuronID).metaBox != null){
            Receptor receptor = getReceptor(neuronID, rep);
            if (receptor != null){
                receptor.metaBoxRep.set(set);
                return true;
            }
        }
        return false;
    }

    public boolean setBoxReceptor(Receptor rep, boolean set){
        return setBoxReceptor(rep.hostNeuID.get(), rep.ID.get(), set);
    }

    @Override
    public Circuit subCircuit(Set<String> neuronSet){
        CircuitBuilder builder = new CircuitBuilder();
        Map<String, String> map = new HashMap<>();
        for (String sourceNeuronName : neuronSet){
            CentralNeuron sourceNeuron = neurons.get(sourceNeuronName);
            assert sourceNeuronName.equals(sourceNeuron.ID.get());
            CentralNeuron targetNeuron = builder.createNeuron(sourceNeuronName, sourceNeuron);
            //
            String targetNeuronName = targetNeuron.ID.get();
            map.put(sourceNeuronName, targetNeuronName);
            // receptor
            for (Receptor sourceReceptor : receptors.getOrDefault(sourceNeuronName, Collections.emptyList())){
                builder.createReceptor(targetNeuronName, sourceReceptor.ID.get(), sourceReceptor);
            }
            // box
            String box = sourceNeuron.metaBox.get();
            if (box != null){
                builder.getBoxMeta(box).setLocation(getBoxMeta(box));
            }
        }
        for (Synapse sourceSynapse : getSynapseBetween(neuronSet)){
            String prev = sourceSynapse.prevSynNeuID.get();
            String post = sourceSynapse.postSynNeuID.get();
            builder.createSynapse(map.get(prev),
                                  map.get(post),
                                  sourceSynapse.targetRepID.get(),
                                  sourceSynapse);
        }
        return builder;
    }

    public Set<String> cloneBox(String boxName, String targetBoxName){
        if (!boxMeta.containsKey(Objects.requireNonNull(boxName, "box name"))){
            throw new RuntimeException("Box " + boxName + " not found");
        }
        if (boxMeta.containsKey(Objects.requireNonNull(targetBoxName, "target box name"))){
            throw new RuntimeException("Box " + targetBoxName + " is in-used");
        }
        boxMeta.put(targetBoxName, new PositionData(boxMeta.get(boxName)));
        //
        Set<CentralNeuron> source = getNeuronInBox(boxName);
        Map<String, String> map = new HashMap<>();
        //
        for (CentralNeuron neuron : source){
            CentralNeuron n = cloneNeuron(neuron);
            //
            String oID = neuron.ID.get();
            String nID = n.ID.get();
            //
            map.put(oID, nID);
            // meta
            n.metaBox.set(targetBoxName);
            n.metaBoxAxon.set(neuron.metaBoxAxon.get());
            // receptor
            for (Receptor r : receptors.getOrDefault(nID, Collections.emptyList())){
                Receptor receptor = getReceptor(oID, r.ID.get());
                if (receptor != null){
                    r.metaBoxRep.set(receptor.metaBoxRep.get());
                }
            }
        }
        // synapse
        for (Synapse synapse : getSynapseRelate(map.keySet())){
            String prev = synapse.prevSynNeuID.get();
            String post = synapse.postSynNeuID.get();
            createSynapse(map.getOrDefault(prev, prev),
                          map.getOrDefault(post, post),
                          synapse.targetRepID.get(),
                          synapse);
        }
        //
        HashSet<String> set = new HashSet<>(map.values());
        if (set.size() != map.size()) throw new RuntimeException("ID duplicate");
        return set;
    }

    public CentralNeuron cloneNeuron(String refNeuronID){
        Objects.requireNonNull(refNeuronID);
        CentralNeuron ref = getNeuron(refNeuronID);
        CentralNeuron n = createNeuron(ref);
        //
        String tID = n.ID.get();
        //
        for (Receptor receptor : getReceptor(refNeuronID)){
            createReceptor(tID, receptor.ID.get(), receptor);
        }
        //
        for (Synapse synapse : getSynapseRelate(tID)){
            String prev = synapse.prevSynNeuID.get();
            String post = synapse.postSynNeuID.get();
            createSynapse(prev.equals(refNeuronID)? tID: prev,
                          post.equals(refNeuronID)? tID: post,
                          synapse.targetRepID.get(),
                          synapse);
        }
        //
        return n;
    }

    public CentralNeuron cloneNeuron(CentralNeuron ref){
        return cloneNeuron(ref.ID.get());
    }

    public Set<String> cloneNeurons(Set<String> neurons){
        Map<String, String> map = new HashMap<>();
        //
        for (String sID : neurons){
            CentralNeuron targetNeuron = createNeuron(getNeuron(sID));
            //
            String tID = targetNeuron.ID.get();
            map.put(sID, tID);
            //
            for (Receptor receptor : getReceptor(sID)){
                createReceptor(tID, receptor.ID.get(), receptor);
            }
            // remove box information
            targetNeuron.metaBox.set(null);
        }
        //
        for (Synapse synapse : getSynapseRelate(neurons)){
            String prev = synapse.prevSynNeuID.get();
            String post = synapse.postSynNeuID.get();
            createSynapse(map.getOrDefault(prev, prev),
                          map.getOrDefault(post, post),
                          synapse.targetRepID.get(),
                          synapse);
        }
        //
        HashSet<String> set = new HashSet<>(map.values());
        if (set.size() != map.size()) throw new RuntimeException("ID duplicate");
        return set;
    }

    public Set<String> cloneCircuit(Circuit circuit /*it is a module circuit*/){
        Map<String, String> map = new HashMap<>();
        String suffix = "-" + genNeuronID();
        // neuron
        for (CentralNeuron sourceNeuron : circuit.neurons()){
            CentralNeuron targetNeuron = createNeuron();
            targetNeuron.set(sourceNeuron);
            //
            String sID = sourceNeuron.ID.get();
            String tID = targetNeuron.ID.get();
            //
            map.put(sID, tID);
            // receptor
            for (Receptor receptor : circuit.getReceptor(sID)){
                createReceptor(tID, receptor.ID.get(), receptor);
            }
            // box
            String sourceBox = sourceNeuron.metaBox.get();
            if (sourceBox != null){
                String targetBox = sourceBox + suffix;
                targetNeuron.metaBox.set(targetBox);
                getBoxMeta(targetBox).setLocation(circuit.getBoxMeta(sourceBox));
            }
        }
        // synapse
        for (Synapse synapse : circuit.synapses()){
            String prev = synapse.prevSynNeuID.get();
            String post = synapse.postSynNeuID.get();
            if (map.containsKey(prev) && map.containsKey(post)){
                createSynapse(map.get(prev), map.get(post), synapse.targetRepID.get(), synapse);
            }
        }
        //
        HashSet<String> set = new HashSet<>(map.values());
        if (set.size() != map.size()) throw new RuntimeException("ID duplicate");
        return set;
    }
}
