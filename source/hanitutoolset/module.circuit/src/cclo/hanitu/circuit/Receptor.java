/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cclo.hanitu.circuit;

import java.util.Objects;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

import cclo.hanitu.data.DataClass;
import cclo.hanitu.data.EventProperty;

/**
 * receptor.
 *
 * @author antonio
 */
public class Receptor implements DataClass{

    /**
     * the neuron owner.
     */
    public final ReadOnlyStringProperty hostNeuID = new SimpleStringProperty(this, "neuron");
    public final ReadOnlyStringProperty ID = new SimpleStringProperty(this, "id");
    /**
     * the time constant in ms, also known as `tau`
     */
    public final SimpleDoubleProperty timeConstant
      = new SimpleDoubleProperty(this, "timeconstant", 20);
    /**
     * the reversal potential in mV, also known as `RRevP`
     */
    public final SimpleDoubleProperty reversalPotential
      = new SimpleDoubleProperty(this, "reversalpotential", 0);

    public static final String META_BOX_REP = "box_rep";
    public final SimpleBooleanProperty metaBoxRep = new SimpleBooleanProperty(this, "meta.box_rep");

    @SuppressWarnings("unused")
    public final EventProperty resetDefExt = new EventProperty(this, "action.resetext",
                                                               () -> reversalPotential.set(0));
    @SuppressWarnings("unused")
    public final EventProperty resetDefInh = new EventProperty(this, "action.resetinh",
                                                               () -> reversalPotential.set(-70));

    public Receptor(ReadOnlyStringProperty hostNeuID, String ID, Receptor r){
        ((SimpleStringProperty)this.hostNeuID).bind(hostNeuID);
        ((SimpleStringProperty)this.ID).set(Objects.requireNonNull(ID, "receptor ID"));
        if (r != null){
            timeConstant.set(r.timeConstant.get());
            reversalPotential.set(r.reversalPotential.get());
            metaBoxRep.set(r.metaBoxRep.get());
        }
    }

    /**
     * copy value from reference
     *
     * @param r reference receptor
     */
    @SuppressWarnings("unused")
    public void set(Receptor r){
        Objects.requireNonNull(r);
        timeConstant.set(r.timeConstant.get());
        reversalPotential.set(r.reversalPotential.get());
        //
        metaBoxRep.set(r.metaBoxRep.get());
    }

    @Override
    public String toString(){
        return "Receptor:" + Objects.toString(hostNeuID.get(), "?") + "[" + ID.get() + "]";
    }
}
