/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.util.Objects;

import javafx.beans.property.*;

import cclo.hanitu.data.DataClass;
import cclo.hanitu.data.Direction;
import cclo.hanitu.data.EventProperty;

/**
 * Neuron, the master role in the circuit, contain several receptors and synapses.
 * Unlike {@link BodyNeuron}, this type neuron doesn't has the information of the {@link Direction}.
 *
 * @author antonio
 */
public class CentralNeuron implements DataClass{

    public final ReadOnlyStringProperty ID = new SimpleStringProperty(this, "id");

    /**
     * refractory period in 0.1 ms.
     */
    public final SimpleIntegerProperty refractoryPeriod = new SimpleIntegerProperty(this, "refractoryperiod", 20);
    /**
     * the delay after a spike in 0.1 ms
     */
    public final SimpleIntegerProperty spikeDelay = new SimpleIntegerProperty(this, "spikedelay", 18);
    /**
     * conductance, also known as `G`
     */
    public final SimpleDoubleProperty conductance = new SimpleDoubleProperty(this, "conductance", 25);
    /**
     * capacitance, also known as `C`
     */
    public final SimpleDoubleProperty capacitance = new SimpleDoubleProperty(this, "capacitance", 0.5);
    /**
     * the reversal potential in mV, also known as `NRevP`.
     */
    public final SimpleDoubleProperty reversalPotential = new SimpleDoubleProperty(this, "reversalpotential", -70);
    /**
     */
    public final SimpleDoubleProperty resetPotential = new SimpleDoubleProperty(this, "resetpotential", -55);
    /**
     * the spike threshold.
     */
    public final SimpleDoubleProperty threshold = new SimpleDoubleProperty(this, "threshold", -50);
    /**
     * the standard deviation of the noise in mV.
     */
    public final SimpleDoubleProperty noiseStd = new SimpleDoubleProperty(this, "noisestd", 0);
    /**
     * the mean of the membrane potential noise in mV.
     */
    public final SimpleDoubleProperty noiseMean = new SimpleDoubleProperty(this, "noisemean", 0);

    public static final String META_X = "x";
    public static final String META_Y = "y";
    public static final String META_THETA = "t";
    public static final String META_BOX = "box";
    public static final String META_BOX_AXON = "box_axon";

    public final SimpleIntegerProperty metaX = new SimpleIntegerProperty(this, "meta.x", 0);
    public final SimpleIntegerProperty metaY = new SimpleIntegerProperty(this, "meta.y", 0);
    public final SimpleDoubleProperty metaTheta = new SimpleDoubleProperty(this, "meta.theta", -Math.PI / 2);
    public final SimpleStringProperty metaBox = new SimpleStringProperty(this, "meta.box");
    public final SimpleBooleanProperty metaBoxAxon = new SimpleBooleanProperty(this, "meta.box_axon");

    public final EventProperty resetDefault = new EventProperty(this, "action.reset", () -> {
        CentralNeuron neuron = new CentralNeuron("", null);
        refractoryPeriod.set(neuron.refractoryPeriod.get());
        spikeDelay.set(neuron.spikeDelay.get());
        conductance.set(neuron.conductance.get());
        capacitance.set(neuron.capacitance.get());
        reversalPotential.set(neuron.reversalPotential.get());
        resetPotential.set(neuron.resetPotential.get());
        threshold.set(neuron.threshold.get());
        noiseStd.set(neuron.noiseStd.get());
        noiseMean.set(neuron.noiseMean.get());
    });

    /**
     * create a neuron with value copy from reference
     *
     * @param ID     identify number
     * @param neuron reference neuron
     */
    public CentralNeuron(String ID, CentralNeuron neuron){
        ((SimpleStringProperty)this.ID).set(Objects.requireNonNull(ID, "neuron ID"));
        if (neuron != null){
            refractoryPeriod.set(neuron.refractoryPeriod.get());
            spikeDelay.set(neuron.spikeDelay.get());
            conductance.set(neuron.conductance.get());
            capacitance.set(neuron.capacitance.get());
            reversalPotential.set(neuron.reversalPotential.get());
            resetPotential.set(neuron.resetPotential.get());
            threshold.set(neuron.threshold.get());
            noiseStd.set(neuron.noiseStd.get());
            noiseMean.set(neuron.noiseMean.get());
            //
            metaX.set(neuron.metaX.get());
            metaY.set(neuron.metaY.get());
            metaTheta.set(neuron.metaTheta.get());
            metaBox.set(neuron.metaBox.get());
            metaBoxAxon.set(neuron.metaBoxAxon.get());
        }
    }

    /**
     * copy value from reference
     *
     * @param neuron reference neuron
     */
    public void set(CentralNeuron neuron){
        Objects.requireNonNull(neuron, "reference neuron");
        refractoryPeriod.set(neuron.refractoryPeriod.get());
        spikeDelay.set(neuron.spikeDelay.get());
        conductance.set(neuron.conductance.get());
        capacitance.set(neuron.capacitance.get());
        reversalPotential.set(neuron.reversalPotential.get());
        resetPotential.set(neuron.resetPotential.get());
        threshold.set(neuron.threshold.get());
        noiseStd.set(neuron.noiseStd.get());
        noiseMean.set(neuron.noiseMean.get());
        //
        metaX.set(neuron.metaX.get());
        metaY.set(neuron.metaY.get());
        metaTheta.set(neuron.metaTheta.get());
        metaBox.set(neuron.metaBox.get());
        metaBoxAxon.set(neuron.metaBoxAxon.get());
    }

    @Override
    public String toString(){
        return "Neuron:" + ID.get();
    }
}
