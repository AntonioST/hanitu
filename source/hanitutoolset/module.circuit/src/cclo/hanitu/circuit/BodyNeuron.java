/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit;

import java.util.Objects;
import java.util.Properties;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

import cclo.hanitu.Base;
import cclo.hanitu.data.DataClass;
import cclo.hanitu.data.Direction;

/**
 * The neuron on the body, like sensor and motor, do the character of input/output of the circuit.
 * This type neuron have the information of the {@link Direction}. However, all of their parameters
 * are same, except the synapse connections.
 *
 * @author antonio
 */
public class BodyNeuron implements DataClass{

    /**
     * refractory period in 0.1 ms.
     */
    public final SimpleIntegerProperty refractoryPeriod = new SimpleIntegerProperty(this, "refractoryperiod", 20);
    /**
     * capacitance in nF, also known as `C`
     */
    public final SimpleDoubleProperty capacitance = new SimpleDoubleProperty(this, "capacitance", 0.5);
    /**
     * time constant in ms
     */
    public final SimpleDoubleProperty timeConstant = new SimpleDoubleProperty(this, "timeconstant", 24);
    /**
     * the reversal potential in mV, also known as NRevP.
     */
    public final SimpleDoubleProperty reversalPotential = new SimpleDoubleProperty(this, "reversalpotential", -70);
    /**
     * unit mV
     */
    public final SimpleDoubleProperty resetPotential = new SimpleDoubleProperty(this, "resetpotential", -55);
    /**
     * the spike threshold in mV.
     */
    public final SimpleDoubleProperty threshold = new SimpleDoubleProperty(this, "threshold", -50);
    /**
     * body synaptic weight.
     */
    public final SimpleDoubleProperty weight = new SimpleDoubleProperty(this, "weight", 1);

    /**
     * create a neuron with (program) default value
     */
    @SuppressWarnings("WeakerAccess")
    protected BodyNeuron(){
    }

    /**
     * create a neuron with value copy from reference
     *
     * @param neuron reference neuron
     */
    @SuppressWarnings("WeakerAccess")
    protected BodyNeuron(BodyNeuron neuron){
        if (neuron != null){
            refractoryPeriod.set(neuron.refractoryPeriod.get());
            capacitance.set(neuron.capacitance.get());
            timeConstant.set(neuron.timeConstant.get());
            reversalPotential.set(neuron.reversalPotential.get());
            resetPotential.set(neuron.resetPotential.get());
            threshold.set(neuron.threshold.get());
            weight.set(neuron.weight.get());
        }
    }

    /**
     * copy value from reference
     *
     * @param neuron reference neuron
     */
    public void set(BodyNeuron neuron){
        Objects.requireNonNull(neuron);
        refractoryPeriod.set(neuron.refractoryPeriod.get());
        capacitance.set(neuron.capacitance.get());
        timeConstant.set(neuron.timeConstant.get());
        reversalPotential.set(neuron.reversalPotential.get());
        resetPotential.set(neuron.resetPotential.get());
        threshold.set(neuron.threshold.get());
        weight.set(neuron.weight.get());
    }

    @Override
    public Properties getProperty(){
        return Base.loadProperties(CentralNeuron.class);
    }
}
