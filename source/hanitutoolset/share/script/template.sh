#! /bin/bash

# 位於 '#' 後面的文字是註解，內容與程式執行無關
# (注意！)刪掉也沒關係，除了第一行 '#!' ，這有特殊用意

## 程式說明 ##
# (該script為範本檔案，建議複製一份後修改裡面的內容)
# 結合兩個Hanitu Tool Set Script, filter.sh and firingrate.sh
# 給程式新手使用的script，幾本上只要在Ununtu File Manager下
# 滑鼠點擊兩下就可以執行 routine 的工作

## 使用方式 ##
# ./template.sh     : 無參數執行，全部使用預設值
# ./template.sh [SPIKE] [NEURON [TYPE]] 
#                   : 從 SPIKE 讀取data並且過濾
#                     SPIKE 可為任意檔案，但必須以txt結尾作為辨識

#[Note] 參數說明中，使用 '[', ']' 括住的內容表示為可有可無的參數
#  若是表示成 '[a [b]]'，意指要不是全部沒有，或是只有一個a，亦或a b同時出現
#  不能只出現b (不能與a無法區別)
#  另外，a b順序不可調換

#[Note] 參數說明中，小寫表示常量(不可變動)，大寫表示變數
#  使用時，小寫部份需一模一樣，大寫部份需依需求而改變

#[Note] Linux 終端環境(terminal)下，執行某目錄下某個可執行檔案，鍵入方式為：
#  ./檔案名稱 [參數...]
#  其中 '.' 表是當下目錄，父目錄為 '..' 

#[Note] 檢視該檔案可否執行，終端下鍵入：
#  ls -l 檔案名稱
#  並檢視系統輸出，一般情況下輸出(意義)如下：
#  -rwxrwxr-x 1 使用者 群組 大小 修改日期 檔案
#  檢查最前面的權限使否有 'x' ，其代表可否執行；三個不同位置的 'x' 代表 '誰' 可以執行該檔案
#  若權限顯示如右 'lrwxrwxrwx' (第一個字母為 'l', 小寫L)，表示一個連結(symbolic link)
#  其類似Windows下的捷徑，代表真正的檔案放在其他地方，可改為輸入： 
#  ls -l `readlink -f 檔案名稱`         #('`' 是鍵盤左上角 '~' 那個鍵)
#  愈修改權限，設為 '所有人' '可執行' 該檔案，鍵入
#  chmod a+x 檔案名稱
#  欲知詳細
#  http://en.wikipedia.org/wiki/File_system_permissions#Notation_of_traditional_Unix_permissions

#[Tip] 查詢Linux相關命令，鍵入
#  man 命令

## 正文開始 ##

## 輸出入設定 ##
# 預設輸入spike file
SPIKE_FILE=Spike.txt
# 預設輸出檔案前綴
OUTPUT_FILE_PREFIX=fr_

## filter.sh 預設參數 ##
USER=0
WORM=0
NEURON=0
TYPE=b

## firingrate.sh 預設參數 ##
WINDOW=100
STEP=20
SCALAR=uniform

## 本script命令列參數解析 ##
if [ $# -ge 1 ]; then
    if expr $1 : '.\+\.txt'; then
        SPIKE_FILE=$1
        shift
    fi
fi

if [ $# -ge 1 ]; then
    NEURON=$1
fi

if [ $# -ge 2 ]; then
    TYPE=$2
fi

## spike file檢查 ##
if [ ! -f "$SPIKE_FILE" ]; then
    >&2 echo "file doesn't exist : $SPIKE_FILE"
    exit 1
fi

## 執行 ##
# filter.sh 命令
FILTER_CMD="hanitu_filter -u $USER -w $WORM -n $NEURON -t $TYPE"
# firingrate.sh 命令
FIRINGRATE_CMD="hanitu_firingrate -w $WINDOW -s $STEP --scalar=$SCALAR"
# 輸出檔案
OUTPUT_FILE="${OUTPUT_FILE_PREFIX}${NEURON}${TYPE}.txt"
# 執行，其下擇一執行
#cat $SPIKE_FILE | $FILTER_CMD | $FIRINGRATE_CMD
cat $SPIKE_FILE | $FILTER_CMD | $FIRINGRATE_CMD > $OUTPUT_FILE
