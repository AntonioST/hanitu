/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.util.Objects;

import cclo.hanitu.circuit.Synapse;

/**
 * the viewer for synapse.
 *
 * @author antonio
 */
public class SynapseViewer extends Viewer{

    final Synapse synapse;
    //
    final AxonViewer prevSynAxon;
    final ReceptorViewer postSynRep;
    //
//    PositionData prev;
//    PositionData post;
    double effectWeightCache;
    boolean visibleCache;

    public SynapseViewer(Synapse synapse, AxonViewer prev, ReceptorViewer post){
        Objects.requireNonNull(prev, "prev-synapse axon");
        Objects.requireNonNull(post, "post-synapse receptor");
        this.synapse = synapse;
        prevSynAxon = prev;
        postSynRep = post;
    }

    @Override
    public String getText(){
        return "Synapse:" + prevSynAxon.getText() + "->" + postSynRep.getText();
    }

    @Override
    public boolean isRelative(Viewer other){
        return other == postSynRep ||
               other == prevSynAxon ||
               other == postSynRep.host ||
               other == prevSynAxon.host;
    }

    @Deprecated
    public boolean isLocalSynapse(){
        return synapse.isLocalSynapse();
    }

    @Deprecated
    public boolean isNonLocalSynapse(){
        return synapse.isNonLocalSynapse();
    }

    @Deprecated
    public boolean isCommunicateSynapse(){
        return synapse.isCommunicateSynapse();
    }
}
