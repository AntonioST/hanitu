/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javafx.beans.property.ReadOnlyProperty;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.Effect;
import javafx.scene.effect.Shadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

import cclo.hanitu.circuit.Synapse;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.gui.ColorProperty;


/**
 * basic implement the way (shape and location, no color) to painting the circuit component.
 *
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class DefaultCircuitPainter extends AbstractCircuitPainter{

    private double maxSynapseWeight = 1;
    private final Effect boxComponentEffect = new Shadow();
    // cache
    boolean bodySizeChange;
    int viewSize;
    int bodySize;
    int boxViewSize;
    int boxSize;
    int halfBodySize;
    int anchor = 100;
    int neuronSize;
    int neuronRadius;
    int axonSize;
    int receptorSize;
    double synapseLinewidth;
    double synapseMaxLinewidth;
    Map<String, BoxViewer> boxViewers;
    Map<String, NeuronViewer> neuronViewers;
    List<ReceptorViewer<NeuronViewer>> receptorViewers;
    List<SynapseViewer> synapseViewers;
    SensorViewer[] sensorViewer;
    MotorViewer[] motorViewer;

    @Override
    public void setup(VisualCircuit visual){
        super.setup(visual);
        prepare();
        bodySizeChange = true; // reset flag
    }

    protected double getSynapseStroke(SynapseViewer s){
        return Math.max(synapseMaxLinewidth * s.effectWeightCache / maxSynapseWeight, 1);
    }

    @Override
    public void paint(GraphicsContext g){
        prepare();
        g.translate(anchor, anchor);
        //noinspection EmptyCatchBlock
        try {
            if (visual.selectedBox == null){
                paintEnvironment(g);
                paintWholeCircuit(g);
            } else {
                prepareForBoxCircuit(visual.selectedBox);
                paintBoxCircuit(g, visual.selectedBox);
            }
            Rectangle rectangle = visual.getSelectedRectangle();
            if (rectangle != null){
                paintSelectedRectangle(g, rectangle);
            }
            if (visual.waitUserMouseClickData != null){
                TextViewer text = visual.waitUserMouseClickData;
                Font f = env.messageFont.get();
                g.setFont(f);
                g.setFill(env.messageTextColor.get());
                g.fillText(text.text, text.x, text.y);
            }
        } catch (ConcurrentModificationException e){
        } finally {
            g.translate(-anchor, -anchor);
        }
    }

    private void paintSelectedRectangle(GraphicsContext g, Rectangle rect){
        g.setFill(env.selectRectangleColor.get());
        g.fillRect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
    }

    @Override
    public double getXInArea(double x){
        return super.getXInArea(x) - anchor;
    }

    @Override
    public double getYInArea(double y){
        return super.getYInArea(y) - anchor;
    }

    protected void prepare(){
        viewSize = env.viewSize.get();
        boxViewSize = env.boxViewSize.get();
        //
        // Avoid calculate same thing (the location of the body neurons)
        // Use a flag to check the change of te body size. if it change, update the location
        int newBodySize = env.bodySize.get();
        bodySizeChange = bodySizeChange || bodySize != newBodySize;
        bodySize = newBodySize;
        //
        boxSize = env.boxSize.get();
        halfBodySize = bodySize >> 1;
        neuronSize = env.neuronSize.get();
        neuronRadius = env.neuronRadius.get();
        axonSize = env.axonSize.get();
        receptorSize = env.receptorSize.get();
        synapseLinewidth = env.synapseLinewidth.get();
        synapseMaxLinewidth = env.synapseMaxLinewidth.get();
        boxViewers = visual.boxViewers;
        neuronViewers = visual.neuronViewers;
        receptorViewers = visual.receptorViewers;
        synapseViewers = visual.synapseViewers;
        sensorViewer = visual.sensorViewer;
        motorViewer = visual.motorViewer;
        //
        //noinspection EmptyCatchBlock
        try {
            maxSynapseWeight = synapseViewers.stream()
              .mapToDouble(s -> {
                  Synapse syn = s.synapse;
                  s.effectWeightCache = syn == null? 0: syn.conductance.get() * syn.weight.get();
                  return s.effectWeightCache;
              }).max().orElse(0);
        } catch (ConcurrentModificationException e){
        }
        if (maxSynapseWeight <= 0) maxSynapseWeight = 1;
    }

    @SuppressWarnings("EmptyMethod")
    protected void prepareForBoxCircuit(@SuppressWarnings("UnusedParameters") BoxViewer v){
    }

    protected void paintEnvironment(GraphicsContext g){
    }

    protected void paintWholeCircuit(GraphicsContext g){
        int nr = neuronRadius;
        // worm body
        paintWormBody(g);
        // body neuron
        if (bodySizeChange){
            int sb = halfBodySize;
            int sn = neuronSize;
            int sb2 = bodySize;
            setSensor(sensorViewer[0], sb + sn, 0, nr);
            setSensor(sensorViewer[1], sb - sn, sb2, nr);
            setSensor(sensorViewer[2], 0, sb - sn, nr);
            setSensor(sensorViewer[3], sb2, sb + sn, nr);
            setSensor(sensorViewer[4], sb - sn, 0, nr);
            setSensor(sensorViewer[5], sb + sn, sb2, nr);
            setSensor(sensorViewer[6], 0, sb + sn, nr);
            setSensor(sensorViewer[7], sb2, sb - sn, nr);
            setMotor(motorViewer[0], sb, -sn, nr);
            setMotor(motorViewer[1], sb, sb2 + sn, nr);
            setMotor(motorViewer[2], -sn, sb, nr);
            setMotor(motorViewer[3], sb2 + sn, sb, nr);
            sensorViewer[8].axon.visibleCache = true;
        }
        // npy neuron
        setLocationForComponent(sensorViewer[8], 0, 1, sensorViewer[8].axon, nr);
        // body neuron sensor
        for (int i = 0; i < 9; i++){
            SensorViewer target = sensorViewer[i];
            paintSensor(g, target);
            paintAxon(g, target.axon);
        }
        // body neuron motor
        for (int i = 0; i < 4; i++){
            MotorViewer target = motorViewer[i];
            paintMotor(g, target);
            paintReceptor(g, target.rep);
        }
        // Box
        for (BoxViewer box : boxViewers.values()){
            String boxName = box.name;
            List<AxonViewer> aa = visual.getAxonOnBox(boxName).collect(Collectors.toList());
            List<ReceptorViewer> rr = visual.getReceptorOnBox(boxName).collect(Collectors.toList());
            int all = aa.size() + rr.size();
            int radius = boxSize * 2 / 3 + (all / 10) * 10;
            int loc = 0;
            paintBox(g, box);
            for (AxonViewer a : aa){
                a.visibleCache = true;
                setLocationForComponent(box, loc++, all, a, radius);
            }
            for (ReceptorViewer r : rr){
                r.visibleCache = true;
                setLocationForComponent(box, loc++, all, r, radius);
            }
        }
        // central neuron
        for (NeuronViewer n : neuronViewers.values()){
            if (!n.inBox()){
                paintNeuron(g, n);
                n.axon.visibleCache = true;
                setLocationForComponent(n, 0, 1, n.axon, nr);
            }
        }
        // receptor location setting
        for (ReceptorViewer<NeuronViewer> r : receptorViewers){
            if (!r.visibleCache && !r.host.inBox()){
                setLocationForComponent(r.host, r.locationCache + 1, r.host.receptorCountCache + 1, r, nr);
                r.visibleCache = true;
            }
        }
        //synapse line
        for (SynapseViewer s : synapseViewers){
            if (s.visibleCache = (s.prevSynAxon.visibleCache || s.postSynRep.visibleCache)){
                if (!s.prevSynAxon.visibleCache){
                    Viewer prev = s.prevSynAxon.host;
                    visual.getBelongBox(prev).ifPresent(s.prevSynAxon::setLocation);
                } else if (!s.postSynRep.visibleCache){
                    Viewer post = s.postSynRep.host;
                    visual.getBelongBox(post).ifPresent(s.postSynRep::setLocation);
                }
                paintSynapseLine(g, s);

                if (isSelectedOrRelative(s)){
                    //only those synapse which are relative to selected viewer will display the weight text
                    paintSynapseText(g, s);
                }
            }
        }
        // receptor painting
        for (ReceptorViewer<NeuronViewer> r : receptorViewers){
            if (r.visibleCache){
                paintReceptor(g, r);
            }
            if (!r.host.inBox() && isSelectedOrRelative(r)){
                paintReceptorText(g, r);
            }
            r.visibleCache = false;
        }
        // axon
        for (NeuronViewer n : neuronViewers.values()){
            if (n.axon.visibleCache){
                paintAxon(g, n.axon);
            }
            n.axon.visibleCache = false;
        }
    }

    private static void setSensor(SensorViewer v, int x, int y, int r){
        v.setLocation(x, y);
        setLocationForComponent(v, 0, 1, v.axon, r);
        v.axon.visibleCache = true;
    }

    private static void setMotor(MotorViewer v, int x, int y, int r){
        v.setLocation(x, y);
        setLocationForComponent(v, 0, 1, v.rep, r);
        v.rep.visibleCache = true;
    }

    protected void paintBoxCircuit(GraphicsContext g, BoxViewer box){
        int nr = neuronRadius;
        String name = box.name;
        // box
        paintBoxView(g);
        // body synapse
        for (int i = 0; i < 9; i++){
            sensorViewer[i].axon.visibleCache = false;
        }
        for (int i = 0; i < 4; i++){
            motorViewer[i].rep.visibleCache = false;
        }
        // neuron axon location setting
        for (NeuronViewer n : neuronViewers.values()){
            if (n.inBox(name)){
                paintNeuron(g, n);
                setLocationForComponent(n, 0, 1, n.axon, nr);
                n.axon.visibleCache = true;
            } else {
                n.axon.visibleCache = false;
            }
        }
        // receptor location setting
        for (ReceptorViewer<NeuronViewer> r : receptorViewers){
            if (r.host.inBox(name)){
                setLocationForComponent(r.host, r.locationCache + 1, r.host.receptorCountCache + 1, r, nr);
                r.visibleCache = true;
            } else {
                r.visibleCache = false;
            }
        }
        //synapse line
        for (SynapseViewer s : synapseViewers){
            s.visibleCache = false;
            if (s.prevSynAxon.visibleCache || s.postSynRep.visibleCache){
                if (!s.prevSynAxon.visibleCache){
                    Viewer prev = s.prevSynAxon.host;
                    setLocationBesideBox(s.prevSynAxon, box, prev);
                    paintNeuronLike(g, prev, s.prevSynAxon);
                } else if (!s.postSynRep.visibleCache){
                    Viewer post = s.postSynRep.host;
                    setLocationBesideBox(s.postSynRep, box, post);
                    paintNeuronLike(g, post, s.postSynRep);
                } else {
                    s.visibleCache = true;
                }
                paintSynapseLine(g, s);

                if (isSelectedOrRelative(s)){
                    //only those synapse which are relative to selected viewer will display the weight text
                    paintSynapseText(g, s);
                }
            }
        }
        // receptor
        for (ReceptorViewer<NeuronViewer> r : receptorViewers){
            if (r.visibleCache){
                if (r.receptor.metaBoxRep.get()){
                    g.setEffect(boxComponentEffect);
                    paintReceptor(g, r);
                    g.setEffect(null);
                }
                paintReceptor(g, r);
                if (isSelectedOrRelative(r)){
                    paintReceptorText(g, r);
                }
            }
            r.visibleCache = false;
        }
        // axon
        for (NeuronViewer n : neuronViewers.values()){
            if (n.axon.visibleCache){
                if (n.isItsAxonABoxAxon()){
                    g.setEffect(boxComponentEffect);
                    paintAxon(g, n.axon);
                    g.setEffect(null);
                }
                paintAxon(g, n.axon);
            }
            n.axon.visibleCache = false;
        }
    }

    protected void paintWormBody(GraphicsContext g){
        g.setStroke(env.bodyColor.get());
        g.setLineWidth(env.bodyLinewidth.get());
        g.strokeOval(0, 0, bodySize, bodySize);
    }

    protected void paintBoxView(GraphicsContext g){
        g.setStroke(env.boxColor.get());
        g.setLineWidth(env.boxLinewidth.get());
        g.strokeRect(0, 0, bodySize, bodySize);
    }

    protected void paintBox(GraphicsContext g, BoxViewer v){
        g.setStroke(getColor(v, env.boxColor, env.boxHighlightColor));
        g.setLineWidth(get(v, env.boxLinewidth, env.boxHighlightLinewidth));
        drawRect(g, v, boxSize);
        //Text
        g.setFont(env.boxTextFont.get());
        g.setFill(getColor(v, env.boxTextColor.get()));
        int textSize = (int)g.getFont().getSize();
        String text = v.getText();
        g.fillText(text,
                   v.x - textSize * text.length() / 4,
                   v.y + boxSize / 4);
    }

    protected void paintNeuronLike(GraphicsContext g, Viewer neuronLikeViewer, PositionData location){
        neuronLikeViewer.swapLocation(location);
        if (neuronLikeViewer instanceof NeuronViewer){
            paintNeuron(g, (NeuronViewer)neuronLikeViewer);
        } else if (neuronLikeViewer instanceof SensorViewer){
            paintSensor(g, (SensorViewer)neuronLikeViewer);
        } else if (neuronLikeViewer instanceof MotorViewer){
            paintMotor(g, (MotorViewer)neuronLikeViewer);
        }
        neuronLikeViewer.swapLocation(location);
    }

    protected void paintNeuron(GraphicsContext g, NeuronViewer n){
        g.setStroke(getColor(n, env.neuronColor, env.neuronHighlightColor));
        g.setLineWidth(get(n, env.neuronLinewidth, env.neuronHighlightLinewidth));
        drawCircle(g, n, neuronSize);
        paintNeuronText(g, n);
    }

    protected void paintNeuronText(GraphicsContext g, Viewer n){
        paintNeuronText(g, n, n.getText());
    }

    protected void paintNeuronText(GraphicsContext g, Viewer n, String text){
        g.setFont(env.neuronTextFont.get());
        g.setFill(getColor(n, env.neuronTextColor));
        int textSize = (int)g.getFont().getSize();
        g.fillText(text,
                   n.x - textSize * text.length() / 4,
                   n.y + neuronSize / 4);
    }

    protected void paintAxon(GraphicsContext g, AxonViewer a){
        g.setStroke(getColor(a, env.axonColor, env.axonHighlightColor));
        g.setLineWidth(get(a, env.axonLinewidth, env.axonHighlightLinewidth));
        drawCircle(g, a, axonSize);
    }

    protected void paintReceptor(GraphicsContext g, ReceptorViewer r){
        g.setStroke(getColor(r, env.receptorColor, env.receptorHighlightColor));
        g.setLineWidth(get(r, env.receptorLinewidth, env.receptorHighlightLinewidth));
        drawTriangle(g, r, receptorSize);
    }

    protected void paintReceptorText(GraphicsContext g, ReceptorViewer<NeuronViewer> r){
        Viewer empty = new Viewer();
        setLocationForComponent(r.host, r.locationCache + 1, r.host.receptorCountCache + 1, empty, neuronRadius + 12);
        paintReceptorText(g, r, empty);
    }

    protected void paintReceptorText(GraphicsContext g, ReceptorViewer r, Viewer loc){
        g.setFont(get(r, env.receptorTextFont, env.receptorHighlightTextFont));
        g.setFill(getColor(r, env.receptorTextColor));
        double size = g.getFont().getSize();
        g.fillText(r.receptor.ID.get(),
                   loc.x - size / 4,
                   loc.y + size / 4);
    }

    protected void paintSynapseLine(GraphicsContext g, SynapseViewer s){
        g.setStroke(getColor(s,
                             env.synapseColor.get(),
                             env.synapseColor.get().darker(),
                             env.synapseHighlightColor.get()));
        g.setLineWidth(getSynapseStroke(s));
        g.strokeLine(s.prevSynAxon.x, s.prevSynAxon.y, s.postSynRep.x, s.postSynRep.y);
    }

    protected void paintSynapseText(GraphicsContext g, SynapseViewer s){
        g.setFont(get(s, env.synapseTextFont, env.synapseHighlightTextFont));
        g.setFill(getColor(s, env.synapseTextColor));
        double x;
        double y;
        if (isSelected(s)){
            x = (s.prevSynAxon.x + s.postSynRep.x) / 2;
            y = (s.prevSynAxon.y + s.postSynRep.y) / 2;
        } else if (isSelectedOrRelative(s.prevSynAxon.host)){
            x = s.postSynRep.x;
            y = s.postSynRep.y;
        } else {
            x = s.prevSynAxon.x;
            y = s.prevSynAxon.y;
        }
        g.fillText(String.format(env.floatFormat.get(), s.effectWeightCache), x - 25, y);
    }

    protected void paintSensor(GraphicsContext g, SensorViewer s){
        switch (s.sensor.type){
        case FOOD:
            g.setStroke(getColor(s, env.foodSensorColor));
            break;
        case TOXICANT:
            g.setStroke(getColor(s, env.toxicantSensorColor));
            break;
        //XXX other type sensor : somatic
        //XXX other type sensor : light
        case NPY:
            g.setStroke(getColor(s, env.npySensorColor));
            break;
        default:
            return;
        }
        g.setLineWidth(env.neuronLinewidth.get());
        drawCircle(g, s, neuronSize);
        paintNeuronText(g, s, s.sensor.type.ID);
    }

    protected void paintMotor(GraphicsContext g, MotorViewer m){
        g.setLineWidth(env.neuronLinewidth.get());
        g.setStroke(getColor(m, env.motorColor));
        drawCircle(g, m, neuronSize);
        paintNeuronText(g, m, "M");
    }

    @Override
    public double isTouch(Viewer v, double x, double y){
//        System.out.println("" + x + ", " + y + " <> " + v.x + ", " + v.y + " : " + v);
        if (v instanceof NeuronViewer){
            return isTaughtSurround(v, neuronSize, x, y);
        } else if (v instanceof MotorViewer){
            return isTaughtSurround(v, neuronSize, x, y);
        } else if (v instanceof SensorViewer){
            return isTaughtSurround(v, neuronSize, x, y);
        } else if (v instanceof AxonViewer){
            return isTaughtSurround(v, axonSize, x, y);
        } else if (v instanceof ReceptorViewer){
            return isTaughtSurround(v, receptorSize, x, y);
        } else if (v instanceof BoxViewer){
            if (v == visual.selectedBox){
                if (x > 0 && x < bodySize && y > 0 && y < bodySize) return 0;
            } else {
                return isTaughtRectangle(v, boxSize, x, y);
            }
        } else if (v instanceof SynapseViewer){
            SynapseViewer s = (SynapseViewer)v;
            return isTouchLine(s.prevSynAxon, s.postSynRep, x, y);
        }
        return Double.POSITIVE_INFINITY;
    }

    protected Color getColor(Viewer target, Color unSelected, Color relative, Color selected){
        if (target == null) return unSelected;
        if (visual.isDirectSelected(target)){
            return selected;
        } else if (visual.isSelected(target) || visual.isRelativeToSelect(target)){
            return relative;
        } else if (visual.hasSelected()){
            // fade other unselected
            return new Color(unSelected.getRed(), unSelected.getGreen(), unSelected.getBlue(), env.fadeAlpha.get());
        } else {
            return unSelected;
        }
    }

    protected Color getColor(Viewer target, ColorProperty unSelected, ColorProperty relative, ColorProperty selected){
        if (target == null) return unSelected.get();
        if (visual.isDirectSelected(target)){
            return selected.get();
        } else if (visual.isSelected(target) || visual.isRelativeToSelect(target)){
            return relative.get();
        } else if (visual.hasSelected()){
            // fade other unselected
            Color c = unSelected.get();
            return new Color(c.getRed(), c.getGreen(), c.getBlue(), env.fadeAlpha.get());
        } else {
            return unSelected.get();
        }
    }

    @SuppressWarnings("unused")
    protected Color getColor(Viewer target, Color unSelected, Color selected){
        if (target == null) return unSelected;
        return getColor(target, unSelected, unSelected, selected);
    }

    protected Color getColor(Viewer target, ColorProperty unSelected, ColorProperty selected){
        if (target == null) return unSelected.get();
        return getColor(target, unSelected, unSelected, selected);
    }

    protected Color getColor(Viewer target, Color c){
        if (target == null) return c;
        return getColor(target, c, c, c);
    }

    protected Color getColor(Viewer target, ColorProperty c){
        if (target == null) return c.get();
        return getColor(target, c, c, c);
    }

    @SuppressWarnings("unused")
    protected <T> T get(Viewer target, T unSelected, T selected){
        if (target == null) return unSelected;
        if (visual.isSelected(target)) return selected;
        if (visual.isRelativeToSelect(target)) return selected;
        return unSelected;
    }

    protected <T> T get(Viewer target, ReadOnlyProperty<? super T> unSelected, ReadOnlyProperty<? super T> selected){
        if (target == null) return (T)unSelected.getValue();
        if (visual.isSelected(target)) return (T)selected.getValue();
        if (visual.isRelativeToSelect(target)) return (T)selected.getValue();
        return (T)unSelected.getValue();
    }

    protected void drawTriangle(GraphicsContext g, Viewer viewer, double size){
        double vx = viewer.x;
        double vy = viewer.y;
        double theta = viewer.theta + Math.PI;
        double inc = 2 * Math.PI / 3;
        size /= 2;
        for (int i = 0; i < 3; i++){
            g.strokeLine(vx + (int)(size * Math.cos(theta)),
                         vy + (int)(size * Math.sin(theta)),
                         vx + (int)(size * Math.cos(theta += inc)),
                         vy + (int)(size * Math.sin(theta)));
        }
    }

    protected void drawCircle(GraphicsContext g, PositionData v, double size){
        g.strokeOval(v.x - size / 2, v.y - size / 2, size, size);
    }

    protected void drawRect(GraphicsContext g, PositionData v, double size){
        g.strokeRect(v.x - size / 2, v.y - size / 2, size, size);
    }

    protected static void setLocationForComponent(Viewer host, int id, int all, Viewer self, int radius){
        double t = 2.0 * Math.PI * id / all + host.theta;
        self.x = (int)(host.x + radius * Math.cos(t));
        self.y = (int)(host.y + radius * Math.sin(t));
        self.theta = t;
    }

    protected void setLocationBesideBox(PositionData pointer,
                                        BoxViewer source,
                                        PositionData target){
        double sx = source.x;
        double sy = source.y;
        double tx = target.x;
        double ty = target.y;
        double dx = tx - sx;
        double dy = ty - sy;
        int nr = neuronSize >> 1;
        int NR = boxViewSize - neuronSize;//neuronSize + boxViewSize + nr;
        if (sx > tx){
            double fx = -sx / dx;
            double yy = sy + fx * dy;
            if (yy > nr && yy < NR){
                pointer.setLocation(-nr, yy - nr);
            } else if (sy > ty){
                pointer.setLocation(sx - dx * sy / dy, -nr);
            } else if (sy < ty){
                pointer.setLocation(sx + dx * (NR - sy) / dy, NR);
            } else {
                pointer.setLocation(-nr, sy);
            }
        } else if (sx < tx){
            double fx = (NR - sx) / dx;
            double yy = sy + fx * dy;
            if (yy > nr && yy < NR){
                pointer.setLocation(NR, yy);
            } else if (sy > ty){
                pointer.setLocation(sx - dx * sy / dy, -nr);
            } else if (sy < ty){
                pointer.setLocation(sx + dx * (NR - sy) / dy, NR);
            } else {
                pointer.setLocation(NR, sy);
            }
        } else if (sy > ty){
            pointer.setLocation(sx, -nr);
        } else if (sy < ty){
            pointer.setLocation(sx, NR);
        } else {
            assert sx == tx && sy == ty;
            pointer.setLocation(sx, sy);
        }
    }
}
