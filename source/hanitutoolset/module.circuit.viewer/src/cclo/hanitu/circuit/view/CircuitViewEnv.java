/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.util.Properties;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import cclo.hanitu.Base;
import cclo.hanitu.data.DataClass;
import cclo.hanitu.gui.ColorProperty;
import cclo.hanitu.gui.FontProperty;

/**
 * all config variable.
 *
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class CircuitViewEnv implements DataClass{

    public final SimpleIntegerProperty viewSize
      = new SimpleIntegerProperty(this, "view.size", 600);
    public final SimpleIntegerProperty boxViewSize
      = new SimpleIntegerProperty(this, "box.view.size", 490);
    public final SimpleIntegerProperty bodySize
      = new SimpleIntegerProperty(this, "body.size", 430);
    public final SimpleIntegerProperty boxSize
      = new SimpleIntegerProperty(this, "box.size", 30);
    public final SimpleIntegerProperty neuronSize
      = new SimpleIntegerProperty(this, "neuron.size", 30);
    public final SimpleIntegerProperty neuronRadius
      = new SimpleIntegerProperty(this, "neuron.radius", 20);
    public final SimpleIntegerProperty axonSize
      = new SimpleIntegerProperty(this, "axon.size", 15);
    public final SimpleIntegerProperty receptorSize
      = new SimpleIntegerProperty(this, "receptor.size", 15);
    public final SimpleDoubleProperty fadeAlpha
      = new SimpleDoubleProperty(this, "fadeAlpha", 0.3);
    public final ColorProperty backgroundColor
      = new ColorProperty(this, "background.color", Color.WHITE);
    public final ColorProperty selectRectangleColor
      = new ColorProperty(this, "select.rectangle.color", Color.color(0.6784314F, 0.84705883F, 0.9019608F, 0.5F));
    public final ColorProperty messageTextColor
      = new ColorProperty(this, "message.text.color", Color.color(0.2, 0.2, 0.2, 0.2));
    public final ColorProperty bodyColor
      = new ColorProperty(this, "body.color", Color.LIGHTGRAY);
    public final ColorProperty boxTextColor
      = new ColorProperty(this, "box.text.color", Color.BLACK);
    public final ColorProperty boxColor
      = new ColorProperty(this, "box.color", Color.BLACK);
    public final ColorProperty boxHighlightColor
      = new ColorProperty(this, "box.highlight.color", Color.ORANGE);
    public final ColorProperty neuronTextColor
      = new ColorProperty(this, "neuron.text.color", Color.BLACK);
    public final ColorProperty neuronColor
      = new ColorProperty(this, "neuron.color", Color.BLACK);
    public final ColorProperty neuronHighlightColor
      = new ColorProperty(this, "neuron.highlight.color", Color.ORANGE);
    public final ColorProperty axonColor
      = new ColorProperty(this, "axon.color", Color.RED);
    public final ColorProperty axonHighlightColor
      = new ColorProperty(this, "axon.highlight.color", Color.ORANGE);
    public final ColorProperty receptorTextColor
      = new ColorProperty(this, "receptor.text.color", Color.BLACK);
    public final ColorProperty receptorColor
      = new ColorProperty(this, "receptor.color", Color.GRAY);
    public final ColorProperty receptorHighlightColor
      = new ColorProperty(this, "receptor.highlight.color", Color.ORANGE);
    public final ColorProperty synapseTextColor
      = new ColorProperty(this, "synapse.text.color", Color.BLACK);
    public final ColorProperty synapseColor
      = new ColorProperty(this, "synapse.color", Color.color(0, 1, 0, 0.5));
    public final ColorProperty synapseHighlightColor
      = new ColorProperty(this, "synapse.highlight.color", Color.ORANGE);
    public final ColorProperty foodSensorColor
      = new ColorProperty(this, "food.sensor.color", Color.CYAN);
    public final ColorProperty toxicantSensorColor
      = new ColorProperty(this, "toxicant.sensor.color", Color.YELLOW);
    public final ColorProperty npySensorColor
      = new ColorProperty(this, "npy.sensor.color", Color.MAGENTA);
    public final ColorProperty motorColor
      = new ColorProperty(this, "motor.color", Color.RED);
    public final SimpleDoubleProperty bodyLinewidth
      = new SimpleDoubleProperty(this, "body.linewidth", 50);
    public final SimpleDoubleProperty defaultLinewidth
      = new SimpleDoubleProperty(this, "default.linewidth", 2);
    public final SimpleDoubleProperty boxLinewidth
      = new SimpleDoubleProperty(this, "box.linewidth", 4);
    public final SimpleDoubleProperty boxHighlightLinewidth
      = new SimpleDoubleProperty(this, "box.highlight.linewidth", 4);
    public final SimpleDoubleProperty neuronLinewidth
      = new SimpleDoubleProperty(this, "neuron.linewidth", 4);
    public final SimpleDoubleProperty neuronHighlightLinewidth
      = new SimpleDoubleProperty(this, "neuron.highlight.linewidth", 4);
    public final SimpleDoubleProperty axonLinewidth
      = new SimpleDoubleProperty(this, "axon.linewidth", 3);
    public final SimpleDoubleProperty axonHighlightLinewidth
      = new SimpleDoubleProperty(this, "axon.highlight.linewidth", 3);
    public final SimpleDoubleProperty receptorLinewidth
      = new SimpleDoubleProperty(this, "receptor.linewidth", 3);
    public final SimpleDoubleProperty receptorHighlightLinewidth
      = new SimpleDoubleProperty(this, "receptor.highlight.linewidth", 3);
    public final SimpleDoubleProperty synapseLinewidth
      = new SimpleDoubleProperty(this, "synapse.linewidth", 2);
    public final SimpleDoubleProperty synapseHighlightLinewidth
      = new SimpleDoubleProperty(this, "synapse.highlight.linewidth", 2);
    public final SimpleDoubleProperty synapseMaxLinewidth
      = new SimpleDoubleProperty(this, "synapse.max.linewidth", 5);
    public final SimpleStringProperty defaultFont = new SimpleStringProperty(this, "default.font", "FreeMono");
    public final FontProperty messageFont
      = new FontProperty(this, "message.font", Font.font("FreeMono", FontWeight.BOLD, 28));
    public final FontProperty boxTextFont
      = new FontProperty(this, "box.font", Font.font("FreeMono", FontWeight.BOLD, 20));
    public final FontProperty neuronTextFont
      = new FontProperty(this, "neuron.font", Font.font("FreeMono", FontWeight.BOLD, 20));
    public final FontProperty receptorTextFont
      = new FontProperty(this, "receptor.font", Font.font("FreeMono", 15));
    public final FontProperty receptorHighlightTextFont
      = new FontProperty(this, "receptor.highlight.font", Font.font("FreeMono", FontWeight.BOLD, 17));
    public final FontProperty synapseTextFont
      = new FontProperty(this, "synapse.font", Font.font("FreeMono", 15));
    public final FontProperty synapseHighlightTextFont
      = new FontProperty(this, "synapse.highlight.font", Font.font("FreeMono", FontWeight.BOLD, 17));
    public final SimpleStringProperty floatFormat
      = new SimpleStringProperty(this, "float.format", "%.2f");

    {
        defaultFont.addListener((v, o, f) -> {
            messageFont.set(Font.font(f, FontWeight.BOLD, messageFont.get().getSize()));
            boxTextFont.set(Font.font(f, FontWeight.BOLD, boxTextFont.get().getSize()));
            neuronTextFont.set(Font.font(f, FontWeight.BOLD, neuronTextFont.get().getSize()));
            receptorHighlightTextFont.set(Font.font(f, receptorTextFont.get().getSize()));
            receptorHighlightTextFont.set(Font.font(f, FontWeight.BOLD, receptorHighlightTextFont.get().getSize()));
            synapseHighlightTextFont.set(Font.font(f, synapseTextFont.get().getSize()));
            synapseHighlightTextFont.set(Font.font(f, FontWeight.BOLD, synapseHighlightTextFont.get().getSize()));
        });
    }

    @Override
    public Properties getProperty(){
        return Base.loadProperties("circuitenv");
    }
}
