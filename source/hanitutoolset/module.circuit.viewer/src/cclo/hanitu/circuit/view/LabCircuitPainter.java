/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;


import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.Effect;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.paint.Color;

import cclo.hanitu.data.Direction;
import cclo.hanitu.gui.GraphicTextUtils;

/**
 * @author antonio
 */
public class LabCircuitPainter extends DefaultCircuitPainter{

    private final LabCircuitMode mode;

    @SuppressWarnings("CanBeFinal")
    private int colorBarX = 10;

    @SuppressWarnings("CanBeFinal")
    private int colorBarY = 40;

    @SuppressWarnings("CanBeFinal")
    private int colorBarWidth = 10;

    @SuppressWarnings("CanBeFinal")
    private int colorBarHeight = 500;
    //
    private int maxActivity = 500;
    //
    double hp = 100;
    final boolean[] touchWall = new boolean[4];
    boolean touchWorm;
    boolean touchFood;
    boolean touchToxicant;
    boolean wormDie = false;
    Direction wormMove = Direction.SILENCE;
    private final Effect gaussian = new GaussianBlur(10);

    public LabCircuitPainter(LabCircuitMode mode){
        super();
        this.mode = mode;
    }

    public int getMaxActivity(){
        return maxActivity;
    }

    public void setMaxActivity(int maxActivity){
        if (maxActivity <= 0) throw new IllegalArgumentException("negative or zero max activity");
        this.maxActivity = maxActivity;
    }

    private Color getColorByHp(double hp){
        if (hp > 100) hp = 100;
        else if (hp < 0) hp = 0;
        int v = (int)(255 * hp / 100);
        return Color.rgb(v, v, v, 0.5);
    }

    private double normalizeActivity(double activity){
        return Math.min(Math.max(activity, 0), maxActivity) / maxActivity;
    }

    private Color getColorByActivity(double activity){
        double h = 240 * normalizeActivity(activity);
        if (Double.isNaN(h)) return Color.BLACK;
        double b = Math.min(1, h / 100);
        return Color.hsb(240 - h, 1, b);
    }

    private int getYOnColorBar(double activity){
        int h = colorBarHeight;
        double a = normalizeActivity(activity);
        if (Double.isNaN(a)) return h + colorBarY;
        return (int)(h * (1 - a)) + colorBarY;
    }

    @Override
    public void paint(GraphicsContext g){
        super.paint(g);
        paintCurrentTimeText(g);
        paintColorBar(g);
        paintActivityPointer(g);
    }

    private void paintCurrentTimeText(GraphicsContext g){
        g.setFill(Color.BLACK);
        g.fillText(String.format("%.4f", mode.getCurrentTime() / 1000.0), colorBarX, g.getFont().getSize());
    }

    private void paintColorBar(GraphicsContext g){
        int x1 = colorBarX;
        int y = colorBarY;
        int x2 = x1 + colorBarWidth;
        int h = colorBarHeight;
        for (int i = h; i >= 0; i--){
            g.setStroke(getColorByActivity(maxActivity * (1 - ((double)i) / h)));
            g.strokeLine(x1, i + y, x2, i + y);
        }
    }

    private void paintActivityPointer(GraphicsContext g){
        g.setFill(Color.BLACK);
        g.setStroke(Color.BLACK);
        g.setLineWidth(1);
        int x2 = colorBarX + colorBarWidth;
        if (!visual.selectedViewers.isEmpty()){
            Viewer v = visual.selectedViewers.get(0);
            if ((v instanceof NeuronViewer) || (v instanceof SensorViewer) || (v instanceof MotorViewer)){
                double activity = mode.getActivity(v);
                int y = getYOnColorBar(activity);
                String text = v.getText();
                double h = GraphicTextUtils.getTextHeight(g, text);
                g.strokeLine(x2, y, x2 + 10, y);
                g.fillText(text, x2 + 10, y);
                g.fillText(String.format("%.2f", activity), x2 + 10, y + h);
            }
        } else {
            for (SensorViewer v : sensorViewer){
                int y = getYOnColorBar(mode.getActivity(v));
                g.strokeLine(x2, y, x2 + 10, y);
                g.fillText(v.getText(), x2 + 10, y);
            }
            for (MotorViewer v : motorViewer){
                int y = getYOnColorBar(mode.getActivity(v));
                g.strokeLine(x2, y, x2 + 10, y);
                g.fillText(v.getText(), x2 + 10, y);
            }
            for (NeuronViewer v : neuronViewers.values()){
                int y = getYOnColorBar(mode.getActivity(v));
                g.strokeLine(x2, y, x2 + 10, y);
                g.fillText(v.getText(), x2 + 10, y);
            }
        }
    }

    @Override
    protected void paintEnvironment(GraphicsContext g){
        if (touchFood){
            g.setFill(Color.LIGHTGREEN);
            g.fillOval(0, 0, bodySize, bodySize);
        } else if (touchToxicant){
            g.setFill(Color.MAGENTA.brighter());
            g.fillOval(0, 0, bodySize, bodySize);
        } else if (touchWorm){
            g.setFill(env.backgroundColor.get().brighter());
            g.fillOval(0, 0, bodySize, bodySize);
        }
    }

    @Override
    protected void paintWormBody(GraphicsContext g){
        // Hp
        g.setEffect(gaussian);
        g.setStroke(getColorByHp(hp));
        g.setLineWidth(env.bodyLinewidth.get() * 2);
        g.strokeOval(0, 0, bodySize, bodySize);
        g.setEffect(null);
        // Body
        g.setLineWidth(env.bodyLinewidth.get());
        g.setStroke(env.bodyColor.get());
        g.strokeOval(0, 0, bodySize, bodySize);
    }

    @Override
    protected void paintNeuron(GraphicsContext g, NeuronViewer n){
        g.setStroke(getColorByActivity(mode.getActivity(n)));
        g.setLineWidth(env.neuronLinewidth.get());
        drawCircle(g, n, neuronSize);
        paintNeuronText(g, n);
    }

    @Override
    protected void paintSensor(GraphicsContext g, SensorViewer s){
        g.setStroke(getColorByActivity(mode.getActivity(s)));
        g.setLineWidth(env.neuronLinewidth.get());
        drawCircle(g, s, neuronSize);
        paintNeuronText(g, s, s.sensor.type.ID);
    }

    @Override
    protected void paintMotor(GraphicsContext g, MotorViewer m){
        g.setStroke(getColorByActivity(mode.getActivity(m)));
        g.setLineWidth(env.neuronLinewidth.get());
        drawCircle(g, m, neuronSize);
        paintNeuronText(g, m, "M");
    }
}
