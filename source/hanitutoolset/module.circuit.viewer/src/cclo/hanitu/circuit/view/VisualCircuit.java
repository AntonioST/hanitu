/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.Base;
import cclo.hanitu.circuit.*;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.data.Tuple;
import cclo.hanitu.exe.About;
import cclo.hanitu.gui.*;

import static cclo.hanitu.data.Direction.*;
import static cclo.hanitu.data.SensorType.*;

/**
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class VisualCircuit extends FXApplication{

    final Logger LOG = LoggerFactory.getLogger(VisualCircuit.class);

    final CircuitViewEnv env = new CircuitViewEnv();
    final FileSession file;
    final MenuManager menu;
    final PaintLayer canvas;

    AbstractCircuitPainter paint;
    VisualCircuitMode mode;
    InfoStage info;

    Circuit circuit;

    protected final Map<String, BoxViewer> boxViewers = new HashMap<>();
    protected final Map<String, NeuronViewer> neuronViewers = new HashMap<>();
    protected final List<ReceptorViewer<NeuronViewer>> receptorViewers = new ArrayList<>();
    protected final List<SynapseViewer> synapseViewers = new ArrayList<>();
    protected final SensorViewer[] sensorViewer = new SensorViewer[9];
    protected final MotorViewer[] motorViewer = new MotorViewer[4];

    BoxViewer selectedBox;
    final SimpleObjectProperty<Viewer> lastSelectedTarget = new SimpleObjectProperty<>();
    final List<Viewer> selectedViewers = new ArrayList<>();
    int silenceSelectedUpdate;
    boolean enableDragJoinNeuron;
    private Viewer lastMousePressTarget;

    double mousePositionX;
    double mousePositionY;
    private double lastMousePressedXOnPanel;
    private double lastMousePressedYOnPanel;
    private double lastMousePressedXInArea;
    private double lastMousePressedYInArea;
    private boolean onMouseDraggingMode;
    private boolean onSelectRectangleMode;
    private MouseButton lastMousePressedButton;
    TextViewer waitUserMouseClickData;
    private int dox;
    private int doy;
    private final SimpleIntegerProperty dx = new SimpleIntegerProperty(0);
    private final SimpleIntegerProperty dy = new SimpleIntegerProperty(0);

    public VisualCircuit(){
        file = initFile();
        menu = initMenu();
        canvas = initCanvas();
        root.getChildren().addAll(menu.getMenuBar(), canvas);
        int minViewSize = env.viewSize.get();
        root.setMinWidth(minViewSize);
        root.setMinHeight(minViewSize);
        primaryStage.setResizable(false);
        //
        setCircuitPainter(new DefaultCircuitPainter());
        initInfoStage();
        //
        setCircuit(new CircuitBuilder());
        setTitle(null);
        root.requestLayout();
        primaryStage.sizeToScene();
        primaryStage.show();
        canvas.repaint();
        canvas.repaint();
    }

    @Override
    public void close(){
        if (mode != null){
            mode.processQuitEvent();
        }
        if (info != null) info.close();
        super.close();
    }

    private FileSession initFile(){
        return new FileSession(primaryStage){
            {
                setDefaultFileExtName(HanituFile.CIRCUIT_EXTEND_FILENAME);
                setFileFilter(HanituFile.CIRCUIT_CONFIG_FILTERS);
            }

            @Override
            protected void onNewFile(){
                LOG.debug("new a empty file");
                setCircuit(new CircuitBuilder());
            }

            @Override
            protected void onOpenFile(Path openFilePath) throws IOException{
                LOG.debug("open file : {}", openFilePath);
                setCircuit(openFilePath, false);
            }

            @Override
            protected void onSaveFile(Path saveFilePath) throws IOException{
                LOG.debug("save file : {}", saveFilePath);
                FileSession.saveFileByTempReplace(saveFilePath, os -> {
                    writeMeta();
                    CircuitPrinter cp = new CircuitPrinter();
                    cp.write(os, circuit);
                });
            }

            @Override
            protected void onUpdate(){
                Platform.runLater(() -> setTitle(null));
            }

            @Override
            protected void onQuit(){
                close();
            }
        };
    }

    private void writeMeta(){
        neuronViewers.values().forEach(NeuronViewer::writeMeta);
        sensorViewer[8].writeMeta();
        boxViewers.values().forEach(box -> box.writeMeta(circuit));
    }

    private MenuManager initMenu(){
        MenuManager ret = new MenuManager(this::processMenuEvent);
        ret.resetTopMenu();
        return ret;
    }

    public void resetMenu(String subMenuType){
        if (inFxThread()){
            menu.resetTopMenu(subMenuType);
        } else {
            Platform.runLater(() -> resetMenu(subMenuType));
        }
    }

    private PaintLayer initCanvas(){
        PaintLayer ret = new PaintLayer(env.viewSize, env.viewSize);
        ret.backgroundColor.bind(env.backgroundColor);
        ret.setOnMouseMoved(e -> {
            mousePositionX = getXInPaintArea(e.getX());
            mousePositionY = getYInPaintArea(e.getY());
        });
        ret.setOnMousePressed(this::update);
        ret.setOnMouseClicked(e -> {
            update(e);
            if (mode != null && !onMouseDraggingMode && !onSelectRectangleMode){
                if (lastMousePressedButton == MouseButton.PRIMARY){
                    if (e.getClickCount() == 1){
                        LOG.debug("click event {}", lastMousePressTarget);
                        mode.processClickEvent(e, lastMousePressTarget);
                    } else if (selectedBox != null){
                        if (paint == null ||
                            paint.isTouch(selectedBox, lastMousePressedXInArea, lastMousePressedYInArea) != 0){
                            zoomOutBox();
                        }
                    } else if (lastMousePressTarget instanceof BoxViewer){
                        zoomInBox((BoxViewer)lastMousePressTarget);
                    } else {
                        LOG.debug("double click event {}", lastMousePressTarget);
                        mode.processDoubleClickEvent(e, lastMousePressTarget);
                    }
                } else if (lastMousePressedButton == MouseButton.SECONDARY){
                    LOG.debug("right click event {}", lastMousePressTarget);
                    mode.processRightClickEvent(e, lastMousePressTarget);
                }
            }
            onMouseDraggingMode = false;
            onSelectRectangleMode = false;
            if (waitUserMouseClickData != null){
                waitUserMouseClickData.setLocation(lastMousePressedXInArea, lastMousePressedYInArea);
                synchronized (VisualCircuit.this){
                    notifyAll();
                }
            }
        });
        ret.setOnMouseDragged(e -> {
            mousePositionX = getXInPaintArea(e.getX());
            mousePositionY = getYInPaintArea(e.getY());
            onMouseDraggingMode = true;
            if (lastMousePressedButton == MouseButton.PRIMARY){
                if (mode != null && lastMousePressTarget != null){
                    mode.processDragEvent(e, lastMousePressTarget);
                } else {
                    onSelectRectangleMode = true;
                    canvas.repaint();
                }
            } else if (lastMousePressedButton == MouseButton.MIDDLE){
                dragViewPanel(e);
            }
        });
        ret.setOnMouseReleased(e -> {
            if (mode != null && lastMousePressedButton == MouseButton.PRIMARY){
                if (onMouseDraggingMode && lastMousePressTarget != null){
                    LOG.debug("drag release event {}", lastMousePressTarget);
                    mode.processDragReleaseEvent(e, lastMousePressTarget);
                } else if (onSelectRectangleMode){
                    double x = Math.min(lastMousePressedXInArea, mousePositionX);
                    double y = Math.min(lastMousePressedYInArea, mousePositionY);
                    double w = Math.abs(lastMousePressedXInArea - mousePositionX);
                    double h = Math.abs(lastMousePressedYInArea - mousePositionY);
                    LOG.debug("rectangle select event ({}, {}, {}, {})", x, y, w, h);
                    mode.processRectangleSelectEvent(x, y, w, h);
                }
            }
            if (lastMousePressedButton == MouseButton.MIDDLE){
                dox = dx.get();
                doy = dy.get();
            }
            lastMousePressTarget = null;
            canvas.repaint();
        });
        return ret;
    }

    private void update(MouseEvent e){
        LOG.trace("mouse on scene ({}, {})", e.getSceneX(), e.getSceneY());
        lastMousePressedXOnPanel = e.getX();
        lastMousePressedYOnPanel = e.getY();
        lastMousePressedXInArea = getXInPaintArea(lastMousePressedXOnPanel);
        lastMousePressedYInArea = getYInPaintArea(lastMousePressedYOnPanel);
        LOG.trace("mouse on panel ({}, {})", lastMousePressedXOnPanel, lastMousePressedYOnPanel);
        lastMousePressedButton = e.getButton();
        lastMousePressTarget = findTouch(lastMousePressedXInArea, lastMousePressedYInArea,
                                         (lastSelectedTarget.get() instanceof SynapseViewer)
                                         && lastMousePressedButton == MouseButton.PRIMARY);
        LOG.trace("touch {}", lastMousePressTarget);
    }

    @Override
    protected void processMenuEvent(ActionEvent e, String action){
        LOG.debug("menu event : {}", action);
        if (action.startsWith("menu.file")){
            file.processMenuEvent(e, action);
        } else if (action.startsWith("&")){
            try {
                MenuSession.processReflectionMenuEvent(this, e, action.substring(1));
            } catch (RuntimeException ex){
                MessageSession.showErrorMessageDialog(ex);
            }
        } else {
            switch (action){
            case "menu.view.info":
                showInfo();
                break;
            case "menu.help.about":
                FXApplication.launch(About.class).show();
                break;
            default:
                if (mode != null){
                    try {
                        mode.processMenuEvent(e, action);
                    } catch (Exception ex){
                        MessageSession.showErrorMessageDialog(ex);
                    }
                }
            }
        }
    }

    @Override
    protected boolean processKeyboardEvent(KeyEvent e){
        switch (e.getCode()){
        case F5:
            LOG.debug("repaint canvas");
            canvas.repaint();
            break;
        case ESCAPE:
            if (waitUserMouseClickData != null){
                waitUserMouseClickData = null;
                synchronized (VisualCircuit.this){
                    notifyAll();
                }
                setSelected(null);
                break;
            } else if (!selectedViewers.isEmpty()){
                setSelected(null);
                break;
            } else if (selectedBox != null){
                zoomOutBox();
                break;
            }
            return false;
        default:
            return mode != null && mode.processKeyEvent(e);
        }
        return true;
    }

    private void initInfoStage(){
        info = new InfoStage(this);
        info.setEditable(false);
        lastSelectedTarget.addListener((v, o, n) -> setInfo(n));
    }

    @Override
    public void setTitle(String message){
        Properties p = Base.loadProperties();
        StringBuilder sb = new StringBuilder();
        if (file.isCurrentFileModified()){
            sb.append("* ");
        }
        sb.append(p.getProperty("title"));
        if (message != null){
            sb.append(" - ").append(message);
        }
        Path path = file.getCurrentFilePath();
        if (path != null){
            Path rp = Paths.get(System.getProperty("user.dir")).relativize(path.toAbsolutePath());
            if (rp.startsWith("..")){
                sb.append(" - ").append(path);
            } else {
                sb.append(" - ").append(rp);
            }
        }
        Platform.runLater(() -> primaryStage.setTitle(sb.toString()));
    }

    void setEditable(boolean editable){
        file.setEditable(editable);
        info.setEditable(editable);
    }

    @SuppressWarnings("unused")
    public Circuit getCircuit(){
        return circuit;
    }

    public void setCircuit(Path filePath, boolean failOnLoad) throws IOException{
        CircuitLoader loader = new CircuitLoader();
        loader.setFailOnLoad(failOnLoad);
        setCircuit(loader.load(filePath));
        file.setCurrentFilePath(filePath);
        file.setCurrentFileModified(false);
    }

    public void setCircuit(Circuit circuit){
        Objects.requireNonNull(circuit, "circuit instance");
        if (mode != null){
            LOG.trace("set circuit {}@{}", circuit.getClass().getName(), Objects.hashCode(circuit));
            circuit = mode.setCircuit(circuit);
            LOG.debug("set circuit {}@{}", circuit.getClass().getName(), Objects.hashCode(circuit));
        }
        if (this.circuit == circuit) return;
        //
        boxViewers.clear();
        neuronViewers.clear();
        receptorViewers.clear();
        synapseViewers.clear();
        setSelected(null);
        //
        this.circuit = circuit;
        setCircuitViewer(circuit);
        //
        file.setCurrentFilePath(null);
    }

    private void setCircuitViewer(Circuit circuit){
        LOG.debug("init circuit");
        LOG.debug("neuron count : {}", circuit.neuronCount());
        boolean randomLocation = circuit.neurons().stream().allMatch(n -> n.metaX.get() == 0 && n.metaY.get() == 0);
        LOG.debug("random neuron location : {}", randomLocation);
        int bodySize = env.bodySize.get();
        //create box
        EditCircuitMode.createAllBoxViewer(this, circuit);
        //create body neuron
        Sensor sensor = circuit.sensor(FOOD);
        sensorViewer[0] = new SensorViewer(sensor, FOOD, FORWARD);
        sensorViewer[1] = new SensorViewer(sensor, FOOD, BACKWARD);
        sensorViewer[2] = new SensorViewer(sensor, FOOD, LEFTWARD);
        sensorViewer[3] = new SensorViewer(sensor, FOOD, RIGHTWARD);
        //
        sensor = circuit.sensor(TOXICANT);
        sensorViewer[4] = new SensorViewer(sensor, TOXICANT, FORWARD);
        sensorViewer[5] = new SensorViewer(sensor, TOXICANT, BACKWARD);
        sensorViewer[6] = new SensorViewer(sensor, TOXICANT, LEFTWARD);
        sensorViewer[7] = new SensorViewer(sensor, TOXICANT, RIGHTWARD);
        //
        sensorViewer[8] = new SensorViewer(circuit.sensor(NPY), NPY, SILENCE);
        if (randomLocation){
            sensorViewer[8].setLocation(bodySize >> 1, bodySize >> 1);
        } else {
            sensor = circuit.sensor(NPY);
            sensorViewer[8].setLocation(sensor.metaX.get(), sensor.metaY.get());
        }
        LOG.trace("set NPY sensor to location ({}, {})", sensorViewer[8].x, sensorViewer[8].y);
        //
//        Motor motor = circuit.motor();
        motorViewer[0] = new MotorViewer(/*motor, */FORWARD);
        motorViewer[1] = new MotorViewer(/*motor, */BACKWARD);
        motorViewer[2] = new MotorViewer(/*motor, */LEFTWARD);
        motorViewer[3] = new MotorViewer(/*motor, */RIGHTWARD);
        //create neuron
        for (CentralNeuron n : circuit.neurons()){
            NeuronViewer v = EditCircuitMode.createNeuronViewer(this, n);
            int receptorCount = 0;
            for (Receptor r : circuit.getReceptor(n)){
                ReceptorViewer<NeuronViewer> rv = EditCircuitMode.createReceptorViewer(this, v, r);
                rv.locationCache = receptorCount++;
            }
            v.receptorCountCache = receptorCount;
            if (randomLocation){
                v.setLocation(Math.random() * bodySize, Math.random() * bodySize);
            } else {
                v.setLocation(n.metaX.get(), n.metaY.get());
            }
            LOG.trace("set location to ({}, {})", v.x, v.y);
        }

        //create synapse
        EditCircuitMode.createAllSynapseViewer(this, circuit);
    }

    public void setMode(VisualCircuitMode mode){
        this.mode = mode;
        if (mode != null){
            LOG.debug("set mode : {}", mode.getClass());
            mode.setup(this);
        }
    }

    @SuppressWarnings("unused")
    public AbstractCircuitPainter getCircuitPaint(){
        return paint;
    }

    public void setCircuitPainter(AbstractCircuitPainter cp){
        if (paint != null){
            canvas.removePanel(paint);
        }
        paint = cp;

        paint.x.bind(dx);
        paint.y.bind(dy);
        paint.width.bind(env.viewSize);
        paint.height.bind(env.viewSize);
        paint.setup(this);
        if (paint != null){
            canvas.addPanel(0, paint);
        }
    }

    public double getXInPaintArea(double x){
        return paint == null? x: paint.getXInArea(x);
    }

    public double getYInPaintArea(double y){
        return paint == null? y: paint.getYInArea(y);
    }

    public Rectangle getSelectedRectangle(){
        if (!onSelectRectangleMode) return null;
        double x = Math.min(lastMousePressedXInArea, mousePositionX);
        double y = Math.min(lastMousePressedYInArea, mousePositionY);
        double w = Math.abs(lastMousePressedXInArea - mousePositionX);
        double h = Math.abs(lastMousePressedYInArea - mousePositionY);
        return new Rectangle(x, y, w, h);
    }

    public Optional<BoxViewer> getBoxViewer(String name){
        return Optional.ofNullable(boxViewers.get(name));
    }

    public Optional<BoxViewer> getBelongBox(Viewer v){
        while (true){
            if (v == null) return null;
            if (v instanceof NeuronViewer){
                String box = ((NeuronViewer)v).neuron.metaBox.get();
                return box == null? null: getBoxViewer(box);
            } else if (v instanceof AxonViewer){
                v = ((AxonViewer)v).host;
            } else if (v instanceof ReceptorViewer){
                v = ((ReceptorViewer)v).host;
            } else {
                return null;
            }
        }
    }

    public Optional<NeuronViewer> getNeuronViewer(String neuronID){
        Objects.requireNonNull(neuronID, "neuron ID");
        return Optional.ofNullable(neuronViewers.get(neuronID));
    }

    public Stream<NeuronViewer> getNeuronInBox(){
        if (selectedBox == null) return Stream.empty();
        String name = selectedBox.name;
        return neuronViewers.values().stream().filter(n -> name.equals(n.neuron.metaBox.get()));
    }

    public Stream<NeuronViewer> getNeuronInBox(String name){
        Objects.requireNonNull(name, "Box name");
        return neuronViewers.values().stream().filter(n -> name.equals(n.neuron.metaBox.get()));
    }

    public Stream<ReceptorViewer<NeuronViewer>> getReceptorViewer(String neuronID){
        Objects.requireNonNull(neuronID, "neuron ID");
        return receptorViewers.stream()
          .filter(r -> neuronID.equals(r.receptor.hostNeuID.get()));
    }

    public Stream<ReceptorViewer<NeuronViewer>> getReceptorViewer(NeuronViewer neuron){
        Objects.requireNonNull(neuron, "neuron");
        return receptorViewers.stream().filter(r -> r.host == neuron);
    }

    public Optional<ReceptorViewer<NeuronViewer>> getReceptorViewer(String neuronID, String receptorID){
        Objects.requireNonNull(receptorID, "receptor ID");
        return getReceptorViewer(neuronID)
          .filter(r -> receptorID.equals(r.receptor.ID.get()))
          .findFirst();
    }

    public Optional<ReceptorViewer<NeuronViewer>> getFirstReceptorViewer(String neuronID){
        return getReceptorViewer(neuronID).findFirst();
    }

    public Optional<ReceptorViewer<NeuronViewer>> getFirstReceptorViewer(NeuronViewer neuron){
        return getReceptorViewer(neuron).findFirst();
    }

    public Optional<ReceptorViewer<NeuronViewer>> getNextReceptorViewer(String neuronID, String receptorID){
        Objects.requireNonNull(receptorID, "receptor ID");
        Iterator<ReceptorViewer<NeuronViewer>> it = getReceptorViewer(neuronID).iterator();
        if (it.hasNext()){
            ReceptorViewer first = it.next();
            ReceptorViewer next = first;
            while (true){
                if (receptorID.equals(next.receptor.ID.get())){
                    if (it.hasNext()) return Optional.of(it.next());
                    else return Optional.of(first);
                } else if (it.hasNext()){
                    next = it.next();
                } else {
                    break;
                }
            }
        }
        return Optional.empty();
    }

    public Stream<ReceptorViewer<NeuronViewer>> getReceptorInBox(String boxName){
        return getNeuronInBox(boxName).flatMap(n -> getReceptorViewer(n.neuron.ID.get()));
    }

    public Stream<ReceptorViewer<NeuronViewer>> getReceptorOnBox(String boxName){
        return getReceptorInBox(boxName).filter(r -> r.receptor.metaBoxRep.get());
    }

    public Optional<AxonViewer<NeuronViewer>> getAxonViewer(String neuronID){
        return getNeuronViewer(neuronID).map(n -> n.axon);
    }

    public Stream<AxonViewer<NeuronViewer>> getAxonInBox(String boxName){
        return getNeuronInBox(boxName).map(n -> n.axon);
    }

    public Stream<AxonViewer<NeuronViewer>> getAxonOnBox(String boxName){
        return getAxonInBox(boxName).filter(a -> a.host.isItsAxonABoxAxon());
    }

    public Stream<SynapseViewer> getSynapses(String neuron){
        Objects.requireNonNull(neuron, "neuron ID");
        return synapseViewers.stream()
          .filter(s -> neuron.equals(s.synapse.prevSynNeuID.get()));
    }

    @SuppressWarnings("unused")
    public Stream<SynapseViewer> getSynapses(NeuronViewer neuron){
        return getSynapses(neuron.neuron.ID.get());
    }

    public void setSelected(Viewer v){
        if (selectedBox != null && selectedBox == v) v = null;
        if (v == null){
            lastSelectedTarget.set(null);
            selectedViewers.clear();
        } else if (lastSelectedTarget.get() == v){
            if (selectedViewers.size() != 1){
                selectedViewers.clear();
                selectedViewers.add(v);
            }
        } else {
            selectedViewers.clear();
            selectedViewers.add(v);
            lastSelectedTarget.set(v);
        }
        if (silenceSelectedUpdate == 0){
            if (mode != null){
                LOG.debug("select event {}", v);
                mode.processSelectEvent(v, false);
            }
            canvas.repaint();
        }
    }

    public void appendSelect(Viewer v){
        if (v != null){
            if (selectedViewers.contains(v)){
                selectedViewers.remove(v);
            } else {
                selectedViewers.add(0, v);
            }
            lastSelectedTarget.set(v);
        }
        if (silenceSelectedUpdate == 0){
            if (mode != null){
                LOG.debug("select event {}", v);
                mode.processSelectEvent(v, false);
            }
            canvas.repaint();
        }
    }

    public boolean hasSelected(){
        return !selectedViewers.isEmpty();
    }

    public boolean isDirectSelected(Viewer v){
        return v != null && v == lastSelectedTarget.get();
    }

    public boolean isSelected(Viewer v){
        return selectedViewers.contains(v);
    }

    public boolean isRelativeToSelect(Viewer v){
        Viewer viewer = lastSelectedTarget.get();
        return v != null &&
               viewer != null &&
               v != viewer &&
               viewer.isRelative(v);
    }

    public void setInfo(Viewer v){
        if (silenceSelectedUpdate != 0) return;
        if (!inFxThread()){
            Platform.runLater(() -> setInfo(v));
            return;
        }
        if (v == null){
            info.reset(null);
        } else if (v instanceof NeuronViewer){
            info.reset(((NeuronViewer)v).neuron);
        } else if (v instanceof ReceptorViewer){
            ReceptorViewer r = (ReceptorViewer)v;
            if (r.host instanceof MotorViewer){
                info.reset(circuit.motor());
            } else {
                info.reset(r.receptor);
            }
        } else if (v instanceof SynapseViewer){
            SynapseViewer s = (SynapseViewer)v;
            if (s.postSynRep.host instanceof MotorViewer){
                info.reset(circuit.motor());
            } else {
                info.reset(s.synapse);
            }
        } else if (v instanceof AxonViewer){
            AxonViewer a = (AxonViewer)v;
            if (a.host instanceof SensorViewer){
                info.reset(circuit.sensor(((SensorViewer)a.host).sensor.type));
            } else {
                info.reset(((NeuronViewer)a.host).neuron);
            }
        } else if (v instanceof SensorViewer){
            info.reset(circuit.sensor(((SensorViewer)v).sensor.type));
        }
        info.setX(primaryStage.getX() + primaryStage.getWidth());
        info.setY(primaryStage.getY());
    }

    public void showInfo(){
        info.show();
        info.setX(primaryStage.getX() + primaryStage.getWidth());
        info.setY(primaryStage.getY());
    }

    public Viewer findTouch(double x, double y, boolean switchSelect){
        /*also do equal test to selected, to make select-exchange
        avoid above viewer always be selected and cover behind viewer
        */
        if (selectedBox == null){
            for (ReceptorViewer<NeuronViewer> r : receptorViewers){
                if (r.host.inBox()){
                    if (r.receptor.metaBoxRep.get() && testTouch(r, x, y, switchSelect)) return r;
                    continue;
                }
                if (testTouch(r, x, y, switchSelect)) return r;
            }
            //
            for (NeuronViewer n : neuronViewers.values()){
                if (n.inBox()){
                    if (n.isItsAxonABoxAxon() && testTouch(n.axon, x, y, switchSelect)) return n.axon;
                    continue;
                }
                if (testTouch(n.axon, x, y, switchSelect)) return n.axon;
                if (testTouch(n, x, y, switchSelect)) return n;
            }
            // npy
            SensorViewer s = sensorViewer[8];
            if (testTouch(s.axon, x, y, switchSelect)) return s.axon;
            if (testTouch(s, x, y, switchSelect)) return s;
            //
            for (BoxViewer v : boxViewers.values()){
                if (testTouch(v, x, y, switchSelect)) return v;
            }
            //
            for (int i = 0; i < 8; i++){
                s = sensorViewer[i];
                if (testTouch(s, x, y, switchSelect)) return s.axon;
                if (testTouch(s.axon, x, y, switchSelect)) return s.axon;
            }
            //
            for (MotorViewer m : motorViewer){
                if (testTouch(m, x, y, switchSelect)) return m.rep;
                if (testTouch(m.rep, x, y, switchSelect)) return m.rep;
            }
            //
            return findTouchSynapse(x, y, switchSelect);
        } else {
            for (ReceptorViewer r : receptorViewers){
                if (!selectedBox.name.equals(((NeuronViewer)r.host).neuron.metaBox.get())){
                    continue;
                }
                if (testTouch(r, x, y, switchSelect)) return r;
            }
            //
            for (NeuronViewer n : neuronViewers.values()){
                if (!selectedBox.name.equals(n.neuron.metaBox.get())) continue;
                if (testTouch(n.axon, x, y, switchSelect)) return n.axon;
                if (testTouch(n, x, y, switchSelect)) return n;
            }
            return findTouchSynapse(x, y, switchSelect);
        }
    }

    private SynapseViewer findTouchSynapse(double x, double y, boolean switchSelect){
        return synapseViewers.stream()
          .filter(s -> s.visibleCache)
          .filter(v -> !(switchSelect && selectedViewers.contains(v))) // can be selected?
          .map(v -> new Tuple<>(v, (Double)paint.isTouch(v, x, y)))
          .filter(e -> e.c2 < 10)
          .min(Comparator.comparingDouble(Tuple::right))
          .map(Tuple::left)
          .orElse(null);
    }

    private boolean testTouch(Viewer v, double x, double y, boolean switchSelect){
        // (can be selected) && (is mouse on it)
        return !(switchSelect && selectedViewers.contains(v)) && paint.isTouch(v, x, y) == 0;
    }

    protected void dragViewPanel(MouseEvent e){
        //lastMousePressedXOnPanel = e(old).getX - oldPanelX
        //dx = e(new).getX - e(old).getX
        //   = e(new).getX - lastMousePressedXOnPanel - oldPanelX
        //newX = oldPanelX + dx
        //     = e(new).getX - lastMousePressedXOnPanel
        //lastMousePressedX will keep old information during dragging, so it also keep the
        //information of the location of the viewPanel. We don't need the variable to record it.
        double x = e.getX() - lastMousePressedXOnPanel;
        double y = e.getY() - lastMousePressedYOnPanel;
        dx.set(dox + (int)x);
        dy.set(doy + (int)y);
        canvas.repaint();
    }

    public void zoomInBox(BoxViewer box){
        LOG.debug("zoom in {}", box);
        selectedBox = box;
        setSelected(null);
        canvas.repaint();
    }

    public void zoomOutBox(){
        LOG.debug("zoom out {}", selectedBox);
        selectedBox = null;
        setSelected(null);
        canvas.repaint();
    }

    public PositionData waitUserMouseClick(){
        return waitUserMouseClick(Base.loadProperties().getProperty("wait.click.message"));
    }

    public PositionData waitUserMouseClick(String message){
        Objects.requireNonNull(message);
        if (inFxThread()){
            throw new IllegalStateException("cannot call this method in Fx Thread");
        }
        TextViewer empty = new TextViewer(message);
        setSelected(empty);
        waitUserMouseClickData = empty;
        synchronized (this){
            //noinspection EmptyCatchBlock
            try {
                wait();
            } catch (InterruptedException e){
            }
        }
        empty = waitUserMouseClickData;
        waitUserMouseClickData = null;
        return empty;
    }

    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public static void main(String[] args) throws IOException{
        if (args.length == 0){
            System.out.println("java ... " + VisualCircuit.class.getName() + " CIRCUIT");
            System.exit(0);
        }
        VisualCircuit main = FXApplication.launch(VisualCircuit.class);
        main.setMode(new EditCircuitMode());
        main.setCircuitPainter(new DefaultCircuitPainter());
        main.setCircuit(Paths.get(args[0]), true);
    }
}
