/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import cclo.hanitu.data.PositionData;
import cclo.hanitu.gui.PaintArea;

/**
 *
 * top class of circuit painter. only define how the protocol of the painting and provide some utility methods.
 *
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public abstract class AbstractCircuitPainter extends PaintArea{

    protected VisualCircuit visual;
    protected CircuitViewEnv env;

    public AbstractCircuitPainter(){
    }

    public void setup(VisualCircuit visual){
        this.visual = visual;
        env = visual.env;
    }

    protected boolean isSelected(Viewer target){
        if (visual.hasSelected() && target != null){
            if (visual.selectedViewers.contains(target)){
                return true;
            }
        }
        return false;
    }

    protected boolean isSelectedOrRelative(Viewer target){
        if (visual.hasSelected() && target != null){
            if (visual.selectedViewers.contains(target) || visual.isRelativeToSelect(target)){
                return true;
            }
        }
        return false;
    }

    public abstract double isTouch(Viewer v, double x, double y);

    /**
     * is point (x, y) locate in the this circle-shape viewer.
     *
     * @param center circle center location
     * @param r      circle radius
     * @param x      point x
     * @param y      point y
     * @return negative number if point doesn't locate in. 0 if point locate in.
     */
    protected static double isTaughtSurround(PositionData center, double r, double x, double y){
        x -= center.x;
        y -= center.y;
        if ((x < 0 && x < -r) || x > r){
            return Double.POSITIVE_INFINITY;
        }
        if ((y < 0 && y < -r) || y > r){
            return Double.POSITIVE_INFINITY;
        }
        return x * x + y * y < r * r? 0: Double.POSITIVE_INFINITY;
    }

    protected static double isTaughtRectangle(Viewer center, double r, double x, double y){
        x = Math.abs(x - center.x);
        y = Math.abs(y - center.y);
        r /= 2;
        return (x < r && y < r)? 0: Double.POSITIVE_INFINITY;
    }

    protected static double isTaughtRectangle(double x, double y, double w, double h, double vx, double vy, double vr){
        vr /= 2;
        return (vx > x && vx + vr < x + w && vy > y && vy + vr < y + h)? 0: Double.POSITIVE_INFINITY;
    }

    protected static double isTouchLine(PositionData source, PositionData target, double x, double y){
        //point P (x, y)
        //point S (sx, sy)
        //point T (tx, ty)
        double sx = source.x;
        double sy = source.y;
        double tx = target.x;
        double ty = target.y;
        double dx = tx - sx;
        double dy = ty - sy;
        if (dx == dy) return 0;
        //boundary check
        if (Math.abs(dx) > 10 && (x < Math.min(sx, tx) || Math.max(sx, tx) < x))
            return Double.POSITIVE_INFINITY;
        if (Math.abs(dy) > 10 && (y < Math.min(sy, ty) || Math.max(sy, ty) < y))
            return Double.POSITIVE_INFINITY;
        //calculate triangle PST area and bottom line ST length
        //get the triangle PST height, which is the distance between point P and line ST
        //area = bottom * height / 2
        //area = |sx * (ty - y) + tx * (y - sy) + x * (sy - ty)| / 2
        //button = sqrt(dx * dx + dy * dy)
        //height = area * 2 / bottom
        //       = |sx * (ty - y) + tx * (y - sy) + x * (sy - ty)| / sqrt(dx * dx + dy * dy)
        //area = |sx*ty - sx*y + tx*y - tx*sy + x * (sy - ty)| / 2
        //     = |sx*ty - tx*sy + y * (tx - sx) + x * (sy - ty)| / 2
        //     = |sx*ty - tx*sy + y * (dx) + x * (-dy)| / 2
        //     = |sx*ty - tx*sy + y*dx - x*dy| / 2
        return Math.abs(sx * ty - tx * sy + y * dx - x * dy)
               / Math.sqrt(dx * dx + dy * dy);
    }
}
