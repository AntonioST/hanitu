/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.data.*;
import cclo.hanitu.io.TailHistoryStream;
import cclo.hanitu.io.TailStream;

/**
 * @author antonio
 */
public class LabCircuitMode extends VisualCircuitMode{

    private final LabCircuitPainter painter;
    private final SingleSpikeFilter identify;
    //
    private int currentTime;
    private final Map<String, FiringRateData> centralNeuronActivity = new HashMap<>();
    private final FiringRateData[] bodyNeuronActivity = new FiringRateData[13];

    public LabCircuitMode(){
        painter = new LabCircuitPainter(this);
        identify = null;
    }

    public LabCircuitMode(SingleSpikeFilter identify){
        painter = new LabCircuitPainter(this);
        this.identify = identify;
    }

    @Override
    protected void setup(VisualCircuit circuit){
        super.setup(circuit);
        circuit.setCircuitPainter(painter);
        circuit.info.setEditable(false);
    }

    public double getActivity(Viewer v){
        FiringRateData data;
        if (v instanceof NeuronViewer){
            data = centralNeuronActivity.get(((NeuronViewer)v).neuron.ID.get());
        } else if (v instanceof SensorViewer){
            SensorViewer s = (SensorViewer)v;
            if (s.type == SensorType.NPY){
                data = bodyNeuronActivity[8];
            } else {
                data = bodyNeuronActivity[s.type.ordinal() * 4 + s.direction.ordinal() - 1];
            }
        } else if (v instanceof MotorViewer){
            data = bodyNeuronActivity[9 + ((MotorViewer)v).direction.ordinal() - 1];
        } else {
            data = null;
        }
        return data == null? Double.NaN: data.rate;
    }

    @Override
    protected void processClickEvent(MouseEvent e, Viewer target){
        if (target != null){
            if (target instanceof ReceptorViewer){
                target = ((ReceptorViewer)target).host;
            } else if (target instanceof AxonViewer){
                target = ((AxonViewer)target).host;
            }
            setSelected(target);
        } else {
            setSelectNone();
        }
    }

    @Override
    protected void processDragEvent(MouseEvent e, Viewer v){
        dragViewer(e, v);
        repaintCanvas();
    }

    public int getCurrentTime(){
        return currentTime;
    }

    @SuppressWarnings("WeakerAccess")
    public void setCurrentTime(int currentTime){
        this.currentTime = currentTime;
    }

    public void updateWormHp(double hp){
        painter.hp = hp;
    }

    public void updateEvent(EventData data){
        switch (data.type){
        case TOUCH_WALL:{
            EventData.WormTouchWallEvent e = (EventData.WormTouchWallEvent)data;
            painter.touchWall[e.getDirection().ordinal() - 1] = true;
            break;
        }
        case MOVE_UP:
            painter.wormMove = Direction.FORWARD;
            break;
        case MOVE_DOWN:
            painter.wormMove = Direction.BACKWARD;
            break;
        case MOVE_LEFT:
            painter.wormMove = Direction.LEFTWARD;
            break;
        case MOVE_RIGHT:
            painter.wormMove = Direction.RIGHTWARD;
            break;
        case TOUCH_WORM:
            painter.touchWorm = true;
            break;
        case TOUCH_FOOD:
            painter.touchFood = true;
            break;
        case TOUCH_TOXICANT:
            painter.touchToxicant = true;
            break;
        case WORM_DIE:
            painter.wormDie = true;
            break;
        }
    }

    public void resetEvent(){
        Arrays.fill(painter.touchWall, false);
        painter.touchWorm = false;
        painter.touchFood = false;
        painter.touchToxicant = false;
        painter.wormMove = Direction.SILENCE;
    }

    @SuppressWarnings("WeakerAccess")
    public void updateActivity(FiringrateResult.SnapshotResult result){
        if (identify == null) return;
        for (SingleSpikeFilter filter : result.getFilters()){
            if (identify.isSameWorm(filter)){
                FiringRateData data = result.getFiringrate(filter);
                if (filter.isNeuron()){
                    centralNeuronActivity.put(filter.neuron, data);
                } else if (filter.isModularitySensor()){
                    bodyNeuronActivity[8] = data;
                } else if (filter.isMotor()){
                    bodyNeuronActivity[9 + filter.getDirection().ordinal() - 1] = data;
                } else {
                    bodyNeuronActivity[filter.getSensorType().ordinal() * 4 + filter.getDirection().ordinal() - 1]
                      = data;
                }
            }
        }
    }

    public static class LabDataSource implements TimeDataHandler, SpikeDataHandler, EventDataHandler{

        private final Logger LOG = LoggerFactory.getLogger(LabCircuitMode.class);

        private final SimpleIntegerProperty currentTime = new SimpleIntegerProperty(0);
        private FiringrateFunction function = new FiringrateFunction(100, 10);
        private TailHistoryStream<FiringrateResult.SnapshotResult> firingrateStream;
        private TailHistoryStream<EventData> eventStream;
        private final List<LabCircuitMode> downstream = new ArrayList<>();

        private final ReentrantLock lock = new ReentrantLock();

        public FiringrateFunction getFiringrateFunction(){
            return function;
        }

        public void setFiringrateFunction(FiringrateFunction function){
            this.function = Objects.requireNonNull(function);
        }

        @Override
        public ReadOnlyIntegerProperty getTimeProperty(){
            return currentTime;
        }

        @Override
        public void setSpikeFile(Path path){
            LOG.debug("set spike file : {}", path);
            SpikeDataHandler.super.setSpikeFile(path);
        }

        @Override
        public void setSpikeFile(String path){
            LOG.debug("set spike file : {}", path);
            SpikeDataHandler.super.setSpikeFile(path);
        }

        @Override
        public void setSpikeStream(TailStream<SpikeData> source){
            if (firingrateStream != null){
                //noinspection EmptyCatchBlock
                try {
                    firingrateStream.close();
                } catch (IOException e){
                }
            }
            firingrateStream = new TailHistoryStream<>(FiringrateResult.asStream(function, source));
        }

        @Override
        public void closeSpikeStream(){
            //noinspection EmptyCatchBlock
            try {
                firingrateStream.close();
            } catch (IOException e){
            }
        }

        @Override
        public void setEventFile(Path path){
            LOG.debug("set event file : {}", path);
            EventDataHandler.super.setEventFile(path);
        }

        @Override
        public void setEventFile(String path){
            LOG.debug("set event file : {}", path);
            EventDataHandler.super.setEventFile(path);
        }

        @Override
        public void setEventStream(TailStream<EventData> source){
            if (eventStream != null){
                //noinspection EmptyCatchBlock
                try {
                    eventStream.close();
                } catch (IOException e){
                }
            }
            eventStream = new TailHistoryStream<>(source);
        }

        @Override
        public void closeEventStream(){
            //noinspection EmptyCatchBlock
            try {
                eventStream.close();
            } catch (IOException e){
            }
        }

        public void addUpdateListener(LabCircuitMode lab){
            downstream.add(lab);
        }

        @Override
        public boolean updateUntilTime(int time){
            //noinspection EmptyCatchBlock
            try {
                if (lock.tryLock(10, TimeUnit.MILLISECONDS)){
                    try {
                        updateUntilTimePrivate(time);
                    } finally {
                        lock.unlock();
                    }
                } else {
                    LOG.debug("skip {}", time);
                }
            } catch (InterruptedException e){
            }
            return true;
        }

        private void updateUntilTimePrivate(int time){
            if (currentTime.get() > time){
                firingrateStream.head();
                eventStream.head();
            }
            currentTime.set(time);
            downstream.forEach(lab -> lab.setCurrentTime(time));
            if (firingrateStream != null){
                FiringrateResult.SnapshotResult result = TimeSerialData.getAfterTime(firingrateStream, time);
                if (result != null){
                    downstream.forEach(lab -> lab.updateActivity(result));
                    if (eventStream != null){
                        downstream.forEach(LabCircuitMode::resetEvent);
                        TimeSerialData.getAfterTime(eventStream, time - 1);
                        TimeSerialData.untilTime(eventStream, time, e -> downstream.forEach(lab -> lab.updateEvent(e)));
                    }
                    downstream.forEach(VisualCircuitMode::repaintCanvas);
                }
            }
        }
    }

    public static class LabPlayer extends LabDataSource{

        private int fps = 20;
        private Timeline timeline;

        @SuppressWarnings("unused")
        public int getFps(){
            return fps;
        }

        public void setFps(int fps){
            this.fps = fps;
        }

        public void play(){
            initTimeLine();
            timeline.play();
        }

        @SuppressWarnings("unused")
        public void stop(){
            if (timeline != null) timeline.stop();
        }

        private void initTimeLine(){
            if (timeline == null){
                timeline = new Timeline(new KeyFrame(new Duration(50), this::timeLineKeyFrame));
                timeline.setCycleCount(Timeline.INDEFINITE);
            }
            timeline.stop();
        }

        private void timeLineKeyFrame(ActionEvent e){
            boolean t = updateUntilTime(getCurrentTime() + getFiringrateFunction().getTimeStep());
            if (!t){
                timeline.stop();
            }
        }
    }
}
