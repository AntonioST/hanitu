/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import cclo.hanitu.data.PositionData;

/**
 * the class contain some drawing data.
 *
 * @author antonio
 */
public class Viewer extends PositionData{

    double theta = -Math.PI / 2;


    public String getText(){
        return "";
    }

    /**
     * is viewer {@code other} has the relationship with this viewer.
     *
     * @param other other viewer
     * @return true if both of they has relationship
     */
    public boolean isRelative(Viewer other){
        return false;
    }

    @Override
    public String toString(){
        return getClass().getSimpleName() + ":" + getText();
    }
}
