/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import cclo.hanitu.data.Direction;

/**
 * the viewer for motor
 *
 * @author antonio
 */
public class MotorViewer extends Viewer{

    //    final Motor motor;
    final Direction direction;
    final ReceptorViewer<MotorViewer> rep;
    private final String text;

    public MotorViewer(/*Motor motor, */Direction d){
//        this.motor = motor;
        direction = d;
        rep = new ReceptorViewer(this, null);
        theta = Direction.getTheta(Direction.opposite(d));
        text = String.format("M%c", d.name().charAt(0));
    }

    @Override
    public String getText(){
        return text;
    }
}
