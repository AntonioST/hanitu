/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

import javafx.event.ActionEvent;
import javafx.scene.control.ContextMenu;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.Base;
import cclo.hanitu.circuit.CentralNeuron;
import cclo.hanitu.circuit.Circuit;
import cclo.hanitu.circuit.Receptor;
import cclo.hanitu.data.SensorType;
import cclo.hanitu.gui.KeyboardBufferInt;

/**
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class VisualCircuitMode{

    protected VisualCircuit visual;
    protected final Logger LOG = LoggerFactory.getLogger(VisualCircuitMode.class);
    private Path saveFileName;
    private final KeyboardBufferInt buffer;

    public VisualCircuitMode(){
        Properties p = Base.loadProperties();
        buffer = new KeyboardBufferInt();
        buffer.setWaitingTime(Integer.parseInt(p.getProperty("buffer.waiting", "1000")));
    }

    protected void setup(VisualCircuit visual){
        this.visual = visual;
        visual.setEditable(false);
        if (saveFileName != null){
            setSaveFileName(saveFileName);
        }
    }

    protected Circuit setCircuit(Circuit c){
        return c;
    }

    public void setSaveFileName(Path filePath){
        saveFileName = filePath;
        if (visual != null){
            visual.file.setCurrentFilePath(filePath);
        }
    }

    protected void processMenuEvent(ActionEvent e, String action){
        switch (action){
        case "menu.select.toneu":
            transferSelectToNeuron();
            break;
        case "menu.select.allneu":
            selectAllNeuron();
            break;
        case "menu.select.torep":
            transferSelectToReceptor();
            break;
        case "menu.select.toaxon":
            transferSelectToAxon(false);
            break;
        case "menu.select.toAxon":
            transferSelectToAxon(true);
            break;
        case "menu.select.tosyn":
            transferSelectToSynapse();
            break;
        }
    }

    protected boolean processKeyEvent(KeyEvent e){
        return false;
    }

    protected void processClickEvent(MouseEvent e, Viewer v){
    }

    protected void processRightClickEvent(MouseEvent e, Viewer v){
    }

    @SuppressWarnings({"EmptyMethod", "UnusedParameters"})
    protected void processDoubleClickEvent(MouseEvent e, Viewer v){
    }

    protected void processDragEvent(MouseEvent e, Viewer v){
    }

    @SuppressWarnings("UnusedParameters")
    protected void processDragReleaseEvent(MouseEvent e, Viewer v){
    }

    @SuppressWarnings({"UnusedParameters", "EmptyMethod"})
    protected void processSelectEvent(Viewer v, boolean append){
    }

    protected void processRectangleSelectEvent(double x, double y, double w, double h){
    }

    @SuppressWarnings("EmptyMethod")
    protected void processUpdateEvent(){
    }

    @SuppressWarnings("EmptyMethod")
    protected void processQuitEvent(){
    }

    protected boolean processKeyboardSelect(KeyEvent e){
        if (buffer.processKeyboardEvent(e.getCode())){
            buffer.get().ifPresent(this::selectNeuronByNumericID);
            return true;
        }
        return false;
    }

    protected List<Viewer> getViewerInRectangle(double x, double y, double w, double h){
        List<Viewer> list = new LinkedList<>();
        int r = visual.env.neuronSize.get();
        for (NeuronViewer viewer : visual.neuronViewers.values()){
            if (AbstractCircuitPainter.isTaughtRectangle(x, y, w, h, viewer.x, viewer.y, r) == 0) list.add(viewer);
        }
        r = visual.env.boxSize.get();
        for (BoxViewer viewer : visual.boxViewers.values()){
            if (AbstractCircuitPainter.isTaughtRectangle(x, y, w, h, viewer.x, viewer.y, r) == 0) list.add(viewer);
        }
        return new ArrayList<>(list);
    }

    protected void silenceVisualEvent(boolean silence){
        if (silence){
            visual.silenceSelectedUpdate++;
        } else {
            visual.silenceSelectedUpdate--;
            if (visual.silenceSelectedUpdate < 0) visual.silenceSelectedUpdate = 0;
        }
    }

    public void showPopupMenu(ContextMenu menu, double screenX, double screenY){
        menu.show(visual.primaryStage, screenX, screenY);
    }

    public double getPaneX(){
        return visual.canvas.getLayoutX();
    }

    public double getPaneY(){
        return visual.canvas.getLayoutY();
    }

    public double getCanvasX(){
        return visual.paint.x.get();
    }

    public double getCanvasY(){
        return visual.paint.y.get();
    }

    public void repaintCanvas(){
        visual.canvas.repaint();
    }

    public void selectNeuronByNumericID(int numericID){
        String ID = Integer.toString(numericID);
        if (visual.selectedBox == null){
            for (NeuronViewer viewer : visual.neuronViewers.values()){
                if (viewer.neuron.ID.get().startsWith(ID)){
                    setSelected(viewer);
                    break;
                }
            }
        } else {
            String name = visual.selectedBox.name;
            for (NeuronViewer viewer : visual.neuronViewers.values()){
                CentralNeuron n = viewer.neuron;
                if (name.equals(n.metaBox.get()) && n.ID.get().startsWith(ID)){
                    setSelected(viewer);
                    break;
                }
            }
        }
    }

    public void selectAllNeuron(){
        LOG.trace("transfer select : all neuron");
        if (visual.selectedBox == null){
            visual.selectedViewers.clear();
            setSelected(visual.neuronViewers.values());
        } else {
            setSelected(visual.getNeuronInBox().collect(Collectors.toList()));
        }
    }

    public void transferSelectToNeuron(){
        LOG.trace("transfer select : neuron");
        List<Viewer> newSelected = new LinkedList<>();
        for (Viewer viewer : visual.selectedViewers){
            if (viewer instanceof ReceptorViewer){
                viewer = ((ReceptorViewer)viewer).host;
            } else if (viewer instanceof AxonViewer){
                viewer = ((AxonViewer)viewer).host;
            } else if (viewer instanceof SynapseViewer){
                viewer = ((SynapseViewer)viewer).postSynRep.host;
            }
            if (viewer instanceof NeuronViewer){
                newSelected.add(viewer);
            }
        }
        setSelected(newSelected);
    }

    public void transferSelectToAxon(boolean next){
        LOG.trace("transfer select : axon");
        List<Viewer> newSelected = new LinkedList<>();
        for (Viewer viewer : visual.selectedViewers){
            if (viewer instanceof SynapseViewer){
                if (next){
                    viewer = ((SynapseViewer)viewer).postSynRep.host;
                } else {
                    viewer = ((SynapseViewer)viewer).prevSynAxon.host;
                }
            } else if (viewer instanceof ReceptorViewer){
                viewer = ((ReceptorViewer)viewer).host;
            }
            if (viewer instanceof NeuronViewer){
                newSelected.add(((NeuronViewer)viewer).axon);
            } else if (viewer instanceof AxonViewer){
                if (next){
                    Viewer fv = viewer;
                    visual.synapseViewers.stream()
                      .filter(v -> v.prevSynAxon == fv)
                      .map(v -> v.postSynRep.host)
                      .filter(v -> (v instanceof NeuronViewer))
                      .map(v -> ((NeuronViewer)v).axon)
                      .forEach(newSelected::add);
                } else {
                    newSelected.add(viewer);
                }
            }
        }
        setSelected(newSelected);
    }

    public void transferSelectToReceptor(){
        LOG.trace("transfer select : receptor");
        List<Viewer> newSelected = new LinkedList<>();
        for (int i = 0, size = visual.selectedViewers.size(); i < size; i++){
            Viewer viewer = visual.selectedViewers.get(i);
            if (viewer instanceof NeuronViewer){
                String id = ((NeuronViewer)viewer).neuron.ID.get();
                visual.getFirstReceptorViewer(id).ifPresent(newSelected::add);
            } else if (viewer instanceof ReceptorViewer){
                Receptor r = ((ReceptorViewer)viewer).receptor;
                if (r.hostNeuID.get() != null){
                    visual.getNextReceptorViewer(r.hostNeuID.get(), r.ID.get()).ifPresent(newSelected::add);
                }
            } else if (viewer instanceof SynapseViewer){
                ReceptorViewer rep = ((SynapseViewer)viewer).postSynRep;
                if (rep.host instanceof NeuronViewer){
                    newSelected.add(rep);
                }
            } else if (viewer instanceof AxonViewer){
                visual.synapseViewers.stream()
                  .filter(v -> v.prevSynAxon == viewer)
                  .map(v -> v.postSynRep)
                  .forEach(newSelected::add);
            }
        }
        setSelected(newSelected);
    }

    public void transferSelectToSynapse(){
        LOG.trace("transfer select : synapse");
        List<Viewer> newSelected = new LinkedList<>();
        for (int i = 0, size = visual.selectedViewers.size(); i < size; i++){
            Viewer viewer = visual.selectedViewers.get(i);
            if (viewer instanceof AxonViewer){
                Viewer host = ((AxonViewer)viewer).host;
                if ((host instanceof NeuronViewer) || (host instanceof SensorViewer)){
                    viewer = host;
                }
            }
            if ((viewer instanceof NeuronViewer) || (viewer instanceof SensorViewer)){
                Viewer find = viewer;
                visual.synapseViewers.stream()
                  .filter(s -> s != null)
                  .filter(s -> s.prevSynAxon.host == find)
                  .forEach(newSelected::add);
            } else if (viewer instanceof ReceptorViewer){
                Viewer find = viewer;
                visual.synapseViewers.stream()
                  .filter(s -> s != null)
                  .filter(s -> s.postSynRep.host == find)
                  .forEach(newSelected::add);
            } else if (viewer instanceof SynapseViewer){
                newSelected.add(viewer);
            }
        }
        setSelected(newSelected);
    }

    protected void setSelectNone(){
        visual.lastSelectedTarget.set(null);
        visual.selectedViewers.clear();
        visual.canvas.repaint();
    }

    protected void setSelected(Viewer newSelected){
        Objects.requireNonNull(newSelected);
        visual.lastSelectedTarget.set(newSelected);
        visual.selectedViewers.clear();
        visual.selectedViewers.add(newSelected);
        LOG.debug("select {}", visual.lastSelectedTarget);
        visual.canvas.repaint();
    }

    protected void setSelected(Collection<? extends Viewer> newSelected){
        if (!newSelected.isEmpty()){
            Viewer firstSelected = newSelected.iterator().next();
            visual.selectedViewers.clear();
            visual.selectedViewers.addAll(new HashSet<>(newSelected));
            visual.lastSelectedTarget.set(firstSelected);
            LOG.debug("select {}", visual.selectedViewers);
        } else {
            visual.lastSelectedTarget.set(null);
            visual.selectedViewers.clear();
        }
        visual.canvas.repaint();
    }

    public void dragViewer(MouseEvent e, Viewer dragTarget){
        if (dragTarget == null) return;
        //only central neuron, npy sensor and box can be dragged
        if ((dragTarget instanceof NeuronViewer) || (dragTarget instanceof BoxViewer) || isNPYSensor(dragTarget)){
            double x = visual.getXInPaintArea(e.getX());
            double y = visual.getYInPaintArea(e.getY());
            dragTarget.setLocation(x, y);
        } else if (dragTarget instanceof AxonViewer){
            AxonViewer av = (AxonViewer)dragTarget;
            if ((av.host instanceof NeuronViewer) || isNPYSensor(av.host)){
                double dx = visual.getXInPaintArea(e.getX()) - av.host.x;
                double dy = visual.getYInPaintArea(e.getY()) - av.host.y;
                av.host.theta = Math.atan2(dy, dx);
            }
        }
    }

    private static boolean isNPYSensor(Viewer dragTarget){
        return (dragTarget instanceof SensorViewer) && (((SensorViewer)dragTarget).sensor.type == SensorType.NPY);
    }
}
