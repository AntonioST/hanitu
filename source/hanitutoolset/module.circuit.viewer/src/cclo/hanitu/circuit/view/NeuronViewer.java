/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import cclo.hanitu.circuit.CentralNeuron;
import cclo.hanitu.circuit.Circuit;

/**
 * the viewer for neuron.
 *
 * @author antonio
 */
public class NeuronViewer extends Viewer{

    final CentralNeuron neuron;
    final AxonViewer<NeuronViewer> axon;
    //cache use
    int receptorCountCache;

    public NeuronViewer(CentralNeuron neuron){
        this.neuron = neuron;
        axon = new AxonViewer(this);
    }

    public int updateReceptorCount(Circuit circuit){
        return receptorCountCache = circuit.receptorCount(neuron.ID.get()).orElse(0);
    }

    public boolean inBox(){
        return neuron.metaBox.get() != null;
    }

    public boolean inBox(String boxName){
        String box = neuron.metaBox.get();
        return box != null && box.equals(boxName);
    }

    public boolean isItsAxonABoxAxon(){
        return neuron.metaBoxAxon.get();
    }

    public void writeMeta(){
        neuron.metaX.set((int)x);
        neuron.metaY.set((int)y);
        neuron.metaTheta.set(theta);
    }

    @Override
    public String getText(){
        String s = neuron.ID.get();
        return s.length() <= 2? s: s.substring(0, 2);
    }

    @Override
    public boolean isRelative(Viewer other){
        return other == axon
               || ((other instanceof ReceptorViewer) && ((ReceptorViewer)other).host == this)
               || ((other instanceof SynapseViewer) && (((SynapseViewer)other).prevSynAxon == axon ||
                                                        ((SynapseViewer)other).postSynRep.host == this));
    }
}
