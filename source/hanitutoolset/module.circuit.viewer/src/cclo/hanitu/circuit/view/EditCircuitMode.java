/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import javafx.event.ActionEvent;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import cclo.hanitu.Base;
import cclo.hanitu.circuit.*;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.gui.MenuManager;
import cclo.hanitu.gui.MenuSession;
import cclo.hanitu.gui.MessageSession;

/**
 * @author antonio
 */
@SuppressWarnings({"WeakerAccess", "UnusedReturnValue"})
public class EditCircuitMode extends VisualCircuitMode{

    private final Properties menuProperties = Base.loadProperties(EditCircuitMode.class.getName() + ".menu");

    private CircuitBuilder builder;
    private Viewer selectedTarget;
    private final AtomicInteger neuronIDCounter = new AtomicInteger();

    @Override
    protected void setup(VisualCircuit visual){
        super.setup(visual);
        visual.resetMenu("edit");
        visual.setCircuit(visual.circuit);
        visual.setEditable(true);
    }

    @Override
    protected Circuit setCircuit(Circuit c){
        if (c != null){
            if (c != builder){
                builder = new CircuitBuilder(c);
            }
        } else {
            builder = new CircuitBuilder();
        }
        int last = builder.getNeuronIDSet().stream()
          .filter(id -> id.matches("\\d+"))
          .mapToInt(Integer::parseInt)
          .max().orElse(0);
        neuronIDCounter.set(last);
        return builder;
    }

    @Override
    public void setSaveFileName(Path filePath){
        super.setSaveFileName(filePath);
        if (visual != null){
            visual.file.setCurrentFileModified(true);
        }
    }

    @Override
    protected void processMenuEvent(ActionEvent e, String action){
        switch (action){
        case "menu.edit.newneu":
            createNewNeuron();
            break;
        case "menu.edit.newbox":
            createNewBox();
            break;
        case "menu.edit.package":
            packageNeuronIntoBox();
            break;
        case "menu.edit.newrep":
            createNewReceptor();
            break;
        case "menu.edit.newsyn":
            createSynapseBetween();
            break;
        case "menu.edit.extend":
            extendSelected(false);
            break;
        case "menu.edit.deepextend":
            extendSelected(true);
            break;
        case "menu.edit.duplicate":
            duplicateSelected();
            break;
        case "menu.edit.moveto":
            moveSelectToMousePosition();
            break;
        case "menu.edit.delete":
            deleteSelected();
            break;
        case "menu.edit.import":
            importBox();
            break;
        case "menu.edit.autojoin":
            visual.enableDragJoinNeuron = ((CheckMenuItem)e.getSource()).isSelected();
            break;
        case "menu.edit.join":
            tryJoinNeuronIntoBox();
            break;
        case "menu.edit.kick":
            tryKickNeuronFromBox();
            break;
        default:
            super.processMenuEvent(e, action);
        }
    }

    @Override
    protected boolean processKeyEvent(KeyEvent e){
        return processKeyboardSelect(e);
    }

    @Override
    protected void processClickEvent(MouseEvent e, Viewer target){
        if (e.isControlDown()){
            if (target != null){
                visual.appendSelect(target);
            }
        } else if (target != null){
            setSelected(target);
        } else {
            setSelectNone();
        }
        selectedTarget = target;
    }

    @Override
    protected void processRightClickEvent(MouseEvent e, Viewer target){
        if ((selectedTarget instanceof AxonViewer) && (target instanceof ReceptorViewer)){
            createNewSynapse((AxonViewer)selectedTarget, (ReceptorViewer)target, null);
        } else if (target == null){
            setSelectNone();
            showPopupMenu(null, e);
        } else {
            setSelected(target);
            showPopupMenu(target, e);
        }
        repaintCanvas();
    }

    @Override
    protected void processDragEvent(MouseEvent e, Viewer target){
        visual.file.setCurrentFileModified(true);
        dragViewer(e, target);
        setSelected(target);
        repaintCanvas();
    }

    @Override
    protected void processDragReleaseEvent(MouseEvent e, Viewer target){
        if (target instanceof NeuronViewer){
            NeuronViewer neuron = (NeuronViewer)target;
            if (visual.selectedBox == null){
                if (visual.enableDragJoinNeuron){
                    tryJoinNeuronIntoBox(neuron);
                }
            } else if (visual.paint != null && visual.paint.isTouch(visual.selectedBox, neuron.x, neuron.y) != 0){
                kickNeuron(neuron);
            }
        }
    }

    @Override
    protected void processRectangleSelectEvent(double x, double y, double w, double h){
        List<Viewer> select = getViewerInRectangle(x, y, w, h);
        if (!select.isEmpty()){
            setSelected(select);
        }
    }

    private String genBoxName(){
        Set<String> names = builder.getBoxNameSet();
        String name;
        int index = names.size();
        while (names.contains(name = "Box:" + index)){
            index++;
        }
        return name;
    }

    public void createNewBox(){
        new Thread(() -> {
            PositionData pos = visual.waitUserMouseClick();
            if (pos != null) createNewBox(pos.x, pos.y);
        }).start();
    }

    public BoxViewer createNewBox(double x, double y){
        visual.file.setCurrentFileModified(true);
        BoxViewer v = createBoxViewer(visual, genBoxName(), x, y);
        setSelected(v);
        return v;
    }

    public void deleteBox(BoxViewer box){
        LOG.debug("delete box : {}", box.name);
        visual.file.setCurrentFileModified(true);
        double x = box.x;
        double y = box.y;
        double s = visual.env.boxSize.get();
        visual.neuronViewers.values().stream()
          .filter(box::containNeuron)
          .peek(this::kickNeuron)
          .forEach(n -> n.setLocation(x + s * Math.random(), y + s * Math.random()));
        visual.boxViewers.remove(box.name);
        builder.removeBox(box.name);
    }

    public void tryJoinNeuronIntoBox(){
        Viewer viewer = visual.lastSelectedTarget.get();
        if (viewer instanceof NeuronViewer){
            tryJoinNeuronIntoBox((NeuronViewer)viewer);
        }
    }

    public void tryJoinNeuronIntoBox(NeuronViewer neuron){
        for (BoxViewer box : visual.boxViewers.values()){
            if (isOverlap(neuron, box)){
                joinNeuron(box, neuron);
                break;
            }
        }
    }

    public void joinNeuron(BoxViewer box, NeuronViewer n){
        Objects.requireNonNull(box);
        Objects.requireNonNull(n);
        LOG.debug("join {} into {}", n, box);
        visual.file.setCurrentFileModified(true);
        builder.joinBox(n.neuron, box.name);
        box.updateNeuronCountCache(builder); // update cache
        repaintCanvas();
    }

    public void joinNeuron(BoxViewer box, Collection<NeuronViewer> n){
        Objects.requireNonNull(box);
        if (n.isEmpty()) return;
        visual.file.setCurrentFileModified(true);
        for (NeuronViewer v : n){
            LOG.debug("join {} into {}", v, box);
            builder.joinBox(v.neuron, box.name);
        }
        box.updateNeuronCountCache(builder); // update cache
        repaintCanvas();
    }

    public void tryKickNeuronFromBox(){
        Viewer viewer = visual.lastSelectedTarget.get();
        if (viewer instanceof NeuronViewer){
            kickNeuron((NeuronViewer)viewer);
        }
    }

    public boolean kickNeuron(NeuronViewer n){
        BoxViewer box = visual.selectedBox;
        if (box == null) return false;
        visual.file.setCurrentFileModified(true);
        LOG.debug("kick neuron {} from box {}", n.neuron.ID, box.name);
        builder.kickBox(n.neuron);
        box.updateNeuronCountCache(builder); // update cache
        n.setLocation(box.x + 20, box.y + 20);
        repaintCanvas();
        return true;
    }

    public BoxViewer packageNeuronIntoBox(){
        return packageNeuronIntoBox(getSelectedNeurons());
    }

    public BoxViewer packageNeuronIntoBox(List<NeuronViewer> neurons){
        visual.file.setCurrentFileModified(true);
        PositionData c = getCentralPosition(new PositionData(), neurons);
        BoxViewer box = createBoxViewer(visual, genBoxName(), c);
        for (NeuronViewer n : neurons){
            LOG.debug("join {} into {}", n, box);
            builder.joinBox(n.neuron, box.name);
        }
        box.updateNeuronCountCache(builder); // update cache
        setSelected(box);
        repaintCanvas();
        return box;
    }

    public boolean setBoxAxon(AxonViewer<NeuronViewer> a){
        if (visual.selectedBox == null) return false;
        if (builder.setBoxAxon(a.host.neuron, true)){
            LOG.debug("set box axon : {}", a);
            visual.file.setCurrentFileModified(true);
            repaintCanvas();
            return true;
        }
        return false;
    }

    public boolean removeBoxAxon(AxonViewer<NeuronViewer> a){
        if (visual.selectedBox == null) return false;
        if (builder.setBoxAxon(a.host.neuron, false)){
            LOG.debug("unset box axon : {}", a);
            visual.file.setCurrentFileModified(true);
            repaintCanvas();
            return true;
        }
        return false;
    }

    public boolean setBoxReceptor(ReceptorViewer<NeuronViewer> r){
        if (visual.selectedBox == null) return false;
        if (r.receptor == null) return false;
        if (builder.setBoxReceptor(r.receptor, true)){
            LOG.debug("set box receptor : {}", r);
            visual.file.setCurrentFileModified(true);
            repaintCanvas();
            return true;
        }
        return false;
    }

    public boolean removeBoxReceptor(ReceptorViewer<NeuronViewer> r){
        if (visual.selectedBox == null) return false;
        if (r.receptor == null) return false;
        if (builder.setBoxReceptor(r.receptor, false)){
            LOG.debug("unset box receptor : {}", r);
            visual.file.setCurrentFileModified(true);
            repaintCanvas();
            return true;
        }
        return false;
    }

    public BoxViewer renameBox(BoxViewer box){
        Properties p = Base.loadProperties();
        String name = MessageSession.showInputDialog(p.getProperty("dialog.renamebox.title"),
                                                     p.getProperty("dialog.renamebox.message"),
                                                     box.name,
                                                     null);
        if (name == null) return null;
        builder.renameBox(box.name, name);
        LOG.debug("rename box {} -> {}", box.name, name);
        visual.file.setCurrentFileModified(true);
        visual.boxViewers.remove(box.name);
        BoxViewer newBox = createBoxViewer(visual, name, box);
        setSelected(newBox);
        return newBox;
    }

    public BoxViewer importBox(){
        Path path = visual.file.showOpenFileDialog();
        if (path == null) return null;
        try {
            CircuitLoader loader = new CircuitLoader();
            loader.setFailOnLoad(true);
            return importBox(loader.load(path));
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    public BoxViewer importBox(Circuit circuit){
        Properties p = Base.loadProperties();
        if (circuit.getBoxNameSet().isEmpty()){
            MessageSession.showConfirmDialog(p.getProperty("dialog.importbox.title"),
                                             p.getProperty("dialog.importbox.message.empty"),
                                             "K");
            return null;
        }
        String select = MessageSession.showListSelectDialog(p.getProperty("dialog.importbox.title"),
                                                            p.getProperty("dialog.importbox.message"),
                                                            new ArrayList<>(circuit.getBoxNameSet()),
                                                            0);
        if (select == null) return null;
        return importBox(circuit, select);
    }

    public BoxViewer importBox(Circuit circuit, String boxName){
        visual.file.setCurrentFileModified(true);
        silenceVisualEvent(true);
        BoxViewer box = null;
        try {
            Set<String> create = builder.cloneCircuit(circuit.subCircuit(circuit.getBoxNeuron(boxName)));
            reassignNeuronID(create);
            //
            List<NeuronViewer> neurons = createCircuitViewer(visual, builder, create);
            //
            box = createNewBox(visual.mousePositionX, visual.mousePositionY);
            joinNeuron(box, neurons);
        } finally {
            silenceVisualEvent(false);
        }
        return box;
    }

    private void reassignNeuronID(Set<String> create){
        Map<String, String> map = create.stream()
          .collect(Collectors.toMap(Function.identity(),
                                    _k -> Integer.toString(neuronIDCounter.incrementAndGet())));
        builder.reassignNeuronID(name -> map.getOrDefault(name, name));
        reassignNeuronViewerID();
        repaintCanvas();
    }

    public void reassignNeuronID(){
        visual.file.setCurrentFileModified(true);
        neuronIDCounter.set(0);
        Map<String, String> map = builder.getNeuronIDSet().stream()
          .sorted(Comparator.comparingInt(String::length).thenComparing(String::compareToIgnoreCase))
          .collect(Collectors.toMap(Function.identity(), _k -> Integer.toString(neuronIDCounter.getAndIncrement())));
        builder.reassignNeuronID(map::get);
        reassignNeuronViewerID();
        repaintCanvas();
    }

    private void reassignNeuronViewerID(){
        ArrayList<NeuronViewer> list = new ArrayList<>(visual.neuronViewers.values());
        visual.neuronViewers.clear();
        for (NeuronViewer v : list){
            visual.neuronViewers.put(v.neuron.ID.get(), v);
        }
    }

    public void createNewNeuron(){
        new Thread(() -> {
            PositionData pos = visual.waitUserMouseClick();
            if (pos != null) createNewNeuron(pos.x, pos.y, null, true);
        }).start();
    }

    public NeuronViewer createNewNeuron(double x, double y, CentralNeuron ref, boolean createReceptor){
        visual.file.setCurrentFileModified(true);
        String ID;
        do {
            ID = Integer.toString(neuronIDCounter.getAndIncrement());
        } while (visual.neuronViewers.containsKey(ID));
        NeuronViewer v = createNeuronViewer(visual, builder.createNeuron(ID, ref), x, y);
        if (createReceptor){
            createNewReceptor(v, null); //default create a new receptor
        }
        if (visual.selectedBox != null){
            joinNeuron(visual.selectedBox, v);
        }
        setSelected(v);
        return v;
    }

    public void createNewReceptor(){
        Viewer viewer = visual.lastSelectedTarget.get();
        if (viewer instanceof NeuronViewer){
            createNewReceptor((NeuronViewer)viewer, null);
        }
    }

    public ReceptorViewer createNewReceptor(NeuronViewer n, Receptor ref){
        visual.file.setCurrentFileModified(true);
        int count = n.updateReceptorCount(builder);
        Receptor r = builder.createReceptor(n.neuron.ID.get(), Integer.toString(count), ref);
        n.receptorCountCache++;
        ReceptorViewer v = createReceptorViewer(visual, n, r);
        v.locationCache = count;
        setSelected(v);
        return v;
    }

    public SynapseViewer createNewSynapse(AxonViewer axon, ReceptorViewer rep, Synapse ref){
        //Neuron -> Neuron
        //Neuron -> Motor
        //Sensor -> Neuron
        SynapseViewer ret = null;
        if ((axon.host instanceof NeuronViewer) && (rep.host instanceof NeuronViewer)){
            NeuronViewer prev = (NeuronViewer)axon.host;
            NeuronViewer post = (NeuronViewer)rep.host;
            // CircuitBuilder doesn't avoid that prev neuron connect to post neuron with many synapse which bind
            // to different receptor, but VisualCircuit doesn't allow this. Therefore, I need to check whether
            // prev neuron has connected to post neuron or not.
            if (prev != post && builder.getSynapse(prev.neuron, post.neuron) == null){
                visual.file.setCurrentFileModified(true);
                ret = createSynapseViewer(visual, builder.createSynapse(prev.neuron, rep.receptor, ref));
            }
        } else if ((axon.host instanceof NeuronViewer) && (rep.host instanceof MotorViewer)){
            NeuronViewer prev = (NeuronViewer)axon.host;
            MotorViewer post = (MotorViewer)rep.host;
            if (builder.getPreMotorSynapse(post.direction) != null){
                visual.file.setCurrentFileModified(true);
                visual.synapseViewers.removeIf(v -> v.postSynRep == post.rep);
            }
            ret = createSynapseViewer(visual, builder.setPreMotorSynapse(prev.neuron, post.direction));
        } else if ((axon.host instanceof SensorViewer) && (rep.host instanceof NeuronViewer)){
            SensorViewer prev = (SensorViewer)axon.host;
            NeuronViewer post = (NeuronViewer)rep.host;
            if (builder.getSensorSynapse(prev.sensor.type, prev.direction, post.neuron) == null){
                visual.file.setCurrentFileModified(true);
                ret = createSynapseViewer(visual,
                                          builder.createSensorSynapse(prev.type, prev.direction, rep.receptor, ref));
            }
        }
        if (ret != null){
            setSelected(ret);
        } else {
            setSelectNone();
        }
        return ret;
    }

    public SynapseViewer switchSynapseTargetReceptor(SynapseViewer v){
        if (v.postSynRep == null || !(v.postSynRep.host instanceof NeuronViewer)) return null;
        NeuronViewer neuron = (NeuronViewer)v.postSynRep.host;
        AxonViewer axon = v.prevSynAxon;
        List<ReceptorViewer> list = visual.getReceptorViewer(neuron).collect(Collectors.toList());
        Properties p = Base.loadProperties();
        ReceptorViewer rep = MessageSession.showListSelectDialog(p.getProperty("dialog.switch.title"),
                                                                 p.getProperty("dialog.switch.message"),
                                                                 list,
                                                                 list.indexOf(v.postSynRep),
                                                                 r -> r.receptor.ID.get());
        if (rep == null || rep == v.postSynRep) return null;
        deleteSynapse(v);
        return createNewSynapse(axon, rep, v.synapse);
    }


    public List<SynapseViewer> createSynapseBetween(){
        return createSynapseBetween(getSelectedNeurons());
    }

    public List<SynapseViewer> createSynapseBetween(List<NeuronViewer> neurons){
        visual.file.setCurrentFileModified(true);
        silenceVisualEvent(true);
        List<SynapseViewer> ret = new ArrayList<>();
        try {
            for (NeuronViewer prev : neurons){
                for (NeuronViewer post : neurons){
                    if (prev == post) continue;
                    if (builder.getSynapse(prev.neuron, post.neuron) != null) continue;
                    visual.getFirstReceptorViewer(post).ifPresent(r -> ret.add(createNewSynapse(prev.axon, r, null)));
                }
            }
        } finally {
            silenceVisualEvent(false);
        }
        setSelected(ret);
        return ret;
    }

    public void duplicateSelected(){
        if (visual.selectedViewers.size() > 1){
            List<NeuronViewer> source = getSelectedNeurons();
            if (source.isEmpty()) return;
            duplicateNeurons(source);
        } else {
            Viewer v = visual.lastSelectedTarget.get();
            if (v != null){
                if (v instanceof NeuronViewer){
                    v = duplicateNeuron((NeuronViewer)v);
                } else if (v instanceof BoxViewer){
                    v = duplicateBox(((BoxViewer)visual.lastSelectedTarget.get()));
                }
            }
            if (v != null) setSelected(v);
        }
    }

    /**
     * duplicate the reference neuron viewer. copy neuron parameters value and receptor
     * parameters value.
     *
     * @param source  reference neuron viewer
     * @return new neuron viewer
     */
    public NeuronViewer duplicateNeuron(NeuronViewer source){
        visual.file.setCurrentFileModified(true);
        String sID = source.neuron.ID.get();
        LOG.debug("duplicate {}", source);
        silenceVisualEvent(true);
        NeuronViewer viewer;
        try {
            CentralNeuron neuron = builder.cloneNeuron(sID);
            builder.reassignNeuronID(neuron.ID.get(), Integer.toString(neuronIDCounter.incrementAndGet()));
            viewer = createCircuitViewer(visual, builder, neuron.ID.get());
            viewer.setLocation(visual.mousePositionX, visual.mousePositionY);
        } finally {
            silenceVisualEvent(false);
        }
        setSelected(viewer);
        return viewer;
    }

    public List<NeuronViewer> duplicateNeurons(List<NeuronViewer> neurons){
        visual.file.setCurrentFileModified(true);
        silenceVisualEvent(true);
        List<NeuronViewer> target;
        try {
            double x = visual.mousePositionX;
            double y = visual.mousePositionY;
            PositionData c = getCentralPosition(new PositionData(), neurons);
            //
            Set<String> origin = neurons.stream().map(n -> n.neuron.ID.get()).collect(Collectors.toSet());
            Set<String> create = builder.cloneNeurons(origin);
            reassignNeuronID(create);
            //
            target = createCircuitViewer(visual, builder, create);
            target.forEach(n -> n.setLocation(x + n.x - c.x, y + n.y - c.y));
        } finally {
            silenceVisualEvent(false);
        }
        setSelected(target);
        repaintCanvas();
        return target;
    }

    public BoxViewer duplicateBox(BoxViewer box){
        Objects.requireNonNull(box, "duplicate target box");
        visual.file.setCurrentFileModified(true);
        silenceVisualEvent(true);
        BoxViewer viewer;
        try {
            String boxName = genBoxName();
            Set<String> create = builder.cloneBox(box.name, boxName);
            reassignNeuronID(create);
            //
            viewer = createBoxViewer(visual, boxName, visual.mousePositionX, visual.mousePositionY);
            createCircuitViewer(visual, builder, create);
            //
            viewer.updateNeuronCountCache(builder); // update cache
        } finally {
            silenceVisualEvent(false);
        }
        setSelected(viewer);
        repaintCanvas();
        return viewer;
    }

    public void extendSelected(boolean progress){
        extendViewer(visual.lastSelectedTarget.get(), progress);
    }

    public NeuronViewer extendViewer(Viewer v, boolean progress){
        NeuronViewer ret = null;
        if (v instanceof AxonViewer){
            AxonViewer axon = (AxonViewer)v;
            if (axon.host instanceof NeuronViewer){
                ret = createNewNeuron(visual.mousePositionX,
                                      visual.mousePositionY,
                                      ((NeuronViewer)axon.host).neuron,
                                      false);
                createNewSynapse(axon, createNewReceptor(ret, null), null);
                if (progress){
                    setSelected(ret.axon);
                } else {
                    setSelected(v);
                }
            }
        } else if (v instanceof ReceptorViewer){
            ReceptorViewer rep = (ReceptorViewer)v;
            if (rep.host instanceof NeuronViewer){
                ret = createNewNeuron(visual.mousePositionX,
                                      visual.mousePositionY,
                                      ((NeuronViewer)rep.host).neuron,
                                      false);
                ReceptorViewer receptor = createNewReceptor(ret, null);
                createNewSynapse(ret.axon, rep, null);
                if (progress){
                    setSelected(receptor);
                } else {
                    setSelected(v);
                }
            }
        }
        return ret;
    }

    public void moveSelectToMousePosition(){
        visual.file.setCurrentFileModified(true);
        double x = visual.mousePositionX;
        double y = visual.mousePositionY;
        List<NeuronViewer> list = getSelectedNeurons();
        if (list.isEmpty()) return;
        if (list.size() == 1){
            NeuronViewer v = list.get(0);
            v.setLocation(x, y);
            LOG.debug("set {} location to ({}, {})", v, x, y);
        } else {
            PositionData c = getCentralPosition(new PositionData(), list);
            LOG.debug("set location central ({}, {})", c.x, c.y);
            for (NeuronViewer v : list){
                v.setLocation(x + v.x - c.x, y + v.y - c.y);
                LOG.debug("set {} location to ({}, {})", v, v.x, v.y);
            }
        }
        repaintCanvas();
    }

    public void deleteSelected(){
        silenceVisualEvent(true);
        List<Viewer> delete = new ArrayList<>(visual.selectedViewers);
        getSelectedNeurons().forEach(this::deleteNeuron);
        for (Viewer v : delete){
            if (v instanceof ReceptorViewer){
                deleteReceptor((ReceptorViewer)v);
            } else if (v instanceof SynapseViewer){
                deleteSynapse((SynapseViewer)v);
            } else if (v instanceof BoxViewer){
                if (v != visual.selectedBox){
                    deleteBox(((BoxViewer)v));
                }
            }
        }
        silenceVisualEvent(false);
        reassignNeuronID();
        setSelectNone();
    }

    public void deleteNeuron(NeuronViewer n){
        Objects.requireNonNull(n, "neuron viewer be deleted");
        visual.file.setCurrentFileModified(true);
        // delete neuron
        if (visual.neuronViewers.remove(n.neuron.ID.get()) != null){
            LOG.debug("delete : {}", n);
        }
        // delete receptor
        visual.receptorViewers.removeIf(v -> {
            if (v.host == n){
                LOG.debug("delete : {}", v);
                return true;
            }
            return false;
        });
        // delete synapse
        visual.synapseViewers.removeIf(v -> {
            if (v.postSynRep.host instanceof MotorViewer){
                if (v.prevSynAxon.host == n){
                    LOG.debug("delete : {}", v);
                    return true;
                }
            } else if (v.prevSynAxon.host == n || v.postSynRep.host == n){
                LOG.debug("delete : {}", v);
                return true;
            }
            return false;
        });
        // delete
        builder.removeNeuron(n.neuron);
        // update box if need
        String box = n.neuron.metaBox.get();
        if (box != null){
            visual.getBoxViewer(box).ifPresent(b -> b.updateNeuronCountCache(builder));
        }
        //
        if (visual.silenceSelectedUpdate == 0) reassignNeuronID();
        //
        setSelectNone();
    }


    public void deleteReceptor(ReceptorViewer<NeuronViewer> receptor){
        if (!visual.receptorViewers.remove(receptor)) return;
        LOG.debug("delete : {}", receptor);
        visual.file.setCurrentFileModified(true);
        visual.synapseViewers.removeIf(v -> {
            if (v.postSynRep == receptor){
                LOG.debug("delete : {}", v);
                return true;
            }
            return false;
        });
        String neuronID = receptor.receptor.hostNeuID.get();
        builder.removeReceptor(receptor.receptor);
        //
        NeuronViewer v = visual.getNeuronViewer(neuronID).get();
        v.updateReceptorCount(builder);
        //
        List<ReceptorViewer<NeuronViewer>> rs = visual.getReceptorViewer(v)
          .sorted(Comparator.comparing(r -> r.receptor.ID.get()))
          .collect(Collectors.toList());
        int counter = 0;
        for (ReceptorViewer<NeuronViewer> nr : rs){
            nr.locationCache = counter++;
        }
        //
        setSelectNone();
    }

    public void deleteSynapse(SynapseViewer s){
        if (!visual.synapseViewers.remove(s)) return;
        visual.file.setCurrentFileModified(true);
        LOG.debug("delete : {}", s);
        AxonViewer axon = s.prevSynAxon;
        ReceptorViewer rep = s.postSynRep;
        if ((axon.host instanceof NeuronViewer) && (rep.host instanceof NeuronViewer)){
            builder.removeSynapse(s.synapse);
        } else if ((axon.host instanceof NeuronViewer) && (rep.host instanceof MotorViewer)){
            MotorViewer post = (MotorViewer)rep.host;
            builder.removeMotorSynapse(post.direction);
        } else if ((axon.host instanceof SensorViewer) && (rep.host instanceof NeuronViewer)){
            SensorViewer pre = (SensorViewer)axon.host;
            NeuronViewer post = (NeuronViewer)rep.host;
            builder.removeSensorSynapse(pre.sensor.type, pre.direction, post.neuron);
        }
        setSelectNone();
    }

    void showPopupMenu(Viewer v, MouseEvent e){
        ContextMenu menu = null;
        if (v == null){
            if (visual.selectedBox == null){
                menu = showWormBody(e);
            } else {
                menu = showBoxView(e);
            }
        } else if ((v instanceof NeuronViewer)){
            menu = showNeuron((NeuronViewer)v);
        } else if ((v instanceof ReceptorViewer)
                   && (((ReceptorViewer)v).host instanceof NeuronViewer)){
            menu = showReceptor((ReceptorViewer)v);
        } else if (v instanceof SynapseViewer){
            menu = showSynapse((SynapseViewer)v);
        } else if (v instanceof BoxViewer){
            menu = showBox((BoxViewer)v);
        } else if (v instanceof AxonViewer){

            menu = showAxon((AxonViewer)v);

        }
        if (menu != null){
            showPopupMenu(menu, e.getScreenX(), e.getScreenY());
        }
    }

    ContextMenu showBox(BoxViewer box){
        ContextMenu menu = new ContextMenu();
        MenuItem title = new MenuItem(box.name);
        title.setDisable(true);
        menu.getItems().add(title);
        menu.getItems().addAll(MenuManager.loadMenu(Base.loadProperties(), "popup.box", (event, action) -> {
            switch (action){
            case "popup.box.rename":
                renameBox(box);
                break;
            case "popup.box.delete":
                deleteBox(box);
                break;
            }
        }));
        return menu;
    }

    ContextMenu showNeuron(NeuronViewer n){
        BoxViewer box;
        if (visual.selectedBox == null){
            box = visual.boxViewers.values().stream()
              .filter(v -> isOverlap(n, v))
              .findFirst().orElse(null);
        } else {
            box = null;
        }
        ContextMenu menu = MenuManager.loadContextMenu(menuProperties, "popup.neuron", (event, action) -> {
            switch (action){
            case "popup.neuron.newreceptor":
                createNewReceptor(n, null);
                break;
            case "popup.neuron.join":
                joinNeuron(box, n);
                break;
            case "popup.neuron.kick":
                kickNeuron(n);
                break;
            case "popup.neuron.delete":
                deleteNeuron(n);
                break;
            }
        });
        if (visual.selectedBox == null){
            MenuSession.removeMenuItemById(menu.getItems(), "popup.neuron.kick");
        }
        if (box == null){
            MenuSession.removeMenuItemById(menu.getItems(), "popup.neuron.join");
        }
        return menu;
    }

    ContextMenu showReceptor(ReceptorViewer r){
        ContextMenu menu = MenuManager.loadContextMenu(menuProperties, "popup.receptor", (event, action) -> {
            switch (action){
            case "popup.receptor.ref":
                setBoxReceptor(r);
                break;
            case "popup.receptor.deref":
                removeBoxReceptor(r);
                break;
            case "popup.receptor.delete":
                deleteReceptor(r);
                break;
            }
        });
        if (visual.selectedBox == null){
            MenuSession.removeMenuItemById(menu.getItems(), "popup.receptor.ref");
            MenuSession.removeMenuItemById(menu.getItems(), "popup.receptor.deref");
            MenuSession.removeMenuItemById(menu.getItems(), "popup.receptor.syn");
        } else {
            if (visual.getReceptorOnBox(visual.selectedBox.name).anyMatch(v -> v == r)){
                MenuSession.removeMenuItemById(menu.getItems(), "popup.receptor.ref");
            } else {
                MenuSession.removeMenuItemById(menu.getItems(), "popup.receptor.deref");
            }
            Menu synMenu = (Menu)MenuSession.getMenuItemById(menu.getItems(), "popup.receptor.syn");
            if (synMenu != null){
                for (NeuronViewer n : visual.neuronViewers.values()){
                    if (!n.inBox()){
                        synMenu.getItems()
                          .add(MenuSession.createMenuItem(n.getText(), e -> createNewSynapse(n.axon, r, null)));
                    }
                }
            }
        }
        return menu;
    }

    ContextMenu showSynapse(SynapseViewer s){
        return MenuManager.loadContextMenu(menuProperties, "popup.synapse", (event, action) -> {
            switch (action){
            case "popup.synapse.switch":
                switchSynapseTargetReceptor(s);
                break;
            case "popup.synapse.delete":
                deleteSynapse(s);
                break;
            }
        });
    }

    ContextMenu showAxon(AxonViewer a){
        if (visual.selectedBox == null) return null;
        ContextMenu menu = MenuManager.loadContextMenu(menuProperties, "popup.axon", (event, action) -> {
            switch (action){
            case "popup.axon.ref":
                setBoxAxon(a);
                break;
            case "popup.axon.deref":
                removeBoxAxon(a);
                break;
            }
        });
        if (/*visual.selectedBox != null && */visual.getAxonOnBox(visual.selectedBox.name).anyMatch(v -> v == a)){
            MenuSession.removeMenuItemById(menu.getItems(), "popup.axon.ref");
        } else {
            MenuSession.removeMenuItemById(menu.getItems(), "popup.axon.deref");
        }
        Menu synMenu = (Menu)MenuSession.getMenuItemById(menu.getItems(), "popup.axon.syn");
        if (synMenu != null){
            for (NeuronViewer n : visual.neuronViewers.values()){
                if (!n.inBox()){
                    Menu neuMenu = new Menu(n.getText());
                    synMenu.getItems().add(neuMenu);
                    visual.getReceptorViewer(n)
                      .forEach(r -> neuMenu.getItems()
                        .add(MenuSession.createMenuItem(r.getText(), e -> createNewSynapse(a, r, null))));
                }
            }
        }
        return menu;
    }

    ContextMenu showWormBody(MouseEvent e){
        double x = visual.getXInPaintArea(e.getX());
        double y = visual.getYInPaintArea(e.getY());
        return MenuManager.loadContextMenu(menuProperties, "popup.body", (event, action) -> {
            switch (action){
            case "popup.body.newneuron":
                createNewNeuron(x, y, null, true);
                break;
            case "popup.body.newbox":
                createNewBox(x, y);
                break;
            }
        });
    }

    ContextMenu showBoxView(MouseEvent e){
        double x = visual.getXInPaintArea(e.getX());
        double y = visual.getYInPaintArea(e.getY());
        return MenuManager.loadContextMenu(menuProperties, "popup.boxview", (event, action) -> {
            switch (action){
            case "popup.boxview.newneuron":
                createNewNeuron(x, y, null, true);
                break;
            case "popup.boxview.rename":
                BoxViewer box = renameBox(visual.selectedBox);
                if (box != null){
                    visual.zoomInBox(box);
                    repaintCanvas();
                }
                break;
            }
        });
    }

    private List<NeuronViewer> getSelectedNeurons(){
        return visual.selectedViewers.stream()
          .filter(v -> (v instanceof NeuronViewer))
          .map(v -> (NeuronViewer)v)
          .collect(Collectors.toList());
    }

    private boolean isOverlap(NeuronViewer neuron, BoxViewer box){
        return AbstractCircuitPainter.isTaughtSurround(neuron, visual.env.boxSize.get(), box.x, box.y) == 0;
    }

    private static PositionData getCentralPosition(PositionData back, List<? extends PositionData> list){
        double x = 0;
        double y = 0;
        for (PositionData data : list){
            x += data.x;
            y += data.y;
        }
        back.setLocation(x / list.size(), y / list.size());
        return back;
    }

    public static NeuronViewer createNeuronViewer(VisualCircuit visual, CentralNeuron neuron){
        NeuronViewer viewer = new NeuronViewer(neuron);
        visual.LOG.trace("create {}", viewer);
        visual.neuronViewers.put(neuron.ID.get(), viewer);
        viewer.setLocation(neuron.metaX.get(), neuron.metaY.get());
        viewer.theta = neuron.metaTheta.get();
        viewer.receptorCountCache = 0;
        return viewer;
    }

    public static NeuronViewer createNeuronViewer(VisualCircuit visual, CentralNeuron neuron, double x, double y){
        NeuronViewer viewer = new NeuronViewer(neuron);
        visual.LOG.trace("create {}", viewer);
        visual.neuronViewers.put(neuron.ID.get(), viewer);
        viewer.setLocation(x, y);
        viewer.receptorCountCache = 0;
        return viewer;
    }

    public static ReceptorViewer<NeuronViewer> createReceptorViewer(VisualCircuit visual,
                                                                    NeuronViewer neuron,
                                                                    Receptor receptor){
        ReceptorViewer viewer = new ReceptorViewer(neuron, receptor);
        visual.LOG.trace("create {}", viewer);
        visual.receptorViewers.add(viewer);
        return viewer;
    }


    public static SynapseViewer createSynapseViewer(VisualCircuit visual, Synapse synapse){
        String prev = synapse.prevSynNeuID.get();
        String post = synapse.postSynNeuID.get();
        AxonViewer axon;
        ReceptorViewer rep;
        if (CircuitInfo.isSensorID(prev)){
            int id = resolveBodyNeuronID(prev);
            if (id < 0){
                throw new RuntimeException("prev-synapse neuron ID : " + prev);
            }
            axon = visual.sensorViewer[id].axon;
        } else {
            axon = visual.getAxonViewer(prev)
              .orElseThrow(() -> new RuntimeException("prev-synapse neuron ID : " + prev));
        }
        if (CircuitInfo.isMotorID(post)){
            int id = resolveBodyNeuronID(post);
            if (id < 0){
                throw new RuntimeException("post-synapse neuron ID : " + post);
            }
            rep = visual.motorViewer[id - 9].rep;
        } else {
            rep = visual.getReceptorViewer(post, synapse.targetRepID.get())
              .orElseThrow(() -> new RuntimeException("prev-synapse neuron ID : " + prev));
        }
        SynapseViewer viewer = new SynapseViewer(synapse, axon, rep);
        visual.LOG.trace("create {}", viewer);
        visual.synapseViewers.add(viewer);
        return viewer;
    }

    public static void createAllSynapseViewer(VisualCircuit visual, Circuit circuit){
        for (Synapse synapse : circuit.synapses()){
            createSynapseViewer(visual, synapse);
        }
    }

    private static int resolveBodyNeuronID(String ID){
        int base = 0;
        int ret = -1;
        if (ID.startsWith(CircuitInfo.SENSOR)){
            ID = ID.substring(CircuitInfo.SENSOR.length());
            if (ID.startsWith("food.")){
                ret = Integer.parseInt(ID.substring(5));
            } else if (ID.startsWith("toxicant.")){
                ret = Integer.parseInt(ID.substring(9));
                base = 4;
            } else if (ID.startsWith("npy")){
                ret = 0;
                base = 8;
            }
        } else if (ID.startsWith(CircuitInfo.MOTOR)){
            ret = Integer.parseInt(ID.substring(CircuitInfo.MOTOR.length()));
            base = 9;
        }
        return base + ret;
    }

    public static BoxViewer createBoxViewer(VisualCircuit visual, String boxName, double x, double y){
        BoxViewer viewer = new BoxViewer(boxName);
        visual.LOG.trace("create {}", viewer);
        visual.boxViewers.put(boxName, viewer);
        viewer.setLocation(x, y);
        return viewer;
    }

    public static BoxViewer createBoxViewer(VisualCircuit visual, String boxName, PositionData position){
        BoxViewer viewer = new BoxViewer(boxName);
        visual.LOG.trace("create {}", viewer);
        viewer.updateNeuronCountCache(visual.circuit);
        visual.boxViewers.put(boxName, viewer);
        if (position != null) viewer.setLocation(position);
        return viewer;
    }

    public static void createAllBoxViewer(VisualCircuit visual, Circuit circuit){
        for (String name : circuit.getBoxNameSet()){
            createBoxViewer(visual, name, circuit.getBoxMeta(name));
        }
    }

    public static NeuronViewer createCircuitViewer(VisualCircuit visual, Circuit circuit, String neuron){
        NeuronViewer n = createNeuronViewer(visual, circuit.getNeuron(neuron));
        //
        List<Receptor> receptors = circuit.getReceptor(neuron);
        n.receptorCountCache = receptors.size();
        int counter = 0;
        for (Receptor receptor : receptors){
            ReceptorViewer<NeuronViewer> r = createReceptorViewer(visual, n, receptor);
            r.locationCache = counter++;
        }
        //
        for (Synapse synapse : circuit.getSynapseRelate(neuron)){
            createSynapseViewer(visual, synapse);
        }
        //
        return n;
    }

    public static List<NeuronViewer> createCircuitViewer(VisualCircuit visual, Circuit circuit, Set<String> neurons){
        List<NeuronViewer> list = new ArrayList<>(neurons.size());
        for (String neuron : neurons){
            NeuronViewer n = createNeuronViewer(visual, circuit.getNeuron(neuron));
            list.add(n);
            //
            List<Receptor> receptors = circuit.getReceptor(neuron);
            n.receptorCountCache = receptors.size();
            int counter = 0;
            for (Receptor receptor : receptors){
                ReceptorViewer<NeuronViewer> r = createReceptorViewer(visual, n, receptor);
                r.locationCache = counter++;
            }
        }
        //
        for (Synapse synapse : circuit.getSynapseRelate(neurons)){
            createSynapseViewer(visual, synapse);
        }
        //
        return list;
    }


}
