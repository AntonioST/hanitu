/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import cclo.hanitu.circuit.Sensor;
import cclo.hanitu.data.Direction;
import cclo.hanitu.data.SensorType;

/**
 * the viewer for sensor
 *
 * @author antonio
 */
public class SensorViewer extends Viewer{

    final Sensor sensor;
    final SensorType type;
    final Direction direction;
    final AxonViewer<SensorViewer> axon;
    private final String text;

    public SensorViewer(Sensor sensor, SensorType type, Direction d){
        this.sensor = sensor;
        this.type = type;
        direction = d;
        axon = new AxonViewer(this);
        if (d != Direction.SILENCE){
            theta = Direction.getTheta(Direction.opposite(d));
        }
        if (type == SensorType.NPY){
            text = SensorType.NPY.name();
        } else {
            text = String.format("S%c%c", type.name().charAt(0), d.name().charAt(0));
        }
    }

    public void writeMeta(){
        sensor.metaX.set((int)x);
        sensor.metaY.set((int)y);
        sensor.metaTheta.set(theta);
    }

    @Override
    public String getText(){
        return text;
    }

    @Override
    public boolean isRelative(Viewer other){
        return other == axon ||
               ((other instanceof SynapseViewer) && ((SynapseViewer)other).prevSynAxon == axon);
    }
}
