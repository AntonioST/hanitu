/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.circuit.view;

import java.util.Objects;

import cclo.hanitu.circuit.Receptor;

/**
 * the viewer for receptor.
 *
 * @author antonio
 */
public class ReceptorViewer<V extends Viewer> extends Viewer{

    final Receptor receptor;
    final V host;
    //cache
    boolean visibleCache;
    int locationCache;

    public ReceptorViewer(V host, Receptor receptor){
        this.host = Objects.requireNonNull(host, "receptor host");
        this.receptor = receptor; // null iff host is a MotorViewer
    }

    @Override
    public String getText(){
        return receptor == null? host.getText(): receptor.ID.get();
    }

    @Override
    public boolean isRelative(Viewer other){
        return other == host ||
               ((other instanceof SynapseViewer) && ((SynapseViewer)other).postSynRep == this);
    }

    @Override
    public String toString(){
        return getClass().getSimpleName() + ":" + host.getText() + "[" + getText() + "]";
    }
}
