/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

/**
 * a data class contain the information of time and firing rate value.
 *
 * @author antonio
 */
public class FiringRateData implements TimeSerialData{

    public final int time;
    /**
     * firing rate in unit Hz
     */
    public final double rate;

    public FiringRateData(int time, double rate){
        this.time = time;
        this.rate = rate;
    }

    @Override
    public int getTime(){
        return time;
    }

    @Override
    public String toString(){
        return String.format("%.4f\t%f", time / 1000.0, rate);
    }
}
