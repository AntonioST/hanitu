/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.io.Serializable;

/**
 * data class for storing location information.
 *
 * @author antonio
 */
public class PositionData implements Serializable{

    /**
     * x location in unit 0.1mm
     */
    public double x;
    /**
     * y location in unit 0.1mm
     */
    public double y;

    /**
     * create data with zero value location
     */
    public PositionData(){
    }

    /**
     * create data and copy data from reference.
     *
     * @param p reference location
     */
    public PositionData(PositionData p){
        x = p.x;
        y = p.y;
    }

    /**
     * create data with given location information
     *
     * @param x x location
     * @param y y location
     */
    public PositionData(double x, double y){
        this.x = x;
        this.y = y;
    }

    public void setLocation(double x, double y){
        this.x = x;
        this.y = y;
    }

    public void setLocation(PositionData pos){
        x = pos.x;
        y = pos.y;
    }

    public void swapLocation(PositionData pos){
        double t = pos.x;
        pos.x = x;
        x = t;
        t = pos.y;
        pos.y = y;
        y = t;
    }

    /**
     * Does then distance between self and (`x`, `y`) is less then `size`
     *
     * @param x    another x location
     * @param y    another y location
     * @param size distance threshold, in unit 0.1mm
     * @return true if contacted
     */
    public boolean contact(double x, double y, int size){
        double dx = Math.abs(x - this.x);
        double dy = Math.abs(y - this.y);
        return !(dx > size || dy > size) &&
               dx * dx + dy * dy < size * size;
    }

// --Commented out by Inspection START (3/4/16 6:06 PM):
//    public PositionData move(Direction dir, double delta){
//        switch (dir){
//        case FORWARD:
//            y -= delta;
//            break;
//        case BACKWARD:
//            y += delta;
//            break;
//        case LEFTWARD:
//            x -= delta;
//            break;
//        case RIGHTWARD:
//            x += delta;
//            break;
//        case SILENCE:
//        default:
//        }
//        return this;
//    }
// --Commented out by Inspection STOP (3/4/16 6:06 PM)

    @Override
    public String toString(){
        return String.format("data@[%.2f,%.2f]", x, y);
    }
}
