/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import javafx.beans.property.ReadOnlyIntegerProperty;

import cclo.hanitu.io.TailStream;

/**
 * @author antonio
 */
public class FiringrateResult extends FiringrateFunction implements TimeDataHandler, SpikeDataHandler{

    private int currentTime;
    private TailStream<SpikeData> source;
    private SpikeData lastSpikeData;
    private final Map<SingleSpikeFilter, LinkedList<SpikeData>> queue = new HashMap<>();

    @Override
    public void setSpikeStream(TailStream<SpikeData> source){
        this.source = Objects.requireNonNull(source);
        if (source.hasNext()){
            lastSpikeData = source.next();
            currentTime = lastSpikeData.getTime();
        }
    }

    @Override
    public void closeSpikeStream(){
        //noinspection EmptyCatchBlock
        try {
            source.close();
        } catch (IOException e){
        }
    }

    public static TailStream<SnapshotResult> asStream(FiringrateFunction function, TailStream<SpikeData> source) {
        FiringrateResult self = new FiringrateResult();
        self.set(function);
        self.setSpikeStream(source);
        return new TailStream<SnapshotResult>(){
            SnapshotResult result = null;

            @Override
            public boolean hasNext(){
                if (result == null){
                    if (self.next()) result = self.snapshot();
                }
                return result != null;
            }

            @Override
            public SnapshotResult next(){
                if (result == null) hasNext();
                SnapshotResult ret = result;
                result = null;
                return ret;
            }

            @Override
            public void close() throws IOException{
                source.close();
            }
        };
    }

    public boolean next(){
        currentTime += getTimeStep();
        int lb = getTimeLowerBoundary();
        int rb = getTimeUpperBoundary();
        queue.values().forEach(q -> q.removeIf(d -> d.time < lb));
        if (lastSpikeData == null || lastSpikeData.time < rb){
            if (lastSpikeData != null) addQueue(lastSpikeData);
            SpikeData data = null;
            while (source.hasNext()){
                data = source.next();
                if (data.time > rb) break;
                addQueue(data);
            }
            lastSpikeData = data;
        }
        queue.values().removeIf(AbstractCollection::isEmpty);
        return source.hasNext() || lastSpikeData != null || !queue.isEmpty();
    }

    private void addQueue(SpikeData data){
        for (SingleSpikeFilter filter : queue.keySet()){
            if (filter.test(data)){
                queue.get(filter).add(data);
                return;
            }
        }
        LinkedList<SpikeData> list = new LinkedList<>();
        list.add(data);
        queue.put(new SingleSpikeFilter(data), list);
    }

    @Override
    public int getCurrentTime(){
        return currentTime;
    }

    @Override
    public ReadOnlyIntegerProperty getTimeProperty(){
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean updateUntilTime(int time){
        if (currentTime < time){
            while (next()){
                if (currentTime >= time) break;
            }
        }
        return currentTime >= time;
    }

    @SuppressWarnings("WeakerAccess")
    public int getTimeUpperBoundary(){
        return currentTime + getTimeWindow() / 2;
    }

    @SuppressWarnings("WeakerAccess")
    public int getTimeLowerBoundary(){
        return currentTime - getTimeWindow() / 2;
    }

    public Set<SingleSpikeFilter> getFilters(){
        return queue.keySet();
    }

    @SuppressWarnings("unused")
    public Set<SingleSpikeFilter> getFilters(String user){
        Objects.requireNonNull(user);
        return queue.keySet().stream().filter(f -> user.equals(f.user)).collect(Collectors.toSet());
    }

    @SuppressWarnings("unused")
    public Set<SingleSpikeFilter> getFilters(String user, String worm){
        Objects.requireNonNull(user);
        Objects.requireNonNull(worm);
        return queue.keySet().stream()
          .filter(f -> user.equals(f.user))
          .filter(f -> worm.equals(f.worm))
          .collect(Collectors.toSet());
    }

    @SuppressWarnings("unused")
    public boolean contains(SingleSpikeFilter filter){
        return queue.containsKey(filter);
    }

    public FiringRateData getFiringrate(SingleSpikeFilter filter){
        LinkedList<SpikeData> q = queue.get(filter);
        if (q == null) return new FiringRateData(currentTime, 0);
        return new FiringRateData(currentTime, rate(currentTime, q));
    }

    public SnapshotResult snapshot(){
        return new SnapshotResult();
    }

    public class SnapshotResult implements TimeSerialData{

        private final int snapshotCurrentTime = currentTime;
        private final Map<SingleSpikeFilter, FiringRateData> result = new HashMap<>(queue.size());

        {
            Map<SingleSpikeFilter, List<SpikeData>> tmp = new HashMap<>();
            queue.forEach((f, q) -> tmp.put(f, new ArrayList<>(q)));
            tmp.forEach((f, q) -> {
                if (!q.isEmpty()){
                    result.put(f, new FiringRateData(snapshotCurrentTime,
                                                     rate(snapshotCurrentTime, q)));
                }
            });
        }

        @Override
        public int getTime(){
            return snapshotCurrentTime;
        }

        public Set<SingleSpikeFilter> getFilters(){
            return result.keySet();
        }

        @SuppressWarnings("unused")
        public Set<SingleSpikeFilter> getFilters(String user){
            Objects.requireNonNull(user);
            return result.keySet().stream().filter(f -> user.equals(f.user)).collect(Collectors.toSet());
        }

        @SuppressWarnings("unused")
        public Set<SingleSpikeFilter> getFilters(String user, String worm){
            Objects.requireNonNull(user);
            Objects.requireNonNull(worm);
            return result.keySet().stream()
              .filter(f -> user.equals(f.user))
              .filter(f -> worm.equals(f.worm))
              .collect(Collectors.toSet());
        }

        @SuppressWarnings("unused")
        public boolean contains(SingleSpikeFilter filter){
            return result.containsKey(filter);
        }

        public FiringRateData getFiringrate(SingleSpikeFilter filter){
            FiringRateData f = result.get(filter);
            if (f == null) return new FiringRateData(snapshotCurrentTime, 0);
            return f;
        }

        @Override
        public String toString(){
            StringBuilder builder = new StringBuilder();
            builder.append("time ").append(snapshotCurrentTime).append(" {");
            result.forEach((f, r) -> builder.append(f).append(":").append(r.rate).append(" "));
            return builder.append("}").toString();
        }
    }
}
