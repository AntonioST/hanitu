/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Objects;

/**
 * @author antonio
 */
public class Tuple<T, U>{

    public final T c1;
    public final U c2;

    public Tuple(T c1, U c2){
        this.c1 = Objects.requireNonNull(c1);
        this.c2 = Objects.requireNonNull(c2);
    }

    public T left(){
        return c1;
    }

    public U right(){
        return c2;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tuple<?, ?> tuple = (Tuple<?, ?>)o;

        return c1.equals(tuple.c1) && c2.equals(tuple.c2);

    }

    @Override
    public int hashCode(){
        int result = c1.hashCode();
        result = 31 * result + c2.hashCode();
        return result;
    }

    @Override
    public String toString(){
        return "(" + c1 + "," + c2 + ")";
    }
}
