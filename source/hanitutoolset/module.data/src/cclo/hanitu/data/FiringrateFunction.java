/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.io.IOException;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Objects;

import cclo.hanitu.io.TailStream;

import static java.util.Objects.requireNonNull;

/**
 * @author antonio
 */
public class FiringrateFunction{

    /**
     * time window size in unit msec.
     */
    private int timeWindow = 1000;
    /**
     * unit msec
     */
    private int timeStep = 100;
    private FiringRateScalar scalar = FiringRateScalar.uniform();

    public FiringrateFunction(){
    }

    public FiringrateFunction(int timeWindow, int timeStep){
        this.timeWindow = timeWindow;
        this.timeStep = timeStep;
    }

    @SuppressWarnings("unused")
    public FiringrateFunction(int timeWindow, int timeStep, FiringRateScalar scalar){
        this.timeWindow = timeWindow;
        this.timeStep = timeStep;
        this.scalar = scalar;
    }

    public int getTimeWindow(){
        return timeWindow;
    }

    public void setTimeWindow(int timeWindow){
        if (timeWindow <= 0) throw new IllegalArgumentException("negative or zero time window size");
        this.timeWindow = timeWindow;
    }

    public int getTimeStep(){
        return timeStep;
    }

    public void setTimeStep(int timeStep){
        if (timeStep <= 0) throw new IllegalArgumentException("negative or zero time step mount");
        this.timeStep = timeStep;
    }

    public FiringRateScalar getScalar(){
        return scalar;
    }

    public void setScalar(FiringRateScalar scalar){
        this.scalar = requireNonNull(scalar);
    }

    @SuppressWarnings("WeakerAccess")
    public void set(FiringrateFunction function){
        timeWindow = function.timeWindow;
        timeStep = function.timeStep;
        scalar = function.scalar;
    }

    protected double rate(double currentTime, Collection<SpikeData> list){
        if (list.isEmpty()) return 0;
        return 1000 * list.stream()
          .mapToDouble(spike -> scalar.rate(currentTime, spike.time, timeWindow))
          .sum() / timeWindow;
    }

    public TailStream<FiringRateData> apply(TailStream<SpikeData> source){
        Objects.requireNonNull(source);
        return new TailStream<FiringRateData>(){
            final Deque<SpikeData> queue = new LinkedList<>();
            private SpikeData lastSpikeData;
            private int current;

            {
                if (source.hasNext()){
                    lastSpikeData = source.next();
                    current = (int)lastSpikeData.time;
                }
            }

            @Override
            public boolean hasNext(){
                return source.hasNext() || !queue.isEmpty() || lastSpikeData != null;
            }

            @Override
            public FiringRateData next(){
                int window = timeWindow;
                int lb = current - window / 2; // left boundary
                int rb = current + window / 2; // right boundary
                while (!queue.isEmpty()){
                    if (queue.getFirst().time < lb){
                        queue.removeFirst();
                    } else {
                        break;
                    }
                }
                if (lastSpikeData == null || lastSpikeData.time < rb){
                    if (lastSpikeData != null) queue.add(lastSpikeData);
                    SpikeData data = null;
                    while (source.hasNext()){
                        data = source.next();
                        if (data.time > rb) break;
                        queue.add(data);
                    }
                    lastSpikeData = data;
                }
                FiringRateData data = new FiringRateData(current, rate(current, queue));
                current += timeStep;
                return data;
            }

            @Override
            public void close() throws IOException{
                source.close();
            }
        };
    }
}
