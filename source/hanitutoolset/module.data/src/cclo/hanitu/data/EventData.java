/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Objects;

/**
 * @author antonio
 */
public class EventData implements TimeSerialData{

    /**
     * event time in unit ms.
     */
    public final int time;
    public final String user;
    public final String worm;
    public final EventType type;

    private EventData(int time, String user, String worm, EventType type){
        this.time = time;
        this.user = user;
        this.worm = worm;
        this.type = type;
    }

    @Override
    public int getTime(){
        return time;
    }

    public static EventData parseLine(String line){
        if (line == null || line.isEmpty()){
            throw new IllegalArgumentException("null or empty line");
        }
        String[] part = line.split("(\t| +)");
        if (part.length < 4){
            throw new IllegalArgumentException("uncompleted event : " + line);
        }
        int time = Integer.parseInt(part[0]);
        String user = part[1];
        String worm = part[2];
        EventType type = EventType.getEventType(part[3]);
        switch (type){
        case MOVE_UP:
        case MOVE_DOWN:
        case MOVE_LEFT:
        case MOVE_RIGHT:
            // time user worm type -
            if (part.length == 5 && part[4].equals("-")){
                return new MoveEvent(time, user, worm, type);
            } else {
                throw new RuntimeException("wrong move event : " + line);
            }
        case TOUCH_FOOD:
        case TOUCH_TOXICANT:
            // time user worm type ID dHp
            if (part.length != 6){
                throw new RuntimeException("wrong eating event : " + line);
            } else if (part[5].equals("HP-full")){
                return new MoleculeEvent(time, user, worm, type, part[4], Double.NaN);
            } else {
                return new MoleculeEvent(time, user, worm, type, part[4], Double.parseDouble(part[5]));
            }
        case WORM_DIE:
            // time user worm x -
            if (part.length == 5 && part[4].equals("-")){
                return new WormDieEvent(time, user, worm);
            } else {
                throw new RuntimeException("wrong die event : " + line);
            }
        case TOUCH_WALL:
            // time user worm b dir
            if (part.length != 5){
                throw new RuntimeException("wrong touch event : " + line);
            } else {
                Direction direction;
                try {
                    direction = MoveEvent.getDirection(part[4]);
                } catch (RuntimeException e){
                    throw new RuntimeException("wrong touch event : " + line, e);
                }
                return new WormTouchWallEvent(time, user, worm, direction);
            }
        case TOUCH_WORM:
            // time user worm m user worm
            if (part.length != 6){
                throw new RuntimeException("wrong touch event : " + line);
            } else {
                return new WormTouchWormEvent(time, user, worm, part[4], part[5]);
            }
        default:
            throw new RuntimeException("un-supported event");
        }
    }

    @Override
    public String toString(){
        return String.format("%d %s %s %c %s", time, user, worm, type.symbol, getEventInfString());
    }

    String getEventInfString(){
        return "-";
    }

    public static class MoveEvent extends EventData{

        public MoveEvent(int time, String user, String worm, EventType type){
            super(time, user, worm, getDirection(getDirection(type)));
        }

        public MoveEvent(int time, String user, String worm, Direction direction){
            super(time, user, worm, getDirection(direction));
        }

        public Direction getDirection(){
            return getDirection(type);
        }

        public static Direction getDirection(EventType type){
            switch (type){
            case MOVE_UP:
                return Direction.FORWARD;
            case MOVE_DOWN:
                return Direction.BACKWARD;
            case MOVE_LEFT:
                return Direction.LEFTWARD;
            case MOVE_RIGHT:
                return Direction.RIGHTWARD;
            default:
                throw new RuntimeException("cannot map event type to direction : " + type);
            }
        }

        public static Direction getDirection(char d){
            switch (d){
            case 'u':
                return Direction.FORWARD;
            case 'd':
                return Direction.BACKWARD;
            case 'l':
                return Direction.LEFTWARD;
            case 'r':
                return Direction.RIGHTWARD;
            default:
                throw new RuntimeException("not a direction : " + d);
            }
        }

        public static Direction getDirection(String d){
            if (d.length() == 1) return getDirection(d.charAt(0));
            throw new RuntimeException("not a direction : " + d);
        }

        public static EventType getDirection(Direction direction){
            switch (direction){
            case FORWARD:
                return EventType.MOVE_UP;
            case BACKWARD:
                return EventType.MOVE_DOWN;
            case LEFTWARD:
                return EventType.MOVE_LEFT;
            case RIGHTWARD:
                return EventType.MOVE_RIGHT;
            default:
                throw new RuntimeException();
            }
        }
    }

    public static class MoleculeEvent extends EventData{

        private final String ID;
        private final double deltaHp;

        public MoleculeEvent(int time, String user, String worm, EventType molecule, String ID, double deltaHp){
            super(time, user, worm, getEventType(getMoleculeType(molecule)));
            this.ID = Objects.requireNonNull(ID);
            this.deltaHp = Objects.requireNonNull(deltaHp);
        }

        public MoleculeEvent(int time, String user, String worm, MoleculeType molecule, String ID, double deltaHp){
            super(time, user, worm, getEventType(molecule));
            this.ID = Objects.requireNonNull(ID);
            this.deltaHp = Objects.requireNonNull(deltaHp);
        }

        public MoleculeType getMoleculeType(){
            return getMoleculeType(type);
        }

        public static MoleculeType getMoleculeType(EventType type){
            switch (type){
            case TOUCH_FOOD:
                return MoleculeType.FOOD;
            case TOUCH_TOXICANT:
                return MoleculeType.TOXICANT;
            default:
                throw new RuntimeException();
            }
        }

        public static EventType getEventType(MoleculeType type){
            switch (type){
            case FOOD:
                return EventType.TOUCH_FOOD;
            case TOXICANT:
                return EventType.TOUCH_TOXICANT;
            default:
                throw new RuntimeException();
            }
        }

        public String getMoleculeID(){
            return ID;
        }

        @SuppressWarnings("unused")
        public double getDeltaHealthPoint(){
            return deltaHp;
        }

        public boolean isHpFull(){
            return type == EventType.TOUCH_FOOD && deltaHp == Double.NaN;
        }

        @Override
        protected String getEventInfString(){
            switch (type){
            case TOUCH_FOOD:
                if (isHpFull()) return "HP-full";
                return String.format("%+f", deltaHp);
            case TOUCH_TOXICANT:
                return String.format("%-f", deltaHp);
            default:
                throw new RuntimeException();
            }
        }
    }

    public static class WormDieEvent extends EventData{

        public WormDieEvent(int time, String user, String worm){
            super(time, user, worm, EventType.WORM_DIE);
        }
    }

    public static class WormTouchWormEvent extends EventData{

        private final String otherUser;
        private final String otherWorm;

        public WormTouchWormEvent(int time, String user, String worm, String otherUser, String otherWorm){
            super(time, user, worm, EventType.TOUCH_WORM);
            this.otherUser = otherUser;
            this.otherWorm = otherWorm;
        }

        @SuppressWarnings("unused")
        public String getUser(){
            return otherUser;
        }

        @SuppressWarnings("unused")
        public String getWorm(){
            return otherWorm;
        }

        @Override
        protected String getEventInfString(){
            return String.format("%s %s", otherUser, otherWorm);
        }
    }

    public static class WormTouchWallEvent extends EventData{

        private final Direction direction;

        public WormTouchWallEvent(int time, String user, String worm, Direction direction){
            super(time, user, worm, EventType.TOUCH_WALL);
            this.direction = direction;
        }

        public Direction getDirection(){
            return direction;
        }

        @Override
        protected String getEventInfString(){
            switch (direction){
            case FORWARD:
                return "u";
            case BACKWARD:
                return "d";
            case LEFTWARD:
                return "l";
            case RIGHTWARD:
                return "r";
            default:
                throw new RuntimeException();
            }
        }
    }
}
