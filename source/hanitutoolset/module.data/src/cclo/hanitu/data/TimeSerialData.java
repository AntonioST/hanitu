/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Objects;
import java.util.function.Consumer;

import cclo.hanitu.io.TailHistoryStream;

/**
 * @author antonio
 */
public interface TimeSerialData extends Comparable<TimeSerialData>{

    int getTime();

    @Override
    default int compareTo(TimeSerialData o){
        if (o == null) return 1;
        return Long.compare(getTime(), o.getTime());
    }

    static <T extends TimeSerialData> void untilTime(TailHistoryStream<T> source,
                                                     int time,
                                                     Consumer<T> consumer){
        Objects.requireNonNull(consumer);
        T t = source.get();
        if (t == null && source.hasNext()) t = source.next();
        while (t != null && t.getTime() < time){
            consumer.accept(t);
            t = source.next();
        }
    }

    static <T extends TimeSerialData> T getAfterTime(TailHistoryStream<T> source, int time){
        return getAfterTime(source, time, null);
    }

    static <T extends TimeSerialData> T getAfterTime(TailHistoryStream<T> source, int time, Consumer<T> consumer){
        T t = source.get();
        if (t != null && t.getTime() > time) return null;
        if (t == null && source.hasNext()) t = source.next();
        while (t != null && t.getTime() < time){
            if (consumer != null) consumer.accept(t);
            t = source.next();
        }
        return t;
    }
}
