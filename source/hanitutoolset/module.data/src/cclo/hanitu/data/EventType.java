/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

/**
 * @author antonio
 */
public enum EventType{
    MOVE_UP('u'),
    MOVE_DOWN('d'),
    MOVE_LEFT('l'),
    MOVE_RIGHT('r'),
    TOUCH_FOOD('f'),
    TOUCH_TOXICANT('t'),
    WORM_DIE('x'),
    TOUCH_WALL('b'),
    TOUCH_WORM('m'),
    NONE('\0');

    final char symbol;

    EventType(char symbol){
        this.symbol = symbol;
    }

    @SuppressWarnings("WeakerAccess")
    public static EventType getEventType(char c){
        switch (c){
        case 'u':
            return MOVE_UP;
        case 'd':
            return MOVE_DOWN;
        case 'l':
            return MOVE_LEFT;
        case 'r':
            return MOVE_RIGHT;
        case 'f':
            return TOUCH_FOOD;
        case 't':
            return TOUCH_TOXICANT;
        case 'x':
            return WORM_DIE;
        case 'b':
            return TOUCH_WALL;
        case 'm':
            return TOUCH_WORM;
        default:
            return NONE;
        }
    }

    public static EventType getEventType(String t){
        return (t.length() != 1)? NONE: getEventType(t.charAt(0));
    }
}
