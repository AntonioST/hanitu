/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import javafx.beans.InvalidationListener;
import javafx.beans.property.ReadOnlyObjectPropertyBase;
import javafx.beans.value.ChangeListener;

/**
 * @author antonio
 */
@SuppressWarnings("unused")
public class EventProperty extends ReadOnlyObjectPropertyBase{

    private final Object bean;
    private final String name;
    private final Runnable event;

    public EventProperty(Runnable event){
        bean = null;
        name = "";
        this.event = event;
        super.addListener((s, i, j) -> event.run());
    }

    public EventProperty(Object bean, String name, Runnable event){
        this.bean = bean;
        this.name = name;
        this.event = event;
        super.addListener((s, i, j) -> event.run());
    }

    @Override
    public Object get(){
        return null;
    }

    @Override
    public Object getBean(){
        return bean;
    }

    @Override
    public String getName(){
        return name;
    }

    public void fire(){
        event.run();
    }

    @Override
    public void addListener(InvalidationListener listener){
        throw new UnsupportedOperationException();
    }

    @Override
    public void addListener(ChangeListener listener){
        throw new UnsupportedOperationException();
    }
}
