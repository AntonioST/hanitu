/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Comparator;
import java.util.Objects;

/**
 * @author antonio
 */
public class SingleSpikeFilter implements SpikeFilter, Comparable<SingleSpikeFilter>{

    private static final Comparator<SingleSpikeFilter> CMP
      = Comparator.<SingleSpikeFilter, String>comparing(d -> d.user)
      .thenComparing(d -> d.worm)
      .thenComparing(d -> d.neuron)
      .thenComparingInt(d -> -SpikeData.SPIKE_NEURON_TYPE.indexOf(d.type));
    public final String user;
    public final String worm;
    public final String neuron;
    public final char type;

    public SingleSpikeFilter(SpikeData data){
        user = Objects.requireNonNull(data.user);
        worm = Objects.requireNonNull(data.worm);
        neuron = Objects.requireNonNull(data.neuron);
        type = data.type;
    }

    @SuppressWarnings("unused")
    public SingleSpikeFilter(String user){
        this.user = Objects.requireNonNull(user);
        worm = "";
        neuron = "";
        type = '\0';
    }

    public SingleSpikeFilter(String user, String worm){
        this.user = Objects.requireNonNull(user);
        this.worm = Objects.requireNonNull(worm);
        neuron = "";
        type = '\0';
    }

    @SuppressWarnings("WeakerAccess")
    public SingleSpikeFilter(String user, String worm, String neuron, char type){
        this.user = Objects.requireNonNull(user);
        this.worm = Objects.requireNonNull(worm);
        this.type = Objects.requireNonNull(type);
        if (type == 'n'){
            this.neuron = "";
        } else {
            this.neuron = Objects.requireNonNull(neuron);
        }
    }

    @Override
    public boolean test(SpikeData spikeData){
        if (type == 'n'){
            return (user.isEmpty() || user.equals(spikeData.user)) &&
                   (worm.isEmpty() || worm.equals(spikeData.worm)) &&
                   (type == spikeData.type);
        } else {
            return (user.isEmpty() || user.equals(spikeData.user)) &&
                   (worm.isEmpty() || worm.equals(spikeData.worm)) &&
                   (neuron.isEmpty() || neuron.equals(spikeData.neuron)) &&
                   (type == 0 || type == spikeData.type);
        }
    }

    public SingleSpikeFilter toWormIdentify(){
        return new SingleSpikeFilter(user, worm, "", '\0');
    }

    @SuppressWarnings("unused")
    public SingleSpikeFilter toUserIdentify(){
        return new SingleSpikeFilter(user, "", "", '\0');
    }

    public boolean isSameWorm(SingleSpikeFilter that){
        return !user.isEmpty() && user.equals(that.user) &&
               !worm.isEmpty() && worm.equals(that.worm);
    }

    public boolean isNeuron(){
        return type == 'b';
    }

    @SuppressWarnings("unused")
    public boolean isSensor(){
        return type == 's';
    }

    public boolean isMotor(){
        return type == 'm';
    }

    public boolean isModularitySensor(){
        return type == 'd';
    }

    public SensorType getSensorType(){
        if (type == 'n') return SensorType.NPY;
        if (type == 's'){
            int id = Integer.parseInt(neuron);
            if (id < 0) throw new IllegalArgumentException("negative sensor id");
            if (id < 4) return SensorType.FOOD;
            if (id < 8) return SensorType.TOXICANT;
            throw new IllegalArgumentException("illegal sensor id");
        }
        throw new IllegalArgumentException("not a sensor");
    }

    public Direction getDirection(){
        if (type == 'n') return Direction.SILENCE;
        if (type == 's'){
            int id = Integer.parseInt(neuron);
            if (id < 0) throw new IllegalArgumentException("negative sensor id");
            if (id >= 8) throw new IllegalArgumentException("illegal sensor id");
            return Direction.values()[id % 4 + 1];
        }
        if (type == 'm'){
            int id = Integer.parseInt(neuron);
            if (id < 0) throw new IllegalArgumentException("negative motor id");
            if (id >= 4) throw new IllegalArgumentException("illegal motor id");
            return Direction.values()[id + 1];
        }
        throw new IllegalArgumentException("not a body neuron");
    }

    @Override
    public int compareTo(SingleSpikeFilter o){
        return CMP.compare(this, o);
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SingleSpikeFilter that = (SingleSpikeFilter)o;

        return user.equals(that.user) &&
               worm.equals(that.worm) &&
               neuron.equals(that.neuron) &&
               type == that.type;

    }

    @Override
    public int hashCode(){
        int result = user.hashCode();
        result = 31 * result + worm.hashCode();
        result = 31 * result + neuron.hashCode();
        result = 31 * result + (int)type;
        return result;
    }

    @Override
    public String toString(){
        if (type == 's') return String.format("%s[%s]S%s", user, worm, neuron);
        if (type == 'm') return String.format("%s[%s]M%s", user, worm, neuron);
        if (type == 'n') return String.format("%s[%s]N", user, worm);
        return String.format("%s[%s]%s", user, worm, neuron);
    }
}
