/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

/**
 * molecule type
 * @author antonio
 */
public enum MoleculeType{

    /**
     * the molecule worm eat will increase its hp.
     */
    FOOD,
    /**
     * the molecule worm eat will empty its hp and make it die.
     */
    TOXICANT;

    public static String getTypeName(MoleculeType type){
        switch (type){
        case FOOD:
            return "food";
        case TOXICANT:
            return "toxicant";
        default:
            throw new RuntimeException();
        }
    }

    public static String getTypeShortName(MoleculeType type){
        switch (type){
        case FOOD:
            return "f";
        case TOXICANT:
            return "t";
        default:
            throw new RuntimeException();
        }
    }
}
