/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.io.Serializable;

/**
 * the data class for storing the worm information during plotting.
 *
 * @author antonio
 */
public class WormData extends PositionData implements Serializable, FormatClass{

    /**
     * user identify number
     */
    public String user;
    /**
     * worm identify number
     */
    public String worm;
    /**
     * the health point of the worm
     */
    public double hp = 100.0;
    /**
     * the size of the worm's body, in unit 0.1mm
     */
    public int size;

    /**
     * create a empty worm data
     */
    public WormData(){
    }

    /**
     * create  with copy data from reference
     *
     * @param w reference
     */
    @SuppressWarnings("unused")
    public WormData(WormData w){
        super(w);
        user = w.user;
        worm = w.worm;
        hp = w.hp;
        size = w.size;
    }

    @SuppressWarnings("unused")
    public boolean contact(WormData other){
        return contact(other.x, other.y, size + other.size);
    }

    public boolean contact(PositionData data){
        return contact(data.x, data.y, size);
    }

    @SuppressWarnings("UnusedReturnValue")
    public int changeHp(int mount){
        double newHp = hp + mount;
        if (newHp > 100) return (int)(hp = 100);
        if (newHp < 0) return (int)(hp = 0);
        return (int)(hp = newHp);
    }

    @Override
    public String formatSingleFlag(char flag){
        switch (flag){
        case 'u':
            return user == null? "?": user;
        case 'w':
            return worm == null? "?": worm;
        case 'x':
            return String.format("%.1f", x);
        case 'y':
            return String.format("%.1f", y);
        case 's':
            return Integer.toString(size);
        case 'h':
            return String.format("%.2f", hp);
        }
        return null;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WormData wormData = (WormData)o;
        if (user != null){
            if (!user.equals(wormData.user)) return false;
        } else {
            if (wormData.user != null) return false;
        }
        if (worm != null){
            if (!worm.equals(wormData.worm)) return false;
        } else {
            if (wormData.worm != null) return false;
        }
        return true;
    }

    @Override
    public int hashCode(){
        int result = super.hashCode();
        result = 31 * result + (user != null? user.hashCode(): 0);
        result = 31 * result + (worm != null? worm.hashCode(): 0);
        return result;
    }

    @Override
    public String toString(){
        return String.format("worm[u=%s,%s](x=%.2f, y=%.2f, hp=%.2f)", user, worm, x, y, hp);
    }
}
