/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Objects;

/**
 * the data class for storing the molecule information during plotting
 *
 * @author antonio
 */
public class MoleculeData extends PositionData implements FormatClass{

    /**
     * molecule identify number
     */
    public final String ID;
    /**
     * molecule type
     */
    public final MoleculeType type;

    /**
     * create a empty molecule data
     */
    public MoleculeData(MoleculeType type, String ID){
        this.type = Objects.requireNonNull(type, "molecule type");
        this.ID = Objects.requireNonNull(ID, "molecule ID");
    }

    /**
     * create  with copy data from reference
     *
     * @param m reference
     */
    @SuppressWarnings("unused")
    public MoleculeData(MoleculeData m){
        super(m);
        ID = m.ID;
        type = m.type;
    }

    @Override
    public String formatSingleFlag(char flag){
        switch (flag){
        case 'm':
            return ID;
        case 'N':
            return type.name();
        case 'n':
            return type.name().toLowerCase();
        case 't':
            return MoleculeType.getTypeShortName(type);
        case 'T':
            return MoleculeType.getTypeName(type);
        case 'x':
            return String.format("%.1f", x);
        case 'y':
            return String.format("%.1f", y);
        }
        return null;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        MoleculeData that = (MoleculeData)o;

        return ID.equals(that.ID) && type == that.type;

    }

    @Override
    public int hashCode(){
        int result = super.hashCode();
        result = 31 * result + ID.hashCode();
        result = 31 * result + (type != null? type.hashCode(): 0);
        return result;
    }

    @Override
    public String toString(){
        return String.format("%s[%s](x=%.2f, y=%.2f)", type.name(), ID, x, y);
    }
}
