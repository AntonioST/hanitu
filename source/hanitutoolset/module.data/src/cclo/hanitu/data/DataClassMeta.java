/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

import javafx.beans.property.ReadOnlyProperty;

/**
 * @author antonio
 */
@SuppressWarnings("unused")
public class DataClassMeta<T extends DataClass>{

    private static final Map<Class<? extends DataClass>, DataClassMeta> CACHE = new HashMap<>();

    public static <T extends DataClass> DataClassMeta<T> getMeta(T target){
        return CACHE.computeIfAbsent(target.getClass(), _k -> new DataClassMeta(target));
    }

//    public static <T extends DataClass> DataClassMeta<T> getMeta(Class<T> cls){
//        return CACHE.computeIfAbsent(cls, DataClassMeta::new);
//    }

    private final ArrayList<String> propertyNameList = new ArrayList<>();
    //[(group name, [property name])]
    private final ArrayList<Tuple<String, List<String>>> groupNameList = new ArrayList<>();
    private final Map<String, Field> properties = new HashMap<>();
    private final Map<String, Field> metaProperties = new HashMap<>();

    private DataClassMeta(T data){
        initField(data);
        initGroup(data);
    }

//    private DataClassMeta(Class<T> data){
//        try {
//            T instance = data.newInstance();
//            initField(instance);
//            initGroup(instance);
//        } catch (InstantiationException | IllegalAccessException e){
//            throw new RuntimeException(e);
//        }
//    }

    private void initField(T instance){
        Class<? extends DataClass> target = instance.getClass();
        //noinspection EmptyCatchBlock
        try {
            for (Class c = target; c != Object.class; c = c.getSuperclass()){
                for (Field field : c.getDeclaredFields()){
                    if (!Modifier.isPublic(field.getModifiers())) continue;
                    Class<?> type = field.getType();
                    if (!ReadOnlyProperty.class.isAssignableFrom(type)) continue;
                    ReadOnlyProperty value = (ReadOnlyProperty)field.get(instance);
                    String name = value.getName();
                    if (name.startsWith("meta")){
                        metaProperties.put(name, field);
                    } else if (!name.isEmpty()){
                        propertyNameList.add(name);
                        properties.put(name, field);
                    }
                }
            }
        } catch (IllegalAccessException e){
        }
    }

    private void initGroup(T instance){
        Map<String, List<String>> group = new HashMap<>();
        Map<String, String> groupNameMap = new HashMap<>();
        Properties property = instance.getProperty();
        property.forEach((_k, _v) -> {
            String k = (String)_k;
            if (k.endsWith(".group")){
                String p = k.substring(0, k.length() - ".group".length());
                List<String> v = Arrays.asList(((String)_v).split(" +"));
                group.put(p, v);
                v.forEach(_i -> groupNameMap.put(_i, p));
            }
        });
        for (String p : propertyNameList){
            if (groupNameMap.containsKey(p)){
                String g = groupNameMap.get(p);
                if (group.containsKey(g)){
                    groupNameList.add(new Tuple<>(g, group.remove(g)));
                }
            } else {
                groupNameList.add(new Tuple<>(p, Collections.singletonList(p)));
            }
        }
    }

    public int getGroupSize(){
        return groupNameList.size();
    }

    public List<String> getGroupNameList(){
        return groupNameList.stream().map(Tuple::left).collect(Collectors.toList());
    }

    public String getGroupName(int index){
        return groupNameList.get(index).c1;
    }

    public List<String> getGroupList(int index){
        return groupNameList.get(index).c2;
    }

    public List<String> getPropertyNameList(){
        return Collections.unmodifiableList(propertyNameList);
    }

    public String getPropertyName(int index){
        return propertyNameList.get(index);
    }

    public boolean isPropertyReadOnly(int index){
        Field field = properties.get(propertyNameList.get(index));
        return field.getType().getSimpleName().startsWith("ReadOnly");
    }

    public boolean isPropertyReadOnly(String name){
        Field field = properties.get(name);
        return field.getType().getSimpleName().startsWith("ReadOnly");
    }

    public Field getPropertyField(int index){
        return properties.get(propertyNameList.get(index));
    }

    public Field getPropertyField(String name){
        return properties.get(name);
    }

    public Set<String> getMetaPropertyNameSet(){
        return Collections.unmodifiableSet(metaProperties.keySet());
    }

    public Field getMetaPropertyField(String name){
        return metaProperties.get(name);
    }

    public ReadOnlyProperty getProperty(T target, int index){
        String name = propertyNameList.get(index);
        Field field = Objects.requireNonNull(properties.get(name), "properties " + name);
        try {
            return (ReadOnlyProperty)field.get(target);
        } catch (IllegalAccessException e){
            throw new RuntimeException(e);
        }
    }

    public ReadOnlyProperty getProperty(T target, String name){
        Field field = Objects.requireNonNull(properties.get(name), "properties " + name);
        try {
            return (ReadOnlyProperty)field.get(target);
        } catch (IllegalAccessException e){
            throw new RuntimeException(e);
        }
    }

    public ReadOnlyProperty getMetaProperty(T target, String name){
        Field field = Objects.requireNonNull(metaProperties.get(name), "properties " + name);
        try {
            return (ReadOnlyProperty)field.get(target);
        } catch (IllegalAccessException e){
            throw new RuntimeException(e);
        }
    }
}
