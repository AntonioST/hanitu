/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author antonio
 */
public class Formatter{

    private Function<Character, String> single;
    private BiFunction<String, StringBuilder, Integer> expression;

    /**
     * replace single flag.
     *
     * match pattern
     * -------------
     *
     * * % _c_
     *
     * @param single replace function, return null represent replace fail.
     */
    public void setSingleReplace(Function<Character, String> single){
        this.single = single;
    }

    /**
     * replace expression flag.
     *
     * match pattern
     * -------------
     *
     * * % _expression_
     *
     * replace function arguments
     * --------------------------
     *
     * 1. the string after matched character '%'.
     *
     * 2. the buffer used to get the result of replacing
     *
     * 3. (return value) the length of the original string (the first argument) will be replaced.
     *
     * @param expression replace function.
     */
    @SuppressWarnings("unused")
    public void setReplace(BiFunction<String, StringBuilder, Integer> expression){
        this.expression = expression;
    }

    public String format(String format){
        return formatAppend(new StringBuilder(), format).toString();
    }

    @SuppressWarnings("WeakerAccess")
    public StringBuilder formatAppend(StringBuilder builder, String format){
        formatPrivate(builder, format, 0);
        return builder;
    }

    @SuppressWarnings("UnusedReturnValue")
    private int formatPrivate(StringBuilder buffer, String format, int offset){
        int next = 0;
        int length = format.length();
        while (offset < length && (next = format.indexOf("%", offset)) >= 0){
            if (offset != next){
                buffer.append(format.substring(offset, next));
            }
            char op = format.charAt(next + 1);
            if (single != null){
                String back = single.apply(op);
                if (back != null){
                    buffer.append(back);
                    offset = next + 2;
                    continue;
                }
            }
            if (expression != null){
                StringBuilder tmp = new StringBuilder();
                int ret = expression.apply(format.substring(next), tmp);
                if (ret > 0){
                    buffer.append(tmp);
                    offset = next + ret;
                    continue;
                }
            }
            throw new IllegalArgumentException("unknown format flag : %" + op + " in '" + format + "'");
        }
        if (next < length){
            buffer.append(format.substring(offset));
            next = length;
        }
        return next;
    }
}
