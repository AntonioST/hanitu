/*
 * Copyright (C) 2014 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Objects;

/**
 * the data class contain the information of the simulation time and the spiking neuron ID.
 *
 * @author antonio
 */
public class SpikeData implements TimeSerialData{

    public static final String SPIKE_NEURON_TYPE = "bsmd";

    public final double time;
    /**
     * neuron ID
     */
    public final String neuron;
    /**
     * user ID
     */
    public final String user;
    /**
     * worm ID
     */
    public final String worm;
    /**
     * neuron type
     */
    public final char type;

    @SuppressWarnings("unused")
    public SpikeData(double time, String neuron){
        this.time = time;
        this.neuron = neuron;
        user = "";
        worm = "";
        type = 'b';
    }

    @SuppressWarnings("WeakerAccess")
    public SpikeData(double time, String user, String worm, String neuron, char type){
        if (SPIKE_NEURON_TYPE.indexOf(type) < 0) throw new IllegalArgumentException("illegal neuron type : " + type);
        this.time = time;
        this.user = Objects.requireNonNull(user, "user ID");
        this.worm = Objects.requireNonNull(worm, "worm ID");
        this.neuron = Objects.requireNonNull(neuron, "neuron ID");
        this.type = type;
    }

    @Override
    public int getTime(){
        return (int)time;
    }

    @SuppressWarnings("unused")
    public double getPreciseTime(){
        return time;
    }

    public static SpikeData parseLine(String line){
        String[] sp = line.split("[ \t]+");
        int length = sp.length;
        try {
            if (length == 4){
                // TIME USER WORM NID
                return new SpikeData(Double.parseDouble(sp[0]), sp[1], sp[2], sp[3], 'b');
            } else if (length == 5){
                // TIME USER WORM NID TYPE
                if (sp[4].length() != 1) throw new IllegalArgumentException("illegal neuron type : " + sp[4]);
                return new SpikeData(Double.parseDouble(sp[0]), sp[1], sp[2], sp[3], sp[4].charAt(0));
            } else if (length == 7 && "or".equals(sp[4])){
                // TIME USER WORM NID or ORIGIN_NEURON_ID TYPE
                if (sp[6].length() != 1) throw new IllegalArgumentException("illegal neuron type : " + sp[4]);
                return new SpikeData(Double.parseDouble(sp[0]), sp[1], sp[2], sp[3], sp[6].charAt(0));
            } else {
                throw new IllegalArgumentException("wrong spike data format");
            }
        } catch (NumberFormatException ex){
            throw new IllegalArgumentException("wrong spike data format : " + line, ex);
        }
    }

    @Override
    public String toString(){
        return String.format("%.1f\t%s\t%s\t%s\t%c", time, user, worm, neuron, type);
    }

}
