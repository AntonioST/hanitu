/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * @author antonio
 */
public interface SpikeFilter extends Predicate<SpikeData>{

    static SpikeFilter testUser(String testUser, boolean inverse){
        Predicate<String> t = parse(testUser);
        return data -> t.test(data.user) ^ inverse;
    }

    static SpikeFilter testWorm(String testWorm, boolean inverse){
        Predicate<String> t = parse(testWorm);
        return data -> t.test(data.worm) ^ inverse;
    }


    static SpikeFilter testNeuron(String testNeuron, boolean inverse){
        Predicate<String> t = parse(testNeuron);
        return data -> t.test(data.neuron) ^ inverse;
    }


    static SpikeFilter testType(String testType, boolean inverse){
        IntPredicate t = i -> testType.indexOf((char)i) >= 0;
        return data -> t.test(data.type) ^ inverse;
    }

    static Predicate<String> parse(String expr){
        int index = expr.indexOf(":");
        if (index == 0){
            int value = Integer.parseInt(expr.substring(1));
            return i -> rangeIn(i, 0, value);
        } else if (index > 0){
            int start = Integer.parseInt(expr.substring(0, index));
            int end = Integer.parseInt(expr.substring(index + 1));
            return i -> rangeIn(i, start, end);
        } else if (expr.contains(",")){
            Set<String> set = new HashSet<>(Arrays.asList(expr.split(",")));
            return set::contains;
        } else if (expr.contains("*") || expr.contains("?")){
            Pattern pattern = Pattern.compile(expr.replaceAll("\\*", ".*").replaceAll("\\?", ".?"));
            return i -> pattern.matcher(i).matches();
        } else {
            return expr::equals;
        }
    }

    static boolean rangeIn(String s, int from, int to){
        try {
            int i = Integer.parseInt(s);
            return from <= i && i <= to;
        } catch (NumberFormatException e){
            return false;
        }
    }

    default SpikeFilter and(SpikeFilter other){
        Objects.requireNonNull(other);
        return t -> test(t) && other.test(t);
    }

    @Override
    default SpikeFilter negate(){
        return t -> !test(t);
    }

    default SpikeFilter or(SpikeFilter other){
        Objects.requireNonNull(other);
        return t -> test(t) || other.test(t);
    }
}
