/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Properties;

import javafx.beans.property.SimpleBooleanProperty;

/**
 * @author antonio
 */
@SuppressWarnings("unused")
public class SwitchProperty extends SimpleBooleanProperty{

    /*
    must be non-null. If null, the only meaning is that it isn't initialization.
    should call initEnumSet()
     */
    private String[] switchSet;

    public SwitchProperty(String[] switchSet){
        if (switchSet.length != 2) throw new IllegalArgumentException("expect length 2");
        this.switchSet = switchSet.clone();
    }

    public SwitchProperty(String[] switchSet, boolean initialValue){
        super(initialValue);
        if (switchSet.length != 2) throw new IllegalArgumentException("expect length 2");
        this.switchSet = switchSet.clone();
    }

    public SwitchProperty(Object bean, String name, String[] switchSet){
        super(bean, name);
        if (switchSet.length != 2) throw new IllegalArgumentException("expect length 2");
        this.switchSet = switchSet.clone();
    }

    public SwitchProperty(Object bean, String name, String[] switchSet, boolean initialValue){
        super(bean, name, initialValue);
        if (switchSet.length != 2) throw new IllegalArgumentException("expect length 2");
        this.switchSet = switchSet.clone();
    }

    public SwitchProperty(DataClass bean, String key){
        super(bean, key);
        switchSet = null;
    }

    public SwitchProperty(DataClass bean, String key, boolean initialValue){
        super(bean, key, initialValue);
        switchSet = null;
    }

    private void initSwitchSet(){
        /*
        lazy initialization. avoid calling any method during the target instance initialization.
         */
        DataClass data = (DataClass)getBean();
        Properties property = data.getProperty();
        String key = getName();
        switchSet = new String[2];
        String pF = property.getProperty(key + ".false");
        String pT = property.getProperty(key + ".true");
        if (pF == null || pT == null){
            if (pF == null && pT == null){
                pF = "No";
                pT = "Yes";
            } else {
                throw new RuntimeException("lost property .true or .false : " + key);
            }
        }
        switchSet[0] = pF;
        switchSet[1] = pT;
    }

    public String getSwitchName(){
        if (switchSet == null) initSwitchSet();
        return switchSet[get()? 1: 0];
    }
}
