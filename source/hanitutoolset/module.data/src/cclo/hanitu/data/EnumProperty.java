/*
 * Copyright (C) 2015 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.data;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import javafx.beans.property.SimpleIntegerProperty;

/**
 * @author antonio
 */
@SuppressWarnings("unused")
public class EnumProperty extends SimpleIntegerProperty{

    /*
    must be non-null. If null, the only meaning is that it isn't initialization.
    should call initEnumSet()
     */
    private String[] enumSet;

    public EnumProperty(String[] enumSet){
        this.enumSet = enumSet.clone();
    }

    public EnumProperty(String[] enumSet, int initialValue){
        super(initialValue);
        this.enumSet = enumSet.clone();
    }

    public EnumProperty(Object bean, String name, String[] enumSet){
        super(bean, name);
        this.enumSet = enumSet.clone();
    }

    public EnumProperty(Object bean, String name, String[] enumSet, int initialValue){
        super(bean, name, initialValue);
        this.enumSet = enumSet.clone();
    }

    public EnumProperty(DataClass bean, String key){
        super(Objects.requireNonNull(bean), Objects.requireNonNull(key));
    }

    public EnumProperty(DataClass bean, String key, int initialValue){
        super(Objects.requireNonNull(bean), Objects.requireNonNull(key), initialValue);
    }

    private void initEnumSet(){
        /*
        lazy initialization. avoid calling any method during the target instance initialization.
         */
        DataClass data = (DataClass)getBean();
        Properties property = data.getProperty();
        String key = getName();
        String countProperty = property.getProperty(key + ".count");
        if (countProperty == null){
            enumSet = new String[0];
        } else {
            int count = Integer.parseInt(countProperty);
            enumSet = new String[count];
            for (int i = 0; i < count; i++){
                enumSet[i] = property.getProperty(String.format("%s.%d", key, i));
            }
        }
    }

    public List<String> getEnum(){
        if (enumSet == null) initEnumSet();
        return Arrays.asList(enumSet);
    }

    public String getEnumName(){
        if (enumSet == null) initEnumSet();
        return enumSet[get()];
    }
}
