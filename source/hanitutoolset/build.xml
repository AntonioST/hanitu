<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:if="ant:if"
         name="HanituToolSet" default="package">

    <property name="ant.root" value=".ant"/>
    <property name="build.dir" value="build"/>

    <!--========================
    import pre-defined ant tasks
    ============================
    git sub-module
    git : git@github.com:AntonioST/buildc.git
          https://github.com/AntonioST/buildc.git
    =========================-->
    <include file="${ant.root}/ant-machine.xml"/>
    <include file="${ant.root}/ant-classpath.xml"/>
    <include file="${ant.root}/ant-git.xml"/>
    <import file="${ant.root}/build-module.xml"/>
    <import file="${ant.root}/java-ivy.xml"/>

    <property file="build.properties"/>
    <property-os property="os"/>
    <property-arch property="arch"/>
    <property name="script.dir" value="script/${os}"/>
    <property name="share.dir" value="share"/>
    <property name="jar.file" value="hanitu.jar"/>
    <git-describe property="project.version"/>

    <!--=======
    Module Task
    ========-->
    <target name="base" description="module Hanitu Base">
        <module-jar name="base" dir="module.base" jarfile="hanitu-base.jar" propertyfile="module.properties"/>
    </target>
    <target name="data" depends="base" description="module Hanitu Data">
        <module-jar name="data" dir="module.data" jarfile="hanitu-data.jar" propertyfile="module.properties"/>
    </target>
    <target name="circuit" depends="base,data" description="module Hanitu Circuit">
        <module-jar name="circuit" dir="module.circuit" jarfile="hanitu-circuit.jar" propertyfile="module.properties"/>
    </target>
    <target name="world" depends="base,data" description="module Hanitu World">
        <module-jar name="world" dir="module.world" jarfile="hanitu-world.jar" propertyfile="module.properties"/>
    </target>
    <target name="gui" depends="base,data" description="module Hanitu GUI">
        <module-jar name="gui" dir="module.gui" jarfile="hanitu-gui.jar" propertyfile="module.properties"/>
    </target>
    <target name="exe" depends="base,gui" description="module">
        <module-jar name="exe" dir="module.exe" jarfile="hanitu-exe.jar" propertyfile="module.properties"/>
    </target>
    <target name="chart" depends="gui,data" description="module">
        <module-jar name="chart" dir="module.chart" jarfile="hanitu-chart.jar" propertyfile="module.properties"/>
    </target>
    <target name="circuit-viewer" depends="circuit,gui,exe" description="module Circuit Builder">
        <module-jar name="circuit-viewer" dir="module.circuit.viewer" jarfile="hanitu-circuit-viewer.jar"
                    propertyfile="module.properties"/>
    </target>
    <target name="world-viewer" depends="world,circuit-viewer" description="module World Viewer">
        <module-jar name="world-viewer" dir="module.world.viewer" jarfile="hanitu-world-viewer.jar"
                    propertyfile="module.properties"/>
    </target>
    <target name="world-builder" depends="world-viewer" description="module World builder">
        <module-jar name="world-builder" dir="module.world.builder" jarfile="hanitu-world-builder.jar"
                    propertyfile="module.properties"/>
    </target>
    <target name="tool" depends="exe" description="module Tool">
        <module-jar name="tool" dir="module.tool" jarfile="hanitu-tool.jar" propertyfile="module.properties"/>
    </target>
    <target name="app" description="module Application"
            depends="base,exe,circuit,world,data,circuit-viewer,world-viewer,world-builder,chart,tool">
        <module-jar name="app" dir="module.app" jarfile="hanitu-app.jar" propertyfile="module.properties"/>
    </target>

    <target name="all" depends="app" description="compile all modules"/>

    <!--=====
    dummy jar
    ======-->
    <target name="-jar-init" depends="try-retrieve,all">
        <property name="build.jar.dir" value="${build.dir}/jar"/>
        <mkdir dir="${build.jar.dir}"/>
        <delete file="${build.jar.dir}/${jar.file}"/>
    </target>
    <target name="-jar-module" depends="-jar-init">
        <copy todir="${build.jar.dir}" flatten="true">
            <fileset dir="${build.dir}">
                <include name="*/jar/*.jar"/>
            </fileset>
        </copy>
    </target>
    <target name="-jar-classpath" depends="-jar-module">
        <property name="lib.runtime.dir" value="${lib.dir}/runtime"/>
        <resolve-classpath property="real.runtime.classpath" dir="${lib.runtime.dir}">
            <pathelement path="${classpath}" if:set="classpath"/>
            <pathelement path="${master.classpath}" if:set="master.classpath"/>
            <pathelement path="${compile.classpath}" if:set="compile.classpath"/>
            <pathelement path="${runtime.classpath}" if:set="runtime.classpath"/>
            <fileset dir="${build.jar.dir}">
                <include name="*.jar"/>
            </fileset>
        </resolve-classpath>
    </target>
    <target name="-jar-manifest" depends="-jar-classpath">
        <property name="jar.manifest.file" value="${build.dir}/MANIFEST.MF"/>
        <touch file="${jar.manifest.file}"/>
        <manifest file="${jar.manifest.file}" mode="update">
            <attribute name="Main-Class" value="cclo.hanitu.exe.Main"/>
        </manifest>
        <pathconvert property="jar.classpath" pathsep=" " refid="real.runtime.classpath.ref">
            <mapper type="regexp" from=".*/(.*\.jar)" to="\1"/>
        </pathconvert>
        <manifest file="${jar.manifest.file}" mode="update">
            <attribute name="Class-Path" value="${jar.classpath}"/>
        </manifest>
    </target>
    <target name="jar" depends="-jar-manifest" description="Hanitu facade">
        <jar destfile="${build.jar.dir}/${jar.file}"
             manifest="${jar.manifest.file}">
        </jar>
    </target>


    <!--=================
    task : package binary
    ==================-->
    <target name="-package-init" depends="jar">
        <condition property="os.win">
            <equals arg1="${os}" arg2="windows"/>
        </condition>
        <condition property="os.linux">
            <equals arg1="${os}" arg2="linux"/>
        </condition>
        <!---->
        <property name="package.dir" value="${build.dir}/package"/>
        <mkdir dir="${package.dir}"/>
        <property name="package.bin.dir" value="${package.dir}/bin"/>
        <property name="package.lib.dir" value="${package.dir}/lib/"/>
        <property name="package.share.dir" value="${package.dir}/share/"/>
        <available property="exist.package.bin" file="${package.bin.dir}" type="dir"/>
        <delete dir="${package.bin.dir}" if:set="exist.package.bin"/>
        <mkdir dir="${package.bin.dir}"/>
        <mkdir dir="${package.lib.dir}"/>
        <mkdir dir="${package.share.dir}"/>
    </target>

    <target name="-macro-package-bin" depends="-package-init">
        <macrodef name="package-bin">
            <attribute name="src" default="${script.dir}/hanitu_tool.sh"/>
            <attribute name="dest"/>
            <attribute name="main" default=""/>
            <sequential>
                <copy file="@{src}" tofile="${package.bin.dir}/@{dest}"/>
                <replace file="${package.bin.dir}/@{dest}">
                    <replacefilter token="@HANITU_JAR@" value="${jar.file}"/>
                    <replacefilter token="@HANITU@" value="${native.hanitu.file}"/>
                    <replacefilter token="@FLYSIM@" value="${native.flysim.file}"/>
                    <replacefilter token="@MAIN@" value="@{main}"/>
                </replace>
                <chmod file="${package.bin.dir}/@{dest}" perm="755"/>
            </sequential>
        </macrodef>
    </target>

    <target name="-package-linux" depends="-package-init,-macro-package-bin" if="os.linux">
        <!--dir : bin-->
        <package-bin dest="hanitu_tool"/>
        <package-bin dest="hanitu_circuit" main="circuit"/>
        <package-bin dest="hanitu_world" main="world"/>
        <package-bin dest="hanitu_plot" main="plot"/>
        <package-bin dest="hanitu_run" main="run"/>
        <package-bin dest="hanitu_filter" main="filter"/>
        <package-bin dest="hanitu_firingrate" main="firingrate"/>
        <package-bin dest="hanitu_concentration" main="concentration"/>
        <package-bin dest="hanitu_chart" main="chart"/>
        <!--dir : lib-->
        <copy todir="${package.lib.dir}" flatten="true">
            <fileset dir="${build.jar.dir}"/>
        </copy>
        <chmod perm="755">
            <fileset dir="${package.dir}/bin"/>
        </chmod>
        <!--dir : share-->
        <copy todir="${package.share.dir}">
            <fileset dir="${share.dir}">
                <exclude name="**/.*"/>
            </fileset>
        </copy>
        <!--other-->
        <copy file="jvmoption" todir="${package.dir}"/>
    </target>
    <target name="-package-windows" depends="-package-init" if="os.win">
        <!--XXX-->
    </target>
    <target name="package" depends="-package-linux,-package-windows" description="package hanitu program"/>

    <target name="clean" description="clean built product">
        <delete dir="${build.dir}"/>
    </target>
    <target name="clean-all" depends="clean,clean-lib">
        <delete dir="${release.dir}"/>
        <delete>
            <dirset dir="." includes="**/${lib.dir}/"/>
        </delete>
    </target>

</project>
