/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.builder;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.input.MouseEvent;

import cclo.hanitu.Base;
import cclo.hanitu.data.MoleculeData;
import cclo.hanitu.data.PositionData;
import cclo.hanitu.data.WormData;
import cclo.hanitu.gui.FXApplication;
import cclo.hanitu.world.MoleculeConfig;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.WormConfig;
import cclo.hanitu.world.view.AbstractWorldViewerModule;
import cclo.hanitu.world.view.WorldViewer;

/**
 * @author antonio
 */
public class WorldConfigModulePreview extends AbstractWorldConfigModule{

    private WorldViewer preview;

    @Override
    public String getName(){
        return "preview";
    }

    @Override
    public void setupModule(WorldConfigViewer world){
        world.menu.addMenu("menu.view");
    }

    @Override
    public void destroy(){
        closePreview();
        host.menu.removeMenu("menu.view");
        super.destroy();
    }

    @Override
    public boolean processMenuEvent(ActionEvent e, String action){
        switch (action){
        case "menu.view.preview":
            if (preview == null || preview.isClose()){
                showPreview();
            }
            preview.primaryStage.toFront();
            break;
        default:
            return false;
        }
        return true;
    }

    @Override
    public void processOpenNewWorldConfig(WorldConfig config){
        closePreview();
    }

    @SuppressWarnings("WeakerAccess")
    public void showPreview(){
        preview = FXApplication.launch(WorldViewer.class);
        preview.primaryStage.setTitle(Base.loadProperties().getProperty("title"));
        PreviewModule module = new PreviewModule();
        preview.module.add(module);
        //
        preview.setWorld(host.config);
        preview.updateWorldConfig();
        preview.primaryStage.show();
    }

    @SuppressWarnings("WeakerAccess")
    public void closePreview(){
        if (preview != null){
            preview.close();
            preview = null;
        }
    }

    @Override
    public void processWorldModify(WorldConfig config){
        if (preview != null) preview.updateWorldConfig();
    }

    @Override
    public void processWormCreate(WormConfig config){
        if (preview != null) preview.updateWorldConfig();
    }

    @Override
    public void processWormDelete(WormConfig config){
        if (preview != null) preview.updateWorldConfig();
    }

    @Override
    public void processWormModify(WormConfig config){
        if (preview != null) preview.repaintCanvas();
    }

    @Override
    public void processMoleculeCreate(MoleculeConfig config){
        if (preview != null) preview.updateWorldConfig();
    }

    @Override
    public void processMoleculeDelete(MoleculeConfig config){
        if (preview != null) preview.updateWorldConfig();
    }

    @Override
    public void processMoleculeModify(MoleculeConfig config){
        if (preview != null) preview.repaintCanvas();
    }

    private class PreviewModule extends AbstractWorldViewerModule{

        private final WorldConfigViewer configViewer;
        private final WorldConfigModuleEdit edit;

        public PreviewModule(){
            configViewer = WorldConfigModulePreview.super.host;
            edit = configViewer.getModuleEdit();
        }

        @Override
        public String getName(){
            return "preview";
        }

        @Override
        public void setupModule(WorldViewer world){
            world.setEditable(edit != null);
        }

        @Override
        public void processMousePressed(MouseEvent e, WormData worm){
            configViewer.getWorm(worm.user, worm.worm).ifPresent(WorldConfigTab::select);
        }

        @Override
        public void processMousePressed(MouseEvent e, MoleculeData molecule){
            configViewer.getMolecule(molecule.type, molecule.ID).ifPresent(WorldConfigTab::select);
        }

        @Override
        public void processMousePressed(MouseEvent e, PositionData position){
            configViewer.world.select();
        }

        @Override
        public void processMouseDoubleClicked(MouseEvent e, PositionData position){
            if (edit != null){
                Platform.runLater(() -> {
                    WormConfig worm = edit.createNewWorm();
                    if (worm != null){
                        worm.x.set(Math.rint(position.x * 10) / 10);
                        worm.y.set(Math.rint(position.y * 10) / 10);
                        edit.addWorm(worm);
                    }
                });
            }
        }

        @Override
        public void processMouseDragging(MouseEvent e, WormData worm, PositionData position){
            Platform.runLater(() -> configViewer.getWorm(worm.user, worm.worm).ifPresent(w -> {
                w.target.x.set(Math.rint(position.x * 10) / 10);
                w.target.y.set(Math.rint(position.y * 10) / 10);
            }));
        }

        @Override
        public void processMouseDragging(MouseEvent e, MoleculeData molecule, PositionData position){
            Platform.runLater(() -> configViewer.getMolecule(molecule.type, molecule.ID).ifPresent(m -> {
                m.target.x.set(Math.rint(position.x * 10) / 10);
                m.target.y.set(Math.rint(position.y * 10) / 10);
            }));
        }
    }
}
