/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.builder;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.TabPane;
import javafx.scene.input.KeyEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.Base;
import cclo.hanitu.data.MoleculeType;
import cclo.hanitu.exe.About;
import cclo.hanitu.gui.*;
import cclo.hanitu.world.*;

/**
 * @author antonio
 */
public class WorldConfigViewer extends FXApplication{

    final Logger LOG = LoggerFactory.getLogger(WorldConfigViewer.class);

    public final ModuleManager<WorldConfigViewer, WorldConfigModule> module;
    final FileSession file;
    final MenuManager menu;
    final TabPane content;

    WorldConfig config = WorldConfig.EMPTY;

    WorldConfigTabWorld world;
    final List<WorldConfigTabWorm> worms = new ArrayList<>();
    final List<WorldConfigTabMolecule> foods = new ArrayList<>();
    final List<WorldConfigTabMolecule> toxicant = new ArrayList<>();

    public WorldConfigViewer(){
        primaryStage.setWidth(700);
        primaryStage.setHeight(500);
        //
        file = initFile();
        menu = initMenu();
        content = new TabPane();
        module = new ModuleManager(this, WorldConfigModule.class);
        root.getChildren().addAll(menu.getMenuBar(), content);
        //
        setTitle(null);
        setWorldConfig(new WorldConfig());
        //
        primaryStage.show();
    }

    private FileSession initFile(){
        return new FileSession(primaryStage){
            {
                setDefaultSaveFileName(HanituFile.WORLD_CONFIG_DEFAULT_FILENAME);
                setFileFilter(HanituFile.WORLD_CONFIG_FILTERS);
            }

            @Override
            protected void onNewFile(){
                LOG.debug("new a empty file");
                clear();
            }

            @Override
            protected void onOpenFile(Path openFilePath) throws IOException{
                LOG.debug("open file : {}", openFilePath);
                setWorldConfig(new WorldConfigLoader().load(openFilePath));
            }

            @Override
            protected void onSaveFile(Path saveFilePath) throws IOException{
                LOG.debug("save file : {}", saveFilePath);
                FileSession.saveFileByTempReplace(saveFilePath, os -> new WorldConfigPrinter().write(os, config));
            }

            @Override
            protected void onUpdate(){
                setTitle(null);
            }

            @Override
            protected void onQuit(){
                close();
            }
        };
    }

    private MenuManager initMenu(){
        MenuManager menu = new MenuManager(this::processMenuEvent);
        menu.resetTopMenu();
        return menu;
    }

    @SuppressWarnings("unused")
    public void resetMenu(String subMenu){
        menu.resetTopMenu(subMenu);
    }

    @Override
    protected void processMenuEvent(ActionEvent event, String action){
        LOG.debug("menu event : {}", action);
        if (action.startsWith("menu.file")){
            file.processMenuEvent(event, action);
        } else if (action.startsWith("&")){
            try {
                MenuSession.processReflectionMenuEvent(this, event, action.substring(1));
            } catch (RuntimeException e){
                MessageSession.showErrorMessageDialog(e);
            }
        } else if (!module.root.processMenuEvent(event, action)){
            switch (action){
            case "menu.help.about":
                FXApplication.launch(About.class).show();
                break;
            }
        }
    }

    @Override
    public void close(){
        module.removeAll();
        super.close();
    }

    public void setWorldConfig(Path worldConfigPath, boolean failOnLoad) throws IOException{
        WorldConfigLoader loader = new WorldConfigLoader();
        loader.setFailOnLoad(failOnLoad);
        setWorldConfig(loader.load(worldConfigPath));
        file.setCurrentFilePath(worldConfigPath);
        file.setCurrentFileModified(false);
    }

    public void setWorldConfig(WorldConfig config){
        worms.clear();
        foods.clear();
        toxicant.clear();
        //
        this.config = null;
        world = new WorldConfigTabWorld(this, config);
        config.worms().forEach(this::createWormTab);
        config.molecule().forEach(this::createMoleculeTab);
        //
        this.config = config;
        Platform.runLater(() -> {
            content.getTabs().clear();
            world.add();
            worms.forEach(WorldConfigTab::add);
            foods.forEach(WorldConfigTab::add);
            toxicant.forEach(WorldConfigTab::add);
            world.select();
        });
        //
        file.setCurrentFileModified(false);
        module.root.processOpenNewWorldConfig(config);
    }

    public void setSaveFileName(Path filePath){
        file.setCurrentFilePath(filePath);
        file.setCurrentFileModified(true);
    }

    @Override
    public void setTitle(String title){
        if (primaryStage == null) return;
        Properties p = Base.loadProperties();
        StringBuilder sb = new StringBuilder();
        if (file.isCurrentFileModified()){
            sb.append("* ");
        }
        sb.append(p.getProperty("title"));
        if (title != null){
            sb.append("[").append(title).append("]");
        }
        Path path = file.getCurrentFilePath();
        if (path != null){
            sb.append(" - ").append(path);
        }
        Platform.runLater(() -> primaryStage.setTitle(sb.toString()));
    }

    @Override
    protected boolean processKeyboardEvent(KeyEvent e){
        if (e.isControlDown()){
            switch (e.getCode()){
            case DIGIT1:
                world.select();
                break;
            case DIGIT2:
                if (!worms.isEmpty()) worms.get(0).select();
                break;
            case DIGIT3:
                if (worms.size() > 1) worms.get(1).select();
                break;
            case DIGIT4:
                if (worms.size() > 2) worms.get(2).select();
                break;
            case DIGIT5:
                if (worms.size() > 3) worms.get(3).select();
                break;
            case DIGIT6:
                if (worms.size() > 4) worms.get(4).select();
                break;
            case DIGIT7:
                if (worms.size() > 5) worms.get(5).select();
                break;
            case DIGIT8:
                if (worms.size() > 6) worms.get(6).select();
                break;
            case DIGIT9:
                if (worms.size() > 7) worms.get(7).select();
                break;
            case DIGIT0:
                if (worms.size() > 8) worms.get(8).select();
                break;
            default:
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * create worm tab. this method do not change the WorldConfig and FileSession status.
     * @param config wormConfig
     * @return tab
     */
    WorldConfigTabWorm createWormTab(WormConfig config){
        WorldConfigTabWorm ret = new WorldConfigTabWorm(this, config);
        worms.add(ret);
        return ret;
    }

    /**
     * create molecule tab. this method do not change the WorldConfig and FileSession status.
     * @param config wormConfig
     * @return tab
     */
    WorldConfigTabMolecule createMoleculeTab(MoleculeConfig config){
        WorldConfigTabMolecule ret = new WorldConfigTabMolecule(this, config);
        switch (config.type){
        case FOOD:
            foods.add(ret);
            break;
        case TOXICANT:
            toxicant.add(ret);
            break;
        default:
            throw new IllegalArgumentException("unknown molecule type : " + config.type);
        }
        return ret;
    }

    List<WorldConfigTabWorm> getWorm(String user){
        Objects.requireNonNull(user, "user ID");
        return worms.stream().filter(w -> user.equals(w.target.user.get())).collect(Collectors.toList());
    }

    Optional<WorldConfigTabWorm> getWorm(String user, String worm){
        Objects.requireNonNull(user, "user ID");
        Objects.requireNonNull(worm, "worm ID");
        for (WorldConfigTabWorm w : worms){
            if (user.equals(w.target.user.get()) && worm.equals(w.target.worm.get())){
                return Optional.of(w);
            }
        }
        return Optional.empty();
    }

    Optional<WorldConfigTabWorm> getWorm(WormConfig worm){
        return getWorm(worm.user.get(), worm.worm.get());
    }

    @SuppressWarnings("unused")
    Optional<WorldConfigTabMolecule> getLatestMolecule(MoleculeType type){
        if (type == MoleculeType.FOOD && !foods.isEmpty()){
            return Optional.of(foods.get(foods.size() - 1));
        } else if (type == MoleculeType.TOXICANT && !toxicant.isEmpty()){
            return Optional.of(toxicant.get(toxicant.size() - 1));
        }
        return Optional.empty();
    }

    Optional<WorldConfigTabMolecule> getMolecule(MoleculeType type, String id){
        Objects.requireNonNull(type);
        Objects.requireNonNull(id);
        if (type == MoleculeType.FOOD){
            for (WorldConfigTabMolecule f : foods){
                if (id.equals(f.target.ID.get())) return Optional.of(f);
            }
        } else if (type == MoleculeType.TOXICANT){
            for (WorldConfigTabMolecule t : toxicant){
                if (id.equals(t.target.ID.get())) return Optional.of(t);
            }
        }
        return Optional.empty();
    }

    @SuppressWarnings("WeakerAccess")
    public void clear(){
        setWorldConfig(new WorldConfig());
        file.setCurrentFilePath(null);
        file.setCurrentFileModified(false);
    }

    @SuppressWarnings("unused")
    public int foodCount(){
        return config == null? 0: config.foodCount();
    }

    @SuppressWarnings("unused")
    public int toxicantCount(){
        return config == null? 0: config.toxicantCount();
    }

    @SuppressWarnings("unused")
    public boolean isEditable(){
        return module.get("edit") != null;
    }

    public WorldConfigModuleEdit getModuleEdit(){
        return (WorldConfigModuleEdit)module.get("edit");
    }
}
