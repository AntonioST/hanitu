/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.builder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import cclo.hanitu.circuit.view.EditCircuitMode;
import cclo.hanitu.circuit.view.VisualCircuit;
import cclo.hanitu.gui.FXApplication;
import cclo.hanitu.gui.HanituFile;
import cclo.hanitu.gui.MessageSession;
import cclo.hanitu.gui.PropertySession;
import cclo.hanitu.world.WormConfig;

/**
 * @author antonio
 */
class WorldConfigTabWorm extends WorldConfigTab<WormConfig>{

    private static final Tooltip OVERLAP = new Tooltip("overlap with other worms");

    private final TextField locationXField;
    private final TextField locationYField;
    private final TextField circuitField;
    private final Button browse;
    private final Button edit;

    public WorldConfigTabWorm(WorldConfigViewer host, WormConfig worm){
        super(host, getTitle(worm), worm);
        //
        worm.user.addListener((v, o, n) -> setText(getTitle(worm)));
        worm.worm.addListener((v, o, n) -> setText(getTitle(worm)));
        //
        browse = new Button("Browse");
        edit = new Button("Edit");
        browse.setMinWidth(70);
        edit.setMinWidth(70);
        browse.setOnAction(this::browseCircuitFile);
        edit.setOnAction(this::openCircuitFile);
        //
        GridPane layout = properties.getLayout();
        layout.add(new HBox(browse, edit), 2, 7);

        locationXField = (TextField)properties.getControl(2);
        locationYField = (TextField)properties.getControl(3);
        locationXField.textProperty().addListener((observable, oldValue, newValue) -> {
            checkLocationOverlap(newValue, locationYField.getText());
        });
        locationYField.textProperty().addListener((observable, oldValue, newValue) -> {
            checkLocationOverlap(locationXField.getText(), newValue);
        });
        circuitField = (TextField)properties.getControl(7);
        circuitField.textProperty().addListener((observable, oldValue, newValue) -> {
            checkCircuitFileExist(newValue, true);
        });
        setOnSelectionChanged(event -> {
            checkLocationOverlap(locationXField.getText(), locationYField.getText());
            checkCircuitFileExist(circuitField.getText(), true);
        });
        //
        notifyEvent = host.module.root::processWormModify;
        worm.user.addListener(this::getNotifyEvent);
        worm.worm.addListener(this::getNotifyEvent);
        worm.size.addListener(this::getNotifyEvent);
        worm.x.addListener(this::getNotifyEvent);
        worm.y.addListener(this::getNotifyEvent);
    }

    private static String getTitle(WormConfig w){
        return String.format("Worm %s [%s]", w.user.get(), w.worm.get());
    }

    @Override
    void close(){
        host.getModuleEdit().deleteWorm(target);
    }

    @Override
    void setEditable(boolean editable){
        super.setEditable(editable);
        browse.setDisable(!editable);
    }

    @SuppressWarnings("UnusedParameters")
    private void browseCircuitFile(ActionEvent e){
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(HanituFile.CIRCUIT_CONFIG_FILTERS);
        if (host.file.getCurrentWorkDirectory() != null){
            fc.setInitialDirectory(host.file.getCurrentWorkDirectory().toFile());
        }
        if (target.circuitFileName.get() != null){
            fc.setInitialFileName(target.circuitFileName.getValue());
        }
        File file = fc.showOpenDialog(host.primaryStage);
        if (file != null){
            target.circuitFileName.set(file.getName());
            host.file.setCurrentFileModified(true);
        }
        checkCircuitFileExist(target.circuitFileName.get(), true);
    }

    @SuppressWarnings("UnusedParameters")
    private void openCircuitFile(ActionEvent e){
        VisualCircuit circuit = FXApplication.launch(VisualCircuit.class);
        String circuitFile = target.circuitFileName.get();
        if (circuitFile == null || circuitFile.isEmpty()){
            String filename = MessageSession.showInputDialog("Create Circuit File",
                                                             "Please enter a file name to create a new empty circuit file.");
            if (filename != null){

                if (filename.endsWith(HanituFile.CIRCUIT_EXTEND_FILENAME)){
                    circuitFile = filename;
                } else {
                    circuitFile = filename + "." + HanituFile.CIRCUIT_EXTEND_FILENAME;
                }
                target.circuitFileName.set(circuitFile);
                EditCircuitMode mode = new EditCircuitMode();
                circuit.setMode(mode);
                mode.setSaveFileName(host.file.getCurrentWorkDirectory().resolve(circuitFile));
            }
        } else {
            EditCircuitMode mode = new EditCircuitMode();
            circuit.setMode(mode);
            try {
                circuit.setCircuit(host.file.getCurrentWorkDirectory().resolve(circuitFile), false);
            } catch (IOException e1){
                MessageSession.showErrorMessageDialog("open circuit file error : " + circuitFile, e1);
                circuit.close();
                checkCircuitFileExist(circuitFile, true);
            }
        }
    }

    void checkCircuitFileExist(String circuitFilePath, boolean ignoreNull){
        PropertySession.setBackground(circuitField, Color.WHITE);
        circuitField.setTooltip(null);
        if (circuitFilePath == null){
            if (ignoreNull) return;
            PropertySession.setBackground(circuitField, Color.PINK);
            circuitField.setTooltip(new Tooltip("circuit file not set"));
            return;
        }
        Path p = host.file.getCurrentWorkDirectory().resolve(circuitFilePath);
        if (!Files.exists(p) || !Files.isRegularFile(p)){
            PropertySession.setBackground(circuitField, Color.PINK);
            circuitField.setTooltip(new Tooltip(circuitFilePath + " does not exist"));
        }
    }

    boolean isCircuitFileExist(){
        String circuitFile = target.circuitFileName.get();
        if (circuitFile == null || circuitFile.isEmpty()) return false;
        Path p = host.file.getCurrentWorkDirectory().resolve(circuitFile);
        return Files.exists(p) && Files.isRegularFile(p);
    }

    private void checkLocationOverlap(String x, String y){
        if (host.config == null || x == null || x.isEmpty() || y == null || y.isEmpty()) return;
        try {
            double xx = Double.parseDouble(x.trim());
            double yy = Double.parseDouble(y.trim());
            boolean result = host.config.worms().stream()
              .filter(w -> w != target)
              .anyMatch(w -> {
                  double dx = w.x.get() - xx;
                  double dy = w.y.get() - yy;
                  double dd = target.size.get() + w.size.get();
                  return !(dx > dd || dy > dd) &&
                         dx * dx + dy * dy < dd * dd;
              });
            if (result){
                PropertySession.setBackground(locationXField, Color.PINK);
                PropertySession.setBackground(locationYField, Color.PINK);
                locationXField.setTooltip(OVERLAP);
                locationYField.setTooltip(OVERLAP);
            } else {
                PropertySession.setBackground(locationXField, Color.WHITE);
                PropertySession.setBackground(locationYField, Color.WHITE);
                locationXField.setTooltip(null);
                locationYField.setTooltip(null);
            }
        } catch (NumberFormatException e){
            host.LOG.warn("check location overlap fail", e);
        }
    }
}
