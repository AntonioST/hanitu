/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.builder;

import cclo.hanitu.world.WorldConfig;

/**
 * @author antonio
 */
class WorldConfigTabWorld extends WorldConfigTab<WorldConfig>{

    public WorldConfigTabWorld(WorldConfigViewer host, WorldConfig world){
        super(host, "World", world);
        setClosable(false);
        //
        notifyEvent = host.module.root::processWorldModify;
        world.deltaHealthPoint.addListener(this::getNotifyEvent);
        world.gainFF.addListener(this::getNotifyEvent);
        world.baselineFF.addListener(this::getNotifyEvent);
        world.gainFT.addListener(this::getNotifyEvent);
        world.baselineFT.addListener(this::getNotifyEvent);
        world.gainTF.addListener(this::getNotifyEvent);
        world.baselineTF.addListener(this::getNotifyEvent);
        world.gainTT.addListener(this::getNotifyEvent);
        world.baselineTT.addListener(this::getNotifyEvent);
        world.gainNPY.addListener(this::getNotifyEvent);
        world.baselineNPY.addListener(this::getNotifyEvent);
        world.boundary.addListener(this::getNotifyEvent);
        world.type.addListener(this::getNotifyEvent);
        world.depth.addListener(this::getNotifyEvent);
        world.fixFoodCount.addListener(this::getNotifyEvent);
        world.fixWormLocation.addListener(this::getNotifyEvent);
    }

    @Override
    void setEditable(boolean editable){
        super.setEditable(editable);
        setClosable(false);
    }
}
