/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.builder;

import cclo.hanitu.world.MoleculeConfig;

/**
 * @author antonio
 */
class WorldConfigTabMolecule extends WorldConfigTab<MoleculeConfig>{

    public WorldConfigTabMolecule(WorldConfigViewer host, MoleculeConfig molecule){
        super(host, getTitle(molecule), molecule);
        molecule.ID.addListener((v, o, n) -> setText(getTitle(target)));
        //
        notifyEvent = host.module.root::processMoleculeModify;
        molecule.x.addListener(this::getNotifyEvent);
        molecule.y.addListener(this::getNotifyEvent);
        molecule.count.addListener(this::getNotifyEvent);
        molecule.diffuse.addListener(this::getNotifyEvent);
        molecule.concentration.addListener(this::getNotifyEvent);
        molecule.delay.addListener(this::getNotifyEvent);
    }

    private static String getTitle(MoleculeConfig mc){
        switch (mc.type){
        case FOOD:
            return "Food [" + mc.ID.get() + "]";
        case TOXICANT:
            return "Tox [" + mc.ID.get() + "]";
        default:
            throw new IllegalArgumentException("unknown molecule type");
        }
    }

    @Override
    void close(){
        host.getModuleEdit().deleteMolecule(target);
    }
}
