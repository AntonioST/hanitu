/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.builder;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.Consumer;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import cclo.hanitu.data.DataClass;
import cclo.hanitu.gui.PropertySession;
import cclo.hanitu.world.WorldConfig;

/**
 * @author antonio
 */
abstract class WorldConfigTab<T extends DataClass> extends Tab{

    private static final Comparator<Tab> CMP = new Comparator<Tab>(){
        @Override
        public int compare(Tab t1, Tab t2){
            if (!(t1 instanceof WorldConfigTab)) throw new IllegalArgumentException();
            if (!(t2 instanceof WorldConfigTab)) throw new IllegalArgumentException();
            if (t1 instanceof WorldConfigTabWorld){
                return (t2 instanceof WorldConfigTabWorld)? 0: -1;
            } else if (t2 instanceof WorldConfigTabWorld){
                return 1;
            }
            {
                boolean b1 = t1 instanceof WorldConfigTabWorm;
                boolean b2 = t2 instanceof WorldConfigTabWorm;
                if (b1 && b2){
                    return WorldConfig.WORM_CMP.compare(((WorldConfigTabWorm)t1).target,
                                                        ((WorldConfigTabWorm)t2).target);
                } else if (b1 || b2){
                    return b1? -1: 1;
                }
            }
            return WorldConfig.MOLECULE_CMP.compare(((WorldConfigTabMolecule)t1).target,
                                                    ((WorldConfigTabMolecule)t2).target);
        }
    };

    protected final WorldConfigViewer host;
    protected final T target;
    //
    protected Consumer<T> notifyEvent;
    //
    private final TabPane pane;
    protected final PropertySession properties;

    private final VBox contentPane;

    @SuppressWarnings("CanBeFinal")
    private boolean editable = true;

    protected WorldConfigTab(WorldConfigViewer host, String title, T target){
        super(title);
        this.host = host;
        this.target = Objects.requireNonNull(target, "target");
        pane = host.content;
        //
        properties = new PropertySession();
        properties.setShowReadOnlyProperties(false);
        properties.reset(target);
        properties.modifyEvent.add(() -> host.file.setCurrentFileModified(true));
        //
        GridPane layout = properties.getLayout();
        contentPane = new VBox(layout);
        VBox.setMargin(layout, new Insets(10));
        //
        setContent(contentPane);
        setClosable(editable);
        setOnCloseRequest(e -> close());
    }

    @SuppressWarnings("UnusedParameters")
    protected <U> void getNotifyEvent(ObservableValue<? extends U> v, U o, U n){
        if (notifyEvent != null) notifyEvent.accept(target);
    }

    void add(){
        ObservableList<Tab> tabs = pane.getTabs();
        tabs.add(this);
        tabs.sort(CMP);
        pane.getSelectionModel().select(tabs.indexOf(this));
    }

    void select(){
        Platform.runLater(() -> pane.getSelectionModel().select(this));
    }

    void remove(){
        pane.getTabs().remove(this);
    }

    void close(){
        //do nothing
    }

    void setEditable(boolean editable){
        setClosable(editable);
        properties.setEditable(editable);
    }
}
