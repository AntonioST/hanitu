/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.builder;

import java.util.*;
import java.util.stream.Collectors;

import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.data.MoleculeType;
import cclo.hanitu.gui.MenuManager;
import cclo.hanitu.gui.MessageSession;
import cclo.hanitu.util.ErrorFunction;
import cclo.hanitu.world.MoleculeConfig;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.WormConfig;
import cclo.hanitu.world.WormName;

/**
 * @author antonio
 */
public class WorldConfigModuleEdit extends AbstractWorldConfigModule{

    private final Logger LOG = LoggerFactory.getLogger(WorldConfigModuleEdit.class);

    private MenuItem menuItemCreateWorm;
    private MenuItem menuItemCreateFood;
    private MenuItem menuItemCreateToxicant;
    private MenuItem menuItemEditSwitch;

    private WorldConfigTabWorld worldTabs;
    private List<WorldConfigTabWorm> wormsTabs;
    private List<WorldConfigTabMolecule> foodsTabs;
    private List<WorldConfigTabMolecule> toxicantTabs;

    private boolean editable = true;
    private boolean editModeSwitch = true;

    @Override
    public String getName(){
        return "edit";
    }

    @Override
    public void setupModule(WorldConfigViewer world){
        MenuManager menu = world.menu;
        menu.addMenu("menu.edit");
        menuItemCreateWorm = Objects.requireNonNull(menu.getById("menu.edit.createworm"));
        menuItemCreateFood = Objects.requireNonNull(menu.getById("menu.edit.createfood"));
        menuItemCreateToxicant = Objects.requireNonNull(menu.getById("menu.edit.createtoxicant"));
        menuItemEditSwitch = Objects.requireNonNull(menu.getById("menu.edit.switch"));
        //
        worldTabs = world.world;
        wormsTabs = world.worms;
        foodsTabs = world.foods;
        toxicantTabs = world.toxicant;
        // editable
        menuItemCreateWorm.setDisable(!editable);
        menuItemCreateFood.setDisable(!editable);
        menuItemCreateToxicant.setDisable(!editable);
        worldTabs.setEditable(editable);
        wormsTabs.forEach(w -> w.setEditable(editable));
        foodsTabs.forEach(f -> f.setEditable(editable));
        toxicantTabs.forEach(t -> t.setEditable(editable));
        menuItemEditSwitch.setDisable(!editModeSwitch);
        //
        world.file.setEditable(true);
    }

    @Override
    public void destroy(){
        host.menu.removeMenu("menu.edit");
        super.destroy();
    }

    @Override
    public boolean processMenuEvent(ActionEvent e, String action){
        switch (action){
        case "menu.edit.createworm":
            if (editable){
                WormConfig config = createNewWorm();
                if (config != null) addWorm(config);
            }
            break;
        case "menu.edit.cloneworm":
            if (editable){
                WormConfig config;
                if (host.config.wormCount() == 0){
                    config = createNewWorm();
                } else {
                    config = cloneNewWorm();
                }
                if (config != null) addWorm(config);
            }
            break;
        case "menu.edit.rename":
            if (editable) renameUserName();
            break;
        case "menu.edit.createfood":
            if (editable) addMolecule(askNewFood());
            break;
        case "menu.edit.createtoxicant":
            if (editable) addMolecule(askNewToxicant());
            break;
        case "menu.edit.switch":
            setEditable(((CheckMenuItem)e.getSource()).isSelected());
            break;
        default:
            return false;
        }
        return true;
    }

    @Override
    public void processOpenNewWorldConfig(WorldConfig config){
        worldTabs = host.world;
        worldTabs.setEditable(editable);
    }

    @SuppressWarnings("unused")
    public boolean isEditable(){
        return editable;
    }

    public void setEditable(boolean editable){
        if (!editModeSwitch) return;
        this.editable = editable;
        if (worldTabs != null){
            worldTabs.setEditable(editable);
            wormsTabs.forEach(w -> w.setEditable(editable));
            foodsTabs.forEach(f -> f.setEditable(editable));
            toxicantTabs.forEach(t -> t.setEditable(editable));
        }
        if (host != null){
            menuItemCreateWorm.setDisable(!editable);
            menuItemCreateFood.setDisable(!editable);
            menuItemCreateToxicant.setDisable(!editable);
        }
    }

    public void setEditModeSwitch(boolean enable){
        editModeSwitch = enable;
        if (host != null){
            menuItemEditSwitch.setDisable(!editModeSwitch);
        }
    }

    public void addWorm(WormConfig config){
        LOG.debug("create worm : user={}, id={}", config.user.get(), config.worm.get());
        host.config.addWorm(config);
        WorldConfigTabWorm tab = host.createWormTab(config);
        tab.setEditable(editable);
        tab.add();
        tab.select();
        host.file.setCurrentFileModified(true);
        host.module.root.processWormCreate(config);
    }

    public void addMolecule(MoleculeConfig config){
        LOG.debug("create {} : id={}", config.type, config.ID.get());
        host.config.addMolecule(config);
        WorldConfigTabMolecule tab = host.createMoleculeTab(config);
        tab.setEditable(editable);
        tab.add();
        tab.select();
        host.file.setCurrentFileModified(true);
        host.module.root.processMoleculeCreate(config);
    }

    public Optional<WormConfig> deleteWorm(String user, String worm){
        LOG.debug("delete worm : user={}, id={}", user, worm);

        Optional<WorldConfigTabWorm> ret = host.getWorm(user, worm);
        ret.ifPresent(tab -> {
            WormConfig wormConfig = tab.target;
            if (!wormsTabs.remove(tab) || host.config.removeWorm(wormConfig) == null){
                throw new RuntimeException("delete worm error");
            }

            tab.remove();

            host.file.setCurrentFileModified(true);
            host.module.root.processWormDelete(wormConfig);
        });
        return ret.map(t -> t.target);
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean deleteWorm(WormConfig worm){
        return deleteWorm(worm.user.get(), worm.worm.get()).isPresent();
    }

    public Optional<MoleculeConfig> deleteMolecule(MoleculeType type, String id){
        LOG.debug("delete {} : id={}", type, id);
        if (type != MoleculeType.FOOD && type != MoleculeType.TOXICANT){
            throw new IllegalArgumentException("unknown molecule type : " + type);
        }

        Optional<WorldConfigTabMolecule> ret = host.getMolecule(type, id);
        ret.ifPresent(tab -> {
            MoleculeConfig config = tab.target;
            boolean isFood = type == MoleculeType.FOOD;

            if (isFood){
                if (!foodsTabs.remove(tab) || host.config.removeMolecule(config) == null){
                    throw new RuntimeException("delete food error");
                }
            } else {
                if (!toxicantTabs.remove(tab) || host.config.removeMolecule(config) == null){
                    throw new RuntimeException("delete toxicant error");
                }
            }

            tab.remove();

            host.file.setCurrentFileModified(true);
            host.module.root.processMoleculeDelete(config);
        });
        return ret.map(t -> t.target);
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean deleteMolecule(MoleculeConfig molecule){
        return deleteMolecule(molecule.type, molecule.ID.get()).isPresent();
    }

    private Tab getCurrentSelectedTab(){
        return host.content.getSelectionModel().getSelectedItem();
    }

    public WormConfig createNewWorm(){
        String input = askUserName("Create new worm");
        if (input == null) return null;
        return createNewWorm(input);
    }

    public WormConfig cloneNewWorm(){
        Tab currentTab = getCurrentSelectedTab();
        if (!(currentTab instanceof WorldConfigTabWorm)){
            host.content.getSelectionModel().select(1);
            return null;
        }
        WormConfig config = ((WorldConfigTabWorm)currentTab).target;
        String user = config.user.get();
        try {
            int id = Integer.parseInt(config.worm.get());
            return new WormConfig(user, Integer.toString(id + 1), config);
        } catch (NumberFormatException e){
            List<WorldConfigTabWorm> list = host.getWorm(user);
            for (int i = 0; ; i++){
                String nid = Integer.toString(i);
                if (list.stream().map(w -> w.target.worm.get()).noneMatch(nid::equals)){
                    return new WormConfig(user, nid, config);
                }
            }
        }
    }

    public WormConfig createNewWorm(String user){
        Objects.requireNonNull(user, "user ID");
        List<WorldConfigTabWorm> list = host.getWorm(user);
        if (list.isEmpty()){
            WormConfig c = new WormConfig(user, "0", null);
            c.size.set(3);
            if (host.config.wormCount() != 0){
                WormConfig another = host.config.worms().get(0);
                c.timeDecay.set(another.timeDecay.get());
                c.stepDecay.set(another.stepDecay.get());
            }
            return c;
        } else {
            WorldConfigTabWorm last = list.get(list.size() - 1);
            try {
                int id = Integer.parseInt(last.target.worm.get());
                return new WormConfig(user, Integer.toString(id + 1), last.target);
            } catch (NumberFormatException e){
                for (int i = 0; ; i++){
                    String nid = Integer.toString(i);
                    if (list.stream().map(w -> w.target.worm.get()).noneMatch(nid::equals)){
                        return new WormConfig(user, nid, last.target);
                    }
                }
            }
        }
    }

    MoleculeConfig askNewFood(){
        if (foodsTabs.isEmpty()){
            return new MoleculeConfig(MoleculeType.FOOD, "1");
        } else {
            WorldConfigTabMolecule last = foodsTabs.get(foodsTabs.size() - 1);
            try {
                int id = Integer.parseInt(last.target.ID.get());
                return new MoleculeConfig(Integer.toString(id + 1), last.target);
            } catch (NumberFormatException e){
                for (int i = 1; ; i++){
                    String nid = Integer.toString(i);
                    if (foodsTabs.stream().map(w -> w.target.ID.get()).noneMatch(nid::equals)){
                        return new MoleculeConfig(nid, last.target);
                    }
                }
            }
        }
    }

    MoleculeConfig askNewToxicant(){
        if (toxicantTabs.isEmpty()){
            return new MoleculeConfig(MoleculeType.TOXICANT, "1");
        } else {
            WorldConfigTabMolecule last = toxicantTabs.get(toxicantTabs.size() - 1);
            try {
                int id = Integer.parseInt(last.target.ID.get());
                return new MoleculeConfig(Integer.toString(id + 1), last.target);
            } catch (NumberFormatException e){
                for (int i = 1; ; i++){
                    String nid = Integer.toString(i);
                    if (toxicantTabs.stream().map(w -> w.target.ID.get()).noneMatch(nid::equals)){
                        return new MoleculeConfig(nid, last.target);
                    }
                }
            }
        }
    }

    void renameUserName(){
        Tab tab = getCurrentSelectedTab();
        if (!(tab instanceof WorldConfigTabWorm)) return;
        String newUser = askUserName("Rename User");
        if (newUser != null) changeUserName(((WorldConfigTabWorm)tab).target.user.get(), newUser);
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean changeUserName(String user, String newName){
        Objects.requireNonNull(user, "null user name");
        Objects.requireNonNull(newName, "null new name");
        LOG.debug("change user name : {} -> {}", user, newName);
        if (host.config.containUser(newName)){
            MessageSession.showErrorMessageDialog("Change User Name",
                                                  "name '" + newName + "' has been used");
            return false;
        }
        List<WormConfig> modifyWorms = new ArrayList<>();
        for (WorldConfigTabWorm tab : wormsTabs){
            WormConfig worm = tab.target;
            if (user.equals(worm.user.get())){
                ((SimpleStringProperty)worm.user).set(newName);
                modifyWorms.add(worm);
            }
        }
        return !modifyWorms.isEmpty();
    }

    String askUserName(String title){
        Set<String> avoid = host.config.worms().stream().map(w -> w.user.get()).collect(Collectors.toSet());
        return MessageSession.showInputDialog(title,
                                              "New User ID",
                                              WormName.genName(avoid),
                                              testUserName(avoid));
    }

    static ErrorFunction.EFunction<String, String, RuntimeException> testUserName(Set<String> avoid){
        return name -> {
            if (name.contains(" ") || name.contains("\t")) throw new RuntimeException("contain space character");
            if (avoid.contains(name)) throw new RuntimeException("has been used");
            return null;
        };
    }
}
