/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.world.builder;

import javafx.event.ActionEvent;

import cclo.hanitu.gui.AbstractModule;
import cclo.hanitu.world.MoleculeConfig;
import cclo.hanitu.world.WorldConfig;
import cclo.hanitu.world.WormConfig;

/**
 * @author antonio
 */
public abstract class AbstractWorldConfigModule extends AbstractModule<WorldConfigViewer> implements WorldConfigModule{

    @Override
    public boolean processMenuEvent(ActionEvent e, String action){
        return false;
    }

    @Override
    public void processOpenNewWorldConfig(WorldConfig config){
    }

    @Override
    public void processWorldModify(WorldConfig config){
    }

    @Override
    public void processWormCreate(WormConfig config){
    }

    @Override
    public void processWormDelete(WormConfig config){
    }

    @Override
    public void processWormModify(WormConfig config){
    }

    @Override
    public void processMoleculeCreate(MoleculeConfig config){
    }

    @Override
    public void processMoleculeDelete(MoleculeConfig config){
    }

    @Override
    public void processMoleculeModify(MoleculeConfig config){
    }
}
