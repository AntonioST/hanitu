/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.exe;

import java.util.Arrays;
import java.util.List;

import javafx.geometry.Insets;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.StageStyle;

import cclo.hanitu.Base;
import cclo.hanitu.HanituInfo;
import cclo.hanitu.gui.FXApplication;
import cclo.hanitu.gui.MessageSession;

import static cclo.hanitu.HanituInfo.AUTHORS;

/**
 * @author antonio
 */
public class About extends FXApplication{

    private static final String TITLE = Base.loadProperties("gui").getProperty("menu.help.about");

    private static final String HELP_DOC;

    static{
        StringBuilder sb = new StringBuilder();
        sb.append("~**{Hanitu~}  ").append(HanituInfo.VERSION).append("\n");
        sb.append("~*{website~} : ~_b{").append(HanituInfo.WEB_SITE[0][1]).append("~}\n");
        sb.append("~*{authors~} : \n");
        for (String[] author : AUTHORS){
            sb.append("  ").append(author[0]).append(" ~_b{").append(author[1]).append("~}\n");
        }
        sb.append("~*{license~} : ").append(HanituInfo.LICENSE).append("\n");
        HELP_DOC = sb.toString();
    }

    public About(){
        super(StageStyle.UTILITY);
        primaryStage.setTitle(TITLE);
        primaryStage.setMinWidth(200);
        primaryStage.setMinHeight(100);
        //
        HBox controlRegion = MessageSession.addControl(new HBox(), MessageSession.NONE_OPTIONS, null);
        //
        root.getChildren().addAll(controlRegion);
        Insets value = new Insets(5);
        root.getChildren().forEach(c -> VBox.setMargin(c, value));
    }

    @Override
    protected boolean processKeyboardEvent(KeyEvent e){
        switch (e.getCode()){
        case ENTER:
        case ESCAPE:
            primaryStage.close();
            break;
        default:
            return false;
        }
        return true;
    }

    public void show(){
        MessageSession.addTextWithUrlEvent(root, HELP_DOC);
        primaryStage.requestFocus();
        primaryStage.show();
    }

    public void show(String... message){
        show(Arrays.asList(message));
    }

    public void show(List<String> message){
        StringBuilder builder = new StringBuilder();
        builder.append(HELP_DOC);
        for (String arg : message){
            builder.append("\n").append(arg);
        }
        MessageSession.addTextWithUrlEvent(root, builder.toString());
        primaryStage.requestFocus();
        primaryStage.show();
    }
}
