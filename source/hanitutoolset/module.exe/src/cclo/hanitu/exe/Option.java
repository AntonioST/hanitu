/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.exe;

import java.lang.annotation.*;

/**
 * option annotation.
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ExtendOptions.class)
@Documented
public @interface Option{

    /**
     * name, which length large than 1.
     *
     * @return long name
     */
    String value() default "";

    /**
     * name, whose length equal to 1.
     *
     * @return short name
     */
    char shortName() default 0;

    /**
     * option's argument. Only support zero or one argument.
     *
     * @return the name of the argument.
     */
    String arg() default "";

    /**
     * option group and order in the help document.
     *
     * @return order
     */
    int order() default -1;

    /**
     * The description document of this options.
     *
     * @return document
     */
    String description() default "";

    /**
     * is this option is standard options which will present in the help document.
     *
     * @return standard option or not
     */
    boolean standard() default true;
}
