/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.exe;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.HanituInfo;

import static cclo.hanitu.exe.HelpBuilder.getHelp;

/**
 * Main class. Parsing command line argument and setting variable in {@link Executable} and run them
 *
 * @author antonio
 */
public class Main{

    /**
     * cause a interrupt (exception) when command-line argument contain `--help` or `-h`.
     */
    public static class HanituHelpInterruption extends RuntimeException{}

    public static class HanituHelpNonStandardInterruption extends HanituHelpInterruption{}

    /**
     * cause a interrupt (exception) when command-line argument contain `--version`.
     */
    public static class HanituVersionInterruption extends RuntimeException{}

    @SuppressWarnings("unused")
    public static class HanituExitInterruption extends RuntimeException{

        public final int exitCode;

        public HanituExitInterruption(){
            exitCode = 0;
        }

        public HanituExitInterruption(int exitCode){
            this.exitCode = exitCode;
        }

        public HanituExitInterruption(int exitCode, String message){
            super(message);
            this.exitCode = exitCode;
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    /**
     * short-cut {@link Executable}. Use short-cut name as key map to correspond class.
     */
    static final Map<String, Class> SHORTCUT = new HashMap<>();

    static{
        ServiceLoader<ExecutableManager> loader = ServiceLoader.load(ExecutableManager.class);
        for (ExecutableManager manager : loader){
            manager.getExecutables().forEach((name, exe) -> {
                LOG.debug("add {} : {}", name, exe.getName());
                SHORTCUT.put(name, exe);

            });
        }
    }

    /**
     * main method.
     *
     * @param args command-line argument with class name at first place.
     */
    public static void main(String[] args){
        try {
            mainIO(args);
        } catch (Throwable e){
            LOG.error("main", e);
        }
    }

    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    private static void mainIO(String[] args) throws Throwable{
        if (args.length == 0){
            getHelp().buildHelpDoc(System.out, new Default());
            System.exit(0);
        } else {
            Executable run;
            Class cls;
            int startIndex = 0;
            if (SHORTCUT.containsKey(args[0])){
                cls = SHORTCUT.get(args[0]);
                startIndex = 1;
            } else if (args[0].startsWith("-")){
                cls = Default.class;
            } else {
                cls = Class.forName(args[0]);
                startIndex = 1;
            }
            try {
                run = (Executable)cls.newInstance();
            } catch (IllegalAccessException e){
                try {
                    Method method = cls.getMethod("getInstance");
                    run = (Executable)method.invoke(null);
                } catch (NoSuchMethodException | SecurityException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e1){
                    e.addSuppressed(e1);
                    throw e;
                }
            }
            try {
                List<String> ls = parseGlobalArguments(Arrays.asList(args), startIndex);
                LOG.info("main version : {}", HanituInfo.VERSION);
                LOG.info("toolset version : {}", HanituInfo.TOOLSET_VERSION);
                ExecutableData.set(run, ls);
                run.start();
            } catch (HanituHelpNonStandardInterruption e){
                LOG.trace("non-standard help interruption");
                getHelp().buildHideOptionDoc(System.out, run);
            } catch (HanituHelpInterruption e){
                LOG.trace("help interruption");
                getHelp().buildHelpDoc(System.out, run);
            } catch (HanituVersionInterruption e){
                LOG.trace("version interruption");
                if (run instanceof NativeExecutable){
                    try {
                        System.out.println(((NativeExecutable)run).version());
                    } catch (Throwable th){
                        LOG.error("fetch native program version text", th);
                    }
                } else {
                    System.out.println(HanituInfo.VERSION_DESCRIPTION);
                }
            } catch (HanituExitInterruption e){
                LOG.trace("exit interruption");
                LOG.info("exit message : {}", e.getMessage());
                LOG.info("exit code : {}", e.exitCode);
                System.exit(e.exitCode);
            }
        }
    }

    private static List<String> parseGlobalArguments(List<String> args, int startIndex){
        Iterator<String> it = args.subList(startIndex, args.size()).iterator();
        ArrayList<String> ret = new ArrayList<>(args.size());
        while (it.hasNext()){
            String arg = it.next();
            switch (arg){
            case "-h":
            case "--help":
                throw new HanituHelpInterruption();
            case "--help!":
                throw new HanituHelpNonStandardInterruption();
            case "--version":
                throw new HanituVersionInterruption();
            default:
                ret.add(arg);
                break;
            }
        }
        return ret;
    }
}
