/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.exe;

import java.util.*;

import cclo.hanitu.Base;
import cclo.hanitu.gui.FXApplication;

import static cclo.hanitu.HanituInfo.*;

/**
 * The default class used to print the general help document.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class Default extends Executable{

    @Option(shortName = 'h', value = "help", order = 0, description = "option.help")
    public boolean help;

    @Option(value = "version", order = 1, description = "option.version")
    public boolean version;

    @Option(value = "about", standard = false,
      description = "show about dialog")
    public boolean about;

    @Option(value = "check", standard = false,
      description = "checking hanitu environment. no output any message represent all checking passed")
    public boolean checkEnv;

    @Option(value = "print-self", standard = false,
      description = "print the executable command show in usage section in help document")
    public boolean printUsage;

    @Option(value = "list-classpath", standard = false,
      description = "list the classpath")
    public boolean listClassPath;

    @Option(value = "list-path", standard = false,
      description = "list path which get from system environment")
    public boolean listEnvPath;

    @Option(value = "list-all", standard = false,
      description = "list all executable tool")
    public boolean listAllTool;

    public final List<String> arguments = new ArrayList<>();

    @Override
    public String getName(){
        return Base.loadProperties().getProperty("name");
    }

    @Override
    public String getDescription(){
        return Base.loadProperties().getProperty("desp");
    }

    @Override
    public Properties getProperties(){
        return Base.loadProperties();
    }

    @Override
    public List<String> getUsage(){
        return Arrays.asList("TOOL", "[OPTION]", "[ARGS]");
    }

    @Override
    public boolean extendArgument(int index, String value, ListIterator<String> it){
        arguments.add(value);
        return true;
    }

    @Override
    public String extendHelpDoc(){
        HelpBuilder help = new HelpBuilder();
        StringBuilder sb = new StringBuilder();
        StringBuilder tmp = new StringBuilder();
        sb.append(HelpBuilder.DOC_TITLE).append("list of ~_{TOOL~}");
        sb.append(HelpBuilder.DOC_BLOCK_START);
        for (String line : HelpBuilder.align(Main.SHORTCUT, Comparator.comparing(k -> Main.SHORTCUT.get(k).getName()))){
            sb.append(HelpBuilder.DOC_LIST_0).append(line);
        }
        sb.append(HelpBuilder.DOC_BLOCK_END);
        //version
        sb.append(HelpBuilder.DOC_NEWLINE).append(help.getPreDefined("title.version"));
        sb.append(HelpBuilder.DOC_BLOCK_START).append(VERSION).append(HelpBuilder.DOC_BLOCK_END);
        //license
        sb.append(HelpBuilder.DOC_NEWLINE).append(help.getPreDefined("title.license"));
        sb.append(HelpBuilder.DOC_BLOCK_START).append(LICENSE).append(HelpBuilder.DOC_BLOCK_END);
        //author
        sb.append(HelpBuilder.DOC_NEWLINE).append(help.getPreDefined("title.authors"));
        tmp.setLength(0);
        HelpBuilder.align(tmp, AUTHORS, 0);
        sb.append(HelpBuilder.DOC_BLOCK_START).append(tmp).append(HelpBuilder.DOC_BLOCK_END);
        //group
        sb.append(HelpBuilder.DOC_NEWLINE).append(help.getPreDefined("title.group"));
        sb.append(HelpBuilder.DOC_BLOCK_START);
        for (String line : DEVELOPER_DESCRIPTION.split("\n")){
            sb.append(line).append("\n");
        }
        sb.append(HelpBuilder.DOC_BLOCK_END);
        //website
        sb.append(HelpBuilder.DOC_NEWLINE).append(help.getPreDefined("title.website"));
        tmp.setLength(0);
        HelpBuilder.align(tmp, WEB_SITE, 0);
        sb.append(HelpBuilder.DOC_BLOCK_START).append(tmp).append(HelpBuilder.DOC_BLOCK_END);
        return sb.toString();
    }

    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    @Override
    public void start() throws Throwable{
        if (printUsage){
            if (Base.PRG != null){
                System.out.println(Base.PRG);
            } else {
                System.out.println("  java -jar " + Base.HANITU_JAR);
            }
        }
        if (listAllTool){
            System.out.println("Tool");
            Map<String, String> map = new HashMap<>();
            Main.SHORTCUT.forEach((k, v) -> map.put(k, v.getName()));
            HelpBuilder.align(map, Comparator.comparing(map::get))
              .forEach(System.out::println);
        }
        if (listClassPath){
            System.out.println("Class Path");
            NativeExecutable.listClassPath().forEach(p -> System.out.println("  " + p));
        }
        if (listEnvPath){
            System.out.println("Environment Path");
            NativeExecutable.listEnvPath().forEach(p -> System.out.println("  " + p));
        }
        if (checkEnv){
            System.out.println("jar file location : " + Base.HANITU_JAR);
            if (Base.HOME == null){
                System.out.println("set property '" + Base.HOME_PROPERTY + "' to the hanitu install path");
                System.out.println("  use 'java -D" + Base.HOME_PROPERTY + "=... ...'");
                System.out.println("  or use 'env " + Base.HOME_ENVIRONMENT + "=... java ...'");
                System.out.println("  otherwise, use the location of the hanitu jar file");
                System.out.println();
            } else {
                System.out.println(Base.HOME_PROPERTY + "=" + Base.HOME);
            }
            if (Base.PRG == null){
                System.out.println("set property '" + Base.PRG_PROPERTY + "' the name of the hanitu caller");
                System.out.println("  this property only used in help document.");
                System.out.println("  use 'java -D" + Base.PRG_PROPERTY + "=... ...");
                System.out.println("  By default, usage command will use 'java' instead");
                System.out.println();
            }
        }
        if (about){
            FXApplication.launch(About.class).show(arguments);
        }
    }
}
