/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.exe;

import java.util.List;
import java.util.ListIterator;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Runnable class used for Hanitu Tool Set.
 *
 * @author antonio
 */
public abstract class Executable{

    protected static final Logger LOG = LoggerFactory.getLogger(Executable.class);

    public abstract String getName();

    public abstract String getDescription();

    public abstract Properties getProperties();

    public List<String> getUsage(){
        return null;
    }

    /**
     * handle options for special case.
     *
     * @param key   matching option name
     * @param value option value, null if correspond option doesn't need argument
     * @param it    arguments list iterator.
     * @return accept this option or not
     */
    public boolean extendOption(String key, String value, ListIterator<String> it){
        return false;
    }

    /**
     * handle arguments for special case.
     *
     * @param index the place of the argument
     * @param value argument
     * @param it    arguments list iterator.
     * @return accept this argument or not
     */
    @SuppressWarnings("UnusedParameters")
    public boolean extendArgument(int index, String value, ListIterator<String> it){
        return false;
    }

    /**
     * extend help document with document formatting keyword.
     *
     * @return help document, null represent no extend
     */
    public String extendHelpDoc(){
        return null;
    }

    public abstract void start() throws Throwable;

    protected void exit(int exitCode){
        throw new Main.HanituExitInterruption(exitCode);
    }

    protected void exit(int exitCode, String message){
        throw new Main.HanituExitInterruption(exitCode, message);
    }
}
