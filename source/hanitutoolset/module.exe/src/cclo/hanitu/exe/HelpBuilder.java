/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.exe;

import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import cclo.hanitu.Base;
import cclo.hanitu.util.RichMessageTranslator;

import static cclo.hanitu.HanituInfo.HANITU_TOOL_SET_NAME;
import static cclo.hanitu.util.RichMessageTranslator.echo;
import static java.util.stream.Collectors.toList;

/**
 * The util tool to generate terminal-side help document.
 *
 * ###Default Layout
 *
 * * Tool name
 * * Tool tip
 * * Usage
 * * Description
 * * Tool description
 * * Options
 * * Argument
 * * Extend Help Document
 *
 * @author antonio
 */
@SuppressWarnings("WeakerAccess")
public class HelpBuilder{

    private static final Class<? extends HelpBuilder> HELP_CLASS;

    static{
        // find custom help builder class
        String cls = Base.getProperty("cclo.hanitu.helpbuilder");
        Class<? extends HelpBuilder> c;
        if (cls == null){
            c = HelpBuilder.class;
        } else {
            try {
                c = (Class<? extends HelpBuilder>)Class.forName(cls);
                if (!HelpBuilder.class.isAssignableFrom(c)){
                    throw new RuntimeException(c.getName() + " cannot cast to " + HelpBuilder.class.getName());
                }
            } catch (ClassNotFoundException e){
                throw new RuntimeException(e);
            }
        }
        HELP_CLASS = c;
    }

    private static final int COLUMNS;

    static{
        String c = System.getProperty("cclo.hanitu.columns");
        int cc;
        try {
            cc = Integer.parseInt(c);
        } catch (NumberFormatException | NullPointerException e){
            cc = Base.getIntegerProperty("cclo.hanitu.columns", 100);
        }
        COLUMNS = cc;
    }

    //
    private static final String DOC_PRIVATE_TITLE = "%TITLE%";
    private static final String DOC_PRIVATE_BLOCK_START = "%BLOCK_START%";
    private static final String DOC_PRIVATE_BLOCK_END = "%BLOCK_END%";
    private static final String DOC_PRIVATE_LIST = "%LIST%";
    private static final String DOC_PRIVATE_LIST_0 = DOC_PRIVATE_LIST + "0%";
    private static final String DOC_PRIVATE_LIST_1 = DOC_PRIVATE_LIST + "1%";
    private static final String DOC_PRIVATE_LIST_2 = DOC_PRIVATE_LIST + "2%";
    private static final String DOC_PRIVATE_NEWLINE = "%NEWLINE%";
    /**
     * extend help document formatting keyword. mark following line is a title.
     */
    public static final String DOC_TITLE = "\n" + DOC_PRIVATE_TITLE + "\n";
    /**
     * extend help document formatting keyword. make indent level increase
     */
    public static final String DOC_BLOCK_START = "\n" + DOC_PRIVATE_BLOCK_START + "\n";
    /**
     * extend help document formatting keyword. make indent level decrease
     */
    public static final String DOC_BLOCK_END = "\n" + DOC_PRIVATE_BLOCK_END + "\n";
    /**
     * extend help document formatting keyword. print a leading character.
     */
    public static final String DOC_LIST_0 = "\n" + DOC_PRIVATE_LIST_0;
    @SuppressWarnings("unused")
    public static final String DOC_LIST_1 = "\n" + DOC_PRIVATE_LIST_1;
    @SuppressWarnings("unused")
    public static final String DOC_LIST_2 = "\n" + DOC_PRIVATE_LIST_2;
    /**
     * extend help document formatting keyword. new line.
     */
    public static final String DOC_NEWLINE = "\n" + DOC_PRIVATE_NEWLINE + "\n";


    public static HelpBuilder getHelp(){
        try {
            return HELP_CLASS.newInstance();
        } catch (InstantiationException | IllegalAccessException e){
            throw new RuntimeException(e);
        }
    }

    private final Properties preDefined = Base.loadProperties(HelpBuilder.class);

    private final String LIST_HEAD_LEVEL_0 = getPreDefined("list.level.0", "*");
    private final String LIST_HEAD_LEVEL_1 = getPreDefined("list.level.1", "+");
    private final String LIST_HEAD_LEVEL_2 = getPreDefined("list.level.2", "-");

    public String getPreDefined(String key){
        return echo(preDefined.getProperty(key));
    }

    public String getPreDefined(String key, String def){
        return echo(preDefined.getProperty(key, def));
    }

    /**
     * generate help document with default layout and format.
     *
     * @param sb output target
     * @param r  target for getting the information
     */
    public void buildHelpDoc(Appendable sb, Executable r) throws IOException{
        try {
            Class<? extends Executable> cls = r.getClass();
            Properties properties = r.getProperties();
            //title
            appendTitle(sb, r.getName());
            //usage
            String name = Main.SHORTCUT.entrySet().stream()
              .filter(e -> e.getValue() == cls)
              .map(Map.Entry::getKey)
              .findFirst()
              .orElse(cls.getSimpleName().toLowerCase());
            List<String> usage = r.getUsage();
            sb.append("\n");
            sb.append(getPreDefined("title.usage"));
            sb.append("\n");
            if (usage != null){
                appendUsage(sb, usage);
            } else {
                appendUsage(sb, name, ExecutableData.getArgumentInfo(cls));
            }
            sb.append("\n");
            //description
            sb.append(getPreDefined("title.description"));
            appendDescription(sb, r.getDescription());
            sb.append("\n");
            //self options
            for (Option op : ExecutableData.getOptionInfo(cls)){
                if (!op.standard()) continue;
                char c = op.shortName();
                String l = op.value();
                String arg = op.arg();
                String d = op.description();
                if (properties != null && !d.isEmpty() && !d.contains(" ")){
                    d = properties.getProperty(d, d);
                }
                appendOption(sb,
                             c == 0? null: c,
                             l.isEmpty()? null: l,
                             arg.isEmpty()? null: arg,
                             d.isEmpty()? null: d);
            }
            //self argument
            for (Argument argument : ExecutableData.getArgumentInfo(cls)){
                String d = argument.description();
                if (properties != null && !d.isEmpty() && !d.contains(" ")){
                    d = properties.getProperty(d, d);
                }
                appendArgument(sb, argument.value(), d);
            }
            //self extend help
            appendExtendHelp(sb, r.extendHelpDoc());
            sb.append("\n");
        } catch (RuntimeException e){
            throw new RuntimeException("Executable : " + r.getClass(), e);
        }
    }

    protected void buildHideOptionDoc(Appendable sb, Executable r) throws IOException{
        Properties properties = r.getProperties();
        for (Option op : ExecutableData.getOptionInfo(r.getClass())){
            if (op.standard()) continue;
            char c = op.shortName();
            String l = op.value();
            String arg = op.arg();
            String d = op.description();
            if (properties != null && !d.isEmpty() && !d.contains(" ")){
                d = properties.getProperty(d, d);
            }
            appendOption(sb,
                         c == 0? null: c,
                         l.isEmpty()? null: l,
                         arg.isEmpty()? null: arg,
                         d.isEmpty()? null: d);
        }
    }

    /**
     * append program title.
     *
     * @param builder document buffer
     * @param name       tooltip annotation
     */
    protected void appendTitle(Appendable builder, String name) throws IOException{
        if (name == null || name.isEmpty()) throw new IllegalArgumentException("empty title");
        builder.append(echo("~*{" + HANITU_TOOL_SET_NAME + "~}"));
        builder.append(" - ").append(name).append("\n");
    }

    /**
     * append program description.
     *
     * @param builder document buffer
     * @param desp    description annotation
     */
    protected void appendDescription(Appendable builder, String desp) throws IOException{
        if (desp != null){
            builder.append("\n");
            warpPureText(builder, echo(desp), 1);
        }
    }

    /**
     * append usage. Default form is "java -jar _jar_ _class_ [options] _args_...".
     *
     * `className` is the fully-qualified-name of the class, but some class can have short-cut name. The detail
     * is wrote in the {@link Main}.
     *
     * @param builder   document buffer
     * @param className name
     * @param args      argument list
     */
    protected void appendUsage(Appendable builder, String className, List<Argument> args) throws IOException{
        if (Base.PRG == null){
            builder.append("  java -jar ").append(Base.HANITU_JAR).append(" ").append(className);
        } else if (Base.PRG.equals("hanitu_tool")){
            builder.append("  ").append(Base.PRG).append(" ").append(className);
        } else {
            builder.append("  ").append(Base.PRG);
        }
        builder.append(" [options]");
        for (Argument arg : args){
            if (arg != null){
                if (arg.required()){
                    builder.append(" ").append(arg.value());
                } else {
                    builder.append(" [").append(arg.value()).append("]");
                }
                if (arg.index() < 0){
                    builder.append("...");
                }
            } else {
                builder.append(" [?]");
            }
        }
        builder.append("\n");
    }

    protected void appendUsage(Appendable builder, List<String> args) throws IOException{
        if (Base.PRG != null){
            builder.append("  ").append(Base.PRG);
        } else {
            builder.append("  java -jar").append(Base.HANITU_JAR);
        }
        for (String arg : args){
            builder.append(" ").append(arg);
        }
        builder.append("\n");
    }

    /**
     * append option.
     *
     * @param builder     document buffer
     * @param s           the short name of the option
     * @param l           the long name of the option
     * @param arg         the argument name of the option, null if this option doesn't need argument.
     * @param description the description of the option
     */
    public void appendOption(Appendable builder, Character s, String l, String arg, String description)
      throws IOException{
        builder.append("  ");
        if (s != null){
            builder.append(echo("~*{-" + s + "~}"));
            if (l != null){
                builder.append(", ");
            }
        }
        if (l != null){
            builder.append(echo("~*{--" + l + "~}"));
        }
        if (arg != null){
            builder.append(" [").append(arg).append("]");
        }
        if (description != null){
            builder.append("\n");
            warpPureText(builder, echo(description), 2);
        }
        builder.append("\n");
    }

    public void appendArgument(Appendable builder, String name, String description) throws IOException{
        builder.append("  ").append(echo("~*{" + name + "~}"));
        if (!description.isEmpty()){
            builder.append("\n");
            warpPureText(builder, echo(description), 2);
        }
        builder.append("\n");
    }

    /**
     * append program self-extend help document.
     *
     * @param sb  document buffer
     * @param doc help document with document formatting keyword.
     * @see Executable#extendHelpDoc()
     */
    protected void appendExtendHelp(Appendable sb, String doc) throws IOException{
        StringBuilder builder = new StringBuilder();
        warpFormattedText(builder, doc);
        sb.append(builder);
    }

    /**
     * table content.
     *
     * @param builder document buffer
     * @param matrix  table content
     * @param indent indent level, 1 level for 2 spaces
     */
    public static void align(StringBuilder builder, String[][] matrix, int indent){
        int row = matrix.length;
        int column = Stream.of(matrix).mapToInt(s -> s.length).max().orElse(0);
        List<StringBuilder> list = new ArrayList<>(row);
        //noinspection ForLoopReplaceableByForEach
        for (int i = 0; i < row; i++){
            list.add(new StringBuilder());
        }
        IntStream.range(0, column).forEach(c -> {
            int max = 0;
            for (int r = 0; r < row; r++){
                StringBuilder t = list.get(r);
                t.append(matrix[r][c]);
                max = Math.max(max, t.length());
            }
            for (int r = 0; r < row; r++){
                StringBuilder t = list.get(r);
                int len = t.length();
                while (len < max){
                    t.append(' ');
                    len++;
                }
                t.append(' ');
            }
        });
        if (indent == 0){
            list.forEach(line -> builder.append(line).append("\n"));
        } else {
            StringBuilder tmp = new StringBuilder();
            for (int i = 0; i < indent; i++){
                tmp.append("  ");
            }
            String front = tmp.toString();
            list.forEach(line -> builder.append(front).append(line).append("\n"));
        }
    }

    /**
     * table content.
     *
     * @param matrix table content
     */
    @SuppressWarnings("unused")
    public static List<String> align(Map<String, ?> matrix){
        List<String> list = new ArrayList<>(matrix.keySet());
        int max = list.stream()
          .mapToInt(String::length)
          .max().orElse(1);
        return list.stream()
          .map(k -> {
              StringBuilder t = new StringBuilder(k);
              int len = t.length();
              while (len < max){
                  t.append(' ');
                  len++;
              }
              t.append(' ');
              t.append(Objects.toString(matrix.get(k)));
              return t.toString();
          }).collect(toList());
    }

    public static <T> List<String> align(Map<String, T> matrix, Comparator<String> sort){
        List<String> list = new ArrayList<>(matrix.keySet());
        Collections.sort(list, sort);
        int max = list.stream()
          .mapToInt(String::length)
          .max().orElse(1);
        return list.stream()
          .map(k -> {
              StringBuilder t = new StringBuilder(k);
              int len = t.length();
              while (len < max){
                  t.append(' ');
                  len++;
              }
              t.append(' ');
              t.append(Objects.toString(matrix.get(k)));
              return t.toString();
          }).collect(toList());
    }

    public static void warpPureText(Appendable builder, String doc, int indent) throws IOException{
        if (doc == null) return;
        StringBuilder tmp = new StringBuilder();
        boolean first = true;
        for (String line : doc.split("\n")){
            if (!first){
                builder.append("\n");
            } else {
                first = false;
            }
            tmp.setLength(0);
            tmp.append(line);
            int limit = COLUMNS - indent * 2;
            while (tmp.length() > limit){
                if (printableLength(tmp) < limit) break;
                int cut = lastIndexOfWithPrintable(tmp, ' ', limit);
                for (int i = 0; i < indent; i++){
                    builder.append("  ");
                }
                if (cut > 0){
                    builder.append(tmp, 0, cut).append("\n");
                    tmp.delete(0, cut + 1);
                } else {
                    builder.append(tmp, 0, limit - 1).append("-\n");
                    tmp.delete(0, limit - 1);
                }
            }
            if (tmp.length() != 0){
                for (int i = 0; i < indent; i++){
                    builder.append("  ");
                }
                builder.append(tmp).append("\n");
                tmp.setLength(0);
            }
        }
    }

    private static int printableLength(CharSequence seq){
        int count = 0;
        int size = seq.length();
        for (int i = 0; i < size; i++){
            if (seq.charAt(i) == '\033'){
                //noinspection StatementWithEmptyBody
                for (; i < size && seq.charAt(i) != 'm'; i++) ;
            } else {
                count++;
            }
        }
        return count;
    }

    private static int lastIndexOfWithPrintable(CharSequence seq, char ch, int limit){
        int ret = -1;
        int size = seq.length();
        for (int i = 0; i < size && i < limit; i++){
            char c = seq.charAt(i);
            if (c == ch){
                ret = i;
            } else if (c == '\033'){
                int j = i + 1;
                for (; j < size; j++){
                    if (seq.charAt(j) == 'm') break;
                }
                limit += j - i;
                i = j - 1;
            }
        }
        return ret;
    }

    /**
     * parsing `doc` and fit it into column-limit document buffer.
     *
     * @param builder document buffer
     * @param doc     document formatting document.
     */
    public void warpFormattedText(StringBuilder builder, String doc){
        if (doc == null) return;
        int level = 1;
        boolean isTitle = false;
        for (String line : doc.split("\n")){
            if (line.isEmpty()) continue;
            switch (line){
            case DOC_PRIVATE_TITLE:
                isTitle = true;
                level = 0;
                continue;
            case DOC_PRIVATE_BLOCK_START:
                level++;
                continue;
            case DOC_PRIVATE_BLOCK_END:
                level--;
                continue;
            case DOC_PRIVATE_NEWLINE:
                builder.append("\n");
                continue;
            }
            if (line.startsWith(DOC_PRIVATE_LIST)){
                if (builder.charAt(builder.length() - 1) != '\n'){
                    builder.append("\n");
                }
                for (int i = 0; i < level; i++){
                    builder.append("  ");
                }
                String head;
                if (line.startsWith(DOC_PRIVATE_LIST_0)){
                    head = LIST_HEAD_LEVEL_0;
                } else if (line.startsWith(DOC_PRIVATE_LIST_1)){
                    head = LIST_HEAD_LEVEL_1;
                } else if (line.startsWith(DOC_PRIVATE_LIST_2)){
                    head = LIST_HEAD_LEVEL_2;
                } else {
                    head = "";
                }
                assert DOC_PRIVATE_LIST_0.length() == 8;
                builder.append(head).append(line.substring(8)).append("\n");
            } else if (isTitle){
                if (RichMessageTranslator.containExpression(line)){
                    builder.append(echo(line + "\n"));
                } else {
                    builder.append(echo("~*{" + line + " : ~}\n"));
                }
                isTitle = false;
            } else {
                //noinspection EmptyCatchBlock
                try {
                    warpPureText(builder, echo(line), level);
                } catch (IOException e){
                }
            }
        }
    }
}
