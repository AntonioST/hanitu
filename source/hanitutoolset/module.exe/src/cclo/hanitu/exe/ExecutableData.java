/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.exe;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import cclo.hanitu.util.ErrorFunction;

import static cclo.hanitu.exe.Executable.LOG;

/**
 * @author antonio
 */
public class ExecutableData{

    private static final Map<Class<?>, ExecutableData> CACHE = new HashMap<>();

    private final Class<?> targetClass;
    /**
     * options, key name map to {@code Option}
     */
    private final Map<String, Option> optionData = new HashMap<>();
    /**
     * arguments, key index map to {@code Argument}
     */
    private final Map<Integer, Argument> argumentData = new HashMap<>();

    private final Map<String, BiPredicate<Executable, String>> options = new HashMap<>();
    private final Map<Integer, BiPredicate<Executable, String>> arguments = new HashMap<>();
    private final BiPredicate<Executable, String> leftArgument;

    private static ExecutableData getInstance(Class<? extends Executable> cls){
        return CACHE.computeIfAbsent(cls, c -> {
            long start = System.currentTimeMillis();
            ExecutableData ret = new ExecutableData(cls);
            long time = System.currentTimeMillis() - start;
            LOG.debug("load {} cost {} ms", cls.getName(), time);
            return ret;
        });
    }

    public static List<Argument> getArgumentInfo(Class<? extends Executable> r){
        return getInstance(r).argumentData.values().stream()
          .sorted(Comparator.comparingInt(Argument::index))
          .collect(Collectors.toList());
    }

    public static List<Option> getOptionInfo(Class<? extends Executable> r){
        return getInstance(r).optionData.values().stream()
          .distinct()
          .sorted(Comparator.comparingInt(Option::order))
          .collect(Collectors.toList());
    }

    /**
     * create a template with null target.
     *
     * @param cls running target class
     */
    private ExecutableData(Class<? extends Executable> cls){
        LOG.debug("booting class : {}", cls.getName());
        targetClass = cls;
        processOptionField();
        processArgumentField();
        processOptionMethod();
        processArgumentMethod();
        if (arguments.containsKey(-1)){
            leftArgument = arguments.remove(-1);
        } else {
            leftArgument = null;
        }
        int argc = argumentData.size();
        if (argumentData.keySet().stream().mapToInt(Integer::intValue).max().orElse(-1) + 1 != argc){
            throw new IllegalArgumentException("some argument index lost");
        }
    }


    /**
     * handle all the field annotated by {@link Option} in {@link Executable} class.
     */
    private void processOptionField(){
        for (Field f : targetClass.getFields()){
            Option op = f.getAnnotation(Option.class);
            if (op == null) continue;
            if (op.shortName() != 0){
                String n = "-" + op.shortName();
                if (optionData.containsKey(n))
                    throw new IllegalArgumentException("duplicate option name : " + op.shortName());
                optionData.put(n, op);
                options.put(n, setter(f));
                LOG.trace("+ {}", n);
            }
            if (!op.value().isEmpty()){
                String n = "--" + op.value();
                if (optionData.containsKey(n))
                    throw new IllegalArgumentException("duplicate option name : " + op.value());
                if (n.contains(" "))
                    throw new IllegalArgumentException("option contain space : '" + op.value() + "'");
                optionData.put(n, op);
                options.put(n, setter(f));
                LOG.trace("+ {}", n);
            }
        }
    }

    /**
     * handle all the field annotated by {@link Argument} in {@link Executable} class.
     */
    private void processArgumentField(){
        for (Field f : targetClass.getFields()){
            Argument op = f.getAnnotation(Argument.class);
            if (op == null) continue;
            int i = op.index() < 0? -1: op.index();
            if (arguments.containsKey(i))
                throw new IllegalArgumentException("duplicate argument place : "
                                                   + op.value() + " <> " + argumentData.get(i).value());
            if (op.value().contains(" "))
                throw new IllegalArgumentException("argument name contain space : '" + op.value() + "'");
            argumentData.put(i, op);
            arguments.put(i, setter(f));
            LOG.trace("+ {}", op.value());
        }
    }


    /**
     * handle all the method annotated by {@link Option} in {@link Executable} class.
     *
     * @see Executable#extendOption(String, String, ListIterator)
     */
    private void processOptionMethod(){
        try {
            Method method = targetClass.getMethod("extendOption", String.class, String.class, ListIterator.class);
            for (Option op : method.getAnnotationsByType(Option.class)){
                if (op.shortName() != 0){
                    String key = Character.toString(op.shortName());
                    String name = "-" + key;
                    if (optionData.containsKey(name))
                        throw new IllegalArgumentException("duplicate option name : " + op.shortName());
                    optionData.put(name, op);
                    LOG.trace("+ {}", name);
                }
                if (!op.value().isEmpty()){
                    String key = op.value();
                    String name = "--" + key;
                    if (optionData.containsKey(key))
                        throw new IllegalArgumentException("duplicate option name : " + op.value());
                    if (name.contains(" "))
                        throw new IllegalArgumentException("option contain space : '" + op.value() + "'");
                    optionData.put(name, op);
                    LOG.trace("+ {}", name);
                }
            }
        } catch (NoSuchMethodException e){
            throw new RuntimeException("un-reachable : " + e.toString());
        }
    }

    /**
     * handle all the method annotated by {@link Argument} in {@link Executable} class.
     *
     * @see Executable#extendArgument(int, String, ListIterator)
     */
    private void processArgumentMethod(){
        try {
            Method method = targetClass.getMethod("extendArgument", int.class, String.class, ListIterator.class);
            for (Argument op : method.getAnnotationsByType(Argument.class)){
                int i = op.index() < 0? -1: op.index();
                if (arguments.containsKey(i))
                    throw new IllegalArgumentException("duplicate argument place : "
                                                       + op.value() + " <> " + argumentData.get(i).value());
                if (op.value().contains(" "))
                    throw new IllegalArgumentException("argument name contain space : '" + op.value() + "'");
                argumentData.put(i, op);
                LOG.trace("+ {}", op.value());
            }
        } catch (NoSuchMethodException e){
            throw new RuntimeException("un-reachable : " + e.toString());
        }
    }

    private static BiPredicate<Executable, String> setter(Field f){
        String name = f.getName();
        Class<?> type = f.getType();
        if (type == boolean.class){
            return ErrorFunction.warpError((target, v) -> {
                if (v == null){
                    LOG.debug("{} : true", name);
                    f.set(target, true);
                } else if ("+".equals(v) || "true".equals(v)){
                    LOG.debug("{} : true", name);
                    f.set(target, true);
                } else if ("-".equals(v) || "false".equals(v)){
                    LOG.debug("{} : false", name);
                    f.set(target, false);
                } else {
                    throw new RuntimeException("unknown boolean value : " + v);
                }
                return true;
            });
        } else if (type == String.class){
            return ErrorFunction.warpError((target, v) -> {
                LOG.debug("{} : {}", name, v);
                f.set(target, v);
                return true;
            });
        } else if (type == Path.class){
            return ErrorFunction.warpError((target, v) -> {
                LOG.debug("{} : {}", name, v);
                if (v.startsWith("~/")){
                    f.set(target, Paths.get(System.getProperty("user.home")).resolve(v.substring(2)));
                } else {
                    f.set(target, Paths.get(v));
                }
                return true;
            });
        } else if (type == int.class){
            return ErrorFunction.warpError((target, v) -> {
                LOG.debug("{} : {}", name, v);
                f.set(target, Integer.parseInt(v));
                return true;
            });
        } else if (type == float.class){
            return ErrorFunction.warpError((target, v) -> {
                LOG.debug("{} : {}", name, v);
                f.set(target, Float.parseFloat(v));
                return true;
            });
        } else if (type == double.class){
            return ErrorFunction.warpError((target, v) -> {
                LOG.debug("{} : {}", name, v);
                f.set(target, Double.parseDouble(v));
                return true;
            });
        } else if (List.class.isAssignableFrom(type)){
            return ErrorFunction.warpError((target, v) -> {
                LOG.debug("{} += {}", name, v);
                List<String> list = (List<String>)f.get(target);
                if (list == null){
                    list = new ArrayList<>();
                    f.set(target, list);
                }
                list.add(v);
                return true;
            });
        } else {
            throw new IllegalArgumentException("unsupported field option type : " + type.getSimpleName());
        }
    }

    @SuppressWarnings("unused")
    public Option getOptionData(String s){
        return optionData.get(s);
    }

    @SuppressWarnings("unused")
    public int getArgumentLength(){
        return arguments.size();
    }

    @SuppressWarnings("unused")
    public Argument getArgumentData(int index){
        return argumentData.get(index);
    }

    public static void set(Executable target, List<String> args){
        ExecutableData data = ExecutableData.getInstance(target.getClass());
        LOG.debug("target : {}", target.getClass().getName());
        LOG.debug("args : {}", args.toString());
        int argumentIndex = 0;
        ListIterator<String> it = args.listIterator();
        while (it.hasNext()){
            String arg = it.next();
            if (arg.startsWith("-") && !arg.equals("-") && !arg.equals("--")){
                data.parseOption(target, arg, it);
            } else {
                int inc = data.parseArgument(target, arg, argumentIndex, it);
                argumentIndex += 1 + inc;
            }
        }
    }

    private void parseOption(Executable target, String arg, ListIterator<String> it){
        String key = getOptionKey(arg);
        String value = null;
        Option option = optionData.get(key);
        if (option != null){
            if (!option.arg().isEmpty()){
                value = getOptionValue(option, arg, it);
            }
            try {
                BiPredicate<Executable, String> call;
                if ((call = options.get(key)) != null){
                    if (call.test(target, value)) return;
                } else {
                    if (option.value().isEmpty()){
                        LOG.debug("{} : {}", option.shortName(), value);
                        if (target.extendOption(Character.toString(option.shortName()), value, it)) return;
                    } else {
                        LOG.debug("{} : {}", option.value(), value);
                        if (target.extendOption(option.value(), value, it)) return;
                    }
                }
            } catch (RuntimeException e){
                throw new RuntimeException("option '" + arg + "' : " + e.getMessage(), e);
            }
        }
        throw new IllegalArgumentException("unknown option : " + arg);
    }

    private int parseArgument(Executable target, String arg, int argumentIndex, ListIterator<String> it){
        BiPredicate<Executable, String> call;
        if (argumentIndex < arguments.size()){
            call = arguments.get(argumentIndex);
        } else {
            call = leftArgument;
        }
        try {
            if (call != null){
                if (call.test(target, arg)) return 0;
            } else {
                int next = it.nextIndex();
                if (target.extendArgument(argumentIndex, arg, it)){
                    int newNext = it.nextIndex();
                    return newNext - next;
                }
            }
        } catch (RuntimeException e){
            throw new RuntimeException("argument '" + arg + "' : " + e.getMessage(), e);
        }
        throw new IllegalArgumentException("unknown argument : " + arg);
    }

    /**
     * get the option key from the program argument.
     *
     * ###accept form
     *
     * * --key
     * * --key value
     * * --key=value
     * * -K
     * * -K value
     * * -Kvalue
     *
     * @param arg command argument
     * @return the key of the program argument, may null
     */
    private static String getOptionKey(String arg){
        if (arg.startsWith("--")){
            if (arg.contains("=")){
                return arg.substring(0, arg.indexOf("="));
            }
            return arg;
        } else if (arg.startsWith("-")){
            if (arg.length() == 1){
                throw new RuntimeException("lost option : '-'");
            }
            return arg.substring(0, 2);
        }
        throw new IllegalArgumentException("not a option : " + arg);
    }

    /**
     * get the option value from the program argument
     *
     * ###accept form
     *
     * * --key value
     * * --key=value
     * * -K value
     * * -Kvalue
     *
     * @param option option data, just use for getting information when error occur.
     * @param arg    command argument
     * @param it     left command arguments
     * @return the value of the program argument
     * @throws IllegalArgumentException {@code arg} is not a accept form.
     */
    private static String getOptionValue(Option option, String arg, ListIterator<String> it){
        try {
            if (arg.startsWith("--")){
                if (arg.contains("=")){
                    return arg.substring(arg.indexOf("=") + 1);
                }
                return it.next();
            } else if (arg.startsWith("-")){
                if (arg.length() > 2){
                    return arg.substring(2);
                } else {
                    return it.next();
                }
            }
        } catch (NoSuchElementException e){
            throw new IllegalArgumentException("option " + arg + " lost arguments : " + option.arg());
        }
        throw new IllegalArgumentException("not a option : " + arg);
    }
}
