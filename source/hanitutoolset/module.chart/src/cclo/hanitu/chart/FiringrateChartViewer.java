/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.chart;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cclo.hanitu.Base;
import cclo.hanitu.data.*;
import cclo.hanitu.gui.FXApplication;
import cclo.hanitu.io.TailHistoryStream;
import cclo.hanitu.io.TailStream;

import static cclo.hanitu.gui.PropertySession.setTextFieldGuideSetter;
import static cclo.hanitu.gui.PropertySession.setTextFieldGuider;

/**
 * @author antonio
 */
public class FiringrateChartViewer extends FXApplication implements TimeDataHandler, SpikeDataHandler{

    private final Logger LOG = LoggerFactory.getLogger(FiringrateChartViewer.class);

    private final TabPane content;
    private final Map<SingleSpikeFilter, FiringrateTab> charts = new HashMap<>();
    private final ScrollBar timeScroll = new ScrollBar();
    final NumberAxis xAxis = new NumberAxis();

    private FiringRateScalar scalar = FiringRateScalar.uniform();
    private FiringrateResult result;
    private TailHistoryStream<SpikeData> spikeStream;

    final SimpleIntegerProperty currentTime = new SimpleIntegerProperty(0);
    private final SimpleIntegerProperty lastSpikeTime = new SimpleIntegerProperty(0);
    private final SimpleIntegerProperty timeWindow = new SimpleIntegerProperty(1000);
    private final SimpleIntegerProperty timeStep = new SimpleIntegerProperty(100);
    private final SimpleIntegerProperty timeDuration = new SimpleIntegerProperty(5000);

    private TextField timeWindowTextField;
    private TextField timeStepTextField;
    private TextField timeDuringTextField;

    public FiringrateChartViewer(){
        Properties p = Base.loadProperties();
        //
        primaryStage.setTitle(p.getProperty("title"));
        primaryStage.setMinWidth(800);
        primaryStage.setMinHeight(600);
        //
        content = new TabPane();
        root.setMinWidth(800);
        root.setMinHeight(600);
        root.getChildren().addAll(initToolBar(),
                                  content,
                                  initTimeBar());
        //
        initTimeEvent();
        // init x axis
        initXAxis();
        //
        root.requestLayout();
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    private ToolBar initToolBar(){
        Properties p = Base.loadProperties();
        //
        Label twl = new Label(p.getProperty("label.timewindow"));
        Label tsl = new Label(p.getProperty("label.timestep"));
        Label tdl = new Label(p.getProperty("label.timeduring"));
        timeWindowTextField = new TextField(Integer.toString(timeWindow.get()));
        timeStepTextField = new TextField(Integer.toString(timeStep.get()));
        timeDuringTextField = new TextField(Integer.toString(timeDuration.get()));
        ToolBar tool = new ToolBar(twl, timeWindowTextField,
                                   tsl, timeStepTextField,
                                   tdl, timeDuringTextField);
        //
        timeWindowTextField.setPrefColumnCount(6);
        timeStepTextField.setPrefColumnCount(6);
        timeDuringTextField.setPrefColumnCount(6);
        //
        timeWindow.addListener((v, o, n) -> timeWindowTextField.setText(n.toString()));
        timeStep.addListener((v, o, n) -> timeStepTextField.setText(n.toString()));
        timeDuration.addListener((v, o, n) -> timeDuringTextField.setText(n.toString()));
        setTextFieldGuider(timeWindowTextField, Integer::parseInt, "Wrong Number Format");
        setTextFieldGuider(timeStepTextField, Integer::parseInt, "Wrong Number Format");
        setTextFieldGuider(timeDuringTextField, Integer::parseInt, "Wrong Number Format");
        setTextFieldGuideSetter(timeWindowTextField, Integer::parseInt, "Wrong Number Format", timeWindow);
        setTextFieldGuideSetter(timeStepTextField, Integer::parseInt, "Wrong Number Format", timeStep);
        setTextFieldGuideSetter(timeDuringTextField, Integer::parseInt, "Wrong Number Format", timeDuration);
        //
        return tool;
    }

    private void initTimeEvent(){
        ChangeListener<Number> listener = (v, o, n) -> resetFiringrateResultAndPlot();
        timeWindow.addListener(listener);
        timeStep.addListener(listener);
    }

    private void initXAxis(){
        Properties p = Base.loadProperties();
        xAxis.setLabel(p.getProperty("axis.x"));
        xAxis.setForceZeroInRange(false);
        xAxis.setAutoRanging(false);
        xAxis.lowerBoundProperty().bind(timeScroll.valueProperty().divide(1000.0));
        xAxis.upperBoundProperty().bind(timeScroll.valueProperty().add(timeDuration).divide(1000.0));
        xAxis.tickUnitProperty().bind(timeDuration.divide(1000.0 * 25));
    }

    private Node initTimeBar(){
        Properties p = Base.loadProperties();
        Button head = new Button("0 ms");
        Button tail = new Button("0 ms");
        //
        head.setTooltip(new Tooltip(p.getProperty("label.head")));
        tail.setTooltip(new Tooltip(p.getProperty("label.tail")));
        //
        timeScroll.setOrientation(Orientation.HORIZONTAL);
        timeScroll.setMin(0);
        lastSpikeTime.addListener((v, o, n) -> {
            double time = n.doubleValue() - timeDuration.get();
            if (time > timeScroll.getMax()){
                timeScroll.setMax(time);
            }
            tail.setText(Integer.toString(n.intValue()) + " ms");
        });
        timeScroll.setMaxHeight(100);
        //
        head.setOnAction(e -> timeScroll.setValue(0));
        tail.setOnAction(e -> fetchFiringrateResult(Integer.MAX_VALUE));
        //
        HBox box = new HBox(head, timeScroll, tail);
        box.setFillHeight(true);
        HBox.setHgrow(timeScroll, Priority.ALWAYS);
        return box;
    }

    @Override
    public void close(){
        closeSpikeStream();
        super.close();
    }

    public void select(SingleSpikeFilter filter){
        for (FiringrateTab tab : charts.values()){
            if (tab.isAccept(filter)){
                Platform.runLater(() -> content.getSelectionModel().select(tab));
                break;
            }
        }
    }

    private void forEachTab(Consumer<FiringrateTab> consumer){
        charts.values().forEach(consumer);
    }

    @SuppressWarnings("WeakerAccess")
    public void resetFiringrateResult(){
        LOG.debug("reset Firingrate result");
        spikeStream.head();
        result = new FiringrateResult();
        result.setSpikeStream(spikeStream);
        result.setTimeWindow(timeWindow.get());
        result.setTimeStep(timeStep.get());
        result.setScalar(scalar);
        lastSpikeTime.set(0);
        timeScroll.setValue(0);
        LOG.debug("time window : {}", timeWindow.get());
        LOG.debug("time step : {}", timeStep.get());
        LOG.debug("time duration : {}", timeDuration.get());
        forEachTab(FiringrateTab::clear);
    }

    @SuppressWarnings("WeakerAccess")
    public void resetFiringrateResultAndPlot(){
        int time = lastSpikeTime.get();
        resetFiringrateResult();
        fetchFiringrateResult(time);
    }

    public void fetchAllFiringrateResult(){
        FXApplication.doInFxThreadAndWait(() -> fetchFiringrateResult(Integer.MAX_VALUE));
    }

    private boolean fetchFiringrateResult(int untilTime){
        if (result == null) return false;
        int startTime = lastSpikeTime.get();
        if (startTime > untilTime) return true;
        FiringrateResult result = this.result;
        //
        LinkedList<FiringrateResult.SnapshotResult> queue = new LinkedList<>();
        FiringrateResult.SnapshotResult snapshot = null;
        while (result.next()){
            snapshot = result.snapshot();
            createTabs(snapshot.getFilters());
            queue.add(snapshot);
            if (snapshot.getTime() > untilTime) break;
        }
        if (!queue.isEmpty()){
            assert snapshot != null;
            Platform.runLater(() -> forEachTab(t -> t.updateResult(queue)));
            int lastTime = snapshot.getTime();
            LOG.trace("fetch result to time expected : {} ms", untilTime);
            LOG.trace("fetch result to time actually : {} -> {} ms", startTime, lastTime);
            doInFxThreadAndWait(() -> lastSpikeTime.set(lastTime));
            return snapshot.getTime() >= untilTime;
        }
        return false;
    }

    private void createTabs(Set<SingleSpikeFilter> set){
        Set<SingleSpikeFilter> filters = set.stream()
          .filter(f -> charts.keySet().stream().noneMatch(ff -> ff.isSameWorm(f)))
          .map(SingleSpikeFilter::toWormIdentify)
          .collect(Collectors.toSet());
        if (!filters.isEmpty()){
            for (SingleSpikeFilter filter : filters){
                LOG.trace("create tab for {}", filter);
                charts.put(filter, null);
            }
            Platform.runLater(() -> {
                for (SingleSpikeFilter filter : filters){
                    FiringrateTab tab = new FiringrateTab(this, filter);
                    content.getTabs().add(tab);
                    charts.put(filter, tab);
                }
            });
        }
    }

    private void resetTabs(){
        LOG.debug("reset tabs");
        FXApplication.doInFxThreadAndWait(() -> {
            charts.clear();
            content.getTabs().removeAll();
        });
    }

    @Override
    public ReadOnlyIntegerProperty getTimeProperty(){
        return currentTime;
    }

    @Override
    public boolean updateUntilTime(int time){
        boolean ret = fetchFiringrateResult(time);
        currentTime.set(time);
        timeScroll.setValue(timeScroll.getMax());
        return ret;
    }

    @Override
    public void setSpikeFile(String file){
        LOG.debug("set spike file {}", file);
        SpikeDataHandler.super.setSpikeFile(file);
    }

    @Override
    public void setSpikeFile(Path file){
        LOG.debug("set spike file {}", file);
        SpikeDataHandler.super.setSpikeFile(file);
    }

    @Override
    public void setSpikeStream(TailStream<SpikeData> source){
        closeSpikeStream();
        resetTabs();
        //
        LOG.debug("set spike stream");
        spikeStream = new TailHistoryStream<>(source);
        Platform.runLater(this::resetFiringrateResultAndPlot);
    }

    @Override
    public void closeSpikeStream(){
        if (spikeStream != null){
            LOG.debug("close spike stream");
            //noinspection EmptyCatchBlock
            try {
                spikeStream.close();
            } catch (IOException e){
            }
            spikeStream = null;
        }
        result = null;
    }

    @SuppressWarnings("unused")
    public FiringrateFunction getFunction(){
        FiringrateFunction function = new FiringrateFunction();
        function.setTimeWindow(timeWindow.get());
        function.setTimeStep(timeStep.get());
        function.setScalar(scalar);
        return function;
    }

    @SuppressWarnings("unused")
    public void setFunction(FiringrateFunction function){
        timeWindow.set(function.getTimeWindow());
        timeStep.set(function.getTimeStep());
        scalar = function.getScalar();
    }

    @Override
    protected boolean processKeyboardEvent(KeyEvent e){
        switch (e.getCode()){
        case F5:
            break;
        default:
            return false;
        }
        return true;
    }
}
