/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.chart;

import java.util.Objects;

import javafx.beans.InvalidationListener;
import javafx.beans.NamedArg;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.shape.Line;

/**
 * ref : http://stackoverflow.com/questions/28952133/how-to-add-two-vertical-lines-with-javafx-linechart
 * @author antonio
 */
@SuppressWarnings("unused")
class LineMarkerChart<X extends Number, Y extends Number> extends LineChart<X, Y>{

    private final ObservableList<Data<X, Y>> hMarkers = FXCollections.observableArrayList();
    private final ObservableList<Data<X, Y>> vMarkers = FXCollections.observableArrayList();

    public LineMarkerChart(@NamedArg("xAxis") Axis<X> xAxis,
                           @NamedArg("yAxis") Axis<Y> yAxis){
        super(xAxis, yAxis);
        hMarkers.addListener((InvalidationListener)c -> layoutPlotChildren());
        vMarkers.addListener((InvalidationListener)c -> layoutPlotChildren());
    }

    public Line addHorizontalLineMarker(Data<X, Y> marker){
        Objects.requireNonNull(marker);
        if (hMarkers.contains(marker)){
            assert marker.getNode() != null;
            return (Line)marker.getNode();
        }
        Line line = new Line();
        marker.setNode(line);
        getPlotChildren().add(line);
        hMarkers.add(marker);
        return line;
    }

    public Line addVerticalLineMarker(Data<X, Y> marker){
        Objects.requireNonNull(marker);
        if (vMarkers.contains(marker)){
            assert marker.getNode() != null;
            return (Line)marker.getNode();
        }
        Line line = new Line();
        marker.setNode(line);
        getPlotChildren().add(line);
        vMarkers.add(marker);
        return line;
    }

    public void removeHorizontalLineMarker(Data<X, Y> marker){
        Objects.requireNonNull(marker);
        if (marker.getNode() != null){
            getPlotChildren().remove(marker.getNode());
            marker.setNode(null);
        }
        hMarkers.remove(marker);
    }

    public void removeVerticalLineMarker(Data<X, Y> marker){
        Objects.requireNonNull(marker);
        if (marker.getNode() != null){
            getPlotChildren().remove(marker.getNode());
            marker.setNode(null);
        }
        vMarkers.remove(marker);
    }

    public Node getLegendNode(){
        return getLegend();
    }

    @Override
    protected void layoutPlotChildren(){
        super.layoutPlotChildren();
        for (Data<X, Y> marker : hMarkers){
            Line line = (Line)marker.getNode();
            line.setStartX(0);
            line.setEndX(getBoundsInLocal().getWidth());
            double y = getYAxis().getDisplayPosition(marker.getYValue());
            line.setStartY(y);
            line.setEndY(y);
            line.toFront();
        }
        for (Data<X, Y> marker : vMarkers){
            Line line = (Line)marker.getNode();
            double x = getXAxis().getDisplayPosition(marker.getXValue());
            line.setStartX(x);
            line.setEndX(x);
            line.setStartY(0);
            line.setEndY(getBoundsInLocal().getHeight());
            line.toFront();
        }
    }
}
