/*
 * Copyright (C) 2016 antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cclo.hanitu.chart;

import java.util.*;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Tab;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

import cclo.hanitu.Base;
import cclo.hanitu.data.FiringRateData;
import cclo.hanitu.data.FiringrateResult;
import cclo.hanitu.data.SingleSpikeFilter;

/**
 * @author antonio
 */
class FiringrateTab extends Tab{

    //    private final FiringrateChartViewer viewer;
    private final SingleSpikeFilter identify;
    private final LineMarkerChart<Number, Number> chart;

    private final Map<SingleSpikeFilter, XYChart.Series<Number, Number>> data = new HashMap<>();
    private final Map<String, SingleSpikeFilter> filterMap = new HashMap<>();
    private SingleSpikeFilter lastMouseActiveData;
    private Tooltip lastMouseActiveToolTip;

    public FiringrateTab(FiringrateChartViewer viewer, SingleSpikeFilter identify){
//        this.viewer = viewer;
        this.identify = Objects.requireNonNull(identify);
        VBox root = new VBox();
        root.setFillWidth(true);
        setText(String.format("%s[%s]", identify.user, identify.worm));
        setContent(root);
        setClosable(false);
        // x axis
        NumberAxis xAxis = new NumberAxis(viewer.xAxis.getLabel(), 0, 0, 1);
        xAxis.lowerBoundProperty().bind(viewer.xAxis.lowerBoundProperty());
        xAxis.upperBoundProperty().bind(viewer.xAxis.upperBoundProperty());
        xAxis.tickUnitProperty().bind(viewer.xAxis.tickUnitProperty());
        // y axis
        NumberAxis yAxis = new NumberAxis(Base.loadProperties(viewer.getClass()).getProperty("axis.y"), 0, 100, 20);
        yAxis.setForceZeroInRange(true);
        yAxis.setAutoRanging(true);
        // chart
        chart = new LineMarkerChart<>(xAxis, yAxis);
        chart.setMinHeight(500);
        chart.setStyle("-fx-create-symbols:false");
        chart.setOnMouseClicked(e -> deHighlight());
        chart.getLegendNode().setOnMouseClicked(e -> {
            if (e.getButton() == MouseButton.PRIMARY && (e.getTarget() instanceof Text)){
                Text text = (Text)e.getTarget();
                SingleSpikeFilter filter = filterMap.get(text.getText());
                if (filter != null) highlight(filter);
                e.consume();
            }
        });
        root.getChildren().add(chart);
        // time
        XYChart.Data<Number, Number> currentTimeLine = new XYChart.Data<>(viewer.currentTime.get() / 1000, 0);
        viewer.currentTime.addListener((v, o, n) -> {
            currentTimeLine.setXValue(n.doubleValue() / 1000);
            chart.layoutPlotChildren();
        });
        Line vertical = chart.addVerticalLineMarker(currentTimeLine);
        vertical.setStroke(Color.RED);
    }

    public boolean isAccept(SingleSpikeFilter filter){
        return identify.isSameWorm(filter);
    }

    @SuppressWarnings("unused")
    public Set<SingleSpikeFilter> getFilters(){
        return data.keySet();
    }

    @SuppressWarnings("unused")
    public void updateResult(FiringrateResult result){
        result.getFilters().stream()
          .filter(identify::isSameWorm)
          .forEach(filter -> {
              XYChart.Series<Number, Number> series = data.computeIfAbsent(filter, this::newSeries);
              FiringRateData data = result.getFiringrate(filter);
              series.getData().add(new XYChart.Data<>(data.time / 1000.0, data.rate));
          });
        chart.layoutPlotChildren();
    }

    public void updateResult(List<FiringrateResult.SnapshotResult> result){
        Map<SingleSpikeFilter, List<XYChart.Data<Number, Number>>> cache = new HashMap<>();
        for (FiringrateResult.SnapshotResult r : result){
            r.getFilters().stream()
              .filter(identify::isSameWorm)
              .forEach(filter -> {
                  FiringRateData data = r.getFiringrate(filter);
                  List<XYChart.Data<Number, Number>> list = cache.computeIfAbsent(filter, f -> new LinkedList<>());
                  list.add(new XYChart.Data<>(data.time / 1000.0, data.rate));
              });
        }
        cache.forEach((f, ls) -> data.computeIfAbsent(f, this::newSeries).getData().addAll(ls));
        chart.layoutPlotChildren();
    }

    private XYChart.Series<Number, Number> newSeries(SingleSpikeFilter filter){
        XYChart.Series<Number, Number> series = new XYChart.Series<>();
        String name = getSeriesName(filter);
        series.setName(name);
        filterMap.put(name, filter);
        Platform.runLater(() -> {
            chart.getData().add(series);
            Node node = series.getNode();
            node.setStyle("-fx-stroke-width: 2px;");
            Tooltip tooltip = new Tooltip(name);
            //
            node.setOnMouseEntered(e -> {
                highlight(filter);
                highlight(node, tooltip, e.getScreenX() + 20, e.getScreenY());
                e.consume();
            });
        });
        data.put(filter, series);
        return series;
    }

    private String getSeriesName(SingleSpikeFilter filter){
        if (filter.type == 's') return String.format("S[%s]", filter.neuron);
        if (filter.type == 'm') return String.format("M[%s]", filter.neuron);
        if (filter.type == 'd') return "NPY";
        return filter.neuron;
    }

    private void highlight(SingleSpikeFilter filter){
        Objects.requireNonNull(filter);
        XYChart.Series<Number, Number> series = data.get(filter);
        if (series != null){
            deHighlight();
            Node node = series.getNode();
            node.setStyle("-fx-stroke-width: 5px;");
            node.toFront();
            lastMouseActiveData = filter;
        }
    }

    private void highlight(Node parent, Tooltip tip, double x, double y){
        if (lastMouseActiveToolTip != null && lastMouseActiveToolTip != tip){
            lastMouseActiveToolTip.hide();
            lastMouseActiveToolTip = null;
        }
        if (tip != null){
            tip.show(parent, x, y);
            lastMouseActiveToolTip = tip;
        }
    }

    private void deHighlight(){
        if (lastMouseActiveData != null){
            XYChart.Series<Number, Number> old = data.get(lastMouseActiveData);
            lastMouseActiveData = null;
            if (old != null){
                old.getNode().setStyle("-fx-stroke-width: 2px;");
            }
        }
        if (lastMouseActiveToolTip != null){
            lastMouseActiveToolTip.hide();
            lastMouseActiveToolTip = null;
        }
    }

    @SuppressWarnings("unused")
    public boolean contains(SingleSpikeFilter single){
        return data.containsKey(single);
    }

    public void clear(){
        data.values().forEach(s -> s.getData().clear());
        chart.layoutPlotChildren();
    }
}
