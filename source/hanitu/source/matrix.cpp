﻿/*
 * Copyright (C) 2015 Fang-Kuei Hsieh
 *
 * This file is part of Hanitu.
 *
 * Hanitu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hanitu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "matrix.h"
#include <iostream>
#include <fstream>
#include <sstream>

#define MuscleConflict 1//ms

using namespace std;

#define DEFAULT_DIR 4 //worm direction count
#define DEFAULT_TRANS 4//transformA, B number

worm_base::worm_base()//constructor[goal:initiallized]
{
	worm_ID[0]="0";
	worm_ID[1]="0";
	worm_size=0;
	worm_location[0]=0;
	worm_location[1]=0;
	worm_life=100.0;
	liveornot=true;
	move = 0;
	get_food = 0;
	get_toxi = 0;
	brick = 0;
	touch_worm = 0;
	step=0;
	time_decay=2;// HP/sec
	step_decay=1;// HP/step
}

worm_base::~worm_base()//destructor
{

}

event_buffer::event_buffer()
{
	time=0;
	worm_ID[0]="0";
	worm_ID[1]="0";
}

event_buffer::~event_buffer()
{

}

envi::envi()
{
	Boundary=900;//0.1mm
	timer=0;
	g_f_type[0]=0;
	g_f_type[1]=1;
}

envi::~envi()
{

}

food::food()
{
	food_ID[0]=0;
	food_ID[1]=0;
	food_location[0]=0;
	food_location[1]=0;
	count=100;
	concentration=10e-3f; //Par in pre version Hanitu
	D=1.86*10e-5f;
	T0=7.2*1e4f;
	d=0.264; //cm
	nutrient=10.0;
}

food::~food()
{

}

worm_sense::worm_sense()
{
	worm_ID[0]="0";
	worm_ID[1]="0";
}

worm_sense::~worm_sense()
{

}

read_file_world::read_file_world()
{

}

read_file_world::~read_file_world()
{

}

envifunction::envifunction()
{
	fixed = 0;
	overlap_ign = 0;
	spk_name = SPIKE;
	loc_name = LOCATIONS;
}

envifunction::~envifunction()
{

}


int envifunction::readfile(const string& wfname, vector<worm_sense>& Ws, vector<neuron>& Nn, const float& HP)//(1)read teacher setting file : world_config.txt and distribute data to correlated variable
{
	ifstream readin;  
	string str;
	string delimiters;
	vector<string> Setinf;
	int a=0,b=0;
	read_file_world tempw;//temp

	delimiters = "=\t\r\n";

	//read file start
	readin.open(wfname.c_str());

	if(!readin) {//read file success or not
		cout<<"fail open world_config file\n";
		return -1; //read file error: -1
	}

	while(!readin.eof())	{ 
		str+=readin.get(); 
	}
	readin.close();
	//read file end. all data stored in "str(type : string)"

	//-----clip the string-str to single part of data-----stored to "Setinf"(vector<string>)
	b= str.find_first_of(delimiters, a);//search char

	//select string to store to vector<string> Setinf
	while ( (b != string::npos) || (a != string::npos) )	{
		Setinf.push_back( str.substr(a, b - a) );
		a = str.find_first_not_of(delimiters, b);
		b = str.find_first_of(delimiters, a);
	}
	//select string end

	unsigned int index=0;
	for(;index<Setinf.size();index++) {
		transform(Setinf[index].begin(), Setinf[index].end(), Setinf[index].begin(), ::tolower);
		if(Setinf[index] == "filename" || Setinf[index] == "userid" || Setinf[index] == "wormid")
			index++;
	}

	//Transform A,B vector initialize
	for(unsigned int i=0;i<DEFAULT_TRANS;i++) {
		tempw.TransformA.insert(tempw.TransformA.end(), 0);
		tempw.TransformB.insert(tempw.TransformB.end(), 0);
	}

	// read world parameter----------------------------------------------------------------------

	for(index=0;index<Setinf.size();index++)	{
		if(Setinf[index]=="userid")
			tempw.userID.insert(tempw.userID.end(), Setinf[++index].c_str());
		else if(Setinf[index]=="wormid")
			tempw.WormID.insert(tempw.WormID.end(), Setinf[++index].c_str());
		else if(Setinf[index]=="initialx")
			tempw.InitialX.insert(tempw.InitialX.end(), atoi(Setinf[++index].c_str()));
		else if(Setinf[index]=="initialy")
			tempw.InitialY.insert(tempw.InitialY.end(), atoi(Setinf[++index].c_str()));
		else if(Setinf[index]=="wormsize")
			tempw.Wormsize.insert(tempw.Wormsize.end(), stof(Setinf[++index].c_str()));
		else if(Setinf[index]=="timedecay")
			tempw.Time_decay.insert(tempw.Time_decay.end(), stof(Setinf[++index].c_str()));
		else if(Setinf[index]=="stepdecay")
			tempw.Step_decay.insert(tempw.Step_decay.end(), stof(Setinf[++index].c_str()));
		else if(Setinf[index]=="filename")
			tempw.Filename.insert(tempw.Filename.end(), Setinf[++index]);
		else if(Setinf[index]=="dhp")
			tempw.Nutrient.insert(tempw.Nutrient.end(), stof(Setinf[++index].c_str()));
		else if(Setinf[index]=="gainnpy")
			tempw.gainNPY.insert(tempw.gainNPY.end(), stof(Setinf[++index].c_str()));
		else if(Setinf[index]=="baselinenpy")
			tempw.baselineNPY.insert(tempw.baselineNPY.end(), stof(Setinf[++index].c_str()));
		else if(Setinf[index]=="gainff")
			tempw.TransformA[0] = stof(Setinf[++index].c_str());
		else if(Setinf[index]=="baselineff")
			tempw.TransformB[0] = stof(Setinf[++index].c_str());
		else if(Setinf[index]=="gainft")
			tempw.TransformA[1] = stof(Setinf[++index].c_str());
		else if(Setinf[index]=="baselineft")
			tempw.TransformB[1] = stof(Setinf[++index].c_str());
		else if(Setinf[index]=="gaintt")
			tempw.TransformA[2] = stof(Setinf[++index].c_str());
		else if(Setinf[index]=="baselinett")
			tempw.TransformB[2] = stof(Setinf[++index].c_str());
		else if(Setinf[index]=="gaintf")
			tempw.TransformA[3] = stof(Setinf[++index].c_str());
		else if(Setinf[index]=="baselinetf")
			tempw.TransformB[3] = stof(Setinf[++index].c_str());
		else if(Setinf[index]=="boundary")
			tempw.Boundary.insert(tempw.Boundary.end(), stof(Setinf[++index].c_str()));
		else if(Setinf[index]=="type")
			tempw.Type.insert(tempw.Type.end(), atoi(Setinf[++index].c_str()));
		else if(Setinf[index]=="depth")
			tempw.Depth.insert(tempw.Depth.end(), stof(Setinf[++index].c_str()));
		else if(Setinf[index]=="countmode")
			tempw.CountMode.insert(tempw.CountMode.end(), atoi(Setinf[++index].c_str()));
		else if(Setinf[index]=="fixed")
			fixed = atoi(Setinf[++index].c_str());

		else if(Setinf[index]=="foodlocation")	{
			do{
				index++;
				if(Setinf[index]=="fid")
					tempw.FID.insert(tempw.FID.end(), atoi(Setinf[++index].c_str()));
				else if(Setinf[index]=="x")
					tempw.F_X.insert(tempw.F_X.end(), atoi(Setinf[++index].c_str()));
				else if(Setinf[index]=="y")
					tempw.F_Y.insert(tempw.F_Y.end(), atoi(Setinf[++index].c_str()));
				else if(Setinf[index]=="count")
					tempw.F_Count.insert(tempw.F_Count.end(), atoi(Setinf[++index].c_str()));
				else if(Setinf[index]=="diffusioncoef")
					tempw.F_Diffuse.insert(tempw.F_Diffuse.end(), stof(Setinf[++index].c_str()));
				else if(Setinf[index]=="concentration")
					tempw.F_Concentration.insert(tempw.F_Concentration.end(), stof(Setinf[++index].c_str()));
				else if(Setinf[index]=="delaytime")
					tempw.F_Delay_time.insert(tempw.F_Delay_time.end(), stof(Setinf[++index].c_str()));
			}while(Setinf[index]!="endfoodlocation");

		}
		else if(Setinf[index]=="toxicantlocation")	{
			do{
				index++;
				if(Setinf[index]=="tid")
					tempw.CID.insert(tempw.CID.end(), atoi(Setinf[++index].c_str()));
				else if(Setinf[index]=="x")
					tempw.C_X.insert(tempw.C_X.end(), atoi(Setinf[++index].c_str()));
				else if(Setinf[index]=="y")
					tempw.C_Y.insert(tempw.C_Y.end(), atoi(Setinf[++index].c_str()));
				else if(Setinf[index]=="count")
					tempw.C_Count.insert(tempw.C_Count.end(), atoi(Setinf[++index].c_str()));
				else if(Setinf[index]=="diffusioncoef")
					tempw.C_Diffuse.insert(tempw.C_Diffuse.end(), stof(Setinf[++index].c_str()));
				else if(Setinf[index]=="concentration")
					tempw.C_Concentration.insert(tempw.C_Concentration.end(), stof(Setinf[++index].c_str()));
				else if(Setinf[index]=="delaytime")
					tempw.C_Delay_time.insert(tempw.C_Delay_time.end(), stof(Setinf[++index].c_str()));
			}while(Setinf[index]!="endtoxicantlocation");
		}
	}//for-loop end

	//check whether the data is missing or not
	int flag = 0;
	int cc = tempw.userID.size();
	if(cc == tempw.WormID.size())
		if(cc == tempw.InitialX.size())
			if(cc == tempw.InitialY.size())
				if(cc == tempw.Wormsize.size())
					if(cc == tempw.Time_decay.size())
						if(cc == tempw.Step_decay.size())
							if(cc == tempw.Filename.size())
								flag = 1;
	if(flag != 1)
	{
		cout << "Parameter(s) of WormInf part is(are) missing ! \n";
		exit(EXIT_FAILURE);
	}
	flag = 0;

	cc = tempw.Nutrient.size();
	if(cc == tempw.Nutrient.size())
		//if(cc == tempw.TransformA.size())
			//if(cc == tempw.TransformB.size())
				if(cc == tempw.Boundary.size())
					if(cc == tempw.Type.size())
						if(cc == tempw.Depth.size())
							if(cc == tempw.CountMode.size())
								flag = 1;
	if(flag != 1)
	{
		cout << "Parameter(s) of World part is(are) missing ! \n";
		exit(EXIT_FAILURE);
	}
	flag = 0;

	cc = tempw.FID.size();
	if(cc == tempw.FID.size())
		if(cc == tempw.F_X.size())
			if(cc == tempw.F_Y.size())
				if(cc == tempw.F_Count.size())
					if(cc == tempw.F_Diffuse.size())
						if(cc == tempw.F_Concentration.size())
							if(cc == tempw.F_Delay_time.size())
								flag = 1;
	if(flag != 1)
	{
		cout << "Parameter(s) of Food part is(are) missing ! \n";
		exit(EXIT_FAILURE);
	}
	flag = 0;

	cc = tempw.CID.size();
	if(cc == tempw.CID.size())
		if(cc == tempw.C_X.size())
			if(cc == tempw.C_Y.size())
				if(cc == tempw.C_Count.size())
					if(cc == tempw.C_Diffuse.size())
						if(cc == tempw.C_Concentration.size())
							if(cc == tempw.C_Delay_time.size())
								flag = 1;
	if(flag != 1)
	{
		cout << "Parameter(s) of Toxicant part is(are) missing ! \n";
		exit(EXIT_FAILURE);
	}

	//check end

	// read world parameter  end------------------------------------------------------------------------

	//Distribution every parameters--------------------------------------------
	//initailize
	Wb.clear();//class : worm_base
	En.clear();//class : envi
	foods.clear();//class : food
	file.clear();//class : string

	worm_base tWb;//t means temp
	worm_sense tWs;
	neuron tNn;
	tNn.move=0;
	for(unsigned int i=0; i<tempw.userID.size(); i++)
	{
		tWb.worm_ID[0] = tempw.userID[i];
		tWb.worm_ID[1] = tempw.WormID[i];

		tWs.worm_ID[0] = tempw.userID[i];
		tWs.worm_ID[1] = tempw.WormID[i];

		tNn.worm_ID[0] = tempw.userID[i];
		tNn.worm_ID[1] = tempw.WormID[i];

		tWb.worm_size = tempw.Wormsize[i];
		tWb.worm_location[0] = tempw.InitialX[i];
		tWb.worm_location[1] = tempw.InitialY[i];
		tWb.time_decay = tempw.Time_decay[i];
		tWb.step_decay = tempw.Step_decay[i];

		//initiallized HP setting
	
		if(HP != -1) {
			tWb.worm_life = HP;
		}
		Wb.insert(Wb.end(), tWb);
		Ws.insert(Ws.end(), tWs);
		//Nn.insert(Nn.end(), tNn);
	}

	envi tEn;
	for(unsigned int i=0; i<tempw.Nutrient.size(); i++)
	{
		tEn.Boundary = tempw.Boundary[i];
		tEn.g_f_type[0] = tempw.Type[i];
		tEn.g_f_type[1] = tempw.CountMode[i];
		tEn.gainNPY = tempw.gainNPY[i];
		tEn.baselineNPY = tempw.baselineNPY[i];
	}

	for(unsigned int i=0; i<tempw.TransformA.size(); i++) {
		tEn.TransformA.insert(tEn.TransformA.end(), tempw.TransformA[i]);
		tEn.TransformB.insert(tEn.TransformB.end(), tempw.TransformB[i]);
	}
	En.insert(En.end(), tEn);

	food tf;
	for(unsigned int i = 0; i<tempw.FID.size(); i++)
	{
		tf.food_ID[0] = 1; //1 : food
		tf.food_ID[1] = tempw.FID[i];
		tf.food_location[0] = tempw.F_X[i];
		tf.food_location[1] = tempw.F_Y[i];
		tf.count = tempw.F_Count[i];
		tf.concentration = tempw.F_Concentration[i];
		tf.D = tempw.F_Diffuse[i];
		tf.T0 = tempw.F_Delay_time[i];
		tf.d = tempw.Depth[0];//now just one world
		tf.nutrient = tempw.Nutrient[0];//now just one world

		foods.insert(foods.end(), tf);
	}
	for(unsigned int i = 0; i<tempw.CID.size(); i++)
	{
		tf.food_ID[0] = 0; //0 : chem
		tf.food_ID[1] = tempw.CID[i];
		tf.food_location[0] = tempw.C_X[i];
		tf.food_location[1] = tempw.C_Y[i];
		tf.count = tempw.C_Count[i];
		tf.concentration = tempw.C_Concentration[i];
		tf.D = tempw.C_Diffuse[i];
		tf.T0 = tempw.C_Delay_time[i];
		tf.d = tempw.Depth[0];//now just one world
		tf.nutrient = tempw.Nutrient[0];//now just one world

		foods.insert(foods.end(), tf);
	}
	file = tempw.Filename;

	//initialize Motor neuron spike counter
	int totals = DEFAULT_DIR * Wb.size();
	Wm.resize(totals);

	//cout << "total motor neurons:" << totals << endl;
	//cout << "Wm:" << endl;
	
	int dmi = 0;
	for(int wi=0; wi<Wb.size(); wi++)
	{
		for(int dd=0; dd<DEFAULT_DIR; dd++)		
		{
			Wm[dmi].worm_ID[0] = Wb[wi].worm_ID[0];
			Wm[dmi].worm_ID[1] = Wb[wi].worm_ID[1];
			Wm[dmi].move = 0; //as counter
			dmi++;
			//cout << Wm[dmi].worm_ID[0] << "\t" << Wm[dmi].worm_ID[1] << "\t" << Wm[dmi].move << endl;
		}
	}
	//cout << endl;
	return 0;
}

bool envifunction::result()
{
	int count=0;
	float R2=0.0;

	if(En[0].g_f_type[0]==1)//g_f_type[0]=1(one of worms get food ) 
	{
		for(unsigned int i=0;i<Wb.size();i++)
		{
			for(unsigned int ii=0;ii<foods.size();ii++)
			{
				R2=((foods[ii].food_location[0]-Wb[i].worm_location[0])*(foods[ii].food_location[0]-Wb[i].worm_location[0])+(foods[ii].food_location[1]-Wb[i].worm_location[1])*(foods[ii].food_location[1]-Wb[i].worm_location[1]))*1e-4f; //(x^2 + y^2)*1e-3f
				R2=sqrt(R2);//distance of worm and food
				if(R2<Wb[i].worm_size*0.01 || R2==Wb[i].worm_size*0.01  )//cm*0.01 due to the unit is 0.1mm
				{ 
					R2=0;
					return false;
				}
				R2=0;
			}
		}
	}//if(En[0].g_f_type[0]==1)  end

	for(unsigned int i=0;i<Wb.size();i++)//check worms' life(HP) //if there is no worm alive, the game cannot go on
	{
		if(Wb[i].worm_life<0 || Wb[i].worm_life ==0)
			count++;
	}
	if(count==Wb.size()) //all worms are dead
	{ 
		R2=0;
		return false;
	}

	return true;


}

void envifunction::ConOutput(vector<worm_sense>& Ws)//send WormBody.Odor(order in Transfer.h)(vector<Concentration> ) here.vector<Concentration> defined in Communication.
{
	float Rx=0.0,Ry=0.0,N=0.0,C1=0.0,C2=0.0,R2=0.0;//C1=food C2=Chme
	float Size=0.0;
	int point=0;

	/*cerr<<"food+toxi number: "<<foods.size()<<endl;
	for(unsigned int f=0; f<foods.size(); f++) {
		cerr<<"ID"<<foods[f].food_ID[0]<<endl;
	}*/

	for(unsigned int i=0; i<Ws.size(); i++)//Ws.size() can show the worms' number
	{
		//The worm just has four directions now. So we initialize Ws to four elements that we can manipulate them as arrays later. 
		//(Use vector as array need to initialize before using it. Add each one element into vetor doesn't need initialize.)
		Ws[i].food_Con.resize(DEFAULT_DIR);
		Ws[i].food_firingrate.resize(DEFAULT_DIR);
		Ws[i].chem_Con.resize(DEFAULT_DIR);
		Ws[i].chem_firingrate.resize(DEFAULT_DIR);
	}

	for(unsigned int p=0;p<Ws.size();p++)
	{
		for(unsigned int pp=0;pp<Wb.size();pp++)//identification of worms(vector<WormPar>)
		{ 
			if(Wb[pp].worm_ID[0]==Ws[p].worm_ID[0] && Wb[pp].worm_ID[1]==Ws[p].worm_ID[1]  )//check whether the worm is same or not
			{  point=pp; }
		}
		Size=Wb[point].worm_size;
		for(int i=0;i<DEFAULT_DIR;i++)//F=0,B=1,L=2,R=3 //calculate concentration of four directions
		{
			Rx=0.0;
			Ry=0.0;
			//detected point location count-----------------------------------------------
			if(i>1) //L=2,R=3  
			{ 	
				Rx=0.01*(Wb[point].worm_location[0]+(2*i-5)*Size);//cm
				Ry=0.01*Wb[point].worm_location[1]; //cm
			}
			if(i<2)//F=0,B=1
			{
				Rx=0.01*Wb[point].worm_location[0];//cm
				Ry=0.01*(Wb[point].worm_location[1]+(1-2*i)*Size);//cm 
			}
			//detected point location count-----------------------------------------------end
			for(unsigned int ppp=0;ppp<foods.size();ppp++)
			{
				if(foods[ppp].food_ID[0]==1)//food
				{
					N=foods[ppp].count*foods[ppp].concentration *1e-3f;//N : odor molecules number
					float N2 = 12.56*foods[ppp].D*foods[ppp].d;
					if(N2!=0)
					{
						N = N/N2;
					}
					//R2 is the distance between worm and food
					R2=(0.01*foods[ppp].food_location[0]-Rx)*(0.01*foods[ppp].food_location[0]-Rx)+(0.01*foods[ppp].food_location[1]-Ry)*(0.01*foods[ppp].food_location[1]-Ry);
					float R3 = 4*foods[ppp].D;
					if(R3!=0)
					{
						R2=R2/R3;
					}
					C1+=N*exp((-1)*R2/(foods[ppp].T0+En[0].timer*1e-3f))/(foods[ppp].T0+En[0].timer*1e-3f);
					N=0;
					R2=0;
				}
				else if(foods[ppp].food_ID[0]==0)//Chem
				{
					N=foods[ppp].count*foods[ppp].concentration *1e-3f;//N : odor molecules number
					float N2 = 12.56*foods[ppp].D*foods[ppp].d;
					if(N2!=0)
					{
						N = N/N2;
					}
					//R2 is the distance between worm and food
					R2=(0.01*foods[ppp].food_location[0]-Rx)*(0.01*foods[ppp].food_location[0]-Rx)+(0.01*foods[ppp].food_location[1]-Ry)*(0.01*foods[ppp].food_location[1]-Ry);
					float R3 = 4*foods[ppp].D;
					if(R3!=0)
					{
						R2=R2/R3;
					}
					C2+=N*exp((-1)*R2/(foods[ppp].T0+En[0].timer*1e-3f))/(foods[ppp].T0+En[0].timer*1e-3f);
					N=0;
					R2=0;
				}
			}
			//Ws[p].food_Con.insert(Ws[p].food_Con.end(), C1);
			Ws[p].food_Con[i]=C1;//C1 is the valuw which collect all foods concentration
			Ws[p].chem_Con[i]=C2;
			//cerr<<C1<<"\t"<<C2<<"\r\n";
			C1=0;
			C2=0;
		}//for(int i=0;i<4;i++)//F=0,B=1,L=2,R=3   end
	}//for(int p=0;p<Con.size();p++) end
}

void envifunction::Sensor(vector<worm_sense>& Ws, int debug)//----1=F;2=B;3=L;4=R
{
	if(En[0].TransformA.size() == 1 && En[0].TransformB.size() == 1) //original
	{
		cerr<<"only one Transfer"<<endl;
		for(unsigned int i=0;i<Ws.size();i++)
		{
			for(int p=0;p<DEFAULT_DIR;p++)//four directions now(fixed)
			{
				Ws[i].food_firingrate[p]=En[0].TransformA[0]*Ws[i].food_Con[p]+En[0].TransformB[0];//calculate food firing rate
				Ws[i].chem_firingrate[p]=En[0].TransformA[0]*Ws[i].chem_Con[p]+En[0].TransformB[0];//calculate chem firing rate
			}
		}
	}
	else if(En[0].TransformA.size() == 4 && En[0].TransformB.size() == 4) //cross simulate
	{
		for(unsigned int i=0;i<Ws.size();i++)
		{
			for(int p=0;p<DEFAULT_DIR;p++)//four directions now(fixed)
			{
				Ws[i].food_firingrate[p]=(En[0].TransformA[0]*Ws[i].food_Con[p]+En[0].TransformB[0])+(En[0].TransformA[3]*Ws[i].chem_Con[p]+En[0].TransformB[3]);//calculate food firing rate
				Ws[i].chem_firingrate[p]=(En[0].TransformA[1]*Ws[i].food_Con[p]+En[0].TransformB[1])+(En[0].TransformA[2]*Ws[i].chem_Con[p]+En[0].TransformB[2]);//calculate chem firing rate
				/*cerr
				<<"direction:\t"<<p<<endl
				<<"food_con:\t"<<Ws[i].food_Con[p]<<endl
				<<"toxicant_con:\t"<<Ws[i].chem_Con[p]<<endl
				<<"A_FF:\t"<<En[0].TransformA[0]<<"\r\nB_FF:\t"<<En[0].TransformB[0]
				<<"\r\nA_FT:\t"<<En[0].TransformA[1]<<"\r\nB_FT:\t"<<En[0].TransformB[1]
				<<"\r\nA_TT:\t"<<En[0].TransformA[2]<<"\r\nB_TT:\t"<<En[0].TransformB[2]
				<<"\r\nA_TF:\t"<<En[0].TransformA[3]<<"\r\nB_TF:\t"<<En[0].TransformB[3]
				<<endl;*/
			}
		}
	}

	for(unsigned int i=0;i<Ws.size();i++) {
		Ws[i].NPY_sti = En[0].gainNPY*Wb[i].worm_life + En[0].baselineNPY;
		if(Ws[i].NPY_sti < 0)
			Ws[i].NPY_sti=0;
		if(debug==1) {
			cout
			<<"NPY stimulus["<<i<<"]="<<Ws[i].NPY_sti
			<<"\r\ngainNPY="<<En[0].gainNPY<<"\tbaselineNPY="<<En[0].baselineNPY<<"\r\n\r\n";
		}
	}
}

bool envifunction::Result(int ID, event_buffer& tEt)//collided with wall or worms overlap?
{
	int S=0,R=0;
	bool judge[2]={true,true};
	for(unsigned int i=0;i< Wb.size();i++) {
		//R:distance between two worms
		R=(Wb[ID].worm_location[0]-Wb[i].worm_location[0])*(Wb[ID].worm_location[0]-Wb[i].worm_location[0])+(Wb[ID].worm_location[1]-Wb[i].worm_location[1])*(Wb[ID].worm_location[1]-Wb[i].worm_location[1]);
		S=(Wb[ID].worm_size+Wb[i].worm_size)*(Wb[ID].worm_size+Wb[i].worm_size);
		if(i!=ID && R < S && overlap_ign==0) {//R < S：worms overlap
			judge[0]=false; 
			tEt.event.insert(tEt.event.end(), "m");
			string m_info = "touching worm: [" + Wb[i].worm_ID[0] + "]\t[" + Wb[i].worm_ID[1] + "]";
			tEt.event_info.insert(tEt.event_info.end(), m_info);
			Wb[ID].touch_worm+=1;
		}
		R=0;
		S=0;
	}
	//use "square" math method to check whether the worm collided with the wall or not 
	R=Wb[ID].worm_location[0]*Wb[ID].worm_location[0];//x^2
	S=Wb[ID].worm_location[1]*Wb[ID].worm_location[1];//y^2
	if(R> En[0].Boundary*En[0].Boundary || S> En[0].Boundary*En[0].Boundary) { 
		judge[1]=false;
		tEt.event.insert(tEt.event.end(), "b");
		Wb[ID].brick+=1;
		if(R> En[0].Boundary*En[0].Boundary) {
			if(Wb[ID].worm_location[0] < 0)
				tEt.event_info.insert(tEt.event_info.end(), "l");
			else
				tEt.event_info.insert(tEt.event_info.end(), "r");
		}
		else if(S> En[0].Boundary*En[0].Boundary) {
			if(Wb[ID].worm_location[1] < 0)
				tEt.event_info.insert(tEt.event_info.end(), "d");
			else
				tEt.event_info.insert(tEt.event_info.end(), "u");
		}
	}
	if(judge[0]==judge[1] && judge[0]==true)
		return true;//move
	if(judge[0]==false || judge[1]==false) {
		return false; //cannot move
	}
}

void envifunction::Activation(vector<neuron>& Nn, vector<event_buffer>& Et) {
	float R2=0.0;
	En[0].timer++; //TIME CHANGE to the current time
	//there is just "one" world now, so fixed En index to 0 (En[0])
	for(unsigned int i=0;i<Wb.size();i++) {//check every worm
		event_buffer tEt;//temp ST
		tEt.time = En[0].timer;
		tEt.worm_ID[0] = Wb[i].worm_ID[0];//set worm's ID for event buffer
		tEt.worm_ID[1] = Wb[i].worm_ID[1];
		
		if(Wb[i].liveornot==true) {  //for the living worm
		
			if(fixed != 1) //immobilizing worms if fixed = 1
					Wb[i].worm_life=Wb[i].worm_life-Wb[i].time_decay*1e-3f;//Time_decay：Hp decay for every sec
		
			for(unsigned int ii=0;ii<foods.size();ii++) { //for every molecules
				R2=(foods[ii].food_location[0]-Wb[i].worm_location[0])*(foods[ii].food_location[0]-Wb[i].worm_location[0])+(foods[ii].food_location[1]-Wb[i].worm_location[1])*(foods[ii].food_location[1]-Wb[i].worm_location[1]);
				
				//----touch the molecule or the worm is on the molecule----
				if(R2==(Wb[i].worm_size*Wb[i].worm_size) || R2<(Wb[i].worm_size*Wb[i].worm_size) ) { 
					if(foods[ii].count!=0 && R2>(Wb[i].worm_size-1)*(Wb[i].worm_size-1) ) { //if worm overlaps the molecule, the worm can't eat it
						if(foods[ii].food_ID[0]==1) { //get food
							tEt.event.insert(tEt.event.end(), "f");//food getting-status recording to tempEt
							Wb[i].get_food+=1;
							Wb[i].worm_life+=foods[ii].nutrient; 
							if(Wb[i].worm_life>100) 
							{
								Wb[i].worm_life=100.0;
								tEt.event_info.insert(tEt.event_info.end(), "HP-full");
							}
							else
							{
								string t_info = "+" + to_string(foods[ii].nutrient);
								tEt.event_info.insert(tEt.event_info.end(), t_info);
							}
						}
						if(foods[ii].food_ID[0]==0) {//get toxicant
							tEt.event.insert(tEt.event.end(), "t");
							string t_info = "poison (-" + to_string(foods[ii].nutrient) +")";//debuff
							tEt.event_info.insert(tEt.event_info.end(), t_info);
							Wb[i].worm_life+=(-1*foods[ii].nutrient); 
							Wb[i].get_toxi+=1;
							if(Wb[i].worm_life<0) 
								Wb[i].worm_life=0;
						}
						if(En[0].g_f_type[1]==0) //molecule decay
							foods[ii].count+=-1;
					}
				}
				//----end molecule getting----
				R2=0;
			}//for(int ii=0;ii<foods.size();ii++)//foods end

			//----worm movement----
			for(unsigned int ii=0;ii<Nn.size();ii++)
			{//check Nn vector has content
				if(Wb[i].worm_ID[0]==Nn[ii].worm_ID[0] && Wb[i].worm_ID[1]==Nn[ii].worm_ID[1] )
				{//confirm ID
					if(Nn[ii].move!=0 && fixed != 1)
					{//not silence, mobilizing
						//0 silence 1 Forward 2 Back 3 Left 4 Right			
						if(Nn[ii].move==1) 
							Wb[i].worm_location[1]+=1; 
						else if(Nn[ii].move==2)
							Wb[i].worm_location[1]+=-1; 
						else if(Nn[ii].move==3) 
							Wb[i].worm_location[0]+=-1;
						else if(Nn[ii].move==4)
							Wb[i].worm_location[0]+=1;

						if(Result(i, tEt)==true)
						{//moving
							Wb[i].step+=1;
							Wb[i].worm_life=Wb[i].worm_life-Wb[i].step_decay;
							//event recording
							if(Nn[ii].move==1) {
								tEt.event.insert(tEt.event.end(), "u");
								tEt.event_info.insert(tEt.event_info.end(), "-");
							}
							else if(Nn[ii].move==2) {
								tEt.event.insert(tEt.event.end(), "d");
								tEt.event_info.insert(tEt.event_info.end(), "-");
							}
							else if(Nn[ii].move==3) {
								tEt.event.insert(tEt.event.end(), "l");
								tEt.event_info.insert(tEt.event_info.end(), "-");
							}
							else if(Nn[ii].move==4) {
								tEt.event.insert(tEt.event.end(), "r");
								tEt.event_info.insert(tEt.event_info.end(), "-");
							}
							
						}
						else //if(Result(i)==false )
						{
							if(Nn[ii].move==1)
							{ Wb[i].worm_location[1]+=-1; }
							else if(Nn[ii].move==2)
							{ Wb[i].worm_location[1]+=1; }
							else if(Nn[ii].move==3)
							{ Wb[i].worm_location[0]+=1; }
							else if(Nn[ii].move==4)
							{ Wb[i].worm_location[0]+=-1; }

							//no move that no HP decrease
							//Wb[i].worm_life=Wb[i].worm_life-Wb[i].step_decay;
						}
					}
				}
			}//for(int ii=0;ii<W.size();ii++) end
			//----worm movement end----
			
			if(Wb[i].worm_life==0 || Wb[i].worm_life<0 ) {
				Wb[i].liveornot=false;
				Wb[i].worm_life=0.0;
				tEt.event.insert(tEt.event.end(), "x");
				tEt.event_info.insert(tEt.event_info.end(), "-");
			}
		}//if(Winf[i].Life==true) end
		
		if(tEt.event.size() > 0) 
			Et.insert(Et.end(), tEt);

	}//for(int i=0;i<Winf.size();i++) end

	Nn.clear(); //initialized Nn data
}

void envifunction::OutPut(int round, int timestep, vector<event_buffer>& Et)//file writting for "Locations.txt", "Event.dat"
{
	if(En[0].timer%timestep == 0) {
		//write file:
		fstream fp;
		fstream fe;
		string ef = "Event.dat";
		if(En[0].timer==0) {//first write
			fp.open(loc_name.c_str(), ios::out);
			fe.open(ef, ios::out);
			fe.close();
		}
		else
			fp.open(loc_name.c_str(), ios::out|ios::app);//open file
		if(!fp)//fail to open file:fp=0；success，fp!=0 
			cout<<"Fail to open file: "<<loc_name<<endl;
		else {
			fp<< En[0].timer<< "\t";
			for(unsigned int fpc=0; fpc<Wb.size(); fpc++) {
				fp 
				<< Wb[fpc].worm_ID[0] <<"\t"
				<< Wb[fpc].worm_ID[1] <<"\t"
				<< Wb[fpc].worm_location[0] * 0.1 <<"\t"
				<< Wb[fpc].worm_location[1] * 0.1 <<"\t"
				<< Wb[fpc].worm_life <<"\t";
			}
			fp<<"\r\n";
			fp.close();//close file
		}
		//end of writing "Location" file
		
		if(Et.size() > 0) {
			fe.open(ef, ios::out|ios::app);//open file
			if(!fp)//fail to open file:fp=0；success，fp!=0 
				cout<<"Fail to open file: "<<ef<<endl;
			else {
				for(unsigned int c1=0; c1<Et.size(); c1++) {
					for(unsigned int c2=0; c2<Et[c1].event.size(); c2++) {
						fe 
						<< Et[c1].time << "\t"
						<< Et[c1].worm_ID[0] << "\t"
						<< Et[c1].worm_ID[1] << "\t"
						<< Et[c1].event[c2] << "\t"
						<< Et[c1].event_info[c2] << endl;
					}
				}
				fe.close();//close file
			}
			
			vector<event_buffer> zero;
			Et.swap(zero);
		}
	}
};

void envifunction::print_Spike(vector<int>neumanage, vector<int>neupivot, vector<NeuronSpike>Ns, int round)
{	
	//write file:
	fstream fp;
	if(round==0)
		fp.open(spk_name.c_str(), ios::out);
	else
		fp.open(spk_name.c_str(), ios::out|ios::app);//open file
	if(!fp)//fail to open file:fp=0；success，fp!=0
	{
		cout<<"Fail to open file: "<<spk_name<<endl;
	}
	for(unsigned int ni=0; ni<Ns.size(); ni++)
	{
		//fp<<En[0].timer<<"\t";
		//fp<<"ot ";
		float ot = stof(Ns[ni].ot); //ot unit:0.1ms
		ot = ot * 0.1;
		fp<<ot<<"\t";
		fp<<Ns[ni].worm_ID[0]<<"\t";
		fp<<Ns[ni].worm_ID[1]<<"\t";
		fp<<Ns[ni].worm_ID[2]<<"\t";
		//fp<<"or ";
		//fp<<Ns[ni].o<<"\t";
		fp<<Ns[ni].neurontype<<"\r\n";
	}
	fp.close();//close file
	//end of writing file

}

void envifunction::recsim(const string& ss4, vector<worm_base> Wb, vector<NeuronSpike>& Ns,	 vector<neuron>& Nn, vector<int> neumanage, vector<int> neupivot, vector<read_file_neuron> circuit)
{
	NeuronSpike tns;

	string st="";
	vector<string> st2;
	for(unsigned int i=0; i < ss4.size(); i++)
	{
		if(ss4[i]!=' ')
			st+=ss4[i];
		else
		{
			st2.insert(st2.end(), st);
			st="";
		}
	}

	Ns.clear();// reset Spike neuron data
	//Nn.clear();

	for(unsigned int i=0; i<st2.size(); i++)
	{
		if(i%3==0)//TotalSteps
		{
			//cout<<st2[i]<<endl;
			if(i==0)// first one
			{
				tns.ot = st2[i];
				continue;
			}
			Ns.insert(Ns.end(), tns);
			tns.ot = st2[i];
			continue;
		}
		else if(i%3==1)
			tns.worm_ID[1] = st2[i].c_str();//worm id(now is correct? always "0"?)
		else if(i%3==2)
			tns.worm_ID[2] = st2[i].c_str();//neuron id
	}
	//insert last one
		Ns.insert(Ns.end(), tns);
	//====tns using end====
	
	//insert end----------------------
	//cout<<"insert end"<<endl;
	for(unsigned int i=1; i<=neupivot.size()/2; i++)
	{
		//i indicates which worm
		int h=neupivot[2*i-1] ;
		int t=neupivot[2*i] ;
		int pp=-1;
		int motorID = -1;
		vector<int>ss2;
		int fb=0;

		for(int j=h; j<=t; j++)
		{
			if(neumanage[j-1]==0)
			{
					fb=j-1;
					break; //find first brain circuit neuron index
			}
		}
		ss2.clear();
		for(int j=h-1; j<=t-1; j++)//index:j; h~t
		{
			
			for(unsigned int xy=0; xy<Ns.size(); xy++)//Ns : recording spikes~ //run every spikes neurons
			{
				if((Ns[xy].worm_ID[2]==to_string(j))&&(Ns[xy].record!=1))
				{
					Ns[xy].worm_ID[0] = Wb[i-1].worm_ID[0];//user id
					Ns[xy].worm_ID[1] = Wb[i-1].worm_ID[1];//worm id
					//sensor 0, motor 1, brain circuit 2
					if(neumanage[j]>0)//sensor or NPY
					{
						if(j==t-1) {//NPY
							Ns[xy].neurontype = "d";//modulatory neuron type
							Ns[xy].o = Ns[xy].worm_ID[2];
							Ns[xy].worm_ID[2] = "0";//d[0] = NPY neuron
							Ns[xy].record=1;// record already
						}
						else {
							Ns[xy].neurontype = "s";
							Ns[xy].o = Ns[xy].worm_ID[2];
							Ns[xy].worm_ID[2] = to_string(neumanage[j]-1);
							Ns[xy].record=1;// record already
						}
						break;
					}
					else if(neumanage[j]<0)//motor
					{
							Ns[xy].neurontype = "m";
							Ns[xy].move = neumanage[j]*(-1);				
							ss2.insert(ss2.end(), xy);
							Ns[xy].o = Ns[xy].worm_ID[2];
							Ns[xy].worm_ID[2] = to_string(Ns[xy].move-1);
							Ns[xy].record=1;// record already

							if (motorID == -1)//first time find motor neuron
							{
								motorID = atoi((Ns[xy].worm_ID[2]).c_str());
								pp=0;//motor neuron fired detection
							}
							else if(motorID != -1) //already have motor neuron fired
							{
								if (motorID != atoi((Ns[xy].worm_ID[2]).c_str()))//the spiking motor neuron same as before?
									pp=1; //more than one motor neuron fired at the same time
							}
							break;
					}
					else if(neumanage[j]==0)//brain circuit
					{
						Ns[xy].neurontype = "b";
						Ns[xy].o = Ns[xy].worm_ID[2];
						Ns[xy].worm_ID[2] = to_string(j - fb);
						Ns[xy].record=1;// record already
						break;
					}
				}
			}
		}//for(int j=h; j<=t; j++) end
		// run over one worm

		if(pp == 0) //there is only one motor neuron fire in this ms
		{
			/*neuron tNn;
			tNn.worm_ID[0]=Ns[ss2[0]].worm_ID[0];//userid
			tNn.worm_ID[1]=Ns[ss2[0]].worm_ID[1];//wormid
			tNn.move = Ns[ss2[0]].move;
			tMn.insert(tMn.end(), tNn);*/

			for(unsigned int s=0; s<Wm.size(); s=s+DEFAULT_DIR)
			{
				if(Wm[s].worm_ID[0] == Ns[ss2[0]].worm_ID[0])
					if(Wm[s].worm_ID[1] == Ns[ss2[0]].worm_ID[1])
					{
						int index = s +	Ns[ss2[0]].move - 1;				
						Wm[index].move++;//Use as counter
					}
			}
		}

	}//for(int i=1; i<=neupivot.size()/2; i++)
	//cout << "====Wm====" << "time: " << En[0].timer+1 << endl;
	//for(unsigned int s=0; s<Wm.size(); s++)
	//{
	//	cout << Wm[s].worm_ID[0] << " " << Wm[s].worm_ID[1] << " " << Wm[s].move << endl;
	//}

		if((En[0].timer+1)%MuscleConflict==0) //Muscle Conflict Detection
		{
			Nn.clear(); //last step's movement
			
			//cout << "\n"<<"Muscle Conflict Detection:"<<"\n"<<"----------"<<"\n"<<"[[[Wm]]]"<<"\n";
			/*for(int u=0; u<Wm.size(); u++)
			{
				cout << Wm[u].worm_ID[0] << "\t" << Wm[u].worm_ID[1] << "\t" << Wm[u].move << endl;
			}*/

			int dc = 0; //detect flag
			int mindex =-1;
			for(unsigned int mi=0; mi<Wm.size(); mi++)
			{
				if(Wm[mi].move > 0)
				{
					dc++;
					mindex = mi;
				}					
				
				if((mi+1)%DEFAULT_DIR == 0)
				{	
					//cout << "(mi+1)%DEFAULT_DIR==0"<<"\n"<<"time:"<<En[0].timer+1<<"\n detect flag="<<dc<<endl;

					if(dc == 1) //just one motor neuron fire during this time
					{
						neuron tNn;
						tNn.worm_ID[0]=Wm[mi].worm_ID[0];//userid
						tNn.worm_ID[1]=Wm[mi].worm_ID[1];//wormid
						tNn.move = (mindex % DEFAULT_DIR) +1;
						Nn.insert(Nn.end(), tNn);
						
						
					}					
					dc = 0; //reset detect flag
					/*cout << "[[[Nn]]]\n";
					for(int uu=0; uu<Nn.size(); uu++)
					{
						cout << Nn[uu].worm_ID[0] << "\t" << Nn[uu].worm_ID[1] << "\t" << Nn[uu].move << endl;
					}*/
				}
			}
			
			for(int u=0; u<Wm.size(); u++)//initialization
			{
				Wm[u].move = 0;
			}
			//cout << "\n"<<"Muscle Conflict Detection End"<<"\n"<<"----------"<<"\n";
		}

	
}
