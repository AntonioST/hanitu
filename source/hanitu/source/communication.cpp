/*
 * Copyright (C) 2015 Fang-Kuei Hsieh
 *
 * This file is part of Hanitu.
 *
 * Hanitu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hanitu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include"communication.h"
#include"matrix.h"
#include <iostream>//cin cout
#include <fstream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <sstream>
#include <stdlib.h>

#define DEFAULT_DIR 4 //worm direction count
commufunction::commufunction()
{
	neuc = 1;
	neupivot.insert(neupivot.end(), -1);//index=0 no record
}


int commufunction::readfilec(const string& wfname, int debug)

{
	ifstream readin;
	string str;
	string delimiters;
	vector<string> Setinf;
	int a=0,b=0;
	read_file_neuron tempw;//temp

	delimiters = "=\t\r\r\n";

	//Read file strat
	readin.open(wfname.c_str());

	if(!readin) {//Check read file success or not
		cout<<"fail read circuit file"<<wfname.c_str()<<endl;
		return -1;
	}

	while(!readin.eof())
		str+=readin.get(); 
	readin.close();
	//Read file end. All store in str

	//Collect strings to store in vector<string> Setinf
	while ( (b != string::npos) || (a != string::npos) )	{
		Setinf.push_back( str.substr(a, b - a) );//push_back :�@insert things into vector
		a = str.find_first_not_of(delimiters, b);
		b = str.find_first_of(delimiters, a);
	}
	//Collect end

	unsigned int index=0;
	for(;index<Setinf.size();index++)
		transform(Setinf[index].begin(), Setinf[index].end(), Setinf[index].begin(), ::tolower);

	//start put data into corresponding data structure
	NeuronPar tempn;
	ReceptorPar tempr;
	TargetPar tempt;
	
	for(index=0;index<Setinf.size();index++) {
		if(Setinf[index]=="communication") {
			index++;
			break;
		}
		else if(Setinf[index]=="totalneuronnumber")
				tempw.Total_neuron_number = atoi(Setinf[++index].c_str());
		else if(Setinf[index]=="neuronid")	{
			for(;Setinf[index]!="endneupar";index++)	{
				if(Setinf[index]=="c")
					tempn.C = stof(Setinf[++index].c_str());
				else if(Setinf[index]=="g")
					tempn.G = stof(Setinf[++index].c_str());
				else if(Setinf[index]=="mrevpot")
					tempn.NRevPot = stof(Setinf[++index].c_str());
				else if(Setinf[index]=="threshold")
					tempn.Threshold = stof(Setinf[++index].c_str());
				else if(Setinf[index]=="resetpot")
					tempn.ResetPot = stof(Setinf[++index].c_str());
				else if(Setinf[index]=="refperiod")
					tempn.Refperiod = stof(Setinf[++index].c_str());
				else if(Setinf[index]=="spikedelay")
					tempn.Spikedelay = stof(Setinf[++index].c_str());
				else if(Setinf[index]=="std")
					tempn.mem_std = Setinf[++index];
				else if(Setinf[index]=="mean")
					tempn.mem_mean = Setinf[++index];
			}
		}

		else if(Setinf[index]=="receptorpar")	{
			do{
				index++;
				if(Setinf[index]=="receptor")
					tempr.receptor_ID = atoi(Setinf[++index].c_str());
				else if(Setinf[index]=="type")
					tempr.Type = atoi(Setinf[++index].c_str());
				else if(Setinf[index]=="tau")
					tempr.Tau = stof(Setinf[++index].c_str());
				else if(Setinf[index]=="rrevpot")
					tempr.RRevPot = stof(Setinf[++index].c_str());
				else if(Setinf[index]=="endreceptor")	{
					tempn.Recptor.insert(tempn.Recptor.end(), tempr);
				}
			}while(Setinf[index]!="endreceptorpar");
		}

		else if(Setinf[index]=="targetneuron")	{
			tempt.target_ID = atoi(Setinf[++index].c_str());
			do{
				index++;
				if(Setinf[index]=="receptor")
					tempt.Receptor = atoi(Setinf[++index].c_str());
				else if(Setinf[index]=="weight")
					tempt.Weight = stof(Setinf[++index].c_str());
				else if(Setinf[index]=="g")
					tempt.G = stof(Setinf[++index].c_str());
			}while(Setinf[index]!="endtargetneuron");
			tempn.Target.insert(tempn.Target.end(), tempt);
		}
		else if(Setinf[index]=="endneuron")	{
			tempw.Neuron.insert(tempw.Neuron.end(), tempn);//one neuron done
			tempn.Recptor.clear();
			tempn.Target.clear();
		}
	}

	for(;index<Setinf.size();index++)	{	
		string flags="";
		if(Setinf[index]=="inputneuron")	{
			InputneuronPar tempi;
			do{
				index++;
				if(Setinf[index]=="neuronid")	{
					tempi.NeuID = atoi(Setinf[++index].c_str());
					flags+="1";
				}
				else if(Setinf[index]=="receptor")	{
					tempi.Receptor = atoi(Setinf[++index].c_str());
					flags+="2";
				}
				else if(Setinf[index]=="weight")	{
					tempi.Weight = stof(Setinf[++index].c_str());
					flags+="3";
				}
				else if(Setinf[index]=="g")	{
					tempi.G = stof(Setinf[++index].c_str());
					flags+="4";
				}
				else if(Setinf[index]=="type")	{
					tempi.Type = atoi(Setinf[++index].c_str());
					flags+="5";
				}
				else if(Setinf[index]=="direction")	{
					tempi.Diraction = atoi(Setinf[++index].c_str());
					flags+="6";
				}
				if(flags.size()>=6)	{		
					if(flags=="123456")	
						tempw.Inputneuron.insert(tempw.Inputneuron.end(), tempi);
					else	{
						cout<<"Inputneuron Part parameter error."<<endl;
						exit(EXIT_FAILURE);
					}
					flags="";
				}
			}while(Setinf[index]!="endinputneuron");
		}
		else if(Setinf[index]=="outputneuron")	{
			OutputneuronPar tempo;
			do{
				index++;
				if(Setinf[index]=="neuronid")	{
					tempo.NeuID = atoi(Setinf[++index].c_str());
					tempw.Outputneuron.insert(tempw.Outputneuron.end(), tempo);
				}
			}while(Setinf[index]!="endoutputneuron");
		}
		else if(Setinf[index]=="npytargetneuron") {
			NPYTargetNeuronPar tempnpy;
			for(int npyn = 0; Setinf[index]!="endnpypar"; npyn++) {
				index++;
				if(npyn == 4) {
					npyn = 0;
					tempw.NPYTargetNeuron.insert(tempw.NPYTargetNeuron.end(), tempnpy);				
				}
				if(Setinf[index]=="neuronid")
					tempnpy.target_ID = atoi(Setinf[++index].c_str());
				else if(Setinf[index]=="receptor")
					tempnpy.Receptor = atoi(Setinf[++index].c_str());
				else if(Setinf[index]=="weight") 
					tempnpy.Weight = stof(Setinf[++index].c_str());
				else if(Setinf[index]=="g") 
					tempnpy.G = stof(Setinf[++index].c_str());
			}
			if(debug==1)
				cout<<"\r\nNPYTargetNeuron number="<<tempw.NPYTargetNeuron.size()<<"\r\n\r\n";	
		}
		else if(Setinf[index]=="bodypar") {
			do{
				index++;
				if(Setinf[index]=="mcm")
					tempw.Mcm = Setinf[++index]; 
				else if(Setinf[index]=="mtau")
					tempw.Mtau = Setinf[++index];
				else if(Setinf[index]=="mweight")
					tempw.Mweight = Setinf[++index];
				else if(Setinf[index]=="msilence")
					tempw.Msilence = Setinf[++index];
				else if(Setinf[index]=="mvth")
					tempw.Mvth = Setinf[++index];
				else if(Setinf[index]=="mvl")
					tempw.Mvl = Setinf[++index];
				else if(Setinf[index]=="mreset")
					tempw.Mreset = Setinf[++index];
				else if(Setinf[index]=="sfcm")
					tempw.SFcm = Setinf[++index];
				else if(Setinf[index]=="sftau")
					tempw.SFtau = Setinf[++index];
				else if(Setinf[index]=="sfweight")
					tempw.SFweight = Setinf[++index];
				else if(Setinf[index]=="sfsilence")
					tempw.SFsilence = Setinf[++index];
				else if(Setinf[index]=="sfvth")
					tempw.SFvth = Setinf[++index];
				else if(Setinf[index]=="sfvl")
					tempw.SFvl = Setinf[++index];
				else if(Setinf[index]=="sfreset")
					tempw.SFreset = Setinf[++index];
				else if(Setinf[index]=="stcm")
					tempw.SCcm = Setinf[++index];
				else if(Setinf[index]=="sttau")
					tempw.SCtau = Setinf[++index];
				else if(Setinf[index]=="stweight")
					tempw.SCweight = Setinf[++index];
				else if(Setinf[index]=="stsilence")
					tempw.SCsilence = Setinf[++index];
				else if(Setinf[index]=="stvth")
					tempw.SCvth = Setinf[++index];
				else if(Setinf[index]=="stvl")
					tempw.SCvl = Setinf[++index];
				else if(Setinf[index]=="streset")
					tempw.SCreset = Setinf[++index];
				if(Setinf[index]=="npycm")
					tempw.NPYcm = Setinf[++index];
				else if(Setinf[index]=="npytau")
					tempw.NPYtau = Setinf[++index];
				else if(Setinf[index]=="npyweight")
					tempw.NPYweight = Setinf[++index];
				else if(Setinf[index]=="npysilence")
					tempw.NPYsilence = Setinf[++index];
				else if(Setinf[index]=="npyvth")
					tempw.NPYvth = Setinf[++index];
				else if(Setinf[index]=="npyvl")
					tempw.NPYvl = Setinf[++index];
				else if(Setinf[index]=="npyreset")
					tempw.NPYreset = Setinf[++index];
			}while(Setinf[index]!="endbodypar");
		}
		else if(Setinf[index]=="endcommunication")
			break;
	}
	//end put data to corresponding place

	//insert to circuit

	circuit.insert(circuit.end(), tempw);
	
	return 0;
}

void commufunction::ftraslate1(int id, int round, int debug)//Deal with one file(user circuit file) each time
{
	int msneucount = DEFAULT_ORN*DEFAULT_DIR + circuit[id].Outputneuron.size();
	if(debug==1)
		cerr<<"motor + sensory = "<<msneucount<<endl;
	if(round==0)
	{
		//sensor neuron
		neupivot.insert(neupivot.end(), neuc);//start point
		int nidconvert=neuc-1;
		int idt = 0; //temp variable
		vector<ORNtypetable> sflag;//sensory neurons' set flag

		//initialize-1
		sflag.clear();
		for(unsigned int sf=0; sf<DEFAULT_DIR; sf++)
		{
			ORNtypetable tO;
			for(int torn=0; torn<DEFAULT_ORN; torn++)
				tO.typeflag[torn]=0;
			sflag.insert(sflag.end(), tO);
		}

		for (unsigned int i = 0; i<circuit[id].Inputneuron.size(); i++) //i: input neuron order
			sflag[circuit[id].Inputneuron[i].Diraction].typeflag[circuit[id].Inputneuron[i].Type]=-1;//unsetted = -1, setted = 0

		if(debug==1) {
			cout<<"dir\tfoods\ttoxi"<<endl;
			for(int q=0; q<sflag.size(); q++) {
				cout<<q<<"\t";
				for(int qq=0; qq<DEFAULT_ORN; qq++)
					cout<<sflag[q].typeflag[qq]<<"\t";
				cout<<endl;			
			}
		}

		//Start to make network.conf
		//neuc strats from 1
		//netconf = "";
		//sensor neuron-----------------------------------------------
		if(debug==1)
			cerr<<"input neuron number: "<<circuit[id].Inputneuron.size()<<endl;
		for (unsigned int i = 0; i<DEFAULT_ORN*DEFAULT_DIR; i++) //i: input neuron order
		{
				string s1="";
				string s;
				int typee=-1;
				stringstream ss(s);
				ss << neuc;
				int exc = i+1;
				neumanage.insert(neumanage.end(), exc); //sensory neuron input
				//ss << exc;
				netconf += "NeuralPopulation: Exc" + ss.str() + "\r\n";
				ss.str("");
				ss.clear();
				netconf += "N=1\r\n";
				if(i<4)//foods' sensory neurons
				{
					netconf += "C=" + circuit[id].SFcm + "\r\n";

					float cc = stof(circuit[id].SFcm.c_str());
					float res = cc/2.5; //tau = c/g, g=2.5 
					res=res*1000; //sec -> msec
					ss << res;
					netconf += "Taum=" + ss.str() + "\r\n";
					ss.str("");
					ss.clear();
					netconf += "RestPot=" + circuit[id].SFvl + "\r\n";
					netconf += "ResetPot=" + circuit[id].SFreset + "\r\n";
					netconf += "Threshold=" + circuit[id].SFvth + "\r\n";
					netconf += "RefactoryPeriod=" + circuit[id].SFsilence + "\r\n";
					typee = 0;
				}
				else//toxicants' sensor neurons
				{
					netconf += "C=" + circuit[id].SCcm + "\r\n";

					float cc = stof(circuit[id].SCcm.c_str());
					float res = cc/2.5; //tau = c/g, g=2.5 ns
					res=res*1000; //sec -> msec
					ss << res;
					netconf += "Taum=" + ss.str() + "\r\n";
					ss.str("");
					ss.clear();
					netconf += "RestPot=" + circuit[id].SCvl + "\r\n";
					netconf += "ResetPot=" + circuit[id].SCreset + "\r\n";
					netconf += "Threshold=" + circuit[id].SCvth + "\r\n";
					netconf += "RefactoryPeriod=" + circuit[id].SCsilence + "\r\n";
					typee = 1;
				}
				netconf += "\r\n";

				netconf += "Receptor:AMPA\r\n";
				if(circuit[id].Inputneuron[i].Type==0)
					netconf += "Tau=" + circuit[id].SFtau + "\r\n";
				else if(circuit[id].Inputneuron[i].Type==1)
					netconf += "Tau=" + circuit[id].SCtau + "\r\n";
				netconf += "RevPot=0\r\n";//AMPA must be 0
				netconf += "FreqExt=0\r\n";
				float cc1 = 2.1;
				float cc2 = 0;
				if(circuit[id].Inputneuron[i].Type==0)
					cc2 = stof(circuit[id].SFweight.c_str());
				else if(circuit[id].Inputneuron[i].Type==1)
					cc2 = stof(circuit[id].SCweight.c_str());
				cc1=cc1*cc2;
				ss << cc1;
				netconf += "MeanExtEff=" + ss.str() + "\r\n";
				ss.str("");
				ss.clear();
				netconf += "MeanExtCon=1\r\n";
				netconf += "EndReceptor\r\n";
				netconf += "\r\n";
			//Target neurons' setting

			for(unsigned int tn=0; tn<circuit[id].Inputneuron.size(); tn++)
			{
				if((circuit[id].Inputneuron[tn].Diraction== i%4)&&(circuit[id].Inputneuron[tn].Type == typee))
				{
					string s1="";
					idt = circuit[id].Inputneuron[tn].NeuID + msneucount + 1 + nidconvert;//Change to relative NO
					netconf += "TargetPopulation: Exc" + std::to_string(idt) + "\r\n";

					if(circuit[id].Inputneuron[tn].Receptor==0)
						s1="AMPA";
					else if(circuit[id].Inputneuron[tn].Receptor==1)
						s1="GABA";
					/*else if(circuit[id].Inputneuron[tn].Receptor==2)//we don't have this circuit now
						s1="NMDA";*/
					netconf += "TargetReceptor=" + s1 + "\r\n";
					
					float res=0;
					res = circuit[id].Inputneuron[tn].Weight * circuit[id].Inputneuron[tn].G;
					netconf += "MeanEff=" + std::to_string(res) + "\r\n";

					netconf += "EndTargetPopulation\n\r\n";
				}
			}
			netconf += "EndNeuralPopulation\n\r\n";
			neuc++;
			//Finish one neuron
		}
		
		//motor neuron insert
		for(int i=0;i<circuit[id].Outputneuron.size();i++)
		{
			neumanage.insert(neumanage.end(), -(i+1));
		}

		//motor neuron-----------------------------------------------
		for(unsigned int i=0;i<circuit[id].Outputneuron.size();i++)
		{
			string s1="";
			string s;
			stringstream ss(s);
			ss << neuc;
			netconf += "NeuralPopulation: Exc" + ss.str() + "\r\n";
			ss.str("");
			ss.clear();

			netconf += "N=1\r\n";
			netconf += "C=" + circuit[id].Mcm + "\r\n";//"\r\n"

			float cc = stof(circuit[id].Mcm.c_str());
			float res = cc/2.5; //tau = c/g, g=2.5 ns
			res = res * 1000; //sec -> msec
			ss << res;
			netconf += "Taum=" + ss.str() + "\r\n";
			ss.str("");
			ss.clear();
			netconf += "RestPot=" + circuit[id].Mvl + "\r\n";
			netconf += "ResetPot=" + circuit[id].Mreset + "\r\n";
			netconf += "Threshold=" + circuit[id].Mvth + "\r\n";
			netconf += "RefactoryPeriod=" + circuit[id].Msilence + "\r\n";
			netconf += "\r\n";

			netconf += "Receptor:AMPA\r\n";
			netconf += "Tau=" + circuit[id].Mtau + "\r\n";
			netconf += "RevPot=0\r\n";//AMPA must be 0
			netconf += "FreqExt=0\r\n";
			float cc1 = 2.1;
			float cc2 = stof(circuit[id].Mweight.c_str());
			cc1=cc1*cc2;
			ss << cc1;
			netconf += "MeanExtEff=" + ss.str() + "\r\n";
			ss.str("");
			ss.clear();
			netconf += "MeanExtCon=1\r\n";
			netconf += "EndReceptor\n\r\n";
			netconf += "EndNeuralPopulation\n\r\n";
			//motor neurons don't have target neuron!! Just receive Spikes.
			neuc++;
			//Finish one neuron
		}
		//After finishing one neuron, don't forget to "neuc++"
		
		//brain circuit neuron insert
		for(unsigned int i=0;i<circuit[id].Neuron.size();i++)
		{
			neumanage.insert(neumanage.end(), 0);
		}

		//brain circuit neuron-----------------------------------------------
		unsigned int outn=0;
		int ssn = neuc; // record the first brain circuit neuron
		for(unsigned int i=0;i<circuit[id].Neuron.size();i++)
		{
			string s1="";
			string s;
			stringstream ss(s);
			ss << neuc;
			netconf += "NeuralPopulation: Exc" + ss.str() + "\r\n";
			ss.str("");
			ss.clear();
			netconf += "N=1\r\n";
			ss << circuit[id].Neuron[i].C;
			netconf += "C=" + ss.str() + "\r\n";
			ss.str("");
			ss.clear();

			float res = circuit[id].Neuron[i].C/circuit[id].Neuron[i].G; //tau = c/g, g=2.5 ns
			res = res * 1000; //sec -> msec
			ss << res;
			netconf += "Taum=" + ss.str() + "\r\n";
			ss.str("");
			ss.clear();
			ss << circuit[id].Neuron[i].NRevPot;
			netconf += "RestPot=" + ss.str() + "\r\n";
			ss.str("");
			ss.clear();
			netconf += "ResetPot=" + to_string(circuit[id].Neuron[i].ResetPot) + "\r\n";
			ss << circuit[id].Neuron[i].Threshold;
			netconf += "Threshold=" + ss.str() + "\r\n";
			ss.str("");
			ss.clear();
			ss << circuit[id].Neuron[i].Refperiod;
			netconf += "RefactoryPeriod=" + ss.str() + "\r\n";
			ss.str("");
			ss.clear();
			ss << circuit[id].Neuron[i].Spikedelay;
			netconf += "SpikeDly=" + ss.str() + "\r\n";
			ss.str("");
			ss.clear();

			netconf += "\r\n";

			for(unsigned int j=0; j<circuit[id].Neuron[i].Recptor.size(); j++)
			{
				if(circuit[id].Neuron[i].Recptor[j].receptor_ID==0)
					netconf += "Receptor:AMPA\r\n";
				else if(circuit[id].Neuron[i].Recptor[j].receptor_ID==1)
					netconf += "Receptor:GABA\r\n";
				/*else if(circuit[id].Neuron[i].Recptor[j].receptor_ID==2)
				netconf += "Receptor:NMDA\r\n";*/
				ss << circuit[id].Neuron[i].Recptor[j].Tau;
				netconf += "Tau=" + ss.str() + "\r\n";
				ss.str("");
				ss.clear();
				ss << circuit[id].Neuron[i].Recptor[j].RRevPot;
				netconf += "RevPot=" + ss.str() + "\r\n";
				ss.str("");
				ss.clear();
				netconf += "FreqExt=0\r\n";
				netconf += "MeanExtEff=2.1\r\n";
				//netconf += "MeanExtCon=1\r\n";

				if(circuit[id].Outputneuron[outn].NeuID == i)
					netconf += "MeanExtCon="+ circuit[id].Mweight +"\r\n";
				else
					netconf += "MeanExtCon=1\r\n";

				netconf += "EndReceptor\r\n";
				netconf += "\r\n";
			}

			if(circuit[id].Outputneuron[outn].NeuID == i)
			{
				if(outn<circuit[id].Outputneuron.size()-1)
					outn++;
			}

			for(unsigned int j=0; j<circuit[id].Neuron[i].Target.size(); j++)
			{
				idt = circuit[id].Neuron[i].Target[j].target_ID + msneucount + 1 + nidconvert;//Change to relative NO
				ss << idt;
				netconf += "TargetPopulation: Exc" + ss.str() + "\r\n";
				ss.str("");
				ss.clear();
				if(circuit[id].Neuron[i].Target[j].Receptor==0)
					s1="AMPA";
				else if(circuit[id].Neuron[i].Target[j].Receptor==1)
					s1="GABA";
				/*else if(circuit[id].Neuron[i].Target[j].Receptor==2)//we don't have this circuit now
				s1="NMDA";*/
				netconf += "TargetReceptor=" + s1 + "\r\n";
				float res=0;
				res = circuit[id].Neuron[i].Target[j].Weight * circuit[id].Neuron[i].Target[j].G;
				ss << res;
				netconf += "MeanEff=" + ss.str() + "\r\n";
				ss.str("");
				ss.clear();
				netconf += "EndTargetPopulation\n\r\n";
			}

			//check if this is pre-motor neuron
			for(unsigned int ii = 0; ii < circuit[id].Outputneuron.size(); ii++)
			{
				if((ssn + circuit[id].Outputneuron[ii].NeuID)==neuc) //this is pre-motor neuron
				{
					idt = msneucount - circuit[id].Outputneuron.size()  + ii + 1 + nidconvert;//Change to relative motor neuron NO
					ss << idt;
					netconf += "TargetPopulation: Exc" + ss.str() + "\r\n";
					ss.str("");
					ss.clear();

					s1="AMPA";//because the document only define Mtau..etc data, means "AMPA" receptor
					netconf += "TargetReceptor=" + s1 + "\r\n";

					float res=0;
					res = stof(circuit[id].Mweight.c_str()) * 2.5;
					ss << res;
					netconf += "MeanEff=" + ss.str() + "\r\n";
					ss.str("");
					ss.clear();
					netconf += "EndTargetPopulation\n\r\n";
				}
			}

			netconf += "EndNeuralPopulation\n\r\n";
			neuc++;
			//Finish one neuron
		}//brain end	
		
		//insert NPYsensitive neuron
		neumanage.insert(neumanage.end(), neuc); 
		netconf += "NeuralPopulation: Exc" + to_string(neuc) + "\r\n";
		netconf += "N=1\r\n";
		netconf += "C=" + circuit[id].NPYcm + "\r\n";
		float cc = stof(circuit[id].NPYcm.c_str());
		float res = cc/2.5; //tau = c/g, g=2.5 
		res=res*1000; //sec -> msec
		netconf += "Taum=" + to_string(res) + "\r\n";
		netconf += "RestPot=" + circuit[id].NPYvl + "\r\n";
		netconf += "ResetPot=" + circuit[id].NPYreset + "\r\n";
		netconf += "Threshold=" + circuit[id].NPYvth + "\r\n";
		netconf += "RefactoryPeriod=" + circuit[id].NPYsilence + "\r\n\r\n";
		netconf += "Receptor:AMPA\r\nTau=" + circuit[id].NPYtau + "\r\n";
		netconf += "RevPot=0\r\n";//AMPA must be 0
		netconf += "FreqExt=0\r\n";
		float cc1_npy = 2.1;
		float cc2_npy = stof(circuit[id].NPYweight.c_str());
		cc1_npy=cc1_npy*cc2_npy;
		netconf += "MeanExtEff=" + to_string(cc1_npy) + "\r\n";
		netconf += "MeanExtCon=1\r\n";
		netconf += "EndReceptor\r\n\r\n";

		for(unsigned int tn=0; tn<circuit[id].NPYTargetNeuron.size(); tn++) {
			idt = msneucount + circuit[id].NPYTargetNeuron[tn].target_ID + 1; //NPY target neuron must be body neurons
			netconf += "TargetPopulation: Exc" + std::to_string(idt) + "\r\n";
			if(circuit[id].NPYTargetNeuron[tn].Receptor==0)
				netconf += "TargetReceptor=AMPA\r\n";
			else if(circuit[id].NPYTargetNeuron[tn].Receptor==1) 
				netconf += "TargetReceptor=GABA\r\n";

			float res_npy = circuit[id].NPYTargetNeuron[tn].Weight * circuit[id].NPYTargetNeuron[tn].G;
			netconf += "MeanEff=" + to_string(res_npy) + "\r\n";
			netconf += "EndTargetPopulation\n\r\n";
			}
		netconf += "EndNeuralPopulation\n\r\n";
		//NPY insert end
		neuc++;

		//network.conf Done
		neupivot.insert(neupivot.end(), neuc-1);//end point

		if(debug==1) {
			cout<<"neumanage content:"<<endl;
			for(int q=0; q< neumanage.size(); q++) {
				cout<<q<<"\t"<<neumanage[q]<<endl;		
			}
			cout<<"neupivot content:"<<endl;
			for(int q=0; q< neupivot.size(); q++) {
				cout<<q<<"\t"<<neupivot[q]<<endl;		
			}
			cout<<"dir\tfoods\ttoxi"<<endl;
			for(int q=0; q<sflag.size(); q++) {
				cout<<q<<"\t";
				for(int qq=0; qq<DEFAULT_ORN; qq++)
					cout<<sflag[q].typeflag[qq]<<"\t";
				cout<<endl;			
			}
		}

	}//for round=1

	//FreqExt
	int jj = neupivot[2*(id+1)-1];//make one worm'f pro file each for-loop (according to "id")
	for(; jj <= neupivot[2*(id+1)]; jj++)//the neurons range of this worm (according to "id")
	{
		if(debug==1) {
			cerr<<"jj="<<jj<<endl;
			cerr<<"neumanage[jj-1]="<<neumanage[jj-1]<<endl;
		}

		if(neumanage[jj-1] == jj && neumanage[jj-1]>DEFAULT_ORN*DEFAULT_DIR) {//NPY
			if(debug==1)			
				cerr<<"find NPY neuron jj="<<jj<<endl;
			netpro += "EventTime 0\r\n";
			netpro += "Type=ChangeExtFreq\r\n";
			netpro += "Label=#1#\r\n";
			netpro += "Population: Exc" + to_string(jj) + "\r\n";
			netpro += "Receptor: AMPA\r\n";//all set to AMPA
			netpro += "FreqExt=" + to_string(Ws[id].NPY_sti) + "\r\n";
			netpro += "EndEvent\n\r\n";
		}		
		else if(neumanage[jj-1] > 0)//find input(sensory)neuron
		{
			if(debug==1)
				cerr<<"find sensory neuron\r\n";
			netpro += "EventTime 0\r\n";
			netpro += "Type=ChangeExtFreq\r\n";
			netpro += "Label=#1#\r\n";
			netpro += "Population: Exc" + to_string(jj) + "\r\n";
			netpro += "Receptor: AMPA\r\n";//all set to AMPA
			if(neumanage[jj-1]<=DEFAULT_DIR)
			{
				netpro += "FreqExt=" + to_string(Ws[id].food_firingrate[neumanage[jj-1]-1]) + "\r\n";
			}
			else if(neumanage[jj-1]>DEFAULT_DIR)
			{
				int pi = neumanage[jj-1]%DEFAULT_DIR-1;
				if(pi<0)
					pi=DEFAULT_DIR-1;
				netpro += "FreqExt=" + to_string(Ws[id].chem_firingrate[pi]) + "\r\n";
			}
			netpro += "EndEvent\n\r\n";
		}
	}
	//FreqExt----end	

	if(round == 0)
	{
		if(debug==1)
		{
			cout<<"IN---"<<endl;
			for(int teid=0; teid<circuit.size(); teid++)
			{
				cout<<"teid="<<teid<<endl;
				for(int teN=0; teN <circuit[teid].Neuron.size(); teN++)
				{
					cout<<circuit[teid].Neuron[teN].mem_mean<<endl;
				}
			}
			cout
			<<"END---teid"<<endl
			<<"neupivot[2*(id+1)-1]="<<neupivot[2*(id+1)-1]<<" neupivot[2*(id+1)]="<<neupivot[2*(id+1)]<<endl;
		}
		for(unsigned int j=neupivot[2*(id+1)-1]; j<=neupivot[2*(id+1)]; j++)
		{ 
			if(debug==1)
				cout<<"j="<<j<<" neumanage[j-1]="<<neumanage[j-1]<<endl;		
			if(neumanage[j-1]!=0) 
			{//sensory/motor/NPY sensitive neuron				
				//membrane noise start
				netpro += "EventTime 0\r\n";
				netpro += "Type=ChangeMembraneNoise\r\n";
				netpro += "Label=#1#\r\n";
				netpro += "Population: Exc" + to_string(j) +"\r\n";
				netpro += "GaussMean=0\r\n";
				netpro += "GaussSTD=0.5\r\n";
				netpro += "EndEvent\n\r\n";
				//membrane noise end
				//cout<<"sensory/motor/npy ya!"<<endl;
			}
			else
			{
				int circuit_neurons_c=0;
				if(id>0) //not first worm
				{
					for(int w_id_c=0; w_id_c<=id-1; w_id_c++)
						circuit_neurons_c+=circuit[w_id_c].Neuron.size();
				}
				int body_index = j-1-msneucount*(id+1)-id-circuit_neurons_c;
				
				
				
				if(debug==1)
				{
					cout
					<<"body_index="<<body_index
					<<" j="<<j
					<<" msneucount="<<msneucount
					<<" id="<<id
					<<" circuit[id].Neuron.size()="<<circuit[id].Neuron.size()<<endl;
					//cout
					//<<"id="<<id<<" mean="<<circuit[id].Neuron[body_index].mem_mean<<endl
					//<<"body_index="<<body_index<<endl
					//<<"j="<<j<<" msneucount="<<msneucount<<" id="<<id<<" circuit[id].Neuron.size()="<<circuit[id].Neuron.size()<<endl;
				}
				netpro += "EventTime 0\r\n";
				netpro += "Type=ChangeMembraneNoise\r\n";
				netpro += "Label=#1#\r\n";
				netpro += "Population: Exc" + to_string(j) +"\r\n";
				netpro += "GaussMean=" + circuit[id].Neuron[body_index].mem_mean+ "\r\n";
				netpro += "GaussSTD=" + circuit[id].Neuron[body_index].mem_std + "\r\n";
				netpro += "EndEvent\n\r\n";
			}
		}
		//network.pro Done(Need to add Outcontrol in main.cpp)
	}
}
