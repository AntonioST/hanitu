/*
 * Copyright (C) 2015 Fang-Kuei Hsieh
 *
 * This file is part of Hanitu.
 *
 * Hanitu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hanitu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "matrix.h"
#include "communication.h"
#include "cltcmd.h"
#include <iostream>
#include <time.h>
#include <sstream>
//signal
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

//speed
#include <ctime>
#include <ratio>
#include <chrono>

using namespace std;

//default parameter
#define DEFAULT_DIR 4 //worm direction count

//default STATE
#include "state.h"

/*EXIT CODE
0: normal exit
2: reading file fail
3: writing file fail 
4: flysim error
*/

void my_handler(int s)
{
	char s3[]="0";
	CltCmd("FILE_WRITE","network.pro",s3);
	CltClose(); 
	exit(0); 
}

char *GetChAry(string tmp) //char *GetChAry(char *argv)
{
  char buf32[32], *s2=nullptr;
  
  s2 = (char *) malloc(tmp.size()+1);
  strcpy(s2,tmp.c_str());
  s2[tmp.size()]='\0';

  return s2;
};

int main(int argc, char * const argv[])
{
	struct sigaction sigIntHandler;

	sigIntHandler.sa_handler = my_handler;
 	sigemptyset(&sigIntHandler.sa_mask);	
  	sigIntHandler.sa_flags = 0;

  	sigaction(SIGINT, &sigIntHandler, NULL);
	sigaction(SIGTERM, &sigIntHandler, NULL);
	
	signal(SIGINT, CltSigHdl);

	//all data store in this two datastructure
	envifunction Ef;
	commufunction Cf;
	int round=0;
	int TIME_LIMIT=-1;//experimental timeout
	int FILE_OUTPUT=-1;
	int LOC_OUTPUT_TIMESTEP=2;
	int SPEED_TEST=-1;
	int DEBUG=-1;
	float HP_setting=-1;

	//clock_t start, finish;//velocity measurement variable
	using namespace std::chrono;
	steady_clock::time_point t1, t2, t3, t4;//velocity measurement variable
	duration<double> time_span;
	vector<double> circuit_trans;
	vector<double> cpfile_make;
	vector<double> cpfile_write;
	vector<double> get_spike;

	t3 = steady_clock::now();

	fstream fpsp;
	string st;
	stringstream stt(st);
	double titi;

	string sa;
	int state = STATE_initialize;

	string w_name="";	

	char cmd1[]="FILE_WRITE";
	char cmd2[]="FILE_READ";//read simulator output
	char cmd3[]="ADD_WORM";
	char cmd4[]="DEL_WORM";
	char cmd5[]="DO_EVENTS";
	char *s2=nullptr;
	char s3[]="0";
	char *s4=nullptr;
	char *s5;
	char cmd6[]="DATA_READ";
	char cmd7[]="FILE_SEND";

	char opt6[]="Spikes_Header";
	char opt7[]="Spikes";
	char opt8[]="Spikes_Cont";

	char opt9[]="Membrane_Potential_Header";
	char opt10[]="Membrane_Potential";
	char opt11[]="Membrane_Potential_Cont";

	char rtt[3]={'1', '0'};
	char *rtime = rtt; //return time period
	
	char filename[]="network.conf";
	char filename1[]="network.pro";

	if(argc==1) //previous version usage
	{
		w_name = WORLD_CONF;
	}
	else
	{
		for(unsigned int c=0; c<argc; c++)
		{
			sa = argv[c];
			switch(state)
			{
				case STATE_initialize:
				state = STATE_command;
				break;
	
				case STATE_command:
				if(sa == COMMAND_location)
					state = STATE_location;
				else if(sa == COMMAND_location_short)
					state = STATE_location;
				else if(sa == COMMAND_spike)
					state = STATE_spike;
				else if(sa == COMMAND_spike_short)
					state = STATE_spike;
				else if(sa == COMMAND_world)
					state = STATE_wcfg;
				else if(sa == COMMAND_world_short)
					state = STATE_wcfg;
				else if(sa == COMMAND_timeout_set)
					state = STATE_timeout;
				else if(sa == COMMAND_location_timestep)
					state = STATE_loctimestep;
				else if(sa == COMMAND_overlapign) {
					Ef.overlap_ign = 1;
					state = STATE_command;
				}
				else if(sa == COMMAND_speed_test) {
					SPEED_TEST = 1;
					state = STATE_command;
				}
				else if(sa == COMMAND_output_files) {
					FILE_OUTPUT = 1;
					state = STATE_command;
				}
				else if(sa == COMMAND_debug) {
					DEBUG = 1;
					state = STATE_command;
				}
				else if(sa == COMMAND_version) {
					cout
					<<"Hanitu source code version:\n\t"
					<<VERSION
					<<endl;
					exit(EXIT_FAILURE);
				}
				else if(sa == COMMAND_HP_setting) 
					state = STATE_HP_setting;
				else if(sa == COMMAND_changeport)
					state = STATE_change_port;
				else if(sa == COMMAND_debug_h) {
					cout
					<<COMMAND_speed_test<<":\n\t\t"
					<<"Output speed testing data\n\n"
					<<COMMAND_output_files<<":\n\t\t"
					<<"Output circuit.conf and circuit.pro\n\n"
					<< COMMAND_debug<<":\n\t\t"
					<<"Output debugging data\n\n"
					<<COMMAND_HP_setting<<" [HP]:\n\t\t"
					<<"Set initial HP\n\n";
					exit(EXIT_FAILURE);
				}
				else {//--help/-h/error command
					cout
					<<"version:\n\t "
					<<VERSION					
					<<"\n\n"
					<<"Options:\n\t"
					<<COMMAND_version<<":\n\t\t"
					<<"version information\n\n\t"
					<<COMMAND_world_short<<", "<<COMMAND_world<<" [FILE]:\n\t\t"
					<<"world config file(default: "<<WORLD_CONF<<")\n\n\t"
					<<COMMAND_location_short<<", "<<COMMAND_location<<" [FILE]:\n\t\t"
					<<"set Location output file name(default: "<<LOCATIONS<<")\n\n\t"
					<<COMMAND_spike_short<<", "<<COMMAND_spike<<" [FILE]:\n\t\t"
					<<"set Spike output file name(default: "<<SPIKE<<")\n\n\t"
					<<COMMAND_timeout_set<<" [TIME]:\n\t\t"
					<<"set simulation timeout(ms), program will run without terminated by default\n\n\t"
					<<COMMAND_location_timestep<<" [TIME]:\n\t\t"
					<<"set Location file's data output timestep(ms), default=2ms\n\n\t"
					<<COMMAND_overlapign<<":\n\t\t"
					<<"allow worms overlap\n\n\t"
					<<COMMAND_changeport<<":\n\t\t"
					<<"change port\n\n\t"
					<<"-h, --help:\n\t\t"
					<<"print this help document\n\n\t"
					<<COMMAND_debug_h<<":\n\t\t"
					<<"print debug related help document\n\n"
					<<"contact:\n\thttp://life.nthu.edu.tw/~lablcc/index.html\n\n";
					exit(EXIT_FAILURE);
				}
				break;
			
				case STATE_location:
				Ef.loc_name = sa;	
				state = STATE_command;	
				break;

				case STATE_spike:
				Ef.spk_name = sa;
				state = STATE_command;
				break;

				case STATE_wcfg:
				w_name = sa;
				state = STATE_command;
				break;

				case STATE_timeout:
				TIME_LIMIT = atoi(sa.c_str());
				state = STATE_command;
				break;

				case  STATE_loctimestep:
				LOC_OUTPUT_TIMESTEP = atoi(sa.c_str());
				state = STATE_command;
				break;
		
				case STATE_HP_setting:
				HP_setting = atof(sa.c_str());
				state = STATE_command;
				break;
				
				case STATE_change_port:
				PORT = sa.c_str();
				state = STATE_command;
			}
		}
	}	

	//check folder exist for output file or not
	fstream tempf; 
	tempf.open(Ef.loc_name.c_str(), ios::out);
	if(!tempf) {//read file success or not
		cout<<"locations output setting error\n";
		return 3; 
	}
	tempf.close();
	tempf.open(Ef.spk_name.c_str(), ios::out);
	if(!tempf) {//read file success or not
		cout<<"spikes output setting error\n";
		return 3; 
	}
	tempf.close();

	if(w_name=="") //check world_config file is assigned or not yet
	{
		w_name = WORLD_CONF;//set to default
	}

	unsigned int w_p = w_name.find_last_of("/\\");
	if(w_p != -1)
		Cf.c_path = w_name.substr(0,w_p+1);//world config file and circuit file need be in the same folder
	
	int file_result_w = Ef.readfile(w_name, Cf.Ws, Cf.Nn, HP_setting);
	
	if(file_result_w != 0)
		return 2;
	
	Ef.OutPut(round, LOC_OUTPUT_TIMESTEP, Ef.Et);

	Cf.netconf="";//not every round rewrite conf file

	while(Ef.result()==true )//determine whether the game continues or not
	{
		if(TIME_LIMIT!=-1 && Ef.En[0].timer == TIME_LIMIT)//experiment used: setting & fixed time	
			break;	

		Ef.ConOutput(Cf.Ws);//count the concentration of every directions //envifunction Ef
		Ef.Sensor(Cf.Ws, DEBUG);//count the firing rate of every directions & NPY//commufunction Cf

		if(DEBUG==1) {
			for(unsigned int i=0;i<Cf.Ws.size();i++)
			{
				for(int p=0;p<DEFAULT_DIR;p++)//four directions now(fixed)
				{
					if(DEBUG ==1) {
						cout
						<<"Cf.Ws["
						<<i
						<<"].chem_firingrate["
						<<p
						<<"]="
						<<Cf.Ws[i].chem_firingrate[p]<<endl
						<<"Cf.Ws["
						<<i
						<<"].food_firingrate["
						<<p
						<<"]="
						<<Cf.Ws[i].food_firingrate[p]
						<<endl;
					}
				}
			}
		}

		Cf.netpro="";
		t1 = steady_clock::now();

		for(unsigned int id=0; id<Ef.Wb.size(); id++)
		{	
			if(round==0)//for first time, due to conf file won't change during progress
			{
				int file_result_c = Cf.readfilec(Cf.c_path+Ef.file[id], DEBUG);
				if(file_result_c != 0)
					return 2;
			}
			Cf.ftraslate1(id, round, DEBUG);//conf(for round0), pro file make	
		}

		//outcontrol of network.pro (pro file)
		
		Cf.netpro += "OutControl\n\n";

		Cf.netpro += "FileName:Spikes.dat\n";
		Cf.netpro += "Type=Spike\n";
		Cf.netpro += "population:AllPopulation\n";
		Cf.netpro += "EndOutputFile\n";
		Cf.netpro += "\n";

		Cf.netpro += "EndOutControl\n";
		//conf and pro are already done.

		t2 = steady_clock::now();
		time_span = duration_cast<duration<double>>(t2 - t1);

		cpfile_make.insert(cpfile_make.end(), time_span.count());
		
		if(FILE_OUTPUT == 1) {		
			//write file:network.conf
			char filename[]="network.conf";
			fstream fp;
			fp.open(filename, ios::out);
			if(!fp) {
				cout<<"Fail to open file: "<<filename<<endl;
				return 2;
			}
			for(unsigned int fpc=0; fpc<Cf.netconf.size(); fpc++)
				fp <<Cf.netconf[fpc] ;
			fp.close();
			
			//write file:network.pro
			char filename1[]="network.pro";
			fp.open(filename1, ios::out);
			if(!fp) {
				cout<<"Fail to open file: "<<filename1<<endl;
				return 2;
			}
			for(unsigned int fpc=0; fpc<Cf.netpro.size(); fpc++) 
				fp << Cf.netpro[fpc];
			fp.close();
		}

		//velocity measurement start: FILE_WRITE*2(conf, pro) +ADD_WORM*1

		t1 = steady_clock::now();
		if(round==0)
		{
			if(CltInit()<0) {
				cerr
				<<"Flysim error!"<<endl
				<<"Please check whether flysim is running or not."<<endl
				<<"You can change port and restart if flysim is running"<<endl
				<<"or just restart flysim."
				<<endl;
				return 4;			
			}
			CltInit();
			//s2=s3;
  			s2 = GetChAry(Cf.netconf);
  			
  			CltCmd(cmd7,filename,s2);
			CltCmd(cmd3,filename,s2);//ADD_WORM
			delete s2;
		}
		//printf("\ntest %s for FILE_SEND\n",filename1);//**
  		s2 = GetChAry(Cf.netpro);
  		CltCmd(cmd7,filename1,s2);
  		
  		delete s2;
  		
  		
		//CltCmd(cmd1,filename1,s3);//FILE_WRITE,filename:pro
		t2 = steady_clock::now();
		time_span = duration_cast<duration<double>>(t2 - t1);

		circuit_trans.insert(circuit_trans.end(), time_span.count());

		//s2=s3;
		
		t1 = steady_clock::now();
		CltCmd(cmd5,filename1,s3);//DO_EVENTS, pro
		s4=CltCmd(cmd6,opt8,rtime);//DATA_READ, Spikes_cont
		t2 = steady_clock::now();
		time_span = duration_cast<duration<double>>(t2 - t1);

		get_spike.insert(get_spike.end(), time_span.count());		

 		string ss4(s4);
		delete s4;
		//receive sipke data from simulator

		if(ss4!="END_DATA_READ")
			Ef.recsim(ss4, Ef.Wb, Cf.Ns, Cf.Nn, Cf.neumanage, Cf.neupivot, Cf.circuit);//return's spike data and translate to Hanitu's format
		//spike output-------------------------------------------
		ss4.erase();


		Ef.Activation(Cf.Nn, Ef.Et);
		Ef.OutPut(round, LOC_OUTPUT_TIMESTEP, Ef.Et);//round = 0 need to open new txt.document
		Ef.print_Spike(Cf.neumanage, Cf.neupivot, Cf.Ns, round);

		Cf.Ns.clear();
		round++;
	}
	//Ef.OutPut(round);
	//CltCmd(cmd4,"0",s2); //DEL_WORM
	
	//for outputting Hp=0
	if(Ef.En[0].timer%LOC_OUTPUT_TIMESTEP != 0)
	{
		Ef.En[0].timer = (Ef.En[0].timer/LOC_OUTPUT_TIMESTEP + 1) * LOC_OUTPUT_TIMESTEP;
		Ef.OutPut(round, LOC_OUTPUT_TIMESTEP, Ef.Et);
	}
	//-------------------
	
	//CltCmd(cmd4,filename,s2);

	CltClose();
	if(SPEED_TEST == 1) {
		t4 = steady_clock::now();
		time_span = duration_cast<duration<double>>(t4 - t3);	

		string filess="Speed_test_summary.txt";
		fpsp.open(filess.c_str(), ios::app);

		double c_ave =  0, s_ave = 0, cp_ave = 0, cpw_ave = 0;

		for(size_t cn=0; cn<circuit_trans.size(); cn++) {
			c_ave += circuit_trans[cn];
		}
		c_ave = c_ave/circuit_trans.size();	

		for(size_t sn=0; sn<get_spike.size(); sn++) {
			s_ave += get_spike[sn];
		}
		s_ave = s_ave/get_spike.size();

		for(size_t sn=0; sn<cpfile_make.size(); sn++) {
			cp_ave += cpfile_make[sn];
		}
		cp_ave = cp_ave/cpfile_make.size();

		/*for(size_t sn=0; sn<cpfile_write.size(); sn++) {
			cpw_ave += cpfile_write[sn];
		}
		cpw_ave = cpw_ave/cpfile_write.size();
		*/
		fpsp 
		<< "transmit circuit time ave:\t" << c_ave
		<< "\r\nspike receive time ave:\t" << s_ave
		<< "\r\nconf and pro file make time ave:\t" << cp_ave
		//<< "\r\nconf and pro file write time ave:\t" << cpw_ave
		<< "\r\n10ms Location output time:\t" << time_span.count();

		fpsp.close();

		filess="Speed_test_details.txt";
		fpsp.open(filess.c_str(), ios::out);

		fpsp << "transmit circuit time:\r\n";
		for(size_t cn=0; cn<circuit_trans.size(); cn++) {
			fpsp << circuit_trans[cn] << "\r\n";
		}

		fpsp << "\r\n\r\nspike receive time:\r\n";
		for(size_t sn=0; sn<get_spike.size(); sn++) {
			fpsp << get_spike[sn] << "\r\n";
		}
		
		fpsp << "\r\n\r\nconf and pro file make time:\r\n";
		for(size_t sn=0; sn<cpfile_make.size(); sn++) {
			fpsp << cpfile_make[sn] << "\r\n";
		}
		/*
		fpsp << "\r\n\r\nconf and pro file write time:\r\n";
		for(size_t sn=0; sn<cpfile_write.size(); sn++) {
			fpsp << cpfile_write[sn] << "\r\n";
		}
		*/
		fpsp.close();
	}

	//----output satistic file----
	
	fstream fss;
	fss.open("statistic.csv", ios::out);
	fss
	<< "UID" << "\t"
	<< "WID" << "\t"
	<< "total_steps" << "\t"
	<< "get_food" << "\t"
	<< "get_toxi" << "\t"
	<< "total_brick" << "\t"
	<< "touch_worm" <<endl;
	
	for(unsigned int w=0; w<Ef.Wb.size(); w++) {
		fss
		<< Ef.Wb[w].worm_ID[0] << "\t"
		<< Ef.Wb[w].worm_ID[1] << "\t"
		<< Ef.Wb[w].step << "\t"
		<< Ef.Wb[w].get_food << "\t"
		<< Ef.Wb[w].get_toxi << "\t"
		<< Ef.Wb[w].brick << "\t"
		<< Ef.Wb[w].touch_worm <<endl;
	}
	fss.close();

	return 0;
}
