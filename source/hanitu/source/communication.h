/*
 * Copyright (C) 2015 Fang-Kuei Hsieh
 *
 * This file is part of Hanitu.
 *
 * Hanitu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hanitu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef  TEST
#define TEST

#include <math.h>
#include <vector>
#include <algorithm>
#include <string>
#define SIZEC 100000 
#define DEFAULT_ORN 2 //ORN default types number: 2(food, chemical)

//This part is the basis of data strutcture about sumulator needed(Flysim format)

using namespace std; 

class Sim_TargetPar//Flysim :Targetneuron
{
	string tpopulation;
	string treceptor;
	float MeanEff;
};

class Sim_ReceptorPar//Flysim :Receptor
{
public:
	string Receptor;
	float Tau;
	float RevPot;
	float FreqExt;
	float MeanExtEff;
	float MeanExtCon;
};

class simformat
{
public:
	string NeuralPopulation;//Exc1,Exc2...
	int N;
	float C;
	float Taum;
	float RestPot;
	float ResetPot;
	float Threshold;
	vector<Sim_ReceptorPar> Sim_Receptor;
	vector<Sim_TargetPar>Sim_Target;
};

class ReceptorPar//Circuit file: Receptor
{
public:
	int receptor_ID;
	int Type;
	float Tau;
	float RRevPot;
};

class TargetPar//Circuit file: Target
{
public:
	int target_ID;
	int Receptor;
	float Weight;
	float G;
};

class NeuronPar//Circuit file: neuron
{
public:
	float C;
	float G;
	float NRevPot;
	float ResetPot;
	float Threshold;
	float Refperiod;
	float Spikedelay;

	string mem_std;
	string mem_mean;

	vector<ReceptorPar> Recptor;
	vector<TargetPar> Target;
};

class InputneuronPar//Circuit file: Inputneuron
{
public:
	int NeuID;
	int Receptor;
	float Weight;
	float G;
	int Type;
	int Diraction;
};

class ORNtypetable//Inputneuron input related(different types of sensory neuron(ORN))
{
public:
	int typeflag[DEFAULT_ORN];
};

class OutputneuronPar//Circuit file: Outputneuron
{
public:
	int NeuID;
};

class NPYTargetNeuronPar//Circuit file: NPYTargetNeuron
{
public:
	int target_ID;
	int Receptor;
	float Weight;
	float G;
};

class read_file_neuron//Circuit file components
{
public:
	string user_ID;
	int Total_neuron_number;
	vector<NeuronPar> Neuron;
	vector<InputneuronPar> Inputneuron;
	vector<OutputneuronPar> Outputneuron;
	vector<NPYTargetNeuronPar> NPYTargetNeuron;
	string Mcm;
	string Mtau;
	string Mweight;
	string Msilence;
	string Mvth;
	string Mvl;
	string Mreset;

	string SFcm;
	string SFtau;
	string SFweight;
	string SFsilence;
	string SFvth;
	string SFvl;
	string SFreset;

	string SCcm;
	string SCtau;
	string SCweight;
	string SCsilence;
	string SCvth;
	string SCvl;
	string SCreset;

	string NPYcm;
	string NPYtau;
	string NPYweight;
	string NPYsilence;
	string NPYvth;
	string NPYvl;
	string NPYreset;
};

class neuron//receive the information Hanitu needed from simultor's return data
{
public:
	string worm_ID[2];//recording the worm which will be moving[UserID, WormID]
	int move;
	//move[0 silence 1 Forward 2 Back 3 Left 4 Right]
};

class worm_sense//trasmit to simulator(Flysim)
{
public:
	worm_sense();
	~worm_sense();

	string worm_ID[2];//[UserID, WormID]

	//the following data are defaulted to 4 directions
	vector<float> food_Con;//concentration
	vector<float> chem_Con;

	vector<float> food_firingrate;//firing rate
	vector<float> chem_firingrate;

	float NPY_sti;
};

class NeuronSpike
{
public:
		/*NeuronSpike();
		~NeuronSpike();*/
	string worm_ID[3];//[userID, wormID, neuronID]
	int count;//spikes' frequency count(for specific time)
	string neurontype;//sensor 0, motor 1, brain circuit 2
	int move;//move[0 silence 1 Forward 2 Back 3 Left 4 Right]
	int record;
	string o;//original neuron no
	string ot;//original time
	//debug
	int dir;
	int type;
	int j;
	int h;
};

class commufunction
{
public:
	commufunction();//initialized

	vector<read_file_neuron> circuit;//circuit file

	string netconf;
	string netpro;
	//the following two will be referenced to each other by index
	char netconfc[SIZEC];
	char netproc[SIZEC];

	vector<int> neumanage; //manage sensor, motor & brain circuit neuron//all users
	vector<int> neupivot;//record the worms neurons NO n(1st) - z(last one) 

	vector<int> neuspike;//receive sipke data from Flysim & count spike number of every neuron
	int neuc;

	vector<worm_sense>Ws;
	vector<NeuronSpike>Ns;
	vector<neuron>Nn;
	string c_path; //circuit file's path


	int readfilec(const string& wfnamem, int debug);//circuit file read(one user for one time)
	void ftraslate1(int id, int round, int debug);//(building)circuit to network.conf/.pro
};

#endif 
