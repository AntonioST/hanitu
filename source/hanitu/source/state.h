/*
 * Copyright (C) 2015 Fang-Kuei Hsieh
 *
 * This file is part of Hanitu.
 *
 * Hanitu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hanitu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//STATE definition

#define STATE_initialize 0
#define STATE_command 1
#define STATE_location 2 //change file name
#define STATE_spike 3 //change file name
#define STATE_wcfg 4 //world_config file
#define STATE_timeout 5 //set experimental timeout
#define STATE_loctimestep 6 //set Location output timestep
#define STATE_HP_setting 7
#define STATE_change_port 8

//command 
#define COMMAND_location "--location"
#define COMMAND_location_short "-l"
#define COMMAND_spike "--spike"
#define COMMAND_spike_short "-s"
#define COMMAND_world "--world"
#define COMMAND_world_short "-w"
#define COMMAND_timeout_set "--timeout"
#define COMMAND_location_timestep "--loc-time-step"
#define COMMAND_version "--version"
#define COMMAND_overlapign "--overlap-ignore"
#define COMMAND_speed_test "--speed-test"
#define COMMAND_output_files "--output-conf-pro-files"
#define COMMAND_HP_setting "--HP-set"
#define COMMAND_debug "--debug"
#define COMMAND_debug_h "--debug-h"
#define COMMAND_changeport "--port"


//document variable

#define VERSION "Hanitu_v1.4-r2"

//default file name

#define WORLD_CONF "world_config.wcg"
#define SPIKE "Spike.txt"
#define LOCATIONS "Locations.txt"
