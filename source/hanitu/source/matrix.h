/*
 * Copyright (C) 2015 Fang-Kuei Hsieh
 *
 * This file is part of Hanitu.
 *
 * Hanitu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hanitu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <math.h>
#include <vector>//vector
#include <algorithm>
#include <iostream>//cin cout
#include <fstream>//read file
#include <cstdlib>
#include <string>
#include"communication.h"
#include "state.h"

using namespace std; 

class worm_base//worm identification
{
public:
	worm_base();//initiallized function(construction)
	~worm_base();//destruction("plan to" delete the dead worm)
	string worm_ID[2];//[userID, wormID]
	float worm_size;//Size of worm (0.1mm)
	int worm_location_past[2];//[x, y] last worm's location, if worm's next step is wall, we can use this location to let worm recover state
	int worm_location[2];//[x, y]
	float worm_life;
	bool liveornot;//live oe not
	int move;//Will the worm move this round(time step)?[0 silence 1 Forward 2 Back 3 Left 4 Right]//can know which motor neuron is firing
	int get_food;
	int get_toxi;
	int brick;//touch wall counter
	int touch_worm;//touch worm friendly or want to fight
	int step;//worm moves one step each time step//record total steps worm moved
	float time_decay, step_decay;
	//float nutrient_weight;//different food's weight for different worm
};

class event_buffer//record event
{
	public:
		event_buffer();
		~event_buffer();
		
		int time;
		string worm_ID[2];//[userID, wormID]
		vector<string> event;
		vector<string> event_info;
};

class envi //There are many worlds in the future
{
	public:
		envi();
		~envi();
	float Boundary;//unit:0.1mm : world's length and width	
	int timer;//time counter
	vector<float> TransformA;//firing rate(Hz) = TransformA * [Odor](mM) + TransformB (concentration(food/chem) to firing rate(for sensory neuron))
	vector<float> TransformB;
	float gainNPY;
	float baselineNPY;
	int g_f_type[2];//The game end mode
	//g_f_type[0]=0( all the worm are dead ) type[0]=1(one worm gets food )   type[1]=0 change food count type[1]=1 do not change food count
};

class food
{
public:
	food();
	~food();
	int food_ID[2]; //ID[2]=[ID(foodtype_ID), food number] 
	int food_location[2];//location[2]={X,Y}0.1mm
	int count;//food count(the food's amount at the location ),10-6f L/Count for every food//count * concentration = real concentration(We don't use now)
	float concentration;//(1 mM )food's concentration //Par for pre version Hanitu
	float D;//1.86*10-5f cm*cm/sec diffusion parameter
	float T0;//delaytime( for foods' odor diffusion that worm(s) can detect )	
	float d;//0.264 cmPetri dish height(It doesn't matter to worms' moving)	
	float nutrient;//+-HP of after eating food
};

class read_file_world//read world_config
{
public:
	read_file_world();
	~read_file_world();

	//one worm's information, using vector index to identify each one 
	//vector <vector<string>> userID;
	vector <string> userID;
	vector <string> WormID;
	vector <int> InitialX;
	vector <int> InitialY;
	vector <float> Wormsize;
	vector <float> Time_decay;
	vector <float> Step_decay;
	vector <string> Filename;
	//end

	vector<float> Nutrient;
	vector<float> gainNPY;
	vector<float> baselineNPY;
	vector<float> TransformA;
	vector<float> TransformB;
	vector<float> Boundary;
	vector<int> Type;//The end of calculating，type=0 (all worms die)，type=1 (one worm gets food or chem) 
	vector<float> Depth;
	vector<int> CountMode;

	vector <int> FID;
	vector <int> F_X;
	vector <int> F_Y;
	vector <int> F_Count;
	vector <float> F_Diffuse;
	vector <float> F_Concentration;
	vector <float> F_Delay_time;

	vector <int> CID;
	vector <int> C_X;
	vector <int> C_Y;
	vector <int> C_Count;
	vector <float> C_Diffuse;
	vector <float> C_Concentration;
	vector <float> C_Delay_time;
};

class envifunction
{
public:
	envifunction();
	~envifunction();
	//【read_file_world】
	int readfile(const string& wfname, vector<worm_sense>& Ws, vector<neuron>& Nn, const float& HP);
	//---------------------------------------------------------------------------------------

	//【worm_base】
	bool result();//Game still continue?
	
	void Activation(vector<neuron>& Nn, vector<event_buffer>& Et);
	bool Result(int ID, event_buffer& tEt);

	void OutPut(int round, int timestep, vector<event_buffer>& Et);
	void print_Spike(vector<int>neumanage, vector<int>neupivot, vector<NeuronSpike>Ns, int round);

	//---------------------------------------------------------------------------------------
	void ConOutput(vector<worm_sense>& Ws);//calculate concentration
	void Sensor(vector<worm_sense>& Ws, int debug);//calculate firing rate
	float foodfr_calculate(float FC, float TC);//FC:food_Con; TC:toxicant_Con
	float toxfr_calculate(float FC, float TC);
	//---------------------------------------------------------------------------------------
	void recsim(const string& ss4, vector<worm_base> Wb, vector<NeuronSpike>& Ns,	vector<neuron>& Nn, vector<int> neumanage, vector<int> neupivot, vector<read_file_neuron> circuit);

	//----Variables----
	vector<worm_base> Wb;
	vector<envi>En;
	vector<food>foods;//Things can decrease/decline worms' HP.
	vector<string> file;//The circuit files will be read
	vector<neuron> tMn;//temp Motor neuron for Muscle Conflict Detection
	vector<neuron> Wm;//Motor neuron spike counter
	vector<event_buffer> Et;//status recoding buffer

	string spk_name;
	string loc_name;
	int fixed; //(default=0)
	int overlap_ign; //overlap ignore (default=0)
};
