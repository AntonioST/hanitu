#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef __unix__
#define DIRSP "/"
#elif defined(_WIN32) || defined(WIN32)
#define __windows__
#define DIRSP "\\"
#endif

#if !defined(HANITU_JAR)
#error "compile error: must define HANITU_JAR"
#endif

#if !defined(FLYSIM)
#error "compile error: must define FLYSIM"
#endif

#if !defined(HANITU)
#error "compile error: must define HANITU"
#endif


int is_file_exist(const char* filepath){
    FILE* fp = fopen(filepath, "r");
    if (fp) {
        fclose(fp);
        return 1;
    } else {
        return 0;
    }
}

int main(int argc, char* args[]) {
    int ret;
    #define STR_SIZE 1023
    char str[STR_SIZE + 1] = {0};
    char prefix[100] = {0};

    char* tdir = strrchr(args[0], DIRSP[0]);
    if (tdir == NULL) {
        prefix[0] = '\0';
    } else {
        strncpy(prefix, args[0], tdir - args[0] + 1);
    }

    //check file exist
    ret = snprintf(str, STR_SIZE, "%slib%s%s", prefix, DIRSP, HANITU_JAR);
    if (ret < 0 || !is_file_exist(str)) {
        printf("file not found : %s\n", str);
        return 1;
    }

    ret = snprintf(str, STR_SIZE, "%slib%s%s", prefix, DIRSP, FLYSIM);
    if (ret < 0 || !is_file_exist(str)) {
        printf("file not found : %s\n", str);
        return 1;
    }

    ret = snprintf(str, STR_SIZE, "%slib%s%s", prefix, DIRSP, HANITU);
    if (ret < 0 || !is_file_exist(str)) {
        printf("file not found : %s\n", str);
        return 1;
    }

    //find java
    ret = 0;
    if (ret == 0){
        ret = snprintf(str, STR_SIZE, "%sjre%sbin%sjava", prefix, DIRSP, DIRSP);
        if (ret < 0 || !is_file_exist(str)){
            ret = 0;
        }
    }
    if (ret == 0){
        ret = snprintf(str, STR_SIZE, "%sjre1.8%sbin%sjava", prefix, DIRSP, DIRSP);
        if (ret < 0 || !is_file_exist(str)){
            ret = 0;
        }
    }
    if (ret == 0){
        if (getenv("JAVA_HOME") != NULL){
            ret = snprintf(str, STR_SIZE, "%s%sbin%sjava", getenv("JAVA_HOME"), DIRSP, DIRSP);
            if (ret < 0 || !is_file_exist(str)){
                ret = 0;
            }
        }
    }
    if (ret == 0){
        printf("cannot not find java\n");
        return 1;
    }

    //set java arguments
    ret += snprintf(str + ret, STR_SIZE - ret, " -Dcclo.hanitu.0=%s", args[0]);
    ret += snprintf(str + ret, STR_SIZE - ret, " -Dcclo.hanitu.home=%s", prefix);
    ret += snprintf(str + ret, STR_SIZE - ret, " -Dcclo.hanitu.flysim=%slib%s%s", prefix, DIRSP, FLYSIM);
    ret += snprintf(str + ret, STR_SIZE - ret, " -Dcclo.hanitu.flysim.err=null");
    ret += snprintf(str + ret, STR_SIZE - ret, " -Dcclo.hanitu.flysim.out=null");
    ret += snprintf(str + ret, STR_SIZE - ret, " -Dcclo.hanitu.hanitu=%slib%s%s", prefix, DIRSP, HANITU);
    ret += snprintf(str + ret, STR_SIZE - ret, " -Dcclo.hanitu.hanitu.err=null");
    ret += snprintf(str + ret, STR_SIZE - ret, " -Dcclo.hanitu.hanitu.out=null");
//    ret += snprintf(str + ret, STR_SIZE - ret, " -Dcclo.hanitu.hanitu.version.world=1.3");
//    ret += snprintf(str + ret, STR_SIZE - ret, " -Dcclo.hanitu.hanitu.version.circuit=1.3");
    ret += snprintf(str + ret, STR_SIZE - ret, " -jar %slib%s%s world", prefix, DIRSP, HANITU_JAR);

    for(int i = 1; ret > 0 && i < argc; i++) {
        ret += snprintf(str + ret, STR_SIZE - ret, " %s", args[i]);
    }

    //
    if(ret < 0) {
        printf("command line length out of string buffer\n");
        return 1;
    }

    //call
    system(str);

    //exit
    return 0;
    #undef STR_SIZE
}
