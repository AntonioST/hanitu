Hanitu Project
==============

[Hanitu official website](http://hanitu.net)


Hanitu is a simulation environment that simulates behavior and neural activity of
user-designed virtual worms in a two-dimensional virtual world. In Hanitu, the users need to
think deeply about how different components of a nervous system work together to achieve
the ultimate goal of an animal – to survive in a changing environment.

Hanitu is designed for graduate and undergraduate students who already acquired basic
knowledge about computational neuroscience and would like to apply what they learned to
addressing some of the system-level questions in neuroscience.

Lastest version
---------------

project         | version
-------         | -------
flysim          | 06_12
Hanitu          | v1.4-r2-g2.5
HanituToolSet   | v2.5


Compile Flysim
--------------

Flysim source code is under public. It coming soon.


Compile Hanitu
--------------

### Build Request

* g++ 4.8 or latest
* make

### compile

```
cd source/hanitu/source
make
```

The final product is `Hanitu.out`

Compile HanituToolSet
---------------------

### Build Request

* java 1.8 or latest
* apache-ant 1.9 or latest
* apache-ivy

### compile

```
cd source/hanitutoolset
ant
```

The final product locate at `build/package`

Compile HanituGUI
-----------------

### Build Request

* gcc
* make

### Compile

```
cd source/hanitugui/src
make
```

The final product is `HanituGUI`

Compose Each other
------------------

```
cp -r source/hanitutoolset/build/package/ working
cp source/hanitugui/src/HanituGUI working/
cp binary/linux/x64/flysim.out working/lib/
cp source/hanitu/source/Hanitu.out working/lib/
```

### Directory Structure

```
working/
├── bin/
├── lib/
└── share/
```

How to Run
----------

directly double click `HanituGUI`


Bug Report
----------

Report a [issue](https://bitbucket.org/AntonioST/hanitu/issues)

Lab Information
---------------

[CCLo lab website](http://life.nthu.edu.tw/~lablcc/index.html)

